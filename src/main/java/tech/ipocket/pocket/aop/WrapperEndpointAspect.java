package tech.ipocket.pocket.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tech.ipocket.pocket.utils.LogWriterUtility;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;


@Aspect
@Order(1)
@Component
public class WrapperEndpointAspect {

    private static LogWriterUtility log = new LogWriterUtility(WrapperEndpointAspect.class);


    @Before("within(tech.ipocket.pocket.controllers..*)")
    public void endpointBefore(JoinPoint p) {

        String requestId=parseRequestId(p);

        log.info(requestId,"\n\n");

        log.info(requestId,"++++++++++++++++++++++++++++ REQUEST_IN +++++++++++++++++++++++++++++++++++++++++++");

        log.info(requestId,"IN TIME : "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                .getRequest();

        String requestUrl = request.getScheme() + "://" + request.getServerName()
                + ":" + request.getServerPort()  + request.getRequestURI();
        log.info(requestId,"\n"+requestUrl);
    }

    @After("within(tech.ipocket.pocket.controllers..*)")
    public void endpointAfter(JoinPoint p) {

        String requestId=parseRequestId(p);

        log.info(requestId,"OUT TIME : "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        log.info(requestId,"--------------------------------- REQUEST_OUT () --------------------------------------\n\n");
    }


    private String parseRequestId(JoinPoint p){
        String requestId="WRAPPER : " + System.currentTimeMillis();

        Object[] signatureArgs = p.getArgs();
        if (signatureArgs.length > 0) {
            Class baseRequestClass = signatureArgs[0].getClass();
            try {
                Method requestIdMethod = baseRequestClass.getMethod("getRequestId");
                requestId = requestIdMethod.invoke(signatureArgs[0]) != null ? requestIdMethod.invoke(signatureArgs[0]).toString() : "NO_REQUEST_ID : " + System.currentTimeMillis();
            } catch (NoSuchMethodException|IllegalAccessException|InvocationTargetException e1) {
            }
        }
        return requestId;
    }

}