package tech.ipocket.pocket.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.jsonwebtoken.JwtException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Aspect
@Order(2)
@Component
public class EndpointAspect {
    LogWriterUtility log = new LogWriterUtility(this.getClass());

    @Before("within(tech.ipocket.pocket.controllers..*)")
    public void endpointBefore(JoinPoint p) {
        log.info("", p.getTarget().getClass().getSimpleName() + " " + p.getSignature().getName() + " START");


        try {
            Object[] signatureArgs =  p.getArgs();
            String requestId = parseRequestId(p);

            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            try {

                if (signatureArgs!=null&&signatureArgs.length>0&&signatureArgs[0] != null) {
                    log.info(requestId, "\nRequest object: \n" + mapper.writeValueAsString(signatureArgs[0]));


                    /*String encryptData = parseEncryptData(p);
                    String secretKey = parseEncryptSecretKey(p);

                    String jsonData = EncryptDecrypt.decrypt(encryptData,secretKey);

                    System.out.println(jsonData);*/
                }
            } catch (JsonProcessingException e) {
//                System.out.println(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterReturning(value = ("within(tech.ipocket.pocket.controllers..*)"),
            returning = "returnValue")
    public void endpointAfterReturning(JoinPoint p, Object returnValue) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
            log.info("", "\nResponse object: \n" + mapper.writeValueAsString(returnValue));
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
        log.info("", p.getTarget().getClass().getSimpleName() + " " + p.getSignature().getName() + " END");
    }

    @AfterThrowing(pointcut = ("within(tech.ipocket.pocket.controllers..*)"), throwing = "ex")
    public void endpointAfterThrowing(JoinPoint p, Exception ex) throws PocketException {
        PocketException exception;

        if (ex == null) {
            exception = new PocketException("" + 400);
        } else if (ex instanceof PocketException) {
            exception = (PocketException) ex;
        } else if (ex instanceof JwtException) {
            exception = new PocketException("" + PocketErrorCode.SecurityException.getErrorCode());

        } else {
            exception = new PocketException("" + PocketErrorCode.UNEXPECTED_ERROR_OCCURRED.getErrorCode());
        }

        String requestId = parseRequestId(p, exception);
        exception.setStatusCode(400);
        exception.setRequestId(requestId);

        throw exception;
    }

    private String parseRequestId(JoinPoint p) {
        String requestId = "ENDPOINT_" + System.currentTimeMillis();

        Object[] signatureArgs = p.getArgs();
        if (signatureArgs.length > 0) {
            Class baseRequestClass = signatureArgs[0].getClass();
            try {
                Method requestIdMethod = baseRequestClass.getMethod("getRequestId");
                requestId = requestIdMethod.invoke(signatureArgs[0]) != null ? requestIdMethod.invoke(signatureArgs[0]).toString() : "NO_REQUEST_ID : " + System.currentTimeMillis();

                return requestId;

            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e1) {

            }
        }
        return requestId;
    }

    /*private String parseEncryptData(JoinPoint p) {
        String encryptData = "ENDPOINT_" + System.currentTimeMillis();

        Object[] signatureArgs = p.getArgs();
        if (signatureArgs.length > 0) {
            Class baseRequestClass = signatureArgs[0].getClass();
            try {
                Method requestIdMethod = baseRequestClass.getMethod("getEncryptedTextString");
                encryptData = requestIdMethod.invoke(signatureArgs[0]) != null ? requestIdMethod.invoke(signatureArgs[0]).toString() : "NO_REQUEST_ID : " + System.currentTimeMillis();

                return encryptData;

            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e1) {
                log.error(encryptData, e1.getMessage());

            }
        }
        return encryptData;
    }

    private String parseEncryptSecretKey(JoinPoint p) {
        String encryptedSecretKey = "ENDPOINT_" + System.currentTimeMillis();

        Object[] signatureArgs = p.getArgs();
        if (signatureArgs.length > 0) {
            Class baseRequestClass = signatureArgs[0].getClass();
            try {
                Method requestIdMethod = baseRequestClass.getMethod("getEncryptedSecretKeyString");
                encryptedSecretKey = requestIdMethod.invoke(signatureArgs[0]) != null ? requestIdMethod.invoke(signatureArgs[0]).toString() : "NO_REQUEST_ID : " + System.currentTimeMillis();

                return encryptedSecretKey;

            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e1) {
                log.error(encryptedSecretKey, e1.getMessage());

            }
        }
        return encryptedSecretKey;
    }*/

    private String parseRequestId(JoinPoint p, Exception exception) {
        String requestId = "ENDPOINT_" + System.currentTimeMillis();

        Object[] signatureArgs = p.getArgs();
        if (signatureArgs.length > 0) {
            Class baseRequestClass = signatureArgs[0].getClass();
            try {
                Method requestIdMethod = baseRequestClass.getMethod("getRequestId");
                requestId = requestIdMethod.invoke(signatureArgs[0]) != null ? requestIdMethod.invoke(signatureArgs[0]).toString() : "NO_REQUEST_ID : " + System.currentTimeMillis();

                if (exception != null) {
                    log.errorWithAnalysis(requestId, exception);
                }

            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e1) {
                log.error(requestId, e1.getMessage());
                if (exception != null) {
                    log.errorWithAnalysis(requestId, exception);
                }
            }
        }
        return requestId;
    }
}