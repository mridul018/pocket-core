package tech.ipocket.pocket.response;

import tech.ipocket.pocket.utils.PocketErrorCode;

public class BaseResponseObject {
    private Object data;
    private ErrorObject error;
    private String status;
    private String requestId;

    public BaseResponseObject() {
    }

    public BaseResponseObject(String requestId) {
        this.requestId = requestId;
    }

    public ErrorObject getError() {
        return error;
    }

    public void setError(ErrorObject error) {
        this.error = error;
    }

    public void setErrorCode(PocketErrorCode error) {
        this.error = new ErrorObject(error);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}