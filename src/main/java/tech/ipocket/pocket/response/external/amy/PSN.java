package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PSN {
    @SerializedName("ID")
    @Expose
    public String iD;
    @SerializedName("Typ")
    @Expose
    public String typ;
    @SerializedName("Title")
    @Expose
    public String title;
    @SerializedName("FNam")
    @Expose
    public String fNam;
    @SerializedName("LNam")
    @Expose
    public String lNam;
    @SerializedName("Mobile")
    @Expose
    public String mobile;
    @SerializedName("EMail")
    @Expose
    public String eMail;
    @SerializedName("DOBD")
    @Expose
    public String dOBD;
    @SerializedName("DOBM")
    @Expose
    public String dOBM;
    @SerializedName("DOBY")
    @Expose
    public String dOBY;
    @SerializedName("PP")
    @Expose
    public String pP;
    @SerializedName("PPEX")
    @Expose
    public String pPEX;
    @SerializedName("NAT")
    @Expose
    public String nAT;
    @SerializedName("NATC")
    @Expose
    public Object nATC;
    @SerializedName("VIP")
    @Expose
    public String vIP;
    @SerializedName("FFP")
    @Expose
    public Object fFP;
    @SerializedName("atn")
    @Expose
    public String atn;
    @SerializedName("PPScan")
    @Expose
    public Object pPScan;
    @SerializedName("VSScan")
    @Expose
    public Object vSScan;
    @SerializedName("OK")
    @Expose
    public String oK;

}
