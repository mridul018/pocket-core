package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

public class DAPILoginResponse extends BankResponseBase{
    @SerializedName("jobID")
    public String jobID;
}
