package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightDocumentUploadResponse {
    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("PPScan")
    @Expose
    public String pPScan;
}
