package tech.ipocket.pocket.response.external.banks;

import tech.ipocket.pocket.entity.UmBankList;

import java.util.ArrayList;

public class BankAddResponse {
    private ArrayList<UmBankList> bankLists;

    public BankAddResponse() {
    }

    public BankAddResponse(ArrayList<UmBankList> bankLists) {
        this.bankLists = bankLists;
    }

    public ArrayList<UmBankList> getBankLists() {
        return bankLists;
    }

    public void setBankLists(ArrayList<UmBankList> bankLists) {
        this.bankLists = bankLists;
    }
}
