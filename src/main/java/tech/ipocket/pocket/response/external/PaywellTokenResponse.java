package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

public class PaywellTokenResponse {
    @SerializedName("status")
    public Integer status;
    @SerializedName("message")
    public String message;
    @SerializedName("token")
    public Token token;
}
