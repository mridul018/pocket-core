package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BankUserInputResponse extends BankResponseBase{
    @SerializedName("jobID")
    public String jobID;
    @SerializedName("reference")
    public String reference;
    @SerializedName("userInputs")
    public ArrayList<UserInput> userInputs=null;
}
