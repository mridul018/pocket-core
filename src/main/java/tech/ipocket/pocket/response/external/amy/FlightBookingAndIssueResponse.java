package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightBookingAndIssueResponse {
    @SerializedName("stPNR")
    @Expose
    public StPNR stPNR;
    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("message")
    @Expose
    public String message;
}
