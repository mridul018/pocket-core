package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

public class BankMetadataResponse extends BankResponseBase{
    @SerializedName("jobID")
    public String jobID;
    @SerializedName("accountsMetadata")
    public AccountsMetadata accountsMetadata;

}
