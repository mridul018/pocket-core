package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

public class Beneficiaries {
    @SerializedName("name")
    public String name;
    @SerializedName("iban")
    public String iban;
    @SerializedName("accountNumber")
    public String accountNumber;
    @SerializedName("type")
    public String type;
    @SerializedName("status")
    public String status;
    @SerializedName("id")
    public String id;

}
