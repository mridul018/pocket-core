package tech.ipocket.pocket.response.external.amy;

import java.util.List;

public class DepartureListResponse {
    private List<RouteList> routeLists;

    public List<RouteList> getRouteLists() {
        return routeLists;
    }

    public void setRouteLists(List<RouteList> routeLists) {
        this.routeLists = routeLists;
    }
}
