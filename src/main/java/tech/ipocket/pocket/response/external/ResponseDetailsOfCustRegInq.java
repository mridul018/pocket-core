package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

public class ResponseDetailsOfCustRegInq {
    @SerializedName("Status")
    public String status;
    @SerializedName("StatusName")
    public String statusName;
    @SerializedName("TrxId")
    public String TrxId;
    @SerializedName("SmsAccountNo")
    public String SmsAccountNo;
    @SerializedName("SmsAccountName")
    public String SmsAccountName;
    @SerializedName("CustomerPhoneNo")
    public String CustomerPhoneNo;
    @SerializedName("TrxTime")
    public String TrxTime;
}
