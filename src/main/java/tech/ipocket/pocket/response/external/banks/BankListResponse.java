package tech.ipocket.pocket.response.external.banks;

import tech.ipocket.pocket.entity.UmBankList;

import java.util.ArrayList;

public class BankListResponse {
    private ArrayList<UmBankList> bankLists;

    public BankListResponse() {
    }

    public BankListResponse(ArrayList<UmBankList> bankLists) {
        this.bankLists = bankLists;
    }

    public ArrayList<UmBankList> getBankLists() {
        return bankLists;
    }

    public void setBankLists(ArrayList<UmBankList> bankLists) {
        this.bankLists = bankLists;
    }
}
