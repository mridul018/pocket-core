package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.SerializedName;

public class Trip {
    @SerializedName("fSoft")
    public String fSoft;
    @SerializedName("stAirline")
    public String stAirline;
    @SerializedName("stAirCode")
    public String stAirCode;
    @SerializedName("fReturn")
    public Boolean fReturn;
    @SerializedName("fNo")
    public String fNo;
    @SerializedName("fFrom")
    public String fFrom;
    @SerializedName("fDest")
    public String fDest;
    @SerializedName("fDTime")
    public String fDTime;
    @SerializedName("fATime")
    public String fATime;
    @SerializedName("fDur")
    public String fDur;
    @SerializedName("fStop")
    public String fStop;
    @SerializedName("fGDSid")
    public long fGDSid;
    @SerializedName("fAMYid")
    public long fAMYid;
    @SerializedName("fDesc")
    public Object fDesc;
    @SerializedName("fSize")
    public String fSize;
    @SerializedName("fBag")
    public Object fBag;
    @SerializedName("fClsNam")
    public String fClsNam;
    @SerializedName("fDisplay")
    public String fDisplay;
    @SerializedName("fFare")
    public long fFare;
    @SerializedName("fTFare")
    public long fTFare;
    @SerializedName("fTBFare")
    public long fTBFare;
    @SerializedName("fSeat")
    public String fSeat;
    @SerializedName("fPref")
    public long fPref;
    @SerializedName("fRefund")
    public Object fRefund;
    @SerializedName("fRouteType")
    public String fRouteType;
    @SerializedName("rPoint")
    public long rPoint;
    @SerializedName("fPromo1")
    public Object fPromo1;
    @SerializedName("fPromo1Det")
    public Object fPromo1Det;
    @SerializedName("fPromo2")
    public Object fPromo2;
    @SerializedName("fPromo2Det")
    public Object fPromo2Det;

}
