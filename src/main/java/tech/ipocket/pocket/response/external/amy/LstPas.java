package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LstPas {
    @SerializedName("stTitle")
    @Expose
    public String stTitle;
    @SerializedName("stCat")
    @Expose
    public long stCat;
    @SerializedName("stFNam")
    @Expose
    public String stFNam;
    @SerializedName("stLnam")
    @Expose
    public String stLnam;
    @SerializedName("stMob")
    @Expose
    public Object stMob;
    @SerializedName("stEMail")
    @Expose
    public Object stEMail;
    @SerializedName("stDOB")
    @Expose
    public String stDOB;
    @SerializedName("stTicket")
    @Expose
    public Object stTicket;
    @SerializedName("stBFare")
    @Expose
    public Float stBFare;
    @SerializedName("stTxFare")
    @Expose
    public Float stTxFare;
    @SerializedName("stTotFare")
    @Expose
    public Float stTotFare;
    @SerializedName("stVIP")
    @Expose
    public Object stVIP;
    @SerializedName("stFFP")
    @Expose
    public Object stFFP;
    @SerializedName("stPassport")
    @Expose
    public Object stPassport;
    @SerializedName("stPsExDt")
    @Expose
    public String stPsExDt;
    @SerializedName("stPsCountry")
    @Expose
    public Object stPsCountry;
    @SerializedName("stPsNation")
    @Expose
    public Object stPsNation;
}
