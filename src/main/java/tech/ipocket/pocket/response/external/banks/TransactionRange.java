package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionRange {
    @SerializedName("unit")
    @Expose
    public String unit;
    @SerializedName("value")
    @Expose
    public Integer value;
}
