package tech.ipocket.pocket.response.external;
import com.google.gson.annotations.SerializedName;

public class ResponseDetailsOfPollyBiddyutBillPayment {
    @SerializedName("Status")
    public Integer status;
    @SerializedName("StatusName")
    public String statusName;

    @SerializedName("TrxId")
    public String trxId;

    @SerializedName("MsgText")
    public String msgText;

    @SerializedName("BillNo")
    public String billNo;

    @SerializedName("BillAmount")
    public Integer billAmount;

    @SerializedName("ExtraCharge")
    public Integer extraCharge;

    @SerializedName("RetailerCommission")
    public String retailerCommission;

    @SerializedName("TotalAmount")
    public Integer totalAmount;
}
