package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.SerializedName;

public class RouteList {
    @SerializedName("lVal")
    public String lVal;
    @SerializedName("lTx")
    public String lTx;
}
