package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PSNList {
    @SerializedName("ID")
    @Expose
    public String iD;
    @SerializedName("Typ")
    @Expose
    public String typ;
    @SerializedName("Title")
    @Expose
    public String title;
    @SerializedName("FNam")
    @Expose
    public Object fNam;
    @SerializedName("LNam")
    @Expose
    public Object lNam;
    @SerializedName("Mobile")
    @Expose
    public Object mobile;
    @SerializedName("EMail")
    @Expose
    public Object eMail;
    @SerializedName("DOBD")
    @Expose
    public String dOBD;
    @SerializedName("DOBM")
    @Expose
    public String dOBM;
    @SerializedName("DOBY")
    @Expose
    public String dOBY;
    @SerializedName("PP")
    @Expose
    public Object pP;
    @SerializedName("PPEX")
    @Expose
    public String pPEX;
    @SerializedName("NAT")
    @Expose
    public String nAT;
    @SerializedName("NATC")
    @Expose
    public String nATC;
    @SerializedName("VIP")
    @Expose
    public Object vIP;
    @SerializedName("FFP")
    @Expose
    public Object fFP;
    @SerializedName("atn")
    @Expose
    public Object atn;
    @SerializedName("PPScan")
    @Expose
    public Object pPScan;
    @SerializedName("VSScan")
    @Expose
    public Object vSScan;
    @SerializedName("OK")
    @Expose
    public String oK;
}
