package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BankBeneficiaryAddResponse extends BankResponseBase{
    @SerializedName("jobID")
    public String jobID;
    @SerializedName("userInputs")
    public ArrayList<UserInput> userInputs = null;


}
