package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInput {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("query")
    @Expose
    public String query;
}
