package tech.ipocket.pocket.response.external;
import com.google.gson.annotations.SerializedName;

public class PollyBiddyutBillPaymentResponse {
    @SerializedName("ApiStatus")
    public Integer apiStatus;

    @SerializedName("ApiStatusName")
    public String apiStatusName;

    @SerializedName("OutletName")
    public String outletName;

    @SerializedName("OutletAddress")
    public String outletAddress;

    @SerializedName("CallCenter")
    public String callCenter;

    @SerializedName("RetailerCode")
    public String retailerCode;

    @SerializedName("ResponseDetails")
    public ResponseDetailsOfPollyBiddyutBillPayment responseDetailsOfPollyBiddyutBillPayment;
}
