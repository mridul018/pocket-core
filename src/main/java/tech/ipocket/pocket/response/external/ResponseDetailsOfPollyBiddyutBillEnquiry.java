package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

public class ResponseDetailsOfPollyBiddyutBillEnquiry {
    @SerializedName("Status")
    public Integer status;
    @SerializedName("StatusName")
    public String statusName;

    @SerializedName("TrxId")
    public String trxId;

    @SerializedName("MsgText")
    public String msgText;

    @SerializedName("BillNo")
    public String billNo;

    @SerializedName("BillAmount")
    public Integer billAmount;

    @SerializedName("ExtraCharge")
    public Integer extraCharge;

    @SerializedName("RetailerCommission")
    public String retailerCommission;

    @SerializedName("TotalAmount")
    public Integer totalAmount;

    @SerializedName("contact_no1")
    public String contactNo1;

    @SerializedName("issue_date")
    public String issueDate;

    @SerializedName("bill_month")
    public String billMonth;

    @SerializedName("bill_year")
    public String billYear;

    @SerializedName("due_date")
    public String dueDate;

    @SerializedName("payment_date")
    public String paymentDate;
}
