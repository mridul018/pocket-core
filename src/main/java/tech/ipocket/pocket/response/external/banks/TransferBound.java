package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransferBound {
    @SerializedName("minimum")
    @Expose
    public Integer minimum;
    @SerializedName("currency")
    @Expose
    public Currency currency;
    @SerializedName("type")
    @Expose
    public String type;
}
