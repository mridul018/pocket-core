package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BankBeneficiaryGetResponse extends BankResponseBase{

    @SerializedName("jobID")
    public String jobID;

    @SerializedName("beneficiaries")
    public ArrayList<Beneficiaries> beneficiaries = null;

    @SerializedName("userInputs")
    public ArrayList<UserInput> userInputs = null;

}
