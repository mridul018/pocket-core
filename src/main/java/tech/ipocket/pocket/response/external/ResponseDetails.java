package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

public class ResponseDetails {
    @SerializedName("Status")
    public int status;
    @SerializedName("status")
    public int custRegStatus;
    @SerializedName("message")
    public String message;
    @SerializedName("TrxId")
    public String trxId;
    @SerializedName("OutletName")
    public String outletName;
    @SerializedName("OutletAddress")
    public String outletAddress;
    @SerializedName("CallCenter")
    public String callCenter;
    @SerializedName("RetailerCode")
    public String retailerCode;
    @SerializedName("StatusName")
    public String statusName;
    @SerializedName("BillAmount")
    public String billAmount;
    @SerializedName("ExtraCharge")
    public Integer extraCharge;
    @SerializedName("TotalAmount")
    public Integer totalAmount;

}
