package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExchangeTokenResponse {
    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("accessToken")
    @Expose
    public String accessToken;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String msg;

}
