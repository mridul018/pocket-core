package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AccountsMetadata {
    @SerializedName("swiftCode")
    @Expose
    public String swiftCode;
    @SerializedName("sortCode")
    @Expose
    public String sortCode;
    @SerializedName("bankName")
    @Expose
    public String bankName;
    @SerializedName("branchName")
    @Expose
    public String branchName;
    @SerializedName("branchAddress")
    @Expose
    public String branchAddress;
    @SerializedName("address")
    @Expose
    public Address address;
    @SerializedName("transferBounds")
    @Expose
    public List<TransferBound> transferBounds = null;
    @SerializedName("beneficiaryCoolDownPeriod")
    @Expose
    public BeneficiaryCoolDownPeriod beneficiaryCoolDownPeriod;
    @SerializedName("isCreateBeneficairyEndpointRequired")
    @Expose
    public Boolean isCreateBeneficairyEndpointRequired;
    @SerializedName("country")
    @Expose
    public Country country;
    @SerializedName("transactionRange")
    @Expose
    public TransactionRange transactionRange;
    @SerializedName("userInputs")
    @Expose
    public List<UserInput> userInputs = null;
}
