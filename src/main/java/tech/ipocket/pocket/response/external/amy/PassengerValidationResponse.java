package tech.ipocket.pocket.response.external.amy;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerValidationResponse {
    @SerializedName("ID")
    @Expose
    public Object iD;
    @SerializedName("PSN")
    @Expose
    public PSN pSN;
    @SerializedName("fRoutType")
    @Expose
    public Object fRoutType;
    @SerializedName("dAirline")
    @Expose
    public Object dAirline;
    @SerializedName("dDepTime")
    @Expose
    public Object dDepTime;
    @SerializedName("fSoft")
    @Expose
    public Object fSoft;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("CMND")
    @Expose
    public Object cMND;
}
