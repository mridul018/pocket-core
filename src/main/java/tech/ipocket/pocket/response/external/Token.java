package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

public class Token {
    @SerializedName("security_token")
    public String securityToken;
    @SerializedName("token_exp_time")
    public String tokenExpTime;
    @SerializedName("token_type")
    public String tokenType;
    @SerializedName("ack_timestamp")
    public String ackTimestamp;
}
