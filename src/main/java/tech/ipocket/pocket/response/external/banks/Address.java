package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {
    @SerializedName("line1")
    @Expose
    public String line1;
    @SerializedName("line2")
    @Expose
    public String line2;
    @SerializedName("line3")
    @Expose
    public String line3;
}
