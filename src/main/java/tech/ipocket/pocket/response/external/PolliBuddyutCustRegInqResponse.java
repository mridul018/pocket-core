package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PolliBuddyutCustRegInqResponse {
    @SerializedName("ApiStatus")
    public Integer apiStatus;
    @SerializedName("ApiStatusName")
    public String apiStatusName;
    @SerializedName("ResponseDetails")
    public List<ResponseDetailsOfCustRegInq> responseDetailsList=new ArrayList<>();
}
