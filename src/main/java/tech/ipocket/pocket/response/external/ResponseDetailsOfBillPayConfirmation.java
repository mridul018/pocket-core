package tech.ipocket.pocket.response.external;

import com.google.gson.annotations.SerializedName;

public class ResponseDetailsOfBillPayConfirmation {
    @SerializedName("Status")
    public String status;
    @SerializedName("StatusName")
    public String statusName;
    @SerializedName("TrxId")
    public String TrxId;
    @SerializedName("BillNo")
    public String BillNo;
    @SerializedName("TotalAmount")
    public String TotalAmount;
    @SerializedName("TrxTime")
    public String TrxTime;
}
