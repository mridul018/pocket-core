package tech.ipocket.pocket.response.external.banks;

import com.google.gson.annotations.SerializedName;

public class BankResponseBase {
    @SerializedName("success")
    public boolean success;
    @SerializedName("status")
    public String status;
    @SerializedName("msg")
    public String msg;
    @SerializedName("type")
    public String type;
}
