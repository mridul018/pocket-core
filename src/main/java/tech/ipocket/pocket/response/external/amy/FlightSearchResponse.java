package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FlightSearchResponse {
    @SerializedName("success")
    public Boolean success;
    @SerializedName("message")
    public Object message;
    @SerializedName("SearchID")
    public Integer searchID;
    @SerializedName("Airlines")
    public String airlines;
    @SerializedName("Trips")
    public List<Trip> trips = new ArrayList<>();
}
