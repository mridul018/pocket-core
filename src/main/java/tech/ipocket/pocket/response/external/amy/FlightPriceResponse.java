package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FlightPriceResponse {
    @SerializedName("Token")
    @Expose
    public String token;
    @SerializedName("pUser")
    @Expose
    public String pUser;
    @SerializedName("pUserTyp")
    @Expose
    public String pUserTyp;
    @SerializedName("dAirline")
    @Expose
    public String dAirline;
    @SerializedName("dFlightNo")
    @Expose
    public String dFlightNo;
    @SerializedName("dDepTime")
    @Expose
    public String dDepTime;
    @SerializedName("dArrTime")
    @Expose
    public String dArrTime;
    @SerializedName("dFrom")
    @Expose
    public String dFrom;
    @SerializedName("dDest")
    @Expose
    public String dDest;
    @SerializedName("dClass")
    @Expose
    public String dClass;
    @SerializedName("dDesc")
    @Expose
    public String dDesc;
    @SerializedName("dBag")
    @Expose
    public Object dBag;
    @SerializedName("rFlightNo")
    @Expose
    public String rFlightNo;
    @SerializedName("rDepTime")
    @Expose
    public String rDepTime;
    @SerializedName("rArrTime")
    @Expose
    public String rArrTime;
    @SerializedName("rClass")
    @Expose
    public String rClass;
    @SerializedName("rDesc")
    @Expose
    public String rDesc;
    @SerializedName("rBag")
    @Expose
    public Object rBag;
    @SerializedName("Ad")
    @Expose
    public long ad;
    @SerializedName("Ch")
    @Expose
    public long ch;
    @SerializedName("Inf")
    @Expose
    public long inf;
    @SerializedName("TFare")
    @Expose
    public long tFare;
    @SerializedName("BFare")
    @Expose
    public long bFare;
    @SerializedName("Disc")
    @Expose
    public long disc;
    @SerializedName("NPay")
    @Expose
    public long nPay;
    @SerializedName("SearchID")
    @Expose
    public long searchID;
    @SerializedName("fAMYid1")
    @Expose
    public long fAMYid1;
    @SerializedName("fAMYid2")
    @Expose
    public long fAMYid2;
    @SerializedName("TRIP")
    @Expose
    public String tRIP;
    @SerializedName("fRoutType")
    @Expose
    public String fRoutType;
    @SerializedName("fSoft")
    @Expose
    public String fSoft;
    @SerializedName("PSNList")
    @Expose
    public List<PSNList> pSNList = new ArrayList<>();
    @SerializedName("PPImage")
    @Expose
    public Boolean pPImage;
    @SerializedName("VSImage")
    @Expose
    public Boolean vSImage;
    @SerializedName("isIssue")
    @Expose
    public long isIssue;
    @SerializedName("isReward")
    @Expose
    public long isReward;
    @SerializedName("RewardPoint")
    @Expose
    public long rewardPoint;
    @SerializedName("RewardPointTk")
    @Expose
    public long rewardPointTk;
    @SerializedName("Balance")
    @Expose
    public long balance;
    @SerializedName("NoRefund")
    @Expose
    public Object noRefund;
    @SerializedName("EBClass")
    @Expose
    public String eBClass;
    @SerializedName("stCusNam")
    @Expose
    public String stCusNam;
    @SerializedName("stCusPhn")
    @Expose
    public String stCusPhn;
    @SerializedName("stCusEmail")
    @Expose
    public String stCusEmail;
    @SerializedName("stCusID")
    @Expose
    public String stCusID;
    @SerializedName("stCusPayTyp")
    @Expose
    public String stCusPayTyp;
    @SerializedName("Message")
    @Expose
    public String message;

}
