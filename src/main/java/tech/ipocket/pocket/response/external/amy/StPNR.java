package tech.ipocket.pocket.response.external.amy;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StPNR {
    @SerializedName("AUsr")
    @Expose
    public Object aUsr;
    @SerializedName("APass")
    @Expose
    public Object aPass;
    @SerializedName("stAirline")
    @Expose
    public String stAirline;
    @SerializedName("stAirCode")
    @Expose
    public Object stAirCode;
    @SerializedName("stFlNo")
    @Expose
    public String stFlNo;
    @SerializedName("stRFlNo")
    @Expose
    public String stRFlNo;
    @SerializedName("stJDt")
    @Expose
    public String stJDt;
    @SerializedName("stReturn")
    @Expose
    public Boolean stReturn;
    @SerializedName("stRDt")
    @Expose
    public String stRDt;
    @SerializedName("stFrm")
    @Expose
    public String stFrm;
    @SerializedName("stDest")
    @Expose
    public String stDest;
    @SerializedName("stDTime")
    @Expose
    public String stDTime;
    @SerializedName("stATime")
    @Expose
    public String stATime;
    @SerializedName("stRDTime")
    @Expose
    public String stRDTime;
    @SerializedName("stRATime")
    @Expose
    public String stRATime;
    @SerializedName("stClass")
    @Expose
    public String stClass;
    @SerializedName("stRClass")
    @Expose
    public Object stRClass;
    @SerializedName("Promo")
    @Expose
    public Object promo;
    @SerializedName("stStp")
    @Expose
    public Object stStp;
    @SerializedName("stAd")
    @Expose
    public long stAd;
    @SerializedName("stCh")
    @Expose
    public long stCh;
    @SerializedName("stInf")
    @Expose
    public long stInf;
    @SerializedName("stTotPass")
    @Expose
    public long stTotPass;
    @SerializedName("stTotFare")
    @Expose
    public long stTotFare;
    @SerializedName("stTotBSFare")
    @Expose
    public long stTotBSFare;
    @SerializedName("stTotTXFare")
    @Expose
    public long stTotTXFare;
    @SerializedName("stStatus")
    @Expose
    public String stStatus;
    @SerializedName("lstPass")
    @Expose
    public List<LstPas> lstPass = new ArrayList<>();
    @SerializedName("OnlyBook")
    @Expose
    public Boolean onlyBook;
    @SerializedName("bkdPNR")
    @Expose
    public String bkdPNR;
    @SerializedName("usbPNR")
    @Expose
    public Object usbPNR;
    @SerializedName("bimanPNR")
    @Expose
    public Object bimanPNR;
    @SerializedName("stInternation")
    @Expose
    public Boolean stInternation;
    @SerializedName("stRoutTyp")
    @Expose
    public String stRoutTyp;
    @SerializedName("stPage")
    @Expose
    public String stPage;
    @SerializedName("stErr")
    @Expose
    public String stErr;
    @SerializedName("stUsr")
    @Expose
    public String stUsr;
    @SerializedName("stUsrTyp")
    @Expose
    public String stUsrTyp;
    @SerializedName("stPayMode")
    @Expose
    public Object stPayMode;
    @SerializedName("stCusID")
    @Expose
    public String stCusID;
    @SerializedName("stCusPhn")
    @Expose
    public String stCusPhn;
    @SerializedName("stCusNam")
    @Expose
    public String stCusNam;
    @SerializedName("stCusEmail")
    @Expose
    public String stCusEmail;
    @SerializedName("stCusAddr")
    @Expose
    public String stCusAddr;
    @SerializedName("tmpTotal1")
    @Expose
    public long tmpTotal1;
    @SerializedName("tmpTotal2")
    @Expose
    public long tmpTotal2;
    @SerializedName("stTkTaken")
    @Expose
    public long stTkTaken;
    @SerializedName("stCardCom")
    @Expose
    public long stCardCom;
    @SerializedName("stTrgtSg")
    @Expose
    public Object stTrgtSg;
    @SerializedName("stDesc")
    @Expose
    public Object stDesc;
    @SerializedName("stRDesc")
    @Expose
    public Object stRDesc;
    @SerializedName("stPNRsoft")
    @Expose
    public String stPNRsoft;
    @SerializedName("stReqIP")
    @Expose
    public Object stReqIP;
    @SerializedName("stInfStat")
    @Expose
    public Object stInfStat;
    @SerializedName("stPassPhn")
    @Expose
    public String stPassPhn;
    @SerializedName("stPassMail")
    @Expose
    public String stPassMail;
    @SerializedName("stIssuDt")
    @Expose
    public String stIssuDt;
    @SerializedName("stBkLimitDt")
    @Expose
    public String stBkLimitDt;
    @SerializedName("stPassFFP")
    @Expose
    public String stPassFFP;
    @SerializedName("UniqID")
    @Expose
    public Object uniqID;
    @SerializedName("stBaseCur")
    @Expose
    public String stBaseCur;
    @SerializedName("stBaseConv")
    @Expose
    public Float stBaseConv;
}
