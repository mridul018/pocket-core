package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.response.BaseResponseObject;

public class GroupFunctionMapResponse {
    public int groupFunctionMapId;

    public GroupFunctionMapResponse() {
    }

    public GroupFunctionMapResponse(int groupFunctionMapId) {
        this.groupFunctionMapId = groupFunctionMapId;
    }

    public int getGroupFunctionMapId() {
        return groupFunctionMapId;
    }

    public void setGroupFunctionMapId(int groupFunctionMapId) {
        this.groupFunctionMapId = groupFunctionMapId;
    }
}
