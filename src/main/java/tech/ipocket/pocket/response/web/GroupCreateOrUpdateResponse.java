package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.response.BaseResponseObject;

public class GroupCreateOrUpdateResponse extends BaseResponseObject {
    private int groupId;
    private String message;

    public GroupCreateOrUpdateResponse() {
    }

    public GroupCreateOrUpdateResponse(int groupId, String message) {
        this.groupId = groupId;
        this.message = message;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
