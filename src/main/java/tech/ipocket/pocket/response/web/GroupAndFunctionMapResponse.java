package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.entity.UmUserFunction;

import java.util.ArrayList;
import java.util.List;

public class GroupAndFunctionMapResponse {

    private String groupName;
    private String groupDescription;
    private String groupCode;
    private List<UserFunctionResponse> functions;

    public GroupAndFunctionMapResponse() {
        functions=new ArrayList<>();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }


    public List<UserFunctionResponse> getFunctions() {
        return functions;
    }

    public void setFunctions(List<UserFunctionResponse> functions) {
        this.functions = functions;
    }
}
