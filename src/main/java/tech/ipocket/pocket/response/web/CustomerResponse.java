package tech.ipocket.pocket.response.web;


import java.util.List;

public class CustomerResponse {

   private List<TsClientResponse> tsClientResponses;

    public List<TsClientResponse> getTsClientResponses() {
        return tsClientResponses;
    }

    public void setTsClientResponses(List<TsClientResponse> tsClientResponses) {
        this.tsClientResponses = tsClientResponses;
    }
}
