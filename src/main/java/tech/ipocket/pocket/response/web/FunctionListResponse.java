package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.entity.UmUserFunction;
import tech.ipocket.pocket.response.BaseResponseObject;

import java.util.List;

public class FunctionListResponse {
    public List<UmUserFunction> functions;

    public FunctionListResponse() {
    }

    public FunctionListResponse(List<UmUserFunction> functions) {
        this.functions = functions;
    }

    public List<UmUserFunction> getFunctions() {
        return functions;
    }

    public void setFunctions(List<UmUserFunction> functions) {
        this.functions = functions;
    }
}
