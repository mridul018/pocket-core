package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.entity.UmUserFunction;

import java.util.List;

public class UserFunctionResponse {
    private int id;
    private String functionCode;
    private String functionName;

    public UserFunctionResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
}
