package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.entity.UmGroupFunctionMapping;
import tech.ipocket.pocket.response.BaseResponseObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GroupFunctionMapListResponse {
    public ArrayList<UmGroupFunctionMapping> umGroupFunctionMappings;

    public GroupFunctionMapListResponse() {
    }

    public GroupFunctionMapListResponse(ArrayList<UmGroupFunctionMapping> umGroupFunctionMappings) {
        this.umGroupFunctionMappings = umGroupFunctionMappings;
    }

    public ArrayList<UmGroupFunctionMapping> getUmGroupFunctionMappings() {
        return umGroupFunctionMappings;
    }

    public void setUmGroupFunctionMappings(ArrayList<UmGroupFunctionMapping> umGroupFunctionMappings) {
        this.umGroupFunctionMappings = umGroupFunctionMappings;
    }
}
