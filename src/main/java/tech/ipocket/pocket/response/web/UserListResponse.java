package tech.ipocket.pocket.response.web;


import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

public class UserListResponse {
    private  String stateId;
    private int id;
    private String fullName;
    private String loginId;
    private String groupCode;
    private String groupName;
    private String accountStatus;
    private String picture;
    private BigDecimal walletBalance;
    private String presentAddress;
    private String permanentAddress;
    private String countryCode;
    private String stateName;
    private String createdDate;
    private String merchantName;
    private String applicantName;
    private String applicantsName;
    private String bankAccNo;
    private String bankName;
    private String branchName;
    private String primaryIdType;
    private String primaryIdNumber;
    private String tradeLicenseNo;
    private String nidNo;
    private String userParentName;

//    Summary, Merchant Name, Applicant’s Name, mob no/ account no, address, Bank A/c no, Bank Name, Branch Name,
//    Trade License No, EID Number, Country, State, Current Balance, Registration Date, Status , (active, inactive, pending, suspected/ dormant, suspend) ,
//    Group Name (Merchant/Customer/Partner Merchant and others).


    public UserListResponse() {
    }

//    select ui.id,ui.full_name,ui.login_id,ui.group_code,ui.account_status,ui.photo_id,\n" +
//            "ud.present_address,ud.permanent_address

    public UserListResponse(Object[] data) {
        try {
            this.id= (Integer) data[0];
            this.fullName= (String) data[1];
            this.loginId= (String) data[2];
            this.groupCode= ((String) data[3]).toString();
            this.accountStatus= ((Integer) data[4]).toString();
            this.picture= data[5]==null?"":((Integer)data[5]).toString();
            this.presentAddress= (String) data[6];
            this.permanentAddress= (String) data[7];
            this.stateId= (String) data[8];
            if(data[9]!=null){
                this.createdDate= ((Timestamp) data[9]).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(BigDecimal walletBalance) {
        this.walletBalance = walletBalance;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateName() {
        return stateName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }

    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    public void setTradeLicenseNo(String tradeLicenseNo) {
        this.tradeLicenseNo = tradeLicenseNo;
    }

    public String getTradeLicenseNo() {
        return tradeLicenseNo;
    }

    public void setNidNo(String nidNo) {
        this.nidNo = nidNo;
    }

    public String getNidNo() {
        return nidNo;
    }

    public String getApplicantsName() {
        return applicantsName;
    }

    public void setApplicantsName(String applicantsName) {
        this.applicantsName = applicantsName;
    }

    public String getUserParentName() {
        return userParentName;
    }

    public void setUserParentName(String userParentName) {
        this.userParentName = userParentName;
    }
}
