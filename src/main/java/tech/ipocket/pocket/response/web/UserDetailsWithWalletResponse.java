package tech.ipocket.pocket.response.web;

import tech.ipocket.pocket.response.BaseResponseObject;

import java.math.BigDecimal;

public class UserDetailsWithWalletResponse extends BaseResponseObject {
    private int id;
    private String fullName;
    private String email;
    private String groupCode;
    private int accountStatus;
    private String photoUrl;
    private BigDecimal amount;
    private String feeProfileCode;
    private String transactionProfileCode;

    public UserDetailsWithWalletResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getFeeProfileCode() {
        return feeProfileCode;
    }

    public void setFeeProfileCode(String feeProfileCode) {
        this.feeProfileCode = feeProfileCode;
    }

    public String getTransactionProfileCode() {
        return transactionProfileCode;
    }

    public void setTransactionProfileCode(String transactionProfileCode) {
        this.transactionProfileCode = transactionProfileCode;
    }
}
