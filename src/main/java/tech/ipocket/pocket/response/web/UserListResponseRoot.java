package tech.ipocket.pocket.response.web;

import java.util.ArrayList;
import java.util.List;

public class UserListResponseRoot {
    private String groupCode;
    private long totalUser;
    private List<UserListResponse> userList;

    public UserListResponseRoot() {
        userList=new ArrayList<>();
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public long getTotalUser() {
        return totalUser;
    }

    public void setTotalUser(long totalUser) {
        this.totalUser = totalUser;
    }

    public List<UserListResponse> getUserList() {
        return userList;
    }

    public void setUserList(List<UserListResponse> userList) {
        this.userList = userList;
    }
}
