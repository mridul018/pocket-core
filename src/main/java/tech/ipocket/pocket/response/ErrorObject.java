package tech.ipocket.pocket.response;

import tech.ipocket.pocket.utils.PocketErrorCode;

public class ErrorObject {
    private String errorMessage;
    private String errorCode;
    private String message;
    private String code;

    public ErrorObject() {
    }

    public ErrorObject(String errorCode, String localizedMessage) {
        this.errorCode = errorCode;
        this.errorMessage = localizedMessage;
        this.code = errorCode;
        this.message = localizedMessage;
    }

    public ErrorObject(PocketErrorCode pocketErrorCode) {
        this.setErrorCode("" + pocketErrorCode.getErrorCode());
        this.errorMessage = null;
        this.setCode("" + pocketErrorCode.getErrorCode());
        this.message = null;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
