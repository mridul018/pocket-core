package tech.ipocket.pocket.response.um;

import tech.ipocket.pocket.response.BaseResponseObject;

public class UserSettingsResponse {
    private Boolean isMobileNumberVerified;
    private Boolean isPrimaryIdVerified;

    public Boolean getMobileNumberVerified() {
        return isMobileNumberVerified;
    }

    public void setMobileNumberVerified(Boolean mobileNumberVerified) {
        isMobileNumberVerified = mobileNumberVerified;
    }

    public Boolean getPrimaryIdVerified() {
        return isPrimaryIdVerified;
    }

    public void setPrimaryIdVerified(Boolean primaryIdVerified) {
        isPrimaryIdVerified = primaryIdVerified;
    }
}
