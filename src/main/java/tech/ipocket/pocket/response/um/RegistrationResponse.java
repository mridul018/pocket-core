package tech.ipocket.pocket.response.um;

import java.util.Date;

public class RegistrationResponse {
    private String otpSecret;
    private int otpId;
    private int deviceId;
    private Date registrationDate;
    private String message;
    private int userId;

    public RegistrationResponse() {
    }

    public RegistrationResponse(String otpSecret, int otpId, int deviceId, Date registrationDate) {
        this.otpSecret = otpSecret;
        this.otpId = otpId;
        this.deviceId = deviceId;
        this.registrationDate = registrationDate;
    }

    public RegistrationResponse(String otpSecret, int otpId, int deviceId, Date registrationDate, String message) {
        this.otpSecret = otpSecret;
        this.otpId = otpId;
        this.deviceId = deviceId;
        this.registrationDate = registrationDate;
        this.message = message;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getOtpId() {
        return otpId;
    }

    public void setOtpId(int otpId) {
        this.otpId = otpId;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
