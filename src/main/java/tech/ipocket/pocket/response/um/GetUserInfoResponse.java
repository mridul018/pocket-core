package tech.ipocket.pocket.response.um;


public class GetUserInfoResponse {
private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
