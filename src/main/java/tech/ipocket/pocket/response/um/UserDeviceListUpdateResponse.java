package tech.ipocket.pocket.response.um;

public class UserDeviceListUpdateResponse {
    private boolean updateCompleted;

    public boolean isUpdateCompleted() {
        return updateCompleted;
    }

    public void setUpdateCompleted(boolean updateCompleted) {
        this.updateCompleted = updateCompleted;
    }
}
