package tech.ipocket.pocket.response.um;

public class ForgotPinStepOneResponse {
    private String otpSecret;

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }
}
