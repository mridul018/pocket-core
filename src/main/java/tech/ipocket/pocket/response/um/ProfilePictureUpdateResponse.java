package tech.ipocket.pocket.response.um;

public class ProfilePictureUpdateResponse {
    private String documentName;

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }
}
