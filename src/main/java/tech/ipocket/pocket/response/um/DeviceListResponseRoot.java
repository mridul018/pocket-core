package tech.ipocket.pocket.response.um;

import java.util.List;

public class DeviceListResponseRoot {
    private List<DeviceListItem> devices;

    public DeviceListResponseRoot() {
    }

    public List<DeviceListItem> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceListItem> devices) {
        this.devices = devices;
    }
}
