package tech.ipocket.pocket.response.um;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class UserListItem {

    private Integer id;
    private String fullName;
    private String mobileNumber;
    private String email;
    private Integer accountStatus;
    private String accountStatusString;
    private Integer photoId;
    private String nationality;
    private String countryCode;
    private String stateId;
    private String stateName;
    private String groupName;
    private String groupCode;
    private BigDecimal walletBalance;
    private String presentAddress;
    private String permanentAddress;
    private String createdDate;

    public UserListItem() {
    }

    public UserListItem(Object[] data) {
        try {
            this.id= (Integer) data[0];
            this.fullName= (String) data[1];
            this.mobileNumber= (String) data[2];
            this.email= (String) data[3];
            this.accountStatus= (Integer) data[4];
            this.photoId= (Integer) data[5];
            this.nationality= (String) data[6];
            this.countryCode= (String) data[7];
            this.stateId= (String) data[8];
            this.groupCode= (String) data[9];
            if(data[10]!=null){
                this.createdDate= ((Timestamp) data[10]).toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setWalletBalance(BigDecimal walletBalance) {
        this.walletBalance = walletBalance;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public String getAccountStatusString() {
        return accountStatusString;
    }

    public void setAccountStatusString(String accountStatusString) {
        this.accountStatusString = accountStatusString;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
