package tech.ipocket.pocket.response.um.passport;

import java.math.BigDecimal;

public class EmbPassportTransactionHistoryResponse {
    //TS_TRANSACTION
    private int transactionId;
    private String transactionToken;
    private String transactionSenderWallet;
    private String transactionReceiverWallet;
    private String transactionType;
    private String createdDate;

    //UM_EMB_PASSPORT_TRANSACTION_INFO
    private int id;
    private String senderName;
    private String senderFatherName;
    private String senderBdMobileNo;
    private String uniquePassportGeneratedId;
    private String mrpPassportNumber;
    private String emirateOrResidentialId;
    private String professionOrSkill;
    private String payMode;
    private String gender;
    private String dateOfBirth;
    private String currentPassportExpiryDate;
    private String passportType;
    private String passportSubType;
    private BigDecimal embassyPassportFee;
    private BigDecimal pocketServiceCharge;
    private BigDecimal totalPassportFeeAmount;

    // UM_EMB_PASSPORT_SUB_TYPE
    private Boolean generalDelivery;
    private Boolean emergencyDelivery;

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getTransactionSenderWallet() {
        return transactionSenderWallet;
    }

    public void setTransactionSenderWallet(String transactionSenderWallet) {
        this.transactionSenderWallet = transactionSenderWallet;
    }

    public String getTransactionReceiverWallet() {
        return transactionReceiverWallet;
    }

    public void setTransactionReceiverWallet(String transactionReceiverWallet) {
        this.transactionReceiverWallet = transactionReceiverWallet;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderFatherName() {
        return senderFatherName;
    }

    public void setSenderFatherName(String senderFatherName) {
        this.senderFatherName = senderFatherName;
    }

    public String getSenderBdMobileNo() {
        return senderBdMobileNo;
    }

    public void setSenderBdMobileNo(String senderBdMobileNo) {
        this.senderBdMobileNo = senderBdMobileNo;
    }

    public String getUniquePassportGeneratedId() {
        return uniquePassportGeneratedId;
    }

    public void setUniquePassportGeneratedId(String uniquePassportGeneratedId) {
        this.uniquePassportGeneratedId = uniquePassportGeneratedId;
    }

    public String getMrpPassportNumber() {
        return mrpPassportNumber;
    }

    public void setMrpPassportNumber(String mrpPassportNumber) {
        this.mrpPassportNumber = mrpPassportNumber;
    }

    public String getEmirateOrResidentialId() {
        return emirateOrResidentialId;
    }

    public void setEmirateOrResidentialId(String emirateOrResidentialId) {
        this.emirateOrResidentialId = emirateOrResidentialId;
    }

    public String getProfessionOrSkill() {
        return professionOrSkill;
    }

    public void setProfessionOrSkill(String professionOrSkill) {
        this.professionOrSkill = professionOrSkill;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCurrentPassportExpiryDate() {
        return currentPassportExpiryDate;
    }

    public void setCurrentPassportExpiryDate(String currentPassportExpiryDate) {
        this.currentPassportExpiryDate = currentPassportExpiryDate;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    public BigDecimal getEmbassyPassportFee() {
        return embassyPassportFee;
    }

    public void setEmbassyPassportFee(BigDecimal embassyPassportFee) {
        this.embassyPassportFee = embassyPassportFee;
    }

    public BigDecimal getPocketServiceCharge() {
        return pocketServiceCharge;
    }

    public void setPocketServiceCharge(BigDecimal pocketServiceCharge) {
        this.pocketServiceCharge = pocketServiceCharge;
    }

    public BigDecimal getTotalPassportFeeAmount() {
        return totalPassportFeeAmount;
    }

    public void setTotalPassportFeeAmount(BigDecimal totalPassportFeeAmount) {
        this.totalPassportFeeAmount = totalPassportFeeAmount;
    }

    public Boolean getGeneralDelivery() {
        return generalDelivery;
    }

    public void setGeneralDelivery(Boolean generalDelivery) {
        this.generalDelivery = generalDelivery;
    }

    public Boolean getEmergencyDelivery() {
        return emergencyDelivery;
    }

    public void setEmergencyDelivery(Boolean emergencyDelivery) {
        this.emergencyDelivery = emergencyDelivery;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
