package tech.ipocket.pocket.response.um;

public class DeviceListItem {
    private String hardwareSignature;
    private String deviceName;

    public String getHardwareSignature() {
        return hardwareSignature;
    }

    public void setHardwareSignature(String hardwareSignature) {
        this.hardwareSignature = hardwareSignature;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
