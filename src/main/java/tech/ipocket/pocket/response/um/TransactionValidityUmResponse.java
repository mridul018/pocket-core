package tech.ipocket.pocket.response.um;

import tech.ipocket.pocket.response.ErrorObject;

public class TransactionValidityUmResponse {
    private String status;
    private ErrorObject error;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ErrorObject getError() {
        return error;
    }

    public void setError(ErrorObject error) {
        this.error = error;
    }
}
