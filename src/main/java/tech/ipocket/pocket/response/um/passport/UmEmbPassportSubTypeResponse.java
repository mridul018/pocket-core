package tech.ipocket.pocket.response.um.passport;

import java.math.BigDecimal;
import java.util.Date;

public class UmEmbPassportSubTypeResponse {
    private int id;
    private String passportSubType;
    private String status;
    private String description;
    private Boolean generalDelivery;
    private Boolean emergencyDelivery;
    private BigDecimal embPassportFee;
    private BigDecimal pocketServiceCharge;
    private BigDecimal totalAmount;
    private Date createdDate;
    private UmEmbPassportTypeResponse umEmbPassportTypeResponse;  //fk

    private String embassyMerchantMobileNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getGeneralDelivery() {
        return generalDelivery;
    }

    public void setGeneralDelivery(Boolean generalDelivery) {
        this.generalDelivery = generalDelivery;
    }

    public Boolean getEmergencyDelivery() {
        return emergencyDelivery;
    }

    public void setEmergencyDelivery(Boolean emergencyDelivery) {
        this.emergencyDelivery = emergencyDelivery;
    }

    public BigDecimal getEmbPassportFee() {
        return embPassportFee;
    }

    public void setEmbPassportFee(BigDecimal embPassportFee) {
        this.embPassportFee = embPassportFee;
    }

    public BigDecimal getPocketServiceCharge() {
        return pocketServiceCharge;
    }

    public void setPocketServiceCharge(BigDecimal pocketServiceCharge) {
        this.pocketServiceCharge = pocketServiceCharge;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    public UmEmbPassportTypeResponse getUmEmbPassportTypeResponse() {
        return umEmbPassportTypeResponse;
    }

    public void setUmEmbPassportTypeResponse(UmEmbPassportTypeResponse umEmbPassportTypeResponse) {
        this.umEmbPassportTypeResponse = umEmbPassportTypeResponse;
    }

    public String getEmbassyMerchantMobileNo() {
        return embassyMerchantMobileNo;
    }

    public void setEmbassyMerchantMobileNo(String embassyMerchantMobileNo) {
        this.embassyMerchantMobileNo = embassyMerchantMobileNo;
    }
}
