package tech.ipocket.pocket.response.um.passport;

import com.fasterxml.jackson.annotation.JsonProperty;
import tech.ipocket.pocket.response.BaseResponseObject;

import java.math.BigDecimal;
import java.util.Date;

public class EmbassyPassportTransactionResponse extends BaseResponseObject {
    // Transaction
    private Double transferAmount;
    private Integer convertedAmount;
    private String transactionType;
    private String senderMobileNumber;
    private String receiverMobileNumber;
    private String otpText;
    private String otpSecret;
    private String pin;
    private String notes;

    private String operatorCode;
    private String countryCode;
    @JsonProperty
    private Boolean isPrePaid;
    private String skuCode;
    private String topUpMedium; //1= customer self, 2=Agent app

    private String topupProviderName;
    private String topupProviderCode;

    private String bankCode;
    private String branchSwiftCode;
    private String bankAccountNumber;


    // EmbassyPassportTransaction
    private String name;
    private String fatherName;
    private String bdMobileNo;
    private String uaeMobileNo;
    private String uniquePassportGeneratedId;
    private String mrpPassportNumber;
    private String previousPassportNumber;
    private String passportType;
    private String passportSubType;
    private BigDecimal embassyPassportFee;
    private BigDecimal pocketServiceCharge;
    private BigDecimal totalPassportFeeAmount;
    private String professionOrSkill;
    private String status;
    private Date createDate;
    private String emirateOrResidentialId;
    private String payMode;

    private String gender;
    private Date dateOfBirth;
    private Date currentPassportExpiryDate;

    private String tsTransactionId;
    private String tsTransactionToken;
    private String tsTransactionSender;
    private String tsTransactionReceiver;

    public Double getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(Double transferAmount) {
        this.transferAmount = transferAmount;
    }

    public Integer getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(Integer convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getReceiverMobileNumber() {
        return receiverMobileNumber;
    }

    public void setReceiverMobileNumber(String receiverMobileNumber) {
        this.receiverMobileNumber = receiverMobileNumber;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getPrePaid() {
        return isPrePaid;
    }

    public void setPrePaid(Boolean prePaid) {
        isPrePaid = prePaid;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getTopUpMedium() {
        return topUpMedium;
    }

    public void setTopUpMedium(String topUpMedium) {
        this.topUpMedium = topUpMedium;
    }

    public String getTopupProviderName() {
        return topupProviderName;
    }

    public void setTopupProviderName(String topupProviderName) {
        this.topupProviderName = topupProviderName;
    }

    public String getTopupProviderCode() {
        return topupProviderCode;
    }

    public void setTopupProviderCode(String topupProviderCode) {
        this.topupProviderCode = topupProviderCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public void setBranchSwiftCode(String branchSwiftCode) {
        this.branchSwiftCode = branchSwiftCode;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getBdMobileNo() {
        return bdMobileNo;
    }

    public void setBdMobileNo(String bdMobileNo) {
        this.bdMobileNo = bdMobileNo;
    }

    public String getUaeMobileNo() {
        return uaeMobileNo;
    }

    public void setUaeMobileNo(String uaeMobileNo) {
        this.uaeMobileNo = uaeMobileNo;
    }

    public String getUniquePassportGeneratedId() {
        return uniquePassportGeneratedId;
    }

    public void setUniquePassportGeneratedId(String uniquePassportGeneratedId) {
        this.uniquePassportGeneratedId = uniquePassportGeneratedId;
    }

    public String getMrpPassportNumber() {
        return mrpPassportNumber;
    }

    public void setMrpPassportNumber(String mrpPassportNumber) {
        this.mrpPassportNumber = mrpPassportNumber;
    }

    public String getPreviousPassportNumber() {
        return previousPassportNumber;
    }

    public void setPreviousPassportNumber(String previousPassportNumber) {
        this.previousPassportNumber = previousPassportNumber;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    public BigDecimal getEmbassyPassportFee() {
        return embassyPassportFee;
    }

    public void setEmbassyPassportFee(BigDecimal embassyPassportFee) {
        this.embassyPassportFee = embassyPassportFee;
    }

    public BigDecimal getPocketServiceCharge() {
        return pocketServiceCharge;
    }

    public void setPocketServiceCharge(BigDecimal pocketServiceCharge) {
        this.pocketServiceCharge = pocketServiceCharge;
    }

    public BigDecimal getTotalPassportFeeAmount() {
        return totalPassportFeeAmount;
    }

    public void setTotalPassportFeeAmount(BigDecimal totalPassportFeeAmount) {
        this.totalPassportFeeAmount = totalPassportFeeAmount;
    }

    public String getProfessionOrSkill() {
        return professionOrSkill;
    }

    public void setProfessionOrSkill(String professionOrSkill) {
        this.professionOrSkill = professionOrSkill;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getCurrentPassportExpiryDate() {
        return currentPassportExpiryDate;
    }

    public void setCurrentPassportExpiryDate(Date currentPassportExpiryDate) {
        this.currentPassportExpiryDate = currentPassportExpiryDate;
    }

    public String getTsTransactionId() {
        return tsTransactionId;
    }

    public void setTsTransactionId(String tsTransactionId) {
        this.tsTransactionId = tsTransactionId;
    }

    public String getTsTransactionToken() {
        return tsTransactionToken;
    }

    public void setTsTransactionToken(String tsTransactionToken) {
        this.tsTransactionToken = tsTransactionToken;
    }

    public String getTsTransactionSender() {
        return tsTransactionSender;
    }

    public void setTsTransactionSender(String tsTransactionSender) {
        this.tsTransactionSender = tsTransactionSender;
    }

    public String getTsTransactionReceiver() {
        return tsTransactionReceiver;
    }

    public void setTsTransactionReceiver(String tsTransactionReceiver) {
        this.tsTransactionReceiver = tsTransactionReceiver;
    }

    public String getEmirateOrResidentialId() {
        return emirateOrResidentialId;
    }

    public void setEmirateOrResidentialId(String emirateOrResidentialId) {
        this.emirateOrResidentialId = emirateOrResidentialId;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }
}
