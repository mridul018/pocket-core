package tech.ipocket.pocket.response.zenzero;

import java.math.BigDecimal;

public class ZenZeroPayUserBalanceResponse {
    private String requestId;
    private BigDecimal balance;
    private String requestDateTime;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }
}
