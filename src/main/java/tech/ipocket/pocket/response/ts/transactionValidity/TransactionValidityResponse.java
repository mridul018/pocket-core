package tech.ipocket.pocket.response.ts.transactionValidity;

import java.math.BigDecimal;

public class TransactionValidityResponse {

    private BigDecimal fee;
    private String feePayer;

    public TransactionValidityResponse() {
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }
}