package tech.ipocket.pocket.response.ts.paywell;

public class PaywellTopupData {
    private String msisdn;
    private int amount;
    private String con_type;
    private String operator;
    private String clientTrxId;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCon_type() {
        return con_type;
    }

    public void setCon_type(String con_type) {
        this.con_type = con_type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getClientTrxId() {
        return clientTrxId;
    }

    public void setClientTrxId(String clientTrxId) {
        this.clientTrxId = clientTrxId;
    }
}
