package tech.ipocket.pocket.response.ts.reporting;

import tech.ipocket.pocket.response.ts.SearchData;

import java.math.BigDecimal;
import java.util.List;

public class AccountStatementResponse {

    private String fullName;
    private String userType;
    private String accountOpeningDate;

    private SearchData searchData;
    private AccountStatementBalanceData balance;
    private List<AccountStatementListItemData> statementhistory;
    private long total;
    private BigDecimal totalDebit;
    private BigDecimal totalCredit;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAccountOpeningDate() {
        return accountOpeningDate;
    }

    public void setAccountOpeningDate(String accountOpeningDate) {
        this.accountOpeningDate = accountOpeningDate;
    }

    public void setBalance(AccountStatementBalanceData balance) {
        this.balance = balance;
    }

    public AccountStatementBalanceData getBalance() {
        return balance;
    }

    public void setStatementhistory(List<AccountStatementListItemData> statementhistory) {
        this.statementhistory = statementhistory;
    }

    public List<AccountStatementListItemData> getStatementhistory() {
        return statementhistory;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getTotal() {
        return total;
    }

    public void setTotalDebit(BigDecimal totalDebit) {
        this.totalDebit = totalDebit;
    }

    public BigDecimal getTotalDebit() {
        return totalDebit;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getTotalCredit() {
        return totalCredit;
    }

    public SearchData getSearchData() {
        return searchData;
    }

    public void setSearchData(SearchData searchData) {
        this.searchData = searchData;
    }
}
