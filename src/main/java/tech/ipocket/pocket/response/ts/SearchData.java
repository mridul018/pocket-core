package tech.ipocket.pocket.response.ts;


import java.math.BigDecimal;

public class SearchData {
    private String name;
    private String walletNo;
    private String startDate;
    private String endDate;
    private String searchedBy;
    private BigDecimal currentBalance;

    private int totalNoOfTransaction;
    private BigDecimal totalTransactionAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(String walletNo) {
        this.walletNo = walletNo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSearchedBy() {
        return searchedBy;
    }

    public void setSearchedBy(String searchedBy) {
        this.searchedBy = searchedBy;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public int getTotalNoOfTransaction() {
        return totalNoOfTransaction;
    }

    public void setTotalNoOfTransaction(int totalNoOfTransaction) {
        this.totalNoOfTransaction = totalNoOfTransaction;
    }

    public BigDecimal getTotalTransactionAmount() {
        return totalTransactionAmount;
    }

    public void setTotalTransactionAmount(BigDecimal totalTransactionAmount) {
        this.totalTransactionAmount = totalTransactionAmount;
    }
}
