package tech.ipocket.pocket.response.ts.reporting;

public class LimitListItem {

    private String serviceName;
    private String limitName;

    private int dailyMaxCount;
    private int currentDailyTransactionCount;

    private double dailyMaxAmount;
    private double currentDailyTransactionAmount;

    private int monthlyMaxCount;
    private int currentlyMonthlyTransactionCount;


    private double monthlyMaxAmount;
    private double currentMonthlyTransactionAmount;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getLimitName() {
        return limitName;
    }

    public void setLimitName(String limitName) {
        this.limitName = limitName;
    }

    public int getDailyMaxCount() {
        return dailyMaxCount;
    }

    public void setDailyMaxCount(int dailyMaxCount) {
        this.dailyMaxCount = dailyMaxCount;
    }

    public int getCurrentDailyTransactionCount() {
        return currentDailyTransactionCount;
    }

    public void setCurrentDailyTransactionCount(int currentDailyTransactionCount) {
        this.currentDailyTransactionCount = currentDailyTransactionCount;
    }

    public double getDailyMaxAmount() {
        return dailyMaxAmount;
    }

    public void setDailyMaxAmount(double dailyMaxAmount) {
        this.dailyMaxAmount = dailyMaxAmount;
    }

    public double getCurrentDailyTransactionAmount() {
        return currentDailyTransactionAmount;
    }

    public void setCurrentDailyTransactionAmount(double currentDailyTransactionAmount) {
        this.currentDailyTransactionAmount = currentDailyTransactionAmount;
    }

    public int getMonthlyMaxCount() {
        return monthlyMaxCount;
    }

    public void setMonthlyMaxCount(int monthlyMaxCount) {
        this.monthlyMaxCount = monthlyMaxCount;
    }

    public int getCurrentlyMonthlyTransactionCount() {
        return currentlyMonthlyTransactionCount;
    }

    public void setCurrentlyMonthlyTransactionCount(int currentlyMonthlyTransactionCount) {
        this.currentlyMonthlyTransactionCount = currentlyMonthlyTransactionCount;
    }

    public double getMonthlyMaxAmount() {
        return monthlyMaxAmount;
    }

    public void setMonthlyMaxAmount(double monthlyMaxAmount) {
        this.monthlyMaxAmount = monthlyMaxAmount;
    }

    public double getCurrentMonthlyTransactionAmount() {
        return currentMonthlyTransactionAmount;
    }

    public void setCurrentMonthlyTransactionAmount(double currentMonthlyTransactionAmount) {
        this.currentMonthlyTransactionAmount = currentMonthlyTransactionAmount;
    }
}
