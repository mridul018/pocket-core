package tech.ipocket.pocket.response.ts.reporting;

import java.math.BigDecimal;

public class DashboardAggregateBalanceResponse {
    private BigDecimal masterWalletBalance;
    private BigDecimal bankGLBalance;
    private BigDecimal customerBalance;
    private BigDecimal merchantBalance;
    private BigDecimal stakeholderGlBalance;
    private BigDecimal unResolvedGlbalance;
    private BigDecimal feePayableBalance;
    private BigDecimal vatBalance;
    private BigDecimal aitBalance;
    private BigDecimal aggregateBalance;
    private BigDecimal incomeBalance;
    private BigDecimal expenseBalance;
    private BigDecimal agentGlBalance;
    private BigDecimal srBalance;

    public void setMasterWalletBalance(BigDecimal masterWalletBalance) {
        this.masterWalletBalance = masterWalletBalance;
    }

    public BigDecimal getMasterWalletBalance() {
        return masterWalletBalance;
    }

    public void setBankGLBalance(BigDecimal bankGLBalance) {
        this.bankGLBalance = bankGLBalance;
    }

    public BigDecimal getBankGLBalance() {
        return bankGLBalance;
    }

    public void setCustomerBalance(BigDecimal customerBalance) {
        this.customerBalance = customerBalance;
    }

    public BigDecimal getCustomerBalance() {
        return customerBalance;
    }

    public void setMerchantBalance(BigDecimal merchantBalance) {
        this.merchantBalance = merchantBalance;
    }

    public BigDecimal getMerchantBalance() {
        return merchantBalance;
    }

    public void setStakeholderGlBalance(BigDecimal stakeholderGlBalance) {
        this.stakeholderGlBalance = stakeholderGlBalance;
    }

    public BigDecimal getStakeholderGlBalance() {
        return stakeholderGlBalance;
    }

    public void setUnResolvedGlbalance(BigDecimal unResolvedGlbalance) {
        this.unResolvedGlbalance = unResolvedGlbalance;
    }

    public BigDecimal getUnResolvedGlbalance() {
        return unResolvedGlbalance;
    }

    public void setFeePayableBalance(BigDecimal feePayableBalance) {
        this.feePayableBalance = feePayableBalance;
    }

    public BigDecimal getFeePayableBalance() {
        return feePayableBalance;
    }

    public void setVatBalance(BigDecimal vatBalance) {
        this.vatBalance = vatBalance;
    }

    public BigDecimal getVatBalance() {
        return vatBalance;
    }

    public void setAitBalance(BigDecimal aitBalance) {
        this.aitBalance = aitBalance;
    }

    public BigDecimal getAitBalance() {
        return aitBalance;
    }

    public void setAggregateBalance(BigDecimal aggregateBalance) {
        this.aggregateBalance = aggregateBalance;
    }

    public BigDecimal getAggregateBalance() {
        return aggregateBalance;
    }

    public BigDecimal getIncomeBalance() {
        return incomeBalance;
    }

    public void setIncomeBalance(BigDecimal incomeBalance) {
        this.incomeBalance = incomeBalance;
    }

    public BigDecimal getExpenseBalance() {
        return expenseBalance;
    }

    public void setExpenseBalance(BigDecimal expenseBalance) {
        this.expenseBalance = expenseBalance;
    }

    public void setAgentGlBalance(BigDecimal agentGlBalance) {
        this.agentGlBalance = agentGlBalance;
    }

    public BigDecimal getAgentGlBalance() {
        return agentGlBalance;
    }

    public void setSrBalance(BigDecimal srBalance) {
        this.srBalance = srBalance;
    }

    public BigDecimal getSrBalance() {
        return srBalance;
    }
}
