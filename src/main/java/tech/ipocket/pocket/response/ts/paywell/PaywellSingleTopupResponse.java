package tech.ipocket.pocket.response.ts.paywell;

public class PaywellSingleTopupResponse {
private String hotlineNumber;
private PaywellData data;

    public String getHotlineNumber() {
        return hotlineNumber;
    }

    public void setHotlineNumber(String hotlineNumber) {
        this.hotlineNumber = hotlineNumber;
    }

    public PaywellData getData() {
        return data;
    }

    public void setData(PaywellData data) {
        this.data = data;
    }
}
