package tech.ipocket.pocket.response.ts.reporting;

import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.entity.TsTransaction;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.utils.DateUtil;

import java.math.BigDecimal;

public class AccountStatementListItemData {
    private int id;
    private  String requestId;
    private String transaction_reference;
    private String transaction_type;
    private BigDecimal deposit_Amount;
    private BigDecimal withdraw_amount;
    private BigDecimal fee_amount;
    private String description;
    private String transaction_date;
    private String payerMobile;
    private String payeeMobile;
    private BigDecimal balance;
    private String feePayer;


    public AccountStatementListItemData(String userMobileNumber, TsTransaction transaction, Boolean isForGl,
                                        TsServiceRepository cfeServicesService) {

        this.id = transaction.getId();
        this.transaction_reference = "" + transaction.getToken();

        TsService cfeService = cfeServicesService.findFirstByServiceCode(transaction.getTransactionType());

        if (cfeService == null) {
            this.transaction_type = "Unknown";
        } else {
            this.transaction_type = cfeService.getDescription();

        }

        this.payerMobile = transaction.getLogicalSender();

        this.payeeMobile=transaction.getLogicalReceiver();

        this.description = transaction.getNotes();

        boolean isUserPayer = userMobileNumber.equals(transaction.getSenderWallet());

        feePayer=transaction.getFeePayer();

        if (!isUserPayer) {
//            this.other_entity = CommonTasks.getPayerByTransactionAndType(transaction, cfeService.getServiceName());
//            this.fee_amount= CommonTasks.getFeeByTxType(false,transaction.getFeePayer(),cfeService.getServiceName(),"",
//                    transaction.getFeeAmount());

            this.fee_amount=transaction.getFeeAmount();

            this.withdraw_amount = BigDecimal.ZERO;
            this.balance = transaction.getReceiverRunningBalance() == null ? BigDecimal.ZERO : transaction.getReceiverRunningBalance();
            this.deposit_Amount = transaction.getReceiverCreditAmount();

        } else {
//            this.other_entity = CommonTasks.getPayeeByTransactionAndTransactionTypeForTransactionQuery(transaction, cfeService.getServiceName());
//            this.fee_amount= CommonTasks.getFeeByTxType(true,transaction.getFeePayer(),cfeService.getServiceName(),"",
//                    transaction.getFeeAmount());

            this.fee_amount=transaction.getFeeAmount();


            this.deposit_Amount = BigDecimal.ZERO;

            this.withdraw_amount = transaction.getSenderDebitAmount();


            this.balance = transaction.getSenderRunningBalance() == null ? BigDecimal.ZERO : transaction.getSenderRunningBalance();
        }


        this.transaction_date = "" + DateUtil.formattedDate(transaction.getCreatedDate());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTransaction_reference() {
        return transaction_reference;
    }

    public void setTransaction_reference(String transaction_reference) {
        this.transaction_reference = transaction_reference;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public BigDecimal getDeposit_Amount() {
        return deposit_Amount;
    }

    public void setDeposit_Amount(BigDecimal deposit_Amount) {
        this.deposit_Amount = deposit_Amount;
    }

    public BigDecimal getWithdraw_amount() {
        return withdraw_amount;
    }

    public void setWithdraw_amount(BigDecimal withdraw_amount) {
        this.withdraw_amount = withdraw_amount;
    }

    public BigDecimal getFee_amount() {
        return fee_amount;
    }

    public void setFee_amount(BigDecimal fee_amount) {
        this.fee_amount = fee_amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getPayerMobile() {
        return payerMobile;
    }

    public void setPayerMobile(String payerMobile) {
        this.payerMobile = payerMobile;
    }

    public String getPayeeMobile() {
        return payeeMobile;
    }

    public void setPayeeMobile(String payeeMobile) {
        this.payeeMobile = payeeMobile;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }
}