package tech.ipocket.pocket.response.ts.feeProfile;

import tech.ipocket.pocket.request.ts.feeProfile.FeeStakeHolderItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FeeListItem {
    private String id;
    private String feeCode;
    private String feeName;
    private BigDecimal fixedFee;
    private BigDecimal percentageFee;
    private int isBoth;
    private String feePayer;
    private String appliedFeeMethod;
    private List<FeeStakeHolderItem> stakeHolders;

    private String serviceCode;
    private String serviceName;


    public FeeListItem() {
        stakeHolders = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public BigDecimal getFixedFee() {
        return fixedFee;
    }

    public void setFixedFee(BigDecimal fixedFee) {
        this.fixedFee = fixedFee;
    }

    public BigDecimal getPercentageFee() {
        return percentageFee;
    }

    public void setPercentageFee(BigDecimal percentageFee) {
        this.percentageFee = percentageFee;
    }

    public int getBoth() {
        return isBoth;
    }

    public void setBoth(int both) {
        isBoth = both;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public String getAppliedFeeMethod() {
        return appliedFeeMethod;
    }

    public void setAppliedFeeMethod(String appliedFeeMethod) {
        this.appliedFeeMethod = appliedFeeMethod;
    }

    public List<FeeStakeHolderItem> getStakeHolders() {
        return stakeHolders;
    }

    public void setStakeHolders(List<FeeStakeHolderItem> stakeHolders) {
        this.stakeHolders = stakeHolders;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
