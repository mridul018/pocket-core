package tech.ipocket.pocket.response.ts;

public class TsSessionObject {

    /*{
        "valid" : true,
            "name" : "Anisu",
            "mobileNumber" : "+971561234569",
            "groupCode" : "1001",
            "photoId" : "",
            "userId" : 62
    }*/

    private int userId;
    private boolean valid;
    private String name;
    private String mobileNumber;
    private String groupCode;
    private String photoId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }
}
