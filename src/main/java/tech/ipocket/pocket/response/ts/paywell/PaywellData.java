package tech.ipocket.pocket.response.ts.paywell;

public class PaywellData {
    private String status;
    private String message;
    private String trans_id;
    private PaywellTopupData topupData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public PaywellTopupData getTopupData() {
        return topupData;
    }

    public void setTopupData(PaywellTopupData topupData) {
        this.topupData = topupData;
    }
}
