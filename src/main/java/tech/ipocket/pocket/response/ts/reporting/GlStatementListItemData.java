package tech.ipocket.pocket.response.ts.reporting;

import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.utils.TsEnums;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class GlStatementListItemData {
    private Integer id;
    private String transaction_reference;
    private String transaction_type;
    private BigDecimal deposit_Amount;
    private BigDecimal withdraw_amount;
    private String description;
    private String transaction_date;
    private String other_entity;
    private String payerMobile;
    private String payeeMobile;
    private BigDecimal balance = BigDecimal.ZERO;

    public GlStatementListItemData(Object[] glTxItem, TsServiceRepository tsServiceRepository) {

        this.payeeMobile = (String) glTxItem[0];
        this.transaction_type = (String) glTxItem[1];
        Timestamp transactionDateTemp = (Timestamp) glTxItem[2];
        if(transactionDateTemp!=null){
            this.transaction_date = transactionDateTemp.toString();
        }else{
            this.transaction_date = "";
        }

        this.deposit_Amount = (BigDecimal) glTxItem[3];
        this.withdraw_amount = (BigDecimal) glTxItem[4];

        this.id = ((Integer) glTxItem[5]);


        if (this.deposit_Amount == null) {
            this.deposit_Amount = BigDecimal.ZERO;
        }

        if (this.withdraw_amount == null) {
            this.withdraw_amount = BigDecimal.ZERO;
        }

        TsService cfeService = tsServiceRepository.findFirstByServiceCode(this.transaction_type);

        if (cfeService == null) {
            this.transaction_type = "Unknown";
        } else {
            this.transaction_type = cfeService.getDescription();

            if(cfeService.getServiceCode().equalsIgnoreCase(TsEnums.Services.Intermediate.getServiceCode())){

                String sourceOfFund=(String) glTxItem[6];

                if(sourceOfFund!=null && sourceOfFund.equalsIgnoreCase("Card")){
                    this.transaction_type = "Add Money From Card";
                }
            }
        }
        this.transaction_reference = (String) glTxItem[6];
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransaction_reference() {
        return transaction_reference;
    }

    public void setTransaction_reference(String transaction_reference) {
        this.transaction_reference = transaction_reference;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public BigDecimal getDeposit_Amount() {
        return deposit_Amount;
    }

    public void setDeposit_Amount(BigDecimal deposit_Amount) {
        this.deposit_Amount = deposit_Amount;
    }

    public BigDecimal getWithdraw_amount() {
        return withdraw_amount;
    }

    public void setWithdraw_amount(BigDecimal withdraw_amount) {
        this.withdraw_amount = withdraw_amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getOther_entity() {
        return other_entity;
    }

    public void setOther_entity(String other_entity) {
        this.other_entity = other_entity;
    }

    public String getPayerMobile() {
        return payerMobile;
    }

    public void setPayerMobile(String payerMobile) {
        this.payerMobile = payerMobile;
    }

    public String getPayeeMobile() {
        return payeeMobile;
    }

    public void setPayeeMobile(String payeeMobile) {
        this.payeeMobile = payeeMobile;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
