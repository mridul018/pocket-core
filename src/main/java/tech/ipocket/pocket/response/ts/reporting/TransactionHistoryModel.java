package tech.ipocket.pocket.response.ts.reporting;

import java.math.BigDecimal;

public class TransactionHistoryModel {

    private String transactionId;
    private String transactionStatus;
    private String transactionType;
    private String transactionDate;
    private String disputeStatus;
    private BigDecimal amount;
    private BigDecimal feeAmount;
    private boolean is_debit;
    private String transactionToken;
    private String previousTransactionReference;
    private String payerWallet;
    private String payeeWallet;
    private String note;
    private boolean isDisputeable;
    private String reason;
    private String comments;
    private String requestOrigin;
    private String feePayer;
    private String refTransactionToken;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getDisputeStatus() {
        return disputeStatus;
    }

    public void setDisputeStatus(String disputeStatus) {
        this.disputeStatus = disputeStatus;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public boolean isIs_debit() {
        return is_debit;
    }

    public void setIs_debit(boolean is_debit) {
        this.is_debit = is_debit;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getPreviousTransactionReference() {
        return previousTransactionReference;
    }

    public void setPreviousTransactionReference(String previousTransactionReference) {
        this.previousTransactionReference = previousTransactionReference;
    }

    public String getPayerWallet() {
        return payerWallet;
    }

    public void setPayerWallet(String payerWallet) {
        this.payerWallet = payerWallet;
    }

    public String getPayeeWallet() {
        return payeeWallet;
    }

    public void setPayeeWallet(String payeeWallet) {
        this.payeeWallet = payeeWallet;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isDisputeable() {
        return isDisputeable;
    }

    public void setDisputeable(boolean disputeable) {
        isDisputeable = disputeable;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRequestOrigin() {
        return requestOrigin;
    }

    public void setRequestOrigin(String requestOrigin) {
        this.requestOrigin = requestOrigin;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public String getRefTransactionToken() {
        return refTransactionToken;
    }

    public void setRefTransactionToken(String refTransactionToken) {
        this.refTransactionToken = refTransactionToken;
    }
}
