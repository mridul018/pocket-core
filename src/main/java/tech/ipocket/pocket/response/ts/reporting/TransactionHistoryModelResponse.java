package tech.ipocket.pocket.response.ts.reporting;

import tech.ipocket.pocket.response.ts.SearchData;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryModelResponse {
    private SearchData searchData;
    private  List<TransactionHistoryModel> transactionHistoryModels;

    public TransactionHistoryModelResponse() {
        searchData=new SearchData();
        transactionHistoryModels=new ArrayList<>();
    }

    public SearchData getSearchData() {
        return searchData;
    }

    public void setSearchData(SearchData searchData) {
        this.searchData = searchData;
    }

    public List<TransactionHistoryModel> getTransactionHistoryModels() {
        return transactionHistoryModels;
    }

    public void setTransactionHistoryModels(List<TransactionHistoryModel> transactionHistoryModels) {
        this.transactionHistoryModels = transactionHistoryModels;
    }
}
