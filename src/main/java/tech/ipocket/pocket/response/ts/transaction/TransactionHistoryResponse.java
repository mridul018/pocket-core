package tech.ipocket.pocket.response.ts.transaction;

public class TransactionHistoryResponse {
    private int id;
    private String transactionType;
    private String createdDate;
    private double transactionAmount;
    private String feeCode;
    private String feePayer;
    private Double feeAmount;
    private Double senderDebitAmount;
    private Double receiverCreditAmount;
    private String receiverWallet;
    private String token;
    private String senderWallet;
    private Double senderRunningBalance;
    private Double receiverRunningBalance;
    private String transactionStatus;
    private boolean isDisputable;
    private String  logicalSender;
    private String logicalReceiver;
    private String prevTransactionRef;

    private String disputeStatus;
    private String disputeReason;
    private String disputeResolvedMessage;

    /*{
        "id": 21,
            "transactionType": "1005",
            "createdDate": "2019-05-18T07:51:27.000+0000",
            "transactionAmount": 100,
            "feeCode": null,
            "feePayer": "DR",
            "feeAmount": 0,
            "senderDebitAmount": 100,
            "receiverCreditAmount": 100,
            "eodStatus": "0",
            "receiverWallet": "+8801676173290",
            "token": "EC973D25A5F0",
            "senderWallet": "01837186095",
            "senderGl": null,
            "senderRunningBalance": 490,
            "receiverGl": null,
            "receiverRunningBalance": 100,
            "transactionStatus": "0",
            "isDisputable": false,
            "logicalSender": "01837186095",
            "logicalReceiver": "+8801676173290",
            "prevTransactionRef": null,
            "updatedDate": null
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public Double getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Double feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Double getSenderDebitAmount() {
        return senderDebitAmount;
    }

    public void setSenderDebitAmount(Double senderDebitAmount) {
        this.senderDebitAmount = senderDebitAmount;
    }

    public Double getReceiverCreditAmount() {
        return receiverCreditAmount;
    }

    public void setReceiverCreditAmount(Double receiverCreditAmount) {
        this.receiverCreditAmount = receiverCreditAmount;
    }

    public String getReceiverWallet() {
        return receiverWallet;
    }

    public void setReceiverWallet(String receiverWallet) {
        this.receiverWallet = receiverWallet;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSenderWallet() {
        return senderWallet;
    }

    public void setSenderWallet(String senderWallet) {
        this.senderWallet = senderWallet;
    }

    public Double getSenderRunningBalance() {
        return senderRunningBalance;
    }

    public void setSenderRunningBalance(Double senderRunningBalance) {
        this.senderRunningBalance = senderRunningBalance;
    }

    public Double getReceiverRunningBalance() {
        return receiverRunningBalance;
    }

    public void setReceiverRunningBalance(Double receiverRunningBalance) {
        this.receiverRunningBalance = receiverRunningBalance;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public boolean isDisputable() {
        return isDisputable;
    }

    public void setDisputable(boolean disputable) {
        isDisputable = disputable;
    }

    public String getLogicalSender() {
        return logicalSender;
    }

    public void setLogicalSender(String logicalSender) {
        this.logicalSender = logicalSender;
    }

    public String getLogicalReceiver() {
        return logicalReceiver;
    }

    public void setLogicalReceiver(String logicalReceiver) {
        this.logicalReceiver = logicalReceiver;
    }

    public String getPrevTransactionRef() {
        return prevTransactionRef;
    }

    public void setPrevTransactionRef(String prevTransactionRef) {
        this.prevTransactionRef = prevTransactionRef;
    }

    public String getDisputeStatus() {
        return disputeStatus;
    }

    public void setDisputeStatus(String disputeStatus) {
        this.disputeStatus = disputeStatus;
    }

    public String getDisputeReason() {
        return disputeReason;
    }

    public void setDisputeReason(String disputeReason) {
        this.disputeReason = disputeReason;
    }

    public String getDisputeResolvedMessage() {
        return disputeResolvedMessage;
    }

    public void setDisputeResolvedMessage(String disputeResolvedMessage) {
        this.disputeResolvedMessage = disputeResolvedMessage;
    }
}
