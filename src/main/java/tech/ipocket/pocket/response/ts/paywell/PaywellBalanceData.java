package tech.ipocket.pocket.response.ts.paywell;

public class PaywellBalanceData {
    private String balance;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
