package tech.ipocket.pocket.response.ts;

public class SuccessBoolResponse {
    private boolean result;

    public SuccessBoolResponse(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}