package tech.ipocket.pocket.response.ts.reporting;

import java.math.BigDecimal;

public class AccountStatementBalanceData {


    private BigDecimal startAmount=BigDecimal.ZERO;
    private BigDecimal previousHoldAmount=BigDecimal.ZERO;
    private BigDecimal endAmount=BigDecimal.ZERO;
    private BigDecimal currentBalance=BigDecimal.ZERO;

    public BigDecimal getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(BigDecimal startAmount) {
        this.startAmount = startAmount;
    }

    public BigDecimal getPreviousHoldAmount() {
        return previousHoldAmount;
    }

    public void setPreviousHoldAmount(BigDecimal previousHoldAmount) {
        this.previousHoldAmount = previousHoldAmount;
    }

    public BigDecimal getEndAmount() {
        return endAmount;
    }

    public void setEndAmount(BigDecimal endAmount) {
        this.endAmount = endAmount;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }
}
