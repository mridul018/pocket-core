package tech.ipocket.pocket.response.ts.reporting;

import java.math.BigDecimal;

public class TransactionDashboardListItem {
    private String transactionType;
    private long transactionCount;
    private BigDecimal transactionVolume;
    private long transactionSuccessCount;
    private long transactionFailourCount;
    private BigDecimal transactionReversalCount;
    private BigDecimal fee;
    private BigDecimal successTransactionVolume;

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionCount(long transactionCount) {
        this.transactionCount = transactionCount;
    }

    public long getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionVolume(BigDecimal transactionVolume) {
        this.transactionVolume = transactionVolume;
    }

    public BigDecimal getTransactionVolume() {
        return transactionVolume;
    }

    public void setTransactionSuccessCount(long transactionSuccessCount) {
        this.transactionSuccessCount = transactionSuccessCount;
    }

    public long getTransactionSuccessCount() {
        return transactionSuccessCount;
    }

    public void setTransactionFailourCount(long transactionFailourCount) {
        this.transactionFailourCount = transactionFailourCount;
    }

    public long getTransactionFailourCount() {
        return transactionFailourCount;
    }

    public void setTransactionReversalCount(BigDecimal transactionReversalCount) {
        this.transactionReversalCount = transactionReversalCount;
    }

    public BigDecimal getTransactionReversalCount() {
        return transactionReversalCount;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setSuccessTransactionVolume(BigDecimal successTransactionVolume) {
        this.successTransactionVolume = successTransactionVolume;
    }

    public BigDecimal getSuccessTransactionVolume() {
        return successTransactionVolume;
    }
}
