package tech.ipocket.pocket.response.ts;

public class RegistrationResponse {
    private Integer clientId;
    private Integer status;

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
