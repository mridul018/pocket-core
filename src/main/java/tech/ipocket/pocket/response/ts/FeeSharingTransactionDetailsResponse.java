package tech.ipocket.pocket.response.ts;

import java.math.BigDecimal;

public class FeeSharingTransactionDetailsResponse {
    private boolean isSuccess;
    private String failureReason;
    private BigDecimal senderRunningBalance;
    private BigDecimal receiverRunningBalance;
    private String payerName;
    private String payeeName;
    private String mainTransactionToken;
    private String newTransactionToken;
    private BigDecimal totalFeeAmount;
    private BigDecimal stakeholderFeeAmount;
    private String transactionType;
    private String stakeholderAccount;
    private String feeShareMethod;
    private String senderAccount;

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public BigDecimal getSenderRunningBalance() {
        return senderRunningBalance;
    }

    public void setSenderRunningBalance(BigDecimal senderRunningBalance) {
        this.senderRunningBalance = senderRunningBalance;
    }

    public BigDecimal getReceiverRunningBalance() {
        return receiverRunningBalance;
    }

    public void setReceiverRunningBalance(BigDecimal receiverRunningBalance) {
        this.receiverRunningBalance = receiverRunningBalance;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getMainTransactionToken() {
        return mainTransactionToken;
    }

    public void setMainTransactionToken(String mainTransactionToken) {
        this.mainTransactionToken = mainTransactionToken;
    }

    public String getNewTransactionToken() {
        return newTransactionToken;
    }

    public void setNewTransactionToken(String newTransactionToken) {
        this.newTransactionToken = newTransactionToken;
    }

    public BigDecimal getTotalFeeAmount() {
        return totalFeeAmount;
    }

    public void setTotalFeeAmount(BigDecimal totalFeeAmount) {
        this.totalFeeAmount = totalFeeAmount;
    }

    public BigDecimal getStakeholderFeeAmount() {
        return stakeholderFeeAmount;
    }

    public void setStakeholderFeeAmount(BigDecimal stakeholderFeeAmount) {
        this.stakeholderFeeAmount = stakeholderFeeAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getStakeholderAccount() {
        return stakeholderAccount;
    }

    public void setStakeholderAccount(String stakeholderAccount) {
        this.stakeholderAccount = stakeholderAccount;
    }

    public String getFeeShareMethod() {
        return feeShareMethod;
    }

    public void setFeeShareMethod(String feeShareMethod) {
        this.feeShareMethod = feeShareMethod;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }
}
