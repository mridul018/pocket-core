package tech.ipocket.pocket.response.ts.feeProfile;

import java.util.ArrayList;
import java.util.List;

public class FeeProfileFeeDetailsRoot {
    private String feeProfileName;
    private String feeProfileCode;

    private List<FeeListItem> feeListItems;

    public FeeProfileFeeDetailsRoot() {
        feeListItems=new ArrayList<>();
    }

    public String getFeeProfileName() {
        return feeProfileName;
    }

    public void setFeeProfileName(String feeProfileName) {
        this.feeProfileName = feeProfileName;
    }

    public String getFeeProfileCode() {
        return feeProfileCode;
    }

    public void setFeeProfileCode(String feeProfileCode) {
        this.feeProfileCode = feeProfileCode;
    }

    public List<FeeListItem> getFeeListItems() {
        return feeListItems;
    }

    public void setFeeListItems(List<FeeListItem> feeListItems) {
        this.feeListItems = feeListItems;
    }
}
