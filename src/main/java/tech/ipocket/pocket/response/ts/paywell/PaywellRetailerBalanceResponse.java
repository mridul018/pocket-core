package tech.ipocket.pocket.response.ts.paywell;

public class PaywellRetailerBalanceResponse {
    private String status;
    private String message;
    private String trans_id;
    private String retailerCode;
    private PaywellBalanceData balanceData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getRetailerCode() {
        return retailerCode;
    }

    public void setRetailerCode(String retailerCode) {
        this.retailerCode = retailerCode;
    }

    public PaywellBalanceData getBalanceData() {
        return balanceData;
    }

    public void setBalanceData(PaywellBalanceData balanceData) {
        this.balanceData = balanceData;
    }
}
