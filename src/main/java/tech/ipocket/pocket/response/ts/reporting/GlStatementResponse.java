package tech.ipocket.pocket.response.ts.reporting;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GlStatementResponse {
    private String glCode;
    private String glName;
    private int count;
    private BigDecimal totalDebit;
    private BigDecimal totalCredit;
    private AccountStatementBalanceData balance;
    private List<GlStatementListItemData> transactionList;

    public GlStatementResponse() {
        transactionList=new ArrayList<>();
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public String getGlName() {
        return glName;
    }

    public void setGlName(String glName) {
        this.glName = glName;
    }

    public void setBalance(AccountStatementBalanceData balance) {
        this.balance = balance;
    }

    public AccountStatementBalanceData getBalance() {
        return balance;
    }

    public void setTransactionList(List<GlStatementListItemData> transactionList) {
        this.transactionList = transactionList;
    }

    public List<GlStatementListItemData> getTransactionList() {
        return transactionList;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setTotalDebit(BigDecimal totalDebit) {
        this.totalDebit = totalDebit;
    }

    public BigDecimal getTotalDebit() {
        return totalDebit;
    }

    public void setTotalCredit(BigDecimal totalCredit) {
        this.totalCredit = totalCredit;
    }

    public BigDecimal getTotalCredit() {
        return totalCredit;
    }
}
