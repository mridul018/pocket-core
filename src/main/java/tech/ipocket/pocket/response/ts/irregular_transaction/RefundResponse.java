package tech.ipocket.pocket.response.ts.irregular_transaction;

import java.math.BigDecimal;

public class RefundResponse {
    private String senderWallet;
    private String receiverWallet;
    private String dateTime;
    private BigDecimal fee;

    public void setSenderWallet(String senderWallet) {
        this.senderWallet = senderWallet;
    }

    public String getSenderWallet() {
        return senderWallet;
    }

    public void setReceiverWallet(String receiverWallet) {
        this.receiverWallet = receiverWallet;
    }

    public String getReceiverWallet() {
        return receiverWallet;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getFee() {
        return fee;
    }
}
