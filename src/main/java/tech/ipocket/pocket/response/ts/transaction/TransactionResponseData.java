package tech.ipocket.pocket.response.ts.transaction;

import java.math.BigDecimal;

public class TransactionResponseData {
    private BigDecimal senderRunningBalance;
    private String txToken;
    private String dateTime;

    public void setSenderRunningBalance(BigDecimal senderRunningBalance) {
        this.senderRunningBalance = senderRunningBalance;
    }

    public BigDecimal getSenderRunningBalance() {
        return senderRunningBalance;
    }

    public void setTxToken(String txToken) {
        this.txToken = txToken;
    }

    public String getTxToken() {
        return txToken;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateTime() {
        return dateTime;
    }
}
