package tech.ipocket.pocket.response.ts.transaction;

import tech.ipocket.pocket.response.ts.SearchData;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryResponseRoot {

    private SearchData searchData;

    private List<TransactionHistoryResponse> history;

    public TransactionHistoryResponseRoot() {
        history=new ArrayList<>();
        searchData=new SearchData();
    }

    public List<TransactionHistoryResponse> getHistory() {
        return history;
    }

    public void setHistory(List<TransactionHistoryResponse> history) {
        this.history = history;
    }

    public SearchData getSearchData() {
        return searchData;
    }

    public void setSearchData(SearchData searchData) {
        this.searchData = searchData;
    }
}
