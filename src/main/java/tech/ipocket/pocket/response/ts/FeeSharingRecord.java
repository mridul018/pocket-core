package tech.ipocket.pocket.response.ts;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class FeeSharingRecord {

    private String mainTxToken;
    private String feeTxToken;
    private String txStatus;
    private String mainTxType;
    private BigDecimal stakeholderFeeAmount;
    private BigDecimal totalFeeAmount;
    private BigDecimal totalTxAmount;
    private String senderAccount;
    private String receiverAccount;
    private String requestId;
    private Timestamp shareDate;
    private String feeShareMethod;
    private String receiverGlName;
    private String senderGlName;
    private String feeCode;

    public String getMainTxToken() {
        return mainTxToken;
    }

    public void setMainTxToken(String mainTxToken) {
        this.mainTxToken = mainTxToken;
    }

    public String getFeeTxToken() {
        return feeTxToken;
    }

    public void setFeeTxToken(String feeTxToken) {
        this.feeTxToken = feeTxToken;
    }

    public String getTxStatus() {
        return txStatus;
    }

    public void setTxStatus(String txStatus) {
        this.txStatus = txStatus;
    }

    public String getMainTxType() {
        return mainTxType;
    }

    public void setMainTxType(String mainTxType) {
        this.mainTxType = mainTxType;
    }

    public BigDecimal getStakeholderFeeAmount() {
        return stakeholderFeeAmount;
    }

    public void setStakeholderFeeAmount(BigDecimal stakeholderFeeAmount) {
        this.stakeholderFeeAmount = stakeholderFeeAmount;
    }

    public BigDecimal getTotalFeeAmount() {
        return totalFeeAmount;
    }

    public void setTotalFeeAmount(BigDecimal totalFeeAmount) {
        this.totalFeeAmount = totalFeeAmount;
    }

    public BigDecimal getTotalTxAmount() {
        return totalTxAmount;
    }

    public void setTotalTxAmount(BigDecimal totalTxAmount) {
        this.totalTxAmount = totalTxAmount;
    }

    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Timestamp getShareDate() {
        return shareDate;
    }

    public void setShareDate(Timestamp shareDate) {
        this.shareDate = shareDate;
    }

    public String getFeeShareMethod() {
        return feeShareMethod;
    }

    public void setFeeShareMethod(String feeShareMethod) {
        this.feeShareMethod = feeShareMethod;
    }

    public void setReceiverGlName(String receiverGlName) {
        this.receiverGlName = receiverGlName;
    }

    public String getReceiverGlName() {
        return receiverGlName;
    }

    public void setSenderGlName(String senderGlName) {
        this.senderGlName = senderGlName;
    }

    public String getSenderGlName() {
        return senderGlName;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }
}
