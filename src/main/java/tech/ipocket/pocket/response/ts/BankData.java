package tech.ipocket.pocket.response.ts;

import tech.ipocket.pocket.entity.TsBanks;

import java.math.BigDecimal;

public class BankData {
    private int id;
    private String bankName;
    private String bankCode;
    private String routingNo;
    private String glCode;
    private String status;
    private BigDecimal balance;

    public BankData(TsBanks tsBanks) {
        this.bankCode=tsBanks.getBankCode();
        this.bankName=tsBanks.getBankName();
        this.id=tsBanks.getId();
        this.routingNo=tsBanks.getRoutingNo();
        this.glCode=tsBanks.getGlCode();
        this.status=tsBanks.getStatus();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getRoutingNo() {
        return routingNo;
    }

    public void setRoutingNo(String routingNo) {
        this.routingNo = routingNo;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
