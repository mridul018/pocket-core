package tech.ipocket.pocket.response.ts.paywell;

public class Token {
    private String security_token;
    private String token_exp_time;
    private String token_type;
    private String ack_timestamp;

    public String getSecurity_token() {
        return security_token;
    }

    public void setSecurity_token(String security_token) {
        this.security_token = security_token;
    }

    public String getToken_exp_time() {
        return token_exp_time;
    }

    public void setToken_exp_time(String token_exp_time) {
        this.token_exp_time = token_exp_time;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getAck_timestamp() {
        return ack_timestamp;
    }

    public void setAck_timestamp(String ack_timestamp) {
        this.ack_timestamp = ack_timestamp;
    }
}
