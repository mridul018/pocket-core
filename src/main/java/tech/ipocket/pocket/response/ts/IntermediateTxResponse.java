package tech.ipocket.pocket.response.ts;

import java.math.BigDecimal;

public class IntermediateTxResponse {
    private String txReference;
    private long txId;
    private BigDecimal feeAmount;
    private String feePayer;
    private String txCreateDate;


    public String getTxReference() {
        return txReference;
    }

    public void setTxReference(String txReference) {
        this.txReference = txReference;
    }

    public long getTxId() {
        return txId;
    }

    public void setTxId(long txId) {
        this.txId = txId;
    }

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public String getTxCreateDate() {
        return txCreateDate;
    }

    public void setTxCreateDate(String txCreateDate) {
        this.txCreateDate = txCreateDate;
    }
}
