package tech.ipocket.pocket.response.ts.reporting;

import java.util.ArrayList;
import java.util.List;

public class GetSummaryOfTransactionResponse {
    private String transactionCategory;
    private List<GetSummaryOfTransactionListItem> listData;

    public GetSummaryOfTransactionResponse() {
        listData=new ArrayList<>();
    }

    public String getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(String transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public List<GetSummaryOfTransactionListItem> getListData() {
        return listData;
    }

    public void setListData(List<GetSummaryOfTransactionListItem> listData) {
        this.listData = listData;
    }
}
