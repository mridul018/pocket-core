package tech.ipocket.pocket.response.integration.hss;

public class UserTokenResponse {
    private String status;
    private String message;
    private String security_token;
    private String token_exp_time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSecurity_token() {
        return security_token;
    }

    public void setSecurity_token(String security_token) {
        this.security_token = security_token;
    }

    public String getToken_exp_time() {
        return token_exp_time;
    }

    public void setToken_exp_time(String token_exp_time) {
        this.token_exp_time = token_exp_time;
    }
}
