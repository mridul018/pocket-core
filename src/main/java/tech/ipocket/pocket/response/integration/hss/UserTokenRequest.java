package tech.ipocket.pocket.response.integration.hss;

import tech.ipocket.pocket.request.integration.hss.HssBaseRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserTokenRequest extends HssBaseRequest {
    private String userId;
    private String password;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (userId == null && userId.equalsIgnoreCase("string") && userId.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_CLIENT_USER_ID);
            return;
        }
        if (password == null && password.equalsIgnoreCase("string") && password.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_CLIENT_USER_PASSWORD);
            return;
        }

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
