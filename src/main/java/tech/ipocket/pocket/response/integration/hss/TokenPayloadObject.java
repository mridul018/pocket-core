package tech.ipocket.pocket.response.integration.hss;

import com.google.gson.annotations.SerializedName;

public class TokenPayloadObject {
    @SerializedName("sub")
    private String sub;
    @SerializedName("password")
    private String password;
    @SerializedName("exp")
    private String exp;
    @SerializedName("userId")
    private String userId;

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
