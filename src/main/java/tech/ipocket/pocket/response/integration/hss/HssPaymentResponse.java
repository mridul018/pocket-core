package tech.ipocket.pocket.response.integration.hss;

import java.math.BigDecimal;

public class HssPaymentResponse {
    private String tokenNo;
    private String requestId;
    private String requestDateTime;
    private String transactionStatusCode;
    private String transactionRefId;

    private String senderWalletNumber;
    private Double transactionAmount;
    private String userNotes;

    private String hssUserId;
    private String hssProductTypeName;
    private BigDecimal senderRunningBalance;


    public String getTokenNo() {
        return tokenNo;
    }

    public void setTokenNo(String tokenNo) {
        this.tokenNo = tokenNo;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }

    public void setTransactionStatusCode(String transactionStatusCode) {
        this.transactionStatusCode = transactionStatusCode;
    }


    public String getTransactionRefId() {
        return transactionRefId;
    }

    public void setTransactionRefId(String transactionRefId) {
        this.transactionRefId = transactionRefId;
    }

    public String getSenderWalletNumber() {
        return senderWalletNumber;
    }

    public void setSenderWalletNumber(String senderWalletNumber) {
        this.senderWalletNumber = senderWalletNumber;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getUserNotes() {
        return userNotes;
    }

    public void setUserNotes(String userNotes) {
        this.userNotes = userNotes;
    }

    public String getHssUserId() {
        return hssUserId;
    }

    public void setHssUserId(String hssUserId) {
        this.hssUserId = hssUserId;
    }

    public String getHssProductTypeName() {
        return hssProductTypeName;
    }

    public void setHssProductTypeName(String hssProductTypeName) {
        this.hssProductTypeName = hssProductTypeName;
    }

    public BigDecimal getSenderRunningBalance() {
        return senderRunningBalance;
    }

    public void setSenderRunningBalance(BigDecimal senderRunningBalance) {
        this.senderRunningBalance = senderRunningBalance;
    }
}
