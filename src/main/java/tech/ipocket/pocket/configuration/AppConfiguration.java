package tech.ipocket.pocket.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Value("${external.config}")
    private boolean externalEnable;

    @Value("${external.url}")
    private String externalUrl;

    @Value("${TS_REVERSAL}")
    private String reversalUrl;

    public boolean isExternalEnable() {
        return externalEnable;
    }

    public void setExternalEnable(boolean externalEnable) {
        this.externalEnable = externalEnable;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public String getReversalUrl() {
        return reversalUrl;
    }

    public void setReversalUrl(String reversalUrl) {
        this.reversalUrl = reversalUrl;
    }
}
