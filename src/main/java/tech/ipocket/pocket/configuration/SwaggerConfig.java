package tech.ipocket.pocket.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .tags(new Tag("app", "For web and mobile both"),
                        new Tag("external", "For external services like security, Bank, sms, email, and notifications"),
                        new Tag("mobile", "For mobile apps"),
                        new Tag("web", "For web apps"),
                        new Tag("upay", "Api provide to Upay from pocket server"),
                        new Tag("hello-super-star", "Api provide to HSS from pocket server"),
                        new Tag("paynet", "Api provide to Paynet from pocket server"),
                        new Tag("easypay", "Api provide to EasyPay from pocket server"),
                        new Tag("zenzero", "Api provide to ZenZero from pocket server")
                        )
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}