package tech.ipocket.pocket.configuration;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.entity.UmExternalActivityLog;
import tech.ipocket.pocket.repository.um.UmExternalActivityLogRepository;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.utils.*;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class TaskSchedulingConfig {

    private static final String TAG = "TaskSchedulingConfig";

    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    private PayWellConfig payWellConfig;

    @Autowired
    private UmExternalActivityLogRepository externalActivityLogRepository;

    @Autowired
    private CommonService commonService;

    LogWriterUtility logWriterUtility = new LogWriterUtility(TaskSchedulingConfig.class);

    @Scheduled(cron = "0 0 */3 * * *")
    public void doCheckPendingTaskOfBillPay(){
        logWriterUtility.info(TAG,"Check pending task from external activity log data");
        List<UmExternalActivityLog> listOfExternalLog =
                externalActivityLogRepository.findByTypeAndStatus(ExternalType.POLLIBUDDIT_BILLPAY, 10);
        if(listOfExternalLog == null){
            logWriterUtility.info(TAG, "No pending task");
            return;
        }

        if(listOfExternalLog.size()<=0){
            logWriterUtility.info(TAG, "No pending task");
            return;
        }


        logWriterUtility.info(TAG, "Find some pending task");
        logWriterUtility.info(TAG, "Total number of pending task is : "+listOfExternalLog.size());

        listOfExternalLog.forEach(data -> {
            logWriterUtility.info(TAG, "Check the current status of bill pay of the following id: "+data.getId());
            BaseResponseObject responseObject = findCurrentStatus(data);
            logWriterUtility.info(TAG, "Response from bill status: "+ new Gson().toJson(responseObject));
            if(responseObject.getError() == null){
                logWriterUtility.info(TAG,"Bill Payed status get from scheduler");
                logWriterUtility.info(TAG, "Change status into log table");
                changeStatusOfExternalLog(data,responseObject);
            }else{
                logWriterUtility.info(TAG, "Bill not paid status get from scheduler. Status is: "+responseObject.getError().getErrorCode()+ " and Message is: "+responseObject.getError().getErrorMessage());
                logWriterUtility.info(TAG, "call reversal API");
                logWriterUtility.info(TAG, "Check the time from pending to now");

                long hourDifference = TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis() - data.getDatetime().getTime());
                if(hourDifference>24){
                    logWriterUtility.info(TAG, "Do Reversal");
                    doReversal(data);
                    logWriterUtility.info(TAG, "Reversal process done from Scheduler into pending bill pay");
                }

            }

        });
    }

    private void changeStatusOfExternalLog(UmExternalActivityLog data, BaseResponseObject responseObject) {

        data.setStatus(Integer.parseInt(PocketConstants.OK));
        data.setMessage("Success");

        commonService.addExternalActivityPostLog(data.getId(),
                data.getType(),data.getCountry(),data.getSender(),data.getTopupReceiver(),
                data.getOperator(),data.getConnection(),data.getBillPayCustomerName(),
                data.getBillPayReceiver(),data.getBillNo(),data.getAccountNo(),data.getAmount(),
                data.getStatus(),data.getMessage(),data.getDatetime(),data.getRequestUrl(),
                data.getRequestDirectionIn(),data.getRequestDirectionOut(),data.getCommunicationMedium(),
                data.getRefId());
    }

    private BaseResponseObject findCurrentStatus(UmExternalActivityLog log){
        ExternalCall externalCall = new ExternalCall(appConfiguration.getExternalUrl());
        return externalCall.sendPayWellPollibudditBill(
                payWellConfig.getClientId(),
                payWellConfig.getClientPassword(),
                log.getBillNo(),
                log.getAmount().toBigInteger(),
                log.getRefId(),
                ExternalType.POLLIBUDDIT_BILLPAY_CONFIRMATION,
                log.getAccountNo(),
                log.getBillPayReceiver(),
                log.getSender(),
                log.getBillPayCustomerName()
        );

    }

    private void doReversal(UmExternalActivityLog data){
        try {
            ReversalRequest reversalRequest = new ReversalRequest();
            reversalRequest.setReason(data.getMessage());
            reversalRequest.setRequestId(null);
            reversalRequest.setSource(null);
            reversalRequest.setTransactionToken(data.getRefId());

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> exchange = restTemplate.exchange(appConfiguration.getReversalUrl(), HttpMethod.POST,
                    new HttpEntity<>(reversalRequest), String.class);
            if(exchange.getStatusCode() == HttpStatus.OK){
                BaseResponseObject responseObject = new Gson().fromJson(exchange.getBody(), BaseResponseObject.class);
                if(responseObject.getError()==null) {
                    logWriterUtility.info(TAG, "Reversal complete");
                    return;
                }
                logWriterUtility.info(TAG, "Something error to doing reversal from scheduler. " +
                        "Status: "+responseObject.getError().getErrorCode()+ " and Message: "+
                        responseObject.getError().getErrorMessage());
            }
        }catch (Exception ex){
            logWriterUtility.error(TAG, ex.getLocalizedMessage());
        }
    }
}
