package tech.ipocket.pocket.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaynetConfig {
    @Value("${paynet.posid}")
    private int posId;

    @Value("${paynet.postoken}")
    private String posToken;

    @Value("${paynet.baseurl}")
    private String baseUrl;

    @Value("${paynet.paynetservices}")
    private String paynetService;

    @Value("${paynet.paynetprovider}")
    private String paynetProvider;

    @Value("${paynet.paynetbalance}")
    private String paynetBalance;

    @Value("${paynet.paynetinfo}")
    private String paynetInfo;

    @Value("${paynet.paynettransaction}")
    private String paynetTransaction;

    @Value("${paynet.paynettransaction.confirm}")
    private String paynetTransactionConfirm;

    @Value("${paynet.paynettransaction.find}")
    private String paynetTransactionFind;

    @Value("${paynet.paynetcustomeraccount}")
    private String paynetCustomerAccount;


    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPaynetService() {
        return paynetService;
    }

    public void setPaynetService(String paynetService) {
        this.paynetService = paynetService;
    }

    public String getPaynetProvider() {
        return paynetProvider;
    }

    public void setPaynetProvider(String paynetProvider) {
        this.paynetProvider = paynetProvider;
    }

    public String getPaynetBalance() {
        return paynetBalance;
    }

    public void setPaynetBalance(String paynetBalance) {
        this.paynetBalance = paynetBalance;
    }

    public String getPaynetInfo() {
        return paynetInfo;
    }

    public void setPaynetInfo(String paynetInfo) {
        this.paynetInfo = paynetInfo;
    }

    public String getPaynetTransaction() {
        return paynetTransaction;
    }

    public void setPaynetTransaction(String paynetTransaction) {
        this.paynetTransaction = paynetTransaction;
    }

    public String getPaynetTransactionConfirm() {
        return paynetTransactionConfirm;
    }

    public void setPaynetTransactionConfirm(String paynetTransactionConfirm) {
        this.paynetTransactionConfirm = paynetTransactionConfirm;
    }

    public String getPaynetTransactionFind() {
        return paynetTransactionFind;
    }

    public void setPaynetTransactionFind(String paynetTransactionFind) {
        this.paynetTransactionFind = paynetTransactionFind;
    }

    public int getPosId() {
        return posId;
    }

    public void setPosId(int posId) {
        this.posId = posId;
    }

    public String getPosToken() {
        return posToken;
    }

    public void setPosToken(String posToken) {
        this.posToken = posToken;
    }

    public String getPaynetCustomerAccount() {
        return paynetCustomerAccount;
    }

    public void setPaynetCustomerAccount(String paynetCustomerAccount) {
        this.paynetCustomerAccount = paynetCustomerAccount;
    }
}
