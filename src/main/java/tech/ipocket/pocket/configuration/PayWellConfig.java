package tech.ipocket.pocket.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PayWellConfig {

    @Value("${paywell.client.id}")
    private String clientId;
    @Value("${paywell.client.password}")
    private String clientPassword;
    @Value("${paywell.api.username}")
    private String apiUserName;
    @Value("${paywell.api.password}")
    private String apiPassword;
    @Value("${paywell.api.key}")
    private String apiKey;
    @Value("${paywell.encryption.key}")
    private String encryptionkey;
    @Value("${paywell.cust-reg}")
    private String customerRegistration;
    @Value("${paywell.bill-enq-data}")
    private String billEnquiryData;
    @Value("${paywell.cust-reg-enq}")
    private String customerRegEnquiry;
    @Value("${paywell.auth-url}")
    private String authentication;
    @Value("${paywell.bill-pay}")
    private String billPay;

    @Value("${paywell.getToken}")
    private String getToken;
    @Value("${paywell.retailerBalance}")
    private String retailerBalance;
    @Value("${paywell.singleTopup}")
    private String singleTopup;
    @Value("${paywell.mobileRechargeEnquiry}")
    private String mobileRechargeEnquiry;
    @Value("${paywell.mobileRechargeEnquiryThroughtrxId}")
    private String mobileRechargeEnquiryThroughtrxId;

    @Value("${paywell.billEnquiry}")
    private String billEnquiry;
    @Value("${paywell.billPayment}")
    private String billPayment;
    @Value("${paywell.pollyBillEnquiryData}")
    private String pollyBillEnquiryData;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    public String getApiUserName() {
        return apiUserName;
    }

    public void setApiUserName(String apiUserName) {
        this.apiUserName = apiUserName;
    }

    public String getApiPassword() {
        return apiPassword;
    }

    public void setApiPassword(String apiPassword) {
        this.apiPassword = apiPassword;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEncryptionkey() {
        return encryptionkey;
    }

    public void setEncryptionkey(String encryptionkey) {
        this.encryptionkey = encryptionkey;
    }

    public String getCustomerRegistration() {
        return customerRegistration;
    }

    public void setCustomerRegistration(String customerRegistration) {
        this.customerRegistration = customerRegistration;
    }

    public String getBillEnquiryData() {
        return billEnquiryData;
    }

    public void setBillEnquiryData(String billEnquiryData) {
        this.billEnquiryData = billEnquiryData;
    }

    public String getCustomerRegEnquiry() {
        return customerRegEnquiry;
    }

    public void setCustomerRegEnquiry(String customerRegEnquiry) {
        this.customerRegEnquiry = customerRegEnquiry;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getBillPay() {
        return billPay;
    }

    public void setBillPay(String billPay) {
        this.billPay = billPay;
    }

    public String getGetToken() {
        return getToken;
    }

    public void setGetToken(String getToken) {
        this.getToken = getToken;
    }

    public String getRetailerBalance() {
        return retailerBalance;
    }

    public void setRetailerBalance(String retailerBalance) {
        this.retailerBalance = retailerBalance;
    }

    public String getSingleTopup() {
        return singleTopup;
    }

    public void setSingleTopup(String singleTopup) {
        this.singleTopup = singleTopup;
    }

    public String getMobileRechargeEnquiry() {
        return mobileRechargeEnquiry;
    }

    public void setMobileRechargeEnquiry(String mobileRechargeEnquiry) {
        this.mobileRechargeEnquiry = mobileRechargeEnquiry;
    }

    public String getMobileRechargeEnquiryThroughtrxId() {
        return mobileRechargeEnquiryThroughtrxId;
    }

    public void setMobileRechargeEnquiryThroughtrxId(String mobileRechargeEnquiryThroughtrxId) {
        this.mobileRechargeEnquiryThroughtrxId = mobileRechargeEnquiryThroughtrxId;
    }

    public String getBillEnquiry() {
        return billEnquiry;
    }

    public void setBillEnquiry(String billEnquiry) {
        this.billEnquiry = billEnquiry;
    }

    public String getBillPayment() {
        return billPayment;
    }

    public void setBillPayment(String billPayment) {
        this.billPayment = billPayment;
    }

    public String getPollyBillEnquiryData() {
        return pollyBillEnquiryData;
    }

    public void setPollyBillEnquiryData(String pollyBillEnquiryData) {
        this.pollyBillEnquiryData = pollyBillEnquiryData;
    }
}
