/**
 * SendSmsLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.sms;

public class SendSmsLocator extends org.apache.axis.client.Service implements tech.ipocket.pocket.wsdl.sms.SendSms {

    public SendSmsLocator() {
    }


    public SendSmsLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SendSmsLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SendSmsSoap
    private java.lang.String SendSmsSoap_address = "https://api2.onnorokomsms.com/sendsms.asmx";

    public java.lang.String getSendSmsSoapAddress() {
        return SendSmsSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SendSmsSoapWSDDServiceName = "SendSmsSoap";

    public java.lang.String getSendSmsSoapWSDDServiceName() {
        return SendSmsSoapWSDDServiceName;
    }

    public void setSendSmsSoapWSDDServiceName(java.lang.String name) {
        SendSmsSoapWSDDServiceName = name;
    }

    public tech.ipocket.pocket.wsdl.sms.SendSmsSoap_PortType getSendSmsSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SendSmsSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSendSmsSoap(endpoint);
    }

    public tech.ipocket.pocket.wsdl.sms.SendSmsSoap_PortType getSendSmsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            tech.ipocket.pocket.wsdl.sms.SendSmsSoap_BindingStub _stub = new tech.ipocket.pocket.wsdl.sms.SendSmsSoap_BindingStub(portAddress, this);
            _stub.setPortName(getSendSmsSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSendSmsSoapEndpointAddress(java.lang.String address) {
        SendSmsSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (tech.ipocket.pocket.wsdl.sms.SendSmsSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                tech.ipocket.pocket.wsdl.sms.SendSmsSoap_BindingStub _stub = new tech.ipocket.pocket.wsdl.sms.SendSmsSoap_BindingStub(new java.net.URL(SendSmsSoap_address), this);
                _stub.setPortName(getSendSmsSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SendSmsSoap".equals(inputPortName)) {
            return getSendSmsSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "SendSms");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "SendSmsSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SendSmsSoap".equals(portName)) {
            setSendSmsSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
