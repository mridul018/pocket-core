/**
 * SendSms.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.sms;

public interface SendSms extends javax.xml.rpc.Service {
    public java.lang.String getSendSmsSoapAddress();

    public tech.ipocket.pocket.wsdl.sms.SendSmsSoap_PortType getSendSmsSoap() throws javax.xml.rpc.ServiceException;

    public tech.ipocket.pocket.wsdl.sms.SendSmsSoap_PortType getSendSmsSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
