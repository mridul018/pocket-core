/**
 * SendSmsSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.sms;

public interface SendSmsSoap_PortType extends java.rmi.Remote {
    public java.lang.String oneToOneBulk(tech.ipocket.pocket.wsdl.sms.MessageHeader messageHeader, tech.ipocket.pocket.wsdl.sms.WsSms[] wsSmses) throws java.rmi.RemoteException;
    public java.lang.String oneToOne(java.lang.String userName, java.lang.String userPassword, java.lang.String mobileNumber, java.lang.String smsText, java.lang.String type, java.lang.String maskName, java.lang.String campaignName) throws java.rmi.RemoteException;
    public java.lang.String oneToMany(java.lang.String userName, java.lang.String userPassword, java.lang.String messageText, java.lang.String numberList, java.lang.String smsType, java.lang.String maskName, java.lang.String campaignName) throws java.rmi.RemoteException;
    public java.lang.String deliveryStatus(java.lang.String userName, java.lang.String userPassword, java.lang.String responseId) throws java.rmi.RemoteException;
    public java.lang.String getBalance(java.lang.String userName, java.lang.String userPassword) throws java.rmi.RemoteException;
    public java.lang.String getMask(java.lang.String userName, java.lang.String userPassword) throws java.rmi.RemoteException;
    public java.lang.String numberSms(java.lang.String apiKey, java.lang.String messageText, java.lang.String numberList, java.lang.String smsType, java.lang.String maskName, java.lang.String campaignName) throws java.rmi.RemoteException;
    public java.lang.String listSms(java.lang.String apiKey, tech.ipocket.pocket.wsdl.sms.WsSms[] wsSmses, java.lang.String maskName, java.lang.String campaignName) throws java.rmi.RemoteException;
    public java.lang.String SMSDeliveryStatus(java.lang.String apiKey, java.lang.String responseId) throws java.rmi.RemoteException;
    public java.lang.String getCurrentBalance(java.lang.String apiKey) throws java.rmi.RemoteException;
    public java.lang.String getAllMask(java.lang.String apiKey) throws java.rmi.RemoteException;
    public java.lang.String getRates(java.lang.String apiKey) throws java.rmi.RemoteException;
    public java.lang.String getVersion(java.lang.String apiKey) throws java.rmi.RemoteException;
}
