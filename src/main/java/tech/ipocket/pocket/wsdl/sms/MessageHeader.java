/**
 * MessageHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.sms;

public class MessageHeader  implements java.io.Serializable {
    private java.lang.String userName;

    private java.lang.String userPassword;

    private java.lang.String marskText;

    private java.lang.String campingName;

    public MessageHeader() {
    }

    public MessageHeader(
           java.lang.String userName,
           java.lang.String userPassword,
           java.lang.String marskText,
           java.lang.String campingName) {
           this.userName = userName;
           this.userPassword = userPassword;
           this.marskText = marskText;
           this.campingName = campingName;
    }


    /**
     * Gets the userName value for this MessageHeader.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this MessageHeader.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the userPassword value for this MessageHeader.
     * 
     * @return userPassword
     */
    public java.lang.String getUserPassword() {
        return userPassword;
    }


    /**
     * Sets the userPassword value for this MessageHeader.
     * 
     * @param userPassword
     */
    public void setUserPassword(java.lang.String userPassword) {
        this.userPassword = userPassword;
    }


    /**
     * Gets the marskText value for this MessageHeader.
     * 
     * @return marskText
     */
    public java.lang.String getMarskText() {
        return marskText;
    }


    /**
     * Sets the marskText value for this MessageHeader.
     * 
     * @param marskText
     */
    public void setMarskText(java.lang.String marskText) {
        this.marskText = marskText;
    }


    /**
     * Gets the campingName value for this MessageHeader.
     * 
     * @return campingName
     */
    public java.lang.String getCampingName() {
        return campingName;
    }


    /**
     * Sets the campingName value for this MessageHeader.
     * 
     * @param campingName
     */
    public void setCampingName(java.lang.String campingName) {
        this.campingName = campingName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MessageHeader)) return false;
        MessageHeader other = (MessageHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.userPassword==null && other.getUserPassword()==null) || 
             (this.userPassword!=null &&
              this.userPassword.equals(other.getUserPassword()))) &&
            ((this.marskText==null && other.getMarskText()==null) || 
             (this.marskText!=null &&
              this.marskText.equals(other.getMarskText()))) &&
            ((this.campingName==null && other.getCampingName()==null) || 
             (this.campingName!=null &&
              this.campingName.equals(other.getCampingName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getUserPassword() != null) {
            _hashCode += getUserPassword().hashCode();
        }
        if (getMarskText() != null) {
            _hashCode += getMarskText().hashCode();
        }
        if (getCampingName() != null) {
            _hashCode += getCampingName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MessageHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "MessageHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "UserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "UserPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("marskText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "MarskText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campingName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.onnorokomsms.com/", "CampingName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
