/**
 * ResultObject1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.paywell;

public class ResultObject1  implements java.io.Serializable {
    private java.lang.String cwid;

    private java.lang.String status_code;

    private java.lang.String status_details;

    private java.lang.String telco_transaction_id;

    private java.lang.String transaction_id;

    private java.lang.String account_no;

    private java.lang.String amount;

    private java.lang.String connection_type;

    private java.lang.String freeze_amount_str;

    private java.lang.String service_freeze_amount_str;

    private java.lang.String dip;

    private java.lang.String information;

    public ResultObject1() {
    }

    public ResultObject1(
           java.lang.String cwid,
           java.lang.String status_code,
           java.lang.String status_details,
           java.lang.String telco_transaction_id,
           java.lang.String transaction_id,
           java.lang.String account_no,
           java.lang.String amount,
           java.lang.String connection_type,
           java.lang.String freeze_amount_str,
           java.lang.String service_freeze_amount_str,
           java.lang.String dip,
           java.lang.String information) {
           this.cwid = cwid;
           this.status_code = status_code;
           this.status_details = status_details;
           this.telco_transaction_id = telco_transaction_id;
           this.transaction_id = transaction_id;
           this.account_no = account_no;
           this.amount = amount;
           this.connection_type = connection_type;
           this.freeze_amount_str = freeze_amount_str;
           this.service_freeze_amount_str = service_freeze_amount_str;
           this.dip = dip;
           this.information = information;
    }


    /**
     * Gets the cwid value for this ResultObject1.
     * 
     * @return cwid
     */
    public java.lang.String getCwid() {
        return cwid;
    }


    /**
     * Sets the cwid value for this ResultObject1.
     * 
     * @param cwid
     */
    public void setCwid(java.lang.String cwid) {
        this.cwid = cwid;
    }


    /**
     * Gets the status_code value for this ResultObject1.
     * 
     * @return status_code
     */
    public java.lang.String getStatus_code() {
        return status_code;
    }


    /**
     * Sets the status_code value for this ResultObject1.
     * 
     * @param status_code
     */
    public void setStatus_code(java.lang.String status_code) {
        this.status_code = status_code;
    }


    /**
     * Gets the status_details value for this ResultObject1.
     * 
     * @return status_details
     */
    public java.lang.String getStatus_details() {
        return status_details;
    }


    /**
     * Sets the status_details value for this ResultObject1.
     * 
     * @param status_details
     */
    public void setStatus_details(java.lang.String status_details) {
        this.status_details = status_details;
    }


    /**
     * Gets the telco_transaction_id value for this ResultObject1.
     * 
     * @return telco_transaction_id
     */
    public java.lang.String getTelco_transaction_id() {
        return telco_transaction_id;
    }


    /**
     * Sets the telco_transaction_id value for this ResultObject1.
     * 
     * @param telco_transaction_id
     */
    public void setTelco_transaction_id(java.lang.String telco_transaction_id) {
        this.telco_transaction_id = telco_transaction_id;
    }


    /**
     * Gets the transaction_id value for this ResultObject1.
     * 
     * @return transaction_id
     */
    public java.lang.String getTransaction_id() {
        return transaction_id;
    }


    /**
     * Sets the transaction_id value for this ResultObject1.
     * 
     * @param transaction_id
     */
    public void setTransaction_id(java.lang.String transaction_id) {
        this.transaction_id = transaction_id;
    }


    /**
     * Gets the account_no value for this ResultObject1.
     * 
     * @return account_no
     */
    public java.lang.String getAccount_no() {
        return account_no;
    }


    /**
     * Sets the account_no value for this ResultObject1.
     * 
     * @param account_no
     */
    public void setAccount_no(java.lang.String account_no) {
        this.account_no = account_no;
    }


    /**
     * Gets the amount value for this ResultObject1.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this ResultObject1.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the connection_type value for this ResultObject1.
     * 
     * @return connection_type
     */
    public java.lang.String getConnection_type() {
        return connection_type;
    }


    /**
     * Sets the connection_type value for this ResultObject1.
     * 
     * @param connection_type
     */
    public void setConnection_type(java.lang.String connection_type) {
        this.connection_type = connection_type;
    }


    /**
     * Gets the freeze_amount_str value for this ResultObject1.
     * 
     * @return freeze_amount_str
     */
    public java.lang.String getFreeze_amount_str() {
        return freeze_amount_str;
    }


    /**
     * Sets the freeze_amount_str value for this ResultObject1.
     * 
     * @param freeze_amount_str
     */
    public void setFreeze_amount_str(java.lang.String freeze_amount_str) {
        this.freeze_amount_str = freeze_amount_str;
    }


    /**
     * Gets the service_freeze_amount_str value for this ResultObject1.
     * 
     * @return service_freeze_amount_str
     */
    public java.lang.String getService_freeze_amount_str() {
        return service_freeze_amount_str;
    }


    /**
     * Sets the service_freeze_amount_str value for this ResultObject1.
     * 
     * @param service_freeze_amount_str
     */
    public void setService_freeze_amount_str(java.lang.String service_freeze_amount_str) {
        this.service_freeze_amount_str = service_freeze_amount_str;
    }


    /**
     * Gets the dip value for this ResultObject1.
     * 
     * @return dip
     */
    public java.lang.String getDip() {
        return dip;
    }


    /**
     * Sets the dip value for this ResultObject1.
     * 
     * @param dip
     */
    public void setDip(java.lang.String dip) {
        this.dip = dip;
    }


    /**
     * Gets the information value for this ResultObject1.
     * 
     * @return information
     */
    public java.lang.String getInformation() {
        return information;
    }


    /**
     * Sets the information value for this ResultObject1.
     * 
     * @param information
     */
    public void setInformation(java.lang.String information) {
        this.information = information;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResultObject1)) return false;
        ResultObject1 other = (ResultObject1) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cwid==null && other.getCwid()==null) || 
             (this.cwid!=null &&
              this.cwid.equals(other.getCwid()))) &&
            ((this.status_code==null && other.getStatus_code()==null) || 
             (this.status_code!=null &&
              this.status_code.equals(other.getStatus_code()))) &&
            ((this.status_details==null && other.getStatus_details()==null) || 
             (this.status_details!=null &&
              this.status_details.equals(other.getStatus_details()))) &&
            ((this.telco_transaction_id==null && other.getTelco_transaction_id()==null) || 
             (this.telco_transaction_id!=null &&
              this.telco_transaction_id.equals(other.getTelco_transaction_id()))) &&
            ((this.transaction_id==null && other.getTransaction_id()==null) || 
             (this.transaction_id!=null &&
              this.transaction_id.equals(other.getTransaction_id()))) &&
            ((this.account_no==null && other.getAccount_no()==null) || 
             (this.account_no!=null &&
              this.account_no.equals(other.getAccount_no()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.connection_type==null && other.getConnection_type()==null) || 
             (this.connection_type!=null &&
              this.connection_type.equals(other.getConnection_type()))) &&
            ((this.freeze_amount_str==null && other.getFreeze_amount_str()==null) || 
             (this.freeze_amount_str!=null &&
              this.freeze_amount_str.equals(other.getFreeze_amount_str()))) &&
            ((this.service_freeze_amount_str==null && other.getService_freeze_amount_str()==null) || 
             (this.service_freeze_amount_str!=null &&
              this.service_freeze_amount_str.equals(other.getService_freeze_amount_str()))) &&
            ((this.dip==null && other.getDip()==null) || 
             (this.dip!=null &&
              this.dip.equals(other.getDip()))) &&
            ((this.information==null && other.getInformation()==null) || 
             (this.information!=null &&
              this.information.equals(other.getInformation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCwid() != null) {
            _hashCode += getCwid().hashCode();
        }
        if (getStatus_code() != null) {
            _hashCode += getStatus_code().hashCode();
        }
        if (getStatus_details() != null) {
            _hashCode += getStatus_details().hashCode();
        }
        if (getTelco_transaction_id() != null) {
            _hashCode += getTelco_transaction_id().hashCode();
        }
        if (getTransaction_id() != null) {
            _hashCode += getTransaction_id().hashCode();
        }
        if (getAccount_no() != null) {
            _hashCode += getAccount_no().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getConnection_type() != null) {
            _hashCode += getConnection_type().hashCode();
        }
        if (getFreeze_amount_str() != null) {
            _hashCode += getFreeze_amount_str().hashCode();
        }
        if (getService_freeze_amount_str() != null) {
            _hashCode += getService_freeze_amount_str().hashCode();
        }
        if (getDip() != null) {
            _hashCode += getDip().hashCode();
        }
        if (getInformation() != null) {
            _hashCode += getInformation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResultObject1.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:rechargerequestquote", "ResultObject1"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cwid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cwid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telco_transaction_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "telco_transaction_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transaction_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transaction_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("account_no");
        elemField.setXmlName(new javax.xml.namespace.QName("", "account_no"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("connection_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "connection_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freeze_amount_str");
        elemField.setXmlName(new javax.xml.namespace.QName("", "freeze_amount_str"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("service_freeze_amount_str");
        elemField.setXmlName(new javax.xml.namespace.QName("", "service_freeze_amount_str"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("information");
        elemField.setXmlName(new javax.xml.namespace.QName("", "information"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
