/**
 * DoWASABillPayReturnresult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.paywell;

public class DoWASABillPayReturnresult  implements java.io.Serializable {
    private java.lang.String status;

    private java.lang.String message;

    private java.lang.String trx_id;

    private java.lang.String msg_text;

    public DoWASABillPayReturnresult() {
    }

    public DoWASABillPayReturnresult(
           java.lang.String status,
           java.lang.String message,
           java.lang.String trx_id,
           java.lang.String msg_text) {
           this.status = status;
           this.message = message;
           this.trx_id = trx_id;
           this.msg_text = msg_text;
    }


    /**
     * Gets the status value for this DoWASABillPayReturnresult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this DoWASABillPayReturnresult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the message value for this DoWASABillPayReturnresult.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this DoWASABillPayReturnresult.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the trx_id value for this DoWASABillPayReturnresult.
     * 
     * @return trx_id
     */
    public java.lang.String getTrx_id() {
        return trx_id;
    }


    /**
     * Sets the trx_id value for this DoWASABillPayReturnresult.
     * 
     * @param trx_id
     */
    public void setTrx_id(java.lang.String trx_id) {
        this.trx_id = trx_id;
    }


    /**
     * Gets the msg_text value for this DoWASABillPayReturnresult.
     * 
     * @return msg_text
     */
    public java.lang.String getMsg_text() {
        return msg_text;
    }


    /**
     * Sets the msg_text value for this DoWASABillPayReturnresult.
     * 
     * @param msg_text
     */
    public void setMsg_text(java.lang.String msg_text) {
        this.msg_text = msg_text;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DoWASABillPayReturnresult)) return false;
        DoWASABillPayReturnresult other = (DoWASABillPayReturnresult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.trx_id==null && other.getTrx_id()==null) || 
             (this.trx_id!=null &&
              this.trx_id.equals(other.getTrx_id()))) &&
            ((this.msg_text==null && other.getMsg_text()==null) || 
             (this.msg_text!=null &&
              this.msg_text.equals(other.getMsg_text())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getTrx_id() != null) {
            _hashCode += getTrx_id().hashCode();
        }
        if (getMsg_text() != null) {
            _hashCode += getMsg_text().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DoWASABillPayReturnresult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:rechargerequestquote", "doWASABillPayReturnresult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trx_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trx_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msg_text");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msg_text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
