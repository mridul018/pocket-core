<?xml version="1.0" encoding="ISO-8859-1"?>
<definitions xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="urn:rechargerequestquote" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="urn:rechargerequestquote">
<types>
<xsd:schema targetNamespace="urn:rechargerequestquote"
>
 <xsd:import namespace="http://schemas.xmlsoap.org/soap/encoding/" />
 <xsd:import namespace="http://schemas.xmlsoap.org/wsdl/" />
 <xsd:complexType name="ResultObject">
  <xsd:all>
   <xsd:element name="cwid" type="xsd:string"/>
   <xsd:element name="status_code" type="xsd:string"/>
   <xsd:element name="status_details" type="xsd:string"/>
   <xsd:element name="mobile_number" type="xsd:string"/>
   <xsd:element name="amount" type="xsd:string"/>
   <xsd:element name="last_update_time" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="ResultObject1">
  <xsd:all>
   <xsd:element name="cwid" type="xsd:string"/>
   <xsd:element name="status_code" type="xsd:string"/>
   <xsd:element name="status_details" type="xsd:string"/>
   <xsd:element name="telco_transaction_id" type="xsd:string"/>
   <xsd:element name="transaction_id" type="xsd:string"/>
   <xsd:element name="account_no" type="xsd:string"/>
   <xsd:element name="amount" type="xsd:string"/>
   <xsd:element name="connection_type" type="xsd:string"/>
   <xsd:element name="freeze_amount_str" type="xsd:string"/>
   <xsd:element name="service_freeze_amount_str" type="xsd:string"/>
   <xsd:element name="dip" type="xsd:string"/>
   <xsd:element name="information" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="Returnresult">
  <xsd:all>
   <xsd:element name="cwid" type="xsd:string"/>
   <xsd:element name="status_code" type="xsd:string"/>
   <xsd:element name="status_details" type="xsd:string"/>
   <xsd:element name="telco_transaction_id" type="xsd:string"/>
   <xsd:element name="transaction_id" type="xsd:string"/>
   <xsd:element name="account_no" type="xsd:string"/>
   <xsd:element name="amount" type="xsd:string"/>
   <xsd:element name="connection_type" type="xsd:string"/>
   <xsd:element name="freeze_amount_str" type="xsd:string"/>
   <xsd:element name="service_freeze_amount_str" type="xsd:string"/>
   <xsd:element name="username" type="xsd:string"/>
   <xsd:element name="service_type" type="xsd:string"/>
   <xsd:element name="password" type="xsd:string"/>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="dip" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="bill_amount" type="xsd:string"/>
   <xsd:element name="retailer_commission" type="xsd:string"/>
   <xsd:element name="billAmount" type="xsd:string"/>
   <xsd:element name="extraCharge" type="xsd:string"/>
   <xsd:element name="totalAmount" type="xsd:string"/>
   <xsd:element name="customerName" type="xsd:string"/>
   <xsd:element name="cutomerPhoneNumber" type="xsd:string"/>
   <xsd:element name="retailerCommission" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
   <xsd:element name="information" type="xsd:string"/>
   <xsd:element name="payment_number" type="xsd:string"/>
   <xsd:element name="tbps_charge" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
   <xsd:element name="outlet_name" type="xsd:string"/>
   <xsd:element name="address" type="xsd:string"/>
   <xsd:element name="hotline" type="xsd:string"/>
   <xsd:element name="retailer_code" type="xsd:string"/>
   <xsd:element name="noOfTrx" type="xsd:string"/>
   <xsd:element name="trans_id" type="xsd:string"/>
   <xsd:element name="balance" type="xsd:string"/>
   <xsd:element name="frozen_balance" type="xsd:string"/>
   <xsd:element name="purpose_balance" type="xsd:string"/>
   <xsd:element name="payment_id" type="xsd:string"/>
   <xsd:element name="payment_amount" type="xsd:string"/>
   <xsd:element name="payment_status" type="xsd:string"/>
   <xsd:element name="payment_date" type="xsd:string"/>
   <xsd:element name="sender" type="xsd:string"/>
   <xsd:element name="trx_ref" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getDPDCEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getDESCOEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getWASAEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getKGDCLEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getWZPDCLEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="doDPDCBillPayReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="doDESCOBillPayReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="doWASABillPayReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="doKGDCLBillPayReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="doWZPDCLBillPayReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="RetrunResult_1">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="RetrunResult_sentPollybuddutRegistrationOTP">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="RetrunResult_pollybiddutBillPay">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
   <xsd:element name="total_amount" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="RetrunResult2">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="data" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getDPDCBillEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="noOfTrx" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getDESCOBillEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="noOfTrx" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getWASABillEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="noOfTrx" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getWZPDCLBillEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="noOfTrx" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="getKGDCLBillEnquiryReturnresult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="noOfTrx" type="xsd:string"/>
   <xsd:element name="msg_text" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="pollybuddutRegistrationRetrunResult">
  <xsd:all>
   <xsd:element name="status" type="xsd:string"/>
   <xsd:element name="message" type="xsd:string"/>
   <xsd:element name="trx_id" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
</xsd:schema>
</types>
<message name="initiateRechargeRequest">
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" />
  <part name="crid" type="xsd:string" />
  <part name="msisdn" type="xsd:string" />
  <part name="amount" type="xsd:string" />
  <part name="connection" type="xsd:string" />
  <part name="sender" type="xsd:string" /></message>
<message name="initiateRechargeResponse">
  <part name="ResultObject1" type="tns:ResultObject1" /></message>
<message name="commitRechargeRequest">
  <part name="cwid" type="xsd:string" />
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" /></message>
<message name="commitRechargeResponse">
  <part name="ResultObject1" type="tns:ResultObject1" /></message>
<message name="cancelRechargeRequest">
  <part name="cwid" type="xsd:string" />
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" /></message>
<message name="cancelRechargeResponse">
  <part name="ResultObject1" type="tns:ResultObject1" /></message>
<message name="queryRechargeRequest">
  <part name="crid" type="xsd:string" />
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" /></message>
<message name="queryRechargeResponse">
  <part name="ResultObject" type="tns:ResultObject" /></message>
<message name="queryBalanceRequest">
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" /></message>
<message name="queryBalanceResponse">
  <part name="return" type="xsd:string" /></message>
<message name="checkBkashBalanceRequest">
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" /></message>
<message name="checkBkashBalanceResponse">
  <part name="return" type="xsd:string" /></message>
<message name="topupRequest">
  <part name="clientid" type="xsd:string" />
  <part name="clientpass" type="xsd:string" />
  <part name="crid" type="xsd:string" />
  <part name="msisdn" type="xsd:string" />
  <part name="amount" type="xsd:string" />
  <part name="connection" type="xsd:string" />
  <part name="operator" type="xsd:string" />
  <part name="sender" type="xsd:string" /></message>
<message name="topupResponse">
  <part name="ResultObject1" type="tns:ResultObject1" /></message>
<message name="wimax_topupRequest">
  <part name="username" type="xsd:string" />
  <part name="pincode" type="xsd:string" />
  <part name="wimax" type="xsd:string" />
  <part name="account_no" type="xsd:string" />
  <part name="amount" type="xsd:string" />
  <part name="connection_type" type="xsd:string" /></message>
<message name="wimax_topupResponse">
  <part name="ResultObject1" type="tns:ResultObject1" /></message>
<message name="wimax_enqueryRequest">
  <part name="transaction_id" type="xsd:string" /></message>
<message name="wimax_enqueryResponse">
  <part name="ResultObject1" type="tns:ResultObject1" /></message>
<message name="getWZPDCLEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="billNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" />
  <part name="billYearMonth" type="xsd:string" /></message>
<message name="getWZPDCLEnquiryResponse">
  <part name="getWZPDCLEnquiryReturnresult" type="tns:getWZPDCLEnquiryReturnresult" /></message>
<message name="doWZPDCLBillPayRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="billNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" />
  <part name="billYearMonth" type="xsd:string" />
  <part name="trx_id" type="xsd:string" />
  <part name="amount" type="xsd:string" /></message>
<message name="doWZPDCLBillPayResponse">
  <part name="doWZPDCLBillPayReturnresult" type="tns:doWZPDCLBillPayReturnresult" /></message>
<message name="getKGDCLEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="billNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" /></message>
<message name="getKGDCLEnquiryResponse">
  <part name="getKGDCLEnquiryReturnresult" type="tns:getKGDCLEnquiryReturnresult" /></message>
<message name="doKGDCLBillPayRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="billNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" />
  <part name="trans_id" type="xsd:string" />
  <part name="amount" type="xsd:string" /></message>
<message name="doKGDCLBillPayResponse">
  <part name="doKGDCLBillPayReturnresult" type="tns:doKGDCLBillPayReturnresult" /></message>
<message name="getDPDCEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="customerNo" type="xsd:string" />
  <part name="billYearMonth" type="xsd:string" />
  <part name="location" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" /></message>
<message name="getDPDCEnquiryResponse">
  <part name="getDPDCEnquiryReturnresult" type="tns:getDPDCEnquiryReturnresult" /></message>
<message name="doDPDCBillPayRequest">
  <part name="amount" type="xsd:string" />
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="customerNo" type="xsd:string" />
  <part name="billYearMonth" type="xsd:string" />
  <part name="location" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" />
  <part name="trx_id" type="xsd:string" /></message>
<message name="doDPDCBillPayResponse">
  <part name="doDPDCBillPayReturnresult" type="tns:doDPDCBillPayReturnresult" /></message>
<message name="getDPDCBillEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="payerMobileNoOrBillNo" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="getDPDCBillEnquiryResponse">
  <part name="getDPDCBillEnquiryReturnresult" type="tns:getDPDCBillEnquiryReturnresult" /></message>
<message name="getDESCOBillEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="payerMobileNoOrBillNo" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="getDESCOBillEnquiryResponse">
  <part name="getDESCOBillEnquiryReturnresult" type="tns:getDESCOBillEnquiryReturnresult" /></message>
<message name="getWASABillEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="payerMobileNoOrBillNo" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="getWASABillEnquiryResponse">
  <part name="getWASABillEnquiryReturnresult" type="tns:getWASABillEnquiryReturnresult" /></message>
<message name="getWZPDCLBillEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="payerMobileNoOrBillNo" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="getWZPDCLBillEnquiryResponse">
  <part name="getWZPDCLBillEnquiryReturnresult" type="tns:getWZPDCLBillEnquiryReturnresult" /></message>
<message name="getKGDCLBillEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="payerMobileNoOrBillNo" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="getKGDCLBillEnquiryResponse">
  <part name="getKGDCLBillEnquiryReturnresult" type="tns:getKGDCLBillEnquiryReturnresult" /></message>
<message name="getDESCOEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="BillNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" /></message>
<message name="getDESCOEnquiryResponse">
  <part name="getDESCOEnquiryReturnresult" type="tns:getDESCOEnquiryReturnresult" /></message>
<message name="doDESCOBillPayRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="BillNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" />
  <part name="trx_id" type="xsd:string" />
  <part name="amount" type="xsd:string" /></message>
<message name="doDESCOBillPayResponse">
  <part name="doDESCOBillPayReturnresult" type="tns:doDESCOBillPayReturnresult" /></message>
<message name="getWASAEnquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="BillNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" /></message>
<message name="getWASAEnquiryResponse">
  <part name="getWASAEnquiryReturnresult" type="tns:getWASAEnquiryReturnresult" /></message>
<message name="doWASABillPayRequest">
  <part name="amount" type="xsd:string" />
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="BillNo" type="xsd:string" />
  <part name="payerMobileNo" type="xsd:string" />
  <part name="trx_id" type="xsd:string" /></message>
<message name="doWASABillPayResponse">
  <part name="doWASABillPayReturnresult" type="tns:doWASABillPayReturnresult" /></message>
<message name="getIvacCenterRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" /></message>
<message name="getIvacCenterResponse">
  <part name="Returnresult" type="tns:Returnresult" /></message>
<message name="IVACRequestRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="amount" type="xsd:string" />
  <part name="web_file_no" type="xsd:string" />
  <part name="passport_no" type="xsd:string" />
  <part name="center_no" type="xsd:string" />
  <part name="cust_mobile" type="xsd:string" /></message>
<message name="IVACRequestResponse">
  <part name="Returnresult" type="tns:Returnresult" /></message>
<message name="getIvacCenterSandboxRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" /></message>
<message name="getIvacCenterSandboxResponse">
  <part name="Returnresult" type="tns:Returnresult" /></message>
<message name="IVACRequestSandboxRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="amount" type="xsd:string" />
  <part name="web_file_no" type="xsd:string" />
  <part name="passport_no" type="xsd:string" />
  <part name="center_no" type="xsd:string" />
  <part name="cust_mobile" type="xsd:string" /></message>
<message name="IVACRequestSandboxResponse">
  <part name="Returnresult" type="tns:Returnresult" /></message>
<message name="getIvacTrxListRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="getIvacTrxListResponse">
  <part name="Returnresult" type="tns:Returnresult" /></message>
<message name="pollybuddutPhoneNumberChangeRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="accountNo" type="xsd:string" />
  <part name="msisdn" type="xsd:string" /></message>
<message name="pollybuddutPhoneNumberChangeResponse">
  <part name="RetrunResult_1" type="tns:RetrunResult_1" /></message>
<message name="pollybuddutBillStatusRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="accountNo" type="xsd:string" />
  <part name="month" type="xsd:string" />
  <part name="year" type="xsd:string" /></message>
<message name="pollybuddutBillStatusResponse">
  <part name="RetrunResult_1" type="tns:RetrunResult_1" /></message>
<message name="pollybuddutRegistrationInitiateRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="accountNo" type="xsd:string" />
  <part name="customerName" type="xsd:string" />
  <part name="customerPhoneNumber" type="xsd:string" /></message>
<message name="pollybuddutRegistrationInitiateResponse">
  <part name="RetrunResult_1" type="tns:RetrunResult_1" /></message>
<message name="sentPollybuddutRegistrationOTPRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="accountNo" type="xsd:string" />
  <part name="customerName" type="xsd:string" />
  <part name="customerPhoneNumber" type="xsd:string" />
  <part name="otp" type="xsd:string" /></message>
<message name="sentPollybuddutRegistrationOTPResponse">
  <part name="RetrunResult_sentPollybuddutRegistrationOTP" type="tns:RetrunResult_sentPollybuddutRegistrationOTP" /></message>
<message name="pollybiddutBillPayRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="bill_no" type="xsd:string" />
  <part name="ref_id" type="xsd:string" />
  <part name="amount" type="xsd:string" />
  <part name="smsAccountNumber" type="xsd:string" />
  <part name="coustomerPhoneNumber" type="xsd:string" /></message>
<message name="pollybiddutBillPayResponse">
  <part name="RetrunResult_pollybiddutBillPay" type="tns:RetrunResult_pollybiddutBillPay" /></message>
<message name="pollyBuddutBillPayInquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="pollyBuddutBillPayInquiryResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="pollyBuddutNumberChangeInquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="pollyBuddutNumberChangeInquiryResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="pollyBuddutRegistrationInquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="pollyBuddutRegistrationInquiryResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="pollyBuddutBillStatusInquiryRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="limit" type="xsd:string" /></message>
<message name="pollyBuddutBillStatusInquiryResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="getPollyBillEnquiryDataRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="ref_id" type="xsd:string" />
  <part name="bill_no" type="xsd:string" /></message>
<message name="getPollyBillEnquiryDataResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="getPollyBillRegEnquiryDataRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="ref_id" type="xsd:string" />
  <part name="sms_account_no" type="xsd:string" /></message>
<message name="getPollyBillRegEnquiryDataResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="getPollyPhoneNoChangeAndBillStatusDataRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="ref_id" type="xsd:string" /></message>
<message name="getPollyPhoneNoChangeAndBillStatusDataResponse">
  <part name="RetrunResult2" type="tns:RetrunResult2" /></message>
<message name="pollybuddutRegistrationRequest">
  <part name="username" type="xsd:string" />
  <part name="password" type="xsd:string" />
  <part name="accountNo" type="xsd:string" />
  <part name="customerPhoneNumber" type="xsd:string" />
  <part name="customerName" type="xsd:string" /></message>
<message name="pollybuddutRegistrationResponse">
  <part name="pollybuddutRegistrationRetrunResult" type="tns:pollybuddutRegistrationRetrunResult" /></message>
<portType name="rechargerequestserverPortType">
  <operation name="initiateRecharge">
    <input message="tns:initiateRechargeRequest"/>
    <output message="tns:initiateRechargeResponse"/>
  </operation>
  <operation name="commitRecharge">
    <input message="tns:commitRechargeRequest"/>
    <output message="tns:commitRechargeResponse"/>
  </operation>
  <operation name="cancelRecharge">
    <input message="tns:cancelRechargeRequest"/>
    <output message="tns:cancelRechargeResponse"/>
  </operation>
  <operation name="queryRecharge">
    <input message="tns:queryRechargeRequest"/>
    <output message="tns:queryRechargeResponse"/>
  </operation>
  <operation name="queryBalance">
    <input message="tns:queryBalanceRequest"/>
    <output message="tns:queryBalanceResponse"/>
  </operation>
  <operation name="checkBkashBalance">
    <input message="tns:checkBkashBalanceRequest"/>
    <output message="tns:checkBkashBalanceResponse"/>
  </operation>
  <operation name="topup">
    <input message="tns:topupRequest"/>
    <output message="tns:topupResponse"/>
  </operation>
  <operation name="wimax_topup">
    <input message="tns:wimax_topupRequest"/>
    <output message="tns:wimax_topupResponse"/>
  </operation>
  <operation name="wimax_enquery">
    <input message="tns:wimax_enqueryRequest"/>
    <output message="tns:wimax_enqueryResponse"/>
  </operation>
  <operation name="getWZPDCLEnquiry">
    <input message="tns:getWZPDCLEnquiryRequest"/>
    <output message="tns:getWZPDCLEnquiryResponse"/>
  </operation>
  <operation name="doWZPDCLBillPay">
    <input message="tns:doWZPDCLBillPayRequest"/>
    <output message="tns:doWZPDCLBillPayResponse"/>
  </operation>
  <operation name="getKGDCLEnquiry">
    <input message="tns:getKGDCLEnquiryRequest"/>
    <output message="tns:getKGDCLEnquiryResponse"/>
  </operation>
  <operation name="doKGDCLBillPay">
    <input message="tns:doKGDCLBillPayRequest"/>
    <output message="tns:doKGDCLBillPayResponse"/>
  </operation>
  <operation name="getDPDCEnquiry">
    <input message="tns:getDPDCEnquiryRequest"/>
    <output message="tns:getDPDCEnquiryResponse"/>
  </operation>
  <operation name="doDPDCBillPay">
    <input message="tns:doDPDCBillPayRequest"/>
    <output message="tns:doDPDCBillPayResponse"/>
  </operation>
  <operation name="getDPDCBillEnquiry">
    <input message="tns:getDPDCBillEnquiryRequest"/>
    <output message="tns:getDPDCBillEnquiryResponse"/>
  </operation>
  <operation name="getDESCOBillEnquiry">
    <input message="tns:getDESCOBillEnquiryRequest"/>
    <output message="tns:getDESCOBillEnquiryResponse"/>
  </operation>
  <operation name="getWASABillEnquiry">
    <input message="tns:getWASABillEnquiryRequest"/>
    <output message="tns:getWASABillEnquiryResponse"/>
  </operation>
  <operation name="getWZPDCLBillEnquiry">
    <input message="tns:getWZPDCLBillEnquiryRequest"/>
    <output message="tns:getWZPDCLBillEnquiryResponse"/>
  </operation>
  <operation name="getKGDCLBillEnquiry">
    <input message="tns:getKGDCLBillEnquiryRequest"/>
    <output message="tns:getKGDCLBillEnquiryResponse"/>
  </operation>
  <operation name="getDESCOEnquiry">
    <input message="tns:getDESCOEnquiryRequest"/>
    <output message="tns:getDESCOEnquiryResponse"/>
  </operation>
  <operation name="doDESCOBillPay">
    <input message="tns:doDESCOBillPayRequest"/>
    <output message="tns:doDESCOBillPayResponse"/>
  </operation>
  <operation name="getWASAEnquiry">
    <input message="tns:getWASAEnquiryRequest"/>
    <output message="tns:getWASAEnquiryResponse"/>
  </operation>
  <operation name="doWASABillPay">
    <input message="tns:doWASABillPayRequest"/>
    <output message="tns:doWASABillPayResponse"/>
  </operation>
  <operation name="getIvacCenter">
    <input message="tns:getIvacCenterRequest"/>
    <output message="tns:getIvacCenterResponse"/>
  </operation>
  <operation name="IVACRequest">
    <input message="tns:IVACRequestRequest"/>
    <output message="tns:IVACRequestResponse"/>
  </operation>
  <operation name="getIvacCenterSandbox">
    <input message="tns:getIvacCenterSandboxRequest"/>
    <output message="tns:getIvacCenterSandboxResponse"/>
  </operation>
  <operation name="IVACRequestSandbox">
    <input message="tns:IVACRequestSandboxRequest"/>
    <output message="tns:IVACRequestSandboxResponse"/>
  </operation>
  <operation name="getIvacTrxList">
    <input message="tns:getIvacTrxListRequest"/>
    <output message="tns:getIvacTrxListResponse"/>
  </operation>
  <operation name="pollybuddutPhoneNumberChange">
    <input message="tns:pollybuddutPhoneNumberChangeRequest"/>
    <output message="tns:pollybuddutPhoneNumberChangeResponse"/>
  </operation>
  <operation name="pollybuddutBillStatus">
    <input message="tns:pollybuddutBillStatusRequest"/>
    <output message="tns:pollybuddutBillStatusResponse"/>
  </operation>
  <operation name="pollybuddutRegistrationInitiate">
    <input message="tns:pollybuddutRegistrationInitiateRequest"/>
    <output message="tns:pollybuddutRegistrationInitiateResponse"/>
  </operation>
  <operation name="sentPollybuddutRegistrationOTP">
    <input message="tns:sentPollybuddutRegistrationOTPRequest"/>
    <output message="tns:sentPollybuddutRegistrationOTPResponse"/>
  </operation>
  <operation name="pollybiddutBillPay">
    <input message="tns:pollybiddutBillPayRequest"/>
    <output message="tns:pollybiddutBillPayResponse"/>
  </operation>
  <operation name="pollyBuddutBillPayInquiry">
    <input message="tns:pollyBuddutBillPayInquiryRequest"/>
    <output message="tns:pollyBuddutBillPayInquiryResponse"/>
  </operation>
  <operation name="pollyBuddutNumberChangeInquiry">
    <input message="tns:pollyBuddutNumberChangeInquiryRequest"/>
    <output message="tns:pollyBuddutNumberChangeInquiryResponse"/>
  </operation>
  <operation name="pollyBuddutRegistrationInquiry">
    <input message="tns:pollyBuddutRegistrationInquiryRequest"/>
    <output message="tns:pollyBuddutRegistrationInquiryResponse"/>
  </operation>
  <operation name="pollyBuddutBillStatusInquiry">
    <input message="tns:pollyBuddutBillStatusInquiryRequest"/>
    <output message="tns:pollyBuddutBillStatusInquiryResponse"/>
  </operation>
  <operation name="getPollyBillEnquiryData">
    <input message="tns:getPollyBillEnquiryDataRequest"/>
    <output message="tns:getPollyBillEnquiryDataResponse"/>
  </operation>
  <operation name="getPollyBillRegEnquiryData">
    <input message="tns:getPollyBillRegEnquiryDataRequest"/>
    <output message="tns:getPollyBillRegEnquiryDataResponse"/>
  </operation>
  <operation name="getPollyPhoneNoChangeAndBillStatusData">
    <input message="tns:getPollyPhoneNoChangeAndBillStatusDataRequest"/>
    <output message="tns:getPollyPhoneNoChangeAndBillStatusDataResponse"/>
  </operation>
  <operation name="pollybuddutRegistration">
    <input message="tns:pollybuddutRegistrationRequest"/>
    <output message="tns:pollybuddutRegistrationResponse"/>
  </operation>
</portType>
<binding name="rechargerequestserverBinding" type="tns:rechargerequestserverPortType">
  <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
  <operation name="initiateRecharge">
    <soap:operation soapAction="urn:rechargerequestquote#initiateRecharge" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="commitRecharge">
    <soap:operation soapAction="urn:rechargerequestquote#commitRecharge" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="cancelRecharge">
    <soap:operation soapAction="urn:rechargerequestquote#cancelRecharge" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="queryRecharge">
    <soap:operation soapAction="urn:rechargerequestquote#queryRecharge" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="queryBalance">
    <soap:operation soapAction="urn:rechargerequestquote#queryBalance" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="checkBkashBalance">
    <soap:operation soapAction="urn:rechargerequestquote#checkBkashBalance" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="topup">
    <soap:operation soapAction="urn:rechargerequestquote#topup" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="wimax_topup">
    <soap:operation soapAction="urn:rechargerequestquote#wimax_topup" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="wimax_enquery">
    <soap:operation soapAction="urn:rechargerequestquote#wimax_enquery" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getWZPDCLEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getWZPDCLEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="doWZPDCLBillPay">
    <soap:operation soapAction="urn:rechargerequestquote#doWZPDCLBillPay" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getKGDCLEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getKGDCLEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="doKGDCLBillPay">
    <soap:operation soapAction="urn:rechargerequestquote#doKGDCLBillPay" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getDPDCEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getDPDCEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="doDPDCBillPay">
    <soap:operation soapAction="urn:rechargerequestquote#doDPDCBillPay" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getDPDCBillEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getDPDCBillEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getDESCOBillEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getDESCOBillEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getWASABillEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getWASABillEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getWZPDCLBillEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getWZPDCLBillEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getKGDCLBillEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getKGDCLBillEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getDESCOEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getDESCOEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="doDESCOBillPay">
    <soap:operation soapAction="urn:rechargerequestquote#doDESCOBillPay" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getWASAEnquiry">
    <soap:operation soapAction="urn:rechargerequestquote#getWASAEnquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="doWASABillPay">
    <soap:operation soapAction="urn:rechargerequestquote#doWASABillPay" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getIvacCenter">
    <soap:operation soapAction="urn:rechargerequestquote#getIvacCenter" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="IVACRequest">
    <soap:operation soapAction="urn:rechargerequestquote#IVACRequest" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getIvacCenterSandbox">
    <soap:operation soapAction="urn:rechargerequestquote#getIvacCenterSandbox" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="IVACRequestSandbox">
    <soap:operation soapAction="urn:rechargerequestquote#IVACRequestSandbox" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getIvacTrxList">
    <soap:operation soapAction="urn:rechargerequestquote#getIvacTrxList" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollybuddutPhoneNumberChange">
    <soap:operation soapAction="urn:rechargerequestquote#pollybuddutPhoneNumberChange" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollybuddutBillStatus">
    <soap:operation soapAction="urn:rechargerequestquote#pollybuddutBillStatus" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollybuddutRegistrationInitiate">
    <soap:operation soapAction="urn:rechargerequestquote#pollybuddutRegistrationInitiate" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="sentPollybuddutRegistrationOTP">
    <soap:operation soapAction="urn:rechargerequestquote#sentPollybuddutRegistrationOTP" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollybiddutBillPay">
    <soap:operation soapAction="urn:rechargerequestquote#pollybiddutBillPay" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollyBuddutBillPayInquiry">
    <soap:operation soapAction="urn:rechargerequestquote#pollyBuddutBillPayInquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollyBuddutNumberChangeInquiry">
    <soap:operation soapAction="urn:rechargerequestquote#pollyBuddutNumberChangeInquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollyBuddutRegistrationInquiry">
    <soap:operation soapAction="urn:rechargerequestquote#pollyBuddutRegistrationInquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollyBuddutBillStatusInquiry">
    <soap:operation soapAction="urn:rechargerequestquote#pollyBuddutBillStatusInquiry" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getPollyBillEnquiryData">
    <soap:operation soapAction="urn:rechargerequestquote#getPollyBillEnquiryData" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getPollyBillRegEnquiryData">
    <soap:operation soapAction="urn:rechargerequestquote#getPollyBillRegEnquiryData" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="getPollyPhoneNoChangeAndBillStatusData">
    <soap:operation soapAction="urn:rechargerequestquote#getPollyPhoneNoChangeAndBillStatusData" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
  <operation name="pollybuddutRegistration">
    <soap:operation soapAction="urn:rechargerequestquote#pollybuddutRegistration" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:rechargerequestquote" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
</binding>
<service name="rechargerequestserver">
  <port name="rechargerequestserverPort" binding="tns:rechargerequestserverBinding">
    <soap:address location="https://www.paywellonline.com:443/National_topupsoap/paywellservice.php"/>
  </port>
</service>
</definitions>
