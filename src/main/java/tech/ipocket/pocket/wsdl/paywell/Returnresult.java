/**
 * Returnresult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.paywell;

public class Returnresult  implements java.io.Serializable {
    private java.lang.String cwid;

    private java.lang.String status_code;

    private java.lang.String status_details;

    private java.lang.String telco_transaction_id;

    private java.lang.String transaction_id;

    private java.lang.String account_no;

    private java.lang.String amount;

    private java.lang.String connection_type;

    private java.lang.String freeze_amount_str;

    private java.lang.String service_freeze_amount_str;

    private java.lang.String username;

    private java.lang.String service_type;

    private java.lang.String password;

    private java.lang.String status;

    private java.lang.String message;

    private java.lang.String dip;

    private java.lang.String trx_id;

    private java.lang.String bill_amount;

    private java.lang.String retailer_commission;

    private java.lang.String billAmount;

    private java.lang.String extraCharge;

    private java.lang.String totalAmount;

    private java.lang.String customerName;

    private java.lang.String cutomerPhoneNumber;

    private java.lang.String retailerCommission;

    private java.lang.String msg_text;

    private java.lang.String information;

    private java.lang.String payment_number;

    private java.lang.String tbps_charge;

    private java.lang.String total_amount;

    private java.lang.String outlet_name;

    private java.lang.String address;

    private java.lang.String hotline;

    private java.lang.String retailer_code;

    private java.lang.String noOfTrx;

    private java.lang.String trans_id;

    private java.lang.String balance;

    private java.lang.String frozen_balance;

    private java.lang.String purpose_balance;

    private java.lang.String payment_id;

    private java.lang.String payment_amount;

    private java.lang.String payment_status;

    private java.lang.String payment_date;

    private java.lang.String sender;

    private java.lang.String trx_ref;

    public Returnresult() {
    }

    public Returnresult(
           java.lang.String cwid,
           java.lang.String status_code,
           java.lang.String status_details,
           java.lang.String telco_transaction_id,
           java.lang.String transaction_id,
           java.lang.String account_no,
           java.lang.String amount,
           java.lang.String connection_type,
           java.lang.String freeze_amount_str,
           java.lang.String service_freeze_amount_str,
           java.lang.String username,
           java.lang.String service_type,
           java.lang.String password,
           java.lang.String status,
           java.lang.String message,
           java.lang.String dip,
           java.lang.String trx_id,
           java.lang.String bill_amount,
           java.lang.String retailer_commission,
           java.lang.String billAmount,
           java.lang.String extraCharge,
           java.lang.String totalAmount,
           java.lang.String customerName,
           java.lang.String cutomerPhoneNumber,
           java.lang.String retailerCommission,
           java.lang.String msg_text,
           java.lang.String information,
           java.lang.String payment_number,
           java.lang.String tbps_charge,
           java.lang.String total_amount,
           java.lang.String outlet_name,
           java.lang.String address,
           java.lang.String hotline,
           java.lang.String retailer_code,
           java.lang.String noOfTrx,
           java.lang.String trans_id,
           java.lang.String balance,
           java.lang.String frozen_balance,
           java.lang.String purpose_balance,
           java.lang.String payment_id,
           java.lang.String payment_amount,
           java.lang.String payment_status,
           java.lang.String payment_date,
           java.lang.String sender,
           java.lang.String trx_ref) {
           this.cwid = cwid;
           this.status_code = status_code;
           this.status_details = status_details;
           this.telco_transaction_id = telco_transaction_id;
           this.transaction_id = transaction_id;
           this.account_no = account_no;
           this.amount = amount;
           this.connection_type = connection_type;
           this.freeze_amount_str = freeze_amount_str;
           this.service_freeze_amount_str = service_freeze_amount_str;
           this.username = username;
           this.service_type = service_type;
           this.password = password;
           this.status = status;
           this.message = message;
           this.dip = dip;
           this.trx_id = trx_id;
           this.bill_amount = bill_amount;
           this.retailer_commission = retailer_commission;
           this.billAmount = billAmount;
           this.extraCharge = extraCharge;
           this.totalAmount = totalAmount;
           this.customerName = customerName;
           this.cutomerPhoneNumber = cutomerPhoneNumber;
           this.retailerCommission = retailerCommission;
           this.msg_text = msg_text;
           this.information = information;
           this.payment_number = payment_number;
           this.tbps_charge = tbps_charge;
           this.total_amount = total_amount;
           this.outlet_name = outlet_name;
           this.address = address;
           this.hotline = hotline;
           this.retailer_code = retailer_code;
           this.noOfTrx = noOfTrx;
           this.trans_id = trans_id;
           this.balance = balance;
           this.frozen_balance = frozen_balance;
           this.purpose_balance = purpose_balance;
           this.payment_id = payment_id;
           this.payment_amount = payment_amount;
           this.payment_status = payment_status;
           this.payment_date = payment_date;
           this.sender = sender;
           this.trx_ref = trx_ref;
    }


    /**
     * Gets the cwid value for this Returnresult.
     * 
     * @return cwid
     */
    public java.lang.String getCwid() {
        return cwid;
    }


    /**
     * Sets the cwid value for this Returnresult.
     * 
     * @param cwid
     */
    public void setCwid(java.lang.String cwid) {
        this.cwid = cwid;
    }


    /**
     * Gets the status_code value for this Returnresult.
     * 
     * @return status_code
     */
    public java.lang.String getStatus_code() {
        return status_code;
    }


    /**
     * Sets the status_code value for this Returnresult.
     * 
     * @param status_code
     */
    public void setStatus_code(java.lang.String status_code) {
        this.status_code = status_code;
    }


    /**
     * Gets the status_details value for this Returnresult.
     * 
     * @return status_details
     */
    public java.lang.String getStatus_details() {
        return status_details;
    }


    /**
     * Sets the status_details value for this Returnresult.
     * 
     * @param status_details
     */
    public void setStatus_details(java.lang.String status_details) {
        this.status_details = status_details;
    }


    /**
     * Gets the telco_transaction_id value for this Returnresult.
     * 
     * @return telco_transaction_id
     */
    public java.lang.String getTelco_transaction_id() {
        return telco_transaction_id;
    }


    /**
     * Sets the telco_transaction_id value for this Returnresult.
     * 
     * @param telco_transaction_id
     */
    public void setTelco_transaction_id(java.lang.String telco_transaction_id) {
        this.telco_transaction_id = telco_transaction_id;
    }


    /**
     * Gets the transaction_id value for this Returnresult.
     * 
     * @return transaction_id
     */
    public java.lang.String getTransaction_id() {
        return transaction_id;
    }


    /**
     * Sets the transaction_id value for this Returnresult.
     * 
     * @param transaction_id
     */
    public void setTransaction_id(java.lang.String transaction_id) {
        this.transaction_id = transaction_id;
    }


    /**
     * Gets the account_no value for this Returnresult.
     * 
     * @return account_no
     */
    public java.lang.String getAccount_no() {
        return account_no;
    }


    /**
     * Sets the account_no value for this Returnresult.
     * 
     * @param account_no
     */
    public void setAccount_no(java.lang.String account_no) {
        this.account_no = account_no;
    }


    /**
     * Gets the amount value for this Returnresult.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this Returnresult.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the connection_type value for this Returnresult.
     * 
     * @return connection_type
     */
    public java.lang.String getConnection_type() {
        return connection_type;
    }


    /**
     * Sets the connection_type value for this Returnresult.
     * 
     * @param connection_type
     */
    public void setConnection_type(java.lang.String connection_type) {
        this.connection_type = connection_type;
    }


    /**
     * Gets the freeze_amount_str value for this Returnresult.
     * 
     * @return freeze_amount_str
     */
    public java.lang.String getFreeze_amount_str() {
        return freeze_amount_str;
    }


    /**
     * Sets the freeze_amount_str value for this Returnresult.
     * 
     * @param freeze_amount_str
     */
    public void setFreeze_amount_str(java.lang.String freeze_amount_str) {
        this.freeze_amount_str = freeze_amount_str;
    }


    /**
     * Gets the service_freeze_amount_str value for this Returnresult.
     * 
     * @return service_freeze_amount_str
     */
    public java.lang.String getService_freeze_amount_str() {
        return service_freeze_amount_str;
    }


    /**
     * Sets the service_freeze_amount_str value for this Returnresult.
     * 
     * @param service_freeze_amount_str
     */
    public void setService_freeze_amount_str(java.lang.String service_freeze_amount_str) {
        this.service_freeze_amount_str = service_freeze_amount_str;
    }


    /**
     * Gets the username value for this Returnresult.
     * 
     * @return username
     */
    public java.lang.String getUsername() {
        return username;
    }


    /**
     * Sets the username value for this Returnresult.
     * 
     * @param username
     */
    public void setUsername(java.lang.String username) {
        this.username = username;
    }


    /**
     * Gets the service_type value for this Returnresult.
     * 
     * @return service_type
     */
    public java.lang.String getService_type() {
        return service_type;
    }


    /**
     * Sets the service_type value for this Returnresult.
     * 
     * @param service_type
     */
    public void setService_type(java.lang.String service_type) {
        this.service_type = service_type;
    }


    /**
     * Gets the password value for this Returnresult.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this Returnresult.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the status value for this Returnresult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Returnresult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the message value for this Returnresult.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this Returnresult.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }


    /**
     * Gets the dip value for this Returnresult.
     * 
     * @return dip
     */
    public java.lang.String getDip() {
        return dip;
    }


    /**
     * Sets the dip value for this Returnresult.
     * 
     * @param dip
     */
    public void setDip(java.lang.String dip) {
        this.dip = dip;
    }


    /**
     * Gets the trx_id value for this Returnresult.
     * 
     * @return trx_id
     */
    public java.lang.String getTrx_id() {
        return trx_id;
    }


    /**
     * Sets the trx_id value for this Returnresult.
     * 
     * @param trx_id
     */
    public void setTrx_id(java.lang.String trx_id) {
        this.trx_id = trx_id;
    }


    /**
     * Gets the bill_amount value for this Returnresult.
     * 
     * @return bill_amount
     */
    public java.lang.String getBill_amount() {
        return bill_amount;
    }


    /**
     * Sets the bill_amount value for this Returnresult.
     * 
     * @param bill_amount
     */
    public void setBill_amount(java.lang.String bill_amount) {
        this.bill_amount = bill_amount;
    }


    /**
     * Gets the retailer_commission value for this Returnresult.
     * 
     * @return retailer_commission
     */
    public java.lang.String getRetailer_commission() {
        return retailer_commission;
    }


    /**
     * Sets the retailer_commission value for this Returnresult.
     * 
     * @param retailer_commission
     */
    public void setRetailer_commission(java.lang.String retailer_commission) {
        this.retailer_commission = retailer_commission;
    }


    /**
     * Gets the billAmount value for this Returnresult.
     * 
     * @return billAmount
     */
    public java.lang.String getBillAmount() {
        return billAmount;
    }


    /**
     * Sets the billAmount value for this Returnresult.
     * 
     * @param billAmount
     */
    public void setBillAmount(java.lang.String billAmount) {
        this.billAmount = billAmount;
    }


    /**
     * Gets the extraCharge value for this Returnresult.
     * 
     * @return extraCharge
     */
    public java.lang.String getExtraCharge() {
        return extraCharge;
    }


    /**
     * Sets the extraCharge value for this Returnresult.
     * 
     * @param extraCharge
     */
    public void setExtraCharge(java.lang.String extraCharge) {
        this.extraCharge = extraCharge;
    }


    /**
     * Gets the totalAmount value for this Returnresult.
     * 
     * @return totalAmount
     */
    public java.lang.String getTotalAmount() {
        return totalAmount;
    }


    /**
     * Sets the totalAmount value for this Returnresult.
     * 
     * @param totalAmount
     */
    public void setTotalAmount(java.lang.String totalAmount) {
        this.totalAmount = totalAmount;
    }


    /**
     * Gets the customerName value for this Returnresult.
     * 
     * @return customerName
     */
    public java.lang.String getCustomerName() {
        return customerName;
    }


    /**
     * Sets the customerName value for this Returnresult.
     * 
     * @param customerName
     */
    public void setCustomerName(java.lang.String customerName) {
        this.customerName = customerName;
    }


    /**
     * Gets the cutomerPhoneNumber value for this Returnresult.
     * 
     * @return cutomerPhoneNumber
     */
    public java.lang.String getCutomerPhoneNumber() {
        return cutomerPhoneNumber;
    }


    /**
     * Sets the cutomerPhoneNumber value for this Returnresult.
     * 
     * @param cutomerPhoneNumber
     */
    public void setCutomerPhoneNumber(java.lang.String cutomerPhoneNumber) {
        this.cutomerPhoneNumber = cutomerPhoneNumber;
    }


    /**
     * Gets the retailerCommission value for this Returnresult.
     * 
     * @return retailerCommission
     */
    public java.lang.String getRetailerCommission() {
        return retailerCommission;
    }


    /**
     * Sets the retailerCommission value for this Returnresult.
     * 
     * @param retailerCommission
     */
    public void setRetailerCommission(java.lang.String retailerCommission) {
        this.retailerCommission = retailerCommission;
    }


    /**
     * Gets the msg_text value for this Returnresult.
     * 
     * @return msg_text
     */
    public java.lang.String getMsg_text() {
        return msg_text;
    }


    /**
     * Sets the msg_text value for this Returnresult.
     * 
     * @param msg_text
     */
    public void setMsg_text(java.lang.String msg_text) {
        this.msg_text = msg_text;
    }


    /**
     * Gets the information value for this Returnresult.
     * 
     * @return information
     */
    public java.lang.String getInformation() {
        return information;
    }


    /**
     * Sets the information value for this Returnresult.
     * 
     * @param information
     */
    public void setInformation(java.lang.String information) {
        this.information = information;
    }


    /**
     * Gets the payment_number value for this Returnresult.
     * 
     * @return payment_number
     */
    public java.lang.String getPayment_number() {
        return payment_number;
    }


    /**
     * Sets the payment_number value for this Returnresult.
     * 
     * @param payment_number
     */
    public void setPayment_number(java.lang.String payment_number) {
        this.payment_number = payment_number;
    }


    /**
     * Gets the tbps_charge value for this Returnresult.
     * 
     * @return tbps_charge
     */
    public java.lang.String getTbps_charge() {
        return tbps_charge;
    }


    /**
     * Sets the tbps_charge value for this Returnresult.
     * 
     * @param tbps_charge
     */
    public void setTbps_charge(java.lang.String tbps_charge) {
        this.tbps_charge = tbps_charge;
    }


    /**
     * Gets the total_amount value for this Returnresult.
     * 
     * @return total_amount
     */
    public java.lang.String getTotal_amount() {
        return total_amount;
    }


    /**
     * Sets the total_amount value for this Returnresult.
     * 
     * @param total_amount
     */
    public void setTotal_amount(java.lang.String total_amount) {
        this.total_amount = total_amount;
    }


    /**
     * Gets the outlet_name value for this Returnresult.
     * 
     * @return outlet_name
     */
    public java.lang.String getOutlet_name() {
        return outlet_name;
    }


    /**
     * Sets the outlet_name value for this Returnresult.
     * 
     * @param outlet_name
     */
    public void setOutlet_name(java.lang.String outlet_name) {
        this.outlet_name = outlet_name;
    }


    /**
     * Gets the address value for this Returnresult.
     * 
     * @return address
     */
    public java.lang.String getAddress() {
        return address;
    }


    /**
     * Sets the address value for this Returnresult.
     * 
     * @param address
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }


    /**
     * Gets the hotline value for this Returnresult.
     * 
     * @return hotline
     */
    public java.lang.String getHotline() {
        return hotline;
    }


    /**
     * Sets the hotline value for this Returnresult.
     * 
     * @param hotline
     */
    public void setHotline(java.lang.String hotline) {
        this.hotline = hotline;
    }


    /**
     * Gets the retailer_code value for this Returnresult.
     * 
     * @return retailer_code
     */
    public java.lang.String getRetailer_code() {
        return retailer_code;
    }


    /**
     * Sets the retailer_code value for this Returnresult.
     * 
     * @param retailer_code
     */
    public void setRetailer_code(java.lang.String retailer_code) {
        this.retailer_code = retailer_code;
    }


    /**
     * Gets the noOfTrx value for this Returnresult.
     * 
     * @return noOfTrx
     */
    public java.lang.String getNoOfTrx() {
        return noOfTrx;
    }


    /**
     * Sets the noOfTrx value for this Returnresult.
     * 
     * @param noOfTrx
     */
    public void setNoOfTrx(java.lang.String noOfTrx) {
        this.noOfTrx = noOfTrx;
    }


    /**
     * Gets the trans_id value for this Returnresult.
     * 
     * @return trans_id
     */
    public java.lang.String getTrans_id() {
        return trans_id;
    }


    /**
     * Sets the trans_id value for this Returnresult.
     * 
     * @param trans_id
     */
    public void setTrans_id(java.lang.String trans_id) {
        this.trans_id = trans_id;
    }


    /**
     * Gets the balance value for this Returnresult.
     * 
     * @return balance
     */
    public java.lang.String getBalance() {
        return balance;
    }


    /**
     * Sets the balance value for this Returnresult.
     * 
     * @param balance
     */
    public void setBalance(java.lang.String balance) {
        this.balance = balance;
    }


    /**
     * Gets the frozen_balance value for this Returnresult.
     * 
     * @return frozen_balance
     */
    public java.lang.String getFrozen_balance() {
        return frozen_balance;
    }


    /**
     * Sets the frozen_balance value for this Returnresult.
     * 
     * @param frozen_balance
     */
    public void setFrozen_balance(java.lang.String frozen_balance) {
        this.frozen_balance = frozen_balance;
    }


    /**
     * Gets the purpose_balance value for this Returnresult.
     * 
     * @return purpose_balance
     */
    public java.lang.String getPurpose_balance() {
        return purpose_balance;
    }


    /**
     * Sets the purpose_balance value for this Returnresult.
     * 
     * @param purpose_balance
     */
    public void setPurpose_balance(java.lang.String purpose_balance) {
        this.purpose_balance = purpose_balance;
    }


    /**
     * Gets the payment_id value for this Returnresult.
     * 
     * @return payment_id
     */
    public java.lang.String getPayment_id() {
        return payment_id;
    }


    /**
     * Sets the payment_id value for this Returnresult.
     * 
     * @param payment_id
     */
    public void setPayment_id(java.lang.String payment_id) {
        this.payment_id = payment_id;
    }


    /**
     * Gets the payment_amount value for this Returnresult.
     * 
     * @return payment_amount
     */
    public java.lang.String getPayment_amount() {
        return payment_amount;
    }


    /**
     * Sets the payment_amount value for this Returnresult.
     * 
     * @param payment_amount
     */
    public void setPayment_amount(java.lang.String payment_amount) {
        this.payment_amount = payment_amount;
    }


    /**
     * Gets the payment_status value for this Returnresult.
     * 
     * @return payment_status
     */
    public java.lang.String getPayment_status() {
        return payment_status;
    }


    /**
     * Sets the payment_status value for this Returnresult.
     * 
     * @param payment_status
     */
    public void setPayment_status(java.lang.String payment_status) {
        this.payment_status = payment_status;
    }


    /**
     * Gets the payment_date value for this Returnresult.
     * 
     * @return payment_date
     */
    public java.lang.String getPayment_date() {
        return payment_date;
    }


    /**
     * Sets the payment_date value for this Returnresult.
     * 
     * @param payment_date
     */
    public void setPayment_date(java.lang.String payment_date) {
        this.payment_date = payment_date;
    }


    /**
     * Gets the sender value for this Returnresult.
     * 
     * @return sender
     */
    public java.lang.String getSender() {
        return sender;
    }


    /**
     * Sets the sender value for this Returnresult.
     * 
     * @param sender
     */
    public void setSender(java.lang.String sender) {
        this.sender = sender;
    }


    /**
     * Gets the trx_ref value for this Returnresult.
     * 
     * @return trx_ref
     */
    public java.lang.String getTrx_ref() {
        return trx_ref;
    }


    /**
     * Sets the trx_ref value for this Returnresult.
     * 
     * @param trx_ref
     */
    public void setTrx_ref(java.lang.String trx_ref) {
        this.trx_ref = trx_ref;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Returnresult)) return false;
        Returnresult other = (Returnresult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cwid==null && other.getCwid()==null) || 
             (this.cwid!=null &&
              this.cwid.equals(other.getCwid()))) &&
            ((this.status_code==null && other.getStatus_code()==null) || 
             (this.status_code!=null &&
              this.status_code.equals(other.getStatus_code()))) &&
            ((this.status_details==null && other.getStatus_details()==null) || 
             (this.status_details!=null &&
              this.status_details.equals(other.getStatus_details()))) &&
            ((this.telco_transaction_id==null && other.getTelco_transaction_id()==null) || 
             (this.telco_transaction_id!=null &&
              this.telco_transaction_id.equals(other.getTelco_transaction_id()))) &&
            ((this.transaction_id==null && other.getTransaction_id()==null) || 
             (this.transaction_id!=null &&
              this.transaction_id.equals(other.getTransaction_id()))) &&
            ((this.account_no==null && other.getAccount_no()==null) || 
             (this.account_no!=null &&
              this.account_no.equals(other.getAccount_no()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.connection_type==null && other.getConnection_type()==null) || 
             (this.connection_type!=null &&
              this.connection_type.equals(other.getConnection_type()))) &&
            ((this.freeze_amount_str==null && other.getFreeze_amount_str()==null) || 
             (this.freeze_amount_str!=null &&
              this.freeze_amount_str.equals(other.getFreeze_amount_str()))) &&
            ((this.service_freeze_amount_str==null && other.getService_freeze_amount_str()==null) || 
             (this.service_freeze_amount_str!=null &&
              this.service_freeze_amount_str.equals(other.getService_freeze_amount_str()))) &&
            ((this.username==null && other.getUsername()==null) || 
             (this.username!=null &&
              this.username.equals(other.getUsername()))) &&
            ((this.service_type==null && other.getService_type()==null) || 
             (this.service_type!=null &&
              this.service_type.equals(other.getService_type()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage()))) &&
            ((this.dip==null && other.getDip()==null) || 
             (this.dip!=null &&
              this.dip.equals(other.getDip()))) &&
            ((this.trx_id==null && other.getTrx_id()==null) || 
             (this.trx_id!=null &&
              this.trx_id.equals(other.getTrx_id()))) &&
            ((this.bill_amount==null && other.getBill_amount()==null) || 
             (this.bill_amount!=null &&
              this.bill_amount.equals(other.getBill_amount()))) &&
            ((this.retailer_commission==null && other.getRetailer_commission()==null) || 
             (this.retailer_commission!=null &&
              this.retailer_commission.equals(other.getRetailer_commission()))) &&
            ((this.billAmount==null && other.getBillAmount()==null) || 
             (this.billAmount!=null &&
              this.billAmount.equals(other.getBillAmount()))) &&
            ((this.extraCharge==null && other.getExtraCharge()==null) || 
             (this.extraCharge!=null &&
              this.extraCharge.equals(other.getExtraCharge()))) &&
            ((this.totalAmount==null && other.getTotalAmount()==null) || 
             (this.totalAmount!=null &&
              this.totalAmount.equals(other.getTotalAmount()))) &&
            ((this.customerName==null && other.getCustomerName()==null) || 
             (this.customerName!=null &&
              this.customerName.equals(other.getCustomerName()))) &&
            ((this.cutomerPhoneNumber==null && other.getCutomerPhoneNumber()==null) || 
             (this.cutomerPhoneNumber!=null &&
              this.cutomerPhoneNumber.equals(other.getCutomerPhoneNumber()))) &&
            ((this.retailerCommission==null && other.getRetailerCommission()==null) || 
             (this.retailerCommission!=null &&
              this.retailerCommission.equals(other.getRetailerCommission()))) &&
            ((this.msg_text==null && other.getMsg_text()==null) || 
             (this.msg_text!=null &&
              this.msg_text.equals(other.getMsg_text()))) &&
            ((this.information==null && other.getInformation()==null) || 
             (this.information!=null &&
              this.information.equals(other.getInformation()))) &&
            ((this.payment_number==null && other.getPayment_number()==null) || 
             (this.payment_number!=null &&
              this.payment_number.equals(other.getPayment_number()))) &&
            ((this.tbps_charge==null && other.getTbps_charge()==null) || 
             (this.tbps_charge!=null &&
              this.tbps_charge.equals(other.getTbps_charge()))) &&
            ((this.total_amount==null && other.getTotal_amount()==null) || 
             (this.total_amount!=null &&
              this.total_amount.equals(other.getTotal_amount()))) &&
            ((this.outlet_name==null && other.getOutlet_name()==null) || 
             (this.outlet_name!=null &&
              this.outlet_name.equals(other.getOutlet_name()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.hotline==null && other.getHotline()==null) || 
             (this.hotline!=null &&
              this.hotline.equals(other.getHotline()))) &&
            ((this.retailer_code==null && other.getRetailer_code()==null) || 
             (this.retailer_code!=null &&
              this.retailer_code.equals(other.getRetailer_code()))) &&
            ((this.noOfTrx==null && other.getNoOfTrx()==null) || 
             (this.noOfTrx!=null &&
              this.noOfTrx.equals(other.getNoOfTrx()))) &&
            ((this.trans_id==null && other.getTrans_id()==null) || 
             (this.trans_id!=null &&
              this.trans_id.equals(other.getTrans_id()))) &&
            ((this.balance==null && other.getBalance()==null) || 
             (this.balance!=null &&
              this.balance.equals(other.getBalance()))) &&
            ((this.frozen_balance==null && other.getFrozen_balance()==null) || 
             (this.frozen_balance!=null &&
              this.frozen_balance.equals(other.getFrozen_balance()))) &&
            ((this.purpose_balance==null && other.getPurpose_balance()==null) || 
             (this.purpose_balance!=null &&
              this.purpose_balance.equals(other.getPurpose_balance()))) &&
            ((this.payment_id==null && other.getPayment_id()==null) || 
             (this.payment_id!=null &&
              this.payment_id.equals(other.getPayment_id()))) &&
            ((this.payment_amount==null && other.getPayment_amount()==null) || 
             (this.payment_amount!=null &&
              this.payment_amount.equals(other.getPayment_amount()))) &&
            ((this.payment_status==null && other.getPayment_status()==null) || 
             (this.payment_status!=null &&
              this.payment_status.equals(other.getPayment_status()))) &&
            ((this.payment_date==null && other.getPayment_date()==null) || 
             (this.payment_date!=null &&
              this.payment_date.equals(other.getPayment_date()))) &&
            ((this.sender==null && other.getSender()==null) || 
             (this.sender!=null &&
              this.sender.equals(other.getSender()))) &&
            ((this.trx_ref==null && other.getTrx_ref()==null) || 
             (this.trx_ref!=null &&
              this.trx_ref.equals(other.getTrx_ref())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCwid() != null) {
            _hashCode += getCwid().hashCode();
        }
        if (getStatus_code() != null) {
            _hashCode += getStatus_code().hashCode();
        }
        if (getStatus_details() != null) {
            _hashCode += getStatus_details().hashCode();
        }
        if (getTelco_transaction_id() != null) {
            _hashCode += getTelco_transaction_id().hashCode();
        }
        if (getTransaction_id() != null) {
            _hashCode += getTransaction_id().hashCode();
        }
        if (getAccount_no() != null) {
            _hashCode += getAccount_no().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getConnection_type() != null) {
            _hashCode += getConnection_type().hashCode();
        }
        if (getFreeze_amount_str() != null) {
            _hashCode += getFreeze_amount_str().hashCode();
        }
        if (getService_freeze_amount_str() != null) {
            _hashCode += getService_freeze_amount_str().hashCode();
        }
        if (getUsername() != null) {
            _hashCode += getUsername().hashCode();
        }
        if (getService_type() != null) {
            _hashCode += getService_type().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        if (getDip() != null) {
            _hashCode += getDip().hashCode();
        }
        if (getTrx_id() != null) {
            _hashCode += getTrx_id().hashCode();
        }
        if (getBill_amount() != null) {
            _hashCode += getBill_amount().hashCode();
        }
        if (getRetailer_commission() != null) {
            _hashCode += getRetailer_commission().hashCode();
        }
        if (getBillAmount() != null) {
            _hashCode += getBillAmount().hashCode();
        }
        if (getExtraCharge() != null) {
            _hashCode += getExtraCharge().hashCode();
        }
        if (getTotalAmount() != null) {
            _hashCode += getTotalAmount().hashCode();
        }
        if (getCustomerName() != null) {
            _hashCode += getCustomerName().hashCode();
        }
        if (getCutomerPhoneNumber() != null) {
            _hashCode += getCutomerPhoneNumber().hashCode();
        }
        if (getRetailerCommission() != null) {
            _hashCode += getRetailerCommission().hashCode();
        }
        if (getMsg_text() != null) {
            _hashCode += getMsg_text().hashCode();
        }
        if (getInformation() != null) {
            _hashCode += getInformation().hashCode();
        }
        if (getPayment_number() != null) {
            _hashCode += getPayment_number().hashCode();
        }
        if (getTbps_charge() != null) {
            _hashCode += getTbps_charge().hashCode();
        }
        if (getTotal_amount() != null) {
            _hashCode += getTotal_amount().hashCode();
        }
        if (getOutlet_name() != null) {
            _hashCode += getOutlet_name().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getHotline() != null) {
            _hashCode += getHotline().hashCode();
        }
        if (getRetailer_code() != null) {
            _hashCode += getRetailer_code().hashCode();
        }
        if (getNoOfTrx() != null) {
            _hashCode += getNoOfTrx().hashCode();
        }
        if (getTrans_id() != null) {
            _hashCode += getTrans_id().hashCode();
        }
        if (getBalance() != null) {
            _hashCode += getBalance().hashCode();
        }
        if (getFrozen_balance() != null) {
            _hashCode += getFrozen_balance().hashCode();
        }
        if (getPurpose_balance() != null) {
            _hashCode += getPurpose_balance().hashCode();
        }
        if (getPayment_id() != null) {
            _hashCode += getPayment_id().hashCode();
        }
        if (getPayment_amount() != null) {
            _hashCode += getPayment_amount().hashCode();
        }
        if (getPayment_status() != null) {
            _hashCode += getPayment_status().hashCode();
        }
        if (getPayment_date() != null) {
            _hashCode += getPayment_date().hashCode();
        }
        if (getSender() != null) {
            _hashCode += getSender().hashCode();
        }
        if (getTrx_ref() != null) {
            _hashCode += getTrx_ref().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Returnresult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:rechargerequestquote", "Returnresult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cwid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cwid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telco_transaction_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "telco_transaction_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transaction_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transaction_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("account_no");
        elemField.setXmlName(new javax.xml.namespace.QName("", "account_no"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("connection_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "connection_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("freeze_amount_str");
        elemField.setXmlName(new javax.xml.namespace.QName("", "freeze_amount_str"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("service_freeze_amount_str");
        elemField.setXmlName(new javax.xml.namespace.QName("", "service_freeze_amount_str"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("username");
        elemField.setXmlName(new javax.xml.namespace.QName("", "username"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("service_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "service_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trx_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trx_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bill_amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bill_amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailer_commission");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retailer_commission"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "billAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraCharge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extraCharge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cutomerPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cutomerPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailerCommission");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retailerCommission"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msg_text");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msg_text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("information");
        elemField.setXmlName(new javax.xml.namespace.QName("", "information"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_number");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_number"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tbps_charge");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tbps_charge"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total_amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "total_amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outlet_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outlet_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("", "address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hotline");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hotline"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailer_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "retailer_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noOfTrx");
        elemField.setXmlName(new javax.xml.namespace.QName("", "noOfTrx"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trans_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trans_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("balance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("frozen_balance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "frozen_balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("purpose_balance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "purpose_balance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payment_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sender");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trx_ref");
        elemField.setXmlName(new javax.xml.namespace.QName("", "trx_ref"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
