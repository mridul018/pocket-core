/**
 * RechargerequestserverLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.paywell;

public class RechargerequestserverLocator extends org.apache.axis.client.Service implements tech.ipocket.pocket.wsdl.paywell.Rechargerequestserver {

    public RechargerequestserverLocator() {
    }


    public RechargerequestserverLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RechargerequestserverLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for rechargerequestserverPort
    private java.lang.String rechargerequestserverPort_address = "https://www.paywellonline.com:443/National_topupsoap/paywellservice.php";

    public java.lang.String getrechargerequestserverPortAddress() {
        return rechargerequestserverPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String rechargerequestserverPortWSDDServiceName = "rechargerequestserverPort";

    public java.lang.String getrechargerequestserverPortWSDDServiceName() {
        return rechargerequestserverPortWSDDServiceName;
    }

    public void setrechargerequestserverPortWSDDServiceName(java.lang.String name) {
        rechargerequestserverPortWSDDServiceName = name;
    }

    public tech.ipocket.pocket.wsdl.paywell.RechargerequestserverPortType getrechargerequestserverPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(rechargerequestserverPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getrechargerequestserverPort(endpoint);
    }

    public tech.ipocket.pocket.wsdl.paywell.RechargerequestserverPortType getrechargerequestserverPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            tech.ipocket.pocket.wsdl.paywell.RechargerequestserverBindingStub _stub = new tech.ipocket.pocket.wsdl.paywell.RechargerequestserverBindingStub(portAddress, this);
            _stub.setPortName(getrechargerequestserverPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setrechargerequestserverPortEndpointAddress(java.lang.String address) {
        rechargerequestserverPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (tech.ipocket.pocket.wsdl.paywell.RechargerequestserverPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                tech.ipocket.pocket.wsdl.paywell.RechargerequestserverBindingStub _stub = new tech.ipocket.pocket.wsdl.paywell.RechargerequestserverBindingStub(new java.net.URL(rechargerequestserverPort_address), this);
                _stub.setPortName(getrechargerequestserverPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("rechargerequestserverPort".equals(inputPortName)) {
            return getrechargerequestserverPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:rechargerequestquote", "rechargerequestserver");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:rechargerequestquote", "rechargerequestserverPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("rechargerequestserverPort".equals(portName)) {
            setrechargerequestserverPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
