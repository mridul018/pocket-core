/**
 * RechargerequestserverPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.paywell;

public interface RechargerequestserverPortType extends java.rmi.Remote {
    public tech.ipocket.pocket.wsdl.paywell.ResultObject1 initiateRecharge(java.lang.String clientid, java.lang.String clientpass, java.lang.String crid, java.lang.String msisdn, java.lang.String amount, java.lang.String connection, java.lang.String sender) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.ResultObject1 commitRecharge(java.lang.String cwid, java.lang.String clientid, java.lang.String clientpass) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.ResultObject1 cancelRecharge(java.lang.String cwid, java.lang.String clientid, java.lang.String clientpass) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.ResultObject queryRecharge(java.lang.String crid, java.lang.String clientid, java.lang.String clientpass) throws java.rmi.RemoteException;
    public java.lang.String queryBalance(java.lang.String clientid, java.lang.String clientpass) throws java.rmi.RemoteException;
    public java.lang.String checkBkashBalance(java.lang.String clientid, java.lang.String clientpass) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.ResultObject1 topup(java.lang.String clientid, java.lang.String clientpass, java.lang.String crid, java.lang.String msisdn, java.lang.String amount, java.lang.String connection, java.lang.String operator, java.lang.String sender) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.ResultObject1 wimax_topup(java.lang.String username, java.lang.String pincode, java.lang.String wimax, java.lang.String account_no, java.lang.String amount, java.lang.String connection_type) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.ResultObject1 wimax_enquery(java.lang.String transaction_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetWZPDCLEnquiryReturnresult getWZPDCLEnquiry(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo, java.lang.String billYearMonth) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.DoWZPDCLBillPayReturnresult doWZPDCLBillPay(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo, java.lang.String billYearMonth, java.lang.String trx_id, java.lang.String amount) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetKGDCLEnquiryReturnresult getKGDCLEnquiry(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.DoKGDCLBillPayReturnresult doKGDCLBillPay(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo, java.lang.String trans_id, java.lang.String amount) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetDPDCEnquiryReturnresult getDPDCEnquiry(java.lang.String username, java.lang.String password, java.lang.String customerNo, java.lang.String billYearMonth, java.lang.String location, java.lang.String payerMobileNo) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.DoDPDCBillPayReturnresult doDPDCBillPay(java.lang.String amount, java.lang.String username, java.lang.String password, java.lang.String customerNo, java.lang.String billYearMonth, java.lang.String location, java.lang.String payerMobileNo, java.lang.String trx_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetDPDCBillEnquiryReturnresult getDPDCBillEnquiry(java.lang.String username, java.lang.String payerMobileNoOrBillNo, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetDESCOBillEnquiryReturnresult getDESCOBillEnquiry(java.lang.String username, java.lang.String payerMobileNoOrBillNo, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetWASABillEnquiryReturnresult getWASABillEnquiry(java.lang.String username, java.lang.String payerMobileNoOrBillNo, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetWZPDCLBillEnquiryReturnresult getWZPDCLBillEnquiry(java.lang.String username, java.lang.String payerMobileNoOrBillNo, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetKGDCLBillEnquiryReturnresult getKGDCLBillEnquiry(java.lang.String username, java.lang.String payerMobileNoOrBillNo, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetDESCOEnquiryReturnresult getDESCOEnquiry(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.DoDESCOBillPayReturnresult doDESCOBillPay(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo, java.lang.String trx_id, java.lang.String amount) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.GetWASAEnquiryReturnresult getWASAEnquiry(java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.DoWASABillPayReturnresult doWASABillPay(java.lang.String amount, java.lang.String username, java.lang.String password, java.lang.String billNo, java.lang.String payerMobileNo, java.lang.String trx_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.Returnresult getIvacCenter(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.Returnresult IVACRequest(java.lang.String username, java.lang.String password, java.lang.String amount, java.lang.String web_file_no, java.lang.String passport_no, java.lang.String center_no, java.lang.String cust_mobile) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.Returnresult getIvacCenterSandbox(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.Returnresult IVACRequestSandbox(java.lang.String username, java.lang.String password, java.lang.String amount, java.lang.String web_file_no, java.lang.String passport_no, java.lang.String center_no, java.lang.String cust_mobile) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.Returnresult getIvacTrxList(java.lang.String username, java.lang.String password, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult_1 pollybuddutPhoneNumberChange(java.lang.String username, java.lang.String password, java.lang.String accountNo, java.lang.String msisdn) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult_1 pollybuddutBillStatus(java.lang.String username, java.lang.String password, java.lang.String accountNo, java.lang.String month, java.lang.String year) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult_1 pollybuddutRegistrationInitiate(java.lang.String username, java.lang.String password, java.lang.String accountNo, java.lang.String customerName, java.lang.String customerPhoneNumber) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult_sentPollybuddutRegistrationOTP sentPollybuddutRegistrationOTP(java.lang.String username, java.lang.String password, java.lang.String accountNo, java.lang.String customerName, java.lang.String customerPhoneNumber, java.lang.String otp) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult_pollybiddutBillPay pollybiddutBillPay(java.lang.String username, java.lang.String password, java.lang.String bill_no, java.lang.String ref_id, java.lang.String amount, java.lang.String smsAccountNumber, java.lang.String coustomerPhoneNumber) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 pollyBuddutBillPayInquiry(java.lang.String username, java.lang.String password, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 pollyBuddutNumberChangeInquiry(java.lang.String username, java.lang.String password, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 pollyBuddutRegistrationInquiry(java.lang.String username, java.lang.String password, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 pollyBuddutBillStatusInquiry(java.lang.String username, java.lang.String password, java.lang.String limit) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 getPollyBillEnquiryData(java.lang.String username, java.lang.String password, java.lang.String ref_id, java.lang.String bill_no) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 getPollyBillRegEnquiryData(java.lang.String username, java.lang.String password, java.lang.String ref_id, java.lang.String sms_account_no) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.RetrunResult2 getPollyPhoneNoChangeAndBillStatusData(java.lang.String username, java.lang.String password, java.lang.String ref_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.paywell.PollybuddutRegistrationRetrunResult pollybuddutRegistration(java.lang.String username, java.lang.String password, java.lang.String accountNo, java.lang.String customerPhoneNumber, java.lang.String customerName) throws java.rmi.RemoteException;
}
