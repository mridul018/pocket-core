/**
 * ResultObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.paywell;

public class ResultObject  implements java.io.Serializable {
    private java.lang.String cwid;

    private java.lang.String status_code;

    private java.lang.String status_details;

    private java.lang.String mobile_number;

    private java.lang.String amount;

    private java.lang.String last_update_time;

    public ResultObject() {
    }

    public ResultObject(
           java.lang.String cwid,
           java.lang.String status_code,
           java.lang.String status_details,
           java.lang.String mobile_number,
           java.lang.String amount,
           java.lang.String last_update_time) {
           this.cwid = cwid;
           this.status_code = status_code;
           this.status_details = status_details;
           this.mobile_number = mobile_number;
           this.amount = amount;
           this.last_update_time = last_update_time;
    }


    /**
     * Gets the cwid value for this ResultObject.
     * 
     * @return cwid
     */
    public java.lang.String getCwid() {
        return cwid;
    }


    /**
     * Sets the cwid value for this ResultObject.
     * 
     * @param cwid
     */
    public void setCwid(java.lang.String cwid) {
        this.cwid = cwid;
    }


    /**
     * Gets the status_code value for this ResultObject.
     * 
     * @return status_code
     */
    public java.lang.String getStatus_code() {
        return status_code;
    }


    /**
     * Sets the status_code value for this ResultObject.
     * 
     * @param status_code
     */
    public void setStatus_code(java.lang.String status_code) {
        this.status_code = status_code;
    }


    /**
     * Gets the status_details value for this ResultObject.
     * 
     * @return status_details
     */
    public java.lang.String getStatus_details() {
        return status_details;
    }


    /**
     * Sets the status_details value for this ResultObject.
     * 
     * @param status_details
     */
    public void setStatus_details(java.lang.String status_details) {
        this.status_details = status_details;
    }


    /**
     * Gets the mobile_number value for this ResultObject.
     * 
     * @return mobile_number
     */
    public java.lang.String getMobile_number() {
        return mobile_number;
    }


    /**
     * Sets the mobile_number value for this ResultObject.
     * 
     * @param mobile_number
     */
    public void setMobile_number(java.lang.String mobile_number) {
        this.mobile_number = mobile_number;
    }


    /**
     * Gets the amount value for this ResultObject.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this ResultObject.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the last_update_time value for this ResultObject.
     * 
     * @return last_update_time
     */
    public java.lang.String getLast_update_time() {
        return last_update_time;
    }


    /**
     * Sets the last_update_time value for this ResultObject.
     * 
     * @param last_update_time
     */
    public void setLast_update_time(java.lang.String last_update_time) {
        this.last_update_time = last_update_time;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResultObject)) return false;
        ResultObject other = (ResultObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cwid==null && other.getCwid()==null) || 
             (this.cwid!=null &&
              this.cwid.equals(other.getCwid()))) &&
            ((this.status_code==null && other.getStatus_code()==null) || 
             (this.status_code!=null &&
              this.status_code.equals(other.getStatus_code()))) &&
            ((this.status_details==null && other.getStatus_details()==null) || 
             (this.status_details!=null &&
              this.status_details.equals(other.getStatus_details()))) &&
            ((this.mobile_number==null && other.getMobile_number()==null) || 
             (this.mobile_number!=null &&
              this.mobile_number.equals(other.getMobile_number()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.last_update_time==null && other.getLast_update_time()==null) || 
             (this.last_update_time!=null &&
              this.last_update_time.equals(other.getLast_update_time())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCwid() != null) {
            _hashCode += getCwid().hashCode();
        }
        if (getStatus_code() != null) {
            _hashCode += getStatus_code().hashCode();
        }
        if (getStatus_details() != null) {
            _hashCode += getStatus_details().hashCode();
        }
        if (getMobile_number() != null) {
            _hashCode += getMobile_number().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getLast_update_time() != null) {
            _hashCode += getLast_update_time().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResultObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:rechargerequestquote", "ResultObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cwid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cwid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status_details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status_details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mobile_number");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mobile_number"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_update_time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "last_update_time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
