/**
 * OperatorLimitsDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class OperatorLimitsDataType  implements java.io.Serializable {
    private java.math.BigInteger operator_id;

    private java.math.BigInteger prepaid_low_limit;

    private java.math.BigInteger prepaid_high_limit;

    private java.math.BigInteger postpaid_low_limit;

    private java.math.BigInteger postpaid_high_limit;

    private java.math.BigInteger prepaid_time_limit;

    private java.math.BigInteger postpaid_time_limit;

    public OperatorLimitsDataType() {
    }

    public OperatorLimitsDataType(
           java.math.BigInteger operator_id,
           java.math.BigInteger prepaid_low_limit,
           java.math.BigInteger prepaid_high_limit,
           java.math.BigInteger postpaid_low_limit,
           java.math.BigInteger postpaid_high_limit,
           java.math.BigInteger prepaid_time_limit,
           java.math.BigInteger postpaid_time_limit) {
           this.operator_id = operator_id;
           this.prepaid_low_limit = prepaid_low_limit;
           this.prepaid_high_limit = prepaid_high_limit;
           this.postpaid_low_limit = postpaid_low_limit;
           this.postpaid_high_limit = postpaid_high_limit;
           this.prepaid_time_limit = prepaid_time_limit;
           this.postpaid_time_limit = postpaid_time_limit;
    }


    /**
     * Gets the operator_id value for this OperatorLimitsDataType.
     * 
     * @return operator_id
     */
    public java.math.BigInteger getOperator_id() {
        return operator_id;
    }


    /**
     * Sets the operator_id value for this OperatorLimitsDataType.
     * 
     * @param operator_id
     */
    public void setOperator_id(java.math.BigInteger operator_id) {
        this.operator_id = operator_id;
    }


    /**
     * Gets the prepaid_low_limit value for this OperatorLimitsDataType.
     * 
     * @return prepaid_low_limit
     */
    public java.math.BigInteger getPrepaid_low_limit() {
        return prepaid_low_limit;
    }


    /**
     * Sets the prepaid_low_limit value for this OperatorLimitsDataType.
     * 
     * @param prepaid_low_limit
     */
    public void setPrepaid_low_limit(java.math.BigInteger prepaid_low_limit) {
        this.prepaid_low_limit = prepaid_low_limit;
    }


    /**
     * Gets the prepaid_high_limit value for this OperatorLimitsDataType.
     * 
     * @return prepaid_high_limit
     */
    public java.math.BigInteger getPrepaid_high_limit() {
        return prepaid_high_limit;
    }


    /**
     * Sets the prepaid_high_limit value for this OperatorLimitsDataType.
     * 
     * @param prepaid_high_limit
     */
    public void setPrepaid_high_limit(java.math.BigInteger prepaid_high_limit) {
        this.prepaid_high_limit = prepaid_high_limit;
    }


    /**
     * Gets the postpaid_low_limit value for this OperatorLimitsDataType.
     * 
     * @return postpaid_low_limit
     */
    public java.math.BigInteger getPostpaid_low_limit() {
        return postpaid_low_limit;
    }


    /**
     * Sets the postpaid_low_limit value for this OperatorLimitsDataType.
     * 
     * @param postpaid_low_limit
     */
    public void setPostpaid_low_limit(java.math.BigInteger postpaid_low_limit) {
        this.postpaid_low_limit = postpaid_low_limit;
    }


    /**
     * Gets the postpaid_high_limit value for this OperatorLimitsDataType.
     * 
     * @return postpaid_high_limit
     */
    public java.math.BigInteger getPostpaid_high_limit() {
        return postpaid_high_limit;
    }


    /**
     * Sets the postpaid_high_limit value for this OperatorLimitsDataType.
     * 
     * @param postpaid_high_limit
     */
    public void setPostpaid_high_limit(java.math.BigInteger postpaid_high_limit) {
        this.postpaid_high_limit = postpaid_high_limit;
    }


    /**
     * Gets the prepaid_time_limit value for this OperatorLimitsDataType.
     * 
     * @return prepaid_time_limit
     */
    public java.math.BigInteger getPrepaid_time_limit() {
        return prepaid_time_limit;
    }


    /**
     * Sets the prepaid_time_limit value for this OperatorLimitsDataType.
     * 
     * @param prepaid_time_limit
     */
    public void setPrepaid_time_limit(java.math.BigInteger prepaid_time_limit) {
        this.prepaid_time_limit = prepaid_time_limit;
    }


    /**
     * Gets the postpaid_time_limit value for this OperatorLimitsDataType.
     * 
     * @return postpaid_time_limit
     */
    public java.math.BigInteger getPostpaid_time_limit() {
        return postpaid_time_limit;
    }


    /**
     * Sets the postpaid_time_limit value for this OperatorLimitsDataType.
     * 
     * @param postpaid_time_limit
     */
    public void setPostpaid_time_limit(java.math.BigInteger postpaid_time_limit) {
        this.postpaid_time_limit = postpaid_time_limit;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof OperatorLimitsDataType)) return false;
        OperatorLimitsDataType other = (OperatorLimitsDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.operator_id==null && other.getOperator_id()==null) ||
             (this.operator_id!=null &&
              this.operator_id.equals(other.getOperator_id()))) &&
            ((this.prepaid_low_limit==null && other.getPrepaid_low_limit()==null) ||
             (this.prepaid_low_limit!=null &&
              this.prepaid_low_limit.equals(other.getPrepaid_low_limit()))) &&
            ((this.prepaid_high_limit==null && other.getPrepaid_high_limit()==null) ||
             (this.prepaid_high_limit!=null &&
              this.prepaid_high_limit.equals(other.getPrepaid_high_limit()))) &&
            ((this.postpaid_low_limit==null && other.getPostpaid_low_limit()==null) ||
             (this.postpaid_low_limit!=null &&
              this.postpaid_low_limit.equals(other.getPostpaid_low_limit()))) &&
            ((this.postpaid_high_limit==null && other.getPostpaid_high_limit()==null) ||
             (this.postpaid_high_limit!=null &&
              this.postpaid_high_limit.equals(other.getPostpaid_high_limit()))) &&
            ((this.prepaid_time_limit==null && other.getPrepaid_time_limit()==null) ||
             (this.prepaid_time_limit!=null &&
              this.prepaid_time_limit.equals(other.getPrepaid_time_limit()))) &&
            ((this.postpaid_time_limit==null && other.getPostpaid_time_limit()==null) ||
             (this.postpaid_time_limit!=null &&
              this.postpaid_time_limit.equals(other.getPostpaid_time_limit())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOperator_id() != null) {
            _hashCode += getOperator_id().hashCode();
        }
        if (getPrepaid_low_limit() != null) {
            _hashCode += getPrepaid_low_limit().hashCode();
        }
        if (getPrepaid_high_limit() != null) {
            _hashCode += getPrepaid_high_limit().hashCode();
        }
        if (getPostpaid_low_limit() != null) {
            _hashCode += getPostpaid_low_limit().hashCode();
        }
        if (getPostpaid_high_limit() != null) {
            _hashCode += getPostpaid_high_limit().hashCode();
        }
        if (getPrepaid_time_limit() != null) {
            _hashCode += getPrepaid_time_limit().hashCode();
        }
        if (getPostpaid_time_limit() != null) {
            _hashCode += getPostpaid_time_limit().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OperatorLimitsDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "OperatorLimitsDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prepaid_low_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prepaid_low_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prepaid_high_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prepaid_high_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postpaid_low_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "postpaid_low_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postpaid_high_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "postpaid_high_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prepaid_time_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prepaid_time_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postpaid_time_limit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "postpaid_time_limit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
