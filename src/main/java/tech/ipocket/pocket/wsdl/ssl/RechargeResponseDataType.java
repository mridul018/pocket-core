/**
 * RechargeResponseDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class RechargeResponseDataType  implements java.io.Serializable {
    private String vr_guid;

    private java.math.BigInteger recharge_status;

    private String message;

    public RechargeResponseDataType() {
    }

    public RechargeResponseDataType(
           String vr_guid,
           java.math.BigInteger recharge_status,
           String message) {
           this.vr_guid = vr_guid;
           this.recharge_status = recharge_status;
           this.message = message;
    }


    /**
     * Gets the vr_guid value for this RechargeResponseDataType.
     *
     * @return vr_guid
     */
    public String getVr_guid() {
        return vr_guid;
    }


    /**
     * Sets the vr_guid value for this RechargeResponseDataType.
     *
     * @param vr_guid
     */
    public void setVr_guid(String vr_guid) {
        this.vr_guid = vr_guid;
    }


    /**
     * Gets the recharge_status value for this RechargeResponseDataType.
     *
     * @return recharge_status
     */
    public java.math.BigInteger getRecharge_status() {
        return recharge_status;
    }


    /**
     * Sets the recharge_status value for this RechargeResponseDataType.
     *
     * @param recharge_status
     */
    public void setRecharge_status(java.math.BigInteger recharge_status) {
        this.recharge_status = recharge_status;
    }


    /**
     * Gets the message value for this RechargeResponseDataType.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this RechargeResponseDataType.
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof RechargeResponseDataType)) return false;
        RechargeResponseDataType other = (RechargeResponseDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.vr_guid==null && other.getVr_guid()==null) ||
             (this.vr_guid!=null &&
              this.vr_guid.equals(other.getVr_guid()))) &&
            ((this.recharge_status==null && other.getRecharge_status()==null) ||
             (this.recharge_status!=null &&
              this.recharge_status.equals(other.getRecharge_status()))) &&
            ((this.message==null && other.getMessage()==null) ||
             (this.message!=null &&
              this.message.equals(other.getMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getVr_guid() != null) {
            _hashCode += getVr_guid().hashCode();
        }
        if (getRecharge_status() != null) {
            _hashCode += getRecharge_status().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RechargeResponseDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "RechargeResponseDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vr_guid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vr_guid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recharge_status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recharge_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
