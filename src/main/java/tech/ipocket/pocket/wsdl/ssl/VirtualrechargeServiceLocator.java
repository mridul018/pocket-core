/**
 * VirtualrechargeServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class VirtualrechargeServiceLocator extends org.apache.axis.client.Service implements tech.ipocket.pocket.wsdl.ssl.VirtualrechargeService {

    public VirtualrechargeServiceLocator() {
    }


    public VirtualrechargeServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VirtualrechargeServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for virtualrechargePort
    private String virtualrechargePort_address = "http://103.26.139.140/virtualrecharge/service/index.php";

    public String getvirtualrechargePortAddress() {
        return virtualrechargePort_address;
    }

    // The WSDD service name defaults to the port name.
    private String virtualrechargePortWSDDServiceName = "virtualrechargePort";

    public String getvirtualrechargePortWSDDServiceName() {
        return virtualrechargePortWSDDServiceName;
    }

    public void setvirtualrechargePortWSDDServiceName(String name) {
        virtualrechargePortWSDDServiceName = name;
    }

    public tech.ipocket.pocket.wsdl.ssl.VirtualrechargePortType getvirtualrechargePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(virtualrechargePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getvirtualrechargePort(endpoint);
    }

    public tech.ipocket.pocket.wsdl.ssl.VirtualrechargePortType getvirtualrechargePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            tech.ipocket.pocket.wsdl.ssl.VirtualrechargeBindingStub _stub = new tech.ipocket.pocket.wsdl.ssl.VirtualrechargeBindingStub(portAddress, this);
            _stub.setPortName(getvirtualrechargePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setvirtualrechargePortEndpointAddress(String address) {
        virtualrechargePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (tech.ipocket.pocket.wsdl.ssl.VirtualrechargePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                tech.ipocket.pocket.wsdl.ssl.VirtualrechargeBindingStub _stub = new tech.ipocket.pocket.wsdl.ssl.VirtualrechargeBindingStub(new java.net.URL(virtualrechargePort_address), this);
                _stub.setPortName(getvirtualrechargePortWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("virtualrechargePort".equals(inputPortName)) {
            return getvirtualrechargePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "virtualrechargeService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "virtualrechargePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {

if ("virtualrechargePort".equals(portName)) {
            setvirtualrechargePortEndpointAddress(address);
        }
        else
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
