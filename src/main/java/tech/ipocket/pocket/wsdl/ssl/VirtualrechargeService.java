/**
 * VirtualrechargeService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public interface VirtualrechargeService extends javax.xml.rpc.Service {
    public String getvirtualrechargePortAddress();

    public tech.ipocket.pocket.wsdl.ssl.VirtualrechargePortType getvirtualrechargePort() throws javax.xml.rpc.ServiceException;

    public tech.ipocket.pocket.wsdl.ssl.VirtualrechargePortType getvirtualrechargePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
