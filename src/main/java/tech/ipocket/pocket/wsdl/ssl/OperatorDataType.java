/**
 * OperatorDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class OperatorDataType  implements java.io.Serializable {
    private int operator_id;

    private String operator_name;

    private String msisdn_prefix;

    private int active_status;

    public OperatorDataType() {
    }

    public OperatorDataType(
           int operator_id,
           String operator_name,
           String msisdn_prefix,
           int active_status) {
           this.operator_id = operator_id;
           this.operator_name = operator_name;
           this.msisdn_prefix = msisdn_prefix;
           this.active_status = active_status;
    }


    /**
     * Gets the operator_id value for this OperatorDataType.
     *
     * @return operator_id
     */
    public int getOperator_id() {
        return operator_id;
    }


    /**
     * Sets the operator_id value for this OperatorDataType.
     *
     * @param operator_id
     */
    public void setOperator_id(int operator_id) {
        this.operator_id = operator_id;
    }


    /**
     * Gets the operator_name value for this OperatorDataType.
     *
     * @return operator_name
     */
    public String getOperator_name() {
        return operator_name;
    }


    /**
     * Sets the operator_name value for this OperatorDataType.
     *
     * @param operator_name
     */
    public void setOperator_name(String operator_name) {
        this.operator_name = operator_name;
    }


    /**
     * Gets the msisdn_prefix value for this OperatorDataType.
     *
     * @return msisdn_prefix
     */
    public String getMsisdn_prefix() {
        return msisdn_prefix;
    }


    /**
     * Sets the msisdn_prefix value for this OperatorDataType.
     *
     * @param msisdn_prefix
     */
    public void setMsisdn_prefix(String msisdn_prefix) {
        this.msisdn_prefix = msisdn_prefix;
    }


    /**
     * Gets the active_status value for this OperatorDataType.
     *
     * @return active_status
     */
    public int getActive_status() {
        return active_status;
    }


    /**
     * Sets the active_status value for this OperatorDataType.
     *
     * @param active_status
     */
    public void setActive_status(int active_status) {
        this.active_status = active_status;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof OperatorDataType)) return false;
        OperatorDataType other = (OperatorDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            this.operator_id == other.getOperator_id() &&
            ((this.operator_name==null && other.getOperator_name()==null) ||
             (this.operator_name!=null &&
              this.operator_name.equals(other.getOperator_name()))) &&
            ((this.msisdn_prefix==null && other.getMsisdn_prefix()==null) ||
             (this.msisdn_prefix!=null &&
              this.msisdn_prefix.equals(other.getMsisdn_prefix()))) &&
            this.active_status == other.getActive_status();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getOperator_id();
        if (getOperator_name() != null) {
            _hashCode += getOperator_name().hashCode();
        }
        if (getMsisdn_prefix() != null) {
            _hashCode += getMsisdn_prefix().hashCode();
        }
        _hashCode += getActive_status();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OperatorDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "OperatorDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msisdn_prefix");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msisdn_prefix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("active_status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "active_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
