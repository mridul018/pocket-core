/**
 * ClientBalanceDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class ClientBalanceDataType  implements java.io.Serializable {
    private String client_user_id;

    private String available_credit;

    private String last_updated_time;

    public ClientBalanceDataType() {
    }

    public ClientBalanceDataType(
           String client_user_id,
           String available_credit,
           String last_updated_time) {
           this.client_user_id = client_user_id;
           this.available_credit = available_credit;
           this.last_updated_time = last_updated_time;
    }


    /**
     * Gets the client_user_id value for this ClientBalanceDataType.
     *
     * @return client_user_id
     */
    public String getClient_user_id() {
        return client_user_id;
    }


    /**
     * Sets the client_user_id value for this ClientBalanceDataType.
     *
     * @param client_user_id
     */
    public void setClient_user_id(String client_user_id) {
        this.client_user_id = client_user_id;
    }


    /**
     * Gets the available_credit value for this ClientBalanceDataType.
     *
     * @return available_credit
     */
    public String getAvailable_credit() {
        return available_credit;
    }


    /**
     * Sets the available_credit value for this ClientBalanceDataType.
     *
     * @param available_credit
     */
    public void setAvailable_credit(String available_credit) {
        this.available_credit = available_credit;
    }


    /**
     * Gets the last_updated_time value for this ClientBalanceDataType.
     *
     * @return last_updated_time
     */
    public String getLast_updated_time() {
        return last_updated_time;
    }


    /**
     * Sets the last_updated_time value for this ClientBalanceDataType.
     *
     * @param last_updated_time
     */
    public void setLast_updated_time(String last_updated_time) {
        this.last_updated_time = last_updated_time;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ClientBalanceDataType)) return false;
        ClientBalanceDataType other = (ClientBalanceDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.client_user_id==null && other.getClient_user_id()==null) ||
             (this.client_user_id!=null &&
              this.client_user_id.equals(other.getClient_user_id()))) &&
            ((this.available_credit==null && other.getAvailable_credit()==null) ||
             (this.available_credit!=null &&
              this.available_credit.equals(other.getAvailable_credit()))) &&
            ((this.last_updated_time==null && other.getLast_updated_time()==null) ||
             (this.last_updated_time!=null &&
              this.last_updated_time.equals(other.getLast_updated_time())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClient_user_id() != null) {
            _hashCode += getClient_user_id().hashCode();
        }
        if (getAvailable_credit() != null) {
            _hashCode += getAvailable_credit().hashCode();
        }
        if (getLast_updated_time() != null) {
            _hashCode += getLast_updated_time().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientBalanceDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "ClientBalanceDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client_user_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client_user_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("available_credit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "available_credit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_updated_time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "last_updated_time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
