/**
 * RechargeRequestDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class RechargeRequestDataType  implements java.io.Serializable {
    private String guid;

    private String vr_guid;

    private String request_time;

    private java.math.BigInteger operator_id;

    private String recipient_msisdn;

    private String connection_type;

    private java.math.BigInteger amount;

    private String init_time;

    private String cancel_time;

    private String recharge_date;

    private java.math.BigInteger recharge_status;

    private String message;

    public RechargeRequestDataType() {
    }

    public RechargeRequestDataType(
           String guid,
           String vr_guid,
           String request_time,
           java.math.BigInteger operator_id,
           String recipient_msisdn,
           String connection_type,
           java.math.BigInteger amount,
           String init_time,
           String cancel_time,
           String recharge_date,
           java.math.BigInteger recharge_status,
           String message) {
           this.guid = guid;
           this.vr_guid = vr_guid;
           this.request_time = request_time;
           this.operator_id = operator_id;
           this.recipient_msisdn = recipient_msisdn;
           this.connection_type = connection_type;
           this.amount = amount;
           this.init_time = init_time;
           this.cancel_time = cancel_time;
           this.recharge_date = recharge_date;
           this.recharge_status = recharge_status;
           this.message = message;
    }


    /**
     * Gets the guid value for this RechargeRequestDataType.
     *
     * @return guid
     */
    public String getGuid() {
        return guid;
    }


    /**
     * Sets the guid value for this RechargeRequestDataType.
     *
     * @param guid
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }


    /**
     * Gets the vr_guid value for this RechargeRequestDataType.
     *
     * @return vr_guid
     */
    public String getVr_guid() {
        return vr_guid;
    }


    /**
     * Sets the vr_guid value for this RechargeRequestDataType.
     *
     * @param vr_guid
     */
    public void setVr_guid(String vr_guid) {
        this.vr_guid = vr_guid;
    }


    /**
     * Gets the request_time value for this RechargeRequestDataType.
     *
     * @return request_time
     */
    public String getRequest_time() {
        return request_time;
    }


    /**
     * Sets the request_time value for this RechargeRequestDataType.
     *
     * @param request_time
     */
    public void setRequest_time(String request_time) {
        this.request_time = request_time;
    }


    /**
     * Gets the operator_id value for this RechargeRequestDataType.
     *
     * @return operator_id
     */
    public java.math.BigInteger getOperator_id() {
        return operator_id;
    }


    /**
     * Sets the operator_id value for this RechargeRequestDataType.
     *
     * @param operator_id
     */
    public void setOperator_id(java.math.BigInteger operator_id) {
        this.operator_id = operator_id;
    }


    /**
     * Gets the recipient_msisdn value for this RechargeRequestDataType.
     *
     * @return recipient_msisdn
     */
    public String getRecipient_msisdn() {
        return recipient_msisdn;
    }


    /**
     * Sets the recipient_msisdn value for this RechargeRequestDataType.
     *
     * @param recipient_msisdn
     */
    public void setRecipient_msisdn(String recipient_msisdn) {
        this.recipient_msisdn = recipient_msisdn;
    }


    /**
     * Gets the connection_type value for this RechargeRequestDataType.
     *
     * @return connection_type
     */
    public String getConnection_type() {
        return connection_type;
    }


    /**
     * Sets the connection_type value for this RechargeRequestDataType.
     *
     * @param connection_type
     */
    public void setConnection_type(String connection_type) {
        this.connection_type = connection_type;
    }


    /**
     * Gets the amount value for this RechargeRequestDataType.
     *
     * @return amount
     */
    public java.math.BigInteger getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this RechargeRequestDataType.
     *
     * @param amount
     */
    public void setAmount(java.math.BigInteger amount) {
        this.amount = amount;
    }


    /**
     * Gets the init_time value for this RechargeRequestDataType.
     *
     * @return init_time
     */
    public String getInit_time() {
        return init_time;
    }


    /**
     * Sets the init_time value for this RechargeRequestDataType.
     *
     * @param init_time
     */
    public void setInit_time(String init_time) {
        this.init_time = init_time;
    }


    /**
     * Gets the cancel_time value for this RechargeRequestDataType.
     *
     * @return cancel_time
     */
    public String getCancel_time() {
        return cancel_time;
    }


    /**
     * Sets the cancel_time value for this RechargeRequestDataType.
     *
     * @param cancel_time
     */
    public void setCancel_time(String cancel_time) {
        this.cancel_time = cancel_time;
    }


    /**
     * Gets the recharge_date value for this RechargeRequestDataType.
     *
     * @return recharge_date
     */
    public String getRecharge_date() {
        return recharge_date;
    }


    /**
     * Sets the recharge_date value for this RechargeRequestDataType.
     *
     * @param recharge_date
     */
    public void setRecharge_date(String recharge_date) {
        this.recharge_date = recharge_date;
    }


    /**
     * Gets the recharge_status value for this RechargeRequestDataType.
     *
     * @return recharge_status
     */
    public java.math.BigInteger getRecharge_status() {
        return recharge_status;
    }


    /**
     * Sets the recharge_status value for this RechargeRequestDataType.
     *
     * @param recharge_status
     */
    public void setRecharge_status(java.math.BigInteger recharge_status) {
        this.recharge_status = recharge_status;
    }


    /**
     * Gets the message value for this RechargeRequestDataType.
     *
     * @return message
     */
    public String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this RechargeRequestDataType.
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof RechargeRequestDataType)) return false;
        RechargeRequestDataType other = (RechargeRequestDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.guid==null && other.getGuid()==null) ||
             (this.guid!=null &&
              this.guid.equals(other.getGuid()))) &&
            ((this.vr_guid==null && other.getVr_guid()==null) ||
             (this.vr_guid!=null &&
              this.vr_guid.equals(other.getVr_guid()))) &&
            ((this.request_time==null && other.getRequest_time()==null) ||
             (this.request_time!=null &&
              this.request_time.equals(other.getRequest_time()))) &&
            ((this.operator_id==null && other.getOperator_id()==null) ||
             (this.operator_id!=null &&
              this.operator_id.equals(other.getOperator_id()))) &&
            ((this.recipient_msisdn==null && other.getRecipient_msisdn()==null) ||
             (this.recipient_msisdn!=null &&
              this.recipient_msisdn.equals(other.getRecipient_msisdn()))) &&
            ((this.connection_type==null && other.getConnection_type()==null) ||
             (this.connection_type!=null &&
              this.connection_type.equals(other.getConnection_type()))) &&
            ((this.amount==null && other.getAmount()==null) ||
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.init_time==null && other.getInit_time()==null) ||
             (this.init_time!=null &&
              this.init_time.equals(other.getInit_time()))) &&
            ((this.cancel_time==null && other.getCancel_time()==null) ||
             (this.cancel_time!=null &&
              this.cancel_time.equals(other.getCancel_time()))) &&
            ((this.recharge_date==null && other.getRecharge_date()==null) ||
             (this.recharge_date!=null &&
              this.recharge_date.equals(other.getRecharge_date()))) &&
            ((this.recharge_status==null && other.getRecharge_status()==null) ||
             (this.recharge_status!=null &&
              this.recharge_status.equals(other.getRecharge_status()))) &&
            ((this.message==null && other.getMessage()==null) ||
             (this.message!=null &&
              this.message.equals(other.getMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGuid() != null) {
            _hashCode += getGuid().hashCode();
        }
        if (getVr_guid() != null) {
            _hashCode += getVr_guid().hashCode();
        }
        if (getRequest_time() != null) {
            _hashCode += getRequest_time().hashCode();
        }
        if (getOperator_id() != null) {
            _hashCode += getOperator_id().hashCode();
        }
        if (getRecipient_msisdn() != null) {
            _hashCode += getRecipient_msisdn().hashCode();
        }
        if (getConnection_type() != null) {
            _hashCode += getConnection_type().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getInit_time() != null) {
            _hashCode += getInit_time().hashCode();
        }
        if (getCancel_time() != null) {
            _hashCode += getCancel_time().hashCode();
        }
        if (getRecharge_date() != null) {
            _hashCode += getRecharge_date().hashCode();
        }
        if (getRecharge_status() != null) {
            _hashCode += getRecharge_status().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RechargeRequestDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "RechargeRequestDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("guid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "guid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vr_guid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vr_guid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("request_time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "request_time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operator_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "operator_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipient_msisdn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recipient_msisdn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("connection_type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "connection_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("init_time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "init_time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancel_time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cancel_time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recharge_date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recharge_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recharge_status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "recharge_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
