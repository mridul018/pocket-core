/**
 * VirtualrechargePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public interface VirtualrechargePortType extends java.rmi.Remote {
    public String getLastRechargeTime(String client_id, String operator_id, String msisdn) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.OperatorDataType findOperatorInfo(String client_id, String msisdn) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.OperatorDataType getOperatorInfo(String client_id, String operator_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.OperatorDataType[] getAllOperatorInfo(String client_id, String active_status) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.ClientDataType getClientInfo(String client_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.ClientBalanceDataType getBalanceInfo(String client_id) throws java.rmi.RemoteException;
    public String getStatusCodeMeaning(String client_id, String status_code) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType createRecharge(String client_id, String client_pass, String guid, java.math.BigInteger operator_id, String recipient_msisdn, java.math.BigInteger amount, String connection_type, String sender_id, java.math.BigInteger priority, String success_url, String failure_url) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType createWimaxRecharge(String client_id, String client_pass, String guid, java.math.BigInteger operator_id, String user_account, String amount, String connection_type, String sender_id, java.math.BigInteger priority, String success_url, String failure_url) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType createDescoRecharge(String client_id, String client_pass, String guid, java.math.BigInteger operator_id, String user_account, String bill_no, String amount, String connection_type, String sender_id, java.math.BigInteger priority, String success_url, String failure_url) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType createDpdcRecharge(String client_id, String client_pass, String guid, java.math.BigInteger operator_id, String user_account, String bill_month, String location_code, String amount, String connection_type, String sender_id, java.math.BigInteger priority, String success_url, String failure_url) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType initRecharge(String client_id, String client_pass, String guid, String vr_guid, String success_url, String failure_url) throws java.rmi.RemoteException;
    public java.math.BigInteger verifyMSISDN(String client_id, String msisdn) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.OperatorLimitsDataType getOperatorLimits(String client_id, String operator_id) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeRequestDataType queryRechargeStatus(String client_id, String guid, String vr_guid) throws java.rmi.RemoteException;
    public tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType cancelRecharge(String client_id, String client_pass, String guid, String vr_guid, String cancel_reason) throws java.rmi.RemoteException;
}
