/**
 * ClientDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tech.ipocket.pocket.wsdl.ssl;

public class ClientDataType  implements java.io.Serializable {
    private String client_user_id;

    private String client_name;

    private String client_contact_details;

    private int client_active_status;

    public ClientDataType() {
    }

    public ClientDataType(
           String client_user_id,
           String client_name,
           String client_contact_details,
           int client_active_status) {
           this.client_user_id = client_user_id;
           this.client_name = client_name;
           this.client_contact_details = client_contact_details;
           this.client_active_status = client_active_status;
    }


    /**
     * Gets the client_user_id value for this ClientDataType.
     *
     * @return client_user_id
     */
    public String getClient_user_id() {
        return client_user_id;
    }


    /**
     * Sets the client_user_id value for this ClientDataType.
     *
     * @param client_user_id
     */
    public void setClient_user_id(String client_user_id) {
        this.client_user_id = client_user_id;
    }


    /**
     * Gets the client_name value for this ClientDataType.
     *
     * @return client_name
     */
    public String getClient_name() {
        return client_name;
    }


    /**
     * Sets the client_name value for this ClientDataType.
     *
     * @param client_name
     */
    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }


    /**
     * Gets the client_contact_details value for this ClientDataType.
     *
     * @return client_contact_details
     */
    public String getClient_contact_details() {
        return client_contact_details;
    }


    /**
     * Sets the client_contact_details value for this ClientDataType.
     *
     * @param client_contact_details
     */
    public void setClient_contact_details(String client_contact_details) {
        this.client_contact_details = client_contact_details;
    }


    /**
     * Gets the client_active_status value for this ClientDataType.
     *
     * @return client_active_status
     */
    public int getClient_active_status() {
        return client_active_status;
    }


    /**
     * Sets the client_active_status value for this ClientDataType.
     *
     * @param client_active_status
     */
    public void setClient_active_status(int client_active_status) {
        this.client_active_status = client_active_status;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof ClientDataType)) return false;
        ClientDataType other = (ClientDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.client_user_id==null && other.getClient_user_id()==null) ||
             (this.client_user_id!=null &&
              this.client_user_id.equals(other.getClient_user_id()))) &&
            ((this.client_name==null && other.getClient_name()==null) ||
             (this.client_name!=null &&
              this.client_name.equals(other.getClient_name()))) &&
            ((this.client_contact_details==null && other.getClient_contact_details()==null) ||
             (this.client_contact_details!=null &&
              this.client_contact_details.equals(other.getClient_contact_details()))) &&
            this.client_active_status == other.getClient_active_status();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClient_user_id() != null) {
            _hashCode += getClient_user_id().hashCode();
        }
        if (getClient_name() != null) {
            _hashCode += getClient_name().hashCode();
        }
        if (getClient_contact_details() != null) {
            _hashCode += getClient_contact_details().hashCode();
        }
        _hashCode += getClient_active_status();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://103.26.139.140/virtualrecharge/?wsdl", "ClientDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client_user_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client_user_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client_contact_details");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client_contact_details"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("client_active_status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "client_active_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
