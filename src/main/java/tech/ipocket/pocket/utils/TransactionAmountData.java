package tech.ipocket.pocket.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class TransactionAmountData {

    DecimalFormat decimalFormat = new DecimalFormat("############.####");
    private int PRECISION = 4;
//    private final BigDecimal VAT_PERCENTAGE = new BigDecimal(15);
//    private final BigDecimal AIT_PERCENTAGE = new BigDecimal(10);
    private final BigDecimal VAT_PERCENTAGE = new BigDecimal(5);
    private final BigDecimal AIT_PERCENTAGE = new BigDecimal(0);

    private BigDecimal originalTransactionAmount;
    private BigDecimal senderDebitAmount;
    private BigDecimal receiverCreditAmount;
    private BigDecimal totalFeeAmount;
    private BigDecimal feeAmountAfterVat;
    private BigDecimal feeCreditAmount;
    private BigDecimal vatCreditAmount;
    private BigDecimal aitCreditAmount;

    public TransactionAmountData(FeeData feeData, BigDecimal transactionAmount) {
        originalTransactionAmount = transactionAmount;
        senderDebitAmount = transactionAmount;
        receiverCreditAmount = transactionAmount;

        if (feeData.getFeePayer() != null) {
            if (feeData.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
                senderDebitAmount = transactionAmount.add(feeData.getFeeAmount());
            } else {
                receiverCreditAmount = transactionAmount.subtract(feeData.getFeeAmount());
            }
        }

        // vat and fee calculate
        totalFeeAmount = feeData.getFeeAmount();

        feeAmountAfterVat = totalFeeAmount
                .divide(calculatePercentage(new BigDecimal(100))
                                .add(calculatePercentage(VAT_PERCENTAGE))
                        , PRECISION, RoundingMode.HALF_EVEN)
                .multiply(calculatePercentage(new BigDecimal(100)));

        vatCreditAmount = totalFeeAmount.subtract(feeAmountAfterVat);
        vatCreditAmount = new BigDecimal(decimalFormat.format(vatCreditAmount));

        aitCreditAmount = feeAmountAfterVat.multiply(calculatePercentage(AIT_PERCENTAGE));
        aitCreditAmount = new BigDecimal(decimalFormat.format(aitCreditAmount));

        feeCreditAmount = feeAmountAfterVat.subtract(aitCreditAmount);
        feeCreditAmount = new BigDecimal(decimalFormat.format(feeCreditAmount));

    }

    private BigDecimal calculatePercentage(BigDecimal percentageValue) {
        if(percentageValue==null||percentageValue.compareTo(BigDecimal.ZERO)==0){
            return BigDecimal.ZERO;
        }
        return percentageValue.divide(new BigDecimal(100), PRECISION, RoundingMode.HALF_EVEN);
    }

    public DecimalFormat getDecimalFormat() {
        return decimalFormat;
    }

    public void setDecimalFormat(DecimalFormat decimalFormat) {
        this.decimalFormat = decimalFormat;
    }

    public int getPRECISION() {
        return PRECISION;
    }

    public void setPRECISION(int PRECISION) {
        this.PRECISION = PRECISION;
    }

    public BigDecimal getVAT_PERCENTAGE() {
        return VAT_PERCENTAGE;
    }

    public BigDecimal getAIT_PERCENTAGE() {
        return AIT_PERCENTAGE;
    }

    public BigDecimal getOriginalTransactionAmount() {
        return originalTransactionAmount;
    }

    public void setOriginalTransactionAmount(BigDecimal originalTransactionAmount) {
        this.originalTransactionAmount = originalTransactionAmount;
    }

    public BigDecimal getSenderDebitAmount() {
        return senderDebitAmount;
    }

    public void setSenderDebitAmount(BigDecimal senderDebitAmount) {
        this.senderDebitAmount = senderDebitAmount;
    }

    public BigDecimal getReceiverCreditAmount() {
        return receiverCreditAmount;
    }

    public void setReceiverCreditAmount(BigDecimal receiverCreditAmount) {
        this.receiverCreditAmount = receiverCreditAmount;
    }

    public BigDecimal getTotalFeeAmount() {
        return totalFeeAmount;
    }

    public void setTotalFeeAmount(BigDecimal totalFeeAmount) {
        this.totalFeeAmount = totalFeeAmount;
    }

    public BigDecimal getFeeAmountAfterVat() {
        return feeAmountAfterVat;
    }

    public void setFeeAmountAfterVat(BigDecimal feeAmountAfterVat) {
        this.feeAmountAfterVat = feeAmountAfterVat;
    }

    public BigDecimal getFeeCreditAmount() {
        return feeCreditAmount;
    }

    public void setFeeCreditAmount(BigDecimal feeCreditAmount) {
        this.feeCreditAmount = feeCreditAmount;
    }

    public BigDecimal getVatCreditAmount() {
        return vatCreditAmount;
    }

    public void setVatCreditAmount(BigDecimal vatCreditAmount) {
        this.vatCreditAmount = vatCreditAmount;
    }

    public BigDecimal getAitCreditAmount() {
        return aitCreditAmount;
    }

    public void setAitCreditAmount(BigDecimal aitCreditAmount) {
        this.aitCreditAmount = aitCreditAmount;
    }
}
