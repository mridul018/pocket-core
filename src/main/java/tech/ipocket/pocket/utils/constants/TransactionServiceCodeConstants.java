package tech.ipocket.pocket.utils.constants;

public class TransactionServiceCodeConstants {
    public static final String FUND_TRANSFER = "1001";
    public static final String MERCHANT_PAYMENT = "1002";
    public static final String TOP_UP = "1003";
    public static final String AGENT_CASH_OUT = "1008";
    public static final String AGENT_CASH_IN = "1009";
    public static final String BILL_PAY = "1022";
}
