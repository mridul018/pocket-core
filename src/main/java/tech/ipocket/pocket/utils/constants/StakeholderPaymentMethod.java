package tech.ipocket.pocket.utils.constants;

public class StakeholderPaymentMethod {
    public static final String GL_TRANSACTION = "2";
    public static final String WALLET_TRANSACTION = "1";
    public static final String TO_TOPUP_AGENT_WALLET = "7";
    public static final String TO_SR_WALLET = "8";
    public static final String TO_AGENT_MERCHANT_WALLET = "9";
}
