package tech.ipocket.pocket.utils.constants;

public class BillPayTypeConstants {
    public static final String PALLI_BIDYUT = "PALLI_BIDYUT";
    public static final String TOPUP = "TopUp";
    public static final String PAYNET = "PAYNET";

    //Salik
    public static final String SLK_PIN = "SLK-PIN";   //BY ACCOUNT
    public static final String SLK50 = "SLK50";
    public static final String SLK100 = "SLK100";
    public static final String SLK200 = "SLK200";

    //NOL CART
    public static final String NOL_CART = "NOL_CART";

    //VIRGIN
    public static final String VIRGIN_M20 = "VIRGIN-M20";
    public static final String VIRGIN_M50 = "VIRGIN-M50";
    public static final String VIRGIN_M100 = "VIRGIN-M100";
    public static final String VIRGIN_M150 = "VIRGIN-M150";
    public static final String VIRGIN_M200 = "VIRGIN-M200";

    //E-DIRHAM (All BY ACCOUNT)
    public static final String E_DIRHAM_BLUE = "E-DIRHAM-BLUE";
    public static final String E_DIRHAM_GOLD = "E-DIRHAM-GOLD";
    public static final String E_DIRHAM_GREEN = "E-DIRHAM-GREEN";
    public static final String E_DIRHAM_RED = "E-DIRHAM-RED";
    public static final String E_DIRHAM_SILVER = "E-DIRHAM-SILVER";

    //Etisalat Postpaid (ALL BY ACCOUNT)
    public static final String ET_POST_MOBILE = "ET-POST-MOBILE";
    public static final String ET_POST_DEL = "ET-POST-DEL";
    public static final String ET_POST_DIALUP = "ET-POST-DIALUP";
    public static final String ET_POST_BROADBAND = "ET-POST-BROADBAND";
    public static final String ET_POST_EVISION = "ET-POST-EVISION";
    public static final String ET_POST_ELIFE = "ET-POST-ELIFE";
    //Etisalat Prepaid(BY ACCOUNT)
    public static final String ET_MC = "ET-MC";
    //Etisalat Packages (ALL BY ACCOUNT)
    public static final String ET_MONTHLY_500_MB_DATA_AED_30 = "ET-MONTHLY-500-MB-DATA-AED-30";
    public static final String ET_MONTHLY_DATA_PLAN_AED_33_FIVE_SIM = "ET-MONTHLY-DATA-PLAN-AED-33-FIVE-SIM";
    public static final String ET_MONTHLY_500_INTERNATIONAL_MINS_AED_49 = "ET-MONTHLY-500-INTERNATIONAL-MINS-AED-49";
    public static final String ET_MONTHLY_NONSTOP_50 = "ET-MONTHLY-NONSTOP-50";
    public static final String ET_MONTHLY_1_GB_DATA_AED_50 = "ET-MONTHLY-1-GB-DATA-AED-50";
    public static final String ET_MONTHLY_1000_INTERNATIONAL_MINS_AED_79 = "ET-MONTHLY-1000-INTERNATIONAL-MINS-AED-79";
    public static final String ET_MONTHLY_2000_INTERNATIONAL_MINS_AED_99 = "ET-MONTHLY-2000-INTERNATIONAL-MINS-AED-99";
    public static final String ET_MONTHLY_3_GB_DATA_AED_100 = "ET-MONTHLY-3-GB-DATA-AED-100";
    public static final String ET_MONTHLY_6_GB_DATA_AED_150 = "ET-MONTHLY-6-GB-DATA-AED-150";
    public static final String ET_WEEKLY_200_INTERNATIONAL_MINS_AED_25 = "ET-WEEKLY-200-INTERNATIONAL-MINS-AED-25";
    public static final String ET_2_GB_AND_30_FLEXI_MINS_AED_50 = "ET-2-GB-AND-30-FLEXI-MINS-AED-50";
    public static final String ET_4_GB_AND_60_FLEXI_MINS_AED_90 = "ET-4-GB-AND-60-FLEXI-MINS-AED-90";
    public static final String ET_6_GB_90_FLEXI_ICP_AED_120 = "ET-6-GB-90-FLEXI-ICP-AED-120";
    public static final String ET_COMBO_55_WITH_1GB_AND_50_FLEXI_MINS = "ET-COMBO-55-WITH-1GB-AND-50-FLEXI-MINS";
    public static final String ET_COMBO_35_WITH_500MB_AND_25_FLEXI_MINS = "ET-COMBO-35-WITH-500MB-AND-25-FLEXI-MINS";
    public static final String ET_COMBO_110_WITH_3GB_AND_150_FLEXI_MINS = "ET-COMBO-110-WITH-3GB-AND-150-FLEXI-MINS";
    public static final String ET_COMBO_165_WITH_6GB_AND_200_FLEXI_MINS = "ET-COMBO-165-WITH-6GB-AND-200-FLEXI-MINS";
    public static final String ET_BANGLADESH_60_MINUTES_DAILY_PACK = "ET-BANGLADESH-60-MINUTES-DAILY-PACK";
    public static final String ET_NEW_DAILY_30_INTERNATIONAL_MINUTES = "ET-NEW-DAILY-30-INTERNATIONAL-MINUTES";
    public static final String ET_DAILY_SOCIAL_PLAN = "ET-DAILY-SOCIAL-PLAN";
    public static final String ET_DAILY_NON_STOP_PLAN = "ET-DAILY-NON-STOP-PLAN";
    public static final String ET_100_MB_DAILY_DATA_PACK = "ET-100-MB-DAILY-DATA-PACK";
    public static final String ET_250_MB_DAILY_DATA_PACK = "ET-250-MB-DAILY-DATA-PACK";
    public static final String ET_500_MB_DAILY_DATA_PACK = "ET-500-MB-DAILY-DATA-PACK";
    public static final String ET_VISITOR_LINE = "ET-VISITOR-LINE";
    public static final String ET_VISITOR_LINE_PLUS_79_AED = "ET-VISITOR-LINE-PLUS-79-AED";
    public static final String ET_VISITOR_LINE_PREMIUM = "ET-VISITOR-LINE-PREMIUM";
    public static final String ET_VISITOR_LINE_PREMIUM_PLUS = "ET-VISITOR-LINE-PREMIUM-PLUS";
    //Etisalat e-Vouchers
    public static final String ET30 = "ET30";
    public static final String ET55 = "ET55";
    public static final String ET110 = "ET110";

    //Du Postpaid(BY ACCOUNT)
    public static final String DU_POST_MOBILE = "DU-POST-MOBILE";
    //Du prepaid(ALL BY ACCOUNT)
    public static final String DU_MT_TRANSFER = "DU-MT-TRANSFER";
    public static final String DU_MT = "DU-MT";
    public static final String DU_MD_25AED = "DU-MD-25AED";
    public static final String DU_MD_55AED = "DU-MD-55AED";
    public static final String DU_MD_110AED = "DU-MD-110AED";
    public static final String DU_MD_210AED = "DU-MD-210AED";
    public static final String DU_MD_525AED = "DU-MD-525AED";
    public static final String DU_MI = "DU-MI";
    //Du e-Vouchers
    public static final String DU25 = "DU25";
    public static final String DU55 = "DU55";
    public static final String DU110 = "DU110";
    public static final String DU210 = "DU210";

    //PUBG_MOBILE
    public static final String PUBG_MOBILE_5_AED = "PUBG_MOBILE_5_AED";
    public static final String PUBG_MOBILE_20_AED = "PUBG_MOBILE_20_AED";
    public static final String PUBG_MOBILE_40_AED = "PUBG_MOBILE_40_AED";
    public static final String PUBG_MOBILE_95_AED = "PUBG_MOBILE_95_AED";
    public static final String PUBG_MOBILE_185_AED = "PUBG_MOBILE_185_AED";
    public static final String PUBG_MOBILE_370_AED = "PUBG_MOBILE_370_AED";

    //Netflix
    public static final String NETFLIX_AED_100 = "NETFLIX-AED-100";
    public static final String NETFLIX_AED_500 = "NETFLIX-AED-500";

    //Steam
    public static final String STEAM_AED_40 = "STEAM-AED-40";
    public static final String STEAM_AED_50 = "STEAM-AED-50";
    public static final String STEAM_AED_75 = "STEAM-AED-75";
    public static final String STEAM_AED_100 = "STEAM-AED-100";
    public static final String STEAM_AED_200 = "STEAM-AED-200";
    public static final String STEAM_AED_400 = "STEAM-AED-400";

    //Calling cards (Hello Card)
    public static final String VO_H15 = "VO-H15";
    public static final String VO_H30 = "VO-H30";
    public static final String VO_H50 = "VO-H50";
    //Five Card
    public static final String VO_F15 = "VO-F15";
    public static final String VO_F30 = "VO-F30";
    public static final String VO_F50 = "VO-F50";

    //Xbox
    public static final String XBOX_LIVE_USD_15 = "XBOX_LIVE_USD_15";
    public static final String XBOX_LIVE_USD_25 = "XBOX_LIVE_USD_25";
    public static final String XBOX_LIVE_USD_50 = "XBOX_LIVE_USD_50";
    public static final String XBOX_LIVE_3_MONTH = "XBOX_LIVE_3_MONTH";
    public static final String XBOX_LIVE_12_MONTH = "XBOX_LIVE_12_MONTH";

    //Amazon.ae
    public static final String AMAZON_AE_50_AED = "AMAZON_AE_50_AED";
    public static final String AMAZON_AE_100_AED = "AMAZON_AE_100_AED";
    public static final String AMAZON_AE_250_AED = "AMAZON_AE_250_AED";
    public static final String AMAZON_AE_500_AED = "AMAZON_AE_500_AED";

    public static final String AMAZON_USD_10 = "AMAZON_USD_10";
    public static final String AMAZON_USD_25 = "AMAZON_USD_25";
    public static final String AMAZON_USD_35 = "AMAZON_USD_35";
    public static final String AMAZON_USD_50 = "AMAZON_USD_50";
    public static final String AMAZON_USD_100 = "AMAZON_USD_100";

    //PlayStation
    public static final String PSN_AED_1M = "PSN_AED_1M";
    public static final String PSN_AED_3M = "PSN_AED_3M";
    public static final String PSN_AED_1Y = "PSN_AED_1Y";

    public static final String PSN_USD_5 = "PSN_USD_5";
    public static final String PSN_USD_10 = "PSN_USD_10";
    public static final String PSN_USD_15 = "PSN_USD_15";
    public static final String PSN_USD_20 = "PSN_USD_20";
    public static final String PSN_USD_30 = "PSN_USD_30";
    public static final String PSN_USD_40 = "PSN_USD_40";
    public static final String PSN_USD_50 = "PSN_USD_50";

    public static final String PLAYSTATION_NETWORK_UAE_USD_5 = "PLAYSTATION_NETWORK_UAE_USD_5";
    public static final String PLAYSTATION_NETWORK_UAE_USD_10 = "PLAYSTATION_NETWORK_UAE_USD_10";
    public static final String PLAYSTATION_NETWORK_UAE_USD_20 = "PLAYSTATION_NETWORK_UAE_USD_20";
    public static final String PLAYSTATION_NETWORK_UAE_USD_50 = "PLAYSTATION_NETWORK_UAE_USD_50";
    public static final String PLAYSTATION_NETWORK_UAE_3_MONTHS = "PLAYSTATION_NETWORK_UAE_3_MONTHS";
    public static final String PLAYSTATION_NETWORK_UAE_12_MONTHS = "PLAYSTATION_NETWORK_UAE_12_MONTHS";
    public static final String PLAYSTATION_NETWORK_UAE_1_MONTH_MEMBERSHIP = "PLAYSTATION_NETWORK_UAE_1_MONTH_MEMBERSHIP";
    public static final String PLAYSTATION_NETWORK_UK_5_GBP = "PLAYSTATION_NETWORK_UK_5_GBP";
    public static final String PLAYSTATION_NETWORK_UK_10_GBP = "PLAYSTATION_NETWORK_UK_10_GBP";
    public static final String PLAYSTATION_NETWORK_UK_20_GBP = "PLAYSTATION_NETWORK_UK_20_GBP";
    public static final String PLAYSTATION_NETWORK_UK_50_GBP = "PLAYSTATION_NETWORK_UK_50_GBP";
    public static final String PLAYSTATION_NETWORK_UK_3_MONTHS = "PLAYSTATION_NETWORK_UK_3_MONTHS";
    public static final String PLAYSTATION_NETWORK_UK_12_MONTHS = "PLAYSTATION_NETWORK_UK_12_MONTHS";

    //Google Play Store
    public static final String GPLAY_AED_30 = "GPLAY_AED_30";
    public static final String GPLAY_AED_50 = "GPLAY_AED_50";
    public static final String GPLAY_AED_100 = "GPLAY_AED_100";
    public static final String GPLAY_AED_300 = "GPLAY_AED_300";
    public static final String GPLAY_AED_500 = "GPLAY_AED_500";

    public static final String GPLAY_USD_10 = "GPLAY_USD_10";
    public static final String GPLAY_USD_15 = "GPLAY_USD_15";
    public static final String GPLAY_USD_25 = "GPLAY_USD_25";
    public static final String GPLAY_USD_50 = "GPLAY_USD_50";

    //App Store and iTunes
    public static final String ITUNES_AED_50 = "ITUNES_AED_50";
    public static final String ITUNES_AED_100 = "ITUNES_AED_100";
    public static final String ITUNES_AED_250 = "ITUNES_AED_250";
    public static final String ITUNES_AED_500 = "ITUNES_AED_500";

    public static final String ITUNES_USD_5 = "ITUNES_USD_5";
    public static final String ITUNES_USD_10 = "ITUNES_USD_10";
    public static final String ITUNES_USD_15 = "ITUNES_USD_15";
    public static final String ITUNES_USD_20 = "ITUNES_USD_20";
    public static final String ITUNES_USD_25 = "ITUNES_USD_25";
    public static final String ITUNES_USD_30 = "ITUNES_USD_30";
    public static final String ITUNES_USD_50 = "ITUNES_USD_50";
    public static final String ITUNES_USD_100 = "ITUNES_USD_100";

    public static final String ITUNES_FRANCE_EUR_10_FR = "ITUNES_FRANCE_EUR_10_FR";
    public static final String ITUNES_FRANCE_EUR_15_FR = "ITUNES_FRANCE_EUR_15_FR";
    public static final String ITUNES_FRANCE_EUR_25_FR = "ITUNES_FRANCE_EUR_25_FR";
    public static final String ITUNES_FRANCE_EUR_50_FR = "ITUNES_FRANCE_EUR_50_FR";

    public static final String ITUNES_UK_GBP_10 = "ITUNES_UK_GBP_10";
    public static final String ITUNES_UK_GBP_15 = "ITUNES_UK_GBP_15";
    public static final String ITUNES_UK_GBP_25 = "ITUNES_UK_GBP_25";
    public static final String ITUNES_UK_GBP_50 = "ITUNES_UK_GBP_50";
    public static final String ITUNES_UK_GBP_100 = "ITUNES_UK_GBP_100";

    //Minecraft
    public static final String MINECRAFT = "MINECRAFT";
    public static final String MINECRAFT_1720_MINECOINS = "MINECRAFT_1720_MINECOINS";
    public static final String MINECRAFT_3500_MINECOINS = "MINECRAFT_3500_MINECOINS";

    //Nintendo
    public static final String NINTENDO_US_USD_10 = "NINTENDO_US_USD_10";
    public static final String NINTENDO_US_USD_20 = "NINTENDO_US_USD_20";
    public static final String NINTENDO_US_USD_35 = "NINTENDO_US_USD_35";
    public static final String NINTENDO_US_USD_50 = "NINTENDO_US_USD_50";

    //VIP Baloot
    public static final String VIP_BALOOT_960_CHIPS = "VIP_BALOOT_960_CHIPS";
    public static final String VIP_BALOOT_2000_CHIPS = "VIP_BALOOT_2000_CHIPS";
    public static final String VIP_BALOOT_4200_CHIPS = "VIP_BALOOT_4200_CHIPS";
    public static final String VIP_BALOOT_11400_CHIPS = "VIP_BALOOT_11400_CHIPS";
    public static final String VIP_BALOOT_25000_CHIPS = "VIP_BALOOT_25000_CHIPS";
    public static final String VIP_BALOOT_48000_CHIPS = "VIP_BALOOT_48000_CHIPS";

    //Bigo Likee
    public static final String BIGO_LIKE_USD_250_AE = "BIGO_LIKE_USD_250_AE";
    public static final String BIGO_LIKE_USD_500_AE = "BIGO_LIKE_USD_500_AE";
    public static final String BIGO_LIKE_USD_1000_AE = "BIGO_LIKE_USD_1000_AE";
    public static final String BIGO_LIKE_USD_1500_AE = "BIGO_LIKE_USD_1500_AE";
    public static final String BIGO_LIKE_USD_2000_AE = "BIGO_LIKE_USD_2000_AE";

    //BIGO Live
    public static final String BIGO_LIVE_USD_250_AE = "BIGO_LIVE_USD_250_AE";
    public static final String BIGO_LIVE_USD_1000_AE = "BIGO_LIVE_USD_1000_AE";
    public static final String BIGO_LIVE_USD_1500_AE = "BIGO_LIVE_USD_1500_AE";
    public static final String BIGO_LIVE_USD_2000_AE = "BIGO_LIVE_USD_2000_AE";

    //Spacetoon Go
    public static final String SPACETOON_GO_3_MONTHS_SUBSCRIPTION_UAE = "SPACETOON_GO_3_MONTHS_SUBSCRIPTION_UAE";
    public static final String SPACETOON_GO_12_MONTHS_SUBSCRIPTION_UAE = "SPACETOON_GO_12_MONTHS_SUBSCRIPTION_UAE";

    //BeIn
    public static final String BEIN_1_DAY_SUBSCRIPTION_INT = "BEIN_1_DAY_SUBSCRIPTION_INT";
    public static final String BEIN_1_MONTH_SUBSCRIPTION_INT = "BEIN_1_MONTH_SUBSCRIPTION_INT";
    public static final String BEIN_12_MONTH_SUBSCRIPTION_INT = "BEIN_12_MONTH_SUBSCRIPTION_INT";

    //Crunchyroll
    public static final String CRUNCHYROLL_1_MONTH_SUBSCRIPTION_INT = "CRUNCHYROLL_1_MONTH_SUBSCRIPTION_INT";
    public static final String CRUNCHYROLL_3_MONTH_SUBSCRIPTION_INT = "CRUNCHYROLL_3_MONTH_SUBSCRIPTION_INT";
    public static final String CRUNCHYROLL_12_MONTHS_SUBSCRIPTION_INT = "CRUNCHYROLL_12_MONTHS_SUBSCRIPTION_INT";

    //Mobile Legends
    public static final String MOBILE_LEGENDS_56_DIAMONDS = "MOBILE_LEGENDS_56_DIAMONDS";
    public static final String MOBILE_LEGENDS_278_DIAMONDS = "MOBILE_LEGENDS_278_DIAMONDS";
    public static final String MOBILE_LEGENDS_571_DIAMONDS = "MOBILE_LEGENDS_571_DIAMONDS";
    public static final String MOBILE_LEGENDS_1167_DIAMONDS = "MOBILE_LEGENDS_1167_DIAMONDS";
    public static final String MOBILE_LEGENDS_1783_DIAMONDS = "MOBILE_LEGENDS_1783_DIAMONDS";
    public static final String MOBILE_LEGENDS_3005_DIAMONDS = "MOBILE_LEGENDS_3005_DIAMONDS";
    public static final String MOBILE_LEGENDS_4770_DIAMONDS = "MOBILE_LEGENDS_4770_DIAMONDS";
    public static final String MOBILE_LEGENDS_6012_DIAMONDS = "MOBILE_LEGENDS_6012_DIAMONDS";
    public static final String MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP = "MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP";
    public static final String MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP_PLUS = "MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP_PLUS";
    public static final String MOBILE_LEGENDS_TOP_UP_56_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_56_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_278_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_278_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_571_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_571_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_1167_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_1167_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_1783_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_1783_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_3005_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_3005_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_4770_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_4770_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_6012_DIAMONDS = "MOBILE_LEGENDS_TOP_UP_6012_DIAMONDS";
    public static final String MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP = "MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP";
    public static final String MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP_PLUS = "MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP_PLUS";
    public static final String MOBILE_LEGENDS_TOP_UP_TWILIGHT_PASS = "MOBILE_LEGENDS_TOP_UP_TWILIGHT_PASS";
    public static final String MOBILE_LEGENDS_TWILIGHT_PASS = "MOBILE_LEGENDS_TWILIGHT_PASS";

    //Viber
    public static final String VIBER_USD_10_INT = "VIBER_USD_10_INT";
    public static final String VIBER_USD_25_INT = "VIBER_USD_25_INT";

    //FreeFire
    public static final String FREEFIRE_USD_1_100_10_DIAMONDS = "FREEFIRE_USD_1_100_10_DIAMONDS";
    public static final String FREEFIRE_USD_2_210_21_DIAMONDS = "FREEFIRE_USD_2_210_21_DIAMONDS";
    public static final String FREEFIRE_USD_5_530_53_DIAMONDS = "FREEFIRE_USD_5_530_53_DIAMONDS";
    public static final String FREEFIRE_USD_10_1080_108_DIAMONDS = "FREEFIRE_USD_10_1080_108_DIAMONDS";
    public static final String FREEFIRE_USD_20_2200_220_DIAMONDS = "FREEFIRE_USD_20_2200_220_DIAMONDS";

    //STARZPLAY
    public static final String STARZPLAY_3_MONTH_SUBSCRIPTION_INT = "STARZPLAY_3_MONTH_SUBSCRIPTION_INT";
    public static final String STARZPLAY_6_MONTH_SUBSCRIPTION_INT = "STARZPLAY_6_MONTH_SUBSCRIPTION_INT";
    public static final String STARZPLAY_12_MONTHS_SUBSCRIPTION_INT = "STARZPLAY_12_MONTHS_SUBSCRIPTION_INT";

    //Roblox
    public static final String ROBLOX_USD_10_INT = "ROBLOX_USD_10_INT";
    public static final String ROBLOX_USD_25_INT = "ROBLOX_USD_25_INT";

    //McAfee Antivirus
    public static final String MCAFEE_MCAFEE_ANTIVIRUS_1_USER = "MCAFEE_MCAFEE_ANTIVIRUS_1_USER";
    public static final String MCAFEE_MCAFEE_INTERNET_SECURITY_1_DEVICE = "MCAFEE_MCAFEE_INTERNET_SECURITY_1_DEVICE";
    public static final String MCAFEE_MCAFEE_INTERNET_SECURITY_3_DEVICE = "MCAFEE_MCAFEE_INTERNET_SECURITY_3_DEVICE";
    public static final String MCAFEE_MCAFEE_INTERNET_SECURITY_10_DEVICE = "MCAFEE_MCAFEE_INTERNET_SECURITY_10_DEVICE";
    public static final String MCAFEE_MCAFEE_TOTAL_PROTECTION_5_DEVICE = "MCAFEE_MCAFEE_TOTAL_PROTECTION_5_DEVICE";
    public static final String MCAFEE_MCAFEE_TOTAL_PROTECTION_10_DEVICE = "MCAFEE_MCAFEE_TOTAL_PROTECTION_10_DEVICE";

    //Skype
    public static final String SKYPE_USD_10_INT = "SKYPE_USD_10_INT";
    public static final String SKYPE_USD_25_INT = "SKYPE_USD_25_INT";

    //eBay US
    public static final String EBAY_USD_10_US = "EBAY_USD_10_US";
    public static final String EBAY_USD_50_US = "EBAY_USD_50_US";
    public static final String EBAY_USD_100_US = "EBAY_USD_100_US";

    //Shahid VIP
    public static final String SHAHID_VIP_3_MONTHS_SUBSCRIPTION_GCC = "SHAHID_VIP_3_MONTHS_SUBSCRIPTION_GCC";
    public static final String SHAHID_VIP_6_MONTHS_SUBSCRIPTION_GCC = "SHAHID_VIP_6_MONTHS_SUBSCRIPTION_GCC";
    public static final String SHAHID_VIP_12_MONTHS_SUBSCRIPTION_GCC = "SHAHID_VIP_12_MONTHS_SUBSCRIPTION_GCC";

    //Spotify US
    public static final String SPOTIFY_US_USD_10 = "SPOTIFY_US_USD_10";
    public static final String SPOTIFY_US_USD_30 = "SPOTIFY_US_USD_30";
    public static final String SPOTIFY_US_USD_60 = "SPOTIFY_US_USD_60";

    //Razer Gold
    public static final String RAZER_GOLD_PINS_USD_10 = "RAZER_GOLD_PINS_USD_10";
    public static final String RAZER_GOLD_PINS_USD_20 = "RAZER_GOLD_PINS_USD_20";
    public static final String RAZER_GOLD_PINS_USD_50 = "RAZER_GOLD_PINS_USD_50";
    public static final String RAZER_GOLD_PINS_USD_100 = "RAZER_GOLD_PINS_USD_100";
    public static final String RAZER_GOLD_TOP_UP_5_USD = "RAZER_GOLD_TOP_UP_5_USD";
    public static final String RAZER_GOLD_TOP_UP_10_USD = "RAZER_GOLD_TOP_UP_10_USD";
    public static final String RAZER_GOLD_TOP_UP_20_USD = "RAZER_GOLD_TOP_UP_20_USD";
    public static final String RAZER_GOLD_TOP_UP_50_USD = "RAZER_GOLD_TOP_UP_50_USD";
    public static final String RAZER_GOLD_TOP_UP_100_USD = "RAZER_GOLD_TOP_UP_100_USD";

    //eWallet Cash In(BY ACCOUNT)
    public static final String E_WALLET_CASH_IN = "E-WALLET-CASH-IN";
}
