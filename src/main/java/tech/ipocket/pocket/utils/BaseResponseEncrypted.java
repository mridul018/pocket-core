package tech.ipocket.pocket.utils;

public class BaseResponseEncrypted {
    private String encryptedTextString;
    private String encryptedSecretKeyString;

    public String getEncryptedTextString() {
        return encryptedTextString;
    }

    public void setEncryptedTextString(String encryptedTextString) {
        this.encryptedTextString = encryptedTextString;
    }

    public String getEncryptedSecretKeyString() {
        return encryptedSecretKeyString;
    }

    public void setEncryptedSecretKeyString(String encryptedSecretKeyString) {
        this.encryptedSecretKeyString = encryptedSecretKeyString;
    }
}
