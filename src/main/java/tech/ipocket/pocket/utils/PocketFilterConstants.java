package tech.ipocket.pocket.utils;

public class PocketFilterConstants {
    public static final String TRANSACTION_TOKEN = "TRANSACTION_TOKEN";
    public static final String MOBILE_NO_WITH_DATE_RANGE = "MOBILE_NO_WITH_DATE_RANGE";
    public static final String DATE_RANGE = "DATE_RANGE";
    public static final String TX_TYPES_WITH_DATE_RANGE = "TX_TYPES_WITH_DATE_RANGE";
    public static final String MOBILE_TX_TYPES_WITH_DATE_RANGE = "MOBILE_TX_TYPES_WITH_DATE_RANGE";
    public static final String UPAY_DATE_RANGE = "UPAY_DATE_RANGE";
    public static final String HSS_DATE_RANGE = "HSS_DATE_RANGE";
    public static final String PAYNET_DATE_RANGE = "PAYNET_DATE_RANGE";
}
