package tech.ipocket.pocket.utils;

public class GlConstants {
    public static final String VAT_Payable = "VAT_Payable";
    public static final String AIT_Payable = "AIT_Payable";
    public static final String QCASH = "QCASH";
    public static String Deposit_of_Master = "Deposit_of_Master";
    public static String Deposit_of_SR = "Deposit_of_SR";
    public static String Fee_payable_for_AdminCashIn = "Fee_payable_for_AdminCashIn";
    public static String Fee_payable_for_AdminCashOut = "Fee_payable_for_AdminCashOut";
    public static String Fee_payable_for_FundMovement = "Fee_payable_for_FundMovement";
    public static String Fee_payable_for_CashInFromBank = "Fee_payable_for_CashInFromBank";

    public static final String Deposit_of_Customer = "Deposit_of_Customer";

    public static String Deposit_of_Merchant = "Deposit_of_Merchant";
    public static String Deposit_of_Agent = "Deposit_of_Agent";
    //    public static String Balance_Of_SSL="Balance_Of_SSL";
    public static String Balance_Of_ = "Balance_Of_";


    public static final String Fee_payable_for_TopUp = "Fee_payable_for_TopUp";
    public static final String Fee_payable_for_BillPay = "Fee_payable_for_BillPay";
    public static final String Fee_payable_for_Refill = "Fee_payable_for_Refill";
    public static final String Fee_payable_for_AddFund = "Fee_payable_for_AddFund";
    public static String Fee_payable_for_FundTransfer = "Fee_payable_for_FundTransfer";

    public static String Fee_payable_for_Merchant_Sale = "Fee_payable_for_Merchant_Sale";
    public static String Fee_payable_for_Cash_Out = "Fee_payable_for_Cash_Out";
    public static String Fee_payable_for_Cash_In = "Fee_payable_for_Cash_In";
    public static String Fee_payable_for_CashWithdrawal = "Fee_payable_for_CashWithdrawal";
    public static String Fee_payable_for_CashOutToBankIntermediate="Fee_payable_for_CashOutToBankIntermediate";
    public static String Fee_payable_for_CashOutToBank="Fee_payable_for_CashOutToBank";
    public static String Fee_payable_for_Unresolved="Fee_payable_for_Unresolved";
    public static String POCKET_INCOME_GL="Pocket_Income";
    public static String POCKET_INCOME_PARENT_GL_CODE ="30000000";
    public static String POCKET_EXPENSE_PARENT_GL_CODE ="40000000";
    public static final String BANK_PARENT_GL = "20000100";

    public static String POCKET_EXPENSE_GL_CODE_FOR_CASHIN ="Expense_for_cashIn";

    public static String getGlNameByCustomerType(String groupCode) {

        switch (groupCode){
            case "1001":
                return Deposit_of_Customer;
            case "1002":
                return Deposit_of_Agent;
            case "1003":
                return Deposit_of_Merchant;
            case "2001":
                return Deposit_of_Master;
            case "1006":
                return Deposit_of_SR;
        }
        return Deposit_of_Customer;
    }
}