package tech.ipocket.pocket.utils;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.UmWalletCreateResponse;

public class WalletClass {

    public static boolean walletCreate(WalletCreateRequest request, Environment environment) {

        String tsWalletCreateUrl = environment.getProperty("TS_WALLET_CREATE_URL");

        /*BaseResponseObject responseObject = restTemplate.postForObject(tsWalletCreateUrl, request, BaseResponseObject.class);
        if(responseObject.getError()==null){
            return Boolean.TRUE;
        }else if(responseObject.getError() != null){
            return Boolean.FALSE;
        }*/

        try {
            UmWalletCreateResponse response = (UmWalletCreateResponse) new RestCallerTask()
                    .callToRestService(HttpMethod.POST, tsWalletCreateUrl, request, UmWalletCreateResponse.class, request.getRequestId());

            if (response != null) {
                return true;
            }
            return false;
        } catch (PocketException e) {
            return false;
        }
    }
}
