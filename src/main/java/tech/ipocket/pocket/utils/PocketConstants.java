package tech.ipocket.pocket.utils;

public class PocketConstants {

    public static final String OK = "200";
    public static final String ERROR = "400";
    public static final boolean SECURITY_ON = false;

    public static final String TOP_UP = "TOP_UP";
    public static final String PALLI_BIDYUT = "PALLI_BIDYUT";
    public static final String WALLET_TO_WALLET_FM = "1";
    public static final String WALLET_TO_GL_FM = "2";
    public static final String GL_TO_WALLET_FM = "3";
    public static final String GL_TO_GL_FM = "4";
    public static final String STATUS_ACTIVE = "1";
    public static final String STATUS_INACTIVE = "0";
    public static final String INITIALIZED = "0";
    public static final String RESOLVED = "1";
    public static final String REVERSAL = "2";
    public static final String SUCCESS = "Success";
    public static final String FAILED = "Failed";

    public static final String BILL_PENDING = "600";
    public static final String PAYNET = "PAYNET";
    public static final String PASSPORT_TRANSACTION_TYPE = "1017";
    public static final String PASSPORT_STATUS = "1";
}
