package tech.ipocket.pocket.utils;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.notification.NotificationRequest;

import java.math.BigDecimal;

public class NotificationBuilder {

    public NotificationRequest buildNotificationRequest(BigDecimal transactionAmount, String receiverWallet,
                                                        String senderWalletNo, String transactionToken, String serviceCode,
                                                        String feePayer, BigDecimal feeAmount, String request, String response,
                                                        String requestId, String notes) {

        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setRequestId(requestId);
        notificationRequest.setTransactionAmount(transactionAmount);
        notificationRequest.setReceiverWallet(receiverWallet);
        notificationRequest.setSenderWalletNo(senderWalletNo);
        notificationRequest.setTransactionToken(transactionToken);

        notificationRequest.setFeeAmount(feeAmount);
        notificationRequest.setFeePayer(feePayer);

        notificationRequest.setServiceCode(serviceCode);
        notificationRequest.setNotes(notes);

        notificationRequest.setRequest(requestId);
        notificationRequest.setRequest(request);
        notificationRequest.setResponse(response);

        return notificationRequest;
    }


    public NotificationRequest buildNotificationRequestFcm(BigDecimal transactionAmount, String receiverWallet,
                                                        String senderWalletNo, String transactionToken, String serviceCode,
                                                        String feePayer, BigDecimal feeAmount, String request, String response,
                                                        String requestId, String notes,
                                                        String senderFcmKey,String receiverFcmKey) {

        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setTransactionAmount(transactionAmount);
        notificationRequest.setReceiverWallet(receiverWallet);
        notificationRequest.setSenderWalletNo(senderWalletNo);
        notificationRequest.setTransactionToken(transactionToken);

        notificationRequest.setFeeAmount(feeAmount);
        notificationRequest.setFeePayer(feePayer);

        notificationRequest.setServiceCode(serviceCode);
        notificationRequest.setNotes(notes);
        notificationRequest.setSenderFcmKey(senderFcmKey);
        notificationRequest.setReceiverFcmKey(receiverFcmKey);

        notificationRequest.setRequest(requestId);
        notificationRequest.setRequest(request);
        notificationRequest.setResponse(response);

        return notificationRequest;
    }


    public NotificationRequest buildNotificationRequest(BigDecimal transactionAmount, String receiverWallet,
                                                        String senderWalletNo, String transactionToken, String serviceCode,
                                                        String feePayer, BigDecimal feeAmount, String request, String response,
                                                        String requestId, String notes,
                                                        String bankName,String bankAccountNumber) {

        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setTransactionAmount(transactionAmount);
        notificationRequest.setReceiverWallet(receiverWallet);
        notificationRequest.setSenderWalletNo(senderWalletNo);
        notificationRequest.setTransactionToken(transactionToken);

        notificationRequest.setFeeAmount(feeAmount);
        notificationRequest.setFeePayer(feePayer);

        notificationRequest.setServiceCode(serviceCode);
        notificationRequest.setNotes(notes);

        notificationRequest.setRequest(requestId);
        notificationRequest.setRequest(request);
        notificationRequest.setResponse(response);

        notificationRequest.setBankName(bankName);
        notificationRequest.setBankAccountNumber(bankAccountNumber);

        return notificationRequest;
    }

    public NotificationRequest buildNotificationRequest(BigDecimal transactionAmount, String receiverWallet,
                                                        String senderWalletNo, String transactionToken, String serviceCode,
                                                        String feePayer, BigDecimal feeAmount, String request, String response,
                                                        String requestId, String notes, String passportServiceName) {

        NotificationRequest notificationRequest = new NotificationRequest();
        notificationRequest.setRequestId(requestId);
        notificationRequest.setTransactionAmount(buildTransactionAmount(transactionAmount, 4));
        notificationRequest.setReceiverWallet(receiverWallet);
        notificationRequest.setSenderWalletNo(senderWalletNo);
        notificationRequest.setTransactionToken(transactionToken);

        notificationRequest.setFeeAmount(feeAmount);
        notificationRequest.setFeePayer(feePayer);

        notificationRequest.setServiceCode(serviceCode);
        notificationRequest.setNotes(notes);

        notificationRequest.setRequest(requestId);
        notificationRequest.setRequest(request);
        notificationRequest.setResponse(response);

        notificationRequest.setPinServiceType(passportServiceName);

        return notificationRequest;
    }

    BigDecimal buildTransactionAmount(BigDecimal bigDecimal, int decimalPlaces){
        return bigDecimal.setScale(decimalPlaces, BigDecimal.ROUND_DOWN);
    }
}
