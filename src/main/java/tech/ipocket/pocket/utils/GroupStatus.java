package tech.ipocket.pocket.utils;

public enum GroupStatus {
    ACTIVE(1),
    INACTIVE(0);

    private final Integer CODE;

    GroupStatus(Integer CODE) {
        this.CODE = CODE;
    }

    public Integer getCODE() {
        return this.CODE;
    }

    public String getKeyString() {
        return String.valueOf(CODE);
    }
}
