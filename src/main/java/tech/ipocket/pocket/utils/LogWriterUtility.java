package tech.ipocket.pocket.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogWriterUtility {

    Logger log;

    public LogWriterUtility(Class<?> clazz) {
        log = LogManager.getLogger(clazz);
    }

    public void trace(String requestId, String message) {
        log.trace("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nMessage: " + message + "\n" +
                "===========================================================");
    }

    public void debug(String requestId, String message) {
        log.debug("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nMessage: " + message + "\n" +
                "===========================================================");
    }

    public void error(String requestId, String message) {
        log.error("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nMessage: " + message + "\n" +
                "===========================================================");
    }

    public void error(String requestId, Throwable t) {
        log.error("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nMessage: " + t.getLocalizedMessage() + "\n" +
                "===========================================================");
        log.error(t);
    }

    public void info(String requestId, String message) {
        log.info("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nMessage: " + message + "\n" +
                "===========================================================");
    }

    public void info(String from, Object message){
        log.info("\n===================================================\n" +
                "From: " + from + "\nMessage: " + message + "\n" +
                "===========================================================");
    }

    public void error(String requestId, Exception exception) {
        log.warn("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nExecption: " + exception.getMessage() + "\n" +
                "===========================================================");
    }

    public void errorWithAnalysis(String requestId, Exception exception) {

        if (exception == null)
            return;
        String message = "No Message on error";
        StackTraceElement[] stackTrace = exception.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            message = "";
            for (StackTraceElement e : stackTrace) {
                message += "\n" + e.toString();
            }
        }
        log.error("\n===================================================\n" +
                "REQUESTID: " + requestId + "\nMessage: " + message + "\n" +
                "===========================================================");


    }

    public void warn(String requestId, String message) {
        log.error(requestId, message);
    }

    public boolean isTraceEnabled() {
        return log.isTraceEnabled();
    }

}