package tech.ipocket.pocket.utils;

public class ServiceCodeConstants {
    public static final String Fund_Transfer = "1001";
    public static final String Merchant_Payment = "1002";
    public static final String Top_Up = "1003";
    public static final String Master_Wallet_Deposit = "1004";
    public static final String Refill = "1007";
    public static final String AdminCashIn = "1005";
    public static final String AdminCashOut = "1006";
    public static final String CashIn = "1009";
    public static final String CashOut="1008";
    public static final String FundMovementWalletToWallet="1010";
    public static final String CashInFromBank = "1011";
    public static final String CashInFromBankIntermediate = "1012";
    public static final String CashOutToBank = "1013";
    public static final String CashOutToBankIntermediate = "1014";

    public static final String BillPay="1022";
    public static final String CashInFromUPayKiosk = "1015";
    public static final String CashInFromUPayKioskIntermediate = "1016";
    public static final String CashInToEmbassyForPassport = "1017";

    public static final String HelloSuperStar = "1018";

    public static final String CashInFromPaynetKiosk = "1019";
    public static final String CashInFromPaynetKioskIntermediate = "1020";

    public static final String CashInFromEasyPayKiosk = "1021";
    public static final String CashInFromEasyPayKioskIntermediate = "1022";

    public static final String CashInFromZenZeroKiosk = "1023";
    public static final String CashInFromZenZeroKioskIntermediate = "1024";
}
