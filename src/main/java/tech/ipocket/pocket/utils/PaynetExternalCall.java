package tech.ipocket.pocket.utils;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.PaynetConfig;
import tech.ipocket.pocket.entity.TsTransaction;
import tech.ipocket.pocket.repository.ts.paynet.ConfirmFindTransaction;
import tech.ipocket.pocket.request.ts.paynet.PaynetAuthorization;
import tech.ipocket.pocket.request.ts.paynet.PaynetBillPayRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.services.ts.paynetService.PaynetServiceImpl;

public class PaynetExternalCall {

    LogWriterUtility log = new LogWriterUtility(PaynetExternalCall.class);

    private String EXTERNAL_URL = "";
    public PaynetExternalCall(String externalUrl) {
        this.EXTERNAL_URL=externalUrl;
    }


    public ConfirmFindTransaction callPaynetTransaction(PaynetBillPayRequest paynetBillPayRequest, HttpHeaders httpHeaders, PaynetAuthorization paynetAuthorization, PaynetConfig paynetConfig, TsTransaction tsTransaction){

        return callToPaynetTransactionCheck_Confirm(paynetBillPayRequest, httpHeaders, paynetAuthorization, paynetConfig, tsTransaction);
    }

    private ConfirmFindTransaction callToPaynetTransactionCheck_Confirm(PaynetBillPayRequest request, HttpHeaders httpHeaders, PaynetAuthorization paynetAuthorization, PaynetConfig paynetConfig, TsTransaction tsTransaction) {
        BaseResponseObject responseObject = new BaseResponseObject();
        responseObject.setStatus(PocketConstants.ERROR);

        ConfirmFindTransaction responseObjectCheck = null;
        try {
            log.info(request.getRequestId(),"Calling Eternal Url : "+EXTERNAL_URL);
            log.info(request.getRequestId(),"Request : : "+CommonTasks.getObjectToString(request));

            TransactionActualRequest transactionActualRequest = transactionActualRequest(request, paynetAuthorization, EXTERNAL_URL, paynetConfig, tsTransaction);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ConfirmFindTransaction> exchange = restTemplate.exchange(EXTERNAL_URL, HttpMethod.POST, new HttpEntity<>(transactionActualRequest, httpHeaders), ConfirmFindTransaction.class);

            if(exchange.getStatusCode() != HttpStatus.OK){
                log.error(request.getRequestId(),"External call Failed. Status :"+exchange.getStatusCode());

                responseObject.setData(null);
                responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                responseObject.setStatus(PocketConstants.ERROR);
            }else{
                log.info(request.getRequestId(),"External call success");
            }

            responseObjectCheck = exchange.getBody();

            log.trace(request.getRequestId(),"Response from External :"+CommonTasks.getObjectToString(responseObject));

            if(responseObjectCheck==null || responseObjectCheck.transaction == null || responseObjectCheck.transaction.id == null){
                log.error("External API ", "External API cannot process.Response is null");

                responseObject=new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                responseObject.setData(null);
                responseObject.setStatus(PocketConstants.ERROR);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(request.getRequestId(), "External API call exception :"+ ex.getMessage());

            responseObject=new BaseResponseObject();
            responseObject.setData(null);
            responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
            responseObject.setStatus(PocketConstants.ERROR);
        }
        return responseObjectCheck;
    }

    public TransactionActualRequest transactionActualRequest(PaynetBillPayRequest paynetBillPayRequest, PaynetAuthorization paynetAuthorization, String EXTERNAL_URL, PaynetConfig paynetConfig, TsTransaction tsTransaction){
        TransactionActualRequest request = new TransactionActualRequest();

        if(EXTERNAL_URL.matches("https://api.paynet.one/transaction/find")){
            request.setSimulation(paynetBillPayRequest.getSimulation());
            request.setAuth(paynetAuthorization);
            request.setExternal_transaction_id(tsTransaction.getToken());
            request.setTransaction_id(tsTransaction.getRefTransactionToken());
        }else{
            request.setSimulation(paynetBillPayRequest.getSimulation());
            request.setExternal_transaction_id(paynetBillPayRequest.getExternalTransactionId());
            request.setTransaction_id(paynetBillPayRequest.getTransactionId());
            request.setProduct(paynetBillPayRequest.getProduct());

            Field fields = new Field();
            PaynetServiceImpl paynetServiceImpl = new PaynetServiceImpl();
            boolean isCheckedBillPayType = paynetServiceImpl.isCheckedBillPayType(paynetBillPayRequest);
            if(isCheckedBillPayType){
                fields = field(paynetBillPayRequest, fields);
            }

            request.setAuth(paynetAuthorization);
            request.setFields(fields);
        }
        return request;
    }

    public Field field(PaynetBillPayRequest paynetBillPayRequest, Field field){
        field.setCustomer(paynetBillPayRequest.getAccountNumber());
        field.setAccount(paynetBillPayRequest.getAccountNumber());
        field.setAmount(paynetBillPayRequest.getAmount());
        return field;
    }

}

class TransactionActualRequest{
    PaynetAuthorization auth;
    public int simulation;
    public String external_transaction_id;
    public String transaction_id;
    public String product;
    public Field fields;

    public PaynetAuthorization getAuth() {
        return auth;
    }

    public void setAuth(PaynetAuthorization auth) {
        this.auth = auth;
    }

    public int getSimulation() {
        return simulation;
    }

    public void setSimulation(int simulation) {
        this.simulation = simulation;
    }

    public String getExternal_transaction_id() {
        return external_transaction_id;
    }

    public void setExternal_transaction_id(String external_transaction_id) {
        this.external_transaction_id = external_transaction_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Field getFields() {
        return fields;
    }

    public void setFields(Field fields) {
        this.fields = fields;
    }
}
class Field{
    public String customer;
    public String account;
    public Double amount;

    public Field() {
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}




