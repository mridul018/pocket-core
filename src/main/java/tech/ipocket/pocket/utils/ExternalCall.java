package tech.ipocket.pocket.utils;

import com.google.gson.Gson;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.PayWellConfig;
import tech.ipocket.pocket.request.external.*;
import tech.ipocket.pocket.request.external.amy.FlightBookingRequest;
import tech.ipocket.pocket.request.external.amy.FlightIssueRequest;
import tech.ipocket.pocket.request.ts.paywell.PaywellRetailerBalanceRequest;
import tech.ipocket.pocket.request.ts.paywell.PaywellSingleTopupRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.paywell.PaywellRetailerBalanceResponse;
import tech.ipocket.pocket.response.ts.paywell.PaywellSingleTopupResponse;
import tech.ipocket.pocket.response.ts.paywell.PaywellTokenResponse;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;


public class ExternalCall{

    LogWriterUtility log = new LogWriterUtility(ExternalCall.class);

    private String EXTERNAL_URL = null;

    public ExternalCall(String externalUrl) {
        this.EXTERNAL_URL=externalUrl;
    }

    public BaseResponseObject sendEmail(String senderEmail, String emailBody, String emailSubject, String type){
        ExternalRequest request = new ExternalRequest();
        request.setTo(senderEmail);
        request.setBody(emailBody);
        request.setSubject(emailSubject);
        request.setType(type);

        return callToExternalService(request);
    }


    public BaseResponseObject sendSMS(String senderNumber, String smsBody, String type){
        return smsAndNotificationSend(senderNumber, smsBody, type);
    }

    public BaseResponseObject sendNotification(String toFCMKey, String message, String type){
        return smsAndNotificationSend(toFCMKey, message, type);
    }

    public BaseResponseObject sendTopUp(String skuCode, BigInteger amount, String senderNumber, String receiverNumber, String referenceNumber, String type,Double transferAmount){
        ExternalRequest request = new ExternalRequest();

        DingTopUpRequest dingTopUpRequest = new DingTopUpRequest();
        dingTopUpRequest.setRefNumber(referenceNumber);
        dingTopUpRequest.setSkuCode(skuCode);
        dingTopUpRequest.setTransferAmount(new BigDecimal(transferAmount));

        request.setAmount(amount);
        request.setFrom(senderNumber);
        request.setTo(receiverNumber);
        request.setType(type);
        request.setDingTopUpRequest(dingTopUpRequest);

        return callToExternalService(request);
    }

    public BaseResponseObject sendSSLTopUp(String client_id, String client_pass, String guid, BigInteger operator_id, String recipient_msisdn, BigInteger amount, String connection_type, String sender_id, BigInteger priority, String success_url, String failure_url, String type){
        ExternalRequest request = new ExternalRequest();

        SSLTopUpRequest sslTopUpRequest = new SSLTopUpRequest();
        sslTopUpRequest.setClient_id(client_id);
        sslTopUpRequest.setClient_pass(client_pass);
        sslTopUpRequest.setGuid(guid);
        sslTopUpRequest.setOperator_id(operator_id);
        sslTopUpRequest.setRecipient_msisdn(recipient_msisdn);
        sslTopUpRequest.setConnection_type(connection_type);
        sslTopUpRequest.setSender_id(sender_id);
        sslTopUpRequest.setPriority(priority);
        sslTopUpRequest.setSuccess_url(success_url);
        sslTopUpRequest.setFailure_url(failure_url);

        request.setAmount(amount);
        request.setType(type);
        request.setSslTopUpRequest(sslTopUpRequest);

        return callToExternalService(request);
    }

    public BaseResponseObject sendPayWellTopUp(String clientid, String clientpass, String crid, String senderMobileNumber, String msisdn, BigInteger amount, String connection, String operator, String sender, String type){
        ExternalRequest request = new ExternalRequest();

        PayWellTopUpRequest payWellTopUpRequest = new PayWellTopUpRequest();
        payWellTopUpRequest.setClientid(clientid);
        payWellTopUpRequest.setClientpass(clientpass);
        payWellTopUpRequest.setCrid(crid);
        payWellTopUpRequest.setMsisdn(msisdn);
        payWellTopUpRequest.setConnection(connection);
        payWellTopUpRequest.setOperator(operator);
        payWellTopUpRequest.setSender(sender);

        request.setType(type);
        request.setAmount(amount);
        request.setPayWellTopUpRequest(payWellTopUpRequest);
        request.setFrom(sender);

        return callToExternalService(request);
    }

    public BaseResponseObject sendPayWellPollibudditBill(String username, String password, String smsBillNumber, BigInteger amount, String ref_id,  String type, String accountNumber, String customerPhone, String logedInUserPhoneNumber, String fullName){
        ExternalRequest request = new ExternalRequest();

        PolliBudditBillRequest polliBudditBillRequest = new PolliBudditBillRequest();
        polliBudditBillRequest.setUsername(username);
        polliBudditBillRequest.setPassword(password);
        polliBudditBillRequest.setBill_no(smsBillNumber);
        polliBudditBillRequest.setRef_id(ref_id);
        polliBudditBillRequest.setFormat("json");
        polliBudditBillRequest.setAmount(amount.toString());
        polliBudditBillRequest.setTimestamp(DateUtil.getDateFormatWithPaywell());
        polliBudditBillRequest.setAccount_no(accountNumber);
        polliBudditBillRequest.setPhoneNo(customerPhone);
        polliBudditBillRequest.setFullName(fullName);

        request.setPolliBudditBillRequest(polliBudditBillRequest);
        request.setType(type);
        request.setTo(logedInUserPhoneNumber);


        return callToExternalService(request);
    }

    // API version 2.2 consuming
    public BaseResponseObject sendPayWellPollyBiddyutBillPayment(String username, String password, String smsBillNumber, BigInteger amount, String ref_id,  String type,
                                                                 String accountNumber, String customerPhone, String logedInUserPhoneNumber, String fullName,
                                                                 String billMonth, String billYear){
        ExternalRequest request = new ExternalRequest();

        PolliBudditBillRequest polliBudditBillRequest = new PolliBudditBillRequest();
        polliBudditBillRequest.setUsername(username);
        polliBudditBillRequest.setPassword(password);
        polliBudditBillRequest.setBill_no(smsBillNumber);
        polliBudditBillRequest.setRef_id(ref_id);
        polliBudditBillRequest.setFormat("json");
        polliBudditBillRequest.setAmount(amount.toString());
        polliBudditBillRequest.setTimestamp(DateUtil.getDateFormatWithPaywell());
        polliBudditBillRequest.setAccount_no(accountNumber);
        polliBudditBillRequest.setPhoneNo(customerPhone);
        polliBudditBillRequest.setFullName(fullName);
        polliBudditBillRequest.setBill_month(billMonth);
        polliBudditBillRequest.setBill_year(billYear);

        request.setPolliBudditBillRequest(polliBudditBillRequest);
        request.setType(type);
        request.setTo(logedInUserPhoneNumber);

        return callToExternalService(request);
    }

    public BaseResponseObject sendPayWellDPDCBillPay(BigInteger amount, String username, String password, String customerNo, String billYearMonth, String location, String payerMobileNo, String trx_id, String type){
        ExternalRequest request = new ExternalRequest();

        DPDCBillPayRequest dpdcBillPayRequest = new DPDCBillPayRequest();
        dpdcBillPayRequest.setUsername(username);
        dpdcBillPayRequest.setPassword(password);
        dpdcBillPayRequest.setBillYearMonth(billYearMonth);
        dpdcBillPayRequest.setCustomerNo(customerNo);
        dpdcBillPayRequest.setLocation(location);
        dpdcBillPayRequest.setPayerMobileNo(payerMobileNo);
        dpdcBillPayRequest.setTrx_id(trx_id);

        request.setDpdcBillPayRequest(dpdcBillPayRequest);
        request.setAmount(amount);
        request.setType(type);

        return callToExternalService(request);
    }

    public BaseResponseObject sendPayWellDESCOBillPay(String username, String password, String billNo, String payerMobileNo, String trx_id, BigInteger amount, String type){
        ExternalRequest request = new ExternalRequest();

        DESCOBillPayRequest descoBillPayRequest = new DESCOBillPayRequest();
        descoBillPayRequest.setBillNo(billNo);
        descoBillPayRequest.setPassword(password);
        descoBillPayRequest.setPayerMobileNo(payerMobileNo);
        descoBillPayRequest.setTrx_id(trx_id);
        descoBillPayRequest.setUsername(username);

        request.setDescoBillPayRequest(descoBillPayRequest);
        request.setAmount(amount);
        request.setType(type);

        return callToExternalService(request);
    }

    public BaseResponseObject sendPayWellWASABillPay(BigInteger amount, String username, String password, String billNo, String payerMobileNo, String trx_id, String type){
        ExternalRequest request = new ExternalRequest();

        WASABillPayRequest wasaBillPayRequest = new WASABillPayRequest();
        wasaBillPayRequest.setBillNo(billNo);
        wasaBillPayRequest.setPassword(password);
        wasaBillPayRequest.setPayerMobileNo(payerMobileNo);
        wasaBillPayRequest.setTrx_id(trx_id);
        wasaBillPayRequest.setUsername(username);

        request.setWasaBillPayRequest(wasaBillPayRequest);
        request.setAmount(amount);
        request.setType(type);

        return callToExternalService(request);
    }

    private BaseResponseObject smsAndNotificationSend(String senderNumber, String smsBody, String type) {
        ExternalRequest request = new ExternalRequest();
        request.setTo(senderNumber);
        request.setBody(smsBody);
        request.setType(type);

        return callToExternalService(request);
    }

    public BaseResponseObject flightBooking(FlightBookingRequest request, String type){
        ExternalRequest externalRequest = new ExternalRequest();

        externalRequest.setType(type);
        externalRequest.setFlightBookingRequest(request);

        return callToExternalService(externalRequest);
    }

    public BaseResponseObject flightIssue(FlightIssueRequest request, String type){
        ExternalRequest externalRequest = new ExternalRequest();

        externalRequest.setFlightIssueRequest(request);
        externalRequest.setType(type);

        return callToExternalService(externalRequest);
    }



    private BaseResponseObject callToExternalService(ExternalRequest request) {
        BaseResponseObject responseObject = new BaseResponseObject();
        responseObject.setStatus(PocketConstants.ERROR);
        try {

            log.info(request.getRequestId(),"Calling Eternal Url : "+EXTERNAL_URL);
            log.info(request.getRequestId(),"Request : : "+CommonTasks.getObjectToString(request));

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<BaseResponseObject> exchange = restTemplate.exchange(EXTERNAL_URL, HttpMethod.POST, new HttpEntity<>(request), BaseResponseObject.class);

            if(exchange.getStatusCode() != HttpStatus.OK){

                log.error(request.getRequestId(),"External call Failed. Status :"+exchange.getStatusCode());

                responseObject.setData(null);
                responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }
            log.info(request.getRequestId(),"External call success");

            responseObject = exchange.getBody();

            log.trace(request.getRequestId(),"Response from External :"+CommonTasks.getObjectToString(responseObject));

            if(responseObject==null){
                log.error("External API ", "External API cannot process.Response is null");

                responseObject=new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                responseObject.setData(null);
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }

            if(responseObject.getStatus()!=null&&responseObject.getStatus().equals(PocketConstants.OK)){
                log.info(request.getRequestId(),"External response successful.");
                responseObject.setStatus(PocketConstants.OK);
                responseObject.setError(null);
                return responseObject;
            }else{
                log.error(request.getRequestId(), "External API cannot process");
                responseObject.setError(responseObject.getError());
                responseObject.setData(null);
                responseObject.setStatus(PocketConstants.ERROR);

                return responseObject;
            }


        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(request.getRequestId(), "External API call exception :"+ ex.getMessage());

            responseObject=new BaseResponseObject();
            responseObject.setData(null);
            responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
            responseObject.setStatus(PocketConstants.ERROR);
        }

        return responseObject;

    }

    private void saveExternalActivityLog(ExternalRequest request){

    }

    public void sendMultyEmail(MultipleEmailRequest request) {

    }

    PayWellConfig payWellConfig = null;
    public BaseResponseObject sendPayWellSingleTopUp(PayWellConfig payWellConfigs, String crid, String senderMobileNumber,
                                                String msisdn, BigInteger amount, String connection, String operator, String sender, String type){
        ExternalRequest request = new ExternalRequest();
        payWellConfig = payWellConfigs;
        PayWellTopUpRequest payWellTopUpRequest = new PayWellTopUpRequest();
        payWellTopUpRequest.setClientid(payWellConfig.getClientId());
        payWellTopUpRequest.setClientpass(payWellConfig.getClientPassword());
        payWellTopUpRequest.setCrid(CommonTasks.uniqueRefIdGenerate());
        payWellTopUpRequest.setMsisdn(msisdn);
        payWellTopUpRequest.setConnection(connection);
        payWellTopUpRequest.setOperator(operator);
        payWellTopUpRequest.setSender(sender);

        request.setType(type);
        request.setAmount(amount);
        request.setPayWellTopUpRequest(payWellTopUpRequest);
        request.setFrom(sender);

        return callToPaywellExternalService(request);
    }

    private BaseResponseObject callToPaywellExternalService(ExternalRequest request) {
        BaseResponseObject responseObject = new BaseResponseObject();
        responseObject.setStatus(PocketConstants.ERROR);
        try {
            log.info(request.getRequestId(),"Calling External Url : "+payWellConfig.getGetToken());
            log.info(request.getRequestId(),"Request : : "+CommonTasks.getObjectToString(request));

            HttpHeaders tokenHeaders = CommonTasks.createHeaders(payWellConfig.getApiUserName(), payWellConfig.getApiPassword(), null, null, null);

            PaywellTokenResponse paywellTokenResponse = null;
            RestTemplate tokenRestTemplate = new RestTemplate();
            ResponseEntity<PaywellTokenResponse> tokenExchange = tokenRestTemplate.exchange(payWellConfig.getGetToken(), HttpMethod.POST,new HttpEntity<>(tokenHeaders), PaywellTokenResponse.class);
            if(tokenExchange.getStatusCode() != HttpStatus.OK){
                log.error(request.getRequestId(),"Paywell security token call failed. Status :"+tokenExchange.getStatusCode());
                responseObject.setData(null);
                responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }else{
                log.info(request.getRequestId(),"Paywell security token call success");
                paywellTokenResponse = tokenExchange.getBody();
                log.info(request.getRequestId(),"Security token response : "+CommonTasks.getObjectToString(paywellTokenResponse));
            }

            PaywellRetailerBalanceResponse retailerBalanceResponse = null;
            if(paywellTokenResponse != null){
                log.info(request.getRequestId(),"Calling Eternal Url : "+payWellConfig.getRetailerBalance());

                PaywellRetailerBalanceRequest retailerBalanceRequest = new PaywellRetailerBalanceRequest();
                retailerBalanceRequest.setUsername(request.getPayWellTopUpRequest().getClientid());
                retailerBalanceRequest.setPassword(request.getPayWellTopUpRequest().getClientpass());

                String paywellRetailerBalanceRequest = new Gson().toJson(retailerBalanceRequest);
                String balanceRequestHashedData = CommonTasks.requestHashedDataGenerate(paywellRetailerBalanceRequest, payWellConfig.getEncryptionkey().getBytes());

                RestTemplate balanceRestTemplate = new RestTemplate();
                MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
                mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
                balanceRestTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

                HttpHeaders bearerBalanceTokenHeaders = CommonTasks.createHeaders(null, null,paywellTokenResponse.getToken().getSecurity_token(),
                        payWellConfig.getApiKey(), balanceRequestHashedData);

                ResponseEntity<PaywellRetailerBalanceResponse> balanceExchange = balanceRestTemplate.exchange(payWellConfig.getRetailerBalance(), HttpMethod.POST,
                        new HttpEntity<>(retailerBalanceRequest, bearerBalanceTokenHeaders), PaywellRetailerBalanceResponse.class);
                if(balanceExchange.getStatusCode() != HttpStatus.OK){
                    log.error(request.getRequestId(),"Paywell retailer balance call failed. Status :"+balanceExchange.getStatusCode());
                    responseObject.setData(null);
                    responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                    responseObject.setStatus(PocketConstants.ERROR);
                    return responseObject;
                }else{
                    log.info(request.getRequestId(),"Paywell retailer balance call success");
                    retailerBalanceResponse = balanceExchange.getBody();
                    log.info(request.getRequestId(),"Retailer balance response : "+CommonTasks.getObjectToString(retailerBalanceResponse));
                }
            }else {
                log.error("External API ", "External API cannot process.Security token generated is null");
                responseObject=new BaseResponseObject();
                ErrorObject errorObject = new ErrorObject();
                errorObject.setErrorCode(paywellTokenResponse.getStatus());
                errorObject.setErrorMessage(paywellTokenResponse.getMessage());
                responseObject.setError(errorObject);
                responseObject.setData(null);
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }

            PaywellSingleTopupResponse paywellSingleTopupResponse = null;
            int compareValue = 0;
            if(retailerBalanceResponse != null && retailerBalanceResponse.getBalanceData() != null && !retailerBalanceResponse.getBalanceData().getBalance().equals("")
                    && retailerBalanceResponse.getBalanceData().getBalance() != null){
                compareValue = new BigInteger(retailerBalanceResponse.getBalanceData().getBalance().split("\\.")[0]).compareTo(request.getAmount());
            }

            if(retailerBalanceResponse != null && compareValue == 1){
                PaywellSingleTopupRequest paywellSingleTopupRequest = new PaywellSingleTopupRequest();
                paywellSingleTopupRequest.setUsername(request.getPayWellTopUpRequest().getClientid());
                paywellSingleTopupRequest.setPassword(request.getPayWellTopUpRequest().getClientpass());
                paywellSingleTopupRequest.setRef_id(request.getPayWellTopUpRequest().getCrid());
                paywellSingleTopupRequest.setMsisdn(request.getPayWellTopUpRequest().getMsisdn());
                paywellSingleTopupRequest.setAmount(request.getAmount());
                paywellSingleTopupRequest.setCon_type(request.getPayWellTopUpRequest().getConnection());
                paywellSingleTopupRequest.setOperator(request.getPayWellTopUpRequest().getOperator());
                paywellSingleTopupRequest.setTrxId("");

                String singleTopUpRequest = new Gson().toJson(paywellSingleTopupRequest);
                String singleTopUpRequestHashedData = CommonTasks.requestHashedDataGenerate(singleTopUpRequest, payWellConfig.getEncryptionkey().getBytes());

                log.info(request.getRequestId(),"Calling External Url : "+payWellConfig.getSingleTopup());
                log.info(request.getRequestId(),"Request : : "+CommonTasks.getObjectToString(paywellSingleTopupRequest));

                RestTemplate singleTopUpRestTemplate = new RestTemplate();
                MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter2 = new MappingJackson2HttpMessageConverter();
                mappingJackson2HttpMessageConverter2.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
                singleTopUpRestTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter2);

                HttpHeaders bearerTopUpTokenHeaders = CommonTasks.createHeaders(null, null, paywellTokenResponse.getToken().getSecurity_token(),
                        payWellConfig.getApiKey(), singleTopUpRequestHashedData);

                ResponseEntity<PaywellSingleTopupResponse> singleTopUpExchange = singleTopUpRestTemplate.exchange(payWellConfig.getSingleTopup(), HttpMethod.POST,
                        new HttpEntity<>(paywellSingleTopupRequest, bearerTopUpTokenHeaders), PaywellSingleTopupResponse.class);
                if(singleTopUpExchange.getStatusCode() != HttpStatus.OK){
                    log.error(request.getRequestId(),"Paywell singleTopUp call failed. Status :"+singleTopUpExchange.getStatusCode());
                    responseObject.setData(null);
                    responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                    responseObject.setStatus(PocketConstants.ERROR);
                }else{
                    log.info(request.getRequestId(),"Paywell singleTopUp call success");
                    paywellSingleTopupResponse = singleTopUpExchange.getBody();
                    log.info(request.getRequestId(),"Single Top-up response : "+CommonTasks.getObjectToString(paywellSingleTopupResponse));
                }

            }else{
                log.error("External API ", "External API cannot process.Retailer balance is null or too less");
                responseObject=new BaseResponseObject();
                ErrorObject errorObject = new ErrorObject();
                errorObject.setErrorCode(""+400);
                errorObject.setErrorMessage("External topup error");
                responseObject.setError(errorObject);
                responseObject.setData(null);
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }

            log.trace(request.getRequestId(),"Response from External :"+CommonTasks.getObjectToString(paywellSingleTopupResponse));

            if(paywellSingleTopupResponse==null){
                log.error("External API ", "External API cannot process.Response is null");
                responseObject=new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
                responseObject.setData(null);
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }

            if(paywellSingleTopupResponse != null && paywellSingleTopupResponse.getData().getStatus()!=null
                    && paywellSingleTopupResponse.getData().getStatus().equals(PocketConstants.OK)){
                log.info(request.getRequestId(),"External response successful.");
                responseObject.setStatus(PocketConstants.OK);
                responseObject.setError(null);
                responseObject.setData(paywellSingleTopupResponse);
                return responseObject;
            }else{
                log.error(request.getRequestId(), "External API cannot process");
                ErrorObject errorObject = new ErrorObject();
                errorObject.setErrorCode(paywellSingleTopupResponse.getData().getStatus());
                errorObject.setErrorMessage(paywellSingleTopupResponse.getData().getMessage());
                responseObject.setError(errorObject);
                responseObject.setData(paywellSingleTopupResponse);
                responseObject.setStatus(PocketConstants.ERROR);
                return responseObject;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error(request.getRequestId(), "External API call exception :"+ ex.getMessage());

            responseObject=new BaseResponseObject();
            responseObject.setData(null);
            responseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_SERVICE_ERROR));
            responseObject.setStatus(PocketConstants.ERROR);
        }

        return responseObject;
    }

}
