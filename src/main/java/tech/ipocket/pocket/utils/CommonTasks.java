package tech.ipocket.pocket.utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.GsonBuilder;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.utils.constants.UserTypeConstants;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CommonTasks {
    private static LogWriterUtility logWriterUtility = new LogWriterUtility(CommonTasks.class);

    public static String getObjectToString(Object o) {
        if(o==null){
            return "";
        }
        return new GsonBuilder().setPrettyPrinting().create().toJson(o);
    }

    public static void printObjectByRequestId(Object request, String requestId) {
        try {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(request);
            logWriterUtility.trace(requestId, "Response : \n" + json);
        } catch (Exception e) {
            logWriterUtility.trace(requestId, "Data parsing failed.");
        }
    }

    public static String getRefNumber(){
        return RandomGenerator.getInstance().generateTransactionTokenAsString();
    }

    public static String getOperatorForPayWellTopUp(String skuCode) {

        if(skuCode.contains("BD_GE")){
            return "GP";
        }else if(skuCode.contains("BD_RL")){
            return "RB";
        }else if(skuCode.contains("BD_BG")){
            return "BL";
        }else if(skuCode.contains("BD_AX")){
            return "AT";
        }else if(skuCode.contains("BD_TQ")){
            return "TT";
        }else{
            return null;
        }
        /*
        switch (skuCode){
            case "BD_GE_TopUp":
            case "BD_GE_TopUp_50.00":
                return "GP";
            case "BD_RL_TopUp":
            case "BD_RL_TopUp_50.00":
                return "RB";
            case "BD_BG_TopUp":
            case "BD_BG_TopUp_50.00":
                return "BL";
            case "BD_AX_TopUp":
            case "BD_AX_TopUp_50.00":
                return "AT";
            case "BD_TQ_TopUp":
            case "BD_TQ_TopUp_50.00":
                return "TT";

        }

        return null;*/
    }

    public static String byteToString(byte[] data){
        StringBuffer hash = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            String hex = Integer.toHexString(0xFF & data[i]);
            if (hex.length() == 1) {
                hash.append('0');
            }
            hash.append(hex);
        }
        return hash.toString();
    }

    public static String getGlNameByCustomerType(TsClient receiverCustomerClient) {

        if(receiverCustomerClient==null){
            return null;
        }
        switch (receiverCustomerClient.getGroupCode()){
            case UserTypeConstants.CUSTOMER:
                return GlConstants.Deposit_of_Customer;
            case UserTypeConstants.AGENT:
                return GlConstants.Deposit_of_Agent;
            case UserTypeConstants.MERCHANT:
                return GlConstants.Deposit_of_Merchant;
            case UserTypeConstants.Master:
                return GlConstants.Deposit_of_Master;
        }


        return null;
    }

    public static String getIPAddress() {

        return InetAddress.getLoopbackAddress().getHostAddress();
    }


    public static List<String> getPredefinedGroups() {
        List<String> groups=new ArrayList<>();
        groups.add(UmEnums.UserGroup.MERCHANT.getGroupCode());
        groups.add(UmEnums.UserGroup.AGENT.getGroupCode());
        groups.add(UmEnums.UserGroup.CUSTOMER.getGroupCode());
        groups.add(UmEnums.UserGroup.SR.getGroupCode());
        groups.add(UmEnums.UserGroup.SUPER_ADMIN.getGroupCode());
        groups.add("2001");
        groups.add(UmEnums.UserGroup.ADMIN.getGroupCode());

        return groups;
    }

    public static String requestHashedDataGenerate(String requestObject, byte[] encryptionKey)throws InvalidKeyException, NoSuchAlgorithmException {
        byte[] bytes = hmac("HmacSHA256", encryptionKey, requestObject.getBytes());
        return bytesToHex(bytes);
    }

    static byte[] hmac(String sha256, byte[] encryptionKey, byte[] requestObject) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac mac = Mac.getInstance(sha256);
        mac.init(new SecretKeySpec(encryptionKey, sha256));
        return mac.doFinal(requestObject);
    }

    static String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789abcdef".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0, v; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String uniqueRefIdGenerate(){
        UUID uuid = UUID.randomUUID();
        String dashLess = String.valueOf(uuid).replaceAll("\\-", "");
        return dashLess.substring(dashLess.length() - 15).toUpperCase();
    }

    public static HttpHeaders createHeaders(String apiUserName, String apiPassword, String securityToken, String apiKey, String requestObjectHashedData){
        return new HttpHeaders() {{
            String auth;
            String authHeader = null;
            byte[] encodedAuth;
            if(apiUserName!=null && apiPassword!=null){
                auth = apiUserName + ":" + apiPassword;
                encodedAuth = org.apache.commons.codec.binary.Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
                authHeader = "Basic " + new String( encodedAuth );
            }
            if(securityToken!=null && apiKey!=null && requestObjectHashedData!=null){
                auth = securityToken + ":" + apiKey + ":" + requestObjectHashedData;
                encodedAuth = org.apache.commons.codec.binary.Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
                authHeader = "Bearer " + new String( encodedAuth );
            }
            set( "Content-Type", String.valueOf(MediaType.APPLICATION_JSON));
            set( "Authorization", authHeader );
        }};
    }

}