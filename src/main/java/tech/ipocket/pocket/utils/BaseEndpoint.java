package tech.ipocket.pocket.utils;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.integration.UPayBaseRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayBaseRequest;
import tech.ipocket.pocket.request.integration.hss.HssBaseRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetBaseRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayBaseRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;

public class BaseEndpoint {

    protected void validateRequest(BaseRequestObject request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
        if (request.getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }

        request.validate(baseResponseObject);

        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }

    protected void validateRequest(UPayBaseRequest request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getRequestId() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UPAY_USER_REQUESTID_REQUIRED));
            return;
        }
        if (request.getRequestDateTime() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UPAY_TRANSACTION_DATE_TIME_REQUIRED));
            return;
        }

        request.validate(baseResponseObject);

        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }


    protected void validateRequest(HssBaseRequest request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getRequestId() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.HSS_USER_REQUESTID_REQUIRED));
            return;
        }
        if (request.getRequestDateTime() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.HSS_PAYMENT_DATE_TIME_REQUIRED));
            return;
        }

        request.validate(baseResponseObject);

        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }

    protected void validateRequest(PaynetBaseRequest request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getRequestId() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PAYNET_USER_REQUEST_ID_REQUIRED));
            return;
        }
        if (request.getRequestDateTime() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PAYNET_TRANSACTION_DATE_TIME_REQUIRED));
            return;
        }

        request.validate(baseResponseObject);

        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }
    protected void validateRequest(EasyPayBaseRequest request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getRequestId() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EASYPAY_USER_REQUEST_ID_REQUIRED));
            return;
        }
        if (request.getRequestDateTime() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EASYPAY_TRANSACTION_DATE_TIME_REQUIRED));
            return;
        }

        request.validate(baseResponseObject);

        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }

    protected void validateRequest(ZenZeroPayBaseRequest request, BaseResponseObject baseResponseObject) throws PocketException {
        if (request == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        if (request.getRequestId() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ZENZERO_USER_REQUEST_ID_REQUIRED));
            return;
        }
        if (request.getRequestDateTime() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ZENZERO_TRANSACTION_DATE_TIME_REQUIRED));
            return;
        }

        request.validate(baseResponseObject);

        if (baseResponseObject.getError() != null) {
            throw new PocketException(baseResponseObject.getError().getErrorCode(), baseResponseObject.getError().getErrorMessage());
        }
    }

}
