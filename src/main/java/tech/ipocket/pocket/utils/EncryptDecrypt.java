package tech.ipocket.pocket.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.um.LoginRequest;
import tech.ipocket.pocket.response.BaseResponseObject;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.Valid;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class EncryptDecrypt {

    static String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJortHnMh442/bqvrgeRGKFLiZPvS6Kpp11D/swA82OQJKQQqgIc75h3U4NzOlpVLxFbkIsWU6PU9+vDKofgJ8q54YP3eyOWGztvxTYauWBVnCs9SDAneZk3ii9HvZuLNeA23FbUVt54kbL9I79pXrclMtgtM981Na1G+7ygsV1BAgMBAAECgYBlDPiCTPkc0haE4tmixAvszOIkQNjGiYTVsNe1R3pAbB5YG0QTlnvE55Gpu0EoLGVpbybSw5OW8uR9d7LM1LO/Nw+3s6/bDcIv3+0bfX+OL7Y5zvM7VCTMDtDT16DD6yjlW4VtXck7OYOtVWshL7w8fbcgRqeHuz2Q7QrkwqDNAQJBAPQyyH40moLAYqMjqANstD1zpHFE0lpjEAMNp9yZCffHegucezRNif2fXxIT8g6/eLd1QglhdcgBg4ZzAQN7CskCQQChnxoLQ6J6SA1yFlGY/FzcLbbZGB20Gs1EDC/VbzWPcgT6PrtytErnm7rel21EtMS/Af2ckHXy8/cmzTvIqQK5AkAatH8GhjwrdlyTGLAkB7xCSCZAmNUkO1ch7llgH4Bej3sCUA+NLbmcedRMxs+e20OZVIJvQjA+OiQnDOacedU5AkA7EXrjQwTOzCad26pWz2Tg05RQI5A3ktQSWqHgz5QzqOhIzUSU12/I1hGl6JH22lpNtNyLrABvmtppQ/jljV+BAkBWzp4J24GxaMNcMrIl0Br0wgRWofiU03msh5PrFlND2/9fQHysQUN3rOkLz5ixFv6XwcYuJlQNooLnBqgHlEsg";
    static final String publicKeyString = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCaK7R5zIeONv26r64HkRihS4mT70uiqaddQ/7MAPNjkCSkEKoCHO+Yd1ODczpaVS8RW5CLFlOj1PfrwyqH4CfKueGD93sjlhs7b8U2GrlgVZwrPUgwJ3mZN4ovR72bizXgNtxW1FbeeJGy/SO/aV63JTLYLTPfNTWtRvu8oLFdQQIDAQAB";


    private static String decrypt(String encryptedTextString, String encryptedSecretKeyString) {

        try {

            // 1. Get private key
            PKCS8EncodedKeySpec privateSpec = new PKCS8EncodedKeySpec(Base64.decode(privateKey));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = keyFactory.generatePrivate(privateSpec);

            // 2. Decrypt encrypted secret key using private key
            Cipher cipher1 = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
            cipher1.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] secretKeyBytes = cipher1.doFinal(Base64.decode(encryptedSecretKeyString));
            SecretKey secretKey = new SecretKeySpec(secretKeyBytes, 0, secretKeyBytes.length, "AES");

            // 3. Decrypt encrypted text using secret key
            byte[] raw = secretKey.getEncoded();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
            byte[] original = cipher.doFinal(Base64.decode(encryptedTextString));
            String text = new String(original, Charset.forName("UTF-8"));

            // 4. Print the original text sent by client
            System.out.println("text\n" + text + "\n\n");

            return text;

        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static BaseResponseEncrypted encrypt(String plainText) {


        BaseResponseEncrypted responseEncrypted = new BaseResponseEncrypted();

        try {

            // 1. generate secret key using AES
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(128); // AES is currently available in three key sizes: 128, 192 and 256 bits.The design and strength of all key lengths of the AES algorithm are sufficient to protect classified information up to the SECRET level
            SecretKey secretKey = keyGenerator.generateKey();

            // 2. get string which needs to be encrypted
            String text = plainText;

            // 3. encrypt string using secret key
            byte[] raw = secretKey.getEncoded();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(new byte[16]));
            String cipherTextString = Base64.encode(cipher.doFinal(text.getBytes(Charset.forName("UTF-8"))));

            // 4. get public key
            X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(Base64.decode(publicKeyString));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey publicKey = keyFactory.generatePublic(publicSpec);

            // 6. encrypt secret key using public key
            Cipher cipher2 = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
            cipher2.init(Cipher.ENCRYPT_MODE, publicKey);
            String encryptedSecretKey = Base64.encode(cipher2.doFinal(secretKey.getEncoded()));

            // 7. pass cipherTextString (encypted sensitive data) and encryptedSecretKey to your server via your preferred way.
            // Tips:
            // You may use JSON to combine both the strings under 1 object.
            // You may use a volley call to send this data to your server.

            responseEncrypted.setEncryptedTextString(cipherTextString);
            responseEncrypted.setEncryptedSecretKeyString(encryptedSecretKey);

        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        }
        return responseEncrypted;
    }

    public static BaseRequestObject decryptRequest(BaseRequestObject request, Type type){
        if(PocketConstants.SECURITY_ON) {
            return new Gson().fromJson(decrypt(request.getEncryptedTextString(), request.getEncryptedSecretKeyString()), type);

        }else{
            return request;
        }
    }

    public static BaseResponseObject encryptResponse(BaseResponseObject responseObject){
        if(PocketConstants.SECURITY_ON){
            responseObject.setData(encrypt(new Gson().toJson(responseObject.getData())));
            return responseObject;
        }else{
            return responseObject;
        }
    }
}
