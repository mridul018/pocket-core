package tech.ipocket.pocket.utils;

import tech.ipocket.pocket.request.external.ExternalRequest;

public class ExternalType {
    public static final String EMAIL = "100001";
    public static final String SMS = "100002";
    public static final String NOTIFICATION = "100003";
    public static final String TOPUP_DING = "100004";
    public static final String SSL_TOPUP = "100005";
    public static final String PAYWELL_TOPUP = "100006";
    public static final String POLLIBUDDIT_BILLPAY = "100007";
    public static final String DESCO_BILLPAY = "100008";
    public static final String DPDC_BILLPAY = "100009";
    public static final String WASA_BILLPAY = "100010";
    public static final String CUSTOMER_REGISTRATION = "100011";
    public static final String FLIGHT_BOOKING = "100012";
    public static final String FLIGHT_ISSUE = "100013";

    public static final String POLLIBUDDIT_BILLPAY_CONFIRMATION = "100014";
    public static final String AMY_SESSION="100015";
}
