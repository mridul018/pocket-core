package tech.ipocket.pocket.utils;

public class GroupCodeUtils {
    public static final String CUSTOMER = "1001";
    public static final String AGENT = "1002";
    public static final String MERCHANT = "1003";
    public static final String ADMIN = "1004";
    public static final String SUPER_ADMIN = "1005";
}
