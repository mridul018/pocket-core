package tech.ipocket.pocket.utils;

public enum TsEnums {
    ;

    public enum UserType {

        CUSTOMER("1001"),
        AGENT("1002"),
        MERCHANT("1003"),
        SR("1006"),
        Master("2001");
        private final String userTypeString;

        UserType(String userTypeString) {
            this.userTypeString = userTypeString;
        }

        public String getUserTypeString() {
            return userTypeString;
        }
    }

    public enum Services {

        Fund_Transfer("1001"),
        Merchant_Payment("1002"),
        Top_Up("1003"),
        Master_Wallet_Deposit("1004"),
        AdminCashIn("1005"),
        Refill("1007"),
        CashIn("1009"),
        Reversal("1210"),
        CashInFromBank("1011"),
        CashOutToBank("1013"),
        BillPay("1022"),
        Intermediate("1023"),
        Commission("1024"),
        CurrencyConversion("1025"),
        CashOut("1008"),
        Refund("1026"),
        CashInFromBankIntermediate("1012"),
        CashInFromUPayKiosk("1015"),
        CashInFromUPayKioskIntermediate("1016"),

        CashInFromPaynetKiosk("1019"),
        CashInFromUPaynetKioskIntermediate("1020"),
        CashInFromEasyPayKiosk("1021"),
        CashInFromEasyPayKioskIntermediate("1022"),
        CashInFromZenZeroKiosk("1023"),
        CashInFromZenZeroKioskIntermediate("1024");

        private final String serviceCode;

        Services(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public static String getServiceNameByCode(String transactionType) {
            Services[] values=values();

            for (int i = 0; i < values.length; i++) {
                Services value=values[i];
                if(value.getServiceCode().equals(transactionType)){
                    return value.toString();
                }
            }
            return transactionType;
        }

        public String getServiceCode() {
            return serviceCode;
        }
    }


    public enum UserStatus {
        INITIALIZED("1"),
        ACTIVE("2"),
        DELETED("5");
        private final String userStatusType;

        UserStatus(String userStatusType) {
            this.userStatusType = userStatusType;
        }

        public String getUserStatusType() {
            return userStatusType;
        }
    }

    public enum EodStatus {
        INITIALIZE("0"),
        REVERSED("1"),
        COMPLETED("2");
        private final String eodStatus;

        EodStatus(String eodStatus) {
            this.eodStatus = eodStatus;
        }

        public String getEodStatus() {
            return eodStatus;
        }
    }

    public enum TransactionStatus {

        INITIALIZE("0"),
        REVERSED("1"),
        COMPLETED("2"),
        FAILED("3"),
        INCOMPLETE("4"),
        REFUND("5");
        private final String transactionStatus;

        TransactionStatus(String transactionStatus) {
            this.transactionStatus = transactionStatus;
        }

        public static String getNameByValue(String transactionStatus) {

            TransactionStatus[] values=values();

            for (int i = 0; i < values.length; i++) {
                TransactionStatus value=values[i];
                if(value.transactionStatus.equals(transactionStatus)){
                    return value.toString();
                }
            }


            return transactionStatus;
        }

        public String getTransactionStatus() {
            return transactionStatus;
        }
    }

    public enum TransactionDrCrType {

        DEBIT("DR"),
        CREDIT("CR");
        private final String transactionDrCrType;

        TransactionDrCrType(String transactionDrCrType) {
            this.transactionDrCrType = transactionDrCrType;
        }

        public String getTransactionDrCrType() {
            return transactionDrCrType;
        }
    }

    public enum FeeCode {

        AED_BD_CONVERTION("2090"),
        TOPUP_CONVERSION_DISTRIBUTION_AGENT_APP("3004"),
        TOPUP_CONVERSION_DISTRIBUTION_CUSTOMER_APP("3005"),
        TOPUP_LOCAL_CUSTOMER_APP("3000"),
        TOPUP_LOCAL_AGENT_APP("3001"),
        TOPUP_BD_CUSTOMER_APP("3002"),
        TOPUP_BD_AGENT_APP("3003"),
        BILLPAY_PALLI_BIDDUT_BD_AGENT_APP("4000"),
        BILLPAY_PALLI_BIDDUT_BD_CUSTOMER_APP("4001"),
        BILLPAY_CONVERSION_DISTRIBUTION_AGENT_APP("4002"),
        BILLPAY_CONVERSION_DISTRIBUTION_CUSTOMER_APP("4003"),

        BILLPAY_SALIK_BD_AGENT_APP("5000"),
        BILLPAY_SALIK_BD_CUSTOMER_APP("5001"),
        BILLPAY_NOL_CARD_BD_AGENT_APP("5002"),
        BILLPAY_NOL_CARD_BD_CUSTOMER_APP("5003");
        private final String feeCode;

        FeeCode(String feeCode) {
            this.feeCode = feeCode;
        }


        public String getFeeCode() {
            return feeCode;
        }
    }

}
