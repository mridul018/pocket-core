package tech.ipocket.pocket.utils;

public enum UmEnums {
    ;


    public enum UserGroup {
        CUSTOMER("1001"),
        AGENT("1002"),
        MERCHANT("1003"),
        ADMIN("1004"),
        SUPER_ADMIN("1005"),
        SR("1006");
        private final String groupCode;

        UserGroup(String groupCode) {
            this.groupCode = groupCode;
        }

        public static boolean isUserHasWallet(String groupCode) {

            switch (groupCode) {
                case "1001":
                case "1002":
                case "1003":
                case "1006":
                    return true;
                default:
                    return false;
            }
        }

        public String getGroupCode() {
            return groupCode;
        }

        public static String getGroupNameByCode(String groupCode) {
            for (UserGroup userGroup : UserGroup.values()) {
                if (userGroup.getGroupCode().equalsIgnoreCase(groupCode)) {
                    return userGroup.name();
                }
            }
            return "N/A";
        }
    }

    public enum PrimaryIdTypes {
        //        1=Passport, 2=NID, 3=Driving License, 4=TIN
        Passport("1"),
        NID("2"),
        Driving_License("3"),
        TIN("4");

        private final String primaryIdTypes;

        PrimaryIdTypes(String primaryIdTypes) {
            this.primaryIdTypes = primaryIdTypes;
        }

        public String getPrimaryIdTypes() {
            return primaryIdTypes;
        }

        public static String getPrimaryIdTypesByCode(String primaryIdTypes) {
            for (PrimaryIdTypes status : PrimaryIdTypes.values()) {
                if (status.getPrimaryIdTypes().equalsIgnoreCase(primaryIdTypes)) {
                    return status.name();
                }
            }

            return primaryIdTypes;
        }
    }


    public enum UserStatus {
        Initiation(1),
        Verified(2),
        Blocked(3),
        HardBlocked(4),
        Deleted(5);
        private final int userStatusType;

        UserStatus(int userStatusType) {
            this.userStatusType = userStatusType;
        }

        public static String getStatusNameByStatus(Integer walletStatus) {
            for (UserStatus status : UserStatus.values()) {
                if (status.getUserStatusType() == walletStatus) {
                    return status.name();
                }
            }

            return null;
        }

        public static boolean isUserStatusValid(int providedStatus) {
            for (UserStatus status : UserStatus.values()) {
                if (status.getUserStatusType() == providedStatus) {
                    return true;
                }
            }
            return false;
        }

        public int getUserStatusType() {
            return userStatusType;
        }
    }

    public enum DeviceMapStatus {
        Active("1"),
        Deleted("2");
        private final String mapStatus;

        DeviceMapStatus(String mapStatus) {
            this.mapStatus = mapStatus;
        }

        public String getMapStatus() {
            return mapStatus;
        }
    }

    public enum OtpStatus {
        UnUsed("1"),
        Used("2");
        private final String otpStatus;

        OtpStatus(String otpStatus) {
            this.otpStatus = otpStatus;
        }

        public String getOtpStatus() {
            return otpStatus;
        }
    }

    public enum OtpTypes {
        FundTransafer("1001"),
        OtpForVerification("1002");
        private final String otpType;

        OtpTypes(String otpType) {
            this.otpType = otpType;
        }

        public String getOtpType() {
            return otpType;
        }
    }

    public enum ActivityLogType {
        DeviceAdd("1"),
        Registration("2"),
        LoginFailedTry("3"),
        LoginSuccessTry("4"),
        ForgotPinInitiate("5"),
        ForgotPinSuccess("6"),
        ResendOtp("7"),
        ChangePin("8"),
        CUSTOMER_STATUS_CHANGE("9"),
        TYPE_SUCCESSFUL_LOGIN("10"),
        TYPE_FAILED_LOGIN("11");
        private final String activityLogType;

        ActivityLogType(String activityLogType) {
            this.activityLogType = activityLogType;
        }

        public static String getLogNameByType(String logType) {

            for (ActivityLogType activityLogType : ActivityLogType.values()
            ) {
                if (activityLogType.getActivityLogType().equalsIgnoreCase(logType)) {
                    return activityLogType.name();
                }
            }

            return logType;
        }

        public String getActivityLogType() {
            return activityLogType;
        }
    }

    public enum ActivityLogStatus {
        Initialize("1"),
        Deleted("2");
        private final String activityLogStatus;

        ActivityLogStatus(String activityLogStatus) {
            this.activityLogStatus = activityLogStatus;
        }

        public String getActivityLogStatus() {
            return activityLogStatus;
        }
    }

    public enum DocumentTypes {
        ProfilePicture("1"),
        NID("2"),
        TRADE_LICENSE("3");
        private final String documentType;

        DocumentTypes(String documentType) {
            this.documentType = documentType;
        }

        public String getDocumentType() {
            return documentType;
        }
    }
}
