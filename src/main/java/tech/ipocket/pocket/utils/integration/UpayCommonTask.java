package tech.ipocket.pocket.utils.integration;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class UpayCommonTask {

    public static PublicKey getPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = publicKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decode);
        return keyFactory.generatePublic(keySpec);
    }

    public static PrivateKey getPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = privateKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decode);
        return keyFactory.generatePrivate(keySpec);
    }

    private static byte[] privateKeyDecode() throws IOException {
        Path keyPath = Paths.get("/root/keys/private.pem");
        String key = new String(Files.readAllBytes(keyPath), Charset.defaultCharset());
        String privateKeyPem = key
                .replace("-----BEGIN PRIVATE KEY-----","")
                .replaceAll(System.lineSeparator(),"")
                .replace("-----END PRIVATE KEY-----","");

        System.out.println("Private Key: "+ privateKeyPem);

        return Base64.decodeBase64(privateKeyPem);
    }

    private static byte[] publicKeyDecode() throws IOException {
        Path path = Paths.get("/root/keys/public.pem");
        String key = new String(Files.readAllBytes(path), Charset.defaultCharset());
        String publicKeyPem = key
                .replace("-----BEGIN PUBLIC KEY-----","")
                .replace(System.lineSeparator(),"")
                .replace("-----END PUBLIC KEY-----","");
        System.out.println("public key: " + publicKeyPem);
        return Base64.decodeBase64(publicKeyPem);
    }

    // HSS
    public static PublicKey getHssPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = hssPublicKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decode);
        return keyFactory.generatePublic(keySpec);
    }

    public static PrivateKey getHssPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = hssPrivateKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decode);
        return keyFactory.generatePrivate(keySpec);
    }

    private static byte[] hssPrivateKeyDecode() throws IOException {
        Path keyPath = Paths.get("/root/keys/hss/private.pem");
        String key = new String(Files.readAllBytes(keyPath), Charset.defaultCharset());
        String privateKeyPem = key
                .replace("-----BEGIN PRIVATE KEY-----","")
                .replaceAll(System.lineSeparator(),"")
                .replace("-----END PRIVATE KEY-----","");

        System.out.println("Private Key: "+ privateKeyPem);

        return Base64.decodeBase64(privateKeyPem);
    }

    private static byte[] hssPublicKeyDecode() throws IOException {
        Path path = Paths.get("/root/keys/hss/public.pem");
        String key = new String(Files.readAllBytes(path), Charset.defaultCharset());
        String publicKeyPem = key
                .replace("-----BEGIN PUBLIC KEY-----","")
                .replace(System.lineSeparator(),"")
                .replace("-----END PUBLIC KEY-----","");

        System.out.println("public key: " + publicKeyPem);

        return Base64.decodeBase64(publicKeyPem);
    }


    // Paynet
    public static PublicKey getPaynetPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = paynetPublicKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decode);
        return keyFactory.generatePublic(keySpec);
    }

    public static PrivateKey getPaynetPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = paynetPrivateKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decode);
        return keyFactory.generatePrivate(keySpec);
    }

    private static byte[] paynetPrivateKeyDecode() throws IOException {
        Path keyPath = Paths.get("/root/keys/paynet/private.pem");
        String key = new String(Files.readAllBytes(keyPath), Charset.defaultCharset());
        String privateKeyPem = key
                .replace("-----BEGIN PRIVATE KEY-----","")
                .replaceAll(System.lineSeparator(),"")
                .replace("-----END PRIVATE KEY-----","");

        System.out.println("Private Key: "+ privateKeyPem);

        return Base64.decodeBase64(privateKeyPem);
    }

    private static byte[] paynetPublicKeyDecode() throws IOException {
        Path path = Paths.get("/root/keys/paynet/public.pem");
        String key = new String(Files.readAllBytes(path), Charset.defaultCharset());
        String publicKeyPem = key
                .replace("-----BEGIN PUBLIC KEY-----","")
                .replace(System.lineSeparator(),"")
                .replace("-----END PUBLIC KEY-----","");

        System.out.println("public key: " + publicKeyPem);

        return Base64.decodeBase64(publicKeyPem);
    }

    // EasyPay
    public static PublicKey getEasyPayPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = easyPayPublicKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decode);
        return keyFactory.generatePublic(keySpec);
    }
    public static PrivateKey getEasyPayPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = easyPayPrivateKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decode);
        return keyFactory.generatePrivate(keySpec);
    }
    private static byte[] easyPayPrivateKeyDecode() throws IOException {
        Path keyPath = Paths.get("/root/keys/easypay/private.pem");
        String key = new String(Files.readAllBytes(keyPath), Charset.defaultCharset());
        String privateKeyPem = key
                .replace("-----BEGIN PRIVATE KEY-----","")
                .replaceAll(System.lineSeparator(),"")
                .replace("-----END PRIVATE KEY-----","");

        System.out.println("Private Key: "+ privateKeyPem);

        return Base64.decodeBase64(privateKeyPem);
    }
    private static byte[] easyPayPublicKeyDecode() throws IOException {
        Path path = Paths.get("/root/keys/easypay/public.pem");
        String key = new String(Files.readAllBytes(path), Charset.defaultCharset());
        String publicKeyPem = key
                .replace("-----BEGIN PUBLIC KEY-----","")
                .replace(System.lineSeparator(),"")
                .replace("-----END PUBLIC KEY-----","");

        System.out.println("public key: " + publicKeyPem);

        return Base64.decodeBase64(publicKeyPem);
    }

    // ZenZero
    public static PublicKey getZenZeroPublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = zenZeroPublicKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(decode);
        return keyFactory.generatePublic(keySpec);
    }
    public static PrivateKey getZenZeroPrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decode = zenZeroPrivateKeyDecode();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(decode);
        return keyFactory.generatePrivate(keySpec);
    }
    private static byte[] zenZeroPrivateKeyDecode() throws IOException {
        Path keyPath = Paths.get("/root/keys/zenzero/private.pem");
        String key = new String(Files.readAllBytes(keyPath), Charset.defaultCharset());
        String privateKeyPem = key
                .replace("-----BEGIN PRIVATE KEY-----","")
                .replaceAll(System.lineSeparator(),"")
                .replace("-----END PRIVATE KEY-----","");

        System.out.println("Private Key: "+ privateKeyPem);

        return Base64.decodeBase64(privateKeyPem);
    }
    private static byte[] zenZeroPublicKeyDecode() throws IOException {
        Path path = Paths.get("/root/keys/zenzero/public.pem");
        String key = new String(Files.readAllBytes(path), Charset.defaultCharset());
        String publicKeyPem = key
                .replace("-----BEGIN PUBLIC KEY-----","")
                .replace(System.lineSeparator(),"")
                .replace("-----END PUBLIC KEY-----","");

        System.out.println("public key: " + publicKeyPem);

        return Base64.decodeBase64(publicKeyPem);
    }
}
