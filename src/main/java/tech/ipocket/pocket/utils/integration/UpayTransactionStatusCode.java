package tech.ipocket.pocket.utils.integration;

public enum UpayTransactionStatusCode {
    TRANSACTION_SUCCESS("201"),
    Bad_REQUEST("400"),
    AUTH_FAILED("401");

    private final String transactionStatusCode;

    UpayTransactionStatusCode(String transactionStatusCode) {
        this.transactionStatusCode = transactionStatusCode;
    }

    public String getTransactionStatusCode() {
        return transactionStatusCode;
    }
}
