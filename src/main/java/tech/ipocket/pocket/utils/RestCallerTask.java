package tech.ipocket.pocket.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;

import java.io.IOException;

public class RestCallerTask {
    static LogWriterUtility logWriterUtility = new LogWriterUtility(RestCallerTask.class);

    public Object callToRestService(HttpMethod httpMethod, String url,
                                    Object requestObject, Class<?> responseObject, String requestId) throws PocketException {

        logWriterUtility.debug(requestId, "Calling Url : \n" + url);
        logWriterUtility.debug(requestId, "callToRestService Request : " + new GsonBuilder().setPrettyPrinting().create().toJson(requestObject));

        try {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());


            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Object> entity = new HttpEntity<>(requestObject, requestHeaders);

            long start = System.currentTimeMillis();

            ResponseEntity<?> responseEntity = restTemplate.exchange(url, httpMethod, entity, new BaseResponseObject().getClass());

            printLog("callToRestService Status : " + responseEntity.getStatusCodeValue());
            logWriterUtility.debug(requestId, "callToRestService Status : " + responseEntity.getStatusCodeValue());


            printLog("callToRestService Body : " + new GsonBuilder().setPrettyPrinting().create().toJson(responseEntity.getBody()));
            ObjectMapper mapper = new ObjectMapper();
            String JSON = mapper.writeValueAsString(responseEntity.getBody()); // to get json string
            logWriterUtility.debug(requestId, "callToRestService Body : " + JSON);

            long elapsedTime = System.currentTimeMillis() - start;
            printLog("callToRestService Execution time: " + elapsedTime + " Milliseconds)");
            logWriterUtility.debug(requestId, "TS Call Execution time: " + elapsedTime + " Milliseconds)");

            if (responseEntity.getStatusCodeValue() == 200 && responseEntity.getBody() != null) {
                BaseResponseObject baseResponseObject= (BaseResponseObject) responseEntity.getBody();

                Gson gson=new Gson() ;
                String json = gson.toJson(baseResponseObject.getData());

                logWriterUtility.trace(requestId,json);


                try {
                    return gson.fromJson(json,responseObject);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        } catch (HttpStatusCodeException exception) {
            printLog("callToRestService Error :" + exception.getResponseBodyAsString());
            //Handle exception here

            ErrorObject errorObject=parseErrorMessage(requestId,exception.getResponseBodyAsString(),exception.getStatusCode());

            if(errorObject!=null){
                throw new PocketException(errorObject.getErrorCode(),errorObject.getErrorMessage());
            }
            throw new PocketException(PocketErrorCode.InternalServerCommunicationError);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logWriterUtility.error(requestId, "callToRestService Error :" + e.getMessage());
            throw new PocketException(PocketErrorCode.InternalServerCommunicationError, e.getMessage());
        }
        return null;
    }

    public Object callToRestService(HttpMethod httpMethod, String url,HttpHeaders requestHeaders,
                                    Object requestObject, Class<?> responseObject, String requestId) throws PocketException {

        logWriterUtility.debug(requestId, "Calling Url : \n" + url);
        logWriterUtility.debug(requestId, "callToRestService Request : " + new GsonBuilder().setPrettyPrinting().create().toJson(requestObject));

        try {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            HttpEntity<Object> entity = new HttpEntity<>(requestObject, requestHeaders);

            long start = System.currentTimeMillis();

            ResponseEntity<?> responseEntity = restTemplate.exchange(url, httpMethod, entity, new BaseResponseObject().getClass());

            printLog("callToRestService Status : " + responseEntity.getStatusCodeValue());
            logWriterUtility.debug(requestId, "callToRestService Status : " + responseEntity.getStatusCodeValue());


            printLog("callToRestService Body : " + new GsonBuilder().setPrettyPrinting().create().toJson(responseEntity.getBody()));
            ObjectMapper mapper = new ObjectMapper();
            String JSON = mapper.writeValueAsString(responseEntity.getBody()); // to get json string
            logWriterUtility.debug(requestId, "callToRestService Body : " + JSON);

            long elapsedTime = System.currentTimeMillis() - start;
            printLog("callToRestService Execution time: " + elapsedTime + " Milliseconds)");
            logWriterUtility.debug(requestId, "TS Call Execution time: " + elapsedTime + " Milliseconds)");

            if (responseEntity.getStatusCodeValue() == 200 && responseEntity.getBody() != null) {
                BaseResponseObject baseResponseObject= (BaseResponseObject) responseEntity.getBody();

                Gson gson=new Gson() ;
                String json = gson.toJson(baseResponseObject.getData());

                logWriterUtility.trace(requestId,json);


                try {
                    return gson.fromJson(json,responseObject);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        } catch (HttpStatusCodeException exception) {
            printLog("callToRestService Error :" + exception.getResponseBodyAsString());
            //Handle exception here

            ErrorObject errorObject=parseErrorMessage(requestId,exception.getResponseBodyAsString(),exception.getStatusCode());

            if(errorObject!=null){
                throw new PocketException(errorObject.getErrorCode(),errorObject.getErrorMessage());
            }
            throw new PocketException(PocketErrorCode.InternalServerCommunicationError);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            logWriterUtility.error(requestId, "callToRestService Error :" + e.getMessage());
            throw new PocketException(PocketErrorCode.InternalServerCommunicationError, e.getMessage());
        }
        return null;
    }

    private ErrorObject parseErrorMessage(String requestId, String responseBodyAsString, HttpStatus statusCode) {


        logWriterUtility.trace(requestId, "Error Message :\n" + responseBodyAsString);
        ErrorObject errorObject = new ErrorObject();
        try {

            if (statusCode != null && statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
                logWriterUtility.error(requestId, "Get Internal server error from external server.");
                errorObject.setErrorCode(PocketErrorCode.InternalServerCommunicationError.getKeyString());
                errorObject.setErrorMessage("Internal server error");// TODO: 8/2/18 Have to get message from resourse folder
                return errorObject;
            } else if (statusCode != null && statusCode == HttpStatus.NOT_FOUND) {
                logWriterUtility.error(requestId, "Url not found.");
                errorObject.setErrorCode(PocketErrorCode.InternalServerCommunicationError.getKeyString());
                errorObject.setErrorCode("Could not process your request");// TODO: 8/2/18 Have to get message from resourse folder
                return errorObject;
            }

            new JSONObject(responseBodyAsString);

            ObjectMapper mapper = new ObjectMapper();
            BaseResponseObject responseObject = mapper.readValue(responseBodyAsString, BaseResponseObject.class);

            if (responseObject != null) {
                errorObject = responseObject.getError();
            }

        } catch (JSONException | IOException ex) {
            errorObject.setErrorCode(PocketConstants.ERROR);
            errorObject.setErrorMessage(responseBodyAsString);
        }

        return errorObject;
    }

    public Object callToRestService(HttpMethod httpMethod, String url, HttpHeaders requestHeaders, Object requestObject, Class<?> responseObject) throws PocketException {

        printLog("Url : " + url);
        printLog("callToRestService Request : " + new GsonBuilder().setPrettyPrinting().create().toJson(requestObject));

        try {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());


            HttpEntity<Object> entity = new HttpEntity<>(requestObject, requestHeaders);

            long start = System.currentTimeMillis();

            ResponseEntity<?> responseEntity = restTemplate.exchange(url, httpMethod, entity, responseObject);

            printLog("callToRestService Status : " + responseEntity.getStatusCodeValue());


            printLog("callToRestService Body : " + new GsonBuilder().setPrettyPrinting().create().toJson(responseEntity.getBody()));

            long elapsedTime = System.currentTimeMillis() - start;
            printLog("callToRestService Execution time: " + elapsedTime + " Milliseconds)");

            if (responseEntity.getStatusCodeValue() == 200 && responseEntity.getBody() != null) {
                return responseEntity.getBody();
            }

        } catch (HttpClientErrorException exception) {
            printLog("callToRestService Error :" + exception.getResponseBodyAsString());
            //Handle exception here

            return exception.getResponseBodyAsString();
        } catch (HttpStatusCodeException exception) {
            printLog("callToRestService Error :" + exception.getResponseBodyAsString());
            //Handle exception here
            return exception.getResponseBodyAsString();

        }
        throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
    }

    private void printLog(String message) {
        System.out.println(message);
    }

}
