package tech.ipocket.pocket.services.um.otp;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.um.SendOtpRequest;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.SendOtpResponse;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.UmEnums;

import java.util.concurrent.ExecutionException;

public class OtpForFundTransfer extends AbstractOtp {

    public OtpForFundTransfer(CommonOtpService commonOtpService,OtpService otpService) {
        super(commonOtpService,otpService);
    }

    @Override
    public BaseResponseObject sendOtp(SendOtpRequest sendOtpRequest, Integer userCredentialId) throws PocketException, ExecutionException, InterruptedException {


        validateData(sendOtpRequest);

        BaseResponseObject baseResponseObject = new BaseResponseObject();

        UmUserInfo senderInfo = checkIsValidSender(userCredentialId, sendOtpRequest.getRequestId());
        checkIsValidReceiver(sendOtpRequest.getReceiverMobileNumber(), sendOtpRequest.getRequestId());

        BalanceCheckRequest balanceInqueryRequest = new BalanceCheckRequest();
        balanceInqueryRequest.setMobilenumber(sendOtpRequest.getMobileNumber());
        balanceInqueryRequest.setHardwareSignature(sendOtpRequest.getHardwareSignature());
        balanceInqueryRequest.setSessionToken(sendOtpRequest.getSessionToken());
        balanceInqueryRequest.setRequestId(sendOtpRequest.getRequestId());
        checkIsSenderBalanceSufficient(balanceInqueryRequest);


        TransactionValidityUmRequest transactionValidityUmRequest = new TransactionValidityUmRequest();
        transactionValidityUmRequest.setReceiverMobileNumber(sendOtpRequest.getReceiverMobileNumber());
        transactionValidityUmRequest.setSenderMobileNumber(sendOtpRequest.getMobileNumber());
        transactionValidityUmRequest.setRequestId(sendOtpRequest.getRequestId());
        transactionValidityUmRequest.setTransferAmount("" + sendOtpRequest.getAmount());
        transactionValidityUmRequest.setType(UmEnums.OtpTypes.FundTransafer.getOtpType());
        callTxForTransactionVisibility(transactionValidityUmRequest);

        OtpObject umOtp= otpService.generateOtp(userCredentialId,senderInfo, UmEnums.OtpTypes.FundTransafer.getOtpType(),sendOtpRequest);

        sendOtpNotification(sendOtpRequest.getMobileNumber(),null,senderInfo,
                umOtp.getOtpText(),sendOtpRequest.getRequestId());

        SendOtpResponse sendOtpResponse = new SendOtpResponse();
        sendOtpResponse.setReferenceId(umOtp.getOtpSecret());
        sendOtpResponse.setMobileNumber(sendOtpRequest.getMobileNumber());
        baseResponseObject.setData(sendOtpResponse);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(sendOtpRequest.getRequestId());

        return baseResponseObject;
    }

    private boolean validateData(SendOtpRequest sendOtpRequest) throws PocketException {

        if(sendOtpRequest.getMobileNumber()==null){
            logWriterUtility.error(sendOtpRequest.getRequestId(),"Sender mobile required");
            throw new PocketException(PocketErrorCode.SenderWalletIsRequired);
        }

        if(sendOtpRequest.getReceiverMobileNumber()==null){
            logWriterUtility.error(sendOtpRequest.getRequestId(),"Receiver mobile required");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        if(sendOtpRequest.getAmount()<=0){
            logWriterUtility.error(sendOtpRequest.getRequestId(),"Transaction amount required");
            throw new PocketException(PocketErrorCode.AmountRequired);
        }

        return true;
    }
}