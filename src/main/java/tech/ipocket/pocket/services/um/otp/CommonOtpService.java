package tech.ipocket.pocket.services.um.otp;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.utils.PocketException;

public interface CommonOtpService {
    UmUserInfo checkIsValidSender(Integer credentialId, String requestId) throws PocketException;
    void checkIsValidReceiver(String receiverMobileNumber, String requestId) throws PocketException;
    void checkIsSenderBalanceSufficient(BalanceCheckRequest balanceInqueryRequest) throws PocketException;
    boolean callTxForTransactionVisibility(TransactionValidityUmRequest visibilityCheckRequest) throws PocketException;
    void sendOtpNotification(String mobileNumber, String email, UmUserInfo umUserInfo, String otp, String requestId);
}
