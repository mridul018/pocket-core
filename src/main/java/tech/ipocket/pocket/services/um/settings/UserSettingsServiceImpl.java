package tech.ipocket.pocket.services.um.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmUserSettings;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.repository.um.UserSettingsRepository;
import tech.ipocket.pocket.response.um.UserSettingsResponse;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@Service
public class UserSettingsServiceImpl implements UserSettingsService {

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserSettingsResponse getUserSettings(String loginId) throws PocketException {

        UserSettingsResponse response = new UserSettingsResponse();

        UmUserSettings settings = userSettingsRepository.findFirstByUmUserInfoByUserId(
                userInfoRepository.findByLoginId(loginId)
        );

        if (settings == null) {
            throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND);
        }

        response.setMobileNumberVerified(settings.getMobileNumberVerified());
        response.setPrimaryIdVerified(settings.getPrimaryIdVerified());

        return response;
    }
}
