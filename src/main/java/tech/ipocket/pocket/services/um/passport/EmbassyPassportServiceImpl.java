package tech.ipocket.pocket.services.um.passport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmEmbPassportType;
import tech.ipocket.pocket.repository.um.EmbassyPassportRepository;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeEmptyRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.um.passport.UmEmbPassportTypeResponse;
import tech.ipocket.pocket.utils.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmbassyPassportServiceImpl implements EmbassyPassportService{
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private EmbassyPassportRepository embassyPassportRepository;

    @Override
    public BaseResponseObject createPassportType(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportType umEmbPassportType = new UmEmbPassportType();
        UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();

        checkDuplicatePassportType(request, request.getPassportType());
        try {
            if(request != null){
                umEmbPassportType.setPassportType(request.getPassportType());
                umEmbPassportType.setDescription(request.getDescription());
                umEmbPassportType.setStatus(request.getStatus());
                umEmbPassportType.setCreatedDate(new Date());

                embassyPassportRepository.save(umEmbPassportType);

                umEmbPassportType = embassyPassportRepository.findTopByOrderByIdDesc();
                if(umEmbPassportType != null){
                    umEmbPassportTypeResponse.setId(umEmbPassportType.getId());
                    umEmbPassportTypeResponse.setPassportType(umEmbPassportType.getPassportType());
                    umEmbPassportTypeResponse.setDescription(umEmbPassportType.getDescription());
                    umEmbPassportTypeResponse.setStatus(umEmbPassportType.getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(DateUtil.currentDateTime(umEmbPassportType.getCreatedDate()));
                }
                if(umEmbPassportType != null){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportTypeResponse);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportTypeSavedFailed.getKeyString(),
                            "Passport Type is failed to save. Reason : "+baseResponseObject.getError().getErrorMessage()));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportTypeSavedFailed.getKeyString(),
                    "Passport Type is failed to save. Reason : "+baseResponseObject.getError().getErrorMessage()));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updatePassportType(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportType umEmbPassportType = new UmEmbPassportType();
        UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
        try {
            if(request != null){
                umEmbPassportType = embassyPassportRepository.findById(request.getId()).get();
                if(umEmbPassportType != null){
                    umEmbPassportType.setPassportType(request.getPassportType());
                    umEmbPassportType.setDescription(request.getDescription());
                    umEmbPassportType.setStatus(request.getStatus());
                    umEmbPassportType.setCreatedDate(new Date());

                    embassyPassportRepository.save(umEmbPassportType);

                    umEmbPassportType = embassyPassportRepository.findById(umEmbPassportType.getId()).get();
                    if(umEmbPassportType != null){
                        umEmbPassportTypeResponse.setId(umEmbPassportType.getId());
                        umEmbPassportTypeResponse.setPassportType(umEmbPassportType.getPassportType());
                        umEmbPassportTypeResponse.setDescription(umEmbPassportType.getDescription());
                        umEmbPassportTypeResponse.setStatus(umEmbPassportType.getStatus());
                        umEmbPassportTypeResponse.setCreatedDate(DateUtil.currentDateTime(umEmbPassportType.getCreatedDate()));
                    }

                }

                if(umEmbPassportTypeResponse != null){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportTypeResponse);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportTypeUpdateFailed.getKeyString(),
                            "Passport Type is failed to update. Reason : "+baseResponseObject.getError().getErrorMessage()));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportTypeUpdateFailed.getKeyString(),
                    "Passport Type is failed to update. Reason : "+baseResponseObject.getError().getErrorMessage()));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject createEmbassyPassportList(UmEmbPassportTypeEmptyRequest request, TsSessionObject tsSessionObject) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<UmEmbPassportType> passportTypeList = new ArrayList<>();
        List<UmEmbPassportTypeResponse> umEmbPassportTypeResponseList = new ArrayList<>();
        List<UmEmbPassportTypeResponse> umEmbPassportTypeResponseListTemp = new ArrayList<>();

        try {
            if(request != null){

                passportTypeList = (List<UmEmbPassportType>) embassyPassportRepository.findAll();
                for(UmEmbPassportType passportType : passportTypeList){
                    UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(passportType.getId());
                    umEmbPassportTypeResponse.setPassportType(passportType.getPassportType());
                    umEmbPassportTypeResponse.setDescription(passportType.getDescription());
                    umEmbPassportTypeResponse.setStatus(passportType.getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(passportType.getCreatedDate().toString());
                    umEmbPassportTypeResponseList.add(umEmbPassportTypeResponse);
                }

                if(!umEmbPassportTypeResponseList.isEmpty() && umEmbPassportTypeResponseList.size()>0){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportTypeResponseList);
                    return baseResponseObject;
                }else{
                    baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportTypeListEmpty.getKeyString(),
                            "Passport type list is empty. Reason : "));
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.ERROR);
                    baseResponseObject.setData(null);
                    return baseResponseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportTypeListEmpty.getKeyString(),
                    "Passport sub_type list is empty. Reason : "+baseResponseObject.getError().getErrorMessage()));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;

    }

    @Override
    public BaseResponseObject passportTypeById(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportType passportType = new UmEmbPassportType();
        try {
            if(request != null){
                passportType = embassyPassportRepository.findById(request.getId()).get();

                if(passportType != null){
                    UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(passportType.getId());
                    umEmbPassportTypeResponse.setPassportType(passportType.getPassportType());
                    umEmbPassportTypeResponse.setDescription(passportType.getDescription());
                    umEmbPassportTypeResponse.setStatus(passportType.getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(passportType.getCreatedDate().toString());

                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportTypeResponse);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassPortType.getKeyString(),
                            "Passport type is not found. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassPortType.getKeyString(),
                    "Passport type is not found. Reason : "));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;

    }

    @Override
    public BaseResponseObject searchPassportTypeAndStatus(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportType passportType = new UmEmbPassportType();
        try {
            if(request != null){
                passportType = embassyPassportRepository.findTopByPassportTypeIgnoreCaseAndStatusOrderByIdDesc(request.getPassportType(), request.getStatus());

                if(passportType != null){
                    UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(passportType.getId());
                    umEmbPassportTypeResponse.setPassportType(passportType.getPassportType());
                    umEmbPassportTypeResponse.setDescription(passportType.getDescription());
                    umEmbPassportTypeResponse.setStatus(passportType.getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(passportType.getCreatedDate().toString());

                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportTypeResponse);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassPortType.getKeyString(),
                            "Passport type is not found. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassPortType.getKeyString(),
                    "Passport type is not found. Reason : "));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;

    }

    public void checkDuplicatePassportType(UmEmbPassportTypeRequest request, String passportType) throws PocketException {
        if(request.getStatus() == null || request.getStatus().equals("string") || request.getStatus().isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Passport status is required");
            throw new PocketException(PocketErrorCode.PassPortStatus);
        }

        UmEmbPassportType embPassportType =  embassyPassportRepository.
                findTopByPassportTypeIgnoreCaseAndStatusOrderByIdDesc(passportType, PocketConstants.PASSPORT_STATUS);
        if(embPassportType != null){
            logWriterUtility.error(request.getRequestId(), "Passport type is already existed.");
            throw new PocketException(PocketErrorCode.PassportTypeAlreadyExist);
        }
    }

}
