package tech.ipocket.pocket.services.um.address;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.um.GetStateByCountryCodeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;

public interface AddressService {
    BaseResponseObject getAllCountry(BaseRequestObject baseRequestObject);
    BaseResponseObject getAllState(BaseRequestObject baseRequestObject);
    BaseResponseObject getAllStateByCountryCode(GetStateByCountryCodeRequest request);
}
