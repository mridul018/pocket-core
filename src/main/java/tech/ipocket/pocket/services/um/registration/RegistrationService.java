package tech.ipocket.pocket.services.um.registration;

import tech.ipocket.pocket.request.um.OtpVerificationRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface RegistrationService {
    RegistrationResponse customerRegistration(CustomerRegistration customerRegistration) throws PocketException;

    RegistrationResponse merchantAgentRegistration(MerchantAgentRegistration merchantAgentRegistration) throws PocketException;
    RegistrationResponse srRegistration(SrRegistration srRegistration) throws PocketException;

    RegistrationResponse adminRegistration(AdminRegistration adminRegistration) throws PocketException;

    BaseResponseObject doVerifyUser(OtpVerificationRequest otpVerificationRequest) throws PocketException;

    RegistrationResponse commonRegistration(CommonRegistration commonRegistration)  throws PocketException;
}
