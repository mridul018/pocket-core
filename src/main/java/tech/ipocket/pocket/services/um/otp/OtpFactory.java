package tech.ipocket.pocket.services.um.otp;

import org.springframework.core.env.Environment;
import tech.ipocket.pocket.utils.OtpPurposes;

public class OtpFactory {

    private CommonOtpService commonOtpService;
    private Environment environment;
    private OtpService otpService;

    public OtpFactory(CommonOtpService commonOtpService, Environment environment,OtpService otpService) {
        this.commonOtpService = commonOtpService;
        this.environment = environment;
        this.otpService = otpService;
    }

    public AbstractOtp prepareOtpService(int purpose) {
        switch (purpose) {
            case OtpPurposes.PURPOSE_FUND_TRANSFER:
                return new OtpForFundTransfer(commonOtpService,otpService);

            default:
                return new OtpForVerification(commonOtpService,otpService);
        }

    }
}