package tech.ipocket.pocket.services.um.transactionSummery;

public class ReceiverInfo {
    private String fullName;
    private String profileImageUrl;

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }
}
