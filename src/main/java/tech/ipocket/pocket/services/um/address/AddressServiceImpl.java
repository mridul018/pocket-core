package tech.ipocket.pocket.services.um.address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmCountry;
import tech.ipocket.pocket.entity.UmCountryStateMapping;
import tech.ipocket.pocket.entity.UmState;
import tech.ipocket.pocket.repository.um.UmCountryRepository;
import tech.ipocket.pocket.repository.um.UmCountryStateMappingRepository;
import tech.ipocket.pocket.repository.um.UmStateRepository;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.um.GetStateByCountryCodeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.StateListItem;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private UmCountryRepository countryRepository;

    @Autowired
    private UmStateRepository umStateRepository;

    @Autowired
    private UmCountryStateMappingRepository umCountryStateMappingRepository;

    @Override
    public BaseResponseObject getAllCountry(BaseRequestObject baseRequestObject) {

        BaseResponseObject baseResponseObject=new BaseResponseObject(baseRequestObject.getRequestId());

        List<UmCountry> countryList= countryRepository.findAllByStatus("1");
        baseResponseObject.setError(null);
        baseResponseObject.setData(countryList);
        baseResponseObject.setStatus(PocketConstants.OK);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getAllState(BaseRequestObject baseRequestObject) {

        BaseResponseObject baseResponseObject=new BaseResponseObject(baseRequestObject.getRequestId());

        List<UmState> countryList= umStateRepository.findAllByStatus("1");
        baseResponseObject.setError(null);
        baseResponseObject.setData(countryList);
        baseResponseObject.setStatus(PocketConstants.OK);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getAllStateByCountryCode(GetStateByCountryCodeRequest request) {

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());


        List<UmCountryStateMapping> mappingList=umCountryStateMappingRepository.findAllByCountryCodeAndStatus(request.getCountryCode(),"1");

        List<StateListItem> stateListItems=null;

        if(mappingList==null||mappingList.size()==0){
            baseResponseObject.setData(stateListItems);
        }else{

            stateListItems=new ArrayList<>();

            for (UmCountryStateMapping map:mappingList) {

                UmState state=umStateRepository.findFirstById(Integer.parseInt(map.getStateId()));

                StateListItem stateListItem=new StateListItem();

                stateListItem.setCreatedDate(map.getCreatedDate());


                if(state!=null){
                    stateListItem.setStateId(""+state.getId());
                    stateListItem.setStateName(state.getStateName());
                }

                UmCountry umCountry=countryRepository.findAllByCountryCodeAndStatus(map.getCountryCode(),"1");

                if(umCountry!=null){
                    stateListItem.setCountryName(umCountry.getCountryName());
                }
                stateListItems.add(stateListItem);
            }
        }


        baseResponseObject.setError(null);
        baseResponseObject.setData(stateListItems);
        baseResponseObject.setStatus(PocketConstants.OK);

        return baseResponseObject;
    }
}
