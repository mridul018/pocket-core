package tech.ipocket.pocket.services.um.otp;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.repository.um.UserOTPRepository;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.um.TransactionValidityUmResponse;
import tech.ipocket.pocket.services.external.TsServices;
import tech.ipocket.pocket.utils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class CommonOtpServiceImpl implements CommonOtpService {

    static LogWriterUtility logWriterUtility = new LogWriterUtility(CommonOtpServiceImpl.class);


    private UserInfoRepository userInfoRepository;
    private UserOTPRepository genOtpDao;
    private Environment environment;

    private TsServices tsServices;

    public CommonOtpServiceImpl(UserInfoRepository userInfoRepository, UserOTPRepository genOtpDao,
                                Environment environment,TsServices tsServices) {
        this.userInfoRepository = userInfoRepository;
        this.genOtpDao = genOtpDao;
        this.environment = environment;
        this.tsServices = tsServices;
    }

    @Override
    public UmUserInfo checkIsValidSender(Integer credentialId, String requestId) throws PocketException {
        UmUserInfo userCredential = userInfoRepository.findFirstById(credentialId);
        if (userCredential == null || UmEnums.UserStatus.Deleted.getUserStatusType()==(userCredential.getAccountStatus())) {
            logWriterUtility.error(requestId, "Sender Active Wallet not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        if (userCredential.getAccountStatus()!=(UmEnums.UserStatus.Verified.getUserStatusType())) {
            logWriterUtility.error(requestId, "Invalid User Account Status :" + userCredential.getAccountStatus());
            throw new PocketException(PocketErrorCode.InvalidSenderAccountStatus);
        }
        return userCredential;
    }

    @Override
    public void checkIsValidReceiver(String receiverMobileNumber, String requestId) throws PocketException {
        if (receiverMobileNumber == null) {
            logWriterUtility.debug(requestId, "Receiver Mobile Number not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        List<Integer> statusNotIn = new ArrayList<>();
        statusNotIn.add(UmEnums.UserStatus.Deleted.getUserStatusType());
        statusNotIn.add(UmEnums.UserStatus.Initiation.getUserStatusType());

        UmUserInfo receiverInfo = userInfoRepository.findByMobileNumberAndAccountStatusNotIn(receiverMobileNumber, statusNotIn);

        if (receiverInfo == null) {
            logWriterUtility.error(requestId, "Returning sendOtp() method for Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(requestId, "Receiver Wallet found.");

        boolean isReceiverEligibleForTransfer = receiverInfo.getAccountStatus()==(UmEnums.UserStatus.Verified.getUserStatusType());
        if (!isReceiverEligibleForTransfer) {
            logWriterUtility.error(requestId, "Invalid Receiver Account Status :" + receiverInfo.getAccountStatus());
            throw new PocketException(PocketErrorCode.InvalidReceiverAccountStatus);
        }

        /*if (!UserUtils.isProvidedTypeUserHaveWallet(receiverInfo.getUserType())) {
            logWriterUtility.trace(requestId, "Receiver is not eligible for fund transfer");
            throw new PocketException(PocketErrorCode.ReceiverNotEligible);
        }
*/

    }

    @Override
    public void checkIsSenderBalanceSufficient(BalanceCheckRequest balanceInqueryRequest) throws PocketException {
        boolean isAccountBalanceSufficient;
        logWriterUtility.trace(balanceInqueryRequest.getRequestId(), "Calling Transaction Service for check account balance of sender.");
        isAccountBalanceSufficient = tsServices.callTxForValidatingWalletBalance(balanceInqueryRequest);
        if (!isAccountBalanceSufficient) {
            logWriterUtility.trace(balanceInqueryRequest.getRequestId(), "Insufficient Account Balance.");
            logWriterUtility.trace(balanceInqueryRequest.getRequestId(), "Sender doesn't have sufficient wallet balance. ");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance, "Sender doesn't have sufficient wallet balance. ");

        }
        logWriterUtility.trace(balanceInqueryRequest.getRequestId(), "Sender has sufficient wallet balance. ");
    }

    @Override
    public boolean callTxForTransactionVisibility(TransactionValidityUmRequest visibilityCheckRequest) throws PocketException {

        TransactionValidityUmResponse checkResponse=tsServices.callTxForTransactionVisibility(visibilityCheckRequest);
        if (checkResponse != null && checkResponse.getStatus().equals("200")) {
            return true;
        } else if (checkResponse.getError() != null && checkResponse.getError().getErrorMessage() != null) {
            throw new PocketException(checkResponse.getError().getErrorCode(), checkResponse.getError().getErrorMessage());
        }
        throw new PocketException(PocketErrorCode.InternalServerCommunicationError, "An error occurred while communicating with Tx service. ");
    }

    @Override
    public void sendOtpNotification(String mobileNumber, String email, UmUserInfo umUserInfo, String otp, String requestId) {
        CompletableFuture.runAsync(() -> {


        });

    }

}