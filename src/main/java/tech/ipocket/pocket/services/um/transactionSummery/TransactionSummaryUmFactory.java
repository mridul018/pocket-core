package tech.ipocket.pocket.services.um.transactionSummery;

import org.springframework.core.env.Environment;
import tech.ipocket.pocket.services.ts.transactionValidity.TopUpTransactionValidity;
import tech.ipocket.pocket.utils.ServiceCodeConstants;
import tech.ipocket.pocket.utils.TransactionTypes;

public class TransactionSummaryUmFactory {
    private TransactionSummaryService transactionSummaryService;
    private Environment environment;

    public TransactionSummaryUmFactory(TransactionSummaryService transactionSummaryService, Environment environment) {
        this.transactionSummaryService = transactionSummaryService;
        this.environment = environment;
    }

    public AbstractTransactionSummary prepareTransactionSummary(String purpose) {
        switch (purpose) {
            case ServiceCodeConstants.Fund_Transfer:
                return new TransactionSummaryForFundTransfer(transactionSummaryService);
            case ServiceCodeConstants.Merchant_Payment:
                return new TransactionSummaryForMerchantPayment(transactionSummaryService);
            case ServiceCodeConstants.BillPay:
                return new TransactionSummaryForBillPay(transactionSummaryService);
            case ServiceCodeConstants.Top_Up:
                return new TransactionSummaryForTopUp(transactionSummaryService);
            case ServiceCodeConstants.CashInFromBank:
                return new TransactionSummaryForCashInFromBank(transactionSummaryService);
            case ServiceCodeConstants.CashOutToBank:
                return new TransactionSummaryForCashOutToBank(transactionSummaryService);
            case ServiceCodeConstants.CashOut:
            case ServiceCodeConstants.CashIn:
                return new TransactionSummaryForCashOut(transactionSummaryService);
            default:
                return null;
        }

    }
}
