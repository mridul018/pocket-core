package tech.ipocket.pocket.services.um.passport;

import tech.ipocket.pocket.request.um.passport.EmbassyPassportTransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface EmbassyPassportTransactionService {

    public BaseResponseObject doPassportFeePayment(EmbassyPassportTransactionRequest request, TsSessionObject tsSessionObject) throws PocketException;

}
