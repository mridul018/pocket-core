package tech.ipocket.pocket.services.um.passport;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmEmbPassportSubType;
import tech.ipocket.pocket.entity.UmEmbPassportType;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.EmbassyPassportRepository;
import tech.ipocket.pocket.repository.um.EmbassyPassportSubTypeRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportSubTypeEmptyRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportSubTypeRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.um.passport.UmEmbPassportSubTypeResponse;
import tech.ipocket.pocket.response.um.passport.UmEmbPassportTypeResponse;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmbassyPassportSubTypeServiceImpl implements EmbassyPassportSubTypeService{

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private EmbassyPassportSubTypeRepository embassyPassportSubTypeRepository;

    @Autowired
    private EmbassyPassportRepository embassyPassportRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private Environment environment;

    @Override
    public BaseResponseObject createPassportSubType(UmEmbPassportSubTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();
        UmEmbPassportType umEmbPassportType;
        UmEmbPassportSubTypeResponse umEmbPassportSubTypeResponse = null;
        UmEmbPassportTypeResponse umEmbPassportTypeResponse = null;


        checkDuplicatePassportSubType(request, request.getPassportSubType());
        try {
            if(request != null){
                umEmbPassportSubType.setPassportSubType(request.getPassportSubType());
                umEmbPassportSubType.setStatus("1");
                umEmbPassportSubType.setDescription(request.getDescription());
                umEmbPassportSubType.setGeneralDelivery(request.getGeneralDelivery());
                umEmbPassportSubType.setEmergencyDelivery(request.getEmergencyDelivery());
                umEmbPassportSubType.setEmbPassportFee(request.getEmbPassportFee());
                umEmbPassportSubType.setPocketServiceCharge(serviceChargeForPassport(request.getEmbPassportFee(), request.getPocketServiceCharge()));
                umEmbPassportSubType.setTotalAmount(totalPassportAmountFee(request.getEmbPassportFee(), request.getPocketServiceCharge()));
                umEmbPassportSubType.setCreatedDate(DateUtil.currentDateTime());

                umEmbPassportType = embassyPassportRepository.findTopByIdOrderByIdDesc(request.getUmEmbPassportTypeById().getId()).get();
                if (umEmbPassportType != null){
                    umEmbPassportSubType.setUmEmbPassportTypeByPassportType(umEmbPassportType);

                    embassyPassportSubTypeRepository.save(umEmbPassportSubType);
                }

                umEmbPassportSubType = embassyPassportSubTypeRepository.findTopByOrderByIdDesc();
                UmEmbPassportType umEmbPassportTypeById;
                if(umEmbPassportSubType != null){
                    umEmbPassportTypeById = embassyPassportRepository.findTopByIdOrderByIdDesc(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getId()).get();
                    umEmbPassportSubType.setUmEmbPassportTypeByPassportType(umEmbPassportTypeById);
                }

                umEmbPassportSubTypeResponse = new UmEmbPassportSubTypeResponse();
                umEmbPassportSubTypeResponse.setId(umEmbPassportSubType.getId());
                umEmbPassportSubTypeResponse.setPassportSubType(umEmbPassportSubType.getPassportSubType());
                umEmbPassportSubTypeResponse.setStatus(umEmbPassportSubType.getStatus());
                umEmbPassportSubTypeResponse.setDescription(umEmbPassportSubType.getDescription());
                umEmbPassportSubTypeResponse.setGeneralDelivery(umEmbPassportSubType.getEmergencyDelivery());
                umEmbPassportSubTypeResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());
                umEmbPassportSubTypeResponse.setEmbPassportFee(umEmbPassportSubType.getEmbPassportFee());
                umEmbPassportSubTypeResponse.setPocketServiceCharge(umEmbPassportSubType.getPocketServiceCharge());
                umEmbPassportSubTypeResponse.setTotalAmount(umEmbPassportSubType.getTotalAmount());
                umEmbPassportSubTypeResponse.setCreatedDate(umEmbPassportSubType.getCreatedDate());

                if(umEmbPassportSubType.getUmEmbPassportTypeByPassportType() != null){
                    umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getId());
                    umEmbPassportTypeResponse.setPassportType(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getPassportType());
                    umEmbPassportTypeResponse.setDescription(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getDescription());
                    umEmbPassportTypeResponse.setStatus(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(DateUtil.currentDateTime(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getCreatedDate()));
                }
                umEmbPassportSubTypeResponse.setUmEmbPassportTypeResponse(umEmbPassportTypeResponse);


                if(umEmbPassportSubTypeResponse != null){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportSubTypeResponse);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeSavedFailed.getKeyString(),
                            "Passport sub_ype is failed to save. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }
            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeSavedFailed.getKeyString(),
                    "Passport sub_ype is failed to save. Reason : "));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updatePassportSubType(UmEmbPassportSubTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();
        UmEmbPassportType umEmbPassportType;
        UmEmbPassportSubTypeResponse umEmbPassportSubTypeResponse = null;
        UmEmbPassportTypeResponse umEmbPassportTypeResponse = null;

        try {
            if(request != null){
                umEmbPassportSubType.setId(request.getId());
                umEmbPassportSubType.setPassportSubType(request.getPassportSubType());
                umEmbPassportSubType.setStatus("1");
                umEmbPassportSubType.setDescription(request.getDescription());
                umEmbPassportSubType.setGeneralDelivery(request.getGeneralDelivery());
                umEmbPassportSubType.setEmergencyDelivery(request.getEmergencyDelivery());
                umEmbPassportSubType.setEmbPassportFee(request.getEmbPassportFee());
                umEmbPassportSubType.setPocketServiceCharge(serviceChargeForPassport(request.getEmbPassportFee(), request.getPocketServiceCharge()));
                umEmbPassportSubType.setTotalAmount(totalPassportAmountFee(request.getEmbPassportFee(), request.getPocketServiceCharge()));
                umEmbPassportSubType.setCreatedDate(DateUtil.currentDateTime());

                umEmbPassportType = embassyPassportRepository.findTopByIdOrderByIdDesc(request.getUmEmbPassportTypeById().getId()).get();
                if (umEmbPassportType != null){
                    // first update parent class
                    umEmbPassportType.setPassportType(umEmbPassportType.getPassportType());
                    umEmbPassportType.setDescription(umEmbPassportType.getDescription());
                    umEmbPassportType.setStatus(umEmbPassportType.getStatus());
                    umEmbPassportType.setCreatedDate(new Date());

                    embassyPassportRepository.save(umEmbPassportType);

                    // second update child class
                    umEmbPassportSubType.setUmEmbPassportTypeByPassportType(umEmbPassportType);
                    embassyPassportSubTypeRepository.save(umEmbPassportSubType);
                }

                umEmbPassportSubType = embassyPassportSubTypeRepository.findTopByOrderByIdDesc();
                UmEmbPassportType umEmbPassportTypeById;
                if(umEmbPassportSubType != null){
                    umEmbPassportTypeById = embassyPassportRepository.findTopByIdOrderByIdDesc(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getId()).get();
                    umEmbPassportSubType.setUmEmbPassportTypeByPassportType(umEmbPassportTypeById);
                }

                umEmbPassportSubTypeResponse = new UmEmbPassportSubTypeResponse();
                umEmbPassportSubTypeResponse.setId(umEmbPassportSubType.getId());
                umEmbPassportSubTypeResponse.setPassportSubType(umEmbPassportSubType.getPassportSubType());
                umEmbPassportSubTypeResponse.setStatus(umEmbPassportSubType.getStatus());
                umEmbPassportSubTypeResponse.setDescription(umEmbPassportSubType.getDescription());
                umEmbPassportSubTypeResponse.setGeneralDelivery(umEmbPassportSubType.getEmergencyDelivery());
                umEmbPassportSubTypeResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());
                umEmbPassportSubTypeResponse.setEmbPassportFee(umEmbPassportSubType.getEmbPassportFee());
                umEmbPassportSubTypeResponse.setPocketServiceCharge(umEmbPassportSubType.getPocketServiceCharge());
                umEmbPassportSubTypeResponse.setTotalAmount(umEmbPassportSubType.getTotalAmount());
                umEmbPassportSubTypeResponse.setCreatedDate(umEmbPassportSubType.getCreatedDate());

                if(umEmbPassportSubType.getUmEmbPassportTypeByPassportType() != null){
                    umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getId());
                    umEmbPassportTypeResponse.setPassportType(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getPassportType());
                    umEmbPassportTypeResponse.setDescription(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getDescription());
                    umEmbPassportTypeResponse.setStatus(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(DateUtil.currentDateTime(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getCreatedDate()));
                }
                umEmbPassportSubTypeResponse.setUmEmbPassportTypeResponse(umEmbPassportTypeResponse);

                if(umEmbPassportSubTypeResponse != null){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportSubTypeResponse);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeSavedFailed.getKeyString(),
                            "Passport sub_ype is failed to update. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());
            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeSavedFailed.getKeyString(),
                    "Passport sub_type is failed to update. Reason : "));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject embassyPassportSubTypeListById(UmEmbPassportTypeRequest umEmbPassportTypeById, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<UmEmbPassportSubType> umEmbPassportSubTypeList = new ArrayList<>();
        List<UmEmbPassportSubTypeResponse> umEmbPassportSubTypeResponseList = new ArrayList<>();
        UmEmbPassportType umEmbPassportType = new UmEmbPassportType();

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(environment.getProperty("embassyMerchantMobileNo"));

        try {
            if(umEmbPassportTypeById != null){

                umEmbPassportType = embassyPassportRepository.findTopByPassportTypeIgnoreCaseAndStatusOrderByIdDesc(umEmbPassportTypeById.getPassportType(), "1");
               if(umEmbPassportType != null){
                   umEmbPassportSubTypeList =  embassyPassportSubTypeRepository.findByUmEmbPassportTypeByPassportType(umEmbPassportType);
               }

                for(UmEmbPassportSubType passportSubType : umEmbPassportSubTypeList){
                    UmEmbPassportSubTypeResponse subTypeResponse = new UmEmbPassportSubTypeResponse();
                    subTypeResponse.setId(passportSubType.getId());
                    subTypeResponse.setPassportSubType(passportSubType.getPassportSubType());
                    subTypeResponse.setStatus(passportSubType.getStatus());
                    subTypeResponse.setDescription(passportSubType.getDescription());
                    subTypeResponse.setGeneralDelivery(passportSubType.getGeneralDelivery());
                    subTypeResponse.setEmergencyDelivery(passportSubType.getEmergencyDelivery());
                    subTypeResponse.setEmbPassportFee(passportSubType.getEmbPassportFee());
                    subTypeResponse.setPocketServiceCharge(passportSubType.getPocketServiceCharge());
                    subTypeResponse.setTotalAmount(passportSubType.getTotalAmount());
                    subTypeResponse.setCreatedDate(passportSubType.getCreatedDate());
                    if(umUserInfo != null){
                        subTypeResponse.setEmbassyMerchantMobileNo(umUserInfo.getMobileNumber());
                    }


                    UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(passportSubType.getUmEmbPassportTypeByPassportType().getId());
                    umEmbPassportTypeResponse.setPassportType(passportSubType.getUmEmbPassportTypeByPassportType().getPassportType());
                    umEmbPassportTypeResponse.setDescription(passportSubType.getUmEmbPassportTypeByPassportType().getDescription());
                    umEmbPassportTypeResponse.setStatus(passportSubType.getUmEmbPassportTypeByPassportType().getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(DateUtil.getCurrentDateTime());

                    subTypeResponse.setUmEmbPassportTypeResponse(umEmbPassportTypeResponse);
                    umEmbPassportSubTypeResponseList.add(subTypeResponse);
                }

                if(umEmbPassportSubTypeResponseList != null && umEmbPassportSubTypeResponseList.size()>0){
                    baseResponseObject.setRequestId(umEmbPassportTypeById.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportSubTypeResponseList);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport sub_type list is empty. Reason : "));
                    responseObject.setRequestId(umEmbPassportTypeById.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(umEmbPassportTypeById.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport sub_type list is empty. Reason : "));
            responseObject.setRequestId(umEmbPassportTypeById.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;

    }

    @Override
    public BaseResponseObject embassyPassportSubTypeList(UmEmbPassportSubTypeEmptyRequest request, TsSessionObject tsSessionObject) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<UmEmbPassportSubTypeResponse> umEmbPassportSubTypeResponseList = new ArrayList<>();
        List<UmEmbPassportSubType> umEmbPassportSubTypeListTemp = new ArrayList<>();

        try {
            if(request != null){

                umEmbPassportSubTypeListTemp = (List<UmEmbPassportSubType>) embassyPassportSubTypeRepository.findAll();

                for(UmEmbPassportSubType passportSubType : umEmbPassportSubTypeListTemp){
                    UmEmbPassportSubTypeResponse subTypeResponse = new UmEmbPassportSubTypeResponse();
                    subTypeResponse.setId(passportSubType.getId());
                    subTypeResponse.setPassportSubType(passportSubType.getPassportSubType());
                    subTypeResponse.setStatus(passportSubType.getStatus());
                    subTypeResponse.setDescription(passportSubType.getDescription());
                    subTypeResponse.setGeneralDelivery(passportSubType.getGeneralDelivery());
                    subTypeResponse.setEmergencyDelivery(passportSubType.getEmergencyDelivery());
                    subTypeResponse.setEmbPassportFee(passportSubType.getEmbPassportFee());
                    subTypeResponse.setPocketServiceCharge(passportSubType.getPocketServiceCharge());
                    subTypeResponse.setTotalAmount(passportSubType.getTotalAmount());
                    subTypeResponse.setCreatedDate(passportSubType.getCreatedDate());

                    UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                    umEmbPassportTypeResponse.setId(passportSubType.getUmEmbPassportTypeByPassportType().getId());
                    umEmbPassportTypeResponse.setPassportType(passportSubType.getUmEmbPassportTypeByPassportType().getPassportType());
                    umEmbPassportTypeResponse.setDescription(passportSubType.getUmEmbPassportTypeByPassportType().getDescription());
                    umEmbPassportTypeResponse.setStatus(passportSubType.getUmEmbPassportTypeByPassportType().getStatus());
                    umEmbPassportTypeResponse.setCreatedDate(DateUtil.currentDateTime(passportSubType.getUmEmbPassportTypeByPassportType().getCreatedDate()));

                    subTypeResponse.setUmEmbPassportTypeResponse(umEmbPassportTypeResponse);
                    umEmbPassportSubTypeResponseList.add(subTypeResponse);
                }

                if(umEmbPassportSubTypeResponseList != null && umEmbPassportSubTypeResponseList.size()>0){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(umEmbPassportSubTypeResponseList);
                    return baseResponseObject;
                }else{
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport sub_type list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport sub_type list is empty. Reason : "));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;

    }

    @Override
    public BaseResponseObject searchPassportSubTypeAndStatus(UmEmbPassportSubTypeRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();
        UmEmbPassportType umEmbPassportType = new UmEmbPassportType();
        UmEmbPassportSubTypeResponse subTypeResponse = new UmEmbPassportSubTypeResponse();

        try {
            if(request != null){

                umEmbPassportSubType = embassyPassportSubTypeRepository.findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(request.getPassportSubType(), request.getStatus());
                umEmbPassportType = embassyPassportRepository.findTopByIdOrderByIdDesc(umEmbPassportSubType.getId()).get();

                if(umEmbPassportSubType != null && umEmbPassportType != null ){
                    umEmbPassportSubType.setUmEmbPassportTypeByPassportType(umEmbPassportType);

                    if(umEmbPassportSubType != null){
                        subTypeResponse.setId(umEmbPassportSubType.getId());
                        subTypeResponse.setPassportSubType(umEmbPassportSubType.getPassportSubType());
                        subTypeResponse.setStatus(umEmbPassportSubType.getStatus());
                        subTypeResponse.setDescription(umEmbPassportSubType.getDescription());
                        subTypeResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                        subTypeResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());
                        subTypeResponse.setEmbPassportFee(umEmbPassportSubType.getEmbPassportFee());
                        subTypeResponse.setPocketServiceCharge(umEmbPassportSubType.getPocketServiceCharge());
                        subTypeResponse.setTotalAmount(umEmbPassportSubType.getTotalAmount());
                        subTypeResponse.setCreatedDate(umEmbPassportSubType.getCreatedDate());

                        UmEmbPassportTypeResponse umEmbPassportTypeResponse = new UmEmbPassportTypeResponse();
                        umEmbPassportTypeResponse.setId(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getId());
                        umEmbPassportTypeResponse.setPassportType(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getPassportType());
                        umEmbPassportTypeResponse.setDescription(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getDescription());
                        umEmbPassportTypeResponse.setStatus(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getStatus());
                        umEmbPassportTypeResponse.setCreatedDate(DateUtil.currentDateTime(umEmbPassportSubType.getUmEmbPassportTypeByPassportType().getCreatedDate()));

                        subTypeResponse.setUmEmbPassportTypeResponse(umEmbPassportTypeResponse);
                    }

                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(subTypeResponse);
                    return baseResponseObject;
                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport sub_type list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport sub_type list is empty. Reason : "));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    public BigDecimal serviceChargeForPassport(BigDecimal embPassportFee, BigDecimal pocketServiceChargeFee){
        return embPassportFee.multiply(pocketServiceChargeFee).divide(BigDecimal.valueOf(100));
    }
    public BigDecimal totalPassportAmountFee(BigDecimal embPassportFee, BigDecimal pocketServiceChargeFee){
        BigDecimal serviceCharge = embPassportFee.multiply(pocketServiceChargeFee).divide(BigDecimal.valueOf(100));
        return embPassportFee.add(serviceCharge);
    }

    public void checkDuplicatePassportSubType(UmEmbPassportSubTypeRequest request, String passportSubType) throws PocketException {
        if(request.getStatus() == null || request.getStatus().equals("string") || request.getStatus().isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Passport status is required");
            throw new PocketException(PocketErrorCode.PassPortStatus);
        }

        UmEmbPassportSubType umEmbPassportSubType =  embassyPassportSubTypeRepository.
                findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(passportSubType, PocketConstants.PASSPORT_STATUS);
        if(umEmbPassportSubType != null){
            logWriterUtility.error(request.getRequestId(), "Passport sub type is already existed.");
            throw new PocketException(PocketErrorCode.PassportSubTypeAlreadyExist);
        }
    }

}
