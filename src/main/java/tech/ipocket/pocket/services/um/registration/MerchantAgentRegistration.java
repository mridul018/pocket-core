package tech.ipocket.pocket.services.um.registration;


import tech.ipocket.pocket.request.um.BankData;
import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.request.um.UserDocuments;
import tech.ipocket.pocket.utils.DateUtil;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Date;
import java.util.List;

public class MerchantAgentRegistration extends AbstractRegistration {

    private String srWallet;
    private String email;
    private String primaryIdNumber; // m
    private Date primaryIdIssueDate; // m
    private Date primaryIdExpiryDate; // m
    private String primaryIdType; //m
    private String nationality; // m
    private String deviceName;
    private String metaData;

    private String applicantsName;
    private String tradeLicenseNo;
    private String secondContactName;
    private String secondContactMobileNo;
    private String thirdContactName;
    private String thirdContactMobileNo;


    private BankData bankData;
    private String gender;
    private String presentAddress;
    private String permanentAddress;
    private String postCode;
    private List<UserDocuments> userDocuments;


    private String accountNumber;
    private String accountName;
    private String bankCode;
    private String bankName;
    private String bankRoutingNo;
    private String branchName;
    private String branchSwiftCode;

    @Override
    public void setData(RegistrationRequest registrationRequest) throws PocketException {


        this.accountNumber=registrationRequest.getAccountNumber();
        this.accountName=registrationRequest.getAccountName();
        this.bankCode=registrationRequest.getBankCode();
        this.bankName=registrationRequest.getBankName();
        this.bankRoutingNo=registrationRequest.getBankRoutingNo();
        this.branchName=registrationRequest.getBranchName();
        this.branchSwiftCode=registrationRequest.getBranchSwiftCode();
        if(registrationRequest.getDateOfBirth()!=null){
            try {
                this.setDateOfBirth(DateUtil.parseDate(registrationRequest.getDateOfBirth()));
            } catch (PocketException e) {
                e.printStackTrace();
            }
        }

        this.setSrWallet(registrationRequest.getSrWallet());
        this.setFullName(registrationRequest.getFullName());
        this.setMobileNumber(registrationRequest.getMobileNo());
        this.setPassword(registrationRequest.getPassword());
        this.setGroupCode(registrationRequest.getGroupCode());
        this.setRequestId(registrationRequest.getRequestId());
        this.setBankData(registrationRequest.getBankData());

        this.setGender(registrationRequest.getGender());
        this.setPresentAddress(registrationRequest.getPresentAddress());
        this.setPermanentAddress(registrationRequest.getPermanentAddress());
        this.setPostCode(registrationRequest.getPostCode());

        if(registrationRequest.getPrimaryIdNumber()==null){
            throw new PocketException(PocketErrorCode.PrimayIdNumberRequired);
        }

        if(registrationRequest.getPrimaryIdIssueDate()==null){
            throw new PocketException(PocketErrorCode.PrimayIdNumberIssueDateRequired);
        }

        if(registrationRequest.getPrimaryIdExpiryDate()==null){
            throw new PocketException(PocketErrorCode.PrimayIdNumberExpiryDateRequired);
        }

        this.setPrimaryIdNumber(registrationRequest.getPrimaryIdNumber());
        this.setPrimaryIdIssueDate(DateUtil.parseDate(registrationRequest.getPrimaryIdIssueDate()));
        this.setPrimaryIdExpiryDate(DateUtil.parseDate(registrationRequest.getPrimaryIdExpiryDate()));
        this.setPrimaryIdType(registrationRequest.getPrimaryIdType());
        this.setDeviceName(registrationRequest.getDeviceName());
        this.setMetaData(registrationRequest.getMetaData());
        this.setHardwareSignature(registrationRequest.getHardwareSignature());
        this.setEmail(registrationRequest.getEmail());

        this.setApplicantsName(registrationRequest.getApplicantsName());
        this.setTradeLicenseNo(registrationRequest.getTradeLicenseNo());
        this.setSecondContactName(registrationRequest.getSecondContactName());
        this.setSecondContactMobileNo(registrationRequest.getSecondContactMobileNo());

        this.setThirdContactName(registrationRequest.getThirdContactName());
        this.setThirdContactMobileNo(registrationRequest.getThirdContactMobileNo());
        this.userDocuments=registrationRequest.getUserDocuments();


        this.setCountryCode(registrationRequest.getCountryCode());
        this.setStateId(registrationRequest.getStateId());
        this.setNationality(registrationRequest.getNationality());

    }

    public String getSrWallet() {
        return srWallet;
    }

    public void setSrWallet(String srWallet) {
        this.srWallet = srWallet;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    public Date getPrimaryIdIssueDate() {
        return primaryIdIssueDate;
    }

    public void setPrimaryIdIssueDate(Date primaryIdIssueDate) {
        this.primaryIdIssueDate = primaryIdIssueDate;
    }

    public Date getPrimaryIdExpiryDate() {
        return primaryIdExpiryDate;
    }

    public void setPrimaryIdExpiryDate(Date primaryIdExpiryDate) {
        this.primaryIdExpiryDate = primaryIdExpiryDate;
    }

    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public String getApplicantsName() {
        return applicantsName;
    }

    public void setApplicantsName(String applicantsName) {
        this.applicantsName = applicantsName;
    }

    public String getTradeLicenseNo() {
        return tradeLicenseNo;
    }

    public void setTradeLicenseNo(String tradeLicenseNo) {
        this.tradeLicenseNo = tradeLicenseNo;
    }

    public String getSecondContactName() {
        return secondContactName;
    }

    public void setSecondContactName(String secondContactName) {
        this.secondContactName = secondContactName;
    }

    public String getSecondContactMobileNo() {
        return secondContactMobileNo;
    }

    public void setSecondContactMobileNo(String secondContactMobileNo) {
        this.secondContactMobileNo = secondContactMobileNo;
    }

    public String getThirdContactName() {
        return thirdContactName;
    }

    public void setThirdContactName(String thirdContactName) {
        this.thirdContactName = thirdContactName;
    }

    public String getThirdContactMobileNo() {
        return thirdContactMobileNo;
    }

    public void setThirdContactMobileNo(String thirdContactMobileNo) {
        this.thirdContactMobileNo = thirdContactMobileNo;
    }

    public BankData getBankData() {
        return bankData;
    }

    public void setBankData(BankData bankData) {
        this.bankData = bankData;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public List<UserDocuments> getUserDocuments() {
        return userDocuments;
    }

    public void setUserDocuments(List<UserDocuments> userDocuments) {
        this.userDocuments = userDocuments;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankRoutingNo() {
        return bankRoutingNo;
    }

    public void setBankRoutingNo(String bankRoutingNo) {
        this.bankRoutingNo = bankRoutingNo;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public void setBranchSwiftCode(String branchSwiftCode) {
        this.branchSwiftCode = branchSwiftCode;
    }
}
