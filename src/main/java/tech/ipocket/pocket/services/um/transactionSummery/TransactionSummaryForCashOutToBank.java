package tech.ipocket.pocket.services.um.transactionSummery;

import tech.ipocket.pocket.request.um.TransactionSummaryViewRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

import java.util.concurrent.ExecutionException;

public class TransactionSummaryForCashOutToBank extends AbstractTransactionSummary {

    public TransactionSummaryForCashOutToBank(TransactionSummaryService transactionSummaryService) {
        super(transactionSummaryService);
    }

    @Override
    public BaseResponseObject getTransactionSummary(TransactionSummaryViewRequest transactionSummaryViewRequest) throws PocketException, ExecutionException, InterruptedException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();

        ReceiverInfo senderInfo= checkIsValidReceiver(transactionSummaryViewRequest.getMobileNumber(), transactionSummaryViewRequest.getRequestId());


        // TODO: 2019-05-08 work here
        TransactionSummaryViewResponse transactionSummary= callTxForTransactionSummary(transactionSummaryViewRequest);


        TransactionSummaryViewResponse transactionSummaryViewResponse = new TransactionSummaryViewResponse();
        transactionSummaryViewResponse.setReceiverName(senderInfo.getFullName());
        transactionSummaryViewResponse.setProfileImageUrl(senderInfo.getProfileImageUrl());

        transactionSummaryViewResponse.setReceiverMobileNumber(transactionSummaryViewRequest.getMobileNumber());
        transactionSummaryViewResponse.setFee(transactionSummary.getFee());
        transactionSummaryViewResponse.setFeePayer(transactionSummary.getFeePayer());
        baseResponseObject.setData(transactionSummaryViewResponse);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(transactionSummaryViewRequest.getRequestId());

        return baseResponseObject;
    }


}