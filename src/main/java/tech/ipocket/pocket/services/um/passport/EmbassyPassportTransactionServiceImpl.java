package tech.ipocket.pocket.services.um.passport;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.EmbassyPassportRepository;
import tech.ipocket.pocket.repository.um.EmbassyPassportSubTypeRepository;
import tech.ipocket.pocket.repository.um.EmbassyPassportTransactionRepository;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.um.passport.EmbassyPassportTransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.transaction.TransactionResponseData;
import tech.ipocket.pocket.response.um.passport.EmbassyPassportTransactionResponse;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

@Service
public class EmbassyPassportTransactionServiceImpl implements EmbassyPassportTransactionService{

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;
    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;
    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private TransactionValidator transactionValidator;
    @Autowired
    private TsGlRepository tsGlRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;


    @Autowired
    private EmbassyPassportServiceImpl embassyPassportServiceImpl;
    @Autowired
    private EmbassyPassportRepository embassyPassportRepository;
    @Autowired
    private EmbassyPassportSubTypeServiceImpl embassyPassportSubTypeServiceImpl;
    @Autowired
    private EmbassyPassportSubTypeRepository embassyPassportSubTypeRepository;
    @Autowired
    private EmbassyPassportTransactionRepository embassyPassportTransactionRepository;


    EmbassyPassportTransactionResponse embassyPassportTransactionResponse = new EmbassyPassportTransactionResponse();
    @Override
    public BaseResponseObject doPassportFeePayment(EmbassyPassportTransactionRequest request, TsSessionObject tsSessionObject) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientSender = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), statusNotIn);
        if (clientSender == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        //Embassy merchant wallet
        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), statusNotIn);
        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(request.getTransactionType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //check duplicate (start)
        EmbassyPassportTransaction checkMrpPassport = embassyPassportTransactionRepository.
                findByMrpPassportNumberIgnoreCaseAndStatus(request.getMrpPassportNumber(), PocketConstants.PASSPORT_STATUS);

        EmbassyPassportTransaction checkEmirateId = embassyPassportTransactionRepository.
                findByEmirateResidentialIdIgnoreCaseAndStatus(request.getEmirateOrResidentialId(), PocketConstants.PASSPORT_STATUS);

        List<EmbassyPassportTransaction> checkPassSubType = embassyPassportTransactionRepository.
                findAllByPassportSubTypeIgnoreCaseAndStatus(request.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

        if(checkMrpPassport != null){
            logWriterUtility.error(request.getRequestId(), "Duplicate mrp passport number is not allowed.");
            throw new PocketException(PocketErrorCode.DuplicateMrpPassportNumber);
        }
        if(checkEmirateId != null){
            logWriterUtility.error(request.getRequestId(), "Duplicate emirateId is not allowed.");
            throw new PocketException(PocketErrorCode.DuplicateEmirateId);
        }
//        if(checkPassSubType != null){
//            logWriterUtility.error(request.getRequestId(), "Duplicate passport sub type is not allowed.");
//            throw new PocketException(PocketErrorCode.DuplicatePassportSubType);
//        }
//        if(checkMrpPassport != null || checkEmirateId != null || checkPassSubType != null){
//            logWriterUtility.error(request.getRequestId(), "Duplicate value is not allowed.");
//            throw new PocketException(PocketErrorCode.PassportDuplicateValueField);
//        }
        //end


        //Calculate fee start  (fee have to change and will be followed {UM_EMB_PASSPORT_SUB_TYPE} table )     // have to work here...
        FeeData feeData = feeManager.calculateFee(clientSender, cfeService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }
        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransferAmount()));
        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");
        //calculate fee end


        //Transaction profile validation for sender & check balance limit for sender
        Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSender, cfeService,
                transactionAmountData.getSenderDebitAmount(), request.getRequestId());
        if (!isSenderProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
            throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
        }

        //Transaction profile validation for receiver
        Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(clientReceiver, cfeService, request.getRequestId());
        if (!isReceiverProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
            throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
        }



        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //Build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(clientSender, clientReceiver, cfeService.getServiceCode(),feeData, transactionAmountData);
        tsTransactionRepository.save(cfeTransaction);

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");


        //Build transaction detail
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();

        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.DEBIT,
                clientSender.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                clientReceiver.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");


        //Build gl entry
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();

        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Deposit_of_Customer, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Deposit_of_Customer, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_FundTransfer, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        tsGlTransactionDetailsRepository.saveAll(glTxDetailList);


        //Sender & Receiver balance update
        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSender, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(clientReceiver, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setSenderRunningBalance(senderRunningBalance);
        cfeTransaction.setReceiverRunningBalance(receiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);


        //After full complete in tsTransaction , then hit the UM_EMB_PASSPORT_TRANSACTION_NFO tables;
        EmbassyPassportTransaction embassyPassportTransaction = buildEmbassyPassportTransaction(request, cfeTransaction,clientSender);
        if(embassyPassportTransaction != null){
            embassyPassportTransactionRepository.save(embassyPassportTransaction);
        }

        //also update tsTransaction Table using set of some filed value from getting  embassyPassportTransaction
        embassyPassportTransaction = embassyPassportTransactionRepository.findTopByOrderByIdDesc();
        if(embassyPassportTransaction != null){
            cfeTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
            cfeTransaction.setRefTransactionToken(embassyPassportTransaction.getUniquePassportGeneratedId());
            String notes = "Passport fee is paid for " + embassyPassportTransaction.getPassportSubType().concat("with generatedId of ")
                    .concat(embassyPassportTransaction.getUniquePassportGeneratedId().concat(" via ").concat(embassyPassportTransaction.getPayMode()));
            cfeTransaction.setNotes(notes);
            cfeTransaction.setRequestId(embassyPassportTransaction.getEmirateResidentialId());

            // have to work here...
            cfeTransaction.setFeeCode("1002"); //3003
//            cfeTransaction.setFeePayer(); //DR,CR
            cfeTransaction.setFeeAmount(embassyPassportTransaction.getPocketServiceCharge());
            tsTransactionRepository.save(cfeTransaction);
        }


        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setSenderRunningBalance(senderRunningBalance);
        fundTransferResponseData.setTxToken(cfeTransaction.getToken());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();
        List<String> status=new ArrayList<>();
        status.add(PocketConstants.PASSPORT_STATUS);

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(transactionAmountData.getOriginalTransactionAmount(), clientReceiver.getWalletNo(),
                        clientSender.getWalletNo(), cfeTransaction.getToken(), cfeService.getServiceCode(),
                        feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes(),
                        request.getPassportSubType());

        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));
        CompletableFuture.runAsync(() -> {
            mfsFeeSharingService.doFeeShareForIndividualTransaction(cfeTransaction.getToken(), request.getRequestId());   //have to work here...
        });

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(fundTransferResponseData);
        return baseResponseObject;
    }



    public EmbassyPassportTransaction buildEmbassyPassportTransaction(EmbassyPassportTransactionRequest passportTransactionRequest,
                                                                       TsTransaction cfeTransaction,
                                                                      TsClient clientSender){
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        if(cfeTransaction != null && clientSender != null){
            embassyPassportTransaction.setName(clientSender.getFullName());
            embassyPassportTransaction.setFatherName(passportTransactionRequest.getFatherName());
            embassyPassportTransaction.setBdMobileNo(passportTransactionRequest.getBdMobileNo());
            embassyPassportTransaction.setUaeMobileNo(clientSender.getWalletNo());
            embassyPassportTransaction.setUniquePassportGeneratedId(uniquePassportGeneratedNumber());
            embassyPassportTransaction.setMrpPassportNumber(passportTransactionRequest.getMrpPassportNumber());
            embassyPassportTransaction.setPreviousPassportNumber(passportTransactionRequest.getPreviousPassportNumber());
            embassyPassportTransaction.setPassportType(passportTransactionRequest.getPassportType());
            embassyPassportTransaction.setPassportSubType(passportTransactionRequest.getPassportSubType());
            embassyPassportTransaction.setEmbassyPassportFee(passportTransactionRequest.getEmbassyPassportFee());
            embassyPassportTransaction.setPocketServiceCharge(passportTransactionRequest.getPocketServiceCharge());
            embassyPassportTransaction.setTotalPassportFeeAmount(passportTransactionRequest.getTotalPassportFeeAmount());
            embassyPassportTransaction.setProfessionOrSkill(passportTransactionRequest.getProfessionOrSkill());
            embassyPassportTransaction.setStatus(passportTransactionRequest.getStatus());
            embassyPassportTransaction.setCreateDate(new Date());
            embassyPassportTransaction.setEmirateResidentialId(passportTransactionRequest.getEmirateOrResidentialId());
            embassyPassportTransaction.setPayMode(passportTransactionRequest.getPayMode());
            embassyPassportTransaction.setGender(passportTransactionRequest.getGender());
            embassyPassportTransaction.setDateOfBirth(passportTransactionRequest.getDateOfBirth());
            embassyPassportTransaction.setCurrentPassportExpiryDate(passportTransactionRequest.getCurrentPassportExpiryDate());

            embassyPassportTransaction.setTsTransactionId(String.valueOf(cfeTransaction.getId()));
            embassyPassportTransaction.setTsTransactionToken(cfeTransaction.getToken());
            embassyPassportTransaction.setTsTransactionSender(cfeTransaction.getLogicalSender());
            embassyPassportTransaction.setTsTransactionReceiver(cfeTransaction.getLogicalReceiver());
        }

        return embassyPassportTransaction;
    }


    public String uniquePassportGeneratedNumber(){
        int leftLimit = 48;
        int rightLimit = 122;
        int targetStringLength = 16;
        Random random = new Random();

        String passportGeneratedNumber = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return passportGeneratedNumber.toUpperCase();
    }


}
