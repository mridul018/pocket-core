package tech.ipocket.pocket.services.um.transactionSummery;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.um.TransactionSummaryViewRequest;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface TransactionSummaryService {
    UmUserInfo checkIsValidSender(Integer credentialId, String requestId) throws PocketException;

    ReceiverInfo checkIsValidReceiver(String receiverMobileNumber, String requestId, boolean needToCheckMposOrNot) throws PocketException;
    ReceiverInfo checkIsValidReceiverBillpay(String receiverMobileNumber, String requestId, boolean needToCheckMposOrNot) throws PocketException;

    TransactionSummaryViewResponse callTxForTransactionSummary(TransactionSummaryViewRequest transactionSummaryRequest)
            throws PocketException;
}
