package tech.ipocket.pocket.services.um.otp;

public class OtpObject {
    private String otpText;
    private String otpSecret;

    public OtpObject(String otpText, String otpSecret) {
        this.otpText = otpText;
        this.otpSecret = otpSecret;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }
}
