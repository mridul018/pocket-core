package tech.ipocket.pocket.services.um.device;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmActivityLog;
import tech.ipocket.pocket.entity.UmUserDevice;
import tech.ipocket.pocket.entity.UmUserDeviceMapping;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserActivityLogRepository;
import tech.ipocket.pocket.repository.um.UserDeviceMapRepository;
import tech.ipocket.pocket.repository.um.UserDeviceRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.um.GetActivityLogRequest;
import tech.ipocket.pocket.request.um.UserDeviceListUpdateRequest;
import tech.ipocket.pocket.response.um.ActivityLogResponse;
import tech.ipocket.pocket.response.um.UserDeviceListUpdateResponse;
import tech.ipocket.pocket.response.um.UserDeviceResponse;
import tech.ipocket.pocket.utils.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserDeviceServiceImpl implements UserDeviceService {

    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;
    @Autowired
    private UserActivityLogRepository activityLogsRepository;

    @Override
    public ArrayList<UserDeviceResponse> getAllUserDevice(String loginId) throws PocketException {

        ArrayList<UserDeviceResponse> responses;
        List<UmUserDeviceMapping> userDeviceMappings = userDeviceMapRepository.findByUmUserInfoByUserId(userInfoRepository.findByLoginId(loginId));

        if (userDeviceMappings == null) {
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        responses = new ArrayList<>();

        userDeviceMappings.forEach(map -> {
            UmUserDevice device = map.getUmUserDeviceByDeviceId();

            responses.add(new UserDeviceResponse(
                    device.getId(), device.getHardwareSignature(), device.getDeviceName(),
                    device.getMetaData(), device.getCreatedDate(), map.getStatus()
            ));

        });

        return responses;
    }

    @Override
    public UserDeviceListUpdateResponse updateUserDevice(UserDeviceListUpdateRequest request) throws PocketException {

        UserDeviceListUpdateResponse response = new UserDeviceListUpdateResponse();

        UmUserInfo userInfo = new UmUserInfo();
        userInfo.setId(request.getUserId());

        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setId(request.getDeviceId());

        UmUserDeviceMapping mapping = userDeviceMapRepository
                .findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(
                        userInfo, userDevice, request.getStatus()
                );
        if (mapping == null) {
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        mapping.setStatus(request.getStatus());
        userDeviceMapRepository.save(mapping);

        response.setUpdateCompleted(true);

        return response;
    }

    @Override
    public List<ActivityLogResponse> getActivityLog(GetActivityLogRequest request) throws PocketException {

        List<UmActivityLog> activityLogs;

        if(request.getStartDate()==null||request.getEndDate()==null){
            throw new PocketException(PocketErrorCode.DateRangeRequired);
        }else {

            Date fromDate = DateUtil.parseDate(request.getStartDate());

            Date toDate = DateUtil.parseDate(request.getEndDate());
            toDate = DateUtil.addDay(toDate, 1);

            if(request.getMobileNumber()==null||request.getMobileNumber().trim().length()==0){
                activityLogs=activityLogsRepository.findAllByCreatedDateBetweenOrderByIdDesc(fromDate,toDate);
            }else{
                activityLogs=activityLogsRepository.findAllByMobileNumberAndCreatedDateBetweenOrderByIdDesc(request.getMobileNumber(),fromDate,toDate);
            }
        }

        List<ActivityLogResponse> logResponses=new ArrayList<>();
        if(activityLogs==null||activityLogs.size()==0){
            return logResponses;
        }

        for (UmActivityLog log:activityLogs) {
            ActivityLogResponse logResponse=new ActivityLogResponse();

            String logType= UmEnums.ActivityLogType.getLogNameByType(log.getLogType());

            logResponse.setLogType(logType);
            logResponse.setId(log.getId());
            logResponse.setCreatedDate(log.getCreatedDate());
            logResponse.setDeviceInfo(log.getDeviceInfo());
            logResponse.setStatus(log.getStatus());
            logResponse.setMetaData(log.getMetaData());
            logResponse.setUpdatedDate(log.getUpdatedDate());
            logResponse.setMobileNumber(log.getMobileNumber());
            logResponse.setFullName(log.getUmUserInfoByUserId().getFullName());
            logResponse.setCreatedDateString(DateUtil.formattedDate(log.getCreatedDate()));
            logResponses.add(logResponse);
        }


        return logResponses;
    }
}
