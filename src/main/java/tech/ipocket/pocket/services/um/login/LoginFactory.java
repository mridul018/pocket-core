package tech.ipocket.pocket.services.um.login;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.um.LoginRequest;
import tech.ipocket.pocket.response.um.LoginResponse;
import tech.ipocket.pocket.utils.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Scope("prototype")
@Service
public class LoginFactory implements ApplicationContextAware {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());
    private ApplicationContext context;

    @Autowired
    private UserInfoRepository userInfoRepository;

    private LoginService loginService;


    public LoginResponse doLogin(LoginRequest loginRequest) throws PocketException, IOException {

        List<Integer> statusIn = new ArrayList<>();
        statusIn.add(UmEnums.UserStatus.Verified.getUserStatusType());
        statusIn.add(UmEnums.UserStatus.Blocked.getUserStatusType());
        statusIn.add(UmEnums.UserStatus.HardBlocked.getUserStatusType());

        UmUserInfo umUserInfo = userInfoRepository.findByLoginIdAndAccountStatusIn(loginRequest.getLoginId(), statusIn);

        if (umUserInfo == null) {
            logWriterUtility.error(loginRequest.getRequestId(), "User not found using this loginId");
            throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
        }

        //(Block Customer login in Merchant app)
        if(umUserInfo.getGroupCode().equalsIgnoreCase("1001")){
            if(loginRequest.getSource().equalsIgnoreCase("AGENT_APP")){
                logWriterUtility.error(loginRequest.getRequestId(), "Customer can't login in Merchant App");
                return null;
            }
        }
        //(Block Merchant login in Customer app)
        if(umUserInfo.getGroupCode().equalsIgnoreCase("1003")){
            if(loginRequest.getSource().equalsIgnoreCase("CUSTOMER_APP")){
                logWriterUtility.error(loginRequest.getRequestId(), "Merchant can't login in Customer App");
                return null;
            }
        }

        loginService = context.getBean(LoginService.class);

        switch (umUserInfo.getGroupCode()) {
            case GroupCodeUtils.CUSTOMER:
            case GroupCodeUtils.AGENT:
            case GroupCodeUtils.MERCHANT:

                Boolean isUserAbleToLogin = loginService.isUserStatusAbleToLogin(umUserInfo, loginRequest);

                logWriterUtility.trace(loginRequest.getRequestId(), "isUserAbleToLogin : " + isUserAbleToLogin);

                Boolean isNewDevice = loginService.checkIsNewDevice(umUserInfo, loginRequest);


                String sessionToken = loginService.validatePasswordAndGenerateToken(loginRequest.getCredential(),umUserInfo, loginRequest, isNewDevice);

                if (sessionToken == null) {
                    throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
                }

                return loginService.generateLoginResponse(umUserInfo, loginRequest.getRequestId(), isNewDevice, sessionToken);


            case GroupCodeUtils.ADMIN:
            case GroupCodeUtils.SUPER_ADMIN:

                isUserAbleToLogin = loginService.isUserStatusAbleToLogin(umUserInfo, loginRequest);

                logWriterUtility.trace(loginRequest.getRequestId(), "isUserAbleToLogin : " + isUserAbleToLogin);


                sessionToken = loginService.validatePasswordAndGenerateToken(loginRequest.getCredential(),umUserInfo, loginRequest, false);

                if (sessionToken == null) {
                    throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
                }

                return loginService.generateLoginResponse(umUserInfo, loginRequest.getRequestId(), false, sessionToken);

            default:
                isUserAbleToLogin = loginService.isUserStatusAbleToLogin(umUserInfo, loginRequest);

                logWriterUtility.trace(loginRequest.getRequestId(), "isUserAbleToLogin : " + isUserAbleToLogin);


                sessionToken = loginService.validatePasswordAndGenerateToken(loginRequest.getCredential(),umUserInfo, loginRequest, false);

                if (sessionToken == null) {
                    throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
                }

                return loginService.generateLoginResponse(umUserInfo, loginRequest.getRequestId(), false, sessionToken);
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
