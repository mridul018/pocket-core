package tech.ipocket.pocket.services.um.passport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.EmbassyPassportRepository;
import tech.ipocket.pocket.repository.um.EmbassyPassportSubTypeRepository;
import tech.ipocket.pocket.repository.um.EmbassyPassportTransactionRepository;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.passport.EmbPassportTransactionHistoryRequest;
import tech.ipocket.pocket.request.um.passport.EmbPassportTrxReportRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.um.passport.EmbPassportTransactionHistoryResponse;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.utils.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class EmbPassportTransactionHistoryServiceImpl implements EmbPassportTransactionHistoryService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;
    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;
    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private TransactionValidator transactionValidator;
    @Autowired
    private TsGlRepository tsGlRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;

    @Autowired
    private EmbassyPassportServiceImpl embassyPassportServiceImpl;
    @Autowired
    private EmbassyPassportRepository embassyPassportRepository;
    @Autowired
    private EmbassyPassportSubTypeServiceImpl embassyPassportSubTypeServiceImpl;
    @Autowired
    private EmbassyPassportSubTypeRepository embassyPassportSubTypeRepository;
    @Autowired
    private EmbassyPassportTransactionRepository embassyPassportTransactionRepository;


    // Mobile
    @Override
    public BaseResponseObject trxHistoryReport(EmbPassportTransactionHistoryRequest request, TsSessionObject tsSessionObject) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        TsTransaction tsTransaction = new TsTransaction();
        List<TsTransaction> tsTransactionList = new ArrayList<>();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        UmEmbPassportType umEmbPassportType = new UmEmbPassportType();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();
        TsClient senderClient = new TsClient();
        TsClient receiverClient = new TsClient();

        try {
            if (request != null) {
                tsTransactionList = tsTransactionRepository.findAllBySenderWalletAndTransactionType(request.getSenderWalletNo(), request.getTransactionType());

                if (tsTransactionList != null && tsTransactionList.size() > 0) {

                    for (TsTransaction tsTrx : tsTransactionList) {
                        EmbPassportTransactionHistoryResponse passportTransactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        embassyPassportTransaction = embassyPassportTransactionRepository.
                                findByUaeMobileNoAndUniquePassportGeneratedIdAndTsTransactionIdAndTsTransactionToken(
                                        tsTrx.getSenderWallet(),
                                        tsTrx.getRefTransactionToken(),
                                        String.valueOf(tsTrx.getId()),
                                        tsTrx.getToken());


                        embassyPassportTransactionList.add(embassyPassportTransaction);

                        for (EmbassyPassportTransaction embassyPassportTrx : embassyPassportTransactionList) {
                            umEmbPassportType = embassyPassportRepository.
                                    findTopByPassportTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTrx.getPassportType(), String.valueOf(1));

                            umEmbPassportSubType = embassyPassportSubTypeRepository.
                                    findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTrx.getPassportSubType(), String.valueOf(1));


                            //UM_EMB_PASSPORT_TRANSACTION_INFO
                            passportTransactionHistoryResponse.setSenderName(embassyPassportTrx.getName());
                            passportTransactionHistoryResponse.setSenderFatherName(embassyPassportTrx.getFatherName());
                            passportTransactionHistoryResponse.setSenderBdMobileNo(embassyPassportTrx.getBdMobileNo());
                            passportTransactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTrx.getUniquePassportGeneratedId());
                            passportTransactionHistoryResponse.setMrpPassportNumber(embassyPassportTrx.getMrpPassportNumber());
                            passportTransactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTrx.getEmirateResidentialId());
                            passportTransactionHistoryResponse.setProfessionOrSkill(embassyPassportTrx.getProfessionOrSkill());
                            passportTransactionHistoryResponse.setPayMode(embassyPassportTrx.getPayMode());
                            passportTransactionHistoryResponse.setGender(embassyPassportTrx.getGender());
                            passportTransactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTrx.getDateOfBirth()));
                            passportTransactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTrx.getCurrentPassportExpiryDate()));
                            passportTransactionHistoryResponse.setPassportType(embassyPassportTrx.getPassportType());
                            passportTransactionHistoryResponse.setPassportSubType(embassyPassportTrx.getPassportSubType());
                            passportTransactionHistoryResponse.setEmbassyPassportFee(embassyPassportTrx.getEmbassyPassportFee());
                            passportTransactionHistoryResponse.setPocketServiceCharge(embassyPassportTrx.getPocketServiceCharge());
                            passportTransactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTrx.getTotalPassportFeeAmount());

                            //UM_EMB_PASSPORT_SUB_TYPE
                            if(umEmbPassportSubType != null){
                                passportTransactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery()); //
                                passportTransactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery()); //
                            }

                        }

                        //TS_TRANSACTION
                        passportTransactionHistoryResponse.setTransactionId(tsTrx.getId());
                        passportTransactionHistoryResponse.setTransactionToken(tsTrx.getToken());
                        passportTransactionHistoryResponse.setTransactionSenderWallet(tsTrx.getSenderWallet());
                        passportTransactionHistoryResponse.setTransactionReceiverWallet(tsTrx.getReceiverWallet()); //
                        passportTransactionHistoryResponse.setTransactionType(tsTrx.getTransactionType()); //
                        passportTransactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTrx.getCreatedDate()));


                        passportTransactionHistoryResponseList.add(passportTransactionHistoryResponse);
                    }

                }

                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.OK);
                baseResponseObject.setData(passportTransactionHistoryResponseList);
                return baseResponseObject;

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "EmbPassportTransactionHistory list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "EmbPassportTransactionHistory list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }


    // Web portal
    @Override
    public BaseResponseObject trxHistoryReportList(EmptyRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<TsTransaction> tsTransactionList = new ArrayList<>();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                embassyPassportTransactionList = (List<EmbassyPassportTransaction>) embassyPassportTransactionRepository.findAll();

                if (embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0) {

                    for (EmbassyPassportTransaction embassyPassportTrx : embassyPassportTransactionList) {
                        EmbPassportTransactionHistoryResponse passportTrxHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //UM_EMB_PASSPORT_TRANSACTION_INFO
                        passportTrxHistoryResponse.setId(embassyPassportTrx.getId());
                        passportTrxHistoryResponse.setSenderName(embassyPassportTrx.getName());
                        passportTrxHistoryResponse.setSenderFatherName(embassyPassportTrx.getFatherName());
                        passportTrxHistoryResponse.setSenderBdMobileNo(embassyPassportTrx.getBdMobileNo());
                        passportTrxHistoryResponse.setUniquePassportGeneratedId(embassyPassportTrx.getUniquePassportGeneratedId());
                        passportTrxHistoryResponse.setMrpPassportNumber(embassyPassportTrx.getMrpPassportNumber());
                        passportTrxHistoryResponse.setEmirateOrResidentialId(embassyPassportTrx.getEmirateResidentialId());
                        passportTrxHistoryResponse.setProfessionOrSkill(embassyPassportTrx.getProfessionOrSkill());
                        passportTrxHistoryResponse.setPayMode(embassyPassportTrx.getPayMode());
                        passportTrxHistoryResponse.setGender(embassyPassportTrx.getGender());
                        passportTrxHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTrx.getDateOfBirth()));
                        passportTrxHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTrx.getCurrentPassportExpiryDate()));
                        passportTrxHistoryResponse.setPassportType(embassyPassportTrx.getPassportType());
                        passportTrxHistoryResponse.setPassportSubType(embassyPassportTrx.getPassportSubType());
                        passportTrxHistoryResponse.setEmbassyPassportFee(embassyPassportTrx.getEmbassyPassportFee());
                        passportTrxHistoryResponse.setPocketServiceCharge(embassyPassportTrx.getPocketServiceCharge());
                        passportTrxHistoryResponse.setTotalPassportFeeAmount(embassyPassportTrx.getTotalPassportFeeAmount());

                        //UM_EMB_PASSPORT_SUB_TYPE
                        umEmbPassportSubType = embassyPassportSubTypeRepository.
                                findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTrx.getPassportSubType(), String.valueOf(1));
                        if(umEmbPassportSubType != null){
                            passportTrxHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                            passportTrxHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());
                        }

                        //TS_TRANSACTION
                        tsTransactionList = tsTransactionRepository.findAllByTokenAndTransactionType(embassyPassportTrx.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                        if(tsTransactionList != null && tsTransactionList.size() > 0){
                            for(TsTransaction tsTrx : tsTransactionList){
                                passportTrxHistoryResponse.setTransactionId(tsTrx.getId());
                                passportTrxHistoryResponse.setTransactionToken(tsTrx.getToken());
                                passportTrxHistoryResponse.setTransactionSenderWallet(tsTrx.getSenderWallet());
                                passportTrxHistoryResponse.setTransactionReceiverWallet(tsTrx.getReceiverWallet()); //
                                passportTrxHistoryResponse.setTransactionType(tsTrx.getTransactionType()); //
                                passportTrxHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTrx.getCreatedDate()));
                            }
                        }

                        passportTransactionHistoryResponseList.add(passportTrxHistoryResponse);

                    }

                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(passportTransactionHistoryResponseList);
                    return baseResponseObject;

                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "EmbPassportTransactionHistory list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "EmbPassportTransactionHistory list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
        return baseResponseObject;
    }


    @Override
    public BaseResponseObject searchTrxHistoryReportByDateRange(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<TsTransaction> tsTransactionList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        EmbassyPassportTransaction embassyPassportTransaction = null;
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;

        checkCreatedDate(request, request.getFromDate(), request.getToDate());
        try {
            if (request != null) {

                tsTransactionList = tsTransactionRepository.findByCreatedDateBetweenAndTransactionType(
                        DateUtil.toDateAndFromDate(request.getFromDate()),
                        DateUtil.toDateAndFromDate(request.getToDate()),
                        PocketConstants.PASSPORT_TRANSACTION_TYPE);

                if(tsTransactionList != null && tsTransactionList.size() > 0){
                    for(TsTransaction trx : tsTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        embassyPassportTransaction = new EmbassyPassportTransaction();

                        embassyPassportTransaction = embassyPassportTransactionRepository.findByTsTransactionIdAndTsTransactionTokenAndStatus(
                                                    String.valueOf(trx.getId()),
                                                    trx.getToken(),
                                                    PocketConstants.PASSPORT_STATUS);

                        if(embassyPassportTransaction != null){
                            umEmbPassportSubType =embassyPassportSubTypeRepository.findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(
                                    embassyPassportTransaction.getPassportSubType(),
                                    PocketConstants.PASSPORT_STATUS);

                            if(umEmbPassportSubType != null){
                                // UM_EMB_PASSPORT_TRANSACTION_INFO
                                transactionHistoryResponse.setId(trx.getId());
                                transactionHistoryResponse.setSenderName(embassyPassportTransaction.getName());
                                transactionHistoryResponse.setSenderFatherName(embassyPassportTransaction.getFatherName());
                                transactionHistoryResponse.setSenderBdMobileNo(embassyPassportTransaction.getBdMobileNo());
                                transactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTransaction.getUniquePassportGeneratedId());
                                transactionHistoryResponse.setMrpPassportNumber(embassyPassportTransaction.getMrpPassportNumber());
                                transactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTransaction.getEmirateResidentialId());
                                transactionHistoryResponse.setProfessionOrSkill(embassyPassportTransaction.getProfessionOrSkill());
                                transactionHistoryResponse.setPayMode(embassyPassportTransaction.getPayMode());
                                transactionHistoryResponse.setGender(embassyPassportTransaction.getGender());
                                transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTransaction.getDateOfBirth()));
                                transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTransaction.getCurrentPassportExpiryDate()));
                                transactionHistoryResponse.setPassportType(embassyPassportTransaction.getPassportType());
                                transactionHistoryResponse.setPassportSubType(embassyPassportTransaction.getPassportSubType());
                                transactionHistoryResponse.setEmbassyPassportFee(embassyPassportTransaction.getEmbassyPassportFee());
                                transactionHistoryResponse.setPocketServiceCharge(embassyPassportTransaction.getPocketServiceCharge());
                                transactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTransaction.getTotalPassportFeeAmount());
                                //TS_TRANSACTION
                                transactionHistoryResponse.setTransactionId(trx.getId());
                                transactionHistoryResponse.setTransactionToken(trx.getToken());
                                transactionHistoryResponse.setTransactionSenderWallet(trx.getSenderWallet());
                                transactionHistoryResponse.setTransactionReceiverWallet(trx.getReceiverWallet());
                                transactionHistoryResponse.setTransactionType(trx.getTransactionType());
                                transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(trx.getCreatedDate()));
                                //UM_EMB_PASSPORT_SUB_TYPE
                                transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                                transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());
                            }

                        }
                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;
                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Date wise searching list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Date wise searching list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Date wise searching list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByToken(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkTransactionToken(request, request.getTsTransactionToken());
        try {
            if (request != null) {
                embassyPassportTransaction = embassyPassportTransactionRepository.findByTsTransactionTokenIgnoreCaseAndStatus(request.getTsTransactionToken(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransaction != null){
                    tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embassyPassportTransaction.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                    umEmbPassportSubType =embassyPassportSubTypeRepository.
                            findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTransaction.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                    if(tsTransaction != null && umEmbPassportSubType != null){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        // UM_EMB_PASSPORT_TRANSACTION_INFO
                        transactionHistoryResponse.setId(embassyPassportTransaction.getId());
                        transactionHistoryResponse.setSenderName(embassyPassportTransaction.getName());
                        transactionHistoryResponse.setSenderFatherName(embassyPassportTransaction.getFatherName());
                        transactionHistoryResponse.setSenderBdMobileNo(embassyPassportTransaction.getBdMobileNo());
                        transactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTransaction.getUniquePassportGeneratedId());
                        transactionHistoryResponse.setMrpPassportNumber(embassyPassportTransaction.getMrpPassportNumber());
                        transactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTransaction.getEmirateResidentialId());
                        transactionHistoryResponse.setProfessionOrSkill(embassyPassportTransaction.getProfessionOrSkill());
                        transactionHistoryResponse.setPayMode(embassyPassportTransaction.getPayMode());
                        transactionHistoryResponse.setGender(embassyPassportTransaction.getGender());
                        transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTransaction.getDateOfBirth()));
                        transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTransaction.getCurrentPassportExpiryDate()));
                        transactionHistoryResponse.setPassportType(embassyPassportTransaction.getPassportType());
                        transactionHistoryResponse.setPassportSubType(embassyPassportTransaction.getPassportSubType());
                        transactionHistoryResponse.setEmbassyPassportFee(embassyPassportTransaction.getEmbassyPassportFee());
                        transactionHistoryResponse.setPocketServiceCharge(embassyPassportTransaction.getPocketServiceCharge());
                        transactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTransaction.getTotalPassportFeeAmount());
                        //TS_TRANSACTION
                        transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                        transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                        transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                        transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                        transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                        transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                        //UM_EMB_PASSPORT_SUB_TYPE
                        transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                        transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);

                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.OK);
                        responseObject.setData(passportTransactionHistoryResponseList);
                        return responseObject;
                    }else {
                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                                "TsTransaction and passport_sub_type object return null. Reason : "));
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.ERROR);
                        responseObject.setData(null);
                        return responseObject;
                    }

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Transaction object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Token wise searching list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Token wise searching list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByQrCodeId(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkUniquePassportId(request,request.getUniquePassportGeneratedId());
        try {
            if (request != null) {
                embassyPassportTransaction = embassyPassportTransactionRepository.findByUniquePassportGeneratedIdIgnoreCaseAndStatus(request.getUniquePassportGeneratedId(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransaction != null){
                    tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embassyPassportTransaction.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                    umEmbPassportSubType =embassyPassportSubTypeRepository.
                            findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTransaction.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                    if(tsTransaction != null && umEmbPassportSubType != null){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        // UM_EMB_PASSPORT_TRANSACTION_INFO
                        transactionHistoryResponse.setId(embassyPassportTransaction.getId());
                        transactionHistoryResponse.setSenderName(embassyPassportTransaction.getName());
                        transactionHistoryResponse.setSenderFatherName(embassyPassportTransaction.getFatherName());
                        transactionHistoryResponse.setSenderBdMobileNo(embassyPassportTransaction.getBdMobileNo());
                        transactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTransaction.getUniquePassportGeneratedId());
                        transactionHistoryResponse.setMrpPassportNumber(embassyPassportTransaction.getMrpPassportNumber());
                        transactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTransaction.getEmirateResidentialId());
                        transactionHistoryResponse.setProfessionOrSkill(embassyPassportTransaction.getProfessionOrSkill());
                        transactionHistoryResponse.setPayMode(embassyPassportTransaction.getPayMode());
                        transactionHistoryResponse.setGender(embassyPassportTransaction.getGender());
                        transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTransaction.getDateOfBirth()));
                        transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTransaction.getCurrentPassportExpiryDate()));
                        transactionHistoryResponse.setPassportType(embassyPassportTransaction.getPassportType());
                        transactionHistoryResponse.setPassportSubType(embassyPassportTransaction.getPassportSubType());
                        transactionHistoryResponse.setEmbassyPassportFee(embassyPassportTransaction.getEmbassyPassportFee());
                        transactionHistoryResponse.setPocketServiceCharge(embassyPassportTransaction.getPocketServiceCharge());
                        transactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTransaction.getTotalPassportFeeAmount());
                        //TS_TRANSACTION
                        transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                        transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                        transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                        transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                        transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                        transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                        //UM_EMB_PASSPORT_SUB_TYPE
                        transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                        transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);

                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.OK);
                        responseObject.setData(passportTransactionHistoryResponseList);
                        return responseObject;
                    }else {
                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                                "TsTransaction and passport_sub_type object return null. Reason : "));
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.ERROR);
                        responseObject.setData(null);
                        return responseObject;
                    }

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Transaction object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "QrCode wise searching list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "QrCode wise searching list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByPType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkPassportType(request, request.getPassportType());
        try {
            if (request != null) {
                embassyPassportTransactionList = embassyPassportTransactionRepository.findByPassportTypeIgnoreCaseAndStatus(request.getPassportType(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){

                    for(EmbassyPassportTransaction embPassTrx : embassyPassportTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embPassTrx.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                        umEmbPassportSubType =embassyPassportSubTypeRepository.
                                findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embPassTrx.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                        if(tsTransaction != null && umEmbPassportSubType != null){
                            // UM_EMB_PASSPORT_TRANSACTION_INFO
                            transactionHistoryResponse.setId(embPassTrx.getId());
                            transactionHistoryResponse.setSenderName(embPassTrx.getName());
                            transactionHistoryResponse.setSenderFatherName(embPassTrx.getFatherName());
                            transactionHistoryResponse.setSenderBdMobileNo(embPassTrx.getBdMobileNo());
                            transactionHistoryResponse.setUniquePassportGeneratedId(embPassTrx.getUniquePassportGeneratedId());
                            transactionHistoryResponse.setMrpPassportNumber(embPassTrx.getMrpPassportNumber());
                            transactionHistoryResponse.setEmirateOrResidentialId(embPassTrx.getEmirateResidentialId());
                            transactionHistoryResponse.setProfessionOrSkill(embPassTrx.getProfessionOrSkill());
                            transactionHistoryResponse.setPayMode(embPassTrx.getPayMode());
                            transactionHistoryResponse.setGender(embPassTrx.getGender());
                            transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embPassTrx.getDateOfBirth()));
                            transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embPassTrx.getCurrentPassportExpiryDate()));
                            transactionHistoryResponse.setPassportType(embPassTrx.getPassportType());
                            transactionHistoryResponse.setPassportSubType(embPassTrx.getPassportSubType());
                            transactionHistoryResponse.setEmbassyPassportFee(embPassTrx.getEmbassyPassportFee());
                            transactionHistoryResponse.setPocketServiceCharge(embPassTrx.getPocketServiceCharge());
                            transactionHistoryResponse.setTotalPassportFeeAmount(embPassTrx.getTotalPassportFeeAmount());
                            //TS_TRANSACTION
                            transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                            transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                            transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                            transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                            transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                            transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                            //UM_EMB_PASSPORT_SUB_TYPE
                            transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                            transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                        }
                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;
                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "TsTransaction and passport_sub_type object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Passport Type wise searching list is empty. Reason : "));  //"+baseResponseObject.getError().getErrorMessage()
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport Type wise searching list is empty. Reason : ")); //"+baseResponseObject.getError().getErrorMessage()
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportBySubType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkPassportSubType(request,request.getPassportSubType());
        try {
            if (request != null) {
                embassyPassportTransactionList = embassyPassportTransactionRepository.findAllByPassportSubTypeIgnoreCaseAndStatus(request.getPassportSubType(), PocketConstants.PASSPORT_STATUS);
                if(embassyPassportTransactionList.size() > 0){
                    for(EmbassyPassportTransaction  embassyPassportTransaction: embassyPassportTransactionList){
                        tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embassyPassportTransaction.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                        umEmbPassportSubType =embassyPassportSubTypeRepository.
                                findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTransaction.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                        if(tsTransaction != null && umEmbPassportSubType != null) {
                            transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                            // UM_EMB_PASSPORT_TRANSACTION_INFO
                            transactionHistoryResponse.setId(embassyPassportTransaction.getId());
                            transactionHistoryResponse.setSenderName(embassyPassportTransaction.getName());
                            transactionHistoryResponse.setSenderFatherName(embassyPassportTransaction.getFatherName());
                            transactionHistoryResponse.setSenderBdMobileNo(embassyPassportTransaction.getBdMobileNo());
                            transactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTransaction.getUniquePassportGeneratedId());
                            transactionHistoryResponse.setMrpPassportNumber(embassyPassportTransaction.getMrpPassportNumber());
                            transactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTransaction.getEmirateResidentialId());
                            transactionHistoryResponse.setProfessionOrSkill(embassyPassportTransaction.getProfessionOrSkill());
                            transactionHistoryResponse.setPayMode(embassyPassportTransaction.getPayMode());
                            transactionHistoryResponse.setGender(embassyPassportTransaction.getGender());
                            transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTransaction.getDateOfBirth()));
                            transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTransaction.getCurrentPassportExpiryDate()));
                            transactionHistoryResponse.setPassportType(embassyPassportTransaction.getPassportType());
                            transactionHistoryResponse.setPassportSubType(embassyPassportTransaction.getPassportSubType());
                            transactionHistoryResponse.setEmbassyPassportFee(embassyPassportTransaction.getEmbassyPassportFee());
                            transactionHistoryResponse.setPocketServiceCharge(embassyPassportTransaction.getPocketServiceCharge());
                            transactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTransaction.getTotalPassportFeeAmount());
                            //TS_TRANSACTION
                            transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                            transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                            transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                            transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                            transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                            transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                            //UM_EMB_PASSPORT_SUB_TYPE
                            transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                            transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                            passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                        }

                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;
                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Transaction object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Passport Sub Type wise searching list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport Sub Type wise searching list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByPassportId(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkMrpPassportNumber(request,request.getMrpPassportNumber());
        try {
            if (request != null) {
                embassyPassportTransaction = embassyPassportTransactionRepository.findByMrpPassportNumberIgnoreCaseAndStatus(request.getMrpPassportNumber(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransaction != null){
                    tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embassyPassportTransaction.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                    umEmbPassportSubType =embassyPassportSubTypeRepository.
                            findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTransaction.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                    if(tsTransaction != null && umEmbPassportSubType != null){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        // UM_EMB_PASSPORT_TRANSACTION_INFO
                        transactionHistoryResponse.setId(embassyPassportTransaction.getId());
                        transactionHistoryResponse.setSenderName(embassyPassportTransaction.getName());
                        transactionHistoryResponse.setSenderFatherName(embassyPassportTransaction.getFatherName());
                        transactionHistoryResponse.setSenderBdMobileNo(embassyPassportTransaction.getBdMobileNo());
                        transactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTransaction.getUniquePassportGeneratedId());
                        transactionHistoryResponse.setMrpPassportNumber(embassyPassportTransaction.getMrpPassportNumber());
                        transactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTransaction.getEmirateResidentialId());
                        transactionHistoryResponse.setProfessionOrSkill(embassyPassportTransaction.getProfessionOrSkill());
                        transactionHistoryResponse.setPayMode(embassyPassportTransaction.getPayMode());
                        transactionHistoryResponse.setGender(embassyPassportTransaction.getGender());
                        transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTransaction.getDateOfBirth()));
                        transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTransaction.getCurrentPassportExpiryDate()));
                        transactionHistoryResponse.setPassportType(embassyPassportTransaction.getPassportType());
                        transactionHistoryResponse.setPassportSubType(embassyPassportTransaction.getPassportSubType());
                        transactionHistoryResponse.setEmbassyPassportFee(embassyPassportTransaction.getEmbassyPassportFee());
                        transactionHistoryResponse.setPocketServiceCharge(embassyPassportTransaction.getPocketServiceCharge());
                        transactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTransaction.getTotalPassportFeeAmount());
                        //TS_TRANSACTION
                        transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                        transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                        transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                        transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                        transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                        transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                        //UM_EMB_PASSPORT_SUB_TYPE
                        transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                        transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);

                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.OK);
                        responseObject.setData(passportTransactionHistoryResponseList);
                        return responseObject;
                    }else {
                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                                "TsTransaction and passport_sub_type object return null. Reason : "));
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.ERROR);
                        responseObject.setData(null);
                        return responseObject;
                    }

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Transaction object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "PassportId wise searching list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "PassportId wise searching list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByPayMode(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkPayMode(request,request.getPayMode());
        try {
            if (request != null) {
                embassyPassportTransactionList = embassyPassportTransactionRepository.findByPayModeIgnoreCaseAndStatus(request.getPayMode(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){

                    for(EmbassyPassportTransaction embPassTrx : embassyPassportTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embPassTrx.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                        umEmbPassportSubType =embassyPassportSubTypeRepository.
                                findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embPassTrx.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                        if(tsTransaction != null && umEmbPassportSubType != null){
                            // UM_EMB_PASSPORT_TRANSACTION_INFO
                            transactionHistoryResponse.setId(embPassTrx.getId());
                            transactionHistoryResponse.setSenderName(embPassTrx.getName());
                            transactionHistoryResponse.setSenderFatherName(embPassTrx.getFatherName());
                            transactionHistoryResponse.setSenderBdMobileNo(embPassTrx.getBdMobileNo());
                            transactionHistoryResponse.setUniquePassportGeneratedId(embPassTrx.getUniquePassportGeneratedId());
                            transactionHistoryResponse.setMrpPassportNumber(embPassTrx.getMrpPassportNumber());
                            transactionHistoryResponse.setEmirateOrResidentialId(embPassTrx.getEmirateResidentialId());
                            transactionHistoryResponse.setProfessionOrSkill(embPassTrx.getProfessionOrSkill());
                            transactionHistoryResponse.setPayMode(embPassTrx.getPayMode());
                            transactionHistoryResponse.setGender(embPassTrx.getGender());
                            transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embPassTrx.getDateOfBirth()));
                            transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embPassTrx.getCurrentPassportExpiryDate()));
                            transactionHistoryResponse.setPassportType(embPassTrx.getPassportType());
                            transactionHistoryResponse.setPassportSubType(embPassTrx.getPassportSubType());
                            transactionHistoryResponse.setEmbassyPassportFee(embPassTrx.getEmbassyPassportFee());
                            transactionHistoryResponse.setPocketServiceCharge(embPassTrx.getPocketServiceCharge());
                            transactionHistoryResponse.setTotalPassportFeeAmount(embPassTrx.getTotalPassportFeeAmount());
                            //TS_TRANSACTION
                            transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                            transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                            transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                            transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                            transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                            transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                            //UM_EMB_PASSPORT_SUB_TYPE
                            transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                            transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                            passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                        }
                    }
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;
                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "TsTransaction and passport_sub_type object return null. Reason : "));  //"+baseResponseObject.getError().getErrorMessage()
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "PayMode wise searching list is empty. Reason : "));  //"+baseResponseObject.getError().getErrorMessage()
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "PayMode wise searching list is empty. Reason : ")); //"+baseResponseObject.getError().getErrorMessage()
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByWalletNo(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkSenderWallet(request, request.getUaeMobileNo());
        try {
            if (request != null) {
                embassyPassportTransactionList = embassyPassportTransactionRepository.findByTsTransactionSenderAndStatus(request.getUaeMobileNo(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){

                    for(EmbassyPassportTransaction embPassTrx : embassyPassportTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embPassTrx.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                        umEmbPassportSubType =embassyPassportSubTypeRepository.
                                findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embPassTrx.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                        if(tsTransaction != null && umEmbPassportSubType != null){
                            // UM_EMB_PASSPORT_TRANSACTION_INFO
                            transactionHistoryResponse.setId(embPassTrx.getId());
                            transactionHistoryResponse.setSenderName(embPassTrx.getName());
                            transactionHistoryResponse.setSenderFatherName(embPassTrx.getFatherName());
                            transactionHistoryResponse.setSenderBdMobileNo(embPassTrx.getBdMobileNo());
                            transactionHistoryResponse.setUniquePassportGeneratedId(embPassTrx.getUniquePassportGeneratedId());
                            transactionHistoryResponse.setMrpPassportNumber(embPassTrx.getMrpPassportNumber());
                            transactionHistoryResponse.setEmirateOrResidentialId(embPassTrx.getEmirateResidentialId());
                            transactionHistoryResponse.setProfessionOrSkill(embPassTrx.getProfessionOrSkill());
                            transactionHistoryResponse.setPayMode(embPassTrx.getPayMode());
                            transactionHistoryResponse.setGender(embPassTrx.getGender());
                            transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embPassTrx.getDateOfBirth()));
                            transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embPassTrx.getCurrentPassportExpiryDate()));
                            transactionHistoryResponse.setPassportType(embPassTrx.getPassportType());
                            transactionHistoryResponse.setPassportSubType(embPassTrx.getPassportSubType());
                            transactionHistoryResponse.setEmbassyPassportFee(embPassTrx.getEmbassyPassportFee());
                            transactionHistoryResponse.setPocketServiceCharge(embPassTrx.getPocketServiceCharge());
                            transactionHistoryResponse.setTotalPassportFeeAmount(embPassTrx.getTotalPassportFeeAmount());
                            //TS_TRANSACTION
                            transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                            transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                            transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                            transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                            transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                            transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                            //UM_EMB_PASSPORT_SUB_TYPE
                            transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                            transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                            passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                        }
                    }
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;
                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "TsTransaction and passport_sub_type object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Wallet wise searching list is empty. Reason : "));  //"+baseResponseObject.getError().getErrorMessage()
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Wallet wise searching list is empty. Reason : ")); //"+baseResponseObject.getError().getErrorMessage()
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject searchTrxHistoryReportByEmirateId(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        EmbassyPassportTransaction embassyPassportTransaction = new EmbassyPassportTransaction();
        TsTransaction tsTransaction = new TsTransaction();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        UmEmbPassportSubType umEmbPassportSubType = new UmEmbPassportSubType();

        checkEmirateId(request, request.getEmirateOrResidentialId());
        try {
            if (request != null) {
                embassyPassportTransaction = embassyPassportTransactionRepository.findByEmirateResidentialIdIgnoreCaseAndStatus(request.getEmirateOrResidentialId(), PocketConstants.PASSPORT_STATUS);

                if(embassyPassportTransaction != null){
                    tsTransaction = tsTransactionRepository.findByTokenAndTransactionTypeIgnoreCase(embassyPassportTransaction.getTsTransactionToken(), PocketConstants.PASSPORT_TRANSACTION_TYPE);
                    umEmbPassportSubType =embassyPassportSubTypeRepository.
                            findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(embassyPassportTransaction.getPassportSubType(), PocketConstants.PASSPORT_STATUS);

                    if(tsTransaction != null && umEmbPassportSubType != null){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        // UM_EMB_PASSPORT_TRANSACTION_INFO
                        transactionHistoryResponse.setId(embassyPassportTransaction.getId());
                        transactionHistoryResponse.setSenderName(embassyPassportTransaction.getName());
                        transactionHistoryResponse.setSenderFatherName(embassyPassportTransaction.getFatherName());
                        transactionHistoryResponse.setSenderBdMobileNo(embassyPassportTransaction.getBdMobileNo());
                        transactionHistoryResponse.setUniquePassportGeneratedId(embassyPassportTransaction.getUniquePassportGeneratedId());
                        transactionHistoryResponse.setMrpPassportNumber(embassyPassportTransaction.getMrpPassportNumber());
                        transactionHistoryResponse.setEmirateOrResidentialId(embassyPassportTransaction.getEmirateResidentialId());
                        transactionHistoryResponse.setProfessionOrSkill(embassyPassportTransaction.getProfessionOrSkill());
                        transactionHistoryResponse.setPayMode(embassyPassportTransaction.getPayMode());
                        transactionHistoryResponse.setGender(embassyPassportTransaction.getGender());
                        transactionHistoryResponse.setDateOfBirth(DateUtil.currentDateTime(embassyPassportTransaction.getDateOfBirth()));
                        transactionHistoryResponse.setCurrentPassportExpiryDate(DateUtil.currentDateTime(embassyPassportTransaction.getCurrentPassportExpiryDate()));
                        transactionHistoryResponse.setPassportType(embassyPassportTransaction.getPassportType());
                        transactionHistoryResponse.setPassportSubType(embassyPassportTransaction.getPassportSubType());
                        transactionHistoryResponse.setEmbassyPassportFee(embassyPassportTransaction.getEmbassyPassportFee());
                        transactionHistoryResponse.setPocketServiceCharge(embassyPassportTransaction.getPocketServiceCharge());
                        transactionHistoryResponse.setTotalPassportFeeAmount(embassyPassportTransaction.getTotalPassportFeeAmount());
                        //TS_TRANSACTION
                        transactionHistoryResponse.setTransactionId(tsTransaction.getId());
                        transactionHistoryResponse.setTransactionToken(tsTransaction.getToken());
                        transactionHistoryResponse.setTransactionSenderWallet(tsTransaction.getSenderWallet());
                        transactionHistoryResponse.setTransactionReceiverWallet(tsTransaction.getReceiverWallet());
                        transactionHistoryResponse.setTransactionType(tsTransaction.getTransactionType());
                        transactionHistoryResponse.setCreatedDate(DateUtil.currentDateTime(tsTransaction.getCreatedDate()));
                        //UM_EMB_PASSPORT_SUB_TYPE
                        transactionHistoryResponse.setGeneralDelivery(umEmbPassportSubType.getGeneralDelivery());
                        transactionHistoryResponse.setEmergencyDelivery(umEmbPassportSubType.getEmergencyDelivery());

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);

                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.OK);
                        responseObject.setData(passportTransactionHistoryResponseList);
                        return responseObject;
                    }else {
                        BaseResponseObject responseObject = new BaseResponseObject();
                        responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                                "TsTransaction and passport_sub_type object return null. Reason : "));
                        responseObject.setRequestId(request.getRequestId());
                        responseObject.setStatus(PocketConstants.ERROR);
                        responseObject.setData(null);
                        return responseObject;
                    }

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Transaction object return null. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "EmirateId wise searching list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "EmirateId wise searching list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllTokenByTransactionType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<TsTransaction> tsTransactionList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                tsTransactionList = tsTransactionRepository.findByTransactionType(PocketConstants.PASSPORT_TRANSACTION_TYPE);

                if(tsTransactionList != null && tsTransactionList.size() > 0){
                    for (TsTransaction trx: tsTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //TS_TRANSACTION
                        if(trx.getToken() != null){
                            transactionHistoryResponse.setTransactionToken(trx.getToken());
                        }

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Token list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Token list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Token list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllUniqueQrCode(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<TsTransaction> tsTransactionList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                tsTransactionList = tsTransactionRepository.findByTransactionType(PocketConstants.PASSPORT_TRANSACTION_TYPE);

                if(tsTransactionList != null && tsTransactionList.size() > 0){
                    for (TsTransaction trx: tsTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //TS_TRANSACTION
                        if(trx.getRefTransactionToken() != null){
                            transactionHistoryResponse.setUniquePassportGeneratedId(trx.getRefTransactionToken());
                        }

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Generated QRCode list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Generated QRCode list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Generated QRCode list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllPassportType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<UmEmbPassportType> passportTypeList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                passportTypeList = (List<UmEmbPassportType>) embassyPassportRepository.findAll();

                if(passportTypeList != null && passportTypeList.size() > 0){
                    for (UmEmbPassportType type : passportTypeList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //UM_EMB_PASSPORT_TYPE
                        if(type.getPassportType() != null){
                            transactionHistoryResponse.setPassportType(type.getPassportType());
                        }

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Type list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Passport Type list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport Type list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllPassportSubType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<UmEmbPassportSubType> umEmbPassportSubTypList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                umEmbPassportSubTypList = (List<UmEmbPassportSubType>) embassyPassportSubTypeRepository.findAll();

                if(umEmbPassportSubTypList != null && umEmbPassportSubTypList.size() > 0){
                    for (UmEmbPassportSubType subType : umEmbPassportSubTypList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //UM_EMB_PASSPORT_SUB_TYPE
                        if(subType.getPassportSubType() != null){
                            transactionHistoryResponse.setPassportSubType(subType.getPassportSubType());
                        }

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Sub Type list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Passport Sub Type list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport Sub Type list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllMrpPassport(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                embassyPassportTransactionList = (List<EmbassyPassportTransaction>) embassyPassportTransactionRepository.findAll();

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){
                    for (EmbassyPassportTransaction passportTrx : embassyPassportTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //UM_EMB_PASSPORT_TRANSACTION_INFO
                        if(passportTrx.getMrpPassportNumber() != null){
                            transactionHistoryResponse.setMrpPassportNumber(passportTrx.getMrpPassportNumber());
                        }

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Mrp Passport list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Mrp Passport list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Mrp Passport list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllPayMode(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        List<String> passportStringListTemp = new ArrayList<>();
        List<String> passportStringList = new ArrayList<>();


        try {
            if (request != null) {
                embassyPassportTransactionList = (List<EmbassyPassportTransaction>) embassyPassportTransactionRepository.findAll();

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){
                    for (EmbassyPassportTransaction passportTrx : embassyPassportTransactionList){

                        if(passportTrx.getPayMode() != null && !passportTrx.getPayMode().isEmpty() && !passportTrx.getPayMode().equals("string")){
                            passportStringListTemp.add(passportTrx.getPayMode());
                        }

                    }

                    //Duplicate control
                    passportStringList = passportStringListTemp.stream().distinct().collect(Collectors.toList());
                    for(String s : passportStringList){
                        //UM_EMB_PASSPORT_TRANSACTION_INFO
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        transactionHistoryResponse.setPayMode(s);
                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Passport Pay Mode list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Passport Pay Mode list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Passport Pay Mode list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllSenderWallet(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();
        List<String> walletListTemp = new ArrayList<>();
        List<String> walletList = new ArrayList<>();

        try {
            if (request != null) {
                embassyPassportTransactionList = (List<EmbassyPassportTransaction>) embassyPassportTransactionRepository.findAll();

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){
                    for (EmbassyPassportTransaction passportTrx : embassyPassportTransactionList){

                        if(passportTrx.getTsTransactionSender() != null && !passportTrx.getTsTransactionSender().isEmpty()
                                && !passportTrx.getTsTransactionSender().equals("string")){
                            walletListTemp.add(passportTrx.getTsTransactionSender());
                        }

                    }

                    //Duplicate control
                    walletList = walletListTemp.stream().distinct().collect(Collectors.toList());
                    for(String wallet : walletList){
                        //UM_EMB_PASSPORT_TRANSACTION_INFO
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();
                        transactionHistoryResponse.setTransactionSenderWallet(wallet);

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "Sender wallet list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "Sender wallet list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "Sender wallet list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject loadAllEmirateID(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        List<EmbassyPassportTransaction> embassyPassportTransactionList = new ArrayList<>();
        EmbPassportTransactionHistoryResponse transactionHistoryResponse = null;
        List<EmbPassportTransactionHistoryResponse> passportTransactionHistoryResponseList = new ArrayList<>();

        try {
            if (request != null) {
                embassyPassportTransactionList = (List<EmbassyPassportTransaction>) embassyPassportTransactionRepository.findAll();

                if(embassyPassportTransactionList != null && embassyPassportTransactionList.size() > 0){
                    for (EmbassyPassportTransaction passportTrx : embassyPassportTransactionList){
                        transactionHistoryResponse = new EmbPassportTransactionHistoryResponse();

                        //UM_EMB_PASSPORT_TRANSACTION_INFO
                        if(passportTrx.getEmirateResidentialId() != null){
                            transactionHistoryResponse.setEmirateOrResidentialId(passportTrx.getEmirateResidentialId());
                        }

                        passportTransactionHistoryResponseList.add(transactionHistoryResponse);
                    }

                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.OK);
                    responseObject.setData(passportTransactionHistoryResponseList);
                    return responseObject;

                }else {
                    BaseResponseObject responseObject = new BaseResponseObject();
                    responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                            "User EmirateId list is empty. Reason : "));
                    responseObject.setRequestId(request.getRequestId());
                    responseObject.setStatus(PocketConstants.ERROR);
                    responseObject.setData(null);
                    return responseObject;
                }

            } else {
                BaseResponseObject responseObject = new BaseResponseObject();
                responseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                        "User EmirateId list is empty. Reason : "));
                responseObject.setRequestId(request.getRequestId());
                responseObject.setStatus(PocketConstants.ERROR);
                responseObject.setData(null);
                return responseObject;
            }
        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(), "" + e.getMessage());
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeListEmpty.getKeyString(),
                    "User EmirateId list is empty. Reason : "));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }
    }


    public void checkCreatedDate(EmbPassportTrxReportRequest request, Date fromDate, Date toDate) throws PocketException {
        if(fromDate == null && toDate == null){
            logWriterUtility.error(request.getRequestId(), "Passport transaction date not found.");
            throw new PocketException(PocketErrorCode.PassportTransactionCreatedDate);
        }
    }
    public void checkTransactionToken(EmbPassportTrxReportRequest request,String tsTransactionToken) throws PocketException {
        if(tsTransactionToken == null || tsTransactionToken.equals("string") || tsTransactionToken.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Passport transaction token not found.");
            throw new PocketException(PocketErrorCode.PassportTransactionToken);
        }
    }
    public void checkUniquePassportId(EmbPassportTrxReportRequest request,String uniquePassportGeneratedId) throws PocketException {
        if(uniquePassportGeneratedId == null || uniquePassportGeneratedId.equals("string") || uniquePassportGeneratedId.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Unique passport Id not found.");
            throw new PocketException(PocketErrorCode.UniquePassportGeneratedId);
        }
    }
    public void checkPassportType(EmbPassportTrxReportRequest request,String passportType) throws PocketException {
        if(passportType == null || passportType.equals("string") || passportType.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Passport type not found.");
            throw new PocketException(PocketErrorCode.PassPortType);
        }
    }
    public void checkPassportSubType(EmbPassportTrxReportRequest request,String passportSubType) throws PocketException {
        if(passportSubType == null || passportSubType.equals("string") || passportSubType.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Passport sub type not found.");
            throw new PocketException(PocketErrorCode.PassportSubType);
        }
    }
    public void checkMrpPassportNumber(EmbPassportTrxReportRequest request,String mrpPassportNumber) throws PocketException {
        if(mrpPassportNumber == null || mrpPassportNumber.equals("string") || mrpPassportNumber.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "MrpPassport number not found.");
            throw new PocketException(PocketErrorCode.MrpPassportNumber);
        }
    }
    public void checkPayMode(EmbPassportTrxReportRequest request,String payMode) throws PocketException {
        if(payMode == null || payMode.equals("string") || payMode.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Passport pay mode not found.");
            throw new PocketException(PocketErrorCode.PayMode);
        }
    }
    public void checkSenderWallet(EmbPassportTrxReportRequest request,String uaeMobileNo) throws PocketException {
        if(uaeMobileNo == null || uaeMobileNo.equals("string") || uaeMobileNo.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "Sender Wallet not found.");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }
    }
    public void checkEmirateId(EmbPassportTrxReportRequest request,String emirateId) throws PocketException {
        if(emirateId == null || emirateId.equals("string") || emirateId.isEmpty()){
            logWriterUtility.error(request.getRequestId(), "EmirateId not found.");
             throw new PocketException(PocketErrorCode.EmirateOrResidentialId);
        }
    }

}
