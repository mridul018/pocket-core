package tech.ipocket.pocket.services.um.card;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface CardServices {

    BaseResponseObject getWalletUsingCardNumber(String cardNumber,String requestId) throws PocketException;
    BaseResponseObject getWalletUsingCardUid(String cardUid,String requestId) throws PocketException;
}
