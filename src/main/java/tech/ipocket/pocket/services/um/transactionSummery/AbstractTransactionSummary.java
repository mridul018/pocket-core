package tech.ipocket.pocket.services.um.transactionSummery;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.um.TransactionSummaryViewRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.concurrent.ExecutionException;

public abstract class AbstractTransactionSummary {
    static LogWriterUtility logWriterUtility = new LogWriterUtility(AbstractTransactionSummary.class);


    TransactionSummaryService transactionSummaryService;

    public AbstractTransactionSummary(TransactionSummaryService transactionSummaryService) {
        this.transactionSummaryService = transactionSummaryService;
    }

    public abstract BaseResponseObject getTransactionSummary(TransactionSummaryViewRequest transactionSummaryViewRequest) throws PocketException, ExecutionException, InterruptedException;

    protected UmUserInfo checkIsValidSender(Integer credentialId, String requestID) throws PocketException {
        return transactionSummaryService.checkIsValidSender(credentialId, requestID);
    }

    protected ReceiverInfo checkIsValidReceiver(String receiverMobileNumber, String requestId) throws PocketException {
        return transactionSummaryService.checkIsValidReceiver(receiverMobileNumber, requestId, false);
    }

    protected ReceiverInfo checkIsValidReceiverBillpay(String receiverMobileNumber, String requestId) throws PocketException {
        return transactionSummaryService.checkIsValidReceiverBillpay(receiverMobileNumber, requestId, false);
    }

    protected void checkIsValidReceiverAndIsValidMpos(String receiverMobileNumber, String requestId) throws PocketException {
        transactionSummaryService.checkIsValidReceiver(receiverMobileNumber, requestId, false);
    }
    protected TransactionSummaryViewResponse callTxForTransactionSummary(TransactionSummaryViewRequest transactionSummaryRequest) throws PocketException {

        logWriterUtility.trace(transactionSummaryRequest.getRequestId(), "Calling Transaction Service for check Transaction Visibility");

        TransactionSummaryViewResponse transactionSummary= transactionSummaryService.callTxForTransactionSummary(transactionSummaryRequest);
        if (transactionSummary==null) {
            logWriterUtility.trace(transactionSummaryRequest.getRequestId(), "Returning because Transaction not possible for this accounts.");
            throw new PocketException(PocketErrorCode.TransactionNotPossibleForAccounts);
        }
        logWriterUtility.trace(transactionSummaryRequest.getRequestId(), "Transaction possible among these accounts.");

        return transactionSummary;
    }
}