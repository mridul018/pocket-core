package tech.ipocket.pocket.services.um.passport;


import tech.ipocket.pocket.request.um.passport.UmEmbPassportSubTypeEmptyRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportSubTypeRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface EmbassyPassportSubTypeService {

    BaseResponseObject createPassportSubType(UmEmbPassportSubTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject updatePassportSubType(UmEmbPassportSubTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject embassyPassportSubTypeListById(UmEmbPassportTypeRequest umEmbPassportTypeById, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject embassyPassportSubTypeList(UmEmbPassportSubTypeEmptyRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject searchPassportSubTypeAndStatus(UmEmbPassportSubTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;


}
