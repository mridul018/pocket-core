package tech.ipocket.pocket.services.um.otp;

import tech.ipocket.pocket.entity.UmOtp;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.um.LoginStepTwoRequest;
import tech.ipocket.pocket.request.um.ResendOtpRequest;
import tech.ipocket.pocket.request.um.SendOtpRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface OtpService {
    OtpObject generateOtp(UmUserInfo umUserInfo, String requestId);

    UmUserInfo verifyOtp(OtpObject request) throws PocketException;

    BaseResponseObject resendOtp(ResendOtpRequest request) throws PocketException;

    OtpObject generateOtp(Integer userCredentialId, UmUserInfo umUserCredential, String otpType, SendOtpRequest sendOtpRequest);
}
