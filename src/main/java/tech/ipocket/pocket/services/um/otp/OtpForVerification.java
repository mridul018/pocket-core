package tech.ipocket.pocket.services.um.otp;

import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.request.um.SendOtpRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.SendOtpResponse;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.UmEnums;

import java.util.concurrent.ExecutionException;

public class OtpForVerification extends AbstractOtp {

    public OtpForVerification(CommonOtpService commonOtpService, OtpService otpService) {
        super(commonOtpService, otpService);
    }

    @Override
    public BaseResponseObject sendOtp(SendOtpRequest sendOtpRequest, Integer userCredentialId) throws PocketException, ExecutionException, InterruptedException {

        validateData(sendOtpRequest);

        BaseResponseObject baseResponseObject = new BaseResponseObject();

        UmUserInfo senderInfo = checkIsValidSender(userCredentialId, sendOtpRequest.getRequestId());

        OtpObject umOtp= otpService.generateOtp(userCredentialId,senderInfo, UmEnums.OtpTypes.OtpForVerification.getOtpType(),sendOtpRequest);

        sendOtpNotification(sendOtpRequest.getMobileNumber(),null,senderInfo,
                umOtp.getOtpText(),sendOtpRequest.getRequestId());

        SendOtpResponse sendOtpResponse = new SendOtpResponse();
        sendOtpResponse.setReferenceId(umOtp.getOtpSecret());
        sendOtpResponse.setMobileNumber(sendOtpRequest.getMobileNumber());
        baseResponseObject.setData(sendOtpResponse);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(sendOtpRequest.getRequestId());

        return baseResponseObject;
    }


    private boolean validateData(SendOtpRequest sendOtpRequest) throws PocketException {

        if(sendOtpRequest.getMobileNumber()==null){
            logWriterUtility.error(sendOtpRequest.getRequestId(),"Sender mobile required");
            throw new PocketException(PocketErrorCode.SenderWalletIsRequired);
        }

        return true;
    }

}
