package tech.ipocket.pocket.services.um.settings;

import tech.ipocket.pocket.response.um.UserSettingsResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface UserSettingsService {
    UserSettingsResponse getUserSettings(String loginId) throws PocketException;
}
