package tech.ipocket.pocket.services.um.card;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmCustomerCardMapping;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.CustomerCardMappingRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.CardWalletResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@Service
public class CardServicesImpl implements CardServices {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private CustomerCardMappingRepository customerCardMappingRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public BaseResponseObject getWalletUsingCardNumber(String cardNumber, String requestId) throws PocketException {

        UmCustomerCardMapping umCustomerCardMapping=customerCardMappingRepository.findFirstByCardNumber(cardNumber);

        if(umCustomerCardMapping==null){
            logWriterUtility.error(requestId,"Invalid card number");
            throw new PocketException(PocketErrorCode.InvalidCardNumber);
        }

        return parseResponse(umCustomerCardMapping,requestId);
    }

    private BaseResponseObject parseResponse(UmCustomerCardMapping umCustomerCardMapping, String requestId) throws PocketException {

        UmUserInfo umUserInfo=userInfoRepository.findByLoginId(umCustomerCardMapping.getWalletNo());

        if(umUserInfo==null){
            logWriterUtility.error(requestId,"User not found for wallet :"+umCustomerCardMapping.getWalletNo());
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        CardWalletResponse cardWalletResponse=new CardWalletResponse();
        cardWalletResponse.setWalletNumber(umUserInfo.getMobileNumber());
        cardWalletResponse.setFullName(umUserInfo.getFullName());

        BaseResponseObject baseResponseObject=new BaseResponseObject(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(cardWalletResponse);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getWalletUsingCardUid(String cardUid, String requestId) throws PocketException {
        UmCustomerCardMapping umCustomerCardMapping=customerCardMappingRepository.findFirstByCardUid(cardUid);

        if(umCustomerCardMapping==null){
            logWriterUtility.error(requestId,"Invalid card number");
            throw new PocketException(PocketErrorCode.InvalidCardUID);
        }

        return parseResponse(umCustomerCardMapping,requestId);

    }
}
