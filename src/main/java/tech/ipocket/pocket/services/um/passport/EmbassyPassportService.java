package tech.ipocket.pocket.services.um.passport;

import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeEmptyRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface EmbassyPassportService {
    BaseResponseObject createPassportType(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject updatePassportType(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject createEmbassyPassportList(UmEmbPassportTypeEmptyRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject passportTypeById(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject searchPassportTypeAndStatus(UmEmbPassportTypeRequest request, TsSessionObject tsSessionObject) throws PocketException;

}
