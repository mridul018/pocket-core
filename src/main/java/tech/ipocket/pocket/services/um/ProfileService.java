package tech.ipocket.pocket.services.um;

import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.GetUserInfoRequest;
import tech.ipocket.pocket.request.um.ProfilePicUpdateRequest;
import tech.ipocket.pocket.request.um.UpdateDocumentRequest;
import tech.ipocket.pocket.request.um.UpdateFcmKeyRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface ProfileService {
    TokenValidateUmResponse getUserInfo(GetUserInfoRequest request) throws PocketException;

    BaseResponseObject getUserDevices(TokenValidateUmResponse validateResponse, EmptyRequest request) throws PocketException;

    BaseResponseObject updateFcmKey(TokenValidateUmResponse validateResponse, UpdateFcmKeyRequest request) throws PocketException;

    BaseResponseObject updateUserDocuments(TokenValidateUmResponse validateResponse, UpdateDocumentRequest request) throws PocketException;

    BaseResponseObject getUserDetails(GetUserInfoRequest request) throws PocketException;

    BaseResponseObject updateProfilePicture(TokenValidateUmResponse validateResponse, ProfilePicUpdateRequest request) throws PocketException;
}
