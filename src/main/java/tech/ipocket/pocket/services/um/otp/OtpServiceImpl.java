package tech.ipocket.pocket.services.um.otp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.UmOtp;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserOTPRepository;
import tech.ipocket.pocket.request.um.LoginStepTwoRequest;
import tech.ipocket.pocket.request.um.ResendOtpRequest;
import tech.ipocket.pocket.request.um.SendOtpRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.utils.*;

import java.util.Date;
import java.util.concurrent.CompletableFuture;

@Service
public class OtpServiceImpl implements OtpService {
    @Autowired
    private UserOTPRepository userOTPRepository;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AppConfiguration appConfiguration;


    @Autowired
    private Environment environment;

    @Override
    public OtpObject generateOtp(UmUserInfo umUserInfo, String requestId) {

        UmOtp umOtp = new UmOtp();
        umOtp.setCreatedDate(new Date());
        umOtp.setLoginId(umUserInfo.getLoginId());
        umOtp.setStatus("1");
        umOtp.setUmUserInfoByUserId(umUserInfo);

        RandomGenerator securityService = RandomGenerator.getInstance();
        if(umUserInfo.getMobileNumber().equalsIgnoreCase("+8801736597526")){     // Customer_app A/C test for google privacy policy
            umOtp.setOtpText("2121");
            umOtp.setOtpSecret(securityService.generateTokenAsString(12));
        }else {
            umOtp.setOtpText(securityService.generatePIN(4,environment));
            umOtp.setOtpSecret(securityService.generateTokenAsString(12));
        }
//        umOtp.setOtpText(securityService.generatePIN(4,environment));
//        umOtp.setOtpSecret(securityService.generateTokenAsString(12));

        userOTPRepository.save(umOtp);
        return new OtpObject(umOtp.getOtpText(), umOtp.getOtpSecret());
    }

    @Override
    public UmUserInfo verifyOtp(OtpObject request) throws PocketException {
        UmOtp umOtp = userOTPRepository.findFirstByOtpTextAndOtpSecret(request.getOtpText(), request.getOtpSecret());
        if (umOtp == null) {
            throw new PocketException(PocketErrorCode.OtpNotMatched);
        }

        umOtp.setStatus(UmEnums.OtpStatus.Used.getOtpStatus());
        userOTPRepository.save(umOtp);

        return umOtp.getUmUserInfoByUserId();

    }

    @Override
    public BaseResponseObject resendOtp(ResendOtpRequest request) throws PocketException {
        UmOtp umOtp = userOTPRepository.findFirstByOtpSecretAndStatus(request.getOtpSecret(),
                UmEnums.OtpStatus.UnUsed.getOtpStatus());
        if (umOtp == null) {
            throw new PocketException(PocketErrorCode.OtpNotFound);
        }

        umOtp.setCreatedDate(new Date());

        userOTPRepository.save(umOtp);


            CompletableFuture.runAsync(() -> {

                // send otp text as message
                String otpMessage = "Your otp is :" + umOtp.getOtpText();

                new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(umOtp.getLoginId(), otpMessage, ExternalType.SMS);

                String message = "Resend otp request from :" + request.getDeviceName();

                commonService.addInteractionLog(umOtp.getUmUserInfoByUserId().getLoginId(), message, request.getHardwareSignature(),
                        request.getDeviceName(), UmEnums.ActivityLogType.ResendOtp.getActivityLogType(), umOtp.getUmUserInfoByUserId());
            });

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public OtpObject generateOtp(Integer userCredentialId, UmUserInfo userInfo, String otpType, SendOtpRequest sendOtpRequest) {
        UmOtp umOtp=new UmOtp();
        umOtp.setCreatedDate(new Date());
        umOtp.setLoginId(userInfo.getLoginId());
        umOtp.setStatus("1");
        umOtp.setUmUserInfoByUserId(userInfo);

        RandomGenerator securityService = RandomGenerator.getInstance();
        umOtp.setOtpText(securityService.generatePIN(4,environment));
        umOtp.setOtpSecret(securityService.generateTokenAsString(12));

        userOTPRepository.save(umOtp);
        return new OtpObject(umOtp.getOtpText(),umOtp.getOtpSecret());
    }
}
