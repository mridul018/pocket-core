package tech.ipocket.pocket.services.um.registration;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;
import tech.ipocket.pocket.request.um.OtpVerificationRequest;
import tech.ipocket.pocket.request.um.UserDocuments;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.services.web.group.GroupService;
import tech.ipocket.pocket.utils.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional
public class RegistrationServiceImpl implements RegistrationService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private UserGroupRepository userGroupRepository;
    @Autowired
    private UserOTPRepository otpRepository;
    @Autowired
    private UserDeviceRepository userDeviceRepository;
    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserActivityLogRepository activityLogRepository;
    @Autowired
    private UserSettingsRepository userSettingsRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private Environment environment;
    @Autowired
    private AppConfiguration appConfiguration;
    @Autowired
    private UmUserBankMappingRepository userBankRepository;
    @Autowired
    private UmSrAgentMappingRepository umSrAgentMappingRepository;
    @Autowired
    private UserDocumentRepository userDocumentRepository;
    @Autowired
    private GroupService groupService;
    @Autowired
    private UmCustomerMerchantMappingRepository customerMerchantMappingRepository;


    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse customerRegistration(CustomerRegistration customerRegistration) throws PocketException {

        validateCustomerData(customerRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(customerRegistration.getMobileNumber());
        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCodeAndGroupStatus(customerRegistration.getGroupCode(),"1");
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        // set user information
        if (userInfo == null)
            userInfo = new UmUserInfo();
        userInfo.setFullName(customerRegistration.getFullName());
        userInfo.setMobileNumber(customerRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(customerRegistration.getPassword()));
        userInfo.setGroupCode(customerRegistration.getGroupCode());
        userInfo.setLoginId(customerRegistration.getMobileNumber());
        userInfo.setAccountStatus(AccountStatus.INITIATION.getCODE());
        userInfo.setDob(customerRegistration.getDateOfBirth());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setPrimaryIdNumber(customerRegistration.getPrimaryIdNumber());
        userDetails.setPrimaryIdIssueDate(customerRegistration.getPrimaryIdIssueDate());
        userDetails.setPrimaryIdExpiryDate(customerRegistration.getPrimaryIdExpiryDate());
        userDetails.setPrimaryIdType(customerRegistration.getPrimaryIdType());
        userDetails.setNationality(customerRegistration.getNationality());
        userDetails.setUmUserInfoByUserId(newUserInfo);
        userDetails.setNationality(customerRegistration.getNationality());
        userDetails.setCountryCode(customerRegistration.getCountryCode());
        userDetails.setStateId(customerRegistration.getStateId());

        userDetailsRepository.save(userDetails);


        //Merchant check(optional) for customer registration
        UmUserInfo umMerchant = null;
        if(customerRegistration != null && customerRegistration.getSrWallet() != null){
            umMerchant = userInfoRepository.findByLoginIdAndGroupCode(customerRegistration.getSrWallet(), GroupCodes.MERCHANT);
            if(umMerchant == null){
                throw new PocketException("Merchant number isn't found.", "Merchant number isn't found.");
            }
            UmCustomerMerchantMapping customerMerchantMapping = new UmCustomerMerchantMapping();
            customerMerchantMapping.setCustomerId(userInfo.getId());
            customerMerchantMapping.setMerchantId(umMerchant.getId());
            customerMerchantMapping.setStatus("1");
            customerMerchantMapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            customerMerchantMappingRepository.save(customerMerchantMapping);
        }

        // save SR merchant mapp
        if(customerRegistration.getSrWallet()==null||customerRegistration.getSrWallet().trim().length()==0){
            // TODO: 2019-10-12 work here
            customerRegistration.setSrWallet("");
        }

        UmUserInfo srUserInfo=userInfoRepository.findByLoginId(customerRegistration.getSrWallet());
        if(srUserInfo!=null){
            UmSrAgentMapping umSrAgentMapping=new UmSrAgentMapping();
            umSrAgentMapping.setAgentId(userInfo.getId());
            umSrAgentMapping.setSrId(srUserInfo.getId());
            umSrAgentMapping.setStatus("1");
            umSrAgentMapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            umSrAgentMappingRepository.save(umSrAgentMapping);
        }

        // save documents

        // set for otp
//        String otpText = RandomGenerator.getInstance().generatePIN(4,environment);   //Comment out this line after completing April/22 event.
        String otpText = generatePINTemp(4,environment);                //Remove this line after upcoming event on April/22 start
        String secret = RandomGenerator.getInstance().generateTokenAsString(12);
        UmOtp otp = new UmOtp();
        otp.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        otp.setOtpSecret(secret);
        otp.setOtpText(otpText);
        otp.setUmUserInfoByUserId(newUserInfo);
        otp.setStatus(String.valueOf(AccountStatus.INITIATION.getCODE()));
        otp.setLoginId(newUserInfo.getMobileNumber());
        UmOtp newOtp = otpRepository.save(otp);

        // save user device info
        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userDevice.setDeviceName(customerRegistration.getDeviceName());
        userDevice.setHardwareSignature(customerRegistration.getHardwareSignature());
        userDevice.setMetaData(customerRegistration.getMetaData());
        UmUserDevice newDevice = userDeviceRepository.save(userDevice);


        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(customerRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.CUSTOMER_REGISTRATION.getKeyString());
        log.setMetaData(customerRegistration.getMetaData());
        log.setMobileNumber(customerRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);

//        CompletableFuture.runAsync(() -> {                                 //Comment out this after completing April/22 event.
//            String msg = "Your PIN number is : " + newOtp.getOtpText();
//            new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(customerRegistration.getMobileNumber(), msg, ExternalType.SMS);
//        });

        // send response
        registrationResponse.setDeviceId(newDevice.getId());
        registrationResponse.setOtpId(newOtp.getId());
        registrationResponse.setOtpSecret(secret);
        registrationResponse.setRegistrationDate(new Date());

        return registrationResponse;
    }

    //Remove this after upcoming event on April/22 start
    public String generatePINTemp(int numberOfDigits, Environment environment) {
        String randomOtp=environment.getProperty("RANDOM_OTP");
        if(randomOtp==null){
            randomOtp=String.format("%0" + numberOfDigits + "d", (new Random()).nextInt((int) Math.pow(10.0D, (double) numberOfDigits)));
        }
        return randomOtp;
    }
    //end

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse merchantAgentRegistration(MerchantAgentRegistration merchantAgentRegistration) throws PocketException {

        validateAgentMerchantData(merchantAgentRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(merchantAgentRegistration.getMobileNumber());
        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCodeAndGroupStatus(merchantAgentRegistration.getGroupCode(),"1");
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        if(merchantAgentRegistration.getSrWallet()==null){
            logWriterUtility.error(merchantAgentRegistration.getRequestId(),"SR wallet is null");
            throw new PocketException(PocketErrorCode.SrWalletRequired);
        }
        UmUserInfo srUserInfo=userInfoRepository.findByLoginIdAndGroupCode(merchantAgentRegistration.getSrWallet(),GroupCodes.SR);

        if(srUserInfo==null){
            logWriterUtility.error(merchantAgentRegistration.getRequestId(),"SR wallet not found");
            throw new PocketException(PocketErrorCode.InvalidSrWallet);
        }

        // TS for create wallet
        WalletCreateRequest walletCreateRequest = new WalletCreateRequest();
        walletCreateRequest.setFullName(merchantAgentRegistration.getFullName());
        walletCreateRequest.setGroupCode(merchantAgentRegistration.getGroupCode());
        walletCreateRequest.setMobileNumber(merchantAgentRegistration.getMobileNumber());
        walletCreateRequest.setUserStatus("" + UmEnums.UserStatus.Verified.getUserStatusType());
        walletCreateRequest.setHardwareSignature(merchantAgentRegistration.getHardwareSignature());
        walletCreateRequest.setRequestId(merchantAgentRegistration.getRequestId());

        if (!WalletClass.walletCreate(walletCreateRequest,environment)) {
            throw new PocketException(PocketErrorCode.WALLET_CREATE_EXCEPTION);
        }


        // insert user info
        if (userInfo == null) {
            userInfo = new UmUserInfo();
        }
        userInfo.setFullName(merchantAgentRegistration.getFullName());
        userInfo.setMobileNumber(merchantAgentRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(merchantAgentRegistration.getPassword()));
        //userInfo.setPassword(Base64.getEncoder().encodeToString(merchantAgentRegistration.getPassword().getBytes()));
        userInfo.setGroupCode(merchantAgentRegistration.getGroupCode());
        userInfo.setLoginId(merchantAgentRegistration.getMobileNumber());
        userInfo.setAccountStatus(UmEnums.UserStatus.Verified.getUserStatusType());

        if(walletCreateRequest.getDob()!=null) {
            userInfo.setDob(DateUtil.parseDate(walletCreateRequest.getDob()));
        }
        userInfo.setGender(merchantAgentRegistration.getGender());
        userInfo.setDob(merchantAgentRegistration.getDateOfBirth());


        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // insert user details
        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setPrimaryIdNumber(merchantAgentRegistration.getPrimaryIdNumber());
        userDetails.setPrimaryIdIssueDate(merchantAgentRegistration.getPrimaryIdIssueDate());
        userDetails.setPrimaryIdExpiryDate(merchantAgentRegistration.getPrimaryIdExpiryDate());
        userDetails.setPrimaryIdType(merchantAgentRegistration.getPrimaryIdType());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetails.setApplicantsName(merchantAgentRegistration.getApplicantsName());
        userDetails.setTradeLicenseNo(merchantAgentRegistration.getTradeLicenseNo());
        userDetails.setSecondContactName(merchantAgentRegistration.getSecondContactName());
        userDetails.setSecondContactMobileNo(merchantAgentRegistration.getSecondContactMobileNo());
        userDetails.setThirdContactName(merchantAgentRegistration.getThirdContactName());
        userDetails.setThirdContactMobileNo(merchantAgentRegistration.getThirdContactMobileNo());

        userDetails.setPresentAddress(merchantAgentRegistration.getPresentAddress());
        userDetails.setPermanentAddress(merchantAgentRegistration.getPermanentAddress());
        userDetails.setPostCode(merchantAgentRegistration.getPostCode());


        userDetails.setNationality(merchantAgentRegistration.getNationality());
        userDetails.setCountryCode(merchantAgentRegistration.getCountryCode());
        userDetails.setStateId(merchantAgentRegistration.getStateId());

        userDetailsRepository.save(userDetails);

        UmUserSettings umUserSettings = new UmUserSettings();
        umUserSettings.setMobileNumberVerified(Boolean.TRUE);
        umUserSettings.setUmUserInfoByUserId(userInfo);
//        umUserSettings.setPrimaryIdVerified(Boolean.FALSE);
        umUserSettings.setPrimaryIdVerified(Boolean.TRUE);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);

        // save SR merchant mapp

        if(merchantAgentRegistration.getSrWallet()==null||merchantAgentRegistration.getSrWallet().trim().length()==0){
            // TODO: 2019-10-12 work here
            merchantAgentRegistration.setSrWallet("");
        }


        UmSrAgentMapping umSrAgentMapping=new UmSrAgentMapping();
        umSrAgentMapping.setAgentId(userInfo.getId());
        umSrAgentMapping.setSrId(srUserInfo.getId());
        umSrAgentMapping.setStatus("1");
        umSrAgentMapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));

        umSrAgentMappingRepository.save(umSrAgentMapping);


        // Save user bank mapping

        if(merchantAgentRegistration.getAccountNumber()!=null){


            UmUserBankMapping umUserBankMapping=new UmUserBankMapping();
            umUserBankMapping.setAccountName(merchantAgentRegistration.getAccountName());
            umUserBankMapping.setAccountNumber(merchantAgentRegistration.getAccountNumber());
            umUserBankMapping.setBankCode(merchantAgentRegistration.getBankCode());
            umUserBankMapping.setBankName(merchantAgentRegistration.getBankName());
            umUserBankMapping.setBankRoutingNo(merchantAgentRegistration.getBankRoutingNo());

            umUserBankMapping.setBranchName(merchantAgentRegistration.getBranchName());
            umUserBankMapping.setBranchSwiftCode(merchantAgentRegistration.getBranchSwiftCode());
            umUserBankMapping.setUserId(userInfo.getId());

            umUserBankMapping.setCreatedDate(new Date());
            umUserBankMapping.setStatus("1");
            userBankRepository.save(umUserBankMapping);

            logWriterUtility.trace(walletCreateRequest.getRequestId(),"User bank map save done");
        }


        if(merchantAgentRegistration.getUserDocuments()!=null&&merchantAgentRegistration.getUserDocuments().size()>0){
            for (UserDocuments documentItem : merchantAgentRegistration.getUserDocuments()) {

                UmDocuments umDocuments = new UmDocuments();

                if (documentItem.getDocumentType().equals(UmEnums.DocumentTypes.ProfilePicture.getDocumentType())) {
                    String documentPath = saveDocumentsToDisk(documentItem.getDocumentBase64String(), ".png",
                            merchantAgentRegistration.getRequestId());
                    umDocuments.setCreatedDate(new Date());
                    umDocuments.setDocumentType(documentItem.getDocumentType());
                    umDocuments.setStatus("1");
                    umDocuments.setUmUserInfoByUserId(userInfo);
                    umDocuments.setDocumentPath(documentPath);
                    userDocumentRepository.save(umDocuments);

                    userInfo.setPhotoId(umDocuments.getId());
                    userInfoRepository.save(userInfo);
                }else if (documentItem.getDocumentType().equals(UmEnums.DocumentTypes.NID.getDocumentType())) {
                    String documentPath = saveDocumentsToDisk(documentItem.getDocumentBase64String(), ".png",
                            merchantAgentRegistration.getRequestId());
                    umDocuments.setCreatedDate(new Date());
                    umDocuments.setDocumentType(documentItem.getDocumentType());
                    umDocuments.setStatus("1");
                    umDocuments.setUmUserInfoByUserId(userInfo);
                    umDocuments.setDocumentPath(documentPath);
                    userDocumentRepository.save(umDocuments);

                    userDetails.setNidId(umDocuments.getId());
                    userDetailsRepository.save(userDetails);
                }else if (documentItem.getDocumentType().equals(UmEnums.DocumentTypes.TRADE_LICENSE.getDocumentType())) {
                    String documentPath = saveDocumentsToDisk(documentItem.getDocumentBase64String(), ".png",
                            merchantAgentRegistration.getRequestId());
                    umDocuments.setCreatedDate(new Date());
                    umDocuments.setDocumentType(documentItem.getDocumentType());
                    umDocuments.setStatus("1");
                    umDocuments.setUmUserInfoByUserId(userInfo);
                    umDocuments.setDocumentPath(documentPath);
                    userDocumentRepository.save(umDocuments);

                    userDetails.setTradeLicenseId(umDocuments.getId());
                    userDetailsRepository.save(userDetails);
                }
            }
        }

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(merchantAgentRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.MERCHANT_OR_AGENT_REGISTRATION.getKeyString());
        log.setMetaData(merchantAgentRegistration.getMetaData());
        log.setMobileNumber(merchantAgentRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);


        CompletableFuture.runAsync(() -> {
            String msg = "Your registration is completed. Please login with default" +
                    "mobile number and password";
            new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(merchantAgentRegistration.getMobileNumber(), msg, ExternalType.SMS);

            msg = "Hi, " + merchantAgentRegistration.getFullName() + "," +
                    "Congratulation, your account has been created successfully.\nPlease " +
                    "login with default mobile number and password";
            new ExternalCall(appConfiguration.getExternalUrl()).sendEmail(merchantAgentRegistration.getEmail(), msg,"Registration Complete",ExternalType.EMAIL);
        });




        // send response
        registrationResponse.setRegistrationDate(new Date());
        registrationResponse.setMessage("Registration successfully completed");
        registrationResponse.setUserId(newUserInfo.getId());

        return registrationResponse;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse srRegistration(SrRegistration srRegistration) throws PocketException {
        validateSrData(srRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(srRegistration.getMobileNumber());
        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCodeAndGroupStatus(srRegistration.getGroupCode(),"1");
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        // TS for create wallet
        WalletCreateRequest walletCreateRequest = new WalletCreateRequest();
        walletCreateRequest.setFullName(srRegistration.getFullName());
        walletCreateRequest.setGroupCode(srRegistration.getGroupCode());
        walletCreateRequest.setMobileNumber(srRegistration.getMobileNumber());
        walletCreateRequest.setUserStatus("" + UmEnums.UserStatus.Verified.getUserStatusType());
        walletCreateRequest.setHardwareSignature(srRegistration.getHardwareSignature());
        walletCreateRequest.setRequestId(srRegistration.getRequestId());


        if (!WalletClass.walletCreate(walletCreateRequest,environment)) {
            throw new PocketException(PocketErrorCode.WALLET_CREATE_EXCEPTION);
        }


        // insert user info
        if (userInfo == null)
            userInfo = new UmUserInfo();
        userInfo.setFullName(srRegistration.getFullName());
        userInfo.setMobileNumber(srRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(srRegistration.getPassword()));
        //userInfo.setPassword(Base64.getEncoder().encodeToString(merchantAgentRegistration.getPassword().getBytes()));
        userInfo.setGroupCode(srRegistration.getGroupCode());
        userInfo.setLoginId(srRegistration.getMobileNumber());
        userInfo.setAccountStatus(UmEnums.UserStatus.Verified.getUserStatusType());

        if(walletCreateRequest.getDob()!=null) {
            userInfo.setDob(DateUtil.parseDate(walletCreateRequest.getDob()));
        }
        userInfo.setGender(srRegistration.getGender());
        userInfo.setDob(srRegistration.getDateOfBirth());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // insert user details
        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setPrimaryIdNumber(srRegistration.getPrimaryIdNumber());
        userDetails.setPrimaryIdIssueDate(srRegistration.getPrimaryIdIssueDate());
        userDetails.setPrimaryIdExpiryDate(srRegistration.getPrimaryIdExpiryDate());
        userDetails.setPrimaryIdType(srRegistration.getPrimaryIdType());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetails.setApplicantsName(srRegistration.getApplicantsName());
        userDetails.setTradeLicenseNo(srRegistration.getTradeLicenseNo());
        userDetails.setSecondContactName(srRegistration.getSecondContactName());
        userDetails.setSecondContactMobileNo(srRegistration.getSecondContactMobileNo());
        userDetails.setThirdContactName(srRegistration.getThirdContactName());
        userDetails.setThirdContactMobileNo(srRegistration.getThirdContactMobileNo());

        userDetails.setPresentAddress(srRegistration.getPresentAddress());
        userDetails.setPermanentAddress(srRegistration.getPermanentAddress());
        userDetails.setPostCode(srRegistration.getPostCode());


        userDetails.setNationality(srRegistration.getNationality());
        userDetails.setCountryCode(srRegistration.getCountryCode());
        userDetails.setStateId(srRegistration.getStateId());

        userDetailsRepository.save(userDetails);

        UmUserSettings umUserSettings = new UmUserSettings();
        umUserSettings.setMobileNumberVerified(Boolean.TRUE);
        umUserSettings.setUmUserInfoByUserId(userInfo);
//        umUserSettings.setPrimaryIdVerified(Boolean.FALSE);
        umUserSettings.setPrimaryIdVerified(Boolean.TRUE);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);


        // Save user bank mapping

        if(srRegistration.getAccountNumber()!=null){


            UmUserBankMapping umUserBankMapping=new UmUserBankMapping();
            umUserBankMapping.setAccountName(srRegistration.getAccountName());
            umUserBankMapping.setAccountNumber(srRegistration.getAccountNumber());
            umUserBankMapping.setBankCode(srRegistration.getBankCode());
            umUserBankMapping.setBankName(srRegistration.getBankName());
            umUserBankMapping.setBankRoutingNo(srRegistration.getBankRoutingNo());

            umUserBankMapping.setBranchName(srRegistration.getBranchName());
            umUserBankMapping.setBranchSwiftCode(srRegistration.getBranchSwiftCode());
            umUserBankMapping.setUserId(userInfo.getId());

            umUserBankMapping.setCreatedDate(new Date());
            umUserBankMapping.setStatus("1");
            userBankRepository.save(umUserBankMapping);

            logWriterUtility.trace(walletCreateRequest.getRequestId(),"User bank map save done");
        }

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(srRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.MERCHANT_OR_AGENT_REGISTRATION.getKeyString());
        log.setMetaData(srRegistration.getMetaData());
        log.setMobileNumber(srRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);


        CompletableFuture.runAsync(() -> {
            String msg = "Your registration is complete. Please login with default" +
                    "mobile number and password";
            new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(srRegistration.getMobileNumber(), msg, ExternalType.SMS);

            msg = "Hi, " + srRegistration.getFullName() + "," +
                    "Congratulation, you account has been create successfully.\nPlease " +
                    "login with default mobile number and password";
            new ExternalCall(appConfiguration.getExternalUrl()).sendEmail(srRegistration.getEmail(), msg,"Registration Complete",ExternalType.EMAIL);
        });


        // send response
        registrationResponse.setRegistrationDate(new Date());
        registrationResponse.setMessage("Registration successfully completed");
        registrationResponse.setUserId(newUserInfo.getId());

        return registrationResponse;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse adminRegistration(AdminRegistration adminRegistration) throws PocketException {

        validateAdminData(adminRegistration);

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findFirstByEmail(adminRegistration.getEmail());

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        UmUserGroup userGroup = userGroupRepository.findByGroupCodeAndGroupStatus(adminRegistration.getGroupCode(),"1");
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        // insert user info
        if (userInfo == null)
            userInfo = new UmUserInfo();

        userInfo.setFullName(adminRegistration.getFullName());
        userInfo.setMobileNumber(adminRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(adminRegistration.getPassword()));
        //userInfo.setPassword(Base64.getEncoder().encodeToString(adminRegistration.getPassword().getBytes()));
        userInfo.setGroupCode(adminRegistration.getGroupCode());
        userInfo.setLoginId(adminRegistration.getEmail());
        userInfo.setAccountStatus(AccountStatus.VERIFIED.getCODE());
        userInfo.setDob(adminRegistration.getDateOfBirth());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // insert user details
        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setNationality(adminRegistration.getNationality());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetailsRepository.save(userDetails);


        // save user device info
        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userDevice.setDeviceName(adminRegistration.getDeviceName());
        userDevice.setHardwareSignature(adminRegistration.getHardwareSignature());
        userDevice.setMetaData(adminRegistration.getMetaData());
        UmUserDevice newDevice = userDeviceRepository.save(userDevice);

        // add userDeviceMap new row
        UmUserDeviceMapping mapping = new UmUserDeviceMapping();
        mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapping.setUmUserInfoByUserId(newUserInfo);
        mapping.setUmUserDeviceByDeviceId(newDevice);
        mapping.setStatus("1");
        userDeviceMapRepository.save(mapping);

        // user settings
        UmUserSettings umUserSettings = new UmUserSettings();
        umUserSettings.setMobileNumberVerified(Boolean.FALSE);
        umUserSettings.setUmUserInfoByUserId(userInfo);
        umUserSettings.setPrimaryIdVerified(Boolean.FALSE);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);


        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(adminRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.ADMIN_REGISTRATION.getKeyString());
        log.setMobileNumber(adminRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);

        // send response
        registrationResponse.setRegistrationDate(new Date());
        registrationResponse.setMessage("Registration successfully completed");
        registrationResponse.setUserId(newUserInfo.getId());

        CompletableFuture.runAsync(() -> {
            String msg = "Hi, " + adminRegistration.getFullName() + "," +
                    "Congratulation, you account has been create successfully.\nPlease " +
                    "login with default email and password";
            new ExternalCall(appConfiguration.getExternalUrl()).sendEmail(adminRegistration.getEmail(), msg,"Registration Confirmation",
                     ExternalType.EMAIL);
        });


        return registrationResponse;
    }


    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse commonRegistration(CommonRegistration commonRegistration) throws PocketException {
        validateCommonRegistrationData(commonRegistration);

        UmUserGroup userGroup = userGroupRepository.findByGroupCodeAndGroupStatus(commonRegistration.getGroupCode(),"1");
        if (userGroup == null) {
            throw new PocketException(PocketErrorCode.GROUPCODENOTFOUND);
        }

        List<UmUserGroup> supportedGroups=groupService.getCommonUserRegistrationGroup();
        if(groupService==null){
            logWriterUtility.error(commonRegistration.getRequestId(),"No supported common group found");
            throw new PocketException(PocketErrorCode.InvalidGroupCode);
        }

        boolean isSupportedGroup=false;
        for (UmUserGroup umUserGroup:supportedGroups
             ) {
            if(umUserGroup.getGroupCode().equalsIgnoreCase(userGroup.getGroupCode())){
                isSupportedGroup=true;
                break;
            }
        }

        if(!isSupportedGroup){
            logWriterUtility.error(commonRegistration.getRequestId(),"Groupcode not in common group :"+commonRegistration.getGroupCode());
            throw new PocketException(PocketErrorCode.InvalidGroupCode);
        }

        RegistrationResponse registrationResponse = new RegistrationResponse();

        UmUserInfo userInfo = userInfoRepository.findFirstByEmail(commonRegistration.getEmail());

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.INITIATION.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.VERIFIED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_ALREADY_REGISTERED);
        }

        if (userInfo != null && (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE() ||
                userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE())) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }



        // insert user info
        if (userInfo == null)
            userInfo = new UmUserInfo();

        userInfo.setFullName(commonRegistration.getFullName());
        userInfo.setMobileNumber(commonRegistration.getMobileNumber());
        userInfo.setPassword(passwordEncoder.encode(commonRegistration.getPassword()));
        //userInfo.setPassword(Base64.getEncoder().encodeToString(adminRegistration.getPassword().getBytes()));
        userInfo.setGroupCode(commonRegistration.getGroupCode());
        userInfo.setLoginId(commonRegistration.getEmail());
        userInfo.setAccountStatus(AccountStatus.VERIFIED.getCODE());
        userInfo.setDob(commonRegistration.getDateOfBirth());

        UmUserInfo newUserInfo = userInfoRepository.saveAndFlush(userInfo);

        // insert user details
        // set user details
        UmUserDetails userDetails = new UmUserDetails();
        userDetails.setNationality(commonRegistration.getNationality());
        userDetails.setUmUserInfoByUserId(newUserInfo);

        userDetailsRepository.save(userDetails);


        // save user device info
        UmUserDevice userDevice = new UmUserDevice();
        userDevice.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userDevice.setDeviceName(commonRegistration.getDeviceName());
        userDevice.setHardwareSignature(commonRegistration.getHardwareSignature());
        userDevice.setMetaData(commonRegistration.getMetaData());
        UmUserDevice newDevice = userDeviceRepository.save(userDevice);

        // add userDeviceMap new row
        UmUserDeviceMapping mapping = new UmUserDeviceMapping();
        mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapping.setUmUserInfoByUserId(newUserInfo);
        mapping.setUmUserDeviceByDeviceId(newDevice);
        mapping.setStatus("1");
        userDeviceMapRepository.save(mapping);

        // user settings
        UmUserSettings umUserSettings = new UmUserSettings();
        umUserSettings.setMobileNumberVerified(Boolean.FALSE);
        umUserSettings.setUmUserInfoByUserId(userInfo);
        umUserSettings.setPrimaryIdVerified(Boolean.FALSE);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);


        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(commonRegistration.getHardwareSignature());
        log.setLogType(ActivityLogType.ADMIN_REGISTRATION.getKeyString());
        log.setMobileNumber(commonRegistration.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(newUserInfo);

        activityLogRepository.save(log);

        // send response
        registrationResponse.setRegistrationDate(new Date());
        registrationResponse.setMessage("Registration successfully completed");
        registrationResponse.setUserId(newUserInfo.getId());

        CompletableFuture.runAsync(() -> {
            String msg = "Hi, " + commonRegistration.getFullName() + "," +
                    "Congratulation, you account has been create successfully.\nPlease " +
                    "login with default email and password";
            new ExternalCall(appConfiguration.getExternalUrl()).sendEmail(commonRegistration.getEmail(), msg,"Registration Confirmation",
                    ExternalType.EMAIL);
        });


        return registrationResponse;
    }
    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public BaseResponseObject doVerifyUser(OtpVerificationRequest otpVerificationRequest) throws PocketException {

        // TODO: 2019-05-09 work for reversal with ts during failed in um

        UmOtp otp = otpRepository.findFirstByOtpTextAndOtpSecret(otpVerificationRequest.getOtpText(), otpVerificationRequest.getOtpSecret());
        if (otp == null) {
            throw new PocketException(PocketErrorCode.OtpNotMatched);
        }

        UmUserInfo umUserInfo = otp.getUmUserInfoByUserId();

        // create wallet
        // TS for create wallet
        WalletCreateRequest walletCreateRequest = new WalletCreateRequest();
        walletCreateRequest.setFullName(umUserInfo.getFullName());
        walletCreateRequest.setGroupCode(umUserInfo.getGroupCode());
        walletCreateRequest.setMobileNumber(umUserInfo.getMobileNumber());
//        walletCreateRequest.setUserStatus(String.valueOf(umUserInfo.getAccountStatus()));
        walletCreateRequest.setUserStatus("" + UmEnums.UserStatus.Verified.getUserStatusType());
        walletCreateRequest.setHardwareSignature(otpVerificationRequest.getHardwareSignature());
        walletCreateRequest.setRequestId(otpVerificationRequest.getRequestId());


        if (!WalletClass.walletCreate(walletCreateRequest,environment)) {
            throw new PocketException(PocketErrorCode.WALLET_CREATE_EXCEPTION);
        }


        otp.setStatus(String.valueOf(AccountStatus.VERIFIED.getCODE()));
        otpRepository.save(otp);


        // todo have to replace code here
        // add user settings data
        UmUserSettings userSettings = new UmUserSettings();
        userSettings.setMobileNumberVerified(true);
//        userSettings.setPrimaryIdVerified(false);
        userSettings.setPrimaryIdVerified(true);
        userSettings.setUmUserInfoByUserId(umUserInfo);
        userSettingsRepository.save(userSettings);

        // update user info data status = verify
        umUserInfo.setAccountStatus(AccountStatus.VERIFIED.getCODE());
        userInfoRepository.save(umUserInfo);

        // check device already exists or not
        UmUserDevice userDevice = userDeviceRepository.findFirstByHardwareSignature(otpVerificationRequest.getHardwareSignature());
        if (userDevice == null) {
            logWriterUtility.error(otpVerificationRequest.getRequestId(), "Device not found");
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        // add userDeviceMap new row
        UmUserDeviceMapping mapping = new UmUserDeviceMapping();
        mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        mapping.setUmUserInfoByUserId(umUserInfo);
        mapping.setUmUserDeviceByDeviceId(userDevice);
        mapping.setStatus("1");
        userDeviceMapRepository.save(mapping);

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(otpVerificationRequest.getHardwareSignature());
        log.setLogType(ActivityLogType.CUSTOMER_OTP_VERIFICATION_AFTER_REGISTRATION.getKeyString());
        log.setMobileNumber(umUserInfo.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(umUserInfo);

        activityLogRepository.save(log);

        CompletableFuture.runAsync(() -> {
                    String msg= "Your account activation done";

                     // Remove this after upcoming event on April/22 start
                    if(!otpVerificationRequest.getSource().equalsIgnoreCase("CUSTOMER_APP")){
                        new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(umUserInfo.getMobileNumber(), msg, ExternalType.SMS);
                    }
                    //end

//                  new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(umUserInfo.getMobileNumber(), msg, ExternalType.SMS); //Comment out this line after completing April/22 event.
                    msg = "Hi " + umUserInfo.getFullName() + ",\nYour account is create successfully. Now you can login with your default " +
                            "mobile number and pin";
                    new ExternalCall(appConfiguration.getExternalUrl()).sendEmail(umUserInfo.getEmail(), msg,"Account Create",  ExternalType.EMAIL);

                });



        BaseResponseObject baseResponseObject = new BaseResponseObject(otpVerificationRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(otpVerificationRequest.getRequestId());
        baseResponseObject.setStatus("200");
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }


    private void validateCustomerData(CustomerRegistration customerRegistration) throws PocketException {
        check(customerRegistration.getMobileNumber(), customerRegistration.getPrimaryIdNumber(), customerRegistration.getPrimaryIdType(), customerRegistration.getPrimaryIdIssueDate(), customerRegistration.getPrimaryIdExpiryDate(), customerRegistration.getNationality());
    }

    private void validateAgentMerchantData(MerchantAgentRegistration merchantAgentRegistration) throws PocketException {
        check(merchantAgentRegistration.getMobileNumber(), merchantAgentRegistration.getPrimaryIdNumber(), merchantAgentRegistration.getPrimaryIdType(), merchantAgentRegistration.getPrimaryIdIssueDate(), merchantAgentRegistration.getPrimaryIdExpiryDate(), merchantAgentRegistration.getNationality());
    }

    private void validateSrData(SrRegistration srRegistration) throws PocketException {
        check(srRegistration.getMobileNumber(), srRegistration.getPrimaryIdNumber(), srRegistration.getPrimaryIdType(), srRegistration.getPrimaryIdIssueDate(), srRegistration.getPrimaryIdExpiryDate(), srRegistration.getNationality());
    }

    private void check(String mobileNumber, String primaryIdNumber, String primaryIdType, java.sql.Date primaryIdIssueDate, java.sql.Date primaryIdExpiryDate, String nationality) throws PocketException {
        if (mobileNumber == null || mobileNumber.equals("")) {
            throw new PocketException(PocketErrorCode.MobileNumberRequired);
        }
        if((mobileNumber.substring(0,3).equals("+97")) && (mobileNumber.length() != 13)) {
            throw new PocketException(""+PocketErrorCode.InvalidUAEMobileNumber);
        }
        if((mobileNumber.substring(0,3).equals("+88")) && (mobileNumber.length() != 14)) {
            throw new PocketException(""+PocketErrorCode.InvalidBDMobileNumber);
        }
        if (primaryIdNumber == null || primaryIdNumber.equals("")) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberRequired);
        }

        if (primaryIdType == null) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberTypeRequired);
        }

        if (primaryIdIssueDate == null) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberIssueDateRequired);
        }
        if (primaryIdExpiryDate == null) {
            throw new PocketException(PocketErrorCode.PrimayIdNumberExpiryDateRequired);
        }
        if (nationality == null || nationality.equals("")) {
            throw new PocketException(PocketErrorCode.NationalityRequired);
        }
    }


    private void validateAdminData(AdminRegistration adminRegistration) throws PocketException {
        if (adminRegistration.getEmail() == null || adminRegistration.getEmail().equals("")) {
            throw new PocketException(PocketErrorCode.EmailRequired);
        }
        if (adminRegistration.getNationality() == null || adminRegistration.getNationality().equals("")) {
            throw new PocketException(PocketErrorCode.NationalityRequired);
        }

    }

    private void validateCommonRegistrationData(CommonRegistration commonRegistration) throws PocketException {

        if (commonRegistration.getGroupCode() == null || commonRegistration.getGroupCode().equals("")) {
            throw new PocketException(PocketErrorCode.GroupCodeRequired);
        }

        if (commonRegistration.getEmail() == null || commonRegistration.getEmail().equals("")) {
            throw new PocketException(PocketErrorCode.EmailRequired);
        }
        if (commonRegistration.getNationality() == null || commonRegistration.getNationality().equals("")) {
            throw new PocketException(PocketErrorCode.NationalityRequired);
        }

    }

    private String saveDocumentsToDisk(String document, String extension, String requestId) throws PocketException {

        String home=System.getProperty("user.home");

        String target = home+""+environment.getProperty("file_base_path");
        String fileName = System.currentTimeMillis() + "." + extension;


        File file = new File(target);
        if (!file.exists()) {
            System.out.print("No Directory");
            file.mkdir();
            System.out.print("Folder created");
        } else {
            logWriterUtility.debug(requestId, " Exists :" + target + "\".");
        }

        byte[] imageByte = Base64.decodeBase64(document);

        String directory = target + "/" + fileName;

        logWriterUtility.debug(requestId, "Saving File :" + directory);

        try {
            new FileOutputStream(directory).write(imageByte);
        } catch (IOException e) {
            logWriterUtility.error(requestId, e);
            throw new PocketException(PocketErrorCode.IOException, e.getMessage());
        }

        return fileName;
    }
}
