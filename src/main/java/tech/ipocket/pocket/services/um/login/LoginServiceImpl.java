package tech.ipocket.pocket.services.um.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.repository.web.UserGroupFunctionMapRepository;
import tech.ipocket.pocket.repository.web.UserFunctionRepository;
import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.request.security.TokenGenerateRequest;
import tech.ipocket.pocket.request.um.ForgotPinStepOneRequest;
import tech.ipocket.pocket.request.um.ForgotPinStepTwoRequest;
import tech.ipocket.pocket.request.um.LoginRequest;
import tech.ipocket.pocket.request.um.LoginStepTwoRequest;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.response.security.TokenGenerateResponse;
import tech.ipocket.pocket.response.um.ForgotPinStepOneResponse;
import tech.ipocket.pocket.response.um.LoginResponse;
import tech.ipocket.pocket.response.um.UserFunctionResponseItem;
import tech.ipocket.pocket.response.um.UserSettingsResponse;
import tech.ipocket.pocket.services.external.ExternalServiceImpl;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.services.um.otp.OtpObject;
import tech.ipocket.pocket.services.um.otp.OtpService;
import tech.ipocket.pocket.utils.*;

import java.io.IOException;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Scope("prototype")
@Service
public class LoginServiceImpl implements LoginService {

    private static final long BLOCK_TIME = 30;
    private static final long ONE_MINUTE_IN_MILLIS = 60000;
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserDeviceRepository userDeviceRepository;

    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserActivityLogRepository activityLogRepository;

    @Autowired
    private UserGroupFunctionMapRepository userGroupFunctionMapRepository;
    @Autowired
    private UserFunctionRepository userFunctionRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private OtpService otpService;

    @Autowired
    SecurityService securityService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AppConfiguration appConfiguration;
    @Autowired
    private UserDocumentRepository documentsDao;

    @Autowired
    private ExternalServiceImpl externalService;
    @Autowired
    private UserOTPRepository userOTPRepository;
    @Autowired
    private Environment environment;

    @Override
    public Boolean isUserStatusAbleToLogin(UmUserInfo userInfo, LoginRequest loginRequest) throws PocketException {

        String requestId = loginRequest.getRequestId();

        if (userInfo.getAccountStatus() == UmEnums.UserStatus.Verified.getUserStatusType()) return Boolean.TRUE;
        logWriterUtility.debug(requestId, "Customer Wallet Status : " + userInfo.getAccountStatus());

        if (userInfo.getAccountStatus() == UmEnums.UserStatus.Deleted.getUserStatusType()) {
            logWriterUtility.error(requestId, "User account is deleted:" + userInfo.getAccountStatus());
            throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
        } else if (userInfo.getAccountStatus() == UmEnums.UserStatus.HardBlocked.getUserStatusType()) {
            logWriterUtility.error(requestId, "Your account is hard blocked.");
            throw new PocketException(PocketErrorCode.AccountIsHardBlocked);
        } else if (userInfo.getAccountStatus() == UmEnums.UserStatus.Blocked.getUserStatusType()) {
            logWriterUtility.error(requestId, "Your account is blocked. Try again later");
            throw new PocketException(PocketErrorCode.AccountIsBlocked);
        }


        // Get Interaction Log within 30 mins
        // Remove after Right try portion
        // Check wrong try Count>0 , return user for completing 30 minutes
        UmActivityLog interactionLog = null;
        Date startDate = new Date(Calendar.getInstance().getTimeInMillis() - (BLOCK_TIME * ONE_MINUTE_IN_MILLIS));
        List<UmActivityLog> interactionLogs = activityLogRepository.findAllByMobileNumberAndCreatedDateBetweenOrderByIdDesc(userInfo.getMobileNumber(), startDate, new Date());

        if (interactionLogs == null || interactionLogs.size() == 0) return Boolean.TRUE;
        logWriterUtility.debug(requestId, "Try Size :" + interactionLogs.size());

        for (int i = 0; i < interactionLogs.size(); i++) {
            UmActivityLog interactionLog1 = interactionLogs.get(i);

            if (interactionLog1.getLogType().equals(InteractionUtils.TYPE_SUCCESSFUL_LOGIN)) {
                break;
            } else {
                interactionLog = interactionLog1;
            }
        }

        if (interactionLog != null) {
            long diff = BLOCK_TIME - TimeUnit.MINUTES.convert(new Date().getTime() - interactionLog.getCreatedDate().getTime(), TimeUnit.MILLISECONDS);
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED, "" + diff, "" + diff);
        }
        return Boolean.TRUE;
    }

    @Override
    public String validatePasswordAndGenerateToken(String rawPassword, UmUserInfo umUserInfo, LoginRequest loginRequest, Boolean isNewDevice) throws PocketException, IOException {


        boolean isCredentialValid = true;


        if (!passwordEncoder.matches(rawPassword,umUserInfo.getPassword()))
            isCredentialValid = false;

        if (isCredentialValid) {


            if (isNewDevice) {
                return "";
            }

            UmUserDevice umUserDevice = userDeviceRepository.findFirstByHardwareSignature(loginRequest.getHardwareSignature());

            if(umUserDevice!=null){
                UmUserDeviceMapping umUserDeviceMapping = userDeviceMapRepository
                        .findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(umUserInfo, umUserDevice, "1");
                if(umUserDeviceMapping!=null){
                    umUserDeviceMapping.setFcmKey(loginRequest.getFcmKey());
                    userDeviceMapRepository.save(umUserDeviceMapping);
                }
            }

            String sessionToken = getToken(loginRequest.getLoginId(), loginRequest.getHardwareSignature(), loginRequest.getRequestId());


            logWriterUtility.trace(loginRequest.getRequestId(), "Generated session Token :" + sessionToken);

            changeCredentialStatus(loginRequest.getLoginId(), loginRequest.getRequestId(), umUserInfo, true);

            String message = generateLoginResultMessage(loginRequest.getHardwareSignature(), loginRequest.getDeviceName(), isNewDevice, true);

            commonService.addInteractionLog(loginRequest.getLoginId(), message, loginRequest.getHardwareSignature(), loginRequest.getDeviceName(),
                    UmEnums.ActivityLogType.LoginSuccessTry.getActivityLogType(), umUserInfo);


            return sessionToken;

        } else {
            logWriterUtility.trace(loginRequest.getRequestId(), "Pin/Password not matched with DB");
            //Insert Row
            // Get Interaction Log within 30 mins
            // Remove after Right try portion
            // Check wrong try Count>2 ? , Set UserStatus=B
            String message = generateLoginResultMessage(loginRequest.getHardwareSignature(), loginRequest.getDeviceName(), isNewDevice, false);

            commonService.addInteractionLog(loginRequest.getLoginId(), message, loginRequest.getHardwareSignature(), loginRequest.getDeviceName(),
                    UmEnums.ActivityLogType.LoginFailedTry.getActivityLogType(), umUserInfo);

            changeCredentialStatus(loginRequest.getLoginId(), loginRequest.getRequestId(), umUserInfo, false);

            throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
        }
    }


    @Override
    public Boolean checkIsNewDevice(UmUserInfo umUserInfo, LoginRequest loginRequest) {

        UmUserDevice umUserDevice = userDeviceRepository.findFirstByHardwareSignature(loginRequest.getHardwareSignature());
        if (umUserDevice == null) {
            return Boolean.TRUE;
        }

        UmUserDeviceMapping umUserDeviceMapping = userDeviceMapRepository.findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(umUserInfo, umUserDevice, "1");
        if (umUserDeviceMapping == null) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    @Override
    public LoginResponse generateLoginResponse(UmUserInfo umUserInfo, String requestId, Boolean isNewDevice, String sessionToken) {
        LoginResponse loginResponse = new LoginResponse();

        if (isNewDevice) {
            loginResponse.setDeviceVerificationNeeded(true);

            OtpObject otpObject = otpService.generateOtp(umUserInfo, requestId);
            loginResponse.setSecretKey(otpObject.getOtpSecret());
            return loginResponse;
        }

        loginResponse.setSessionToken(sessionToken);
        loginResponse.setDeviceVerificationNeeded(false);
        loginResponse.setFullName(umUserInfo.getFullName());
        loginResponse.setMobileNumber(umUserInfo.getMobileNumber());

        UmDocuments umDocuments=documentsDao.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);
        if(umDocuments!=null){
            logWriterUtility.trace(requestId,"Document Id :"+umDocuments.getId());
            loginResponse.setProfilePic("" + umDocuments.getDocumentPath());
        }

        loginResponse.setAccountType(umUserInfo.getGroupCode());

        List<UmGroupFunctionMapping> umGroupFunctionMapping = userGroupFunctionMapRepository.findAllByGroupCodeAndStatus(umUserInfo.getGroupCode(), "1");

        if (umGroupFunctionMapping != null && umGroupFunctionMapping.size() > 0) {
            List<String> functionCodes = umGroupFunctionMapping.stream()
                    .map(UmGroupFunctionMapping::getFunctionCode)
                    .collect(Collectors.toList());

            List<UmUserFunction> functions = userFunctionRepository
                    .findAllByFunctionCodeInAndFunctionStatus(functionCodes,"1");

            List<UserFunctionResponseItem> functionResponse = new ArrayList<>();

            for (UmUserFunction function : functions) {
                UserFunctionResponseItem tempFunction = new UserFunctionResponseItem();
                tempFunction.setFunctionCode(function.getFunctionCode());
                tempFunction.setFunctionName(function.getFunctionName());
                functionResponse.add(tempFunction);
            }
            loginResponse.setFunctions(functionResponse);
        }

        //Load setting

        UmUserSettings umUserSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(umUserInfo);

        if (umUserSettings != null) {
            UserSettingsResponse userSettingsResponse = new UserSettingsResponse();
            userSettingsResponse.setMobileNumberVerified(umUserSettings.getMobileNumberVerified());
            userSettingsResponse.setPrimaryIdVerified(umUserSettings.getPrimaryIdVerified());
            loginResponse.setSettingsResponse(userSettingsResponse);
        }

        return loginResponse;
    }

    @Override
    public LoginResponse doLoginWithOtp(LoginStepTwoRequest request) throws PocketException {


        OtpObject otpObject = new OtpObject(request.getOtpText(), request.getOtpSecret());

        UmUserInfo umUserInfo = otpService.verifyOtp(otpObject);

        UmUserDevice umUserDevice = userDeviceRepository.findFirstByHardwareSignature(request.getHardwareSignature());

        if (umUserDevice == null) {
            umUserDevice = new UmUserDevice();
            umUserDevice.setCreatedDate(new Date());
            umUserDevice.setDeviceName(request.getDeviceName());
            umUserDevice.setHardwareSignature(request.getHardwareSignature());
            umUserDevice.setMetaData(request.getHardwareSignature());
            userDeviceRepository.save(umUserDevice);
        }

        UmUserDeviceMapping umUserDeviceMapping = new UmUserDeviceMapping();
        umUserDeviceMapping.setCreatedDate(new Date());
        umUserDeviceMapping.setUmUserDeviceByDeviceId(umUserDevice);
        umUserDeviceMapping.setFcmKey(request.getFcmKey());
        umUserDeviceMapping.setUmUserInfoByUserId(umUserInfo);
        umUserDeviceMapping.setStatus("1");

        String message = "New device added successfully as trusted device. Device name :" + request.getDeviceName() + " device Id :" + request.getHardwareSignature();

        commonService.addInteractionLog(umUserInfo.getLoginId(), message, request.getHardwareSignature(), request.getDeviceName(),
                UmEnums.ActivityLogType.LoginSuccessTry.getActivityLogType(), umUserInfo);

        userDeviceMapRepository.save(umUserDeviceMapping);


        String sessionToken = getToken(umUserInfo.getMobileNumber(), request.getHardwareSignature(), request.getRequestId());


        LoginResponse loginResponse = generateLoginResponse(umUserInfo, request.getRequestId(), false, sessionToken);
        return loginResponse;
    }

    @Override
    public ForgotPinStepOneResponse forgotPinStepOneRequest(ForgotPinStepOneRequest request) throws PocketException {
        List<Integer> statusIn = new ArrayList<>();
        statusIn.add(UmEnums.UserStatus.Verified.getUserStatusType());

        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getMobileNumber(), statusIn);

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found using this loginId");
            throw new PocketException(PocketErrorCode.INVALID_LOGIN_CREDENTIAL);
        }


//        OtpObject otpObject = otpService.generateOtp(umUserInfo, request.getRequestId());  //Comment out this line after completing April/22 event.

        //Remove this after upcoming event on April/22 start
        OtpObject otpObject = generateOtpTemp(umUserInfo, request.getRequestId());
        //end

        // send otp as message

        //SMS off for customer registration and forget PIN. After completing April/22 event then comment on korba
//        CompletableFuture.runAsync(() -> {
//            String otpMessage = "Your otp is :" + otpObject.getOtpText();
//
//            new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(request.getMobileNumber(), otpMessage, ExternalType.SMS);
//        });



        String message = "Forgot Pin request from device :" + request.getDeviceName();

        commonService.addInteractionLog(umUserInfo.getLoginId(), message, request.getHardwareSignature(),
                request.getDeviceName(), UmEnums.ActivityLogType.ForgotPinInitiate.getActivityLogType(), umUserInfo);

        ForgotPinStepOneResponse forgotPinStepOneResponse = new ForgotPinStepOneResponse();
        forgotPinStepOneResponse.setOtpSecret(otpObject.getOtpSecret());

        return forgotPinStepOneResponse;
    }

    //Remove this after upcoming event on April/22 start
    public OtpObject generateOtpTemp(UmUserInfo umUserInfo, String requestId) {
        UmOtp umOtp = new UmOtp();
        umOtp.setCreatedDate(new Date());
        umOtp.setLoginId(umUserInfo.getLoginId());
        umOtp.setStatus("1");
        umOtp.setUmUserInfoByUserId(umUserInfo);

        RandomGenerator securityService = RandomGenerator.getInstance();
        umOtp.setOtpText(generatePINTemp(4,environment));
        umOtp.setOtpSecret(securityService.generateTokenAsString(12));

        userOTPRepository.save(umOtp);
        return new OtpObject(umOtp.getOtpText(), umOtp.getOtpSecret());
    }
    public String generatePINTemp(int numberOfDigits, Environment environment) {
        String randomOtp=environment.getProperty("RANDOM_OTP");
        if(randomOtp==null){
            randomOtp=String.format("%0" + numberOfDigits + "d", (new Random()).nextInt((int) Math.pow(10.0D, (double) numberOfDigits)));
        }
        return randomOtp;
    }
    //end

    @Override
    public boolean forgotPinStepTwoRequest(ForgotPinStepTwoRequest request) throws PocketException {

        OtpObject otpObject = new OtpObject(request.getOtpText(), request.getOtpSecret());

        UmUserInfo umUserInfo = otpService.verifyOtp(otpObject);

        if(umUserInfo==null){
            logWriterUtility.error(request.getRequestId(),"Otp not matched.");
            throw new PocketException(PocketErrorCode.OtpNotMatched);
        }

        String newPin = request.getNewPin();

        umUserInfo.setPassword(passwordEncoder.encode(newPin));

        userInfoRepository.save(umUserInfo);

        // update user settings to shouldChangePinPassword

        UmUserSettings umUserSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(umUserInfo);
        umUserSettings.setShouldChangeCredential("N");
        userSettingsRepository.save(umUserSettings);

        String message = "Your pin is reset successfully.";

        commonService.addInteractionLog(umUserInfo.getLoginId(), message, request.getHardwareSignature(), request.getDeviceName(), UmEnums.ActivityLogType.ForgotPinSuccess.getActivityLogType(), umUserInfo);

        return true;
    }

    private int changeCredentialStatus(String mobileNumber, String requestId, UmUserInfo userCredential, boolean isSuccessfullLogin) throws PocketException {


            if (UmEnums.UserStatus.Blocked.getUserStatusType() == (userCredential.getAccountStatus())) {
            userCredential.setAccountStatus((isSuccessfullLogin) ? UmEnums.UserStatus.Verified.getUserStatusType() : UmEnums.UserStatus.HardBlocked.getUserStatusType());
            userInfoRepository.save(userCredential);
            throw new PocketException(PocketErrorCode.AccountIsHardBlocked);
        }

        if (isSuccessfullLogin) {
            return 0;
        }

        int failedTryCountBeforeLastSuccessfulLogin = countNumberOfUnsuccessFulTry(mobileNumber, requestId);

        logWriterUtility.debug(requestId, "failedTryCountBeforeLastSuccessfulLogin :" + failedTryCountBeforeLastSuccessfulLogin);

        if (failedTryCountBeforeLastSuccessfulLogin > 2) {
            // Block That user
            userCredential.setAccountStatus(UmEnums.UserStatus.Blocked.getUserStatusType());
            userInfoRepository.save(userCredential);
            logWriterUtility.debug(requestId, "User " + userCredential.getId() + " is temporary Blocked Now.");
            throw new PocketException(PocketErrorCode.UserTemporarilyBlockedWithoutMessage, "" + BLOCK_TIME, "" + BLOCK_TIME);
        }
        return failedTryCountBeforeLastSuccessfulLogin;
    }

    private int countNumberOfUnsuccessFulTry(String mobileNumber, String requestId) {

        int failedTryCountBeforeLastSuccessfulLogin = 0;
        Date startDate = new Date(Calendar.getInstance().getTimeInMillis() - (BLOCK_TIME * ONE_MINUTE_IN_MILLIS));
        List<UmActivityLog> interactionLogs = activityLogRepository.findAllByMobileNumberAndCreatedDateBetweenOrderByIdDesc(mobileNumber, startDate, new Date());

        if (interactionLogs == null || interactionLogs.size() == 0) {
            logWriterUtility.debug(requestId, "Try Size :0");
            return failedTryCountBeforeLastSuccessfulLogin;
        }
        logWriterUtility.debug(requestId, "Try Size :" + interactionLogs.size());

        for (UmActivityLog interactionLog1 : interactionLogs) {

            if (interactionLog1.getLogType().equals(InteractionUtils.TYPE_SUCCESSFUL_LOGIN)) {
                break;
            }
            failedTryCountBeforeLastSuccessfulLogin++;

            if (failedTryCountBeforeLastSuccessfulLogin >= 2)
                break;
        }
        return failedTryCountBeforeLastSuccessfulLogin;
    }


    private String generateLoginResultMessage(String hardwareSignature, String deviceName, boolean isNewDevice, boolean isSuccessfulLogin) {
        String message = null;

        if (isSuccessfulLogin) {
            if (isNewDevice) {
                message = "Successful login from new device :" + deviceName + " hardwareSignature :" + hardwareSignature;
            } else {
                message = "Successful login from device :" + deviceName + " hardwareSignature :" + hardwareSignature;

            }
        } else {
            if (isNewDevice) {
                message = "Failed login from new device :" + deviceName + " hardwareSignature :" + hardwareSignature;
            } else {
                message = "Failed login from device :" + deviceName + " hardwareSignature :" + hardwareSignature;

            }
        }
        return message;
    }

    private String getToken(String credential, String hardwareSignature, String requestId) throws PocketException {
        TokenGenerateRequest request = new TokenGenerateRequest();
        request.setMobileNumber(credential);
        request.setHardwareSignature(hardwareSignature);
        request.setRequestId(requestId);

        TokenGenerateResponse tokenGenerateResponse =
                securityService.generateToken(request);
        if (tokenGenerateResponse.getError() != null) {
            logWriterUtility.error(requestId, "Cannot generate token");
            throw new PocketException(PocketErrorCode.INVALID_HARDWARE_SIGNATURE);
        }
        return tokenGenerateResponse.getToken();
    }
}
