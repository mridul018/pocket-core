package tech.ipocket.pocket.services.um.registration;

import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.utils.DateUtil;
import tech.ipocket.pocket.utils.PocketException;

public class CommonRegistration extends AbstractRegistration {

    private String nationality;
    private String email;
    private String deviceName;
    private String metaData;

    @Override
    public void setData(RegistrationRequest registrationRequest) {
        this.setFullName(registrationRequest.getFullName());
        this.setMobileNumber(registrationRequest.getMobileNo());
        this.setPassword(registrationRequest.getPassword());
        this.setGroupCode(registrationRequest.getGroupCode());
        this.setRequestId(registrationRequest.getRequestId());
        this.setNationality(registrationRequest.getNationality());
        this.setEmail(registrationRequest.getEmail());
        this.setDeviceName(registrationRequest.getDeviceName());
        this.setMetaData(registrationRequest.getMetaData());
        this.setHardwareSignature(registrationRequest.getHardwareSignature());
        if(registrationRequest.getDateOfBirth()!=null){
            try {
                this.setDateOfBirth(DateUtil.parseDate(registrationRequest.getDateOfBirth()));
            } catch (PocketException e) {
                e.printStackTrace();
            }
        }


        this.setCountryCode(registrationRequest.getCountryCode());
        this.setStateId(registrationRequest.getStateId());
        this.setNationality(registrationRequest.getNationality());
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }
}
