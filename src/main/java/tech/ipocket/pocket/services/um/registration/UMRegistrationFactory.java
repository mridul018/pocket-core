package tech.ipocket.pocket.services.um.registration;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.utils.GroupCodes;
import tech.ipocket.pocket.utils.PocketException;

@Component
public class UMRegistrationFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public RegistrationResponse doRegistration(RegistrationRequest registrationRequest, String type) throws PocketException {
       switch (type) {
            case GroupCodes.CUSTOMER:
                CustomerRegistration customerRegistration = new CustomerRegistration();

                //Remove this after upcoming event on April/22 start
                registrationRequest.setPrimaryIdNumber("123456789");
                registrationRequest.setPrimaryIdType("101");
                if(registrationRequest.getMobileNo().substring(0,3).equals("+97")){
                    registrationRequest.setNationality("UAE");
                    registrationRequest.setCountryCode("AE");
                    registrationRequest.setStateId("11");
                }else {
                    registrationRequest.setNationality("Bangladeshi");
                    registrationRequest.setCountryCode("BD");
                    registrationRequest.setStateId("1");
                }
                registrationRequest.setPrimaryIdIssueDate("20/11/2021");
                registrationRequest.setPrimaryIdExpiryDate("20/11/2024");
                // **end**

                customerRegistration.setData(registrationRequest);
                RegistrationService registrationService = applicationContext.getBean(RegistrationServiceImpl.class);
                return registrationService.customerRegistration(customerRegistration);
            case GroupCodes.AGENT:
                return registration(registrationRequest);
           case GroupCodes.MERCHANT:
                return registration(registrationRequest);

           case GroupCodes.SR:
               SrRegistration srRegistration = new SrRegistration();
               srRegistration.setData(registrationRequest);
               RegistrationService srRegistrationService = applicationContext.getBean(RegistrationServiceImpl.class);
               return srRegistrationService.srRegistration(srRegistration);

           case GroupCodes.ADMIN:
            case GroupCodes.SUPER_ADMIN:
                AdminRegistration adminRegistration = new AdminRegistration();
                adminRegistration.setData(registrationRequest);
                RegistrationService adminRegistrationService = applicationContext.getBean(RegistrationServiceImpl.class);
                return adminRegistrationService.adminRegistration(adminRegistration);

            default:
                CommonRegistration commonRegistration = new CommonRegistration();
                commonRegistration.setData(registrationRequest);
                RegistrationService commonRegistrationService = applicationContext.getBean(RegistrationServiceImpl.class);
                return commonRegistrationService.commonRegistration(commonRegistration);
        }
    }

    private RegistrationResponse registration(RegistrationRequest request) throws PocketException {
        MerchantAgentRegistration merchantAgentRegistration = new MerchantAgentRegistration();
        merchantAgentRegistration.setData(request);
        RegistrationService registrationService = applicationContext.getBean(RegistrationServiceImpl.class);
        return registrationService.merchantAgentRegistration(merchantAgentRegistration);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
