package tech.ipocket.pocket.services.um;

import tech.ipocket.pocket.entity.UmExternalActivityLog;
import tech.ipocket.pocket.entity.UmUserInfo;

import java.math.BigDecimal;
import java.sql.Timestamp;

public interface CommonService {
    void addInteractionLog(String loginId, String message, String hardwareSignature, String deviceName,
                           String logType, UmUserInfo userInfo);
    UmExternalActivityLog addExternalActivityPreLog(
            String type,
            String country,
            String sender,
            String topupReceiver,
            String operator,
            String connection,
            String billPayCustomerName,
            String billPayReceiverNumber,
            String billNo,
            String accountNo,
            BigDecimal amount,
            Integer status,
            String message,
            Timestamp datetime,
            String requestUrl,
            Boolean requestDirectionIn,
            Boolean requestDirectionOut,
            String communicationMedium,
            String ref_id);

    void addExternalActivityPostLog(
            int id,
            String type,
            String country,
            String sender,
            String topupReceiver,
            String operator,
            String connection,
            String billPayCustomerName,
            String billPayReceiverNumber,
            String billNo,
            String accountNo,
            BigDecimal amount,
            Integer status,
            String message,
            Timestamp datetime,
            String requestUrl,
            Boolean requestDirectionIn,
            Boolean requestDirectionOut,
            String communicationMedium,
            String ref_id
    );
}
