package tech.ipocket.pocket.services.um;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.DeviceListItem;
import tech.ipocket.pocket.response.um.DeviceListResponseRoot;
import tech.ipocket.pocket.response.um.ProfilePictureUpdateResponse;
import tech.ipocket.pocket.response.um.UserDetailsResponseResponse;
import tech.ipocket.pocket.utils.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProfileServiceImpl implements ProfileService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserDeviceRepository userDeviceRepository;

    @Autowired
    private UserDocumentRepository userDocumentRepository;

    @Autowired
    private Environment environment;
    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private UserActivityLogRepository activityLogRepository;
    @Autowired
    private UserGroupRepository userGroupRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private UmUserBankMappingRepository userBankMapRepository;
    @Autowired
    private UmStateRepository umStateRepository;
    @Autowired
    private UmSrAgentMappingRepository umSrAgentMappingRepository;

    @Override
    public TokenValidateUmResponse getUserInfo(GetUserInfoRequest request) throws PocketException {

        List<Integer> statusIn = new ArrayList<>();
        statusIn.add(Integer.parseInt(TsEnums.UserStatus.INITIALIZED.getUserStatusType()));
        statusIn.add(Integer.parseInt(TsEnums.UserStatus.ACTIVE.getUserStatusType()));

//      UmUserInfo umUserInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getMobileNumber(), null);
        UmUserInfo umUserInfo = userInfoRepository.findByMobileNumberAndAccountStatusIn(request.getMobileNumber(), statusIn);

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        TokenValidateUmResponse validateResponse = new TokenValidateUmResponse();
        validateResponse.setGroupCode(umUserInfo.getGroupCode());
        validateResponse.setMobileNumber(umUserInfo.getMobileNumber());
        validateResponse.setName(umUserInfo.getFullName());
        validateResponse.setPhotoId("" + umUserInfo.getPhotoId());
        return validateResponse;
    }

    @Override
    public BaseResponseObject getUserDevices(TokenValidateUmResponse validateResponse, EmptyRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(validateResponse.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        List<String> statusIn = new ArrayList<>();
        statusIn.add(UmEnums.DeviceMapStatus.Active.getMapStatus());
        List<UmUserDeviceMapping> maps = userDeviceMapRepository.findAllByUmUserInfoByUserIdAndStatusIn(umUserInfo, statusIn);

        if (maps == null || maps.size() == 0) {
            return baseResponseObject;
        }

        DeviceListResponseRoot listResponseRoot = new DeviceListResponseRoot();

        maps.forEach(map -> {
            DeviceListItem deviceListItem = new DeviceListItem();
            deviceListItem.setDeviceName(map.getUmUserDeviceByDeviceId().getDeviceName());
            deviceListItem.setHardwareSignature(map.getUmUserDeviceByDeviceId().getHardwareSignature());
            listResponseRoot.getDevices().add(deviceListItem);
        });
        baseResponseObject.setData(listResponseRoot);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateFcmKey(TokenValidateUmResponse validateResponse, UpdateFcmKeyRequest request) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(validateResponse.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        UmUserDevice umUserDevice = userDeviceRepository.findFirstByHardwareSignature(request.getHardwareSignature());
        if (umUserDevice == null) {
            logWriterUtility.error(request.getRequestId(), "User device found");
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        UmUserDeviceMapping userDeviceMap = userDeviceMapRepository.findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(umUserInfo, umUserDevice, UmEnums.DeviceMapStatus.Active.getMapStatus());

        if (userDeviceMap == null) {
            logWriterUtility.error(request.getRequestId(), "User device found");
            throw new PocketException(PocketErrorCode.DEVICE_NOT_FOUND);
        }

        userDeviceMap.setFcmKey(request.getFcmKey());
        userDeviceMapRepository.save(userDeviceMap);

        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateUserDocuments(TokenValidateUmResponse validateResponse, UpdateDocumentRequest request) throws PocketException {

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(request.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        for (DocumentItem documentItem : request.getDocumentList()) {

            UmDocuments umDocuments = new UmDocuments();

            if (documentItem.getDocumentType().equals(UmEnums.DocumentTypes.ProfilePicture.getDocumentType())) {
                String documentPath = saveDocumentsToDisk(documentItem.getDocumentAsString(), ".png", request.getRequestId());
                umDocuments.setCreatedDate(new Date());
                umDocuments.setDocumentType(documentItem.getDocumentType());
                umDocuments.setStatus("1");
                umDocuments.setUmUserInfoByUserId(umUserInfo);
                umDocuments.setDocumentPath(documentPath);
                userDocumentRepository.save(umDocuments);

                umUserInfo.setPhotoId(umDocuments.getId());
                userInfoRepository.save(umUserInfo);
            }
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getUserDetails(GetUserInfoRequest request) throws PocketException {

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(request.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }


        UmUserDetails userDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);
        UmUserGroup umUserGroup = userGroupRepository.findByGroupCodeAndGroupStatus(umUserInfo.getGroupCode(), "1");

        UserDetailsResponseResponse response = new UserDetailsResponseResponse();

        if(umUserInfo.getDob()!=null){
            response.setDateOfBirth(DateUtil.parseDate(umUserInfo.getDob()));
        }
        response.setUserId(umUserInfo.getId());
        response.setFullName(umUserInfo.getFullName());
        response.setEmail(umUserInfo.getEmail());
        response.setMobileNo(umUserInfo.getMobileNumber());
        response.setMobileNumber(umUserInfo.getMobileNumber());
        response.setUserStatus("" + umUserInfo.getAccountStatus());

        response.setPresentAddress(userDetails.getPresentAddress());
        response.setPermanentAddress(userDetails.getPermanentAddress());


        if (umUserInfo.getGroupCode().equalsIgnoreCase(UmEnums.UserGroup.AGENT.getGroupCode()) ||
                umUserInfo.getGroupCode().equalsIgnoreCase(UmEnums.UserGroup.MERCHANT.getGroupCode())) {
            response.setMerchantName(umUserInfo.getFullName());
        }
        if (umUserGroup != null) {
            response.setGroupCode(umUserGroup.getGroupCode());
            response.setGroupName(umUserGroup.getGroupName());
        }

        response.setGender(umUserInfo.getGender());


        if (userDetails != null) {

            response.setPostCode(userDetails.getPostCode());
            response.setCountryCode(userDetails.getCountryCode());

            response.setPrimaryIdIssueDate(userDetails.getPrimaryIdIssueDate().toString());
            response.setPrimaryIdExpiryDate(userDetails.getPrimaryIdExpiryDate().toString());

            response.setNationality(userDetails.getNationality());
            response.setMetaData("");
            response.setApplicantName(userDetails.getApplicantsName());
            response.setApplicantsName(userDetails.getApplicantsName());
            response.setPrimaryIdNumber(userDetails.getPrimaryIdNumber());
//            response.setPrimaryIdType(UmEnums.PrimaryIdTypes.getPrimaryIdTypesByCode(userDetails.getPrimaryIdType()));
            response.setPrimaryIdType((userDetails.getPrimaryIdType()));

            response.setNid(userDetails.getNidNo());
            response.setTradeLicense(userDetails.getTradeLicenseNo());
            response.setTradeLicenseNo(userDetails.getTradeLicenseNo());

            UmDocuments nidDocuments = userDocumentRepository.findFirstByUmUserInfoByUserIdAndAndDocumentTypeOrderByIdDesc(umUserInfo, UmEnums.DocumentTypes.NID.getDocumentType());
            if (nidDocuments != null) {
                response.setNidUrl(nidDocuments.getDocumentPath());
            }

            UmDocuments tradeLicenseDocuments = userDocumentRepository.findFirstByUmUserInfoByUserIdAndAndDocumentTypeOrderByIdDesc(umUserInfo, UmEnums.DocumentTypes.TRADE_LICENSE.getDocumentType());
            if (tradeLicenseDocuments != null) {
                response.setTradeLicenseUrl(tradeLicenseDocuments.getDocumentPath());
            }

            response.setSecondContactMobileNo(userDetails.getSecondContactMobileNo());
            response.setSecondContactName(userDetails.getSecondContactName());

            response.setThirdContactMobileNo(userDetails.getThirdContactMobileNo());
            response.setThirdContactName(userDetails.getThirdContactName());

            if (userDetails.getStateId() != null) {
                UmState umState = umStateRepository.findFirstById(Integer.parseInt(userDetails.getStateId()));
                if (umState != null) {
                    response.setStateId(""+umState.getId());
                }

            }
        }

        UmSrAgentMapping umSrAgentMapping = umSrAgentMappingRepository.findFirstByAgentIdAndStatus(umUserInfo.getId(), "1");
        if (umSrAgentMapping != null) {

            Optional<UmUserInfo> srWalletInfo = userInfoRepository.findById(umSrAgentMapping.getSrId());

            srWalletInfo.ifPresent(userInfo -> response.setSrWallet(userInfo.getMobileNumber()));

        }



        UmDocuments profilePicDocuments = userDocumentRepository.findFirstByUmUserInfoByUserIdAndAndDocumentTypeOrderByIdDesc(umUserInfo, UmEnums.DocumentTypes.ProfilePicture.getDocumentType());
        if (profilePicDocuments != null) {
            response.setPhotoUrl(profilePicDocuments.getDocumentPath());
        }

        UmUserBankMapping umUserBankMapping = userBankMapRepository.findLastByUserIdOrderByIdDesc(umUserInfo.getId());

        if (umUserBankMapping != null) {
            response.setAccountName(umUserBankMapping.getAccountName());
            response.setAccountNumber(umUserBankMapping.getAccountNumber());
            response.setBankCode(umUserBankMapping.getBankCode());
            response.setBankRoutingNo(umUserBankMapping.getBankRoutingNo());
            response.setBranchSwiftCode(umUserBankMapping.getBranchSwiftCode());
            response.setBankAccNo(umUserBankMapping.getAccountNumber());
            response.setBankName(umUserBankMapping.getBankName());
            response.setBranchName(umUserBankMapping.getBranchName());
        }


        UmUserSettings umUserSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(umUserInfo);
        if (umUserSettings != null) {
            response.setPrimaryIdVerified(umUserSettings.getPrimaryIdVerified());
            response.setMobileNumberVerified(umUserSettings.getMobileNumberVerified());
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(response);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateProfilePicture(TokenValidateUmResponse validateResponse, ProfilePicUpdateRequest request) throws PocketException {
        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(validateResponse.getMobileNumber());
        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        UmDocuments umDocuments = new UmDocuments();

        String documentPath = saveDocumentsToDisk(request.getDocumentAsString(), "jpg", request.getRequestId());
        umDocuments.setCreatedDate(new Date());
        umDocuments.setDocumentType(request.getDocumentType());
        umDocuments.setStatus("1");
        umDocuments.setUmUserInfoByUserId(umUserInfo);
        umDocuments.setDocumentPath(documentPath);
        userDocumentRepository.save(umDocuments);

        umUserInfo.setPhotoId(umDocuments.getId());
        userInfoRepository.save(umUserInfo);

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(request.getHardwareSignature());
        log.setLogType(ActivityLogType.UPDATE_PROFILE_PICTURE.getKeyString());
        log.setMetaData("Update profile picture done");
        log.setMobileNumber(umUserInfo.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(umUserInfo);

        activityLogRepository.save(log);


        ProfilePictureUpdateResponse updateResponse = new ProfilePictureUpdateResponse();
        updateResponse.setDocumentName(documentPath);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(updateResponse);
        return baseResponseObject;
    }

    private String saveDocumentsToDisk(String document, String extension, String requestId) throws PocketException {

        String home = System.getProperty("user.home");

        String target = home + "" + environment.getProperty("file_base_path");
        String fileName = System.currentTimeMillis() + "." + extension;


        File file = new File(target);
        if (!file.exists()) {
            System.out.print("No Directory");
            file.mkdir();
            System.out.print("Folder created");
        } else {
            logWriterUtility.debug(requestId, " Exists :" + target + "\".");
        }

        byte[] imageByte = Base64.decodeBase64(document);

        String directory = target + "/" + fileName;

        logWriterUtility.debug(requestId, "Saving File :" + directory);

        try {
            new FileOutputStream(directory).write(imageByte);
        } catch (IOException e) {
            logWriterUtility.error(requestId, e);
            throw new PocketException(PocketErrorCode.IOException, e.getMessage());
        }

        return fileName;
    }
}
