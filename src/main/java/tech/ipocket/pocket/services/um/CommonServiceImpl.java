package tech.ipocket.pocket.services.um;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmActivityLog;
import tech.ipocket.pocket.entity.UmExternalActivityLog;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UmExternalActivityLogRepository;
import tech.ipocket.pocket.repository.um.UserActivityLogRepository;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.UmEnums;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Service
public class CommonServiceImpl implements CommonService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserActivityLogRepository activityLogRepository;

    @Autowired
    private UmExternalActivityLogRepository externalActivityLogRepository;

    @Override
    public void addInteractionLog(String loginId, String message,
                                  String hardwareSignature, String deviceName, String logType,
                                  UmUserInfo userInfo) {
        UmActivityLog umActivityLog = new UmActivityLog();
        umActivityLog.setCreatedDate(new Date());
        umActivityLog.setLogType(logType);
        umActivityLog.setDeviceInfo(hardwareSignature);
        umActivityLog.setMobileNumber(loginId);
        umActivityLog.setMetaData(message);
        umActivityLog.setStatus(UmEnums.ActivityLogStatus.Initialize.getActivityLogStatus());
        umActivityLog.setUmUserInfoByUserId(userInfo);

        activityLogRepository.save(umActivityLog);

    }

    @Override
    public UmExternalActivityLog addExternalActivityPreLog(String type, String country, String sender, String topupReceiver,
                                                           String operator, String connection, String name, String billPayNumber, String billNo, String accountNo,
                                                           BigDecimal amount, Integer status, String message, Timestamp datetime,
                                                           String requestUrl, Boolean requestDirectionIn,
                                                           Boolean requestDirectionOut, String communicationMedium, String ref_id) {

        try {
            UmExternalActivityLog externalActivityLog = new UmExternalActivityLog();

            externalActivityLog.setType(type);
            externalActivityLog.setCountry(country);
            externalActivityLog.setSender(sender);
            externalActivityLog.setTopupReceiver(topupReceiver);
            externalActivityLog.setOperator(operator);
            externalActivityLog.setConnection(connection);
            externalActivityLog.setBillPayReceiver(billPayNumber);
            externalActivityLog.setBillNo(billNo);
            externalActivityLog.setAccountNo(accountNo);
            externalActivityLog.setAmount(amount);
            externalActivityLog.setStatus(status);
            externalActivityLog.setMessage(message);
            externalActivityLog.setDatetime(datetime);
            externalActivityLog.setRequestUrl(requestUrl);
            externalActivityLog.setRequestDirectionIn(requestDirectionIn);
            externalActivityLog.setRequestDirectionOut(requestDirectionOut);
            externalActivityLog.setCommunicationMedium(communicationMedium);
            externalActivityLog.setBillPayCustomerName(name);
            externalActivityLog.setRefId(ref_id);


            return externalActivityLogRepository.save(externalActivityLog);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;

    }

    @Override
    public void addExternalActivityPostLog(int id, String type, String country, String sender, String topupReceiver,
                                           String operator, String connection, String name, String billPayNumber,String billNo, String accountNo,
                                           BigDecimal amount, Integer status, String message, Timestamp datetime,
                                           String requestUrl, Boolean requestDirectionIn,
                                           Boolean requestDirectionOut, String communicationMedium, String ref_id) {
        try {
            UmExternalActivityLog externalActivityLog = new UmExternalActivityLog();

            externalActivityLog.setId(id);
            externalActivityLog.setType(type);
            externalActivityLog.setCountry(country);
            externalActivityLog.setSender(sender);
            externalActivityLog.setTopupReceiver(topupReceiver);
            externalActivityLog.setOperator(operator);
            externalActivityLog.setConnection(connection);
            externalActivityLog.setBillPayReceiver(billPayNumber);
            externalActivityLog.setBillNo(billNo);
            externalActivityLog.setAccountNo(accountNo);
            externalActivityLog.setAmount(amount);
            externalActivityLog.setStatus(status);
            externalActivityLog.setMessage(message);
            externalActivityLog.setDatetime(datetime);
            externalActivityLog.setRequestUrl(requestUrl);
            externalActivityLog.setRequestDirectionIn(requestDirectionIn);
            externalActivityLog.setRequestDirectionOut(requestDirectionOut);
            externalActivityLog.setCommunicationMedium(communicationMedium);
            externalActivityLog.setBillPayCustomerName(name);
            externalActivityLog.setRefId(ref_id);


            externalActivityLogRepository.save( externalActivityLog);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
