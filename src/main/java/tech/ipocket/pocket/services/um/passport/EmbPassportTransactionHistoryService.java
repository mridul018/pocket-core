package tech.ipocket.pocket.services.um.passport;

import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.passport.EmbPassportTransactionHistoryRequest;
import tech.ipocket.pocket.request.um.passport.EmbPassportTrxReportRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface EmbPassportTransactionHistoryService {
    BaseResponseObject trxHistoryReport(EmbPassportTransactionHistoryRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject trxHistoryReportList(EmptyRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject searchTrxHistoryReportByDateRange(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByToken(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByQrCodeId(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByPType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportBySubType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByPassportId(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByPayMode(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByWalletNo(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject searchTrxHistoryReportByEmirateId(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject loadAllTokenByTransactionType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllUniqueQrCode(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllPassportType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllPassportSubType(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllMrpPassport(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllPayMode(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllSenderWallet(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject loadAllEmirateID(EmbPassportTrxReportRequest request, TsSessionObject tsSessionObject) throws PocketException;


}
