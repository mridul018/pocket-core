package tech.ipocket.pocket.services.um.device;

import tech.ipocket.pocket.entity.UmActivityLog;
import tech.ipocket.pocket.request.um.GetActivityLogRequest;
import tech.ipocket.pocket.request.um.UserDeviceListUpdateRequest;
import tech.ipocket.pocket.response.um.ActivityLogResponse;
import tech.ipocket.pocket.response.um.UserDeviceListUpdateResponse;
import tech.ipocket.pocket.response.um.UserDeviceResponse;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;
import java.util.List;

public interface UserDeviceService {
    ArrayList<UserDeviceResponse> getAllUserDevice(String loginId) throws PocketException;

    UserDeviceListUpdateResponse updateUserDevice(UserDeviceListUpdateRequest request) throws PocketException;

    List<ActivityLogResponse> getActivityLog(GetActivityLogRequest request) throws PocketException;
}
