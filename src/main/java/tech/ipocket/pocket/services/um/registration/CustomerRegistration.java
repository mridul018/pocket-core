package tech.ipocket.pocket.services.um.registration;

import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.utils.DateUtil;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Date;

public class CustomerRegistration extends AbstractRegistration {

    private String primaryIdNumber; // m
    private Date primaryIdIssueDate; // m
    private Date primaryIdExpiryDate; // m
    private String primaryIdType; //m
    private String nationality; // m
    private String deviceName;
    private String metaData;
    private String srWallet;

    @Override
    public void setData(RegistrationRequest registrationRequest) throws PocketException {
        this.setFullName(registrationRequest.getFullName());
        this.setMobileNumber(registrationRequest.getMobileNo());
        this.setPassword(registrationRequest.getPassword());
        this.setGroupCode(registrationRequest.getGroupCode());
        this.setRequestId(registrationRequest.getRequestId());
        this.setPrimaryIdNumber(registrationRequest.getPrimaryIdNumber());
        if(registrationRequest.getDateOfBirth()!=null){
            try {
                this.setDateOfBirth(DateUtil.parseDate(registrationRequest.getDateOfBirth()));
            } catch (PocketException e) {
                e.printStackTrace();
            }
        }

        if(registrationRequest.getPrimaryIdNumber()==null){
            throw new PocketException(PocketErrorCode.PrimayIdNumberRequired);
        }

        if(registrationRequest.getPrimaryIdIssueDate()==null){
            throw new PocketException(PocketErrorCode.PrimayIdNumberIssueDateRequired);
        }


        if(registrationRequest.getPrimaryIdExpiryDate()==null){
            throw new PocketException(PocketErrorCode.PrimayIdNumberExpiryDateRequired);
        }

        this.setPrimaryIdIssueDate(DateUtil.parseDate(registrationRequest.getPrimaryIdIssueDate()));
        this.setPrimaryIdExpiryDate(DateUtil.parseDate(registrationRequest.getPrimaryIdExpiryDate()));

        this.setPrimaryIdType(registrationRequest.getPrimaryIdType());
        this.setDeviceName(registrationRequest.getDeviceName());
        this.setMetaData(registrationRequest.getMetaData());
        this.setHardwareSignature(registrationRequest.getHardwareSignature());


        this.setCountryCode(registrationRequest.getCountryCode());
        this.setStateId(registrationRequest.getStateId());
        this.setNationality(registrationRequest.getNationality());
        this.setSrWallet(registrationRequest.getSrWallet());
    }

    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    public Date getPrimaryIdIssueDate() {
        return primaryIdIssueDate;
    }

    public void setPrimaryIdIssueDate(Date primaryIdIssueDate) {
        this.primaryIdIssueDate = primaryIdIssueDate;
    }

    public Date getPrimaryIdExpiryDate() {
        return primaryIdExpiryDate;
    }

    public void setPrimaryIdExpiryDate(Date primaryIdExpiryDate) {
        this.primaryIdExpiryDate = primaryIdExpiryDate;
    }

    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public String getSrWallet() {
        return srWallet;
    }

    public void setSrWallet(String srWallet) {
        this.srWallet = srWallet;
    }
}
