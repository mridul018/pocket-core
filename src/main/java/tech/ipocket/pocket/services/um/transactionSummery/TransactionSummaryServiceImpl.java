package tech.ipocket.pocket.services.um.transactionSummery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmDocuments;
import tech.ipocket.pocket.entity.UmUserDetails;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserDetailsRepository;
import tech.ipocket.pocket.repository.um.UserDocumentRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.request.um.TransactionSummaryViewRequest;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.services.external.ExternalService;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.UmEnums;

@Service
public class TransactionSummaryServiceImpl implements TransactionSummaryService {

    static LogWriterUtility logWriterUtility = new LogWriterUtility(TransactionSummaryServiceImpl.class);

    private UserInfoRepository dmUserCredentialDao;

    private UserDetailsRepository userDetailsDao;
    private UserDocumentRepository documentDao;
    private ExternalService externalApiCallService;
//    private UserCommonServices userCommonServices;


    @Autowired
    public TransactionSummaryServiceImpl(UserInfoRepository dmUserCredentialDao, UserDetailsRepository userDetailsDao, UserDocumentRepository documentDao, ExternalService externalApiCallService) {
        this.dmUserCredentialDao = dmUserCredentialDao;
        this.userDetailsDao = userDetailsDao;
        this.documentDao = documentDao;
        this.externalApiCallService = externalApiCallService;
    }

    @Override
    public UmUserInfo checkIsValidSender(Integer credentialId, String requestId) throws PocketException {
        UmUserInfo umUserInfo = dmUserCredentialDao.findFirstById(credentialId);
        if (umUserInfo == null || UmEnums.UserStatus.Deleted.getUserStatusType()==(umUserInfo.getAccountStatus())) {
            logWriterUtility.error(requestId, "Sender Active Wallet not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        if (umUserInfo.getAccountStatus()!=(UmEnums.UserStatus.Verified.getUserStatusType())) {
            logWriterUtility.error(requestId, "Invalid User Account Status :" + umUserInfo.getAccountStatus());
            throw new PocketException(PocketErrorCode.InvalidSenderAccountStatus);
        }
        return umUserInfo;
    }

    @Override
    public ReceiverInfo checkIsValidReceiver(String receiverMobileNumber, String requestId,  boolean needToCheckMposOrNot) throws PocketException {


        if (receiverMobileNumber == null) {
            logWriterUtility.debug(requestId, "Receiver Mobile Number not found");
            throw new PocketException(PocketErrorCode.InvalidInput);
        }

        UmUserInfo umUserInfo = dmUserCredentialDao.findByLoginId(receiverMobileNumber);
        if (umUserInfo == null) {
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        Integer[] statusNotIn = new Integer[2];
        statusNotIn[0] = UmEnums.UserStatus.Deleted.getUserStatusType();
        statusNotIn[1] = UmEnums.UserStatus.Initiation.getUserStatusType();


        logWriterUtility.trace(requestId, "Receiver Wallet found.");


        boolean isReceiverEligibleForTransfer = umUserInfo.getAccountStatus()==(UmEnums.UserStatus.Verified.getUserStatusType());
        if (!isReceiverEligibleForTransfer) {
            logWriterUtility.error(requestId, "Invalid Receiver Account Status :" + umUserInfo.getAccountStatus());
            throw new PocketException(PocketErrorCode.InvalidReceiverAccountStatus);
        }


        UmUserDetails umUserDetails = userDetailsDao.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);
        if (umUserDetails == null) {
            logWriterUtility.debug(requestId, "Receiver Mobile Number not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }
        ReceiverInfo receiverInfo = new ReceiverInfo();
        receiverInfo.setFullName(umUserInfo.getFullName());

        UmDocuments documents = documentDao.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);

        if (documents != null) {
            receiverInfo.setProfileImageUrl(documents.getDocumentPath());
        }

        return receiverInfo;
    }

    @Override
    public ReceiverInfo checkIsValidReceiverBillpay(String receiverMobileNumber, String requestId, boolean needToCheckMposOrNot) throws PocketException {
        return new ReceiverInfo();
    }

    @Override
    public TransactionSummaryViewResponse callTxForTransactionSummary(TransactionSummaryViewRequest transactionSummaryRequest)
            throws PocketException {

        TransactionValidityRequest request=new TransactionValidityRequest();
        request.setSenderMobileNumber(transactionSummaryRequest.getMobileNumber());
        request.setReceiverMobileNumber(transactionSummaryRequest.getReceiverMobileNumber());
        request.setTransferAmount(""+transactionSummaryRequest.getAmount());
        request.setType(""+transactionSummaryRequest.getPurpose());
        request.setRequestId(transactionSummaryRequest.getRequestId());
        request.setHardwareSignature(transactionSummaryRequest.getHardwareSignature());
        request.setSessionToken(transactionSummaryRequest.getSessionToken());
        request.setSource(transactionSummaryRequest.getSource());
        request.setCurrencyCode(transactionSummaryRequest.getCurrencyCode());
        request.setCountryCode(transactionSummaryRequest.getCountryCode());

        return externalApiCallService.callTxForTransactionVisibility(request);
    }
    
}
