package tech.ipocket.pocket.services.um.transactionSummery;

import tech.ipocket.pocket.request.um.TransactionSummaryViewRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

import java.util.concurrent.ExecutionException;

public class TransactionSummaryForCashInFromBank extends AbstractTransactionSummary {

    public TransactionSummaryForCashInFromBank(TransactionSummaryService transactionSummaryService) {
        super(transactionSummaryService);
    }

    @Override
    public BaseResponseObject getTransactionSummary(TransactionSummaryViewRequest transactionSummaryViewRequest) throws PocketException, ExecutionException, InterruptedException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();

        ReceiverInfo receiverInfo= checkIsValidReceiver(transactionSummaryViewRequest.getReceiverMobileNumber(), transactionSummaryViewRequest.getRequestId());


        // TODO: 2019-05-08 work here
        TransactionSummaryViewResponse transactionSummary= callTxForTransactionSummary(transactionSummaryViewRequest);


        TransactionSummaryViewResponse transactionSummaryViewResponse = new TransactionSummaryViewResponse();
        transactionSummaryViewResponse.setReceiverName(receiverInfo.getFullName());
        transactionSummaryViewResponse.setProfileImageUrl(receiverInfo.getProfileImageUrl());

        transactionSummaryViewResponse.setReceiverMobileNumber(transactionSummaryViewRequest.getReceiverMobileNumber());
        transactionSummaryViewResponse.setFee(transactionSummary.getFee());
        transactionSummaryViewResponse.setFeePayer(transactionSummary.getFeePayer());
        baseResponseObject.setData(transactionSummaryViewResponse);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(transactionSummaryViewRequest.getRequestId());

        return baseResponseObject;
    }


}