package tech.ipocket.pocket.services;

import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.um.GetNotificationRequestByWalletRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface NotificationService {
    void sendNotification(NotificationRequest notificationRequest);

    BaseResponseObject getUserNotificationByWallet(GetNotificationRequestByWalletRequest request, String walletNumber) throws PocketException;
}
