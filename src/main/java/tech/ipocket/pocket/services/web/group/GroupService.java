package tech.ipocket.pocket.services.web.group;

import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.web.GroupAndFunctionMapResponse;
import tech.ipocket.pocket.response.web.GroupCreateOrUpdateResponse;
import tech.ipocket.pocket.utils.PocketException;

import java.util.List;

public interface GroupService {
    GroupCreateOrUpdateResponse createOrUpdateOrDeleteGroup(GroupRequest request) throws PocketException;

    List<GroupAndFunctionMapResponse> getAllGroupsWithFunctions() throws PocketException;
    List<UmUserGroup> getCommonUserRegistrationGroup() throws PocketException;

}
