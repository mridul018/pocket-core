package tech.ipocket.pocket.services.web.group;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmGroupFunctionMapping;
import tech.ipocket.pocket.entity.UmUserFunction;
import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.repository.web.UserFunctionRepository;
import tech.ipocket.pocket.repository.web.UserGroupFunctionMapRepository;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.web.GroupAndFunctionMapResponse;
import tech.ipocket.pocket.response.web.GroupCreateOrUpdateResponse;
import tech.ipocket.pocket.response.web.UserFunctionResponse;
import tech.ipocket.pocket.utils.CommonTasks;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private UserGroupFunctionMapRepository functionMapRepository;

    @Autowired
    private UserFunctionRepository userFunctionRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public GroupCreateOrUpdateResponse createOrUpdateOrDeleteGroup(GroupRequest request) throws PocketException {
        GroupCreateOrUpdateResponse response = new GroupCreateOrUpdateResponse();

        UmUserGroup userGroup = userGroupRepository.findByGroupCodeAndGroupStatus(request.getGroupCode(), "1");
        if (userGroup != null) {
            // edit or update group

            return editOrUpdateGroup(userGroup, request);
        }

        userGroup = new UmUserGroup();

        userGroup.setGroupName(request.getGroupName());
        userGroup.setGroupCode(request.getGroupCode());
        userGroup.setDescription(request.getDescription());
        userGroup.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        userGroup.setGroupStatus(request.getGroupStatus());

        UmUserGroup newGroup = userGroupRepository.saveAndFlush(userGroup);

        response.setGroupId(newGroup.getId());
        response.setMessage("Group create successfully done");
        return response;
    }

    private GroupCreateOrUpdateResponse editOrUpdateGroup(UmUserGroup userGroup, GroupRequest request) throws PocketException {
        GroupCreateOrUpdateResponse response = new GroupCreateOrUpdateResponse();

        if (request.getGroupStatus() != null && request.getGroupStatus().equalsIgnoreCase("0")) {
            // deleting group

            Long userCount = userInfoRepository.countAllByGroupCode(userGroup.getGroupCode());

            if (userCount != null && userCount > 0) {
                throw new PocketException(PocketErrorCode.GroupIsAssignedToUser);
            }
        }

        userGroup.setDescription(request.getDescription());
        userGroup.setGroupStatus(request.getGroupStatus());
        userGroupRepository.save(userGroup);

        response.setMessage("Group updated successfully");

        return response;
    }

    @Override
    public List<GroupAndFunctionMapResponse> getAllGroupsWithFunctions() throws PocketException {

        List<UmUserGroup> groupList = userGroupRepository.findAllByGroupStatus("1");

        if (groupList == null || groupList.size() == 0) {
            return null;
        }

        List<GroupAndFunctionMapResponse> finalResponse = new ArrayList<>();

        for (UmUserGroup group : groupList) {

            GroupAndFunctionMapResponse listItem = new GroupAndFunctionMapResponse();
            listItem.setGroupCode(group.getGroupCode());
            listItem.setGroupName(group.getGroupName());
            listItem.setGroupDescription(group.getDescription());

            List<UmGroupFunctionMapping> mappings = functionMapRepository.findAllByGroupCodeAndStatus(group.getGroupCode(), "1");

            List<UserFunctionResponse> functionResponses = new ArrayList<>();
            if (mappings != null && mappings.size() > 0) {

                List<String> functionCodesList = mappings.stream()
                        .map(UmGroupFunctionMapping::getFunctionCode)
                        .collect(Collectors.toList());
                List<UmUserFunction> functions = userFunctionRepository.findAllByFunctionCodeInAndFunctionStatus(functionCodesList, "1");

                functionResponses = new ArrayList<>();

                for (UmUserFunction function : functions) {
                    UserFunctionResponse item = new UserFunctionResponse();
                    item.setId(function.getId());
                    item.setFunctionName(function.getFunctionName());
                    item.setFunctionCode(function.getFunctionCode());
                    functionResponses.add(item);
                }
            }
            listItem.setFunctions(functionResponses);

            finalResponse.add(listItem);
        }
        return finalResponse;
    }

    @Override
    public List<UmUserGroup> getCommonUserRegistrationGroup() throws PocketException {
        List<UmUserGroup> groupList = userGroupRepository.findAllByGroupStatus("1");

        if (groupList == null || groupList.size() == 0) {
            return null;
        }

        List<String> predefinedGroups = CommonTasks.getPredefinedGroups();

        List<UmUserGroup> finalGroupList = new ArrayList<>();

        for (UmUserGroup group : groupList) {


            if (predefinedGroups.contains(group.getGroupCode())) {
                continue;
            }

            finalGroupList.add(group);
        }
        return finalGroupList;
    }
}
