package tech.ipocket.pocket.services.web.GroupFunctionMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.UmGroupFunctionMapping;
import tech.ipocket.pocket.entity.UmUserFunction;
import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.repository.web.UserFunctionRepository;
import tech.ipocket.pocket.repository.web.UserGroupFunctionMapRepository;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.um.GroupFunctionListMapRequest;
import tech.ipocket.pocket.request.um.GroupFunctionMapRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.web.GroupFunctionMapListResponse;
import tech.ipocket.pocket.response.web.GroupFunctionMapResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class GroupFunctionMapServiceImpl implements GroupFunctionMapService {
    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private UserGroupFunctionMapRepository userGroupFunctionMapRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private UserFunctionRepository userFunctionRepository;

    @Override
    public GroupFunctionMapResponse addUpdateDeleteGroupFunctionMap(GroupFunctionMapRequest request) throws PocketException {

        GroupFunctionMapResponse response = new GroupFunctionMapResponse();

        request.getGroupFunctionMaps().forEach(groupFunctionMap -> {
            UmGroupFunctionMapping mapping = userGroupFunctionMapRepository
                    .findByGroupCodeAndFunctionCode(
                            groupFunctionMap.getGroupCode(), groupFunctionMap.getFunctionCode());
            if (mapping == null) {
                mapping = new UmGroupFunctionMapping();

                mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
                mapping.setFunctionCode(groupFunctionMap.getFunctionCode());
                mapping.setGroupCode(groupFunctionMap.getGroupCode());
                mapping.setStatus(groupFunctionMap.getStatus());
            } else {
                mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
                mapping.setFunctionCode(groupFunctionMap.getFunctionCode());
                mapping.setGroupCode(groupFunctionMap.getGroupCode());
                mapping.setStatus(groupFunctionMap.getStatus());
            }

            UmGroupFunctionMapping newMapping = userGroupFunctionMapRepository.saveAndFlush(mapping);
            response.setGroupFunctionMapId(newMapping.getId());

        });


        return response;
    }

    @Override
    public GroupFunctionMapListResponse allGroupFunctionMapList() throws PocketException {

        GroupFunctionMapListResponse listResponse = new GroupFunctionMapListResponse();
        ArrayList<UmGroupFunctionMapping> groupFunctionMappings = new ArrayList<>();

        List<UmGroupFunctionMapping> all = userGroupFunctionMapRepository.findAll();

        if (all == null) {
            throw new PocketException(PocketErrorCode.GROUP_FUNCTION_MAPPING_NOT_FOUND);
        }

        all.forEach(map -> {
            UmGroupFunctionMapping mapping = new UmGroupFunctionMapping();
            mapping.setStatus(map.getStatus());
            mapping.setGroupCode(map.getGroupCode());
            mapping.setFunctionCode(map.getFunctionCode());
            mapping.setCreatedDate(map.getCreatedDate());
            mapping.setId(map.getId());

            groupFunctionMappings.add(mapping);
        });

        listResponse.setUmGroupFunctionMappings(groupFunctionMappings);

        return listResponse;
    }

    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    @Override
    public BaseResponseObject mapGroupWithFunctions(GroupFunctionListMapRequest request) throws PocketException {

        // check group validity
        // check function validity

        // delete all map of that group
        // insert new map


        UmUserGroup umUserGroup=userGroupRepository.findByGroupCodeAndGroupStatus(request.getGroupCode(),"1");
        if(umUserGroup==null){
            logWriterUtility.error(request.getRequestId(),"Invalid group code :"+request.getGroupCode());
            throw new PocketException(PocketErrorCode.InvalidGroupCode);
        }

        for (String functionCode:request.getFunctionsCodes()) {
            UmUserFunction umUserFunction=userFunctionRepository.findByFunctionCodeAndFunctionStatus(functionCode,"1");
            if(umUserFunction==null){
                logWriterUtility.error(request.getRequestId(),"Invalid function code :"+functionCode);
                throw new PocketException(PocketErrorCode.InvalidFunctionCode);
            }

        }

        List<UmGroupFunctionMapping> mappings=new ArrayList<>();

        for (String functionCode:request.getFunctionsCodes()) {
            UmGroupFunctionMapping mapping = new UmGroupFunctionMapping();
            mapping.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            mapping.setFunctionCode(functionCode);
            mapping.setGroupCode(request.getGroupCode());
            mapping.setStatus("1");
            mappings.add(mapping);
        }

        userGroupFunctionMapRepository.deleteAllByGroupCode(request.getGroupCode());

        userGroupFunctionMapRepository.saveAll(mappings);

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }
}
