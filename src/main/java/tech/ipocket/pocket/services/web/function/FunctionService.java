package tech.ipocket.pocket.services.web.function;

import tech.ipocket.pocket.request.web.FunctionRequest;
import tech.ipocket.pocket.response.web.FunctionListResponse;
import tech.ipocket.pocket.response.web.FunctionResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface FunctionService {
    FunctionResponse addUpdateDeleteFunction(FunctionRequest request) throws PocketException;

    FunctionListResponse listOfAllFunctions() throws PocketException;

    FunctionListResponse getFunctionsByGroupCode(String groupCode) throws PocketException;

}
