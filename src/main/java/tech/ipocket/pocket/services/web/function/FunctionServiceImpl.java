package tech.ipocket.pocket.services.web.function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmGroupFunctionMapping;
import tech.ipocket.pocket.entity.UmUserFunction;
import tech.ipocket.pocket.repository.web.UserFunctionRepository;
import tech.ipocket.pocket.repository.web.UserGroupFunctionMapRepository;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.web.FunctionRequest;
import tech.ipocket.pocket.response.web.FunctionListResponse;
import tech.ipocket.pocket.response.web.FunctionResponse;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    private UserFunctionRepository userFunctionRepository;

    @Autowired
    private UserGroupFunctionMapRepository userGroupFunctionMapRepository;

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Override
    public FunctionResponse addUpdateDeleteFunction(FunctionRequest request) throws PocketException {
        FunctionResponse response = new FunctionResponse();

        UmUserFunction userFunction = userFunctionRepository
                .findByFunctionCodeAndFunctionStatus(request.getFunctionCode(),"1");
        if (userFunction == null) {
            userFunction = new UmUserFunction();
            userFunction.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            userFunction.setDescription(request.getDescription());
            userFunction.setFunctionCode(request.getFunctionCode());
            userFunction.setFunctionName(request.getFunctionName());
            userFunction.setFunctionStatus(request.getFunctionStatus());
        } else {

            if(request.getFunctionStatus()!=null&&request.getFunctionStatus().equalsIgnoreCase("0")){
                // trying to delete
                // checking is map exists

                Long mapCount=userGroupFunctionMapRepository.countAllByFunctionCodeAndStatus(userFunction.getFunctionCode(),"1");

                if(mapCount!=null&&mapCount>0){
                    throw new PocketException(PocketErrorCode.GroupFunctionMapExists);
                }
                response.setMessage("Function deleted successfully");

            }else{
                response.setMessage("Function operation successfully done");
            }
            userFunction.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            userFunction.setDescription(request.getDescription());
            userFunction.setFunctionCode(request.getFunctionCode());
            userFunction.setFunctionName(request.getFunctionName());
            userFunction.setFunctionStatus(request.getFunctionStatus());
        }

        userFunctionRepository.save(userFunction);

        return response;
    }

    @Override
    public FunctionListResponse listOfAllFunctions() throws PocketException {
        FunctionListResponse response = null;

        List<UmUserFunction> all = userFunctionRepository.findAllByFunctionStatus("1");
        if (all != null) {
            response = new FunctionListResponse();
            response.setFunctions(all);
        }

        return response;
    }

    @Override
    public FunctionListResponse getFunctionsByGroupCode(String groupCode) throws PocketException {
        FunctionListResponse response;
        List<UmGroupFunctionMapping> groupFunctionMappings = userGroupFunctionMapRepository
                .findAllByGroupCodeAndStatus(groupCode, "1");

        if (groupFunctionMappings == null || groupFunctionMappings.size() == 0) {
            throw new PocketException(PocketErrorCode.NoFunctionAssignedForThisGroup);
        }

        List<String> functionCodesList = groupFunctionMappings.stream()
                .map(UmGroupFunctionMapping::getFunctionCode)
                .collect(Collectors.toList());

        List<UmUserFunction> functionList = userFunctionRepository.findAllByFunctionCodeInAndFunctionStatus(functionCodesList,"1");

        response = new FunctionListResponse();
        response.setFunctions(functionList);
        return response;
    }
}
