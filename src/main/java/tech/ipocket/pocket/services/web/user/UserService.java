package tech.ipocket.pocket.services.web.user;

import tech.ipocket.pocket.request.um.ChangeCredentialRequest;
import tech.ipocket.pocket.request.um.GetUserInfoRequest;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface UserService {
    BaseResponseObject getUserList(UserListRequest userListRequest) throws PocketException;

    BaseResponseObject searchUser(UserSearchRequest request) throws PocketException;

    BaseResponseObject editUser(UserEditRequest request) throws PocketException;

    BaseResponseObject pinReset(UserPinPasswordResetRequest request) throws PocketException;

    BaseResponseObject localUnlockWallet(UserLockUnlockWalletRequest request) throws PocketException;

    BaseResponseObject verifyPrimaryIdInfo(UserPrimaryIdInfoVerificationRequest request) throws PocketException;

    BaseResponseObject userDetailsWithWallet(UserDetailsWithWalletRequest request) throws PocketException;

    BaseResponseObject changeCredential(ChangeCredentialRequest request, TokenValidateUmResponse validateResponse) throws PocketException;

    BaseResponseObject deleteUser(GetUserInfoRequest userInfoRequest) throws PocketException;

    BaseResponseObject getCustomerList(CustomerRequest customerRequest) throws PocketException;

    BaseResponseObject getCustomerSerial(CustomerSerialRequest customerSerialRequest) throws PocketException;
}
