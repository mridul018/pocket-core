package tech.ipocket.pocket.services.web.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.UmDocuments;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.entity.UmUserSettings;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.CustomQuery;
import tech.ipocket.pocket.repository.ts.TsClientBalanceRepository;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionRepository;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.um.ChangeCredentialRequest;
import tech.ipocket.pocket.request.um.GetUserInfoRequest;
import tech.ipocket.pocket.request.um.UmUpdateClientStatusRequest;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.GetUserInfoResponse;
import tech.ipocket.pocket.response.web.*;
import tech.ipocket.pocket.services.external.ExternalService;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.utils.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private TsClientRepository clientRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private UserDocumentRepository userDocumentRepository;

    @Autowired
    private CommonService commonService;
    @Autowired
    private ExternalService externalService;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;
    @Autowired
    private UserActivityLogRepository activityLogRepository;
    @Autowired
    private UserDocumentRepository documentsDao;
    @Autowired
    private UserGroupRepository userGroupRepository;
    @Autowired
    private Environment environMent;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private UmStateRepository stateRepository;
    @Autowired
    private UmUserBankMappingRepository userBankMapRepository;

    @Autowired
    private  TsClientRepository tsClientRepository;
    @Autowired
    private TsClientBalanceRepository tsClientBalanceRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;
    @Autowired
    private  UmSrAgentMappingRepository umSrAgentMappingRepository;

    @Override
    public BaseResponseObject getUserList(UserListRequest userListRequest) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();


        List<String> groupCodes = new ArrayList<>();
        groupCodes.add(userListRequest.getGroupCode());
        UserListResponseRoot userListResponseRoot = new UserListResponseRoot();
        HashMap<BigInteger, List<UserListResponse>> userListResponsesMap = new CustomQuery(entityManager)
                .getUserListByTypeAndStatePage(userListRequest.getCountryCode(), groupCodes,
                        userListRequest.getStateList(), userListRequest.getNumberOfItemPerPage(),
                        userListRequest.getPageId(), userListRequest.getRequestId());


        int dataSize = 0;

        List<UserListResponse> finalResponse = new ArrayList<>();
        if (userListResponsesMap != null && userListResponsesMap.size() > 0) {

            Optional<BigInteger> dataSizeOptional = userListResponsesMap.keySet().stream().findFirst();
            dataSize = dataSizeOptional.get().intValue();

            for (UserListResponse listResponse : userListResponsesMap.get(dataSizeOptional.get())) {
                List<String> statusNotIn = new ArrayList<>();
                statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
                statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

                listResponse.setCreatedDate(DateUtil.convertDate(listResponse.getCreatedDate()));

                listResponse.setAccountStatus(UmEnums.UserStatus.getStatusNameByStatus(Integer.parseInt(listResponse.getAccountStatus())));

                listResponse.setCountryCode(userListRequest.getCountryCode());
                if (listResponse.getStateId() != null) {
                    UmState umState = stateRepository.findFirstById(Integer.parseInt(listResponse.getStateId()));
                    if (umState != null) {
                        listResponse.setStateName(umState.getStateName());
                    }
                }

                UmUserInfo userInfo = userInfoRepository.findByLoginId(listResponse.getLoginId());
                if (userInfo == null) {
                    continue;
                }
                if(userInfo != null){
                    UmSrAgentMapping umSrAgentMapping = umSrAgentMappingRepository.findFirstByAgentIdAndStatus(userInfo.getId(), "1");
                    if(umSrAgentMapping != null){
                        UmUserInfo umUserInfo = userInfoRepository.findFirstById(umSrAgentMapping.getSrId());
                        listResponse.setUserParentName(umUserInfo.getFullName());
                    }
                }

                TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(listResponse.getLoginId(), statusNotIn);

                if (client != null) {
                    TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
                    if (clientBalance != null && clientBalance.getBalance() != null) {
                        listResponse.setWalletBalance(clientBalance.getBalance());
                    } else {
                        listResponse.setWalletBalance(BigDecimal.ZERO);
                    }
                } else {
                    listResponse.setWalletBalance(BigDecimal.ZERO);
                }

                UmUserGroup umUserGroup = userGroupRepository.findByGroupCodeAndGroupStatus(listResponse.getGroupCode(), "1");
                if (umUserGroup == null) {
                    logWriterUtility.error(userListRequest.getRequestId(), "Invalid group code :" + listResponse.getGroupCode());
                } else {
                    listResponse.setGroupName(umUserGroup.getGroupName());
                }

                UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(userInfo);
                if (umUserDetails == null) {
                    continue;
                }


                if (userInfo.getGroupCode().equalsIgnoreCase(UmEnums.UserGroup.AGENT.getGroupCode()) ||
                        userInfo.getGroupCode().equalsIgnoreCase(UmEnums.UserGroup.MERCHANT.getGroupCode())) {
                    listResponse.setMerchantName(userInfo.getFullName());
                }

                listResponse.setApplicantName(umUserDetails.getApplicantsName());
                listResponse.setApplicantsName(umUserDetails.getApplicantsName());
                listResponse.setTradeLicenseNo(umUserDetails.getTradeLicenseNo());
                listResponse.setNidNo(umUserDetails.getNidNo());
                listResponse.setPrimaryIdNumber(umUserDetails.getPrimaryIdNumber());
                listResponse.setPrimaryIdType(UmEnums.PrimaryIdTypes.getPrimaryIdTypesByCode(umUserDetails.getPrimaryIdType()));


                UmUserBankMapping umUserBankMapping = userBankMapRepository.findLastByUserIdOrderByIdDesc(userInfo.getId());

                if (umUserBankMapping != null) {
                    listResponse.setBankAccNo(umUserBankMapping.getAccountNumber());
                    listResponse.setBankName(umUserBankMapping.getBankName());
                    listResponse.setBranchName(umUserBankMapping.getBranchName());
                }
                finalResponse.add(listResponse);
            }
        }

        userListResponseRoot.setUserList(finalResponse);
        userListResponseRoot.setGroupCode(userListRequest.getGroupCode());
        userListResponseRoot.setTotalUser(dataSize);

        response.setData(userListResponseRoot);
        response.setError(null);
        response.setStatus(PocketConstants.OK);

        return response;
    }

    @Override
    public BaseResponseObject searchUser(UserSearchRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        List<UmUserInfo> userInfos;

        if (request.getSearchContent().matches("[0-9]")) {
            userInfos = userInfoRepository.findByLoginIdContaining(request.getSearchContent());
        } else {
            userInfos = userInfoRepository.findByFullNameContaining(request.getSearchContent());
        }

        if (userInfos == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        ArrayList<UserListResponse> listResponses = new ArrayList<>();
        userInfos.forEach(userInfo -> {

            UserListResponse listResponse = new UserListResponse();
            listResponse.setId(userInfo.getId());
            listResponse.setFullName(userInfo.getFullName());
            listResponse.setLoginId(userInfo.getLoginId());
            listResponse.setGroupCode(userInfo.getGroupCode());
            listResponse.setPicture("" + userInfo.getPhotoId());
            listResponse.setAccountStatus("" + userInfo.getAccountStatus());
            listResponses.add(listResponse);
        });

        response.setError(null);
        response.setData(listResponses);
        response.setStatus(PocketConstants.OK);

        return response;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public BaseResponseObject editUser(UserEditRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getLoginId());
        if (userInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(userInfo);
        if (umUserDetails == null) {
            logWriterUtility.error(request.getRequestId(), "User details not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (request.getFullName() != null && !request.getFullName().equalsIgnoreCase(userInfo.getFullName())) {
            userInfo.setFullName(request.getFullName());
        }

//        if (request.getDateOfBirth() != null && !request.getDateOfBirth().equalsIgnoreCase(userInfo.getDob())) {
//            userInfo.setDob(request.getDateOfBirth());
//        }
//

        userInfo.setDob(DateUtil.parseDate(request.getDateOfBirth()));

        if (request.getEmail() != null && !request.getEmail().equalsIgnoreCase(userInfo.getEmail())) {
            userInfo.setEmail(request.getEmail());
        }
        if (request.getGender() != null && !request.getGender().equalsIgnoreCase(userInfo.getGender())) {
            userInfo.setGender(request.getGender());
        }

        // user details
        umUserDetails.setPrimaryIdNumber(request.getPrimaryIdNumber());
        umUserDetails.setPrimaryIdIssueDate(DateUtil.parseDate(request.getPrimaryIdIssueDate()));
        umUserDetails.setPrimaryIdExpiryDate(DateUtil.parseDate(request.getPrimaryIdExpiryDate()));
        umUserDetails.setPrimaryIdType(request.getPrimaryIdType());

        umUserDetails.setApplicantsName(request.getApplicantsName());
        umUserDetails.setTradeLicenseNo(request.getTradeLicenseNo());
        umUserDetails.setSecondContactName(request.getSecondContactName());
        umUserDetails.setSecondContactMobileNo(request.getSecondContactMobileNo());
        umUserDetails.setThirdContactName(request.getThirdContactName());
        umUserDetails.setThirdContactMobileNo(request.getThirdContactMobileNo());

        umUserDetails.setPresentAddress(request.getPresentAddress());
        umUserDetails.setPermanentAddress(request.getPermanentAddress());
        umUserDetails.setPostCode(request.getPostCode());


        umUserDetails.setNationality(request.getNationality());
        umUserDetails.setCountryCode(request.getCountryCode());
        umUserDetails.setStateId(request.getStateId());


        if (request.getUserStatus() != null && !request.getUserStatus().equalsIgnoreCase("" + userInfo.getAccountStatus())) {

            boolean isUserStatusValid = UmEnums.UserStatus.isUserStatusValid(Integer.parseInt(request.getUserStatus()));
            if (isUserStatusValid) {

                boolean isUserHasWallet = UmEnums.UserGroup.isUserHasWallet(userInfo.getGroupCode());

                if (isUserHasWallet) {
                    TsClient tsClient = clientRepository.findLastByWalletNoOrderByIdDesc(request.getLoginId());
                    if (tsClient == null) {
                        logWriterUtility.error(request.getRequestId(), "Invalid user status:" + request.getUserStatus());
                        throw new PocketException(PocketErrorCode.WalletNotFound);
                    }
                    tsClient.setUserStatus(request.getUserStatus());
                    clientRepository.save(tsClient);
                }

                userInfo.setAccountStatus(Integer.parseInt(request.getUserStatus()));

            } else {
                logWriterUtility.error(request.getRequestId(), "Invalid user status:" + request.getUserStatus());
            }
        }

        UmUserBankMapping umUserBankMapping = userBankMapRepository.findLastByUserIdOrderByIdDesc(userInfo.getId());

        if (umUserBankMapping == null) {

            if (request.getAccountName()!=null&&request.getAccountNumber()!=null&&
                    request.getBankName() != null && request.getBankCode() != null && request.getBankRoutingNo() != null) {
                umUserBankMapping = new UmUserBankMapping();
                umUserBankMapping.setAccountName(request.getAccountName());
                umUserBankMapping.setAccountNumber(request.getAccountNumber());
                umUserBankMapping.setBranchSwiftCode(request.getBranchSwiftCode());
                umUserBankMapping.setBankName(request.getBankName());
                umUserBankMapping.setBankCode(request.getBankCode());
                umUserBankMapping.setBankRoutingNo(request.getBankRoutingNo());
                umUserBankMapping.setBranchName(request.getBranchName());

            }

        } else {
            if (request.getAccountName()!=null&&request.getAccountNumber()!=null&&
                    request.getBankName() != null && request.getBankCode() != null && request.getBankRoutingNo() != null) {
                umUserBankMapping.setAccountName(request.getAccountName());
                umUserBankMapping.setAccountNumber(request.getAccountNumber());
                umUserBankMapping.setBranchSwiftCode(request.getBranchSwiftCode());
                umUserBankMapping.setBankName(request.getBankName());
                umUserBankMapping.setBranchName(request.getBranchName());
                umUserBankMapping.setBankCode(request.getBankCode());
                umUserBankMapping.setBankRoutingNo(request.getBankRoutingNo());
            }
        }

        if(umUserBankMapping!=null){
            userBankMapRepository.save(umUserBankMapping);
        }

        userInfoRepository.save(userInfo);
        userDetailsRepository.save(umUserDetails);

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(request.getHardwareSignature());
        log.setLogType(ActivityLogType.CUSTOMER_EDIT_BY_PORTAL.getKeyString());
        log.setMetaData("Customer info edit by portal. Edited by :"+userInfo.getLoginId());
        log.setMobileNumber(userInfo.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(userInfo);

        activityLogRepository.save(log);

        // Sr-agent or Partner merchant assign Id updated for merchant user
        UmUserInfo userInfoBySrAgent = userInfoRepository.findFirstByLoginIdOrMobileNumber(request.getSrWallet(), request.getSrWallet());
        if(userInfoBySrAgent != null){
            umSrAgentMappingRepository.updateUmSrAgentMappingByAgentId(userInfo.getId(), userInfoBySrAgent.getId());
        }

        response.setError(null);
        response.setStatus(PocketConstants.OK);
        response.setData(new SuccessBoolResponse(true));
        response.setRequestId(request.getRequestId());

        return response;
    }

    @Override
    public BaseResponseObject pinReset(UserPinPasswordResetRequest request) throws PocketException {

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getLoginId());
        if (userInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found :" + request.getLoginId());
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        String newPin = RandomGenerator.getInstance().generatePIN(4, environMent);

        userInfo.setPassword(passwordEncoder.encode(newPin));

        userInfoRepository.save(userInfo);

        CompletableFuture.runAsync(() -> {
            String msg = "Dear user, Your new PIN number is : " + newPin;

            new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(userInfo.getMobileNumber(), msg, ExternalType.SMS);
        });

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(request.getHardwareSignature());
        log.setLogType(ActivityLogType.RESET_PASSWORD.getKeyString());
        log.setMetaData("Customer pin reset by admin");
        log.setMobileNumber(userInfo.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(userInfo);

        activityLogRepository.save(log);

        BaseResponseObject response = new BaseResponseObject();
        response.setError(null);
        response.setData(new SuccessBoolResponse(true));
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());
        return response;
    }

    @Override
    public BaseResponseObject localUnlockWallet(UserLockUnlockWalletRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        List<Integer> statusNotIn = new ArrayList<>();
        statusNotIn.add(UmEnums.UserStatus.Deleted.getUserStatusType());

        UmUserInfo client = userInfoRepository.findByMobileNumberAndAccountStatusNotIn(request.getWalletNumber(), statusNotIn);

        if (client == null) {
            logWriterUtility.error(request.getRequestId(), "User info not found :" + request.getWalletNumber());
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        String userNewStatus = UmEnums.UserStatus
                .getStatusNameByStatus(Integer.parseInt(request.getWalletStatus()));

        if (userNewStatus == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid user status provided :" + request.getWalletStatus());
            throw new PocketException(PocketErrorCode.InvalidUserStatusProvided);
        }

        client.setAccountStatus(Integer.parseInt(request.getWalletStatus()));
        userInfoRepository.save(client);

        //save data into activity log
        UmActivityLog log = new UmActivityLog();
        log.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        log.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        log.setDeviceInfo(request.getHardwareSignature());
        log.setLogType(ActivityLogType.CUSTOMER_STATUS_CHANGE.getKeyString());
        log.setMetaData("Customer status change by admin. New Status :" + request.getWalletStatus());
        log.setMobileNumber(client.getMobileNumber());
        log.setStatus("1");
        log.setUmUserInfoByUserId(client);

        activityLogRepository.save(log);


        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setData("Wallet status change done. New Status :" + userNewStatus);
        response.setError(null);

        return response;
    }

    @Override
    public BaseResponseObject verifyPrimaryIdInfo(UserPrimaryIdInfoVerificationRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getWalletId());
        if (userInfo == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(request.getWalletId());
        if (umUserInfo == null) {
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        UmUserSettings settings = userSettingsRepository.findFirstByUmUserInfoByUserId(userInfo);
        if (settings == null) {
            throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND);
        }

        // call ts for update wallet status
        UmUpdateClientStatusRequest updateClientStatusRequest = new UmUpdateClientStatusRequest();
        updateClientStatusRequest.setWalletId(userInfo.getMobileNumber());
        updateClientStatusRequest.setNewStatus("" + UmEnums.UserStatus.Verified.getUserStatusType());
        updateClientStatusRequest.setHardwareSignature(request.getHardwareSignature());
        updateClientStatusRequest.setSessionToken(request.getSessionToken());
        updateClientStatusRequest.setRequestId(request.getRequestId());

        Boolean isUpdated = externalService.callTxForUpdateClientStatus(updateClientStatusRequest);

        if (!isUpdated) {
            logWriterUtility.error(request.getRequestId(), "Ts service status update failed");
            throw new PocketException(PocketErrorCode.InternalServerCommunicationError);
        }

        umUserInfo.setAccountStatus(UmEnums.UserStatus.Verified.getUserStatusType());
        userInfoRepository.save(umUserInfo);

        UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);
        umUserDetails.setPrimaryIdVerificationDate(DateUtil.currentDate());
        userDetailsRepository.save(umUserDetails);

        settings.setPrimaryIdVerified(request.isVerifyPrimaryId());
        userSettingsRepository.save(settings);

        response.setError(null);
        response.setData(new SuccessBoolResponse(true));
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());
        return response;
    }

    @Override
    public BaseResponseObject userDetailsWithWallet(UserDetailsWithWalletRequest request) throws PocketException {

        BaseResponseObject response = new BaseResponseObject();

        UserDetailsWithWalletResponse details = new UserDetailsWithWalletResponse();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getLoginId());
        if (userInfo == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        if (userInfo.getAccountStatus() == AccountStatus.HARD_BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.BLOCKED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_BLOCKED);
        }

        if (userInfo.getAccountStatus() == AccountStatus.DELETED.getCODE()) {
            throw new PocketException(PocketErrorCode.CUSTOMER_DELETED);
        }

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getLoginId(), clientStatus);


//        TsClient client = clientRepository.findByWalletNo(request.getLoginId());
        if (client == null) {
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        Optional<UmDocuments> documents = userDocumentRepository.findById(userInfo.getPhotoId());
        if (!documents.isPresent()) {
            logWriterUtility.error(request.getRequestId(), "Image Document not found");
        } else {
            details.setPhotoUrl(documents.get().getDocumentPath());
        }

        if (client.getTsClientBalancesById() == null) {
            response.setError(new ErrorObject(PocketErrorCode.CUSTOMER_BALANCE_NOT_FOUND));
            return response;
        }

        details.setId(userInfo.getId());
        details.setAccountStatus(userInfo.getAccountStatus());
        details.setEmail(userInfo.getEmail());
        details.setFullName(userInfo.getFullName());
        details.setGroupCode(userInfo.getGroupCode());
        details.setFeeProfileCode(client.getFeeProfileCode());
        details.setTransactionProfileCode(client.getTransactionProfileCode());
        client.getTsClientBalancesById().forEach(balance -> {
            details.setAmount(balance.getBalance());
        });

        response.setError(null);
        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setData(details);

        return response;
    }

    @Override
    public BaseResponseObject changeCredential(ChangeCredentialRequest request, TokenValidateUmResponse validateResponse) throws PocketException {

        UmUserInfo client = userInfoRepository.findByLoginId(validateResponse.getMobileNumber());
        if (client == null) {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        // check old pin

        if (request.getNewCredential().equals(request.getOldCredential())) {
            logWriterUtility.error(request.getRequestId(), "Both credential are same");
            throw new PocketException(PocketErrorCode.OldAndNewCredentialMustDifferent);
        }

        if (!passwordEncoder.matches(request.getOldCredential(), client.getPassword())) {
            logWriterUtility.error(request.getRequestId(), "Old credential not matched");
            throw new PocketException(PocketErrorCode.OldCredentialNotMatched);
        }


        // if not matched then throw

        //update new PIN
        client.setPassword(passwordEncoder.encode(request.getNewCredential()));
        userInfoRepository.save(client);

        String message = "Credential changed successfully";

        commonService.addInteractionLog(client.getLoginId(), message, request.getHardwareSignature(), request.getDeviceName(),
                UmEnums.ActivityLogType.ChangePin.getActivityLogType(), client);


        BaseResponseObject response = new BaseResponseObject(request.getRequestId());
        response.setError(null);
        response.setRequestId(request.getRequestId());
        response.setStatus(PocketConstants.OK);
        response.setData(new SuccessBoolResponse(true));
        return response;
    }

    @Override
    public BaseResponseObject deleteUser(GetUserInfoRequest userInfoRequest) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        UmUserInfo client = userInfoRepository.findFirstByLoginIdOrMobileNumber(userInfoRequest.getMobileNumber(), userInfoRequest.getMobileNumber());
        if (client == null) {
            logWriterUtility.error(userInfoRequest.getRequestId(), "User not found.");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND, "User not found.");
        }

        UmUserDetails userDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(client);
        if (userDetails == null) {
            logWriterUtility.error(userInfoRequest.getRequestId(), "User not found with details.");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND, "User not found with details.");
        }

        TsClient tsClient = tsClientRepository.findFirstByWalletNoAndUserStatusNotIn(client.getMobileNumber(), statusNotIn);
        if (tsClient == null) {
            logWriterUtility.error(userInfoRequest.getRequestId(), "Wallet not found.");
            throw new PocketException(PocketErrorCode.WalletNotFound, "Wallet not found.");
        }

        TsClientBalance tsClientBalance = tsClientBalanceRepository.findFirstByTsClientByClientId(tsClient);
        if (tsClientBalance == null) {
            logWriterUtility.error(userInfoRequest.getRequestId(), "Customer balance not found.");
            throw new PocketException(PocketErrorCode.CUSTOMER_BALANCE_NOT_FOUND, "Customer balance not found.");
        }

        UmUserSettings umUserSettings = userSettingsRepository.findFirstByUmUserInfoByUserId(client);
        if(umUserSettings == null){
            logWriterUtility.error(userInfoRequest.getRequestId(), "User setting info not found.");
            throw new PocketException(PocketErrorCode.USER_SETTINGS_NOT_FOUND, "User setting info not found.");
        }

        UmUserBankMapping umUserBankMapping = userBankMapRepository.findLastByUserIdOrderByIdDesc(client.getId());
        UmSrAgentMapping umSrAgentMapping = umSrAgentMappingRepository.findFirstByAgentIdAndStatus(client.getId(), "1");

        BaseResponseObject response = new BaseResponseObject(userInfoRequest.getRequestId());
        GetUserInfoResponse userInfoResponse = new GetUserInfoResponse();

        if(client != null && userDetails!=null && tsClient!=null && tsClientBalance!=null &&
                umUserSettings!=null && umUserBankMapping == null && umSrAgentMapping == null){

            TsTransaction tsTransaction = tsTransactionRepository.findFirstBySenderWalletOrReceiverWallet(tsClient.getWalletNo(), tsClient.getWalletNo());

            if(tsTransaction == null){
                userInfoRepository.delete(client);
                userDetailsRepository.deleteByUmUserInfoByUserId(userDetails.getUmUserInfoByUserId());
                tsClientBalanceRepository.delete(tsClientBalance);
                tsClientRepository.delete(tsClient);
                userSettingsRepository.deleteByUmUserInfoByUserId(umUserSettings.getUmUserInfoByUserId());

                userInfoResponse.setMessage("User wallet account is deleted successfully.");
                response.setError(null);
                response.setRequestId(userInfoRequest.getRequestId());
                response.setStatus(PocketConstants.OK);
                response.setData(userInfoResponse);
            }else{
                responseMsg(response, userInfoRequest);
            }
        }else {
            responseMsg(response, userInfoRequest);
        }

        if(client != null && userDetails!=null && tsClient!=null && tsClientBalance!=null &&
                umUserSettings!=null && umUserBankMapping!=null && umSrAgentMapping!=null) {
            //To do merchant_delete if comes requirement
            responseMsg(response, userInfoRequest);
        }

        if(client != null && userDetails!=null && tsClient!=null && tsClientBalance!=null &&
                umUserSettings!=null && umUserBankMapping!=null && umSrAgentMapping == null) {
            //To do sr_agent or partner_merchant delete if comes requirement
            responseMsg(response, userInfoRequest);
        }

        return response;
    }

    @Override
    public BaseResponseObject getCustomerList(CustomerRequest customerRequest) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();

        List<TsClient> customerWinnerLotteryList = new ArrayList<>();
        List<TsClientResponse> customerResponseList = new ArrayList<>();
        CustomerResponse customerResponse = new CustomerResponse();
        TsClientResponse tsClientResponse = new TsClientResponse();

        if(customerRequest != null &&
                customerRequest.getGroupCode() != null &&  customerRequest.getGroupCode().matches(UmEnums.UserGroup.CUSTOMER.getGroupCode()) &&
                customerRequest.getAccountStatus().matches(String.valueOf(UmEnums.UserStatus.Verified.getUserStatusType()))){

            List<TsClient> tsClientList = tsClientRepository.findByGroupCodeAndUserStatusAndCreatedDateBetween(customerRequest.getGroupCode(),
                    customerRequest.getAccountStatus(),customerRequest.getStartDate(), customerRequest.getEndDate());

            if (tsClientList != null && tsClientList.size() > 0 && customerRequest.getCustomerWinnerNumber() > 0) {
                Random rand = new Random();
                for(int i = 1; i<=tsClientList.size(); i++) {
                    int nextRandom = rand.nextInt(tsClientList.size());

                    TsClient tsClient = tsClientList.get(nextRandom);
                    if(tsClient.getWalletNo().substring(0, 3).matches("\\+97")){
                        customerWinnerLotteryList.add(tsClientList.get(nextRandom));
                    }
                }

                List<TsClient> winnerListWithoutDuplicates = customerWinnerLotteryList.stream()
                        .distinct()
                        .limit(customerRequest.getCustomerWinnerNumber())
                        .collect(Collectors.toList());

                if(winnerListWithoutDuplicates.size() < customerRequest.getCustomerWinnerNumber()){
                    baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired.getKeyString(),
                            "Requested customer winner number is less to actual winner number."));
                    baseResponseObject.setRequestId(customerRequest.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.ERROR);
                    baseResponseObject.setData(null);
                    return baseResponseObject;
                }

                for(TsClient tsClient : winnerListWithoutDuplicates){
                    tsClientResponse = new TsClientResponse();

                    tsClientResponse.setId(tsClient.getId());
                    tsClientResponse.setFullName(tsClient.getFullName());
                    tsClientResponse.setWalletNo(tsClient.getWalletNo());
                    tsClientResponse.setGroupCode(tsClient.getGroupCode());
                    tsClientResponse.setUserStatus(tsClient.getUserStatus());
                    tsClientResponse.setCreatedDate(tsClient.getCreatedDate());

                    UmUserInfo userInfo = userInfoRepository.findFirstByLoginIdOrMobileNumber(tsClient.getWalletNo(), tsClient.getWalletNo());
                    if(userInfo != null){
                        UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(userInfo);

                        if(umUserDetails != null && umUserDetails.getStateId() != null){
                            UmState umState = stateRepository.findFirstById(Integer.parseInt(umUserDetails.getStateId()));

                            if(umState != null && umState.getStateName() != null){
                                tsClientResponse.setStateName(umState.getStateName());
                            }
                        }
                    }

                    customerResponseList.add(tsClientResponse);
                }
            }else {
                baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired.getKeyString(),
                        "Total winning customer number must be given."));
                baseResponseObject.setRequestId(customerRequest.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return baseResponseObject;
            }
        }else{
            logWriterUtility.error(customerRequest.getRequestId(), "Receiver must be customer Or Invalid GroupCode");
            throw new PocketException(PocketErrorCode.ReceiverMustBeCustomer, "Receiver must be customer or Invalid GroupCode");
        }
        customerResponse.setTsClientResponses(customerResponseList);

        baseResponseObject.setData(customerResponse);
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getCustomerSerial(CustomerSerialRequest customerSerialRequest) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        CustomerSerialRequest customerResponse = new CustomerSerialRequest();
        CustomerSerialRequest waitingCustomerResponse = new CustomerSerialRequest();

            try {
                if(customerSerialRequest != null && customerSerialRequest.getWalletNo() != null) {

                    TsClient tsClient = tsClientRepository.findByWalletNo(customerSerialRequest.getWalletNo()).get();
                    String pattern = "yyyy-MM-dd";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    Date startDate = simpleDateFormat.parse("2021-12-01");
                    Date entDate = simpleDateFormat.parse("2021-12-19");

                    if (tsClient != null) {
                        List<TsClient> tsClientList = tsClientRepository.findByGroupCodeAndUserStatusAndCreatedDateBetween(tsClient.getGroupCode(),
                                tsClient.getUserStatus(), startDate, entDate);

                        if(tsClientList != null && tsClientList.size()>0){
                            List<String> customerSerialList = tsClientList.stream()
                                    .filter(x -> x.getWalletNo().substring(0,3).equals("+97"))
                                    .map((TsClient t) -> t.getWalletNo())
                                    .collect(Collectors.toList());

                            if (customerSerialList.size() > 0 && customerSerialList.size() < 25000) {
                                int index = customerSerialList.indexOf(customerSerialRequest.getWalletNo());
                                if(index > 0){
                                    customerResponse.setWalletNo("Serial No : " + Integer.valueOf(1 + index));
                                }else{
                                    customerResponse.setWalletNo("No searching data found of this walletNo.");
                                }

                                baseResponseObject.setData(customerResponse);
                                baseResponseObject.setError(null);
                                baseResponseObject.setStatus(PocketConstants.OK);
                                return baseResponseObject;
                            }

                            if(tsClientList.size() > 25000){
                                List<String> waitingCustomerSerialList = tsClientList.stream()
                                        .filter(t -> t.getWalletNo().substring(0,3).equals("+97"))
                                        .map(TsClient::getWalletNo)
                                        .collect(Collectors.toList());

                                List<String> waitingCustomerSerialSublist = waitingCustomerSerialList.subList(25000, tsClientList.size());
                                int tsClientIndex = tsClientList.indexOf(customerSerialRequest.getWalletNo());
                                if ( tsClientIndex > 25000 && tsClientIndex < tsClientList.size()) {
                                    int index = waitingCustomerSerialSublist.indexOf(customerSerialRequest.getWalletNo());
                                    if(index > 0){
                                        waitingCustomerResponse.setWalletNo("Waiting Serial No : " + Integer.valueOf(index));
                                    }else{
                                        waitingCustomerResponse.setWalletNo("No searching data found of this walletNo.");
                                    }

                                    baseResponseObject.setData(waitingCustomerResponse);
                                    baseResponseObject.setError(null);
                                    baseResponseObject.setStatus(PocketConstants.OK);
                                    return baseResponseObject;
                                }
                            }
                        }
                    }
                }else {
                    baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired.getKeyString(),
                            "Wallet number must be given."));
                    baseResponseObject.setRequestId(customerSerialRequest.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.ERROR);
                    baseResponseObject.setData(null);
                    return baseResponseObject;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        baseResponseObject.setData(null);
        baseResponseObject.setError(new ErrorObject(PocketErrorCode.WalletNotFound.getKeyString(),
                "TsClient searching data not found."));
        baseResponseObject.setStatus(PocketConstants.ERROR);
        return baseResponseObject;
    }

    public void responseMsg(BaseResponseObject response, GetUserInfoRequest userInfoRequest){
        response.setError(new ErrorObject(PocketErrorCode.CUSTOMER_DELETED.getKeyString(),
                "User wallet account would not be deleted."));
        response.setRequestId(userInfoRequest.getRequestId());
        response.setStatus(PocketConstants.ERROR);
        response.setData(null);
    }


}
