package tech.ipocket.pocket.services.ts.remittance;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetBeneficiaryRequest extends BaseRequestObject {

    private String type;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(type==null){
            baseResponseObject.setErrorCode(PocketErrorCode.BeneficiaryTypeRequired);
            return;
        }

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
