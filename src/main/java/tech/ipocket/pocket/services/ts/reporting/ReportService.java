package tech.ipocket.pocket.services.ts.reporting;

import org.springframework.data.domain.Sort;
import tech.ipocket.pocket.request.ts.AccountStatementRequest;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.GetAggregateBalanceRequest;
import tech.ipocket.pocket.request.ts.TransactionDashboardBalanceRequest;
import tech.ipocket.pocket.request.ts.reporting.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.reporting.TransactionHistoryModel;
import tech.ipocket.pocket.utils.PocketException;

import java.util.List;

public interface ReportService {
    BaseResponseObject getAccountStatement(AccountStatementRequest accountStatementRequest, TsSessionObject umSessionResponse, Sort.Direction direction) throws PocketException;
    BaseResponseObject getDashboardAggregateBalance(GetAggregateBalanceRequest aggregateBalanceRequest, TsSessionObject umSessionResponse);

    BaseResponseObject getTransactionDashboardBalance(TransactionDashboardBalanceRequest aggregateBalanceRequest, TsSessionObject umSessionResponse) throws PocketException;

    BaseResponseObject getSummaryOfTransactions(GetSummeryOfTransactionRequest getSummeryOfTransactionRequest, TsSessionObject umSessionResponse) throws PocketException;

    BaseResponseObject transactionQuery(TransactionHistoryRequest transactionHistoryRequest, TsSessionObject umSessionResponse) throws PocketException;

    List<TransactionHistoryModel> getTransactionSummeryDetails(GetTransactionSummeryDetailsRequest transactionHistoryRequest, TsSessionObject umSessionResponse) throws PocketException;

    BaseResponseObject getCustomerTransactionLimit(GetCustomerLimitRequest transactionHistoryRequest, String mobileNumber) throws PocketException;

    BaseResponseObject getTopUpReport(TopUpReportRequest transactionHistoryRequest) throws PocketException;

    BaseResponseObject filterTopUpReport(FilterTopUpReportRequest transactionHistoryRequest) throws PocketException;

    BaseResponseObject getStateWiseUser(GetStateWiseUserRequest transactionHistoryRequest, TsSessionObject umSessionResponse) throws PocketException;

    BaseResponseObject getUserListBySrWallet(GetUserListBySrWalletRequest srWalletRequest, TsSessionObject umSessionResponse) throws PocketException;

    BaseResponseObject getGlList(EmptyRequest request) throws PocketException;
    BaseResponseObject getGlStatement(GetGlStatementRequest request) throws PocketException;
}
