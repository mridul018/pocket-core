package tech.ipocket.pocket.services.ts.feeProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.feeProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.feeProfile.FeeListItem;
import tech.ipocket.pocket.response.ts.feeProfile.FeeProfileFeeDetailsRoot;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FeeProfileServiceImpl implements FeeProfileService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsFeeProfileRepository tsFeeProfileRepository;

    @Autowired
    private TsFeeRepository tsFeeRepository;

    @Autowired
    private TsFeeStakeholderRepository tsFeeStakeholderRepository;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private TsFeeFeeStakeholderMapRepository tsFeeFeeStakeholderMapRepository;

    @Autowired
    private TsStakeHolderRepository tsStakeHolderRepository;
    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsFeeProfileLimitMappingRepository tsFeeProfileLimitMappingRepository;

    @Autowired
    private TsConversionRateRepository tsConversionRateRepository;
    @Autowired
    private TsClientRepository clientRepository;

    @Override
    public BaseResponseObject createFeeProfile(CreateFeeProfileRequest request) throws PocketException {

        // check by fee code
        //check by fee name

        TsFeeProfile tsFeeProfile = tsFeeProfileRepository.findFirstByProfileCodeAndStatus(request.getProfileCode(), "1");
        if (tsFeeProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Fee profile code already exists");
            throw new PocketException(PocketErrorCode.FeeProfileCodeAlreadyExists);
        }

        tsFeeProfile = tsFeeProfileRepository.findFirstByProfileNameAndStatus(request.getProfileName(), "1");
        if (tsFeeProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Fee profile name already exists");
            throw new PocketException(PocketErrorCode.FeeProfileNameAlreadyExists);
        }

        tsFeeProfile = new TsFeeProfile();
        tsFeeProfile.setProfileCode(request.getProfileCode());
        tsFeeProfile.setProfileName(request.getProfileName());
        tsFeeProfile.setCreatedDate(new Date());
        tsFeeProfile.setStatus("1");
        tsFeeProfileRepository.save(tsFeeProfile);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getFeeProfile(GetFeeProfileRequest request) {

        List<TsFeeProfile> feeProfileList = tsFeeProfileRepository.findAllByStatus("1");

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(feeProfileList);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateFeeProfile(CreateFeeProfileRequest request) throws PocketException {
        TsFeeProfile tsFeeProfile = tsFeeProfileRepository.findFirstByProfileCodeAndStatus(request.getProfileCode(), "1");
        if (tsFeeProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Fee profile code doesn't exists");
            throw new PocketException(PocketErrorCode.FeeProfileCodeNotExists);
        }

        if (request.getStatus() != null && !request.getStatus().equalsIgnoreCase(tsFeeProfile.getStatus())) {
            if (request.getStatus().equalsIgnoreCase("0")) {
                // check map exists
                List<TsFeeProfileLimitMapping> mapCount = tsFeeProfileLimitMappingRepository.findAllByFeeProfileCodeOrderByIdDesc(tsFeeProfile.getProfileCode());


                if (mapCount != null && mapCount.size() > 0) {
                    logWriterUtility.error(request.getRequestId(), "Fee profile delete failed. profile limit Map exists");
                    throw new PocketException(PocketErrorCode.MapAlreadyExists);
                }

                tsFeeProfile.setStatus(request.getStatus());
            } else {
                tsFeeProfile.setStatus(request.getStatus());
            }
        }

        tsFeeProfileRepository.save(tsFeeProfile);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    @Override
    public BaseResponseObject createFee(CreateFeeRequest request) throws PocketException {
        // check by fee code
        //check by fee name

        if (request.getId() != null && request.getId() > 0) {
            return updateFee(request);
        }

        TsFee tsFee = tsFeeRepository.findFirstByFeeCodeAndStatus(request.getFeeCode(), "1");
        if (tsFee != null) {
            logWriterUtility.error(request.getRequestId(), "Fee code already exists");
            throw new PocketException(PocketErrorCode.FeeCodeAlreadyExists);
        }

        tsFee = tsFeeRepository.findFirstByFeeNameAndStatus(request.getFeeName(), "1");
        if (tsFee != null) {
            logWriterUtility.error(request.getRequestId(), "Fee name already exists");
            throw new PocketException(PocketErrorCode.FeeNameAlreadyExists);
        }

        // check stakeholder

        List<TsFeeStakeholder> tsFeeStakeholders = new ArrayList<>();
        double totalPercentage = 0;


        for (FeeStakeHolderItem feeStakeHolderItem : request.getStakeHolders()
        ) {

            TsFeeStakeholder tsFeeStakeholder = tsFeeStakeholderRepository.findFirstById(feeStakeHolderItem.getId());
            if (tsFeeStakeholder == null) {
                logWriterUtility.error(request.getRequestId(), "Fee stakeholder not found");
                throw new PocketException(PocketErrorCode.StakeHolderNotFound);
            }
            totalPercentage += feeStakeHolderItem.getPercentage();
            tsFeeStakeholders.add(tsFeeStakeholder);
        }

        if (totalPercentage != 100) {
            logWriterUtility.error(request.getRequestId(), "Total stakeholder percentage must be equals 100");
            throw new PocketException(PocketErrorCode.StakeHolderPercentageMustEquals100);
        }

        tsFee = new TsFee();
        tsFee.setFeeCode(request.getFeeCode());
        tsFee.setFeeName(request.getFeeName());
        tsFee.setFixedFee(request.getFixedFee());
        tsFee.setPercentageFee(request.getPercentageFee());
        tsFee.setBoth(request.getBoth());
        tsFee.setFeePayer(request.getFeePayer());
        tsFee.setAppliedFeeMethod(request.getAppliedFeeMethod());
        tsFee.setStatus("1");
        tsFee.setCreatedDate(new Date());
        tsFeeRepository.save(tsFee);

        // Insert the fee-feestakeholder mapping table

        for (FeeStakeHolderItem feeStakeHolderItem : request.getStakeHolders()
        ) {

            TsFeeStakeholder tsFeeStakeholder = tsFeeStakeholderRepository.findFirstById(feeStakeHolderItem.getId());

            TsFeeFeeStakeholderMap tsFeeFeeStakeholderMap = new TsFeeFeeStakeholderMap();
            tsFeeFeeStakeholderMap.setStakeholderId(tsFeeStakeholder.getId());
            tsFeeFeeStakeholderMap.setFeeProductCode(tsFee.getFeeCode());
            tsFeeFeeStakeholderMap.setStakeholderPercentage(feeStakeHolderItem.getPercentage());
            tsFeeFeeStakeholderMap.setStakeholderPaymentMethod(feeStakeHolderItem.getStakeholderPaymentMethod());
            tsFeeFeeStakeholderMap.setCreatedDate(new Date());

            try {
                tsFeeFeeStakeholderMapRepository.save(tsFeeFeeStakeholderMap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getFee(GetFeeRequest request) throws PocketException {
        List<TsFee> feeList=null;
        if(request.getFeeCode()==null||request.getFeeCode().trim().length()==0){
            feeList = tsFeeRepository.findAllByStatusOrderByFeeCode("1");

        }else{
            TsFee tsFee = tsFeeRepository.findFirstByFeeCodeAndStatus(request.getFeeCode(),"1");
            if(tsFee==null){
                logWriterUtility.error(request.getRequestId(),"Fee details not found :"+request.getFeeCode());
                throw new PocketException(PocketErrorCode.FeeCodeNotFound);
            }

            feeList=new ArrayList<>();
            feeList.add(tsFee);
        }


        List<FeeListItem> feeListItems = new ArrayList<>();

        for (TsFee tempFee : feeList) {
            FeeListItem feeListItem = new FeeListItem();
            feeListItem.setId("" + tempFee.getId());
            feeListItem.setFeeName(tempFee.getFeeName());
            feeListItem.setFeeCode(tempFee.getFeeCode());
            feeListItem.setFeePayer(tempFee.getFeePayer());
            feeListItem.setFixedFee(tempFee.getFixedFee());
            feeListItem.setPercentageFee(tempFee.getPercentageFee());
            feeListItem.setAppliedFeeMethod(tempFee.getAppliedFeeMethod());
            feeListItem.setBoth(tempFee.getBoth());


            List<FeeStakeHolderItem> stakeHolderItems = new ArrayList<>();


            List<TsFeeFeeStakeholderMap> stakeholderMaps = tsFeeFeeStakeholderMapRepository.findAllByFeeProductCode(tempFee.getFeeCode());

            for (TsFeeFeeStakeholderMap tempMap : stakeholderMaps) {

                TsFeeStakeholder tsFeeStakeholder = tsStakeHolderRepository
                        .findFirstById(tempMap.getStakeholderId());

                if (tsFeeStakeholder != null) {
                    FeeStakeHolderItem feeStakeHolderItem = new FeeStakeHolderItem();
                    feeStakeHolderItem.setId(tsFeeStakeholder.getId());
                    feeStakeHolderItem.setGlCode(tsFeeStakeholder.getGlCode());
                    feeStakeHolderItem.setStakeholderName(tsFeeStakeholder.getStakeholderName());
                    feeStakeHolderItem.setPercentage(tempMap.getStakeholderPercentage());
                    feeStakeHolderItem.setStakeholderPaymentMethod(tempMap.getStakeholderPaymentMethod());
                    stakeHolderItems.add(feeStakeHolderItem);
                } else {
                    logWriterUtility.error(request.getRequestId(),
                            "Stakeholder not found :" + tempMap.getStakeholderId());
                }


            }
            feeListItem.setStakeHolders(stakeHolderItems);

            feeListItems.add(feeListItem);
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(feeListItems);

        return baseResponseObject;

    }


    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    @Override
    public BaseResponseObject updateFee(CreateFeeRequest request) throws PocketException {

        TsFee tsFee = tsFeeRepository.findFirstByFeeCodeAndStatus(request.getFeeCode(), "1");

        if (tsFee == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee id");
            throw new PocketException(PocketErrorCode.InvalidFee);
        }


        if (request.getStatus() != null && !request.getStatus().equalsIgnoreCase(tsFee.getStatus())) {
            if (request.getStatus().equalsIgnoreCase("0")) {
                // check map exists
                List<TsFeeProfileLimitMapping> mapCount = tsFeeProfileLimitMappingRepository.findAllByFeeCode(tsFee.getFeeCode());

                if (mapCount != null && mapCount.size() > 0) {
                    logWriterUtility.error(request.getRequestId(), "Fee delete failed. Map exists");
                    throw new PocketException(PocketErrorCode.MapAlreadyExists);
                }

                tsFee.setStatus(request.getStatus());
            } else {
                tsFee.setStatus(request.getStatus());
            }
        }

        if(request.getStatus()!=null &&!request.getStatus().equals("0")){
            if (request.getStakeHolders() == null || request.getStakeHolders().size() == 0) {
                throw new PocketException(PocketErrorCode.StakeHolderRequired);
            }
        }

        if (request.getFeeName() != null && !request.getFeeName().equalsIgnoreCase(tsFee.getFeeName())) {
            tsFee.setFeeName(request.getFeeName());
        }

        if (request.getFeePayer() != null && !request.getFeePayer().equalsIgnoreCase(tsFee.getFeePayer())) {
            tsFee.setFeePayer(request.getFeePayer());
        }

        if (request.getAppliedFeeMethod() != null && !request.getAppliedFeeMethod().equalsIgnoreCase(tsFee.getAppliedFeeMethod())) {
            tsFee.setAppliedFeeMethod(request.getAppliedFeeMethod());
        }

        if (request.getStatus() != null && !request.getStatus().equalsIgnoreCase(tsFee.getStatus())) {
            tsFee.setStatus(request.getStatus());
        }

        if (request.getBoth() != tsFee.getBoth()) {
            tsFee.setBoth(request.getBoth());
        }

        if (request.getFixedFee() != null && request.getFixedFee().compareTo(tsFee.getFixedFee()) != 0) {
            tsFee.setFixedFee(request.getFixedFee());
        }

        if (request.getPercentageFee() != null && request.getPercentageFee().compareTo(tsFee.getPercentageFee()) != 0) {
            tsFee.setPercentageFee(request.getPercentageFee());
        }


        tsFeeRepository.save(tsFee);

        //check all stakeholder percentage

        if(request.getStatus()==null || !request.getStatus().equalsIgnoreCase("0")){
            if(request.getStakeHolders()!=null&&request.getStakeHolders().size()>0){

                double totalPercentage=0;
                for (FeeStakeHolderItem feeStakeHolderItem : request.getStakeHolders()
                ) {
                    totalPercentage+=feeStakeHolderItem.getPercentage();
                }

                if(totalPercentage!=100){
                    logWriterUtility.error(request.getRequestId(),"Stakeholder percentage !=100 :"+totalPercentage);
                    throw new PocketException(PocketErrorCode.StakeHolderPercentageMustEquals100);
                }

                //remove all stakeholder
                tsFeeFeeStakeholderMapRepository.deleteByFeeProductCode(tsFee.getFeeCode());

                //add stakeholder
                for (FeeStakeHolderItem feeStakeHolderItem : request.getStakeHolders()
                ) {

                    TsFeeStakeholder tsFeeStakeholder = tsFeeStakeholderRepository.findFirstById(feeStakeHolderItem.getId());


                    if(tsFeeStakeholder==null){
                        logWriterUtility.error(request.getRequestId(),"Invalid stakeholder id provided :"+feeStakeHolderItem.getId());
                        throw new PocketException(PocketErrorCode.InvalidStakeholder);
                    }

                    TsFeeFeeStakeholderMap tsFeeFeeStakeholderMap = new TsFeeFeeStakeholderMap();
                    tsFeeFeeStakeholderMap.setStakeholderId(tsFeeStakeholder.getId());
                    tsFeeFeeStakeholderMap.setFeeProductCode(tsFee.getFeeCode());
                    tsFeeFeeStakeholderMap.setStakeholderPercentage(feeStakeHolderItem.getPercentage());
                    tsFeeFeeStakeholderMap.setStakeholderPaymentMethod(feeStakeHolderItem.getStakeholderPaymentMethod());
                    tsFeeFeeStakeholderMap.setCreatedDate(new Date());

                    try {
                        tsFeeFeeStakeholderMapRepository.save(tsFeeFeeStakeholderMap);
                    } catch (Exception e) {
                        e.printStackTrace();
                        logWriterUtility.error(request.getRequestId(),"Error:"+e.getLocalizedMessage());

                        throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
                    }
                }
            }
        }




        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getFeeProfileFeeList(GetFeeProfileRequest request) throws PocketException {

        if(request.getFeeProfileCode()==null){
            logWriterUtility.error(request.getRequestId(),"Fee profile code required");
            throw new PocketException(PocketErrorCode.FeeProfileCodeRequired);
        }

        TsFeeProfile feeProfile = tsFeeProfileRepository.findFirstByProfileCodeAndStatus(request.getFeeProfileCode(),"1");

        if(feeProfile==null){
            logWriterUtility.error(request.getRequestId(),"Fee profile code not found");
            throw new PocketException(PocketErrorCode.FeeProfileCodeNotExists);
        }

        FeeProfileFeeDetailsRoot feeProfileFeeDetailsRoot=new FeeProfileFeeDetailsRoot();
        feeProfileFeeDetailsRoot.setFeeProfileName(feeProfile.getProfileName());
        feeProfileFeeDetailsRoot.setFeeProfileCode(feeProfile.getProfileCode());

        List<TsFeeProfileLimitMapping> tsFeeProfileLimitMappingMap = tsFeeProfileLimitMappingRepository.findAllByFeeProfileCodeOrderByIdDesc( feeProfile.getProfileCode());



        List<FeeListItem> feeListItems = new ArrayList<>();

        if(tsFeeProfileLimitMappingMap!=null&&tsFeeProfileLimitMappingMap.size()>0){
            List<TsFeeProfileLimitMapping> feeProfileLimitListUnique=new ArrayList<>();

            for (TsFeeProfileLimitMapping mapping:tsFeeProfileLimitMappingMap
            ) {
                if(feeProfileLimitListUnique.size() == 0){
                    feeProfileLimitListUnique.add(mapping);
                    continue;
                }

                boolean isExists=false;
                for (TsFeeProfileLimitMapping mapping1:feeProfileLimitListUnique
                     ) {
                    if(mapping1.getFeeCode().equals(mapping.getFeeCode())){
                        isExists=true;
                        break;
                    }
                }

                if(!isExists){
                    feeProfileLimitListUnique.add(mapping);
                }
            }


            for (TsFeeProfileLimitMapping mapping:feeProfileLimitListUnique
                 ) {

                TsFee tempFee=tsFeeRepository.findFirstByFeeCodeAndStatus(mapping.getFeeCode(),"1");
                if(tempFee==null){

                    logWriterUtility.error(request.getRequestId(),"Fee is deleted :"+mapping.getFeeCode());
                    continue;
                }

                FeeListItem feeListItem = new FeeListItem();
                feeListItem.setId("" + tempFee.getId());
                feeListItem.setFeeName(tempFee.getFeeName());
                feeListItem.setFeeCode(tempFee.getFeeCode());
                feeListItem.setFeePayer(tempFee.getFeePayer());
                feeListItem.setFixedFee(tempFee.getFixedFee());
                feeListItem.setPercentageFee(tempFee.getPercentageFee());
                feeListItem.setAppliedFeeMethod(tempFee.getAppliedFeeMethod());
                feeListItem.setBoth(tempFee.getBoth());

                TsService service=tsServiceRepository.findFirstByServiceCode(mapping.getServiceCode());

                if(service!=null){
                    feeListItem.setServiceCode(service.getServiceCode());
                    feeListItem.setServiceName(service.getServiceName());
                }


                List<FeeStakeHolderItem> stakeHolderItems = new ArrayList<>();


                List<TsFeeFeeStakeholderMap> stakeholderMaps = tsFeeFeeStakeholderMapRepository.findAllByFeeProductCode(tempFee.getFeeCode());

                for (TsFeeFeeStakeholderMap tempMap : stakeholderMaps) {

                    TsFeeStakeholder tsFeeStakeholder = tsStakeHolderRepository
                            .findFirstById(tempMap.getStakeholderId());

                    if (tsFeeStakeholder != null) {
                        FeeStakeHolderItem feeStakeHolderItem = new FeeStakeHolderItem();
                        feeStakeHolderItem.setId(tsFeeStakeholder.getId());
                        feeStakeHolderItem.setGlCode(tsFeeStakeholder.getGlCode());
                        feeStakeHolderItem.setStakeholderName(tsFeeStakeholder.getStakeholderName());
                        feeStakeHolderItem.setPercentage(tempMap.getStakeholderPercentage());
                        feeStakeHolderItem.setStakeholderPaymentMethod(tempMap.getStakeholderPaymentMethod());
                        stakeHolderItems.add(feeStakeHolderItem);
                    } else {
                        logWriterUtility.error(request.getRequestId(),
                                "Stakeholder not found :" + tempMap.getStakeholderId());
                    }


                }
                feeListItem.setStakeHolders(stakeHolderItems);

                feeListItems.add(feeListItem);

            }

        }

        feeProfileFeeDetailsRoot.setFeeListItems(feeListItems);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(feeProfileFeeDetailsRoot);

        return baseResponseObject;
    }

    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    @Override
    public BaseResponseObject createStakeHolder(CreateStakeHolder request) throws PocketException {

        // check stakeholder name
        // check gl code

        if(request.getId()>0){
            return updateFeeStakeholder(request);
        }

        TsFeeStakeholder tsFeeStakeholder = tsStakeHolderRepository.findFirstByStakeholderName(request.getStakeholderName());
        if (tsFeeStakeholder != null) {
            logWriterUtility.error(request.getRequestId(), "Stakeholder name already exists");
            throw new PocketException(PocketErrorCode.StakeHolderNameAlreadyExists);
        }

        tsFeeStakeholder = tsStakeHolderRepository.findFirstByGlCode(request.getGlCode());
        if (tsFeeStakeholder != null) {
            logWriterUtility.error(request.getRequestId(), "Stakeholder gl already exists");
            throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
        }

        TsGl tsGl = tsGlRepository.findFirstByGlCode(request.getGlCode());
        if (tsGl != null) {
            logWriterUtility.error(request.getRequestId(), "Gl already exists");
            throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
        }

        if(request.getWalletNumber()!=null){

            // Check wallet exists

            List<String> clientStatus = new ArrayList<>();
            clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
            TsClient client=clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(),clientStatus);
            if(client==null){
                logWriterUtility.error(request.getRequestId(),"Wallet not found :"+request.getWalletNumber());
                throw new PocketException(PocketErrorCode.WalletNotFound);
            }
        }

        tsGl = new TsGl();
        tsGl.setGlName(request.getStakeholderName());
        tsGl.setParentGl(false);
        tsGl.setGlCode(request.getGlCode());
        tsGl.setSystemDefault(false);
        tsGl.setParentGlCode("25000000");
        tsGlRepository.save(tsGl);

        tsFeeStakeholder = new TsFeeStakeholder();
        tsFeeStakeholder.setStakeholderName(request.getStakeholderName());
        tsFeeStakeholder.setGlCode(request.getGlCode());
        tsFeeStakeholder.setWalletNumber(request.getWalletNumber());
        tsFeeStakeholder.setCreatedDate(new Date());
        tsStakeHolderRepository.save(tsFeeStakeholder);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    public BaseResponseObject updateFeeStakeholder(CreateStakeHolder request) throws PocketException {


        TsFeeStakeholder tsFeeStakeholder = tsStakeHolderRepository.findFirstById(request.getId());
        if (tsFeeStakeholder == null) {
            logWriterUtility.error(request.getRequestId(), "Stakeholder not found :"+request.getId());
            throw new PocketException(PocketErrorCode.StakeHolderNotFound);
        }

        if(request.getStakeholderName()!=null){

            TsFeeStakeholder tsFeeStakeholderByName = tsStakeHolderRepository.findFirstByStakeholderName(request.getStakeholderName());
            if (tsFeeStakeholderByName != null) {
                logWriterUtility.error(request.getRequestId(), "Stakeholder name already exists");
                throw new PocketException(PocketErrorCode.StakeHolderNameAlreadyExists);
            }
            tsFeeStakeholder.setStakeholderName(request.getStakeholderName());
        }

        if(request.getGlCode()!=null){
            TsGl tsGl = tsGlRepository.findFirstByGlCode(request.getGlCode());
            if (tsGl != null) {
                logWriterUtility.error(request.getRequestId(), "Gl already exists");
                throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
            }

            tsGl = new TsGl();
            tsGl.setGlName(request.getStakeholderName());
            tsGl.setParentGl(false);
            tsGl.setGlCode(request.getGlCode());
            tsGl.setSystemDefault(false);
            tsGl.setParentGlCode("25000000");
            tsGlRepository.save(tsGl);

            tsFeeStakeholder.setStakeholderName(request.getStakeholderName());

        }

        if(request.getWalletNumber()!=null){

            // Check wallet exists

            List<String> clientStatus = new ArrayList<>();
            clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
            TsClient client=clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(),clientStatus);
            if(client==null){
                logWriterUtility.error(request.getRequestId(),"Wallet not found :"+request.getWalletNumber());
                throw new PocketException(PocketErrorCode.WalletNotFound);
            }

            tsFeeStakeholder.setWalletNumber(request.getWalletNumber());
        }

        tsStakeHolderRepository.save(tsFeeStakeholder);


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getStakeHolder(GetStakeHolderRequest request) {
        List<TsFeeStakeholder> stakeholders = (List<TsFeeStakeholder>) tsStakeHolderRepository.findAll();
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(stakeholders);

        return baseResponseObject;
    }

    @Transactional
    @Override
    public BaseResponseObject createFeeFeeProfileServiceMap(CreateFeeProfileLimitMapRequest request) throws PocketException {

        TsFee tsFee = tsFeeRepository.findFirstByFeeCodeAndStatus(request.getFeeCode(), "1");

        if (tsFee == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee id");
            throw new PocketException(PocketErrorCode.InvalidFee);
        }

        TsFeeProfile tsFeeProfile = tsFeeProfileRepository.findFirstByProfileCodeAndStatus(request.getFeeProfileCode(), "1");

        if (tsFeeProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee profile code");
            throw new PocketException(PocketErrorCode.InvalidFeeProfileCode);
        }


        TsService tsService = tsServiceRepository.findFirstByServiceCode(request.getServiceCode());

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid fee profile id");
            throw new PocketException(PocketErrorCode.InvalidServiceCode);
        }

        // load all previous map for fee profile and service.
        // delete them
        //insert new map

        List<TsFeeProfileLimitMapping> tsFeeProfileLimitMappingMap = tsFeeProfileLimitMappingRepository.findAllByFeeProfileCodeAndServiceCode( tsFeeProfile.getProfileCode(),tsService.getServiceCode());

        if(tsFeeProfileLimitMappingMap!=null||tsFeeProfileLimitMappingMap.size()>0){
            logWriterUtility.error(request.getRequestId(),"Previous maps exists. Size :"+tsFeeProfileLimitMappingMap.size());
            tsFeeProfileLimitMappingRepository.deleteAll(tsFeeProfileLimitMappingMap);
        }


        TsFeeProfileLimitMapping tsFeeProfileLimitMapping = tsFeeProfileLimitMappingRepository.findFirstByFeeCodeAndServiceCodeAndFeeProfileCodeOrderByIdDesc(tsFee.getFeeCode(), tsService.getServiceCode(), tsFeeProfile.getProfileCode());

        if (tsFeeProfileLimitMapping != null) {
            logWriterUtility.trace(request.getRequestId(), "Map already exists");
//            throw new PocketException(PocketErrorCode.MapAlreadyExists);
        }else {
            tsFeeProfileLimitMapping = new TsFeeProfileLimitMapping();
        }

        tsFeeProfileLimitMapping.setFeeCode(tsFee.getFeeCode());
        tsFeeProfileLimitMapping.setServiceCode(tsService.getServiceCode());
        tsFeeProfileLimitMapping.setFeeProfileCode(tsFeeProfile.getProfileCode());
        tsFeeProfileLimitMapping.setCreatedDate(new Date());

        tsFeeProfileLimitMappingRepository.save(tsFeeProfileLimitMapping);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getConversionRate(GetConversionRate request) throws PocketException {

        List<TsConversionRate> tsConversionRates = null;

        if (request.getServiceCode() == null || request.getServiceCode().trim().length() == 0) {
            tsConversionRates = tsConversionRateRepository.findAllByStatusOrderByIdDesc("1");
        } else {
            TsConversionRate tsConversionRate = tsConversionRateRepository
                    .findFirstByServiceCodeAndStatusOrderByIdDesc(request.getServiceCode(), "1");

            if (tsConversionRate != null) {
                tsConversionRates = new ArrayList<>();
                tsConversionRates.add(tsConversionRate);
            }

        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setData(tsConversionRates);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject createOrUpdateConversionRate(CreateConversionRate request) throws PocketException {

        if (request.getId() > 0) {
            return updateConversionRate(request);
        }

        TsConversionRate tsConversionRate = tsConversionRateRepository.findFirstByServiceCodeAndStatusOrderByIdDesc(request.getServiceCode(), "1");
        if (tsConversionRate != null) {
            logWriterUtility.error(request.getRequestId(), "Conversion rate exists");
            throw new PocketException(PocketErrorCode.ConversionRateExists);
        }

        tsConversionRate = new TsConversionRate();
        tsConversionRate.setServiceCode(request.getServiceCode());
        tsConversionRate.setServiceName(request.getServiceName());
        tsConversionRate.setConversionRate(request.getConversionRate());
        tsConversionRate.setActualRate(request.getActualRate());
        tsConversionRate.setConversionRatio(new BigDecimal(request.getConversionRate() / request.getActualRate()));
        tsConversionRate.setStatus("1");

        tsConversionRateRepository.save(tsConversionRate);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }

    private BaseResponseObject updateConversionRate(CreateConversionRate request) throws PocketException {

        TsConversionRate tsConversionRate = tsConversionRateRepository.findFirstByServiceCodeAndStatusOrderByIdDesc(request.getServiceCode(), "1");
        if (tsConversionRate == null) {
            logWriterUtility.error(request.getRequestId(), "Conversion rate not found");
            throw new PocketException(PocketErrorCode.ConversionRateNotFound);
        }

        if (request.getActualRate() != null && tsConversionRate.getActualRate() != request.getActualRate()) {
            tsConversionRate.setActualRate(request.getActualRate());
        }

        if (request.getConversionRate() != null && tsConversionRate.getConversionRate() != request.getConversionRate()) {
            tsConversionRate.setConversionRate(request.getConversionRate());
        }

        if (request.getServiceName() != null && !tsConversionRate.getServiceName().equalsIgnoreCase(request.getServiceName())) {
            tsConversionRate.setServiceName(request.getServiceName());
        }

        /*if (request.getServiceCode() != null && !tsConversionRate.getServiceCode().equalsIgnoreCase(request.getServiceCode())) {
            tsConversionRate.setServiceCode(request.getServiceCode());
        }*/

        if (request.getStatus() != null && !tsConversionRate.getStatus().equalsIgnoreCase(request.getStatus())) {
            tsConversionRate.setStatus(request.getStatus());
        }

        tsConversionRate.setConversionRatio(new BigDecimal(request.getConversionRate()/ request.getActualRate()));

        tsConversionRateRepository.save(tsConversionRate);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }

    private double getStakeHolderPercentage(String glCode, List<FeeStakeHolderItem> stakeHolders) {

        for (FeeStakeHolderItem item : stakeHolders) {
            if (item.getGlCode().equals(glCode)) {
                return item.getPercentage();
            }
        }

        return 0;
    }
}
