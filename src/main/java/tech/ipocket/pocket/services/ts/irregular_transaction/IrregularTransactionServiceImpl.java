package tech.ipocket.pocket.services.ts.irregular_transaction;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.RefundRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.irregular_transaction.RefundResponse;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class IrregularTransactionServiceImpl implements IrregularTransactionService {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private TsTransactionRepository transactionRepository;
    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private TsGlTransactionDetailsRepository glTransactionDetailsRepository;
    @Autowired
    private TsGlRepository generalLedgerRepository;
    @Autowired
    private TsTransactionDetailsRepository transactionDetailRepository;
    @Autowired
    private AccountService clientService;
    @Autowired
    private NotificationService notificationService;

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public BaseResponseObject createRefund(RefundRequest refundRequest) throws PocketException {

        TsTransaction actualTransaction = transactionRepository.findFirstByToken(refundRequest.getTxReferenceToken());

        if(actualTransaction==null){
            logWriterUtility.error(refundRequest.getRequestId(),"No transaction found for provided token:"+refundRequest.getTxReferenceToken());
            throw new PocketException(PocketErrorCode.InvalidTransactionToken);
        }

        logWriterUtility.trace(refundRequest.getRequestId(),"Transaction Found :"+actualTransaction.getId());


        boolean isRefundPossible=actualTransaction.getTransactionStatus().equalsIgnoreCase(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus())
                ||actualTransaction.getTransactionStatus().equalsIgnoreCase(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());

        if(!isRefundPossible){
            logWriterUtility.error(refundRequest.getRequestId(),"Refund not possible for this transaction. tx Status:"+actualTransaction.getTransactionStatus());
            throw new PocketException(PocketErrorCode.RefundNotPossibleForThisTransaction);

        }
        logWriterUtility.trace(refundRequest.getRequestId(),"Refund possible for this transaction");


        validateRefundOrVoidRequest(actualTransaction, refundRequest.getRequestId());

        TsClient senderClient = clientRepository.findLastByWalletNoOrderByIdDesc(actualTransaction.getReceiverWallet());
        if (senderClient == null) {
            logWriterUtility.error(refundRequest.getRequestId(), "Sender not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        logWriterUtility.trace(refundRequest.getRequestId(),"Sender client found");


        TsClient receiverClient = clientRepository.findLastByWalletNoOrderByIdDesc(actualTransaction.getSenderWallet());

        if (receiverClient == null) {
            logWriterUtility.error(refundRequest.getRequestId(), "Receiver wallet not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(refundRequest.getRequestId(),"Receiver client found");


        TsClientBalance senderBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderClient);
        if (senderBalance == null || senderBalance.getBalance() == null || senderBalance.getBalance().compareTo(refundRequest.getAmount()) < 0) {
            logWriterUtility.error(refundRequest.getRequestId(), "Insufficient balance on sender account.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }
        logWriterUtility.trace(refundRequest.getRequestId(),"Sender has sufficient account balance");


        TsService cfeService = serviceRepository.findFirstByServiceCode(TsEnums.Services.Refund.getServiceCode());

        if (cfeService == null) {
            logWriterUtility.error(refundRequest.getRequestId(), "Refund service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }
        logWriterUtility.trace(refundRequest.getRequestId(),"Refund service found");


        // Reverse Start
        TsTransaction refundCurrentTransaction = buildRefundTransaction(actualTransaction, refundRequest,cfeService.getServiceCode());
        refundCurrentTransaction.setRequestId(refundRequest.getRequestId());
        refundCurrentTransaction.setTsClientByReceiverClientId(receiverClient);
        refundCurrentTransaction.setTsClientBySenderClientId(senderClient);
        refundCurrentTransaction.setTransactionStatus(TsEnums.TransactionStatus.REFUND.getTransactionStatus());
        transactionRepository.save(refundCurrentTransaction);

        List<TsTransactionDetails> newTxDetailList = buildReverseTransactionDetail(refundRequest, refundCurrentTransaction);
        transactionDetailRepository.saveAll(newTxDetailList);

        List<TsGlTransactionDetails> newGlTransactionDetails = buildReverseGlTransactionDetail(refundRequest, refundCurrentTransaction, actualTransaction);
        glTransactionDetailsRepository.saveAll(newGlTransactionDetails);



        // update sender and receiver balance
        BigDecimal senderCustomerRunningBalance = clientService.updateClientCurrentBalance(senderClient, refundRequest.getRequestId());
        BigDecimal receiverMerchantRunningBalance = clientService.updateClientCurrentBalance(receiverClient, refundRequest.getRequestId());

        refundCurrentTransaction.setSenderRunningBalance(senderCustomerRunningBalance);
        refundCurrentTransaction.setReceiverRunningBalance(receiverMerchantRunningBalance);
        transactionRepository.save(refundCurrentTransaction);


        actualTransaction.setRefTransactionToken(refundCurrentTransaction.getToken());
        actualTransaction.setTransactionStatus(TsEnums.TransactionStatus.REFUND.getTransactionStatus());
        actualTransaction.setIsDisputable(false);
        actualTransaction.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        transactionRepository.save(actualTransaction);


        RefundResponse refundResponse = new RefundResponse();
        refundResponse.setSenderWallet(refundCurrentTransaction.getSenderWallet());
        refundResponse.setReceiverWallet(refundCurrentTransaction.getReceiverWallet());
        refundResponse.setDateTime(String.valueOf(refundCurrentTransaction.getCreatedDate()));
        refundResponse.setFee(BigDecimal.ZERO);

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(actualTransaction.getTransactionAmount(),
                        receiverClient.getWalletNo(),
                        senderClient.getWalletNo(), actualTransaction.getToken(), cfeService.getServiceCode(),
                        "DR", BigDecimal.ZERO, new Gson().toJson(refundRequest),
                        new Gson().toJson(refundResponse), refundRequest.getRequestId(), refundRequest.getNotes());

        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        BaseResponseObject baseResponseObject = new BaseResponseObject(refundRequest.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setData(refundResponse);

        return baseResponseObject;

    }


    @Override
    @Transactional(rollbackFor = {PocketException.class, Exception.class})
    public BaseResponseObject createReversal(ReversalRequest request) throws PocketException {

        String transactionReference = request.getTransactionToken();

        TsTransaction oldTransaction = transactionRepository.findFirstByToken(transactionReference);

        if (oldTransaction == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid transaction id");
            throw new PocketException(PocketErrorCode.InvalidTransactionToken);
        }

        boolean isReversalPossible=oldTransaction.getTransactionStatus().equalsIgnoreCase(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus())
                ||oldTransaction.getTransactionStatus().equalsIgnoreCase(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());


        if (!isReversalPossible) {
            logWriterUtility.error(request.getRequestId(), "Invalid Transaction status :" + oldTransaction.getEodStatus());
            throw new PocketException(PocketErrorCode.ReversalNotPossibleForThisTransaction);
        }



        TsClient senderClient = clientRepository.findLastByWalletNoOrderByIdDesc(oldTransaction.getReceiverWallet());

        if (senderClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender wallet not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        TsClient receiverClient = clientRepository.findLastByWalletNoOrderByIdDesc(oldTransaction.getSenderWallet());

        if (receiverClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver wallet not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        TsService service = serviceRepository.findFirstByServiceCode(TsEnums.Services.Reversal.getServiceCode());

        if (service == null) {
            logWriterUtility.error(request.getRequestId(), "Services not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        String serviceId = service.getServiceCode();

        // Create new transaction
        TsTransaction newTransaction = new TsTransaction();
        newTransaction.setEodStatus(TsEnums.EodStatus.REVERSED.getEodStatus());
        newTransaction.setIsDisputable(false);
        newTransaction.setRefTransactionToken(oldTransaction.getToken());
        newTransaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        newTransaction.setNotes(request.getReason());
        newTransaction.setRequestId(request.getRequestId());
        newTransaction.setCreatedDate(new Date(System.currentTimeMillis()));

        newTransaction.setTransactionAmount(oldTransaction.getTransactionAmount());
        newTransaction.setSenderDebitAmount(oldTransaction.getReceiverCreditAmount());
        newTransaction.setReceiverCreditAmount(oldTransaction.getSenderDebitAmount());

        newTransaction.setSenderWallet(senderClient.getWalletNo());
        newTransaction.setReceiverWallet(receiverClient.getWalletNo());

        newTransaction.setLogicalSender(senderClient.getWalletNo());
        newTransaction.setLogicalReceiver(receiverClient.getWalletNo());

        newTransaction.setTransactionType("" + serviceId);
        newTransaction.setTransactionStatus(TsEnums.TransactionStatus.REVERSED.getTransactionStatus());
        transactionRepository.save(newTransaction);


        List<TsTransactionDetails> transactionDetailList = transactionDetailRepository
                .findAllByTransactionId(oldTransaction.getId());

        List<TsTransactionDetails> transactionDetailListNew = new ArrayList<>();
        for (TsTransactionDetails detail : transactionDetailList) {

            String transactionType = (detail.getTransactionType().equals(FeePayer.DEBIT)) ? FeePayer.CREDIT : FeePayer.DEBIT;

            TsTransactionDetails cfeTransactionDetail = buildCfeTransactionDetailsItem(newTransaction.getId(), transactionType,
                    detail.getWalletReference(), detail.getTransactionAmount());
            transactionDetailListNew.add(cfeTransactionDetail);
        }

        transactionDetailRepository.saveAll(transactionDetailListNew);

        List<TsGlTransactionDetails> cfeGlTransactionDetails = glTransactionDetailsRepository.findAllByTsTransactionByTransactionId(oldTransaction);

        if (cfeGlTransactionDetails == null || cfeGlTransactionDetails.size() == 0) {
            logWriterUtility.error(request.getRequestId(), "Transaction data mismatch");
            throw new PocketException(PocketErrorCode.TransactionDataMismatch);
        }

        List<TsGlTransactionDetails> cfeGlTransactionDetailsNew = new ArrayList<>();

        for (TsGlTransactionDetails glTxDetails : cfeGlTransactionDetails) {
            TsGlTransactionDetails glTransactionDetails = buildGlTransactionDetailsItem(glTxDetails.getGlCode(), newTransaction, glTxDetails.getCreditAmount(),
                    glTxDetails.getDebitCard(), TsEnums.EodStatus.REVERSED.getEodStatus());

            if (glTxDetails.getEodStatus() != null && glTxDetails.getEodStatus().equals(TsEnums.EodStatus.INITIALIZE.getEodStatus())) {
                glTxDetails.setEodStatus(TsEnums.EodStatus.REVERSED.getEodStatus());
                glTransactionDetailsRepository.save(glTxDetails);
            }

            cfeGlTransactionDetailsNew.add(glTransactionDetails);
        }

        glTransactionDetailsRepository.saveAll(cfeGlTransactionDetailsNew);


        BigDecimal senderCustomerRunningBalance = clientService.updateClientCurrentBalance(senderClient, senderClient.getWalletNo());
        BigDecimal receiverMerchantRunningBalance = clientService.updateClientCurrentBalance(receiverClient, receiverClient.getWalletNo());

        newTransaction.setSenderRunningBalance(senderCustomerRunningBalance);
        newTransaction.setReceiverRunningBalance(receiverMerchantRunningBalance);
        transactionRepository.save(newTransaction);


        oldTransaction.setEodStatus(TsEnums.EodStatus.REVERSED.getEodStatus());
        oldTransaction.setTransactionStatus("" + TsEnums.TransactionStatus.REVERSED.getTransactionStatus());
        transactionRepository.save(oldTransaction);

        //send notification
        NotificationRequest notificationRequest = buildNotificationRequest(oldTransaction.getTransactionAmount(),
                oldTransaction.getFeeAmount(),
                receiverClient.getWalletNo(),
                senderClient.getWalletNo(), newTransaction.getToken(), TsEnums.Services.Reversal.name(),
                new Gson().toJson(request), new Gson().toJson(new SuccessBoolResponse(true)), request.getRequestId(), request.getReason());
        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setStatus(PocketConstants.OK);

        return baseResponseObject;
    }
    
    private void validateRefundOrVoidRequest(TsTransaction actualTransaction, String requestId) {

    }

    private TsTransaction buildRefundTransaction(TsTransaction oldTransaction, RefundRequest refundRequest, String serviceId) {

        TsTransaction refundTransaction = new TsTransaction();

        refundTransaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        refundTransaction.setEodStatus(TsEnums.EodStatus.REVERSED.getEodStatus());

        refundTransaction.setRefTransactionToken(oldTransaction.getToken());

        refundTransaction.setReceiverWallet(oldTransaction.getSenderWallet());
        refundTransaction.setSenderWallet(oldTransaction.getReceiverWallet());

        refundTransaction.setLogicalSender(oldTransaction.getReceiverWallet());
        refundTransaction.setLogicalReceiver(oldTransaction.getSenderWallet());

        refundTransaction.setSenderDebitAmount(refundRequest.getAmount());
        refundTransaction.setReceiverCreditAmount(refundRequest.getAmount());

        refundTransaction.setFeeAmount(BigDecimal.ZERO);
        refundTransaction.setNotes("Refund of transaction :" + oldTransaction.getToken());

        refundTransaction.setTransactionAmount(refundRequest.getAmount());

        refundTransaction.setRequestId(refundRequest.getRequestId());

        refundTransaction.setCreatedDate(new Timestamp(System.currentTimeMillis()));

        // CfeTransaction Type
        refundTransaction.setTransactionType(serviceId);
//        CfeTransactionStatus transactionStatus = transactionStatusRepository.findTransactionStatusByNameAndProductCode("CAPTURED", refundRequest.getProductCode());
        refundTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
//        refundTransaction.setIsDisputeable(false);


//        refundTransaction.setAccountname(oldTransaction.getAccountname());
//        refundTransaction.setAccountnumber(oldTransaction.getAccountnumber());
        refundTransaction.setIsDisputable(false);
        refundTransaction.setCreatedDate(DateUtil.currentDate());
//        refundTransaction.setRequestOrigin(refundRequest.getClientIP());
        return refundTransaction;
    }

    private List<TsTransactionDetails> buildReverseTransactionDetail(RefundRequest refundRequest, TsTransaction newIrregularTransaction) {

        List<TsTransactionDetails> reverseTransactionDetailList = new ArrayList<>();


        TsTransactionDetails senderTxDetails = buildCfeTransactionDetailsItem(newIrregularTransaction.getId(), FeePayer.DEBIT,
                newIrregularTransaction.getSenderWallet(), refundRequest.getAmount());
        reverseTransactionDetailList.add(senderTxDetails);

        TsTransactionDetails receiverTxDetails = buildCfeTransactionDetailsItem(newIrregularTransaction.getId(), FeePayer.CREDIT,
                newIrregularTransaction.getReceiverWallet()
                , refundRequest.getAmount());
        reverseTransactionDetailList.add(receiverTxDetails);

        TsTransactionDetails feeTxDetails = buildCfeTransactionDetailsItem(newIrregularTransaction.getId(), FeePayer.CREDIT,
                "Fee"
                , BigDecimal.ZERO);
        reverseTransactionDetailList.add(feeTxDetails);
        return reverseTransactionDetailList;
    }

    private TsTransactionDetails buildCfeTransactionDetailsItem(int transactionId, String feePayer, String wallet, BigDecimal amount) {
        TsTransactionDetails tsTransactionDetails=new TsTransactionDetails();
        tsTransactionDetails.setTransactionId(transactionId);
        tsTransactionDetails.setWalletReference(wallet);
        tsTransactionDetails.setTransactionType(feePayer);
        tsTransactionDetails.setTransactionAmount(amount);
        return tsTransactionDetails;
    }

    private List<TsGlTransactionDetails> buildReverseGlTransactionDetail(RefundRequest refundRequest, TsTransaction refundCurrentTransaction,
                                                                          TsTransaction actualTransaction) {

        List<TsGlTransactionDetails> reverseTxGlList = new ArrayList<>();


        List<TsGlTransactionDetails> glOldTxDetails = glTransactionDetailsRepository.findAllByTsTransactionByTransactionId(actualTransaction);

        for (TsGlTransactionDetails transactionDetails : glOldTxDetails) {

            TsGl cfeGl = generalLedgerRepository.findFirstByGlCode(transactionDetails.getGlCode());

            if (cfeGl.getGlName().toUpperCase().contains("PAYABLE")) {
                // no fee will be added
                continue;
            }

            /*if (transactionDetails.getProcess() != null && transactionDetails.getProcess().equalsIgnoreCase("FEE_SHARED")) {
                // we will not process it
                continue;
            }*/

            TsGlTransactionDetails newGlTxDetails = new TsGlTransactionDetails();
            newGlTxDetails.setTsTransactionByTransactionId(refundCurrentTransaction);
            newGlTxDetails.setEodStatus(TsEnums.EodStatus.REVERSED.getEodStatus());

            if (transactionDetails.getCreditAmount() != null && transactionDetails.getCreditAmount().compareTo(BigDecimal.ZERO) > 0) {
                // this was receiver
                newGlTxDetails.setDebitCard(refundRequest.getAmount());
                newGlTxDetails.setCreditAmount(BigDecimal.ZERO);
                newGlTxDetails.setGlCode(transactionDetails.getGlCode());
                reverseTxGlList.add(newGlTxDetails);
                continue;
            }

            if (transactionDetails.getDebitCard() != null && transactionDetails.getDebitCard().compareTo(BigDecimal.ZERO) > 0) {
                // this was sender
                newGlTxDetails.setDebitCard(BigDecimal.ZERO);
                newGlTxDetails.setCreditAmount(refundRequest.getAmount());
                newGlTxDetails.setGlCode(transactionDetails.getGlCode());
                reverseTxGlList.add(newGlTxDetails);
                continue;
            }
        }

        return reverseTxGlList;
    }

    private NotificationRequest buildNotificationRequest(BigDecimal transactionAmount, BigDecimal feeAmount, String receiverMobileNumber,
                                                         String senderMobileNumber, String transactionId, String transactionType,
                                                         Object requestData, Object responseData, String requestId, String note) {

        NotificationRequest request = new NotificationRequest();

        if (transactionAmount == null ) {
            request.setTransactionAmount(new BigDecimal("0"));
        } else {
            request.setTransactionAmount((transactionAmount));
        }

        request.setFeeAmount(feeAmount == null ? BigDecimal.ZERO : feeAmount);


        request.setReceiverWallet(receiverMobileNumber);
        request.setSenderWalletNo(senderMobileNumber);
        request.setTransactionToken(transactionId);
        request.setServiceCode(transactionType);
        request.setRequestId(requestId);
//        request.setRequest(requestData);
//        request.setResponse(responseData);
//        request.setNote(note);
        return request;
    }

    private TsGlTransactionDetails buildGlTransactionDetailsItem(String glCode, TsTransaction transactionId,
                                                                 BigDecimal creditAmount,
                                                                 BigDecimal debitAmount, String eodStatus) {

        TsGlTransactionDetails cfeGlTransactionDetails = new TsGlTransactionDetails();
        cfeGlTransactionDetails.setGlCode(glCode);
        cfeGlTransactionDetails.setTsTransactionByTransactionId(transactionId);
        cfeGlTransactionDetails.setDebitCard(creditAmount);
        cfeGlTransactionDetails.setCreditAmount(debitAmount);
        cfeGlTransactionDetails.setEodStatus(eodStatus);

        return cfeGlTransactionDetails;
    }
}
