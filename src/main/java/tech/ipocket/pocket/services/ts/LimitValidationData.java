package tech.ipocket.pocket.services.ts;

import java.math.BigDecimal;

public class LimitValidationData {
    private String status;
    private Integer count;
    private BigDecimal amount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getAmount() {

        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
