package tech.ipocket.pocket.services.ts.irregular_transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsDispute;
import tech.ipocket.pocket.entity.TsTransaction;
import tech.ipocket.pocket.repository.ts.TsDisputeRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionRepository;
import tech.ipocket.pocket.request.ts.irregular_transaction.CreateDisputeRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.GetDisputeRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.UpdateDisputeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.irregular_transaction.TsDisputeResponse;
import tech.ipocket.pocket.utils.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TsDisputeServiceImpl implements TsDisputeService {
    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private TsDisputeRepository tsDisputeRepository;

    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private IrregularTransactionService irregularTransactionService;
    @Override
    public BaseResponseObject createDispute(CreateDisputeRequest getDisputeRequest) throws PocketException {

        TsTransaction transaction=tsTransactionRepository.findFirstByToken(getDisputeRequest.getTransactionToken());
        if(transaction==null){
            logWriterUtility.error(getDisputeRequest.getRequestId(),"Transaction not found. Token :"+getDisputeRequest.getTransactionToken());
            throw new PocketException(PocketErrorCode.InvalidTransactionToken);
        }

        logWriterUtility.trace(getDisputeRequest.getRequestId(),"Transaction Found :"+transaction.getId());

        TsDispute dispute=tsDisputeRepository.findFirstByTransactionTokenNoOrderByIdDesc(getDisputeRequest.getTransactionToken());

        if(dispute!=null){
            logWriterUtility.error(getDisputeRequest.getRequestId(),"Dispute already raised: "+dispute.getId());
            throw new PocketException(PocketErrorCode.DisputeAlreadyExistsForThisTransaction);
        }

        boolean isRequesterAuthorizeForDispute=isRequesterAuthorizeForDispute(transaction,getDisputeRequest.getRequesterWallet());

        dispute=new TsDispute();
        dispute.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        dispute.setStatus(PocketConstants.INITIALIZED);
        dispute.setReason(getDisputeRequest.getReason());
        dispute.setTransactionTokenNo(getDisputeRequest.getTransactionToken());

        dispute.setTransactionAmount(""+transaction.getTransactionAmount());
        dispute.setSenderWallet(transaction.getLogicalSender());
        dispute.setReceiverWallet(transaction.getLogicalReceiver());

        tsDisputeRepository.save(dispute);

        BaseResponseObject baseResponseObject=new BaseResponseObject(getDisputeRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateDispute(UpdateDisputeRequest getDisputeRequest) throws PocketException {
        TsDispute tsDispute=tsDisputeRepository.findFirstById(getDisputeRequest.getDisputeId());

        if(tsDispute==null){
            logWriterUtility.error(getDisputeRequest.getRequestId(),"Invalid dispute id");
            throw new PocketException(PocketErrorCode.InvalidDisputeId);
        }

        if(!tsDispute.getStatus().equalsIgnoreCase(PocketConstants.INITIALIZED)){
            logWriterUtility.error(getDisputeRequest.getRequestId(),"Invalid dispute status");
            throw new PocketException(PocketErrorCode.InvalidDisputeStatus);
        }

        if(getDisputeRequest.getDisputeStatus().equalsIgnoreCase(PocketConstants.REVERSAL)){

            ReversalRequest reversalRequest=new ReversalRequest();
            reversalRequest.setTransactionToken(tsDispute.getTransactionTokenNo());
            reversalRequest.setReason(getDisputeRequest.getComment());
            reversalRequest.setSessionToken(getDisputeRequest.getSessionToken());
            reversalRequest.setRequestId(getDisputeRequest.getRequestId());
            reversalRequest.setHardwareSignature(getDisputeRequest.getHardwareSignature());
            reversalRequest.setSource(getDisputeRequest.getSource());

            try {
                irregularTransactionService.createReversal(reversalRequest);
            }catch (PocketException e){
                logWriterUtility.error(getDisputeRequest.getRequestId(),e);
                throw e;
            }

            tsDispute.setStatus(getDisputeRequest.getDisputeStatus());
            tsDispute.setResolvedDate(new Timestamp(System.currentTimeMillis()));
            tsDispute.setResolvedMessage(getDisputeRequest.getComment());
            tsDisputeRepository.save(tsDispute);
        }else{
            tsDispute.setStatus(getDisputeRequest.getDisputeStatus());
            tsDispute.setResolvedDate(new Timestamp(System.currentTimeMillis()));
            tsDispute.setResolvedMessage(getDisputeRequest.getComment());
            tsDisputeRepository.save(tsDispute);
        }

        BaseResponseObject baseResponseObject=new BaseResponseObject(getDisputeRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(tsDispute);
        return baseResponseObject;
    }

    private boolean isRequesterAuthorizeForDispute(TsTransaction transaction, String requesterWallet) {


        return false;
    }

    @Override
    public BaseResponseObject findAllDisputeByStatus(GetDisputeRequest getDisputeRequest) {

        List<TsDispute> disputeList=tsDisputeRepository.findAllByStatusOrderByIdDesc(getDisputeRequest.getStatus());

        List<TsDisputeResponse> disputeResponse=new ArrayList<>();

        for (TsDispute tsDispute:disputeList
             ) {
            TsDisputeResponse tsDisputeResponse=new TsDisputeResponse();
            tsDisputeResponse.setId(tsDispute.getId());
            tsDisputeResponse.setReason(tsDispute.getReason());
            tsDisputeResponse.setResolvedDate(tsDispute.getResolvedMessage());
            tsDisputeResponse.setTransactionTokenNo(tsDispute.getTransactionTokenNo());
            tsDisputeResponse.setSenderWallet(tsDispute.getSenderWallet());
            tsDisputeResponse.setReceiverWallet(tsDispute.getReceiverWallet());
            tsDisputeResponse.setStatus(tsDispute.getStatus());
            tsDisputeResponse.setTransactionAmount(tsDispute.getTransactionAmount());
            if(tsDispute.getCreatedDate()!=null){
                tsDisputeResponse.setCreatedDate(DateUtil.formattedDate(tsDispute.getCreatedDate()));
            }

            if(tsDispute.getResolvedDate()!=null){
                tsDisputeResponse.setResolvedDate(DateUtil.formattedDate(tsDispute.getResolvedDate()));
            }

            disputeResponse.add(tsDisputeResponse);
        }

        BaseResponseObject baseResponseObject=new BaseResponseObject(getDisputeRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(disputeResponse);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getDisputeByTransactionToken(String txToken, String requestId) {

        TsDispute tsDispute=tsDisputeRepository.findFirstByTransactionTokenNoOrderByIdDesc(txToken);

        BaseResponseObject baseResponseObject=new BaseResponseObject(requestId);
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(tsDispute);

        return baseResponseObject;
    }
}
