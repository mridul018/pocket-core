package tech.ipocket.pocket.services.ts.transaction;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.configuration.PayWellConfig;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.transaction.TransactionResponseData;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.services.ts.irregular_transaction.IrregularTransactionService;
import tech.ipocket.pocket.services.ts.paynetService.PaynetServiceImpl;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.BillPayTypeConstants;
import tech.ipocket.pocket.utils.constants.TopUpMedium;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Service
public class MobileTopUpServiceImpl implements MobileTopUpService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;

    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TsServiceRepository serviceRepository;

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TsMerchantDetailsRepository tsMerchantDetailsRepository;
    @Autowired
    private AppConfiguration appConfiguration;
    @Autowired
    private PayWellConfig payWellConfig;

    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;

    @Autowired
    private TsPaywellOperatorMapRepository tsPaywellOperatorMapRepository;
    @Autowired
    private IrregularTransactionService irregularTransactionService;
    @Autowired
    private PaynetServiceImpl paynetService;

    @Autowired
    CommonService commonService;
    private String connection = null;
    private String operator = null;
    private BaseResponseObject baseResponseObject = null;

    @Autowired
    private TsConversionRateRepository tsConversionRateRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public BaseResponseObject doMobileTopUp(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException {

        if(request.getPrePaid()){
            if (request.getSkuCode() == null || request.getSkuCode().trim().length() == 0) {
                logWriterUtility.error(request.getRequestId(), "Sku code required");
                throw new PocketException(PocketErrorCode.SkuCodeRequired);
            }
        }

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientSender = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), statusNotIn);

        if (clientSender == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        TsMerchantDetails tsMerchantDetails = tsMerchantDetailsRepository.findFirstByShortCode(PocketConstants.TOP_UP);

        if (tsMerchantDetails == null) {
            logWriterUtility.error(request.getRequestId(), "TopUp Merchant not found");
            throw new PocketException(PocketErrorCode.TopUpMerchantNotFound);
        }

        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsMerchantDetails.getWalletNo(), statusNotIn);

        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(request.getTransactionType());

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        // Min Max validation for PostPaid top up(DU, Etisalat)
        if(request.getPrePaid() == Boolean.FALSE){
            try {
                paynetService.postPaidMinMaxValidation(request);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // calculate fee by topup medium
        FeeData feeData = null;
        String conversionFeeCode = null;


        if (request.getCurrencyCode() == null || request.getCurrencyCode().equalsIgnoreCase("AED")) {

            if (request.getCountryCode() != null && request.getCountryCode().equalsIgnoreCase("BD")) {
                if (request.getSource() == null || request.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                    feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_BD_CUSTOMER_APP.getFeeCode(),
                            new BigDecimal(request.getTransferAmount()), request.getRequestId());

                    conversionFeeCode = TsEnums.FeeCode.TOPUP_CONVERSION_DISTRIBUTION_CUSTOMER_APP.getFeeCode();

                } else {
                    feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_BD_AGENT_APP.getFeeCode(),
                            new BigDecimal(request.getTransferAmount()), request.getRequestId());
                    conversionFeeCode = TsEnums.FeeCode.TOPUP_CONVERSION_DISTRIBUTION_AGENT_APP.getFeeCode();

                }

            } else {
                if (request.getSource() == null || request.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                    feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_LOCAL_CUSTOMER_APP.getFeeCode(),
                            new BigDecimal(request.getTransferAmount()), request.getRequestId());
                    conversionFeeCode = TsEnums.FeeCode.TOPUP_CONVERSION_DISTRIBUTION_CUSTOMER_APP.getFeeCode();

                } else {
                    feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_LOCAL_AGENT_APP.getFeeCode(),
                            new BigDecimal(request.getTransferAmount()), request.getRequestId());
                    conversionFeeCode = TsEnums.FeeCode.TOPUP_CONVERSION_DISTRIBUTION_AGENT_APP.getFeeCode();

                }
            }
        } else if (request.getCurrencyCode().equalsIgnoreCase("BDT")) {
            if (request.getSource() == null || request.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_BD_CUSTOMER_APP.getFeeCode(),
                        new BigDecimal(request.getTransferAmount()), request.getRequestId());
            } else {
                feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_BD_AGENT_APP.getFeeCode(),
                        new BigDecimal(request.getTransferAmount()), request.getRequestId());
            }
        } else {
            if (request.getSource() == null || request.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_LOCAL_CUSTOMER_APP.getFeeCode(),
                        new BigDecimal(request.getTransferAmount()), request.getRequestId());
            } else {
                feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.TOPUP_LOCAL_AGENT_APP.getFeeCode(),
                        new BigDecimal(request.getTransferAmount()), request.getRequestId());
            }
        }

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransferAmount()));

        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");

        //transaction profile validation for sender + check balance limit for sender
        Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSender, cfeService, transactionAmountData.getSenderDebitAmount(),
                request.getRequestId());
        if (!isSenderProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
            throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
        }

        //transaction profile validation for receiver
        Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(clientReceiver, cfeService,
                request.getRequestId());
        if (!isReceiverProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
            throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction tsTransaction = transactionBuilder.buildTransactionTopUp(clientSender, clientReceiver, cfeService.getServiceCode(),
                feeData, transactionAmountData, request.getReceiverMobileNumber(), request.getSource(), request);

        tsTransaction.setTopupReceiverCountryCode(request.getCountryCode());
        tsTransaction.setTopupSkuCode(request.getSkuCode());

        tsTransactionRepository.save(tsTransaction);

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();

        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(tsTransaction.getId(), FeePayer.DEBIT,
                clientSender.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(tsTransaction.getId(), FeePayer.CREDIT,
                clientReceiver.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(tsTransaction.getId(), FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();

        String senderGlName = CommonTasks.getGlNameByCustomerType(clientSender);

        TsGl senderGl = tsGlRepository.findFirstByGlName(senderGlName);

        if (senderGl == null) {
            logWriterUtility.error(request.getRequestId(), "Sender gl not found");
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(tsTransaction,
                senderGlName, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        String receiverGlName = CommonTasks.getGlNameByCustomerType(clientReceiver);

        TsGl receiverGl = tsGlRepository.findFirstByGlName(receiverGlName);

        if (receiverGl == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver gl not found :" + receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(tsTransaction,
                receiverGlName, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(tsTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(tsTransaction,
                GlConstants.Fee_payable_for_TopUp, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(tsTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        tsGlTransactionDetailsRepository.saveAll(glTxDetailList);

        // update balance
        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSender, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(clientReceiver, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
        tsTransaction.setSenderRunningBalance(senderRunningBalance);
        tsTransaction.setReceiverRunningBalance(receiverRunningBalance);
        tsTransactionRepository.save(tsTransaction);


        BigDecimal pocketConversionProfit = BigDecimal.ZERO;

        if (request.getCurrencyCode() == null || request.getCurrencyCode().equalsIgnoreCase("BDT")) {
            BigInteger receiverCreditAmount = new BigInteger("" + transactionAmountData
                    .getReceiverCreditAmount().intValue());
            BigInteger convertedAmount;
            convertedAmount = new BigInteger("" + (request.getConvertedAmount()));

            String crid = tsTransaction.getToken();
            String msisdn = request.getReceiverMobileNumber();

            connection = (request.getPrePaid() == null ||
                    request.getPrePaid()) ? "prepaid" : "postpaid";


            TsPaywellOperatorMap tsPaywellOperatorMap = tsPaywellOperatorMapRepository.findFirstByOperatorCodeAndStatus(request.getSkuCode(), "1");
            if (tsPaywellOperatorMap == null) {
                logWriterUtility.trace(request.getRequestId(), "Invalid sku code :" + request.getSkuCode());
                tsTransaction.setNotes("Invalid operator :" + request.getSkuCode());
                tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                tsTransactionRepository.save(tsTransaction);
                throw new PocketException(PocketErrorCode.InvalidOperatorSelected);
            }
            operator = tsPaywellOperatorMap.getPaywellCode();


            String sender = request.getSenderMobileNumber();
            String type = ExternalType.PAYWELL_TOPUP;

            baseResponseObject = new ExternalCall(appConfiguration.getExternalUrl())
                    .sendPayWellSingleTopUp(payWellConfig, crid, request.getSenderMobileNumber(), msisdn,
                            convertedAmount, connection, operator, sender, type);


        } else if (request.getCurrencyCode().equalsIgnoreCase("AED")) {
            TsConversionRate tsConversionRate = tsConversionRateRepository
                    .findFirstByServiceCodeAndStatusOrderByIdDesc(BillPayTypeConstants.TOPUP, "1");

            BigInteger receiverCreditAmount = new BigInteger("" + request.getConvertedAmount());

            if (request.getCountryCode().equalsIgnoreCase("BD")) {
                BigInteger convertedAmount;
                Double actualConvertedAmount = (double) 0;

                if (tsConversionRate == null) {
                    convertedAmount = new BigInteger("" + request.getTransferAmount() * 20);
                    actualConvertedAmount = Double.valueOf(convertedAmount.toString());
                } else {
                    Double amount = (request.getTransferAmount() * tsConversionRate.getConversionRate());
                    convertedAmount = new BigInteger("" + (amount.intValue()));

                    Double actualAmount = (request.getTransferAmount() * tsConversionRate.getActualRate());
                    actualConvertedAmount = Double.valueOf(actualAmount.toString());
                }

                if (tsConversionRate != null) {
                    Double value = (actualConvertedAmount - Double.valueOf("" + convertedAmount)) / tsConversionRate.getActualRate();
                    pocketConversionProfit = new BigDecimal(value);
                }

                String crid = tsTransaction.getToken();
                String msisdn = request.getReceiverMobileNumber();

                connection = (request.getPrePaid() == null ||
                        request.getPrePaid()) ? "prepaid" : "postpaid";

                TsPaywellOperatorMap tsPaywellOperatorMap = tsPaywellOperatorMapRepository.findFirstByOperatorCodeAndStatus(request.getSkuCode(), "1");
                if (tsPaywellOperatorMap == null) {
                    logWriterUtility.trace(request.getRequestId(), "Invalid sku code :" + request.getSkuCode());
                    tsTransaction.setNotes("Invalid operator :" + request.getSkuCode());
                    tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
                    tsTransactionRepository.save(tsTransaction);

                    //If Transaction failed then create balance Reversal
                    logWriterUtility.debug(request.getRequestId(), "Balance Reversal for FAILED Transaction");
                    ReversalRequest reversalRequest = new ReversalRequest();
                    reversalRequest.setReason("Unsuccessful topup for system internal failure");
                    reversalRequest.setTransactionToken(tsTransaction.getToken());
                    try {
                        irregularTransactionService.createReversal(reversalRequest);
                    } catch (PocketException e) {
                        e.printStackTrace();
                    }
                    throw new PocketException(PocketErrorCode.InvalidOperatorSelected);
                }
                operator = tsPaywellOperatorMap.getPaywellCode();


                String sender = request.getSenderMobileNumber();
                String type = ExternalType.PAYWELL_TOPUP;


                baseResponseObject = new ExternalCall(appConfiguration.getExternalUrl())
                        .sendPayWellSingleTopUp(payWellConfig, crid, request.getSenderMobileNumber(), msisdn,
                                convertedAmount, connection, operator, sender, type);


            } else {
                if(request.getPrePaid()) {
                    baseResponseObject = new ExternalCall(appConfiguration.getExternalUrl())
                            .sendTopUp(request.getSkuCode(), receiverCreditAmount,
                                    request.getSenderMobileNumber(), request.getReceiverMobileNumber(),
                                    tsTransaction.getToken(), ExternalType.TOPUP_DING, request.getTransferAmount());
                }else{
                    baseResponseObject = paynetService.doMobilePostPaidTopup(request,tsTransaction);
                }
            }


        } else {

        }

        logWriterUtility.trace("Mobile wsdl service", new Gson().toJson(baseResponseObject));

        boolean isReversalRequired = false;

        if (baseResponseObject == null || baseResponseObject.getStatus() == null ||
                !baseResponseObject.getStatus().equals(PocketConstants.OK)) {

            isReversalRequired = true;

            logWriterUtility.error(request.getRequestId(), "TopUp external response failed");

//            tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.FAILED.getTransactionStatus());

            if (baseResponseObject == null) {
                baseResponseObject = new BaseResponseObject(request.getRequestId());
                tsTransaction.setNotes("Topup external response failed. Response Code. No status code provided");
                baseResponseObject.setError(new ErrorObject("400", "TopUp failed"));
            } else {
                if (baseResponseObject.getStatus() == null) {
                    tsTransaction.setNotes("TopUp external response failed. Response Code. No status code provided");

                    if (baseResponseObject.getError() == null) {
                        baseResponseObject.setError(new ErrorObject("400", "TopUp failed"));
                    } else {
                        baseResponseObject.setError(new ErrorObject(PocketErrorCode.TopUpFailed.getKeyString(),
                                "TopUp failed. Reason : " + baseResponseObject.getError().getErrorMessage()));
                    }

                } else {
                    tsTransaction.setNotes("TopUp external response failed. Response Code :" + baseResponseObject.getStatus() + " Reason :" + baseResponseObject.getError().getErrorMessage());

                    baseResponseObject.setError(new ErrorObject(PocketErrorCode.TopUpFailed.getKeyString(),
                            "TopUp failed. Reason :" + baseResponseObject.getError().getErrorMessage()));
                }
            }
        } else {
            tsTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
        }

        tsTransactionRepository.save(tsTransaction);

        if (isReversalRequired) {

            logWriterUtility.debug(request.getRequestId(), "Reversal required.");

            ReversalRequest reversalRequest = new ReversalRequest();
            reversalRequest.setReason("Unsuccessful topup via API");
            reversalRequest.setTransactionToken(tsTransaction.getToken());
            try {
                irregularTransactionService.createReversal(reversalRequest);
            } catch (PocketException e) {
                e.printStackTrace();
            }

            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }

        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setSenderRunningBalance(senderRunningBalance);
        fundTransferResponseData.setTxToken(tsTransaction.getToken());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(transactionAmountData.getOriginalTransactionAmount(),
                        request.getReceiverMobileNumber(),
                        clientSender.getWalletNo(), tsTransaction.getToken(), cfeService.getServiceCode(),
                        feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes());

        CompletableFuture.runAsync(() ->
                notificationService.sendNotification(notificationRequest)
        );


        logWriterUtility.trace(request.getRequestId(), "pocketConversionProfit :" + pocketConversionProfit);
        if (pocketConversionProfit.compareTo(BigDecimal.ZERO) > 0) {
            mfsFeeSharingService.shareConversionProfitForIndividualTransaction(conversionFeeCode, pocketConversionProfit, tsTransaction.getToken(),
                    request.getRequestId());

        }

        CompletableFuture.runAsync(() -> {
            logWriterUtility.info(request.getRequestId(), "doFeeShareForIndividualTransaction :" + tsTransaction.getToken());
            logWriterUtility.info(request.getRequestId(), "tx status  :" + tsTransaction.getTransactionStatus());

            mfsFeeSharingService.doFeeShareForIndividualTransaction(tsTransaction.getToken(),
                    request.getRequestId());
        });

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(fundTransferResponseData);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getTopUpOperators(String countryCode, String requestId) throws PocketException {

//        Set<String> operators=new CustomQuery(entityManager).getDistinctOperators(requestId);
        Set<String> operators = new CustomQuery(entityManager).getDistinctOperatorsName(requestId, countryCode);

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(operators);
        return baseResponseObject;
    }
}
