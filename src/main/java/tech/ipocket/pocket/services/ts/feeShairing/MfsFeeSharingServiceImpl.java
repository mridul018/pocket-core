package tech.ipocket.pocket.services.ts.feeShairing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.fee_share.GetSharedFeeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.FeeSharingRecord;
import tech.ipocket.pocket.response.ts.FeeSharingTransactionDetailsResponse;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.StakeholderPaymentMethod;
import tech.ipocket.pocket.utils.constants.TransactionServiceCodeConstants;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;

@Service
public class MfsFeeSharingServiceImpl implements MfsFeeSharingService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());


    @Autowired
    private TsFeeFeeStakeholderMapRepository stakeholderFeeProductMappingRepository;


    @Autowired
    private TsGlTransactionDetailsRepository glTransactionDetailsRepository;


    @Autowired
    private TsTransactionRepository transactionRepository;

    @Autowired
    private TsFeeShairingRecordDetailsRepository feeSharingRecordDetailsRepository;

    @Autowired
    private Environment environment;

    @Autowired
    private TsGlRepository generalLedgerRepository;
    @Autowired
    private TsFeeStakeholderRepository feeStakeholderRepository;

    @Autowired
    private MfsSecondPartTransactionService mfsSecondPartTransactionService;

    @Autowired
    private TsFeeSharingRecordRepository cfeFeeSharingRecordRepository;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private TsClientRepository tsClientRepository;
    @Autowired
    private TsFeeRepository feeRepository;


    @Override
    public BaseResponseObject exploreFeeSharingItems() {

        String requestId = UUID.randomUUID().toString();

        List<String> successStatusList = new ArrayList<>();
        successStatusList.add(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
        successStatusList.add(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());


            List<Integer> successTxList = new ArrayList<>();
            List<Integer> failedTxList = new ArrayList<>();
            List<Integer> fatalTxList = new ArrayList<>();


            long startTime = System.currentTimeMillis();

            List<TsGlTransactionDetails> glTransactionDetailsList = glTransactionDetailsRepository.findAllByEodStatus("I");

            logWriterUtility.trace(requestId, "Fee share explored :" + glTransactionDetailsList.size());

            for (TsGlTransactionDetails glTransactionDetails : glTransactionDetailsList) {
                TsTransaction cfeTransaction = null;

                cfeTransaction = transactionRepository.findCfeTransactionByIdAndTransactionStatusInAndEodStatus
                        (glTransactionDetails.getTsTransactionByTransactionId().getId(), successStatusList, "I");

                if (cfeTransaction == null) {
                    fatalTxList.add(glTransactionDetails.getTsTransactionByTransactionId().getId());
                    logWriterUtility.trace(requestId, "Transaction id " + glTransactionDetails.getTsTransactionByTransactionId().getId() +
                            " is not found in Transaction table for status " + successStatusList);
                    continue;
                }
                logWriterUtility.trace(requestId, "EOD started for Tr id : " + cfeTransaction.getId());

                BaseResponseObject baseResponseObject = null;
                baseResponseObject = doFeeShareForIndividualTransaction(cfeTransaction.getToken(), requestId);

                if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
                    logWriterUtility.error(requestId, "Fee sharing failed for transaction ID:" + cfeTransaction.getId());
                    failedTxList.add(cfeTransaction.getId());
                } else {
                    logWriterUtility.error(requestId, "Fee sharing successful for transaction ID:" + cfeTransaction.getId() + "" +
                            "Message :" + baseResponseObject.getData().toString());

                    successTxList.add(cfeTransaction.getId());
                }
            }

            long endTime = System.currentTimeMillis();
            long totalTime = (endTime - startTime);

            logWriterUtility.trace(requestId, "Total time take :" + totalTime + " ms");

            Set<Integer> totalNoOfTx = new HashSet<>();
            Set<Integer> totalNoOFFeeShared = new HashSet<>();

            for (TsGlTransactionDetails glTransactionDetails : glTransactionDetailsList) {
                totalNoOfTx.add(glTransactionDetails.getTsTransactionByTransactionId().getId());
                if (successTxList.contains(glTransactionDetails.getTsTransactionByTransactionId().getId())) {
                    totalNoOFFeeShared.add(glTransactionDetails.getTsTransactionByTransactionId().getId());
                }
            }

            logWriterUtility.trace(requestId, "Total number of tx :" + totalNoOfTx.size());
            logWriterUtility.trace(requestId, "Total number of fee shared:" + totalNoOFFeeShared.size());


            updateEodStatisticsForFeeSharing( totalNoOfTx.size(), totalNoOFFeeShared.size(), startTime, endTime, requestId);
            //sendReport( totalNoOfTx.size(), totalNoOFFeeShared.size(), failedTxList.size(), fatalTxList.size(), startTime, endTime);


        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData("Fee sharing successful");

        return baseResponseObject;
    }

//    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public BaseResponseObject doFeeShareForIndividualTransaction(String mainTransactionReference, String requestId) {

        logWriterUtility.debug(requestId,"*****  doFeeShareForIndividualTransaction started ***** :"+mainTransactionReference);



        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);


        List<String> statusList = new ArrayList<>();
        statusList.add(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
        statusList.add(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());

//        TsTransaction mainTransaction = transactionRepository.findFirstByTokenAndTransactionStatusInAndEodStatus
//                (mainTransactionReference, statusList, TsEnums.EodStatus.INITIALIZE.getEodStatus());

        TsTransaction mainTransaction = transactionRepository.findFirstByToken(mainTransactionReference);

        if (mainTransaction == null) {
            logWriterUtility.error(requestId, "Invalid transaction token provide :" + mainTransactionReference);
            baseResponseObject.setData("Invalid transaction token provide :" + mainTransactionReference);
            return baseResponseObject;
        }

        logWriterUtility.trace(requestId, "Transaction found based on provided token :" + mainTransaction.getId());


        TsGlTransactionDetails cfeGlTransactionDetails = glTransactionDetailsRepository.findFirstByTsTransactionByTransactionIdAndEodStatus
                (mainTransaction, TsEnums.EodStatus.INITIALIZE.getEodStatus());

        if (cfeGlTransactionDetails == null) {
            logWriterUtility.error(requestId, "No non fee sharable gl transaction found for transaction ID :" + mainTransaction.getId());
            baseResponseObject.setData("No non fee sharable gl transaction found for transaction ID :" + mainTransaction.getId());
            return baseResponseObject;
        }

        logWriterUtility.trace(requestId, "Fee sharable gl transaction found:" + cfeGlTransactionDetails.getId() + " Amount :" + cfeGlTransactionDetails.getCreditAmount());

        DecimalFormat df = new DecimalFormat("############.####");

        if (cfeGlTransactionDetails.getCreditAmount() == null
                || cfeGlTransactionDetails.getCreditAmount().compareTo(BigDecimal.ZERO) == 0) {

            // 0 credit --> so no transaction required

            cfeGlTransactionDetails.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(cfeGlTransactionDetails);

            mainTransaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(mainTransaction);

            logWriterUtility.trace(requestId, "Fee amount Zero for transaction id " + cfeGlTransactionDetails.getTsTransactionByTransactionId().getId());
            baseResponseObject.setData("Fee amount Zero");

            return baseResponseObject;
        }


        List<TsFeeFeeStakeholderMap> stakeHolderFeeProductMapList =
                stakeholderFeeProductMappingRepository.findAllByFeeProductCode(mainTransaction.getFeeCode());


        TsGl unresolvedGl = generalLedgerRepository.findFirstByGlName(GlConstants.Fee_payable_for_Unresolved);
        String unresolvedGL = unresolvedGl.getGlCode();

        if (stakeHolderFeeProductMapList == null || stakeHolderFeeProductMapList.size() == 0) {

            // no stakeholder found
            // debit this fee amount from gl
            // credit that amount into unresolved gl

            logWriterUtility.trace(requestId, "No stakeholder found for transaction : " + mainTransaction.getId());
            logWriterUtility.trace(requestId, "No stakeholder found for fee id : " + mainTransaction.getFeeCode());
            logWriterUtility.trace(requestId, "Total fee amount : " + cfeGlTransactionDetails.getCreditAmount());

            TsGlTransactionDetails debitFeeAmount = new TsGlTransactionDetails();
            debitFeeAmount.setTsTransactionByTransactionId(mainTransaction);
            debitFeeAmount.setGlCode(cfeGlTransactionDetails.getGlCode());
            debitFeeAmount.setDebitCard(cfeGlTransactionDetails.getCreditAmount());
//            debitFeeAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(debitFeeAmount);

            TsGlTransactionDetails unresolvedGlCreditAmount = new TsGlTransactionDetails();
            unresolvedGlCreditAmount.setTsTransactionByTransactionId(mainTransaction);
            unresolvedGlCreditAmount.setGlCode(unresolvedGL);
            unresolvedGlCreditAmount.setCreditAmount(cfeGlTransactionDetails.getCreditAmount());
//            unresolvedGlCreditAmount.setProcess("FEE_SHARED");
//            unresolvedGlCreditAmount.setProductCode(mainTransaction.getProductCode());

            glTransactionDetailsRepository.save(unresolvedGlCreditAmount);

            cfeGlTransactionDetails.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(cfeGlTransactionDetails);

            mainTransaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(mainTransaction);

            logWriterUtility.trace(requestId, "Fee moved to Unresolved GL for " + mainTransaction.getId());
            baseResponseObject.setData("Fee moved to unresolved gl. Amount :" + cfeGlTransactionDetails.getCreditAmount());
            return baseResponseObject;
        }


        logWriterUtility.trace(requestId, "Total stakeholder size :" + stakeHolderFeeProductMapList.size());

        BigDecimal stakeHolderTotalPercentage = BigDecimal.ZERO;
        for (TsFeeFeeStakeholderMap stakeholderFeeProductMapping : stakeHolderFeeProductMapList) {
            stakeHolderTotalPercentage = stakeHolderTotalPercentage.add(new BigDecimal(stakeholderFeeProductMapping.getStakeholderPercentage()));
        }

        stakeHolderTotalPercentage = stakeHolderTotalPercentage.setScale(4, BigDecimal.ROUND_HALF_UP);


        logWriterUtility.trace(requestId, "Stakeholder total percentage :" + stakeHolderTotalPercentage);


        // if stakeholder percentage is >100 then it means fee stakeholder configuration is wrong
        // Send that transaction amount into unresolved gl account
        if (stakeHolderTotalPercentage.compareTo(new BigDecimal(100)) > 0) {
            logWriterUtility.trace(requestId, "Stakeholder percentage is not Configured properly for Transaction " + mainTransaction.getId());
            logWriterUtility.trace(requestId, "Wrong stakeholder percentage for fee id  : " + mainTransaction.getFeeCode());

            TsGlTransactionDetails debitFeeAmount = new TsGlTransactionDetails();
            debitFeeAmount.setTsTransactionByTransactionId(mainTransaction);
            debitFeeAmount.setGlCode(cfeGlTransactionDetails.getGlCode());
            debitFeeAmount.setDebitCard(cfeGlTransactionDetails.getCreditAmount());
            debitFeeAmount.setCreditAmount(BigDecimal.ZERO);
//            debitFeeAmount.setProcess("FEE_SHARED");
//            debitFeeAmount.setProductCode(mainTransaction.getProductCode());

            glTransactionDetailsRepository.save(debitFeeAmount);

            TsGlTransactionDetails unresolvedGlCreditAmount = new TsGlTransactionDetails();
            unresolvedGlCreditAmount.setTsTransactionByTransactionId(mainTransaction);
            unresolvedGlCreditAmount.setGlCode(unresolvedGL);
            unresolvedGlCreditAmount.setCreditAmount(cfeGlTransactionDetails.getCreditAmount());
            unresolvedGlCreditAmount.setDebitCard(BigDecimal.ZERO);
//            unresolvedGlCreditAmount.setProcess("FEE_SHARED");
//            unresolvedGlCreditAmount.setProductCode(mainTransaction.getProductCode());

            glTransactionDetailsRepository.save(unresolvedGlCreditAmount);

            cfeGlTransactionDetails.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(cfeGlTransactionDetails);

            mainTransaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(mainTransaction);

            logWriterUtility.trace(requestId, "Fee moved to Unresolved GL for " + mainTransaction.getId());
            baseResponseObject.setData("Stakeholder percentage is not Configured properly so fee moved to unresolved gl. Amount :" + cfeGlTransactionDetails.getCreditAmount());
            return baseResponseObject;
        } else {
            // now fee stakeholder configuration is ok
            // debit total credited  fee for gl
            // credit each stakeholder with their configured amount with decimal format in gl
            // truncated amount after decimal format will be added into unresolved gl

            logWriterUtility.trace(requestId, "Stakeholder percentage is correct. EOD starting for transaction : " + mainTransaction.getId());
            BigDecimal totalFeeSharedSum = new BigDecimal(0);
            BigDecimal totalCreditedAmount = cfeGlTransactionDetails.getCreditAmount();

            String senderFeeGlCode = "" + cfeGlTransactionDetails.getGlCode();

            List<FeeSharingTransactionDetailsResponse> feeSharingTransactionDetailsResponses = new ArrayList<>();

            for (TsFeeFeeStakeholderMap cfeStakeholderFeeProductMapping : stakeHolderFeeProductMapList) {

                TsFeeStakeholder feeStakeholder = feeStakeholderRepository.findFirstById(cfeStakeholderFeeProductMapping.getStakeholderId());

                BigDecimal currentStakeholderFee = (totalCreditedAmount.multiply(new BigDecimal(cfeStakeholderFeeProductMapping.getStakeholderPercentage())))
                        .divide(BigDecimal.valueOf(100));

                currentStakeholderFee = new BigDecimal(df.format(currentStakeholderFee.setScale(4, RoundingMode.DOWN)));


                logWriterUtility.trace(requestId, "Stakeholder payment method :" + cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod());


                FeeSharingTransactionDetailsResponse txDetailsResponse = null;


                switch (mainTransaction.getTransactionType()) {

                    case TransactionServiceCodeConstants.TOP_UP:

                        if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod() == null ||
                                cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.GL_TRANSACTION)) {

                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "GL transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToGl(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.WALLET_TRANSACTION)) {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Wallet transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWallet(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_SR_WALLET)) {

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWalletForSR(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());
                        }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_TOPUP_AGENT_WALLET)) {

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWalletForTopUpAgent(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());
                        }
                        else {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Based on transaction logic");
                        }

                        if (txDetailsResponse == null) {
                            logWriterUtility.error(requestId, "Found transaction response null for stake holder :" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            continue;
                        }

                        if (!txDetailsResponse.isSuccess()) {
                            logWriterUtility.error(requestId, "Fee sharing failed " + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber() + "" +
                                    " Reason :" + txDetailsResponse.getFailureReason());
                            continue;
                        } else {
                            logWriterUtility.trace(requestId, "Fee sharing success" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            totalFeeSharedSum = totalFeeSharedSum.add(currentStakeholderFee);
                        }

                        break;

                    case TransactionServiceCodeConstants.FUND_TRANSFER:
                        if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod() == null ||
                                cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.GL_TRANSACTION)) {

                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "GL transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToGl(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.WALLET_TRANSACTION)) {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Wallet transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWallet(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Based on transaction logic");
                        }

                        if (txDetailsResponse == null) {
                            logWriterUtility.error(requestId, "Found transaction response null for stake holder :" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            continue;
                        }

                        if (!txDetailsResponse.isSuccess()) {
                            logWriterUtility.error(requestId, "Fee sharing failed" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber() + "" +
                                    "Reason :" + txDetailsResponse.getFailureReason());
                            continue;
                        } else {
                            logWriterUtility.trace(requestId, "Fee sharing success" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            totalFeeSharedSum = totalFeeSharedSum.add(currentStakeholderFee);
                        }

                        break;

                    case TransactionServiceCodeConstants.AGENT_CASH_IN:
                    case TransactionServiceCodeConstants.AGENT_CASH_OUT:
                        if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod() == null ||
                                cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.GL_TRANSACTION)) {

                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "GL transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToGl(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.WALLET_TRANSACTION)) {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Wallet transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWallet(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_SR_WALLET)) {

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWalletForSR(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());
                        }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_AGENT_MERCHANT_WALLET)) {

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWalletForAgentMerchant(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());
                        }else {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Based on transaction logic");
                        }

                        if (txDetailsResponse == null) {
                            logWriterUtility.error(requestId, "Found transaction response null for stake holder :" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            continue;
                        }

                        if (!txDetailsResponse.isSuccess()) {
                            logWriterUtility.error(requestId, "Fee sharing failed" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber() + "" +
                                    "Reason :" + txDetailsResponse.getFailureReason());
                            continue;
                        } else {
                            logWriterUtility.trace(requestId, "Fee sharing success" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            totalFeeSharedSum = totalFeeSharedSum.add(currentStakeholderFee);
                        }

                        break;

                    case TransactionServiceCodeConstants.BILL_PAY:
                    case TransactionServiceCodeConstants.MERCHANT_PAYMENT:
                        if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod() == null ||
                                cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.GL_TRANSACTION)) {

                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "GL transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToGl(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.WALLET_TRANSACTION)) {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Wallet transaction");

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWallet(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());

                        } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_SR_WALLET)) {

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWalletForSR(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());
                        }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_AGENT_MERCHANT_WALLET)) {

                            txDetailsResponse = mfsSecondPartTransactionService
                                    .feeShareGlToDirectWalletForAgentMerchant(feeStakeholder, senderFeeGlCode,
                                            mainTransaction.getToken(), currentStakeholderFee, requestId, mainTransaction.getFeeAmount());
                        }else {
                            logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Based on transaction logic");
                        }

                        if (txDetailsResponse == null) {
                            logWriterUtility.error(requestId, "Found transaction response null for stake holder :" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            continue;
                        }

                        if (!txDetailsResponse.isSuccess()) {
                            logWriterUtility.error(requestId, "Fee sharing failed" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber() + "" +
                                    "Reason :" + txDetailsResponse.getFailureReason());
                            continue;
                        } else {
                            logWriterUtility.trace(requestId, "Fee sharing success" + feeStakeholder.getGlCode() + " Wallet :" + feeStakeholder.getWalletNumber());
                            totalFeeSharedSum = totalFeeSharedSum.add(currentStakeholderFee);
                        }

                        break;


                }

                feeSharingTransactionDetailsResponses.add(txDetailsResponse);
            }


            if(feeSharingTransactionDetailsResponses.size()>0){
                saveFeeSharingSummery(mainTransaction.getToken(), mainTransaction.getTransactionAmount(), mainTransaction.getFeeAmount(),
                        feeSharingTransactionDetailsResponses, requestId);
                baseResponseObject.setData("Fee sharing successful. Transaction Size :" + feeSharingTransactionDetailsResponses.size());

            }else{
                baseResponseObject.setData("Fee sharing failed. No stake holder found");
            }



            if (totalCreditedAmount.compareTo(totalFeeSharedSum) > 0) {

                BigDecimal unresolvedFee=totalCreditedAmount.subtract(totalFeeSharedSum);

                // truncated amount after decimal format will be added in unresolved gl

                TsGlTransactionDetails debitFeeAmount = new TsGlTransactionDetails();
                debitFeeAmount.setTsTransactionByTransactionId(mainTransaction);
                debitFeeAmount.setGlCode(cfeGlTransactionDetails.getGlCode());
                debitFeeAmount.setDebitCard(unresolvedFee);
                debitFeeAmount.setCreditAmount(BigDecimal.ZERO);

                glTransactionDetailsRepository.save(debitFeeAmount);

                TsGlTransactionDetails unresolvedGlCreditAmount = new TsGlTransactionDetails();
                unresolvedGlCreditAmount.setTsTransactionByTransactionId(mainTransaction);
                unresolvedGlCreditAmount.setGlCode(unresolvedGL);
                unresolvedGlCreditAmount.setCreditAmount(unresolvedFee);
                unresolvedGlCreditAmount.setDebitCard(BigDecimal.ZERO);

                glTransactionDetailsRepository.save(unresolvedGlCreditAmount);
            }

            cfeGlTransactionDetails.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(cfeGlTransactionDetails);

            mainTransaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(mainTransaction);


        }

        logWriterUtility.trace(requestId,"Fee sharing done :"+mainTransactionReference);

        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(requestId);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getSharedFee(GetSharedFeeRequest request) throws PocketException {

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());

        Date startDate = DateUtil.parseDate(request.getStartDate());
        Date endDate = DateUtil.parseDate(request.getEndDate());
        endDate = DateUtil.addDay(endDate, 1);


        List<TsFeeSharingRecord> feeSharingRecords= (List<TsFeeSharingRecord>) cfeFeeSharingRecordRepository.findAllByShareDateBetween(startDate,endDate);

        if(feeSharingRecords!=null && feeSharingRecords.size()>0){
            List<FeeSharingRecord> feeSharingRecordList=new ArrayList<>();

            for (TsFeeSharingRecord tsFeeSharingRecord:feeSharingRecords
                 ) {

                FeeSharingRecord record=new FeeSharingRecord();
                record.setMainTxToken(tsFeeSharingRecord.getMainTxToken());
                record.setFeeTxToken(tsFeeSharingRecord.getFeeTxToken());
                record.setFeeShareMethod(tsFeeSharingRecord.getFeeShareMethod());
                record.setReceiverAccount(tsFeeSharingRecord.getReceiverAccount());
                record.setSenderAccount(tsFeeSharingRecord.getSenderAccount());
                record.setShareDate(tsFeeSharingRecord.getShareDate());
                record.setStakeholderFeeAmount(tsFeeSharingRecord.getStakeholderFeeAmount());
                record.setTotalFeeAmount(tsFeeSharingRecord.getTotalFeeAmount());
                record.setTotalTxAmount(tsFeeSharingRecord.getTotalTxAmount());

                TsService tsService=serviceRepository.findFirstByServiceCode(tsFeeSharingRecord.getMainTxType());
                if(tsService!=null){
                    record.setMainTxType(tsService.getDescription());
                }else {
                    record.setMainTxType(tsFeeSharingRecord.getMainTxType());
                }

                TsGl senderGl=generalLedgerRepository.findFirstByGlCode(tsFeeSharingRecord.getSenderAccount());
                if(senderGl!=null){
                    record.setSenderGlName(senderGl.getGlName());
                }else{
                    record.setSenderGlName(tsFeeSharingRecord.getSenderAccount());
                }

                if(tsFeeSharingRecord.getFeeShareMethod()==null||
                        tsFeeSharingRecord.getFeeShareMethod().equalsIgnoreCase("DirectGl")){

                    if(tsFeeSharingRecord.getReceiverAccount()!=null){

                        TsGl receiverGl=generalLedgerRepository.findFirstByGlCode(tsFeeSharingRecord.getReceiverAccount());
                        if(receiverGl!=null){
                            record.setReceiverGlName(receiverGl.getGlName());
                        }else{
                            record.setReceiverGlName(tsFeeSharingRecord.getReceiverAccount());
                        }
                    }

                }else{

                    if(tsFeeSharingRecord.getReceiverAccount()!=null){

                        TsClient receiverClient=tsClientRepository.findLastByWalletNoOrderByIdDesc(tsFeeSharingRecord.getReceiverAccount());
                        if(receiverClient!=null){
                            record.setReceiverGlName(receiverClient.getFullName());
                        }else{
                            record.setReceiverGlName(tsFeeSharingRecord.getReceiverAccount());
                        }
                    }
                }

                record.setRequestId(tsFeeSharingRecord.getRequestId());
                feeSharingRecordList.add(record);

            }

            baseResponseObject.setData(feeSharingRecordList);

        }
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        return baseResponseObject;
}

    @Override
    public BaseResponseObject shareConversionProfitForIndividualTransaction(BigDecimal pocketConversionProfit,
                                                                            String mainTransactionReference, String requestId) {

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        TsTransaction mainTransaction = transactionRepository.findFirstByToken(mainTransactionReference);

        if (mainTransaction == null) {
            logWriterUtility.error(requestId, "Invalid transaction token provide :" + mainTransactionReference);
            baseResponseObject.setData("Invalid transaction token provide :" + mainTransactionReference);
            return baseResponseObject;
        }

        logWriterUtility.trace(requestId, "Transaction found based on provided token :" + mainTransaction.getId());

        TsFee tsFee=null;

        if(mainTransaction.getTransactionType().equalsIgnoreCase(TsEnums.Services.Top_Up.getServiceCode())){
            tsFee=feeRepository.findFirstByFeeCodeAndStatus(TsEnums.FeeCode.TOPUP_CONVERSION_DISTRIBUTION_AGENT_APP.getFeeCode(),"1");
        }else if(mainTransaction.getTransactionType().equalsIgnoreCase(TsEnums.Services.BillPay.getServiceCode())){
            tsFee=feeRepository.findFirstByFeeCodeAndStatus(TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode(),"1");
        }else{
            tsFee=feeRepository.findFirstByFeeCodeAndStatus(TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode(),"1");
        }

        if(tsFee==null){
            logWriterUtility.error(requestId,"Fee not found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());

            baseResponseObject.setData("Fee not found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());
            return baseResponseObject;
        }

        logWriterUtility.trace(requestId,"Fee code found :"+tsFee.getFeeCode());


        List<TsFeeFeeStakeholderMap> stakeHolderFeeProductMapList =
                stakeholderFeeProductMappingRepository.findAllByFeeProductCode(tsFee.getFeeCode());


        if(stakeHolderFeeProductMapList==null||stakeHolderFeeProductMapList.size()==0){

            logWriterUtility.error(requestId,"Fee stakeholder found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());

            baseResponseObject.setData("Fee stakeholder found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());
            return baseResponseObject;
        }

        List<FeeSharingTransactionDetailsResponse> currencySharingTransactionDetailsResponses = new ArrayList<>();

        for (TsFeeFeeStakeholderMap cfeStakeholderFeeProductMapping : stakeHolderFeeProductMapList) {

            TsFeeStakeholder feeStakeholder = feeStakeholderRepository.findFirstById(cfeStakeholderFeeProductMapping.getStakeholderId());

            BigDecimal currentStakeholderFee = (pocketConversionProfit.multiply(new BigDecimal(cfeStakeholderFeeProductMapping.getStakeholderPercentage())))
                    .divide(BigDecimal.valueOf(100));

            logWriterUtility.trace(requestId, "Stakeholder payment method :" + cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod());

            FeeSharingTransactionDetailsResponse txDetailsResponse=null;
            if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod() == null ||
                    cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.GL_TRANSACTION)) {

                logWriterUtility.trace(requestId, "Stakeholder payment method :" + "GL transaction");

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareWalletToGl(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);

            } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.WALLET_TRANSACTION)) {
                logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Wallet transaction");

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareWalletToWallet(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);

            }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_SR_WALLET)) {

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareSrWallet(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);
            }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_TOPUP_AGENT_WALLET)) {

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareTopUpAgent(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);
            } else {
                logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Based on transaction logic");
            }

            currencySharingTransactionDetailsResponses.add(txDetailsResponse);

        }

        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setData(currencySharingTransactionDetailsResponses);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject shareConversionProfitForIndividualTransaction(String feeCode, BigDecimal pocketConversionProfit, String mainTransactionReference, String requestId) {



        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        if (feeCode == null) {
            baseResponseObject.setStatus(PocketConstants.ERROR);

            logWriterUtility.error(requestId, "Provided fee code is null ");
            baseResponseObject.setData("Provided fee code is null :" + mainTransactionReference);
            return baseResponseObject;
        }

        TsTransaction mainTransaction = transactionRepository.findFirstByToken(mainTransactionReference);

        if (mainTransaction == null) {
            logWriterUtility.error(requestId, "Invalid transaction token provide :" + mainTransactionReference);
            baseResponseObject.setData("Invalid transaction token provide :" + mainTransactionReference);
            return baseResponseObject;
        }

        logWriterUtility.trace(requestId, "Transaction found based on provided token :" + mainTransaction.getId());

        TsFee tsFee=feeRepository.findFirstByFeeCodeAndStatus(feeCode,"1");

        if(tsFee==null){
            logWriterUtility.error(requestId,"Fee not found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());

            baseResponseObject.setData("Fee not found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());
            return baseResponseObject;
        }

        logWriterUtility.trace(requestId,"Fee code found :"+tsFee.getFeeCode());


        List<TsFeeFeeStakeholderMap> stakeHolderFeeProductMapList =
                stakeholderFeeProductMappingRepository.findAllByFeeProductCode(tsFee.getFeeCode());


        if(stakeHolderFeeProductMapList==null||stakeHolderFeeProductMapList.size()==0){

            logWriterUtility.error(requestId,"Fee stakeholder found for fee code :"
                    +TsEnums.FeeCode.AED_BD_CONVERTION.getFeeCode());

            baseResponseObject.setData("Fee stakeholder found for fee code :"
                    +tsFee.getFeeCode());
            return baseResponseObject;
        }

        List<FeeSharingTransactionDetailsResponse> currencySharingTransactionDetailsResponses = new ArrayList<>();

        for (TsFeeFeeStakeholderMap cfeStakeholderFeeProductMapping : stakeHolderFeeProductMapList) {

            TsFeeStakeholder feeStakeholder = feeStakeholderRepository.findFirstById(cfeStakeholderFeeProductMapping.getStakeholderId());

            BigDecimal currentStakeholderFee = (pocketConversionProfit.multiply(new BigDecimal(cfeStakeholderFeeProductMapping.getStakeholderPercentage())))
                    .divide(BigDecimal.valueOf(100));

            logWriterUtility.trace(requestId, "Stakeholder payment method :" + cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod());

            FeeSharingTransactionDetailsResponse txDetailsResponse=null;
            if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod() == null ||
                    cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.GL_TRANSACTION)) {

                logWriterUtility.trace(requestId, "Stakeholder payment method :" + "GL transaction");

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareWalletToGl(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);

            } else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.WALLET_TRANSACTION)) {
                logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Wallet transaction");

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareWalletToWallet(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);

            }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_SR_WALLET)) {

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareSrWallet(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);
            }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_TOPUP_AGENT_WALLET)) {

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareTopUpAgent(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);
            }else if (cfeStakeholderFeeProductMapping.getStakeholderPaymentMethod().equalsIgnoreCase(StakeholderPaymentMethod.TO_AGENT_MERCHANT_WALLET)) {

                txDetailsResponse = mfsSecondPartTransactionService
                        .conversionRateShareTopUpAgent(feeStakeholder, mainTransaction,
                                currentStakeholderFee,tsFee, requestId);
            } else {
                logWriterUtility.trace(requestId, "Stakeholder payment method :" + "Based on transaction logic");
            }

            currencySharingTransactionDetailsResponses.add(txDetailsResponse);

        }

        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(requestId);
        baseResponseObject.setData(currencySharingTransactionDetailsResponses);

        return baseResponseObject;
    }

    private void saveFeeSharingSummery(String mainTxToken, BigDecimal totalTxAmount, BigDecimal totalFeeAmount,
                                       List<FeeSharingTransactionDetailsResponse> feeSharingTransactionDetailsResponses,
                                       String requestId) {
        List<TsFeeSharingRecord> feeRecordList = new ArrayList<>();

        for (FeeSharingTransactionDetailsResponse record : feeSharingTransactionDetailsResponses) {
            TsFeeSharingRecord cfeFeeSharingRecord = new TsFeeSharingRecord();

            cfeFeeSharingRecord.setRequestId(requestId);
            cfeFeeSharingRecord.setShareDate(new Timestamp(System.currentTimeMillis()));

            String isSuccess = record.isSuccess() ? "1" : "0";

            cfeFeeSharingRecord.setTxStatus(isSuccess);
            cfeFeeSharingRecord.setMainTxToken(mainTxToken);
            cfeFeeSharingRecord.setFeeTxToken(record.getNewTransactionToken());
            cfeFeeSharingRecord.setMainTxType(record.getTransactionType());
            cfeFeeSharingRecord.setReceiverAccount(record.getStakeholderAccount());
            cfeFeeSharingRecord.setSenderAccount(record.getSenderAccount());

            cfeFeeSharingRecord.setTotalTxAmount(totalTxAmount);
            cfeFeeSharingRecord.setTotalFeeAmount(totalFeeAmount);
            cfeFeeSharingRecord.setStakeholderFeeAmount(record.getStakeholderFeeAmount());
            cfeFeeSharingRecord.setFeeShareMethod(record.getFeeShareMethod());

            feeRecordList.add(cfeFeeSharingRecord);

        }

        logWriterUtility.trace(requestId, "Total fee record saving :" + feeRecordList.size());

        cfeFeeSharingRecordRepository.saveAll(feeRecordList);


    }


    private void updateEodStatisticsForFeeSharing(int totalNoOfTx, int totalNoOfFeeShared, long startTime, long endTime, String requestId) {
        try {
            TsFeeShairingRecordDetails feeSharingRecordDetails = new TsFeeShairingRecordDetails();

            feeSharingRecordDetails.setWhichDay(java.sql.Date.valueOf(LocalDate.now()));
            feeSharingRecordDetails.setDateOfFeeShared(java.sql.Date.valueOf(LocalDate.now().minusDays(1)));
//            feeSharingRecordDetails.setIsFeeShareCompleted(true);
            feeSharingRecordDetails.setFeeShairingStatus(""+(totalNoOfTx <= totalNoOfFeeShared));
            feeSharingRecordDetails.setStartTime(new java.sql.Date(startTime));
            feeSharingRecordDetails.setEndTime(new java.sql.Date(endTime));
            feeSharingRecordDetails.setTotalNoOfTransaction( totalNoOfTx);
            feeSharingRecordDetails.setTotalNoOfFeeShared(totalNoOfFeeShared);

            feeSharingRecordDetailsRepository.save(feeSharingRecordDetails);

        } catch (Exception ex) {
            logWriterUtility.error(requestId, "Updating EOD statistics in DB failed failed");
        }
    }

}