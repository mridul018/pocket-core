package tech.ipocket.pocket.services.ts.feeShairing;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.utils.GlConstants;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.TsEnums;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

@Service
public class EodTransactionalServiceImpl implements EodTransactionalService {
    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());
    @Autowired
    private TsGlRepository generalLedgerRepository;
    @Autowired
    private TsGlTransactionDetailsRepository glTransactionDetailsRepository;
    @Autowired
    private TsTransactionRepository transactionRepository;
    @Autowired
    private TsFeeStakeholderRepository feeStakeholderRepository;


    @Autowired
    private TsFeeFeeStakeholderMapRepository tsFeeFeeStakeholderMapRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void singleTransactionFeeSharing(TsTransaction transaction, TsGlTransactionDetails details, String requestId) {
        DecimalFormat df = new DecimalFormat("############.####");

        if (details.getCreditAmount().compareTo(BigDecimal.ZERO) == 0) {
            details.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(details);

            transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(transaction);

            logWriterUtility.info(requestId, "Fee amount Zero for transaction id " + details.getTsTransactionByTransactionId().getId());
            return;
        }

        List<TsFeeFeeStakeholderMap> sfpm = tsFeeFeeStakeholderMapRepository.findAllByFeeProductCode(transaction.getFeeCode());


        TsGl unresolvedGl = generalLedgerRepository.findFirstByGlName(GlConstants.Fee_payable_for_Unresolved);
        String unresolvedGL = unresolvedGl.getGlCode();

        if (sfpm.size() <= 0) {
            logWriterUtility.info(requestId, "No stakeholder found for transaction : " + transaction.getId());

            TsGlTransactionDetails debitFeeAmount = new TsGlTransactionDetails();
            debitFeeAmount.setTsTransactionByTransactionId(transaction);
            debitFeeAmount.setGlCode(details.getGlCode());
            debitFeeAmount.setDebitCard(details.getCreditAmount());
//            debitFeeAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(debitFeeAmount);

            TsGlTransactionDetails unresolvedGlCreditAmount = new TsGlTransactionDetails();
            unresolvedGlCreditAmount.setTsTransactionByTransactionId(transaction);
            unresolvedGlCreditAmount.setGlCode(unresolvedGL);
            unresolvedGlCreditAmount.setCreditAmount(details.getCreditAmount());
//            unresolvedGlCreditAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(unresolvedGlCreditAmount);

            details.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(details);

            transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(transaction);

            logWriterUtility.info(requestId, "Fee moved to Unresolved GL for " + transaction.getId());

            return;
        }

        BigDecimal stakeHolderPercentage = BigDecimal.ZERO;
        for (TsFeeFeeStakeholderMap stakeholderFeeProductMapping : sfpm) {
            stakeHolderPercentage = stakeHolderPercentage.add(new BigDecimal(stakeholderFeeProductMapping.getStakeholderPercentage()));
        }

        if (stakeHolderPercentage.compareTo(new BigDecimal(100)) > 0) {
            logWriterUtility.warn(requestId, "Stakeholder percentage is not Configured properly for Transaction " + transaction.getId());

            TsGlTransactionDetails debitFeeAmount = new TsGlTransactionDetails();
            debitFeeAmount.setTsTransactionByTransactionId(transaction);
            debitFeeAmount.setGlCode(details.getGlCode());
            debitFeeAmount.setDebitCard(details.getCreditAmount());
//            debitFeeAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(debitFeeAmount);

            TsGlTransactionDetails unresolvedGlCreditAmount = new TsGlTransactionDetails();
            unresolvedGlCreditAmount.setTsTransactionByTransactionId(transaction);
            unresolvedGlCreditAmount.setGlCode(unresolvedGL);
            unresolvedGlCreditAmount.setCreditAmount(details.getCreditAmount());
//            unresolvedGlCreditAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(unresolvedGlCreditAmount);

            details.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            glTransactionDetailsRepository.save(details);

            transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
            transactionRepository.save(transaction);

            logWriterUtility.info(requestId, "Fee moved to Unresolved GL for " + transaction.getId());
            return;
        }

        logWriterUtility.info(requestId, "Stakeholder found. EOD starting for transaction : " + transaction.getId());
        BigDecimal sum = new BigDecimal(0);
        BigDecimal amount = details.getCreditAmount();

        TsGlTransactionDetails debitFeeAmount = new TsGlTransactionDetails();
        debitFeeAmount.setTsTransactionByTransactionId(transaction);
        debitFeeAmount.setGlCode(details.getGlCode());
        debitFeeAmount.setDebitCard(details.getCreditAmount());
//        debitFeeAmount.setProcess("FEE_SHARED");

        glTransactionDetailsRepository.save(debitFeeAmount);

        for (int i = 0; i < sfpm.size(); i++) {

            TsFeeStakeholder feeStakeholder = feeStakeholderRepository.findById(sfpm.get(i).getStakeholderId()).get();

            BigDecimal stakeholderFee = (amount.multiply(new BigDecimal(sfpm.get(i).getStakeholderPercentage()))).divide(BigDecimal.valueOf(100));

            stakeholderFee = new BigDecimal(df.format(stakeholderFee.setScale(4, RoundingMode.DOWN)));
            sum = sum.add(stakeholderFee);

            TsGlTransactionDetails stakeholderCreditAmount = new TsGlTransactionDetails();
            stakeholderCreditAmount.setTsTransactionByTransactionId(transaction);
            stakeholderCreditAmount.setGlCode(feeStakeholder.getGlCode());
            stakeholderCreditAmount.setCreditAmount(stakeholderFee);
//            stakeholderCreditAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(stakeholderCreditAmount);
        }

        if (amount.compareTo(sum) > 0) {

            TsGlTransactionDetails unresolvedGlCreditAmount = new TsGlTransactionDetails();
            unresolvedGlCreditAmount.setTsTransactionByTransactionId(transaction);
            unresolvedGlCreditAmount.setGlCode(unresolvedGL);
            unresolvedGlCreditAmount.setCreditAmount(amount.subtract(sum));
//            unresolvedGlCreditAmount.setProcess("FEE_SHARED");

            glTransactionDetailsRepository.save(unresolvedGlCreditAmount);
        }

        details.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
        glTransactionDetailsRepository.save(details);

        transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
        transactionRepository.save(transaction);
    }
}
