package tech.ipocket.pocket.services.ts;

import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

import java.math.BigDecimal;

public interface AccountService {
    BaseResponseObject checkBalance(String walletId, String requestId) throws PocketException;

    BigDecimal updateClientCurrentBalance(TsClient client, String requestId);

    BaseResponseObject updateClientStatus(String mobileNumber, String newStatus, String requestId) throws PocketException;
}
