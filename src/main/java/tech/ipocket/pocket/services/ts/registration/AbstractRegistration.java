package tech.ipocket.pocket.services.ts.registration;

import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;

public abstract class AbstractRegistration {
    private String mobileNumber;
    private String fullName;
    private String groupCode;
    private String userStatus;
    private String requestId;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public abstract void setData(WalletCreateRequest walletCreateRequest);

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestId() {
        return requestId;
    }
}
