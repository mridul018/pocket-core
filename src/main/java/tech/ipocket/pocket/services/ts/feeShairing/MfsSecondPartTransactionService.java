package tech.ipocket.pocket.services.ts.feeShairing;

import tech.ipocket.pocket.entity.TsFee;
import tech.ipocket.pocket.entity.TsFeeStakeholder;
import tech.ipocket.pocket.entity.TsTransaction;
import tech.ipocket.pocket.response.ts.FeeSharingTransactionDetailsResponse;

import java.math.BigDecimal;

public interface MfsSecondPartTransactionService {

    FeeSharingTransactionDetailsResponse feeShareGlToDirectWallet(TsFeeStakeholder feeStakeholder, String senderGlCode,
                                                                  String mainTxToken, BigDecimal transactionAmount,
                                                                  String requestId, BigDecimal totalFeeAmount);
    FeeSharingTransactionDetailsResponse feeShareGlToDirectWalletForTopUpAgent(TsFeeStakeholder feeStakeholder, String senderGlCode,
                                                                  String mainTxToken, BigDecimal transactionAmount,
                                                                  String requestId, BigDecimal totalFeeAmount);
    FeeSharingTransactionDetailsResponse feeShareGlToDirectWalletForSR(TsFeeStakeholder feeStakeholder, String senderGlCode,
                                                                  String mainTxToken, BigDecimal transactionAmount,
                                                                  String requestId, BigDecimal totalFeeAmount);
    FeeSharingTransactionDetailsResponse feeShareGlToDirectWalletForAgentMerchant(TsFeeStakeholder feeStakeholder, String senderGlCode,
                                                                  String mainTxToken, BigDecimal transactionAmount,
                                                                  String requestId, BigDecimal totalFeeAmount);
    FeeSharingTransactionDetailsResponse feeShareGlToGl(TsFeeStakeholder feeStakeholder, String senderGlCode,
                                                                  String mainTxToken, BigDecimal transactionAmount,
                                                                  String requestId, BigDecimal totalFeeAmount);

    FeeSharingTransactionDetailsResponse conversionRateShareWalletToGl(TsFeeStakeholder feeStakeholder, TsTransaction mainTransaction, BigDecimal currentStakeholderFee, TsFee tsFee, String requestId);
    FeeSharingTransactionDetailsResponse conversionRateShareWalletToWallet(TsFeeStakeholder feeStakeholder, TsTransaction mainTransaction, BigDecimal currentStakeholderFee, TsFee tsFee, String requestId);
    FeeSharingTransactionDetailsResponse conversionRateShareSrWallet(TsFeeStakeholder feeStakeholder, TsTransaction mainTransaction, BigDecimal currentStakeholderFee, TsFee tsFee, String requestId);
    FeeSharingTransactionDetailsResponse conversionRateShareTopUpAgent(TsFeeStakeholder feeStakeholder, TsTransaction mainTransaction, BigDecimal currentStakeholderFee, TsFee tsFee, String requestId);
}
