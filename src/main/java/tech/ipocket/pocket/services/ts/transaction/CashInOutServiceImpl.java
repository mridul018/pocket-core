package tech.ipocket.pocket.services.ts.transaction;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.IntermediateTxResponse;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.transaction.TransactionResponseData;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.SingleTransactionService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CashInOutServiceImpl implements CashInOutService {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;

    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TsServiceRepository serviceRepository;

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TsBankRepository tsBankRepository;
    @Autowired
    private TsBranchRepository tsBranchRepository;

    @Autowired
    private SingleTransactionService singleTransactionService;
    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;

    @Override
    public BaseResponseObject doAgentCashIn(TransactionRequest request, TsSessionObject tsSessionObject)
            throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientSenderAgent = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(),
                statusNotIn);

        if (clientSenderAgent == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        boolean isSenderValid=(clientSenderAgent.getGroupCode().equals(TsEnums.UserType.AGENT.getUserTypeString()))
                ||(clientSenderAgent.getGroupCode().equals(TsEnums.UserType.MERCHANT.getUserTypeString()));
        if(!isSenderValid){
            logWriterUtility.error(request.getRequestId(), "Sender IS not agent.");
            throw new PocketException(PocketErrorCode.SenderMustBeAgent);
        }

        TsClient clientReceiverCustomer = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(),
                statusNotIn);

        if (clientReceiverCustomer == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        if(!clientReceiverCustomer.getGroupCode().equals(TsEnums.UserType.CUSTOMER.getUserTypeString())){
            logWriterUtility.error(request.getRequestId(), "Receiver is not customer.");
            throw new PocketException(PocketErrorCode.ReceiverMustBeCustomer);
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(request.getTransactionType());

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //calculate fee
        FeeData feeData = feeManager.calculateFee(clientSenderAgent, cfeService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransferAmount()));

        transactionAmountData.setSenderDebitAmount(new BigDecimal(request.getTransferAmount()));
        transactionAmountData.setReceiverCreditAmount(new BigDecimal(request.getTransferAmount()));


        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");

        //transaction profile validation for sender + check balance limit for sender
        Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSenderAgent, cfeService, transactionAmountData.getSenderDebitAmount(),
                request.getRequestId());
        if (!isSenderProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
            throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
        }

        //transaction profile validation for receiver
        Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(clientReceiverCustomer, cfeService,
                request.getRequestId());
        if (!isReceiverProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
            throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder
                .buildTransaction(clientSenderAgent, clientReceiverCustomer, cfeService.getServiceCode(),
                feeData, transactionAmountData);
        tsTransactionRepository.save(cfeTransaction);

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();

        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(),
                FeePayer.DEBIT,
                clientSenderAgent.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(),
                FeePayer.CREDIT,
                clientReceiverCustomer.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(),
                FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(clientSenderAgent);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }


        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                senderGlName, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(clientReceiverCustomer);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails expenseGlDebitEntry = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.POCKET_EXPENSE_GL_CODE_FOR_CASHIN,  transactionAmountData.getFeeCreditAmount(),BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(expenseGlDebitEntry);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_Cash_In, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        tsGlTransactionDetailsRepository.saveAll(glTxDetailList);

        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSenderAgent, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(clientReceiverCustomer, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setSenderRunningBalance(senderRunningBalance);
        cfeTransaction.setReceiverRunningBalance(receiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setSenderRunningBalance(senderRunningBalance);
        fundTransferResponseData.setTxToken(cfeTransaction.getToken());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(transactionAmountData.getOriginalTransactionAmount(),
                        clientReceiverCustomer.getWalletNo(),
                        clientSenderAgent.getWalletNo(), cfeTransaction.getToken(), cfeService.getServiceCode(),
                        feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes());
        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        CompletableFuture.runAsync(() -> {
            mfsFeeSharingService.doFeeShareForIndividualTransaction(cfeTransaction.getToken(),
                    request.getRequestId());
        });

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(fundTransferResponseData);
        return baseResponseObject;
    }

    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    @Override
    public BaseResponseObject doCashInFromBank(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsSessionObject.getMobileNumber(), statusNotIn);

        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        TsBanks tsBanks=tsBankRepository.findFirstByBankCode(request.getBankCode());
        if(tsBanks==null){
            tsBanks  = tsBankRepository.findFirstByBankCode("5001");
//            logWriterUtility.error(request.getRequestId(),"Bank not found");
//            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        /*TsBranch tsBranch=tsBranchRepository.findFirstBySwiftCodeAndStatus(request.getBranchSwiftCode(),"1");
        if(tsBranch==null){
            logWriterUtility.error(request.getRequestId(),"Branch not found");
            throw new PocketException(PocketErrorCode.BranchNotFound);
        }*/

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromBank);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }


        //TODO API call to settlement bank for fund transfer from bank to settlement bank

        String prevTransactionReference;
        try{
            BaseResponseObject baseResponseObject=singleTransactionService.doBankToMasterTransferIntermediate(request,tsSessionObject);

            IntermediateTxResponse intermediateTxResponse= (IntermediateTxResponse) baseResponseObject.getData();
            if(intermediateTxResponse!=null){
                prevTransactionReference=intermediateTxResponse.getTxReference();
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction successful");
            }else{
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction failed");
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }
        }catch (PocketException e){
            logWriterUtility.error(request.getRequestId(),e.getMessage());
            throw e;
        }

        IntermediateTxResponse mainTxResponse;

        try{
            BaseResponseObject baseResponseObject=singleTransactionService.doMasterToCustomerWalletTransfer(request,tsSessionObject,prevTransactionReference,tsSessionObject.getMobileNumber(),tsBanks);

            if(baseResponseObject!=null){
                logWriterUtility.error(request.getRequestId(),"Transaction successful");

                mainTxResponse= (IntermediateTxResponse) baseResponseObject.getData();


            }else{
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction failed");
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }

        }catch (PocketException e){
            logWriterUtility.error(request.getRequestId(),e.getMessage());
            throw e;
        }

        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setTxToken(mainTxResponse.getTxReference());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(new BigDecimal(request.getTransferAmount()),
                        clientReceiver.getWalletNo(),
                        tsBanks.getBankName(), mainTxResponse.getTxReference(), tsService.getServiceCode(),
                        mainTxResponse.getFeePayer(), mainTxResponse.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes(),
                        tsBanks.getBankName(),request.getBankAccountNumber());
        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(fundTransferResponseData);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }

    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    @Override
    public BaseResponseObject doCashOutToBank(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        request.setSenderMobileNumber(tsSessionObject.getMobileNumber());

        TsClient clientSender = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), statusNotIn);

        if (clientSender == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        TsBanks tsBanks=tsBankRepository.findFirstByBankCode(request.getBankCode());
        if(tsBanks==null){
            logWriterUtility.error(request.getRequestId(),"Bank not found");
            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        TsBranch tsBranch=tsBranchRepository.findFirstBySwiftCodeAndStatus(request.getBranchSwiftCode(),"1");
        if(tsBranch==null){
            logWriterUtility.error(request.getRequestId(),"Branch not found");
            throw new PocketException(PocketErrorCode.BranchNotFound);
        }

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashOutToBank);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //TODO API call to settlement bank for fund transfer from customer bank to settlement bank

        String mainTransactionReference;

        IntermediateTxResponse mainTxResponse;

        try{
            BaseResponseObject baseResponseObject=singleTransactionService.doCashOutToBankCustomerToMasterWalletTransferIntermediate(request,tsSessionObject,request.getSenderMobileNumber(),
                    tsBanks.getBankName());

            if(baseResponseObject!=null){
                logWriterUtility.error(request.getRequestId(),"Transaction successful");
                mainTxResponse= (IntermediateTxResponse) baseResponseObject.getData();
            }else{
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction failed");
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }

        }catch (PocketException e){
            logWriterUtility.error(request.getRequestId(),e.getMessage());
            throw e;
        }

        try{
            BaseResponseObject baseResponseObject=singleTransactionService.doMasterToBankGlTransfer(request,tsSessionObject,mainTxResponse.getTxReference(),tsBanks);

            if(baseResponseObject!=null){
                logWriterUtility.error(request.getRequestId(),"Transaction successful");
            }else{
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction failed");
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }

        }catch (PocketException e){
            logWriterUtility.error(request.getRequestId(),e.getMessage());
            throw e;
        }


        TransactionResponseData transactionResponseData = new TransactionResponseData();
        transactionResponseData.setTxToken(mainTxResponse.getTxReference());
        transactionResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(new BigDecimal(request.getTransferAmount()),
                        tsBanks.getBankName(),
                        clientSender.getWalletNo(),mainTxResponse.getTxReference(), tsService.getServiceCode(),
                        mainTxResponse.getFeePayer(), mainTxResponse.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(transactionResponseData), request.getRequestId(), request.getNotes(),
                        tsBanks.getBankName(),request.getBankAccountNumber());
        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(transactionResponseData);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }
}
