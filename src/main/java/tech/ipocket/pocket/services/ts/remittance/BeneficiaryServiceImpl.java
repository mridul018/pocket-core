package tech.ipocket.pocket.services.ts.remittance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsBeneficiary;
import tech.ipocket.pocket.repository.ts.TsBeneficiaryReropository;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

import java.util.Date;
import java.util.List;

@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private TsBeneficiaryReropository tsBeneficiaryReropository;

    @Override
    public BaseResponseObject createBeneficiary(CreateBeneficiaryRequest request, TsSessionObject tsSessionObject) throws PocketException {

        TsBeneficiary tsBeneficiary=new TsBeneficiary();
        tsBeneficiary.setType(request.getType());
        tsBeneficiary.setGivenName(request.getGivenName());
        tsBeneficiary.setFamilyName(request.getFamilyName());
        tsBeneficiary.setAddress(request.getAddress());
        tsBeneficiary.setContactNumber(request.getContactNumber());
        tsBeneficiary.setCreatorWallet(tsSessionObject.getMobileNumber());
        tsBeneficiary.setRelationship(request.getRelationship());
        tsBeneficiary.setStatus(PocketConstants.STATUS_ACTIVE);
        tsBeneficiary.setCreatedDate(new Date());

        tsBeneficiaryReropository.save(tsBeneficiary);

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(tsBeneficiary);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getBeneficiary(GetBeneficiaryRequest request, TsSessionObject tsSessionObject) {

        List<TsBeneficiary> beneficiaries=tsBeneficiaryReropository
                .findAllByCreatorWalletAndStatusAndType(tsSessionObject.getMobileNumber(),
                PocketConstants.STATUS_ACTIVE,request.getType());

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(beneficiaries);

        return baseResponseObject;
    }
}
