package tech.ipocket.pocket.services.ts.feeShairing;

import tech.ipocket.pocket.entity.TsGlTransactionDetails;
import tech.ipocket.pocket.entity.TsTransaction;

public interface EodTransactionalService {
    void singleTransactionFeeSharing(TsTransaction transaction, TsGlTransactionDetails tsGlTransactionDetails, String requestId);
}
