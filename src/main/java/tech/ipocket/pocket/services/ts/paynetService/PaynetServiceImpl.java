package tech.ipocket.pocket.services.ts.paynetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.PaynetConfig;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.ts.paynet.*;
import tech.ipocket.pocket.repository.ts.paynet.Country;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.request.ts.paynet.*;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.paynet.*;
import tech.ipocket.pocket.response.ts.transaction.TransactionResponseData;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.services.ts.irregular_transaction.IrregularTransactionService;
import tech.ipocket.pocket.utils.*;
import com.google.gson.Gson;
import tech.ipocket.pocket.utils.constants.BillPayTypeConstants;
import tech.ipocket.pocket.utils.constants.CurrencyCode;
import tech.ipocket.pocket.utils.constants.TopUpMedium;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional(rollbackFor = {PocketException.class,Exception.class})
public class PaynetServiceImpl implements PaynetService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private PaynetConfig paynetConfig;

    @Autowired
    private TsClientRepository clientRepository;

    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;

    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FeeManager feeManager;

    @Autowired
    private TsServiceRepository serviceRepository;

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TsMerchantDetailsRepository tsMerchantDetailsRepository;

    @Autowired
    private TsConversionRateRepository tsConversionRateRepository;

    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;

    @Autowired
    private IrregularTransactionService irregularTransactionService;


     PaynetAuthorization paynetAuthorization = null;
     HttpHeaders headers = null;
     PaynetAuthorizationRequest auth = null;

    @Override
    public BaseResponseObject createPaynetTransaction(PaynetBillPayRequest request, TsSessionObject tsSessionObject) throws PocketException {
        paynetAuthorization(request);

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient clientSender = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsSessionObject.getMobileNumber(), statusNotIn);
        if (clientSender == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound, "Sender wallet not found.");
        }

        TsMerchantDetails tsMerchantDetails=tsMerchantDetailsRepository.findFirstByShortCode(PocketConstants.PAYNET);
        if(tsMerchantDetails==null){
            logWriterUtility.error(request.getRequestId(),"PAYNET merchant not found");
            throw new PocketException(PocketErrorCode.SalikMerchantNotFound, "PAYNET merchant not found");
        }

        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsMerchantDetails.getWalletNo(), statusNotIn);
        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound, "Receiver wallet not found.");
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.BillPay);
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound, "Service not found.");
        }

        //*** calculate fee ***//
        FeeData feeData = null;
        String conversionFeeCode=null;
        if (request.getSource() == null || request.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
            feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.BILLPAY_SALIK_BD_CUSTOMER_APP.getFeeCode(),
                    new BigDecimal(request.getAmount()), request.getRequestId());
            conversionFeeCode=TsEnums.FeeCode.BILLPAY_CONVERSION_DISTRIBUTION_CUSTOMER_APP.getFeeCode();
        } else {
            feeData = feeManager.calculateFeeByFeeCode(TsEnums.FeeCode.BILLPAY_SALIK_BD_AGENT_APP.getFeeCode(),
                    new BigDecimal(request.getAmount()), request.getRequestId());
            conversionFeeCode=TsEnums.FeeCode.BILLPAY_CONVERSION_DISTRIBUTION_AGENT_APP.getFeeCode();
        }
        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData=new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeeCode(null);
            feeData.setFeePayer("dr");
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getAmount()));
        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");

        // *** transaction profile validation for sender + check balance limit for sender  *** //
        Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSender, cfeService,
                transactionAmountData.getSenderDebitAmount(), request.getRequestId());
        if (!isSenderProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
            throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
        }

        // *** transaction profile validation for receiver *** //
        Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(clientReceiver, cfeService, request.getRequestId());
        if (!isReceiverProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
            throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
        }

        // *** build transaction *** //
        TransactionBuilder transactionBuilder = new TransactionBuilder();
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionBillPay(clientSender, clientReceiver,
                cfeService.getServiceCode(), feeData, transactionAmountData);
        if(isCheckedBillPayType(request)){
            cfeTransaction = tsTransactionRequest(cfeTransaction, request);
        }

        //*** Paynet Transaction ***//
        BaseResponseObject baseResponseObject=null;
        ConfirmFindTransaction confirmFindTransaction=null;
        if(cfeTransaction !=null && cfeTransaction.getToken()!=null){
            request.setExternalTransactionId(cfeTransaction.getToken());
        }
        if(request.getCurrencyCode()!=null && request.getCurrencyCode().equalsIgnoreCase(CurrencyCode.AED)){
            request.setAccountNumber(request.getAccountNumber());
            request.setSimulation(1); //(1=Test,0=live)
            confirmFindTransaction = new PaynetExternalCall(paynetConfig.getPaynetTransaction()).callPaynetTransaction(request, headers, paynetAuthorization, paynetConfig, new TsTransaction());

            if(confirmFindTransaction.transaction != null){
                request.setTransactionId(confirmFindTransaction.transaction.id.toString());
                confirmFindTransaction = confirmTransaction(request);

                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setData(confirmFindTransaction);
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.OK);

                tsTransactionRepository.save(cfeTransaction);
            }
        }else {
            baseResponseObject = new BaseResponseObject();
            baseResponseObject.setError(new ErrorObject("400","AED currency code not matched."));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");

        // *** build transaction detail *** //
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.DEBIT,
                clientSender.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                clientReceiver.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        if(confirmFindTransaction.transaction != null){
            tsTransactionDetailsRepository.saveAll(transactionDetailList);
        }
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();
        String senderGlName=CommonTasks.getGlNameByCustomerType(clientSender);
        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);
        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found");
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                senderGlName, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(clientReceiver);
        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);
        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_BillPay, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        if(confirmFindTransaction.transaction != null){
            tsGlTransactionDetailsRepository.saveAll(glTxDetailList);
        }

        BigInteger convertedAmount = new BigInteger(""+request.getAmount().intValue());
        BigDecimal pocketConversionProfit = BigDecimal.ZERO;

        try {
            BigInteger receiverCreditAmount=new BigInteger(""+transactionAmountData.getReceiverCreditAmount().intValue());
            BigInteger actualBillAmount= BigInteger.valueOf(0);

            if(request.getCurrencyCode()!=null && request.getCurrencyCode().equalsIgnoreCase(CurrencyCode.AED)){
                Double actualConvertedAmount = (double) 0;

                TsConversionRate tsConversionRate = tsConversionRateRepository.findFirstByServiceCodeAndStatusOrderByIdDesc(BillPayTypeConstants.PAYNET, "1");
                if(tsConversionRate!=null){
                    Double bill=request.getAmount()*tsConversionRate.getConversionRate();
                    actualBillAmount= BigInteger.valueOf(bill.intValue());
                    convertedAmount=new BigDecimal(tsConversionRate.getConversionRate()).multiply(new BigDecimal(receiverCreditAmount)).toBigInteger();

                    Double actualAmount = (transactionAmountData.getReceiverCreditAmount().doubleValue() * tsConversionRate.getActualRate());
                    actualConvertedAmount = Double.valueOf(actualAmount.toString());
                }else{
                    convertedAmount=receiverCreditAmount.multiply(BigInteger.valueOf(20));
                    actualConvertedAmount = Double.valueOf(convertedAmount.toString());

                    Double bill=request.getAmount()*20;
                    actualBillAmount= BigInteger.valueOf(bill.intValue());
                }

                if (tsConversionRate != null) {
                    Double value = (actualConvertedAmount - Double.valueOf("" + convertedAmount)) / tsConversionRate.getActualRate();
                    pocketConversionProfit = new BigDecimal(value);
                }

            }else{
                convertedAmount=receiverCreditAmount;
                confirmFindTransaction = new PaynetExternalCall(paynetConfig.getPaynetTransaction()).callPaynetTransaction(request, headers, paynetAuthorization,paynetConfig, new TsTransaction());
            }

            logWriterUtility.trace("Paynet Bill pay service ",new Gson().toJson(baseResponseObject));
            if(confirmFindTransaction.transaction == null){
                logWriterUtility.error(request.getRequestId(),"Paynet Bill pay external response failed");

                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject(confirmFindTransaction.error.code.toString(),
                        confirmFindTransaction.error.message.toString()));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return baseResponseObject;
            }else{
                cfeTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
            }
            logWriterUtility.trace(request.getRequestId(),"Paynet Bill pay external response success");

        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PaynetBillPayFailed.getKeyString(),
                    "Bill pay failed. Reason : "+baseResponseObject.getError().getErrorMessage()));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }

        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSender, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(clientReceiver, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setSenderRunningBalance(senderRunningBalance);
        cfeTransaction.setReceiverRunningBalance(receiverRunningBalance);

        if(isCheckedBillPayType(request)){
            if(confirmFindTransaction.transaction != null){
                cfeTransaction.setNotes(tsTransactionNote(request, confirmFindTransaction));
                cfeTransaction.setRefTransactionToken(confirmFindTransaction.transaction.id.toString());
            }
            cfeTransaction.setTopupProviderCode(request.getProduct());
            cfeTransaction.setTopupProviderName(request.getBillPayType());
        }
        if(request.getBillPayType().equals("NOL_CART")){ //This would be change after taking full NOL_CART service from Paynet.
            cfeTransaction.setNotes("");
            cfeTransaction.setRefTransactionToken(confirmFindTransaction.transaction.id.toString());
        }

        if(confirmFindTransaction.transaction != null){
            tsTransactionRepository.save(cfeTransaction);
        }

        //Reverse balance if transaction is done by testing mood(simulation = true)
        boolean simulation = Boolean.parseBoolean(confirmFindTransaction.simulation.toString());
        if(simulation){
            ReversalRequest reversalRequest = new ReversalRequest();
            reversalRequest.setReason("Paynet service bill via API using simulation(true)");
            reversalRequest.setTransactionToken(cfeTransaction.getToken());
            try {
                irregularTransactionService.createReversal(reversalRequest);
            } catch (PocketException e) {
                e.printStackTrace();
            }
        }

        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setSenderRunningBalance(senderRunningBalance);
        fundTransferResponseData.setTxToken(cfeTransaction.getToken());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        //*** send notification ***//
        NotificationBuilder notificationBuilder = new NotificationBuilder();
        NotificationRequest notificationRequest = notificationBuilder.buildNotificationRequest(new BigDecimal(convertedAmount),
                clientReceiver.getWalletNo(), clientSender.getWalletNo(), cfeTransaction.getToken(), cfeService.getServiceCode(),
                feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes());
        notificationRequest.setAccountNumber(request.getAccountNumber());

        NotificationRequest notificationRequestTemp = pinServiceSmsNotificationRequest(request, notificationRequest, confirmFindTransaction);
        TsTransaction cfeTransactionTemp = cfeTransaction;

        //Pocket customer A/C holder send (Pay_net PIN service Code) to any other number
        boolean isPinReceiverNumber = request.getLogedInUserMobileNumber().matches("\\\\"+request.getPinReceiverMobileNumber());
        if(isPinReceiverNumber == Boolean.FALSE){
            notificationRequestTemp.setSenderWalletNo(request.getPinReceiverMobileNumber());
        }

        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequestTemp));
        CompletableFuture.runAsync(() -> {
            mfsFeeSharingService.doFeeShareForIndividualTransaction(cfeTransactionTemp.getToken(), request.getRequestId());
        });

        if (pocketConversionProfit.compareTo(BigDecimal.ZERO) > 0) {
            mfsFeeSharingService.shareConversionProfitForIndividualTransaction(conversionFeeCode,pocketConversionProfit, cfeTransaction.getToken(),
                    request.getRequestId());
        }

        return baseResponseObject;
    }

    public NotificationRequest pinServiceSmsNotificationRequest(PaynetBillPayRequest request,
                                                         NotificationRequest notificationRequest,
                                                         ConfirmFindTransaction confirmFindTransaction){
        if(request.getBillPayType().equalsIgnoreCase("SLK50") || request.getBillPayType().equalsIgnoreCase("SLK100")
                || request.getBillPayType().equalsIgnoreCase("SLK200")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("SLK");
        }
        if(request.getBillPayType().equalsIgnoreCase("SLK-PIN")){
            notificationRequest.setBillNumber(request.getAccountNumber());
            notificationRequest.setAccountNumber((String) confirmFindTransaction.transaction.account);
        }
        if(request.getBillPayType().equalsIgnoreCase("VIRGIN-M20") || request.getBillPayType().equalsIgnoreCase("VIRGIN-M50")
                || request.getBillPayType().equalsIgnoreCase("VIRGIN-M100") || request.getBillPayType().equalsIgnoreCase("VIRGIN-M150")
                || request.getBillPayType().equalsIgnoreCase("VIRGIN-M200")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("VIRGIN");
        }
        if(request.getBillPayType().equalsIgnoreCase("E-DIRHAM-BLUE") || request.getBillPayType().equalsIgnoreCase("E-DIRHAM-GOLD")
                || request.getBillPayType().equalsIgnoreCase("E-DIRHAM-GREEN") || request.getBillPayType().equalsIgnoreCase("E-DIRHAM-RED")
                || request.getBillPayType().equalsIgnoreCase("E-DIRHAM-SILVER")){
            notificationRequest.setBillNumber(request.getAccountNumber());
            notificationRequest.setAccountNumber((String) confirmFindTransaction.transaction.account);
        }
        if(request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-500-MB-DATA-AED-30") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-DATA-PLAN-AED-33-FIVE-SIM") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-500-INTERNATIONAL-MINS-AED-49") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-NONSTOP-50") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-1-GB-DATA-AED-50") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-1000-INTERNATIONAL-MINS-AED-79") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-2000-INTERNATIONAL-MINS-AED-99") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-3-GB-DATA-AED-100") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-6-GB-DATA-AED-150") || request.getBillPayType().equalsIgnoreCase("ET-WEEKLY-200-INTERNATIONAL-MINS-AED-25") ||
                request.getBillPayType().equalsIgnoreCase("ET-2-GB-AND-30-FLEXI-MINS-AED-50") || request.getBillPayType().equalsIgnoreCase("ET-4-GB-AND-60-FLEXI-MINS-AED-90") ||
                request.getBillPayType().equalsIgnoreCase("ET-6-GB-90-FLEXI-ICP-AED-120") || request.getBillPayType().equalsIgnoreCase("ET-COMBO-55-WITH-1GB-AND-50-FLEXI-MINS") ||
                request.getBillPayType().equalsIgnoreCase("ET-COMBO-35-WITH-500MB-AND-25-FLEXI-MINS") || request.getBillPayType().equalsIgnoreCase("ET-COMBO-110-WITH-3GB-AND-150-FLEXI-MINS") ||
                request.getBillPayType().equalsIgnoreCase("ET-COMBO-165-WITH-6GB-AND-200-FLEXI-MINS") || request.getBillPayType().equalsIgnoreCase("ET-BANGLADESH-60-MINUTES-DAILY-PACK") ||
                request.getBillPayType().equalsIgnoreCase("ET-NEW-DAILY-30-INTERNATIONAL-MINUTES") || request.getBillPayType().equalsIgnoreCase("ET-DAILY-SOCIAL-PLAN") ||
                request.getBillPayType().equalsIgnoreCase("ET-DAILY-NON-STOP-PLAN") || request.getBillPayType().equalsIgnoreCase("ET-100-MB-DAILY-DATA-PACK") ||
                request.getBillPayType().equalsIgnoreCase("ET-250-MB-DAILY-DATA-PACK") || request.getBillPayType().equalsIgnoreCase("ET-500-MB-DAILY-DATA-PACK") ||
                request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE") || request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE-PLUS-79-AED") ||
                request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE-PREMIUM") || request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE-PREMIUM-PLUS")) {
            notificationRequest.setBillNumber(request.getAccountNumber());
            notificationRequest.setAccountNumber((String) confirmFindTransaction.transaction.account);
        }
        if(request.getBillPayType().equalsIgnoreCase("ET30") || request.getBillPayType().equalsIgnoreCase("ET55")
                || request.getBillPayType().equalsIgnoreCase("ET110")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("ETISALAT");
        }
        if(request.getBillPayType().equalsIgnoreCase("DU25") || request.getBillPayType().equalsIgnoreCase("DU55")
                || request.getBillPayType().equalsIgnoreCase("DU110") || request.getBillPayType().equalsIgnoreCase("DU210")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("DU");
        }
        if(request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_5_AED") || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_20_AED")
                || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_40_AED") || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_95_AED")
                || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_185_AED") || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_370_AED")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("PUBG");
        }
        if(request.getBillPayType().equalsIgnoreCase("NETFLIX-AED-100") || request.getBillPayType().equalsIgnoreCase("NETFLIX-AED-500")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("NETFLIX");
        }
        if(request.getBillPayType().equalsIgnoreCase("STEAM-AED-40") || request.getBillPayType().equalsIgnoreCase("STEAM-AED-50")
                || request.getBillPayType().equalsIgnoreCase("STEAM-AED-75") || request.getBillPayType().equalsIgnoreCase("STEAM-AED-100")
                || request.getBillPayType().equalsIgnoreCase("STEAM-AED-200") || request.getBillPayType().equalsIgnoreCase("STEAM-AED-400")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("STEAM");
        }
        if(request.getBillPayType().equalsIgnoreCase("VO-H15") || request.getBillPayType().equalsIgnoreCase("VO-H30")
                || request.getBillPayType().equalsIgnoreCase("VO-H50")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("HELLO_CARD");
        }
        if(request.getBillPayType().equalsIgnoreCase("VO-F15") || request.getBillPayType().equalsIgnoreCase("VO-F30")
                || request.getBillPayType().equalsIgnoreCase("VO-F50")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("FIVE_CARD");
        }
        if(request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_USD_15") || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_USD_25")
                || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_USD_50") || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_3_MONTH")
                || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_12_MONTH")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("XBOX");
        }
        if(request.getBillPayType().equalsIgnoreCase("AMAZON_AE_50_AED") || request.getBillPayType().equalsIgnoreCase("AMAZON_AE_100_AED")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_AE_250_AED") || request.getBillPayType().equalsIgnoreCase("AMAZON_AE_500_AED")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_10") || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_25")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_35") || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_50")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_100")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("AMAZON");
        }
        if(request.getBillPayType().equalsIgnoreCase("PSN_AED_1M") || request.getBillPayType().equalsIgnoreCase("PSN_AED_3M")
                || request.getBillPayType().equalsIgnoreCase("PSN_AED_1Y") || request.getBillPayType().equalsIgnoreCase("PSN_USD_5")
                || request.getBillPayType().equalsIgnoreCase("PSN_USD_10") || request.getBillPayType().equalsIgnoreCase("PSN_USD_15")
                || request.getBillPayType().equalsIgnoreCase("PSN_USD_20") || request.getBillPayType().equalsIgnoreCase("PSN_USD_30")
                || request.getBillPayType().equalsIgnoreCase("PSN_USD_40") || request.getBillPayType().equalsIgnoreCase("PSN_USD_50")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_5") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_10")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_20") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_50")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_3_MONTHS") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_12_MONTHS")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_1_MONTH_MEMBERSHIP") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_5_GBP")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_10_GBP") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_20_GBP")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_50_GBP") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_3_MONTHS")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_12_MONTHS")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("PSN");
        }
        if(request.getBillPayType().equalsIgnoreCase("GPLAY_AED_30") || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_50")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_100") || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_300")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_500") || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_10")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_15") || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_25")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_50")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("GPLAY");
        }
        if(request.getBillPayType().equalsIgnoreCase("ITUNES_AED_50") || request.getBillPayType().equalsIgnoreCase("ITUNES_AED_100")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_AED_250") || request.getBillPayType().equalsIgnoreCase("ITUNES_AED_500")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_5") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_10")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_15") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_20")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_25") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_30")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_50") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_100")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_10_FR") || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_15_FR")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_25_FR") || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_50_FR")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_10") || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_15")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_25") || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_50")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_100")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("ITUNES");
        }
        if(request.getBillPayType().equalsIgnoreCase("MINECRAFT") || request.getBillPayType().equalsIgnoreCase("MINECRAFT_1720_MINECOINS")
                || request.getBillPayType().equalsIgnoreCase("MINECRAFT_3500_MINECOINS")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("MINECRAFT");
        }
        if(request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_10") || request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_20")
                || request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_35") || request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_50")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("NINTENDO");
        }
        if(request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_960_CHIPS") || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_2000_CHIPS")
                || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_4200_CHIPS") || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_11400_CHIPS")
                || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_25000_CHIPS") || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_48000_CHIPS")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("VIP_BALOOT");
        }
        if(request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_250_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_500_AE")
                || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_1000_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_1500_AE")
                || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_2000_AE")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("BIGO_LIKE");
        }
        if(request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_250_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_1000_AE")
                || request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_1500_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_2000_AE")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("BIGO_LIVE");
        }
        if(request.getBillPayType().equalsIgnoreCase("SPACETOON_GO_3_MONTHS_SUBSCRIPTION_UAE") || request.getBillPayType().equalsIgnoreCase("SPACETOON_GO_12_MONTHS_SUBSCRIPTION_UAE")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("SPACETOON_GO");
        }
        if(request.getBillPayType().equalsIgnoreCase("BEIN_1_DAY_SUBSCRIPTION_INT") || request.getBillPayType().equalsIgnoreCase("BEIN_1_MONTH_SUBSCRIPTION_INT")
                || request.getBillPayType().equalsIgnoreCase("BEIN_12_MONTH_SUBSCRIPTION_INT")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("BEIN");
        }
        if(request.getBillPayType().equalsIgnoreCase("CRUNCHYROLL_1_MONTH_SUBSCRIPTION_INT") || request.getBillPayType().equalsIgnoreCase("CRUNCHYROLL_3_MONTH_SUBSCRIPTION_INT")
                || request.getBillPayType().equalsIgnoreCase("CRUNCHYROLL_12_MONTHS_SUBSCRIPTION_INT")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("CRUNCHYROLL");
        }
        if(request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_56_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_278_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_571_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_1167_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_1783_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_3005_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_4770_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_6012_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP_PLUS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_56_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_278_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_571_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_1167_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_1783_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_3005_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_4770_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_6012_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP_PLUS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_TWILIGHT_PASS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TWILIGHT_PASS")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("MOBILE_LEGENDS");
        }
        if(request.getBillPayType().equalsIgnoreCase("VIBER_USD_10_INT") || request.getBillPayType().equalsIgnoreCase("VIBER_USD_25_INT")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("VIBER");
        }
        if(request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_1_100_10_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_2_210_21_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_5_530_53_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_10_1080_108_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_20_2200_220_DIAMONDS")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("FREEFIRE");
        }
        if(request.getBillPayType().equalsIgnoreCase("STARZPLAY_3_MONTH_SUBSCRIPTION_INT") || request.getBillPayType().equalsIgnoreCase("STARZPLAY_6_MONTH_SUBSCRIPTION_INT")
                || request.getBillPayType().equalsIgnoreCase("STARZPLAY_12_MONTHS_SUBSCRIPTION_INT")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("STARZPLAY");
        }
        if(request.getBillPayType().equalsIgnoreCase("ROBLOX_USD_10_INT") || request.getBillPayType().equalsIgnoreCase("ROBLOX_USD_25_INT")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("ROBLOX");
        }
        if(request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_ANTIVIRUS_1_USER") || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_INTERNET_SECURITY_1_DEVICE")
                || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_INTERNET_SECURITY_3_DEVICE") || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_INTERNET_SECURITY_10_DEVICE")
                || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_TOTAL_PROTECTION_5_DEVICE") || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_TOTAL_PROTECTION_10_DEVICE")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("MCAFEE");
        }
        if(request.getBillPayType().equalsIgnoreCase("SKYPE_USD_10_INT") || request.getBillPayType().equalsIgnoreCase("SKYPE_USD_25_INT")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("SKYPE");
        }
        if(request.getBillPayType().equalsIgnoreCase("EBAY_USD_10_US") || request.getBillPayType().equalsIgnoreCase("EBAY_USD_50_US")
                || request.getBillPayType().equalsIgnoreCase("EBAY_USD_100_US")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("EBAY");
        }
        if(request.getBillPayType().equalsIgnoreCase("SHAHID_VIP_3_MONTHS_SUBSCRIPTION_GCC") || request.getBillPayType().equalsIgnoreCase("SHAHID_VIP_6_MONTHS_SUBSCRIPTION_GCC")
                || request.getBillPayType().equalsIgnoreCase("SHAHID_VIP_12_MONTHS_SUBSCRIPTION_GCC")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("SHAHID_VIP");
        }
        if(request.getBillPayType().equalsIgnoreCase("SPOTIFY_US_USD_10") || request.getBillPayType().equalsIgnoreCase("SPOTIFY_US_USD_30")
                || request.getBillPayType().equalsIgnoreCase("SPOTIFY_US_USD_60")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("SPOTIFY");
        }
        if(request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_10") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_20")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_50") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_100")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_5_USD") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_10_USD")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_20_USD") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_50_USD")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_100_USD")){
            notificationRequest.setPinNumber((String) confirmFindTransaction.info.number);
            notificationRequest.setPinServiceType("RAZER");
        }
        if(request.getBillPayType().equalsIgnoreCase("E-WALLET-CASH-IN")){
            notificationRequest.setBillNumber(request.getAccountNumber());
            notificationRequest.setAccountNumber((String) confirmFindTransaction.transaction.account);
        }
        return notificationRequest;
    }

    public TsTransaction tsTransactionRequest(TsTransaction cfeTransaction, PaynetBillPayRequest request){
        cfeTransaction.setSource(request.getSource());
        cfeTransaction.setRequestId(request.getRequestId());
        cfeTransaction.setTransactionAmount(new BigDecimal(request.getAmount()));
        return cfeTransaction;
    }

    public boolean isCheckedBillPayType(PaynetBillPayRequest request){
        if(request.getBillPayType().equalsIgnoreCase("SLK50") || request.getBillPayType().equalsIgnoreCase("SLK100") || request.getBillPayType().equalsIgnoreCase("SLK200")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("SLK-PIN")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("NOL_CART")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("VIRGIN-M20") || request.getBillPayType().equalsIgnoreCase("VIRGIN-M50") ||
                request.getBillPayType().equalsIgnoreCase("VIRGIN-M100") || request.getBillPayType().equalsIgnoreCase("VIRGIN-M150") ||
                request.getBillPayType().equalsIgnoreCase("VIRGIN-M200")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("E-DIRHAM-BLUE") || request.getBillPayType().equalsIgnoreCase("E-DIRHAM-GOLD") ||
                request.getBillPayType().equalsIgnoreCase("E-DIRHAM-GREEN") || request.getBillPayType().equalsIgnoreCase("E-DIRHAM-RED") ||
                request.getBillPayType().equalsIgnoreCase("E-DIRHAM-SILVER")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("ET-POST-MOBILE") || request.getBillPayType().equalsIgnoreCase("ET-POST-DEL") ||
                request.getBillPayType().equalsIgnoreCase("ET-POST-DIALUP") || request.getBillPayType().equalsIgnoreCase("ET-POST-BROADBAND") ||
                request.getBillPayType().equalsIgnoreCase("ET-POST-EVISION") || request.getBillPayType().equalsIgnoreCase("ET-POST-ELIFE")) {
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("ET-MC")) {
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-500-MB-DATA-AED-30") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-DATA-PLAN-AED-33-FIVE-SIM") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-500-INTERNATIONAL-MINS-AED-49") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-NONSTOP-50") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-1-GB-DATA-AED-50") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-1000-INTERNATIONAL-MINS-AED-79") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-2000-INTERNATIONAL-MINS-AED-99") || request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-3-GB-DATA-AED-100") ||
                request.getBillPayType().equalsIgnoreCase("ET-MONTHLY-6-GB-DATA-AED-150") || request.getBillPayType().equalsIgnoreCase("ET-WEEKLY-200-INTERNATIONAL-MINS-AED-25") ||
                request.getBillPayType().equalsIgnoreCase("ET-2-GB-AND-30-FLEXI-MINS-AED-50") || request.getBillPayType().equalsIgnoreCase("ET-4-GB-AND-60-FLEXI-MINS-AED-90") ||
                request.getBillPayType().equalsIgnoreCase("ET-6-GB-90-FLEXI-ICP-AED-120") || request.getBillPayType().equalsIgnoreCase("ET-COMBO-55-WITH-1GB-AND-50-FLEXI-MINS") ||
                request.getBillPayType().equalsIgnoreCase("ET-COMBO-35-WITH-500MB-AND-25-FLEXI-MINS") || request.getBillPayType().equalsIgnoreCase("ET-COMBO-110-WITH-3GB-AND-150-FLEXI-MINS") ||
                request.getBillPayType().equalsIgnoreCase("ET-COMBO-165-WITH-6GB-AND-200-FLEXI-MINS") || request.getBillPayType().equalsIgnoreCase("ET-BANGLADESH-60-MINUTES-DAILY-PACK") ||
                request.getBillPayType().equalsIgnoreCase("ET-NEW-DAILY-30-INTERNATIONAL-MINUTES") || request.getBillPayType().equalsIgnoreCase("ET-DAILY-SOCIAL-PLAN") ||
                request.getBillPayType().equalsIgnoreCase("ET-DAILY-NON-STOP-PLAN") || request.getBillPayType().equalsIgnoreCase("ET-100-MB-DAILY-DATA-PACK") ||
                request.getBillPayType().equalsIgnoreCase("ET-250-MB-DAILY-DATA-PACK") || request.getBillPayType().equalsIgnoreCase("ET-500-MB-DAILY-DATA-PACK") ||
                request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE") || request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE-PLUS-79-AED") ||
                request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE-PREMIUM") || request.getBillPayType().equalsIgnoreCase("ET-VISITOR-LINE-PREMIUM-PLUS")) {
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("ET30") || request.getBillPayType().equalsIgnoreCase("ET55")
                || request.getBillPayType().equalsIgnoreCase("ET110")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("DU-POST-MOBILE")) {
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("DU-MT-TRANSFER") || request.getBillPayType().equalsIgnoreCase("DU-MT")
                || request.getBillPayType().equalsIgnoreCase("DU-MD-25AED") || request.getBillPayType().equalsIgnoreCase("DU-MD-55AED")
                || request.getBillPayType().equalsIgnoreCase("DU-MD-110AED") || request.getBillPayType().equalsIgnoreCase("DU-MD-210AED")
                || request.getBillPayType().equalsIgnoreCase("DU-MD-525AED") || request.getBillPayType().equalsIgnoreCase("DU-MI")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("DU25") || request.getBillPayType().equalsIgnoreCase("DU55")
                || request.getBillPayType().equalsIgnoreCase("DU110") || request.getBillPayType().equalsIgnoreCase("DU210")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_5_AED") || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_20_AED")
                || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_40_AED") || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_95_AED")
                || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_185_AED") || request.getBillPayType().equalsIgnoreCase("PUBG_MOBILE_370_AED")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("NETFLIX-AED-100") || request.getBillPayType().equalsIgnoreCase("NETFLIX-AED-500")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("STEAM-AED-40") || request.getBillPayType().equalsIgnoreCase("STEAM-AED-50")
                || request.getBillPayType().equalsIgnoreCase("STEAM-AED-75") || request.getBillPayType().equalsIgnoreCase("STEAM-AED-100")
                || request.getBillPayType().equalsIgnoreCase("STEAM-AED-200") || request.getBillPayType().equalsIgnoreCase("STEAM-AED-400")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("VO-H15") || request.getBillPayType().equalsIgnoreCase("VO-H30")
                || request.getBillPayType().equalsIgnoreCase("VO-H50")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("VO-F15") || request.getBillPayType().equalsIgnoreCase("VO-F30")
                || request.getBillPayType().equalsIgnoreCase("VO-F50")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_USD_15") || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_USD_25")
                || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_USD_50") || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_3_MONTH")
                || request.getBillPayType().equalsIgnoreCase("XBOX_LIVE_12_MONTH")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("AMAZON_AE_50_AED") || request.getBillPayType().equalsIgnoreCase("AMAZON_AE_100_AED")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_AE_250_AED") || request.getBillPayType().equalsIgnoreCase("AMAZON_AE_500_AED")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_10") || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_25")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_35") || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_50")
                || request.getBillPayType().equalsIgnoreCase("AMAZON_USD_100")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("PSN_AED_1M") || request.getBillPayType().equalsIgnoreCase("PSN_AED_3M")
                || request.getBillPayType().equalsIgnoreCase("PSN_AED_1Y") || request.getBillPayType().equalsIgnoreCase("PSN_USD_5")
                || request.getBillPayType().equalsIgnoreCase("PSN_USD_10") || request.getBillPayType().equalsIgnoreCase("PSN_USD_15")
                || request.getBillPayType().equalsIgnoreCase("PSN_USD_20") || request.getBillPayType().equalsIgnoreCase("PSN_USD_30")
                || request.getBillPayType().equalsIgnoreCase("PSN_USD_40") || request.getBillPayType().equalsIgnoreCase("PSN_USD_50")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_5") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_10")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_20") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_USD_50")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_3_MONTHS") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_12_MONTHS")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UAE_1_MONTH_MEMBERSHIP") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_5_GBP")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_10_GBP") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_20_GBP")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_50_GBP") || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_3_MONTHS")
                || request.getBillPayType().equalsIgnoreCase("PLAYSTATION_NETWORK_UK_12_MONTHS")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("GPLAY_AED_30") || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_50")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_100") || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_300")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_AED_500") || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_10")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_15") || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_25")
                || request.getBillPayType().equalsIgnoreCase("GPLAY_USD_50")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("ITUNES_AED_50") || request.getBillPayType().equalsIgnoreCase("ITUNES_AED_100")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_AED_250") || request.getBillPayType().equalsIgnoreCase("ITUNES_AED_500")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_5") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_10")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_15") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_20")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_25") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_30")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_50") || request.getBillPayType().equalsIgnoreCase("ITUNES_USD_100")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_10_FR") || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_15_FR")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_25_FR") || request.getBillPayType().equalsIgnoreCase("ITUNES_FRANCE_EUR_50_FR")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_10") || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_15")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_25") || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_50")
                || request.getBillPayType().equalsIgnoreCase("ITUNES_UK_GBP_100")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("MINECRAFT") || request.getBillPayType().equalsIgnoreCase("MINECRAFT_1720_MINECOINS")
                || request.getBillPayType().equalsIgnoreCase("MINECRAFT_3500_MINECOINS")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_10") || request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_20")
                || request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_35") || request.getBillPayType().equalsIgnoreCase("NINTENDO_US_USD_50")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_960_CHIPS") || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_2000_CHIPS")
                || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_4200_CHIPS") || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_11400_CHIPS")
                || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_25000_CHIPS") || request.getBillPayType().equalsIgnoreCase("VIP_BALOOT_48000_CHIPS")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_250_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_500_AE")
                || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_1000_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_1500_AE")
                || request.getBillPayType().equalsIgnoreCase("BIGO_LIKE_USD_2000_AE")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_250_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_1000_AE")
                || request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_1500_AE") || request.getBillPayType().equalsIgnoreCase("BIGO_LIVE_USD_2000_AE")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("SPACETOON_GO_3_MONTHS_SUBSCRIPTION_UAE") || request.getBillPayType().equalsIgnoreCase("SPACETOON_GO_12_MONTHS_SUBSCRIPTION_UAE")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("BEIN_1_DAY_SUBSCRIPTION_INT") || request.getBillPayType().equalsIgnoreCase("BEIN_1_MONTH_SUBSCRIPTION_INT")
                || request.getBillPayType().equalsIgnoreCase("BEIN_12_MONTH_SUBSCRIPTION_INT")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("CRUNCHYROLL_1_MONTH_SUBSCRIPTION_INT") || request.getBillPayType().equalsIgnoreCase("CRUNCHYROLL_3_MONTH_SUBSCRIPTION_INT")
                || request.getBillPayType().equalsIgnoreCase("CRUNCHYROLL_12_MONTHS_SUBSCRIPTION_INT")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_56_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_278_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_571_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_1167_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_1783_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_3005_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_4770_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_6012_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP_PLUS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_56_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_278_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_571_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_1167_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_1783_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_3005_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_4770_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_6012_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP_PLUS")
                || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TOP_UP_TWILIGHT_PASS") || request.getBillPayType().equalsIgnoreCase("MOBILE_LEGENDS_TWILIGHT_PASS")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("VIBER_USD_10_INT") || request.getBillPayType().equalsIgnoreCase("VIBER_USD_25_INT")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_1_100_10_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_2_210_21_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_5_530_53_DIAMONDS") || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_10_1080_108_DIAMONDS")
                || request.getBillPayType().equalsIgnoreCase("FREEFIRE_USD_20_2200_220_DIAMONDS")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("STARZPLAY_3_MONTH_SUBSCRIPTION_INT") || request.getBillPayType().equalsIgnoreCase("STARZPLAY_6_MONTH_SUBSCRIPTION_INT")
                || request.getBillPayType().equalsIgnoreCase("STARZPLAY_12_MONTHS_SUBSCRIPTION_INT")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("ROBLOX_USD_10_INT") || request.getBillPayType().equalsIgnoreCase("ROBLOX_USD_25_INT")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_ANTIVIRUS_1_USER") || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_INTERNET_SECURITY_1_DEVICE")
                || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_INTERNET_SECURITY_3_DEVICE") || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_INTERNET_SECURITY_10_DEVICE")
                || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_TOTAL_PROTECTION_5_DEVICE") || request.getBillPayType().equalsIgnoreCase("MCAFEE_MCAFEE_TOTAL_PROTECTION_10_DEVICE")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("SKYPE_USD_10_INT") || request.getBillPayType().equalsIgnoreCase("SKYPE_USD_25_INT")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("EBAY_USD_10_US") || request.getBillPayType().equalsIgnoreCase("EBAY_USD_50_US")
                || request.getBillPayType().equalsIgnoreCase("EBAY_USD_100_US")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("SHAHID_VIP_3_MONTHS_SUBSCRIPTION_GCC") || request.getBillPayType().equalsIgnoreCase("SHAHID_VIP_6_MONTHS_SUBSCRIPTION_GCC")
                || request.getBillPayType().equalsIgnoreCase("SHAHID_VIP_12_MONTHS_SUBSCRIPTION_GCC")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("SPOTIFY_US_USD_10") || request.getBillPayType().equalsIgnoreCase("SPOTIFY_US_USD_30")
                || request.getBillPayType().equalsIgnoreCase("SPOTIFY_US_USD_60")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_10") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_20")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_50") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_PINS_USD_100")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_5_USD") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_10_USD")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_20_USD") || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_50_USD")
                || request.getBillPayType().equalsIgnoreCase("RAZER_GOLD_TOP_UP_100_USD")){
            return true;
        }
        if(request.getBillPayType().equalsIgnoreCase("E-WALLET-CASH-IN")){
            return true;
        }
        return false;
    }


    public String tsTransactionNote(PaynetBillPayRequest request,ConfirmFindTransaction confirmFindTransaction){
        String notes = "Account No : " + request.getAccountNumber() + " Simulation : " + confirmFindTransaction.simulation + " Transaction Id : " + confirmFindTransaction.transaction.id
                + " Product Name : " + confirmFindTransaction.transaction.service.name  + " Pin Number : " + confirmFindTransaction.info.number;
        return notes;
    }

    public String mobilePostpaidTopupNote(PaynetBillPayRequest request,ConfirmFindTransaction confirmFindTransaction){
        String notes = "Bill Type: " + request.getBillPayType() + " Account No : " + request.getAccountNumber() + " Simulation : " + confirmFindTransaction.simulation
                + " Transaction Id : " + confirmFindTransaction.transaction.id + " Product Name : " + confirmFindTransaction.transaction.service.name;
        return notes;
    }

    @Override
    public ConfirmFindTransaction confirmTransaction(PaynetBillPayRequest request) throws PocketException {
        paynetAuthorization(request);
        BaseResponseObject baseResponseObject=null;
        ConfirmFindTransaction confirmFindTransaction = null;

        try {
            if(request.getCurrencyCode()!=null && request.getCurrencyCode().equalsIgnoreCase(CurrencyCode.AED)){
                confirmFindTransaction = new PaynetExternalCall(paynetConfig.getPaynetTransactionConfirm()).callPaynetTransaction(request, headers, paynetAuthorization, paynetConfig, new TsTransaction());
            }

            if(confirmFindTransaction == null){
                return confirmFindTransaction;
            }

        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PaynetBillPayFailed.getKeyString(),
                    "Paynet Bill pay failed. Reason : "+baseResponseObject.getError().getErrorMessage()));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }
        return confirmFindTransaction;
    }

    @Override
    public BaseResponseObject findTransaction(PaynetBillPayRequest request) throws PocketException {
        paynetAuthorization(request);
        BaseResponseObject baseResponseObject = null;
        ConfirmFindTransaction confirmFindTransaction = null;

        try {
            if (request.getCurrencyCode() != null && request.getCurrencyCode().equalsIgnoreCase(CurrencyCode.AED)) {
                TsTransaction tsTransaction = tsTransactionRepository.findByTokenOrRefTransactionToken(request.getExternalTransactionId(), request.getTransactionId());

                if(tsTransaction !=null){
                    if((tsTransaction.getToken().matches(request.getExternalTransactionId())) || (tsTransaction.getRefTransactionToken().matches(request.getTransactionId()))){
                        confirmFindTransaction = new PaynetExternalCall(paynetConfig.getPaynetTransactionFind()).callPaynetTransaction(request, headers, paynetAuthorization,paynetConfig, tsTransaction);
                    }
                }

            } else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject("400","AED currency code not matched."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return baseResponseObject;
            }
            if (confirmFindTransaction == null) {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject(PocketErrorCode.PaynetTransactionFindNotFound.getKeyString(),
                        "Paynet Transaction not found. Reason : Token or Transaction Id not matched."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return baseResponseObject;
            } else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(null);
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.OK);
                baseResponseObject.setData(confirmFindTransaction);
            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PaynetBillPayFailed.getKeyString(),
                    "Paynet Bill pay find failed. Reason : "+baseResponseObject.getError().getErrorMessage()));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    @Override
    public PaynetServiceResponse getPaynetAllServices(PaynetServicesRequest request) throws PocketException, IOException {
        PaynetServiceResponse response = new PaynetServiceResponse();

        paynetAuthorization(request);
        actualPaynetAuthorization();

        PaynetEmptyRequestTemp paynetEmptyRequest = new PaynetEmptyRequestTemp();
        paynetEmptyRequest.setAuth(auth);

        RestTemplate paynetServiceListCall = new RestTemplate();
        ResponseEntity<String> exchange = paynetServiceListCall.exchange(paynetConfig.getPaynetService(), HttpMethod.POST, new HttpEntity<>(paynetEmptyRequest, headers), String.class);
        if(exchange.getStatusCode()== HttpStatus.OK){

            if(exchange.getStatusCode()== HttpStatus.OK){
                response.setData(new Gson().fromJson(exchange.getBody(), Services.class));
                response.setError(null);
                response.setStatus(PocketConstants.OK);
                return response;
            }
        }
        throw new PocketException(PocketErrorCode.PAYNET_SERVICES_NOT_FOUND);
    }

    @Override
    public PaynetCountryResponse getPaynetAllCountries(PaynetEmptyRequest request) throws PocketException, IOException {
        PaynetCountryResponse response = new PaynetCountryResponse();

        paynetAuthorization(request);
        actualPaynetAuthorization();

        PaynetEmptyRequestTemp paynetEmptyRequest = new PaynetEmptyRequestTemp();
        paynetEmptyRequest.setAuth(auth);

        RestTemplate paynetCountryListCall = new RestTemplate();
        ResponseEntity<String> exchange = paynetCountryListCall.exchange(paynetConfig.getPaynetProvider(), HttpMethod.POST, new HttpEntity<>(paynetEmptyRequest, headers), String.class);
        if(exchange.getStatusCode()== HttpStatus.OK){
            response.setData(new Gson().fromJson(exchange.getBody(), Country.class));
            response.setError(null);
            response.setStatus(PocketConstants.OK);
            return response;
        }
        throw new PocketException(PocketErrorCode.PAYNET_COUNTRIES_NOT_FOUND);
    }

    @Override
    public PaynetCountryProviderResponse getPaynetCountryProviders(PaynetCountryProviderRequest request) throws PocketException, IOException {
        PaynetCountryProviderResponse response = new PaynetCountryProviderResponse();

        if(String.valueOf(request.getCountryId()) == null || String.valueOf(request.getCountryId()).matches("0") || String.valueOf(request.getCountryId()).isEmpty()){
            throw new PocketException(PocketErrorCode.PAYNET_COUNTRY_ID_REQUIRED, "Country Id required");
        }

        paynetAuthorization(request);
        actualPaynetAuthorization();

        PaynetCountryProviderRequestTemp paynetCountryProviderRequest = new PaynetCountryProviderRequestTemp();
        paynetCountryProviderRequest.setAuth(auth);
        paynetCountryProviderRequest.setCountry_id(request.getCountryId());

        RestTemplate paynetCountryProviderListCall = new RestTemplate();
        ResponseEntity<String> exchange = paynetCountryProviderListCall.exchange(paynetConfig.getPaynetProvider(), HttpMethod.POST, new HttpEntity<>(paynetCountryProviderRequest, headers), String.class);
        if(exchange.getStatusCode()== HttpStatus.OK){
            response.setData(new Gson().fromJson(exchange.getBody(), Provider.class));
            response.setError(null);
            response.setStatus(PocketConstants.OK);
            return response;
        }
        throw new PocketException(PocketErrorCode.PAYNET_PROVIDER_NOT_FOUND);
    }

    @Override
    public PaynetProviderOperatorResponse getPaynetProviderOperators(PaynetProviderOperatorRequest request) throws PocketException, IOException {
        PaynetProviderOperatorResponse response = new PaynetProviderOperatorResponse();

        if(String.valueOf(request.getProviderId()) == null || String.valueOf(request.getProviderId()).matches("0") || String.valueOf(request.getProviderId()).isEmpty()){
            throw new PocketException(PocketErrorCode.PAYNET_PROVIDER_ID_REQUIRED, "Provider Id required");
        }

        paynetAuthorization(request);
        actualPaynetAuthorization();

        PaynetProviderOperatorRequestTemp paynetProviderOperatorRequest = new PaynetProviderOperatorRequestTemp();
        paynetProviderOperatorRequest.setAuth(auth);
        paynetProviderOperatorRequest.setProvider_id(request.getProviderId());

        RestTemplate paynetProviderOperatorListCall = new RestTemplate();
        ResponseEntity<String> exchange = paynetProviderOperatorListCall.exchange(paynetConfig.getPaynetProvider(), HttpMethod.POST, new HttpEntity<>(paynetProviderOperatorRequest, headers), String.class);
        if(exchange.getStatusCode()== HttpStatus.OK){
            response.setData(new Gson().fromJson(exchange.getBody(), ProviderOperator.class));
            response.setError(null);
            response.setStatus(PocketConstants.OK);
            return response;
        }
        throw new PocketException(PocketErrorCode.PAYNET_PROVIDER_OPERATOR_NOT_FOUND);
    }

    @Override
    public PaynetBalanceResponse getPaynetBalance(PaynetEmptyRequest request) throws PocketException, IOException {
        PaynetBalanceResponse response = new PaynetBalanceResponse();

        paynetAuthorization(request);
        actualPaynetAuthorization();

        PaynetEmptyRequestTemp paynetEmptyRequest = new PaynetEmptyRequestTemp();
        paynetEmptyRequest.setAuth(auth);

        RestTemplate paynetBalanceCall = new RestTemplate();
        ResponseEntity<String> exchange = paynetBalanceCall.exchange(paynetConfig.getPaynetBalance(), HttpMethod.POST, new HttpEntity<>(paynetEmptyRequest, headers), String.class);
        if(exchange.getStatusCode()== HttpStatus.OK){
            response.setData(new Gson().fromJson(exchange.getBody(), Balance.class));
            response.setError(null);
            response.setStatus(PocketConstants.OK);
            return response;
        }
        throw new PocketException(PocketErrorCode.PAYNET_BALANCE_NOT_FOUND);
    }

    @Override
    public PaynetInfoResponse getPaynetInfoDetail(PaynetEmptyRequest request) throws PocketException, IOException {
        PaynetInfoResponse response = new PaynetInfoResponse();

        paynetAuthorization(request);
        actualPaynetAuthorization();

        PaynetEmptyRequestTemp paynetEmptyRequest = new PaynetEmptyRequestTemp();
        paynetEmptyRequest.setAuth(auth);

        RestTemplate paynetInfoCall = new RestTemplate();
        ResponseEntity<String> exchange = paynetInfoCall.exchange(paynetConfig.getPaynetInfo(), HttpMethod.POST, new HttpEntity<>(paynetEmptyRequest, headers), String.class);

        if(exchange.getStatusCode()== HttpStatus.OK){
            response.setData(new Gson().fromJson(exchange.getBody(), InfoDetail.class));
            response.setError(null);
            response.setStatus(PocketConstants.OK);
            return response;
        }
        throw new PocketException(PocketErrorCode.PAYNET_INFO_NOT_FOUND);
    }

    @Override
    public BaseResponseObject doMobilePostPaidTopup(TransactionRequest request, TsTransaction tsTransaction) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();

        try {
            PaynetBillPayRequest paynetMobilePostpaidTopUpRequest = new PaynetBillPayRequest();
            PaynetServiceResponse serviceResponse = getPaynetAllServices(new PaynetServicesRequest());
            Services services = new Services();
            services = (Services) serviceResponse.getData();

            for(ServiceItem serviceItem : services.services){
                if(request.getTopupProviderName().matches("Etisalat UAE") && serviceItem.id.equals("et-post-mobile")){
                    paynetMobilePostpaidTopUpRequest.setBillPayType("ET-POST-MOBILE");
                    paynetMobilePostpaidTopUpRequest.setProduct("et-post-mobile");
                    tsTransaction.setTopupProviderCode(request.getTopupProviderCode());
                    tsTransaction.setTopupProviderName(request.getTopupProviderName());
                }
                if(request.getTopupProviderName().matches("DU UAE") && serviceItem.id.equals("du-post-mobile")){
                    paynetMobilePostpaidTopUpRequest.setBillPayType("DU-POST-MOBILE");
                    paynetMobilePostpaidTopUpRequest.setProduct("du-post-mobile");
                    tsTransaction.setTopupProviderCode(request.getTopupProviderCode());
                    tsTransaction.setTopupProviderName(request.getTopupProviderName());
                }
            }

            paynetMobilePostpaidTopUpRequest.setSimulation(0); //1=test, 0=live
            paynetMobilePostpaidTopUpRequest.setExternalTransactionId(tsTransaction.getToken());
            paynetMobilePostpaidTopUpRequest.setTransactionId(tsTransaction.getToken());

            paynetMobilePostpaidTopUpRequest.setAccountNumber(request.getReceiverMobileNumber());
            paynetMobilePostpaidTopUpRequest.setAmount(request.getConvertedAmount().doubleValue());
            paynetMobilePostpaidTopUpRequest.setCurrencyCode(request.getCurrencyCode());

            ConfirmFindTransaction confirmPostpaidTransaction=null;
            if(request.getCurrencyCode()!=null && request.getCurrencyCode().equalsIgnoreCase(CurrencyCode.AED)){
                //Check balance
                PaynetBalanceResponse paynetBalanceResponse = getPaynetBalance(new PaynetEmptyRequest());
                Balance balance = (Balance) paynetBalanceResponse.getData();

                if(balance !=null && balance.wallets!=null && ((Number)balance.wallets.main).intValue() < request.getConvertedAmount()){
                    baseResponseObject = new BaseResponseObject();
                    baseResponseObject.setError(new ErrorObject("400","Top up master balance is less the requested actual balance."));
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.ERROR);
                    baseResponseObject.setData(null);
                    return baseResponseObject;
                }

                paynetAuthorization(paynetMobilePostpaidTopUpRequest);
                actualPaynetAuthorization();

                confirmPostpaidTransaction = new PaynetExternalCall(paynetConfig.getPaynetTransaction()).
                        callPaynetTransaction(paynetMobilePostpaidTopUpRequest, headers, paynetAuthorization, paynetConfig, new TsTransaction());

                if(confirmPostpaidTransaction.transaction != null){
                    paynetMobilePostpaidTopUpRequest.setTransactionId(confirmPostpaidTransaction.transaction.id.toString());
                    confirmPostpaidTransaction = confirmTransaction(paynetMobilePostpaidTopUpRequest);

                    if(confirmPostpaidTransaction.transaction == null){
                        baseResponseObject = new BaseResponseObject();
                        baseResponseObject.setError(new ErrorObject("400","Top up check confirm api return null."));
                        baseResponseObject.setRequestId(request.getRequestId());
                        baseResponseObject.setStatus(PocketConstants.ERROR);
                        baseResponseObject.setData(null);
                        return baseResponseObject;
                    }

                    baseResponseObject = new BaseResponseObject();
                    baseResponseObject.setData(confirmPostpaidTransaction);
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);

                    tsTransaction.setNotes(mobilePostpaidTopupNote(paynetMobilePostpaidTopUpRequest, confirmPostpaidTransaction));
                    tsTransaction.setRefTransactionToken(confirmPostpaidTransaction.transaction.id.toString());
                    tsTransactionRepository.save(tsTransaction);
                }else{
                    baseResponseObject = new BaseResponseObject();
                    baseResponseObject.setError(new ErrorObject("400","Top up check api return null."));
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.ERROR);
                    baseResponseObject.setData(null);
                    return baseResponseObject;
                }
            }else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject("400","AED currency code not matched."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return baseResponseObject;
            }
        }catch (Exception e){
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            BaseResponseObject responseObject = new BaseResponseObject();
            responseObject.setError(new ErrorObject(PocketErrorCode.PaynetBillPayFailed.getKeyString(),
                    "Paynet postpaid mobile top up failed. Reason : "+baseResponseObject.getError().getErrorMessage()));
            responseObject.setRequestId(request.getRequestId());
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
            return responseObject;
        }
        return baseResponseObject;
    }

    // for DU & Etisalat
    public void postPaidMinMaxValidation(TransactionRequest request) throws PocketException, IOException {
        BaseResponseObject baseResponseObject = new BaseResponseObject();
            PaynetServiceResponse serviceResponse = getPaynetAllServices(new PaynetServicesRequest());
            Services services = new Services();
            services = (Services) serviceResponse.getData();

            if(services != null && services.services.size() >0) {
                for (ServiceItem serviceItem : services.services) {
                    if (request.getTopupProviderName().matches("Etisalat UAE") && serviceItem.id.equals("et-post-mobile")) {
                        if ((request.getTransferAmount() < Double.parseDouble((String) serviceItem.amountMin))
                                || (request.getTransferAmount() > Double.parseDouble((String) serviceItem.amountMax))) {

                            String validateErrorMsg = "Please pay from ".concat(String.valueOf(serviceItem.amountMin)).concat(" AED to ")
                                    .concat(String.valueOf(serviceItem.amountMax).concat(" AED"));

                            logWriterUtility.error(request.getRequestId(), validateErrorMsg);
                            throw new PocketException(validateErrorMsg, validateErrorMsg);
                        }
                    }
                    if (request.getTopupProviderName().matches("DU UAE") && serviceItem.id.equals("du-post-mobile")) {
                        if ((request.getTransferAmount() < Double.parseDouble((String) serviceItem.amountMin))
                                || (request.getTransferAmount() > Double.parseDouble((String) serviceItem.amountMax))) {
                            String validateErrorMsg = "Please pay from ".concat(String.valueOf(serviceItem.amountMin)).concat(" AED to ")
                                    .concat(String.valueOf(serviceItem.amountMax).concat(" AED"));

                            logWriterUtility.error(request.getRequestId(), validateErrorMsg);
                            throw new PocketException(validateErrorMsg, validateErrorMsg);
                        }
                    }
                }
            }
    }

    public <T> void paynetAuthorization(T request){
        headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);

        long key = new Date().getTime();
        String concateId_Token_Key = String.valueOf(paynetConfig.getPosId()).concat(paynetConfig.getPosToken()).concat(String.valueOf(key));
        String hashCode = generateMd5hashCode(concateId_Token_Key);

        paynetAuthorization = new PaynetAuthorization();
        paynetAuthorization.setId(paynetConfig.getPosId());
        paynetAuthorization.setKey(key);
        paynetAuthorization.setHash(hashCode);
    }

    public String requestId(){
       String hashCode = generateMd5hashCode(String.valueOf(new Date().getTime()));
       return hashCode.substring(hashCode.length() - 16);
    }

    public static String generateMd5hashCode(String concateId_Token_Key) {
        String hash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(concateId_Token_Key.getBytes());

            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            hash = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash;
    }

    public void actualPaynetAuthorization(){
        auth = new PaynetAuthorizationRequest();
        auth.setId(paynetAuthorization.getId());
        auth.setKey(paynetAuthorization.getKey());
        auth.setHash(paynetAuthorization.getHash());
    }

}

class PaynetEmptyRequestTemp{
    PaynetAuthorizationRequest auth;

    public PaynetEmptyRequestTemp() {
    }

    public PaynetAuthorizationRequest getAuth() {
        return auth;
    }

    public void setAuth(PaynetAuthorizationRequest auth) {
        this.auth = auth;
    }
}

class PaynetAuthorizationRequest{
    private int id;
    private long key;
    private String hash;

    public PaynetAuthorizationRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}

class PaynetCountryProviderRequestTemp{
    PaynetAuthorizationRequest auth;
    int country_id;

    public PaynetAuthorizationRequest getAuth() {
        return auth;
    }

    public void setAuth(PaynetAuthorizationRequest auth) {
        this.auth = auth;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }
}

class PaynetProviderOperatorRequestTemp{
    PaynetAuthorizationRequest auth;
    int provider_id;

    public PaynetAuthorizationRequest getAuth() {
        return auth;
    }

    public void setAuth(PaynetAuthorizationRequest auth) {
        this.auth = auth;
    }

    public int getProvider_id() {
        return provider_id;
    }

    public void setProvider_id(int provider_id) {
        this.provider_id = provider_id;
    }
}
