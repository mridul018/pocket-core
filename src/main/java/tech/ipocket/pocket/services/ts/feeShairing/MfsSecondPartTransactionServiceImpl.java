package tech.ipocket.pocket.services.ts.feeShairing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.UmSrAgentMappingRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.response.ts.FeeSharingTransactionDetailsResponse;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.TransactionServiceCodeConstants;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class MfsSecondPartTransactionServiceImpl implements MfsSecondPartTransactionService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsGlRepository generalLedgerRepository;

    @Autowired
    private TsGlTransactionDetailsRepository glTransactionDetailsRepository;

    @Autowired
    private TsTransactionDetailsRepository transactionDetailRepository;

    @Autowired
    private AccountService clientService;

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository transactionRepository;

    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private TsClientRepository tsClientRepository;
    @Autowired
    private UmSrAgentMappingRepository srAgentMappingRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRES_NEW)
    @Override
    public FeeSharingTransactionDetailsResponse feeShareGlToDirectWallet(TsFeeStakeholder feeStakeholder, String senderGlCode,
                                                                         String mainTxToken, BigDecimal transactionAmount,
                                                                         String requestId, BigDecimal totalFeeAmount) {

        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, transactionAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWallet");
        detailsResponse.setMainTransactionToken(mainTxToken);
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(totalFeeAmount);
        detailsResponse.setSenderAccount(senderGlCode);

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        TsTransaction oldTransaction = transactionRepository.findFirstByToken(mainTxToken);

        if (oldTransaction == null) {
            logWriterUtility.error(requestId, "Invalid old transaction token :" + mainTxToken);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Invalid old transaction token :" + mainTxToken);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Main transaction found :" + oldTransaction.getId());

        TsGl senderGl = generalLedgerRepository.findFirstByGlCode(senderGlCode);

        if (senderGl == null) {
            logWriterUtility.error(requestId, "Sender gl code not found :" + senderGlCode);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender gl code not found :" + senderGlCode);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender gl found :" + senderGl.getGlCode());
        detailsResponse.setPayerName(senderGl.getGlName());
        detailsResponse.setSenderAccount("" + senderGl.getGlCode());

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient receiverWallet = clientRepository.findFirstByWalletNoAndUserStatusNotIn(feeStakeholder.getWalletNumber(), statusNotIn);
        if (receiverWallet == null) {
            logWriterUtility.error(requestId, "Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver wallet found :" + receiverWallet.getWalletNo());

        String receiverGlName = GlConstants.getGlNameByCustomerType(receiverWallet.getGroupCode());
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);


        BigDecimal senderBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());
        if (senderBalance == null) {
            logWriterUtility.error(requestId, "Sender Account balance not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender Account balance not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender Balance loaded.");

        if (senderBalance.compareTo(transactionAmount) < 0) {
            logWriterUtility.error(requestId, "Insufficient Sender Account balance");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Insufficient Sender Account balance");
            return detailsResponse;
        }

        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.Commission.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "Commission Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Commission Service not found");
            return detailsResponse;
        }


        //Add entry on transaction
        TsTransaction feeShareTransaction = null;
        try {
            feeShareTransaction = buildTransaction(senderGl, receiverWallet.getWalletNo(), cfeService.getServiceCode(), transactionAmountData, feeData, requestId, mainTxToken);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        feeShareTransaction.setReceiverWallet(receiverWallet.getWalletNo());
        feeShareTransaction.setSenderGl("" + senderGl.getGlCode());

//        feeShareTransaction.setIsSenderGl(true);
//        feeShareTransaction.setIsReceiverGl(false);


        feeShareTransaction.setNotes("Fee Shared for transaction :" + mainTxToken);
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        feeShareTransaction.setLogicalSender(senderGlCode);
        feeShareTransaction.setLogicalReceiver(receiverWallet.getWalletNo());

        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

//        Long transactionId, Long glCode, BigDecimal debitAmount, BigDecimal creditAmount,
//                String productCode, String requestId

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.DEBIT,
                "" + senderGl.getGlCode(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT,
                receiverWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, senderGl.getGlName(),
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, receiverGlName, BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal receiverClientRunningBalance = clientService.updateClientCurrentBalance(receiverWallet,
                receiverWallet.getWalletNo());

        BigDecimal senderGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());

        feeShareTransaction.setReceiverRunningBalance(receiverClientRunningBalance);
        feeShareTransaction.setSenderRunningBalance(senderGlRunningBalance);
        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverClientRunningBalance);
        detailsResponse.setSenderRunningBalance(senderGlRunningBalance);
        detailsResponse.setNewTransactionToken(feeShareTransaction.getToken());

        return detailsResponse;
    }

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRES_NEW)
    @Override
    public FeeSharingTransactionDetailsResponse feeShareGlToDirectWalletForTopUpAgent(TsFeeStakeholder feeStakeholder, String senderGlCode, String mainTxToken, BigDecimal transactionAmount, String requestId, BigDecimal totalFeeAmount) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        logWriterUtility.info(requestId,"*****  feeShareGlToDirectWalletForTopUpAgent started *****");
        logWriterUtility.trace(requestId,"*****  feeShareGlToDirectWalletForTopUpAgent started *****");
        logWriterUtility.debug(requestId,"*****  feeShareGlToDirectWalletForTopUpAgent started *****");

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, transactionAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWallet");
        detailsResponse.setMainTransactionToken(mainTxToken);
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(totalFeeAmount);
        detailsResponse.setSenderAccount(senderGlCode);

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        TsTransaction oldTransaction = transactionRepository.findFirstByToken(mainTxToken);

        if (oldTransaction == null) {
            logWriterUtility.error(requestId, "Invalid old transaction token : " + mainTxToken);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(" Invalid old transaction token : " + mainTxToken);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Main transaction found :" + oldTransaction.getId());

        TsGl senderGl = generalLedgerRepository.findFirstByGlCode(senderGlCode);

        if (senderGl == null) {
            logWriterUtility.error(requestId, "Sender gl code not found :" + senderGlCode);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender gl code not found :" + senderGlCode);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender gl found :" + senderGl.getGlCode());
        detailsResponse.setPayerName(senderGl.getGlName());
        detailsResponse.setSenderAccount("" + senderGl.getGlCode());

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient receiverWallet = clientRepository.findFirstByWalletNoAndUserStatusNotIn(oldTransaction.getSenderWallet(), statusNotIn);
        if (receiverWallet == null) {
            logWriterUtility.error(requestId, "Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver wallet found :" + receiverWallet.getWalletNo());

        String receiverGlName = GlConstants.getGlNameByCustomerType(receiverWallet.getGroupCode());
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);


        BigDecimal senderBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());
        if (senderBalance == null) {
            logWriterUtility.error(requestId, "Sender Account balance not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender Account balance not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender Balance loaded.");

        if (senderBalance.compareTo(transactionAmount) < 0) {
            logWriterUtility.error(requestId, "Insufficient Sender Account balance");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Insufficient Sender Account balance");
            return detailsResponse;
        }

        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.Commission.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "Commission Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Commission Service not found");
            return detailsResponse;
        }

        //Add entry on transaction
        TsTransaction feeShareTransaction = null;
        try {
            feeShareTransaction = buildTransaction(senderGl, receiverWallet.getWalletNo(), cfeService.getServiceCode(), transactionAmountData, feeData, requestId, mainTxToken);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        feeShareTransaction.setReceiverWallet(receiverWallet.getWalletNo());
        feeShareTransaction.setSenderGl("" + senderGl.getGlCode());

//        feeShareTransaction.setIsSenderGl(true);
//        feeShareTransaction.setIsReceiverGl(false);


        feeShareTransaction.setNotes("Fee Shared for transaction :" + mainTxToken);
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        feeShareTransaction.setLogicalSender(senderGlCode);
        feeShareTransaction.setLogicalReceiver(receiverWallet.getWalletNo());

        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

//        Long transactionId, Long glCode, BigDecimal debitAmount, BigDecimal creditAmount,
//                String productCode, String requestId

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.DEBIT,
                "" + senderGl.getGlCode(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT,
                receiverWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, senderGl.getGlName(),
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, receiverGlName, BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal receiverClientRunningBalance = clientService.updateClientCurrentBalance(receiverWallet,
                receiverWallet.getWalletNo());

        BigDecimal senderGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());

        feeShareTransaction.setReceiverRunningBalance(receiverClientRunningBalance);
        feeShareTransaction.setSenderRunningBalance(senderGlRunningBalance);
        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverClientRunningBalance);
        detailsResponse.setSenderRunningBalance(senderGlRunningBalance);
        detailsResponse.setNewTransactionToken(feeShareTransaction.getToken());

        return detailsResponse;
    }

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRES_NEW)
    @Override
    public FeeSharingTransactionDetailsResponse feeShareGlToDirectWalletForSR(TsFeeStakeholder feeStakeholder, String senderGlCode, String mainTxToken, BigDecimal transactionAmount, String requestId, BigDecimal totalFeeAmount) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, transactionAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWallet");
        detailsResponse.setMainTransactionToken(mainTxToken);
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(totalFeeAmount);
        detailsResponse.setSenderAccount(senderGlCode);

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        TsTransaction oldTransaction = transactionRepository.findFirstByToken(mainTxToken);

        if (oldTransaction == null) {
            logWriterUtility.error(requestId, "Invalid old transaction token :" + mainTxToken);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Invalid old transaction token :" + mainTxToken);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Main transaction found :" + oldTransaction.getId());

        TsGl senderGl = generalLedgerRepository.findFirstByGlCode(senderGlCode);

        if (senderGl == null) {
            logWriterUtility.error(requestId, "Sender gl code not found :" + senderGlCode);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender gl code not found :" + senderGlCode);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender gl found :" + senderGl.getGlCode());
        detailsResponse.setPayerName(senderGl.getGlName());
        detailsResponse.setSenderAccount("" + senderGl.getGlCode());

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());


        TsClient merchantWallet=null;
        if(oldTransaction.getTransactionType().equals(TransactionServiceCodeConstants.AGENT_CASH_IN)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(oldTransaction.getSenderWallet(), statusNotIn);
        }else if(oldTransaction.getTransactionType().equals(TransactionServiceCodeConstants.TOP_UP)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(oldTransaction.getSenderWallet(), statusNotIn);
        }
        else if(oldTransaction.getTransactionType().equals(TransactionServiceCodeConstants.AGENT_CASH_OUT)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(oldTransaction.getReceiverWallet(), statusNotIn);
        }else{
            logWriterUtility.error(requestId,"Unsupported transaction type for fee shairing SR");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Unsupported transaction type for fee shairing SR");
            return detailsResponse;
        }

        if(merchantWallet==null){
            logWriterUtility.error(requestId,"Sender merchant wallet not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender merchant wallet not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"Sender merchant wallet found");

        UmUserInfo agentUserInfo=userInfoRepository.findByLoginId(merchantWallet.getWalletNo());
        if(agentUserInfo==null){
            logWriterUtility.error(requestId,"Agent user info not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Agent user info not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"merchant user info found");

        UmSrAgentMapping umSrAgentMapping=srAgentMappingRepository.findFirstByAgentIdAndStatus(agentUserInfo.getId(),"1");

        if(umSrAgentMapping==null){
            logWriterUtility.error(requestId,"Sr agent mapping not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sr agent mapping not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"Sr agent mapping found");


        UmUserInfo srUserInfo=userInfoRepository.findFirstById(umSrAgentMapping.getSrId());
        if(srUserInfo==null){
            logWriterUtility.error(requestId,"Sr user info not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sr user info not found");
            return detailsResponse;
        }

        logWriterUtility.error(requestId,"Sr user info found");

        TsClient receiverSrWallet = clientRepository.findFirstByWalletNoAndUserStatusNotIn(srUserInfo.getMobileNumber(), statusNotIn);
        if (receiverSrWallet == null) {
            logWriterUtility.error(requestId, "Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver wallet found :" + receiverSrWallet.getWalletNo());

        String receiverGlName = GlConstants.getGlNameByCustomerType(receiverSrWallet.getGroupCode());
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);

        BigDecimal senderBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());
        if (senderBalance == null) {
            logWriterUtility.error(requestId, "Sender Account balance not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender Account balance not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender Balance loaded.");

        if (senderBalance.compareTo(transactionAmount) < 0) {
            logWriterUtility.error(requestId, "Insufficient Sender Account balance");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Insufficient Sender Account balance");
            return detailsResponse;
        }

        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.Commission.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "Commission Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Commission Service not found");
            return detailsResponse;
        }

        //Add entry on transaction
        TsTransaction feeShareTransaction = null;
        try {
            feeShareTransaction = buildTransaction(senderGl, receiverSrWallet.getWalletNo(), cfeService.getServiceCode(), transactionAmountData, feeData, requestId, mainTxToken);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        feeShareTransaction.setReceiverWallet(receiverSrWallet.getWalletNo());
        feeShareTransaction.setSenderGl("" + senderGl.getGlCode());

//        feeShareTransaction.setIsSenderGl(true);
//        feeShareTransaction.setIsReceiverGl(false);


        feeShareTransaction.setNotes("Fee Shared for transaction :" + mainTxToken);
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        feeShareTransaction.setLogicalSender(senderGlCode);
        feeShareTransaction.setLogicalReceiver(receiverSrWallet.getWalletNo());

        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

//        Long transactionId, Long glCode, BigDecimal debitAmount, BigDecimal creditAmount,
//                String productCode, String requestId

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.DEBIT,
                "" + senderGl.getGlCode(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT,
                receiverSrWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, senderGl.getGlName(),
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, receiverGlName, BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal receiverClientRunningBalance = clientService.updateClientCurrentBalance(receiverSrWallet,
                receiverSrWallet.getWalletNo());

        BigDecimal senderGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());

        feeShareTransaction.setReceiverRunningBalance(receiverClientRunningBalance);
        feeShareTransaction.setSenderRunningBalance(senderGlRunningBalance);
        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverClientRunningBalance);
        detailsResponse.setSenderRunningBalance(senderGlRunningBalance);
        detailsResponse.setNewTransactionToken(feeShareTransaction.getToken());

        return detailsResponse;
    }

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRES_NEW)
    @Override
    public FeeSharingTransactionDetailsResponse feeShareGlToDirectWalletForAgentMerchant(TsFeeStakeholder feeStakeholder, String senderGlCode, String mainTxToken, BigDecimal transactionAmount, String requestId, BigDecimal totalFeeAmount) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, transactionAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWallet");
        detailsResponse.setMainTransactionToken(mainTxToken);
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(totalFeeAmount);
        detailsResponse.setSenderAccount(senderGlCode);

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        TsTransaction oldTransaction = transactionRepository.findFirstByToken(mainTxToken);

        if (oldTransaction == null) {
            logWriterUtility.error(requestId, "Invalid old transaction token :" + mainTxToken);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Invalid old transaction token :" + mainTxToken);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Main transaction found :" + oldTransaction.getId());

        TsGl senderGl = generalLedgerRepository.findFirstByGlCode(senderGlCode);

        if (senderGl == null) {
            logWriterUtility.error(requestId, "Sender gl code not found :" + senderGlCode);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender gl code not found :" + senderGlCode);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender gl found :" + senderGl.getGlCode());
        detailsResponse.setPayerName(senderGl.getGlName());
        detailsResponse.setSenderAccount("" + senderGl.getGlCode());

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());


        TsClient merchantWallet=null;
        if(oldTransaction.getTransactionType().equals(TransactionServiceCodeConstants.AGENT_CASH_IN)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(oldTransaction.getSenderWallet(), statusNotIn);
        }else if(oldTransaction.getTransactionType().equals(TransactionServiceCodeConstants.AGENT_CASH_OUT)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(oldTransaction.getReceiverWallet(), statusNotIn);
        }else{
            logWriterUtility.error(requestId,"Unsupported transaction type for fee shairing SR");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Unsupported transaction type for fee shairing SR");
            return detailsResponse;
        }

        if(merchantWallet==null){
            logWriterUtility.error(requestId,"Sender merchant wallet not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender merchant wallet not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"Sender merchant wallet found");


        String receiverGlName = GlConstants.getGlNameByCustomerType(merchantWallet.getGroupCode());
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);


        BigDecimal senderBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());
        if (senderBalance == null) {
            logWriterUtility.error(requestId, "Sender Account balance not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender Account balance not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender Balance loaded.");

        if (senderBalance.compareTo(transactionAmount) < 0) {
            logWriterUtility.error(requestId, "Insufficient Sender Account balance");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Insufficient Sender Account balance");
            return detailsResponse;
        }

        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.Commission.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "Commission Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Commission Service not found");
            return detailsResponse;
        }

        //Add entry on transaction
        TsTransaction feeShareTransaction = null;
        try {
            feeShareTransaction = buildTransaction(senderGl, merchantWallet.getWalletNo(), cfeService.getServiceCode(), transactionAmountData, feeData, requestId, mainTxToken);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        feeShareTransaction.setReceiverWallet(merchantWallet.getWalletNo());
        feeShareTransaction.setSenderGl("" + senderGl.getGlCode());

//        feeShareTransaction.setIsSenderGl(true);
//        feeShareTransaction.setIsReceiverGl(false);


        feeShareTransaction.setNotes("Fee Shared for transaction :" + mainTxToken);
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        feeShareTransaction.setLogicalSender(senderGlCode);
        feeShareTransaction.setLogicalReceiver(merchantWallet.getWalletNo());

        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

//        Long transactionId, Long glCode, BigDecimal debitAmount, BigDecimal creditAmount,
//                String productCode, String requestId

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.DEBIT,
                "" + senderGl.getGlCode(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT,
                merchantWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, senderGl.getGlName(),
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, receiverGlName, BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal receiverClientRunningBalance = clientService.updateClientCurrentBalance(merchantWallet,
                merchantWallet.getWalletNo());

        BigDecimal senderGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());

        feeShareTransaction.setReceiverRunningBalance(receiverClientRunningBalance);
        feeShareTransaction.setSenderRunningBalance(senderGlRunningBalance);
        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverClientRunningBalance);
        detailsResponse.setSenderRunningBalance(senderGlRunningBalance);
        detailsResponse.setNewTransactionToken(feeShareTransaction.getToken());

        return detailsResponse;
    }

    @Override
    public FeeSharingTransactionDetailsResponse feeShareGlToGl(TsFeeStakeholder feeStakeholder, String senderGlCode, String mainTxToken, BigDecimal transactionAmount, String requestId, BigDecimal totalFeeAmount) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, transactionAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectGl");
        detailsResponse.setMainTransactionToken(mainTxToken);
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(totalFeeAmount);
        detailsResponse.setSenderAccount(senderGlCode);

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        TsTransaction oldTransaction = transactionRepository.findFirstByToken(mainTxToken);

        if (oldTransaction == null) {
            logWriterUtility.error(requestId, "Invalid old transaction token :" + mainTxToken);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Invalid old transaction token :" + mainTxToken);
            return detailsResponse;
        }

        detailsResponse.setTransactionType(oldTransaction.getTransactionType());

        logWriterUtility.trace(requestId, "Main transaction found :" + oldTransaction.getId());

        TsGl senderGl = generalLedgerRepository.findFirstByGlCode(senderGlCode);

        if (senderGl == null) {
            logWriterUtility.error(requestId, "Sender gl code not found :" + senderGlCode);
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender gl code not found :" + senderGlCode);
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender gl found :" + senderGl.getGlCode());
        detailsResponse.setPayerName(senderGl.getGlName());
        detailsResponse.setSenderAccount("" + senderGl.getGlCode());


        TsGl receiverGl = generalLedgerRepository.findFirstByGlCode(feeStakeholder.getGlCode());

        if (receiverGl == null) {
            logWriterUtility.error(requestId, "Receiver gl code not found :" + feeStakeholder.getGlCode());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver gl code not found :" + feeStakeholder.getGlCode());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver gl found :" + receiverGl.getGlCode());
        detailsResponse.setPayeeName(receiverGl.getGlName());
        detailsResponse.setStakeholderAccount("" + receiverGl.getGlCode());


        String receiverGlName = receiverGl.getGlName();
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);


        BigDecimal senderBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());
        if (senderBalance == null) {
            logWriterUtility.error(requestId, "Sender Account balance not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender Account balance not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender Balance loaded.");

        if (senderBalance.compareTo(transactionAmount) < 0) {
            logWriterUtility.error(requestId, "Insufficient Sender Account balance");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Insufficient Sender Account balance");
            return detailsResponse;
        }

        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.Commission.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "Commission Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Commission Service not found");
            return detailsResponse;
        }


        //Add entry on transaction
        TsTransaction feeShareTransaction = null;
        try {
            feeShareTransaction = buildTransaction(senderGl, receiverGl.getGlCode(), cfeService.getServiceCode(), transactionAmountData, feeData, requestId, mainTxToken);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        feeShareTransaction.setReceiverWallet(""+receiverGl.getGlCode());
        feeShareTransaction.setSenderGl("" + senderGl.getGlCode());
        feeShareTransaction.setReceiverGl("" + receiverGl.getGlCode());
        feeShareTransaction.setSenderGl("" + senderGl.getGlCode());

        feeShareTransaction.setLogicalReceiver("" + receiverGl.getGlCode());
        feeShareTransaction.setLogicalSender("" + senderGl.getGlCode());

//        feeShareTransaction.setIsSenderGl(true);
//        feeShareTransaction.setIsReceiverGl(false);


        feeShareTransaction.setNotes("Fee Shared for transaction :" + mainTxToken);
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

//        Long transactionId, Long glCode, BigDecimal debitAmount, BigDecimal creditAmount,
//                String productCode, String requestId

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.DEBIT,
                "" + senderGl.getGlCode(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(feeShareTransaction.getId(), FeePayer.CREDIT,
                receiverGl.getGlCode(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(feeShareTransaction.getId(),
                FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, senderGl.getGlName(),
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, receiverGlName, BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(feeShareTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal receiverGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(receiverGl.getGlCode());
        BigDecimal senderGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(senderGl.getGlCode());

        feeShareTransaction.setReceiverRunningBalance(receiverGlRunningBalance);
        feeShareTransaction.setSenderRunningBalance(senderGlRunningBalance);
        transactionRepository.save(feeShareTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverGlRunningBalance);
        detailsResponse.setSenderRunningBalance(senderGlRunningBalance);
        detailsResponse.setNewTransactionToken(feeShareTransaction.getToken());

        return detailsResponse;
    }

    @Override
    public FeeSharingTransactionDetailsResponse conversionRateShareWalletToGl(TsFeeStakeholder feeStakeholder,
                                                                              TsTransaction mainTransaction,
                                                                              BigDecimal currentStakeholderAmount,
                                                                              TsFee tsFee, String requestId) {

        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, currentStakeholderAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWalletToGl");
        detailsResponse.setMainTransactionToken(mainTransaction.getToken());
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(mainTransaction.getFeeAmount());
        detailsResponse.setSenderAccount(feeStakeholder.getWalletNumber());

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        detailsResponse.setTransactionType(mainTransaction.getTransactionType());

        logWriterUtility.trace(requestId, "Main transaction found :" + mainTransaction.getId());

        TsClient senderWallet = tsClientRepository.findLastByWalletNoOrderByIdDesc(mainTransaction.getReceiverWallet());

        if (senderWallet == null) {
            logWriterUtility.error(requestId, "Sender wallet not found :" + mainTransaction.getReceiverWallet());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender wallet not found :" + mainTransaction.getReceiverWallet());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender wallet found :" + senderWallet.getWalletNo());
        detailsResponse.setPayerName(senderWallet.getFullName());
        detailsResponse.setSenderAccount(senderWallet.getWalletNo());


        String senderGlName = CommonTasks.getGlNameByCustomerType(senderWallet);
        logWriterUtility.trace(requestId, "Sender gl name :" + senderGlName);

        TsGl receiverGl = generalLedgerRepository.findFirstByGlCode(feeStakeholder.getGlCode());
        if (receiverGl == null) {
            logWriterUtility.error(requestId, "Receiver gl code not found :" + feeStakeholder.getGlCode());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver gl code not found :" + feeStakeholder.getGlCode());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver gl found :" + receiverGl.getGlCode());
        detailsResponse.setPayeeName(receiverGl.getGlName());
        detailsResponse.setStakeholderAccount("" + receiverGl.getGlCode());


        String receiverGlName = receiverGl.getGlName();
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);

        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.CurrencyConversion.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "CurrencyConversion Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("CurrencyConversion Service not found");
            return detailsResponse;
        }


        //Add entry on transaction
        TsTransaction currencyConversionTransaction = null;
        try {
            currencyConversionTransaction = buildTransaction(senderWallet, receiverGl.getGlCode(),
                    cfeService.getServiceCode(), transactionAmountData,
                    feeData, requestId, mainTransaction.getToken());
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        currencyConversionTransaction.setReceiverWallet(""+receiverGl.getGlCode());
        currencyConversionTransaction.setSenderWallet(senderWallet.getWalletNo());
        currencyConversionTransaction.setReceiverGl("" + receiverGl.getGlCode());
        currencyConversionTransaction.setSenderGl(null);

        currencyConversionTransaction.setLogicalReceiver("" + receiverGl.getGlCode());
        currencyConversionTransaction.setLogicalSender("" + senderWallet.getWalletNo());

//        feeShareTransaction.setIsSenderGl(true);
//        feeShareTransaction.setIsReceiverGl(false);


        currencyConversionTransaction.setNotes("Currency conversion for transaction :" + mainTransaction.getToken());
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.DEBIT,
                "" + senderWallet.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.CREDIT,
                receiverGl.getGlCode(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(),
                FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, senderGlName,
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.VAT_Payable,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction,
                    GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal receiverGlRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(receiverGl.getGlCode());
        BigDecimal senderWalletRunningBalance = clientService.updateClientCurrentBalance(senderWallet,requestId);

        currencyConversionTransaction.setReceiverRunningBalance(receiverGlRunningBalance);
        currencyConversionTransaction.setSenderRunningBalance(senderWalletRunningBalance);
        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverGlRunningBalance);
        detailsResponse.setSenderRunningBalance(senderWalletRunningBalance);
        detailsResponse.setNewTransactionToken(currencyConversionTransaction.getToken());

        return detailsResponse;
    }

    @Override
    public FeeSharingTransactionDetailsResponse conversionRateShareWalletToWallet(TsFeeStakeholder feeStakeholder,
                                                                                  TsTransaction mainTransaction,
                                                                                  BigDecimal currentStakeholderAmount,
                                                                                  TsFee tsFee, String requestId) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, currentStakeholderAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWalletToWallet");
        detailsResponse.setMainTransactionToken(mainTransaction.getToken());
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(mainTransaction.getFeeAmount());
        detailsResponse.setSenderAccount(feeStakeholder.getWalletNumber());

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        detailsResponse.setTransactionType(mainTransaction.getTransactionType());

        logWriterUtility.trace(requestId, "Main transaction found :" + mainTransaction.getId());

        TsClient senderWallet = tsClientRepository.findLastByWalletNoOrderByIdDesc(mainTransaction.getReceiverWallet());

        if (senderWallet == null) {
            logWriterUtility.error(requestId, "Sender wallet not found :" + mainTransaction.getReceiverWallet());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender wallet not found :" + mainTransaction.getReceiverWallet());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender wallet found :" + senderWallet.getWalletNo());
        detailsResponse.setPayerName(senderWallet.getFullName());
        detailsResponse.setSenderAccount(senderWallet.getWalletNo());


        String senderGlName = CommonTasks.getGlNameByCustomerType(senderWallet);

        logWriterUtility.trace(requestId, "Sender gl name :" + senderGlName);


        TsClient receiverWallet = tsClientRepository.findLastByWalletNoOrderByIdDesc(feeStakeholder.getWalletNumber());

        if (receiverWallet == null) {
            logWriterUtility.error(requestId, "Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver wallet found :" + receiverWallet.getWalletNo());
        detailsResponse.setPayerName(receiverWallet.getFullName());
        detailsResponse.setSenderAccount(receiverWallet.getWalletNo());


        String receiverGlName = CommonTasks.getGlNameByCustomerType(receiverWallet);
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);

        detailsResponse.setPayeeName(receiverWallet.getFullName());
        detailsResponse.setStakeholderAccount("" + receiverWallet.getWalletNo());


        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.CurrencyConversion.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "CurrencyConversion Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("CurrencyConversion Service not found");
            return detailsResponse;
        }


        //Add entry on transaction
        TsTransaction currencyConversionTransaction = null;
        try {
            currencyConversionTransaction = buildTransaction(senderWallet,receiverWallet,
                    cfeService.getServiceCode(), transactionAmountData,
                    feeData, requestId, mainTransaction.getToken());
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        currencyConversionTransaction.setReceiverWallet(""+receiverWallet.getWalletNo());
        currencyConversionTransaction.setSenderWallet(senderWallet.getWalletNo());
        currencyConversionTransaction.setReceiverGl(null);
        currencyConversionTransaction.setSenderGl(null);

        currencyConversionTransaction.setLogicalReceiver("" + receiverWallet.getWalletNo());
        currencyConversionTransaction.setLogicalSender("" + receiverWallet.getWalletNo());


        currencyConversionTransaction.setNotes("Currency conversion for transaction :" + mainTransaction.getToken());
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.DEBIT,
                "" + senderWallet.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.CREDIT,
                receiverWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(),
                FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.VAT_Payable,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction,
                    GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal senderWalletRunningBalance = clientService.updateClientCurrentBalance(senderWallet,requestId);
        BigDecimal receiverWalletRunningBalance = clientService.updateClientCurrentBalance(receiverWallet,requestId);

        currencyConversionTransaction.setReceiverRunningBalance(receiverWalletRunningBalance);
        currencyConversionTransaction.setSenderRunningBalance(senderWalletRunningBalance);
        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverWalletRunningBalance);
        detailsResponse.setSenderRunningBalance(senderWalletRunningBalance);
        detailsResponse.setNewTransactionToken(currencyConversionTransaction.getToken());

        return detailsResponse;
    }

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRES_NEW)
    @Override
    public FeeSharingTransactionDetailsResponse conversionRateShareSrWallet(TsFeeStakeholder feeStakeholder, TsTransaction mainTransaction, BigDecimal currentStakeholderAmount, TsFee tsFee, String requestId) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, currentStakeholderAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWalletToWallet");
        detailsResponse.setMainTransactionToken(mainTransaction.getToken());
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(mainTransaction.getFeeAmount());
        detailsResponse.setSenderAccount(feeStakeholder.getWalletNumber());

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        detailsResponse.setTransactionType(mainTransaction.getTransactionType());

        logWriterUtility.trace(requestId, "Main transaction found :" + mainTransaction.getId());

        TsClient senderWallet = tsClientRepository.findLastByWalletNoOrderByIdDesc(mainTransaction.getReceiverWallet());

        if (senderWallet == null) {
            logWriterUtility.error(requestId, "Sender wallet not found :" + mainTransaction.getReceiverWallet());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender wallet not found :" + mainTransaction.getReceiverWallet());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender wallet found :" + senderWallet.getWalletNo());
        detailsResponse.setPayerName(senderWallet.getFullName());
        detailsResponse.setSenderAccount(senderWallet.getWalletNo());


        String senderGlName = CommonTasks.getGlNameByCustomerType(senderWallet);

        logWriterUtility.trace(requestId, "Sender gl name :" + senderGlName);


        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient merchantWallet=null;
        if(mainTransaction.getTransactionType().equals(TransactionServiceCodeConstants.AGENT_CASH_IN)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(mainTransaction.getSenderWallet(), statusNotIn);
        }else if(mainTransaction.getTransactionType().equals(TransactionServiceCodeConstants.TOP_UP)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(mainTransaction.getSenderWallet(), statusNotIn);
        }
        else if(mainTransaction.getTransactionType().equals(TransactionServiceCodeConstants.AGENT_CASH_OUT)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(mainTransaction.getReceiverWallet(), statusNotIn);
        }
        else if(mainTransaction.getTransactionType().equals(TransactionServiceCodeConstants.BILL_PAY)){
            merchantWallet=clientRepository.findFirstByWalletNoAndUserStatusNotIn(mainTransaction.getSenderWallet(), statusNotIn);
        }else{
            logWriterUtility.error(requestId,"Unsupported transaction type for fee shairing SR");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Unsupported transaction type for fee shairing SR");
            return detailsResponse;
        }

        if(merchantWallet==null){
            logWriterUtility.error(requestId,"Sender merchant wallet not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender merchant wallet not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"Sender merchant wallet found");

        UmUserInfo agentUserInfo=userInfoRepository.findByLoginId(merchantWallet.getWalletNo());
        if(agentUserInfo==null){
            logWriterUtility.error(requestId,"Agent user info not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Agent user info not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"merchant user info found");

        UmSrAgentMapping umSrAgentMapping=srAgentMappingRepository.findFirstByAgentIdAndStatus(agentUserInfo.getId(),"1");

        if(umSrAgentMapping==null){
            logWriterUtility.error(requestId,"Sr agent mapping not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sr agent mapping not found");
            return detailsResponse;
        }

        logWriterUtility.trace(requestId,"Sr agent mapping found");


        UmUserInfo srUserInfo=userInfoRepository.findFirstById(umSrAgentMapping.getSrId());
        if(srUserInfo==null){
            logWriterUtility.error(requestId,"Sr user info not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sr user info not found");
            return detailsResponse;
        }

        logWriterUtility.error(requestId,"Sr user info found");

        TsClient receiverSrWallet = clientRepository.findFirstByWalletNoAndUserStatusNotIn(srUserInfo.getMobileNumber(), statusNotIn);
        if (receiverSrWallet == null) {
            logWriterUtility.error(requestId, "Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver wallet found :" + receiverSrWallet.getWalletNo());

        String receiverGlName = GlConstants.getGlNameByCustomerType(receiverSrWallet.getGroupCode());
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);

        detailsResponse.setPayeeName(receiverSrWallet.getFullName());
        detailsResponse.setStakeholderAccount("" + receiverSrWallet.getWalletNo());


        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.CurrencyConversion.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "CurrencyConversion Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("CurrencyConversion Service not found");
            return detailsResponse;
        }


        //Add entry on transaction
        TsTransaction currencyConversionTransaction = null;
        try {
            currencyConversionTransaction = buildTransaction(senderWallet,receiverSrWallet,
                    cfeService.getServiceCode(), transactionAmountData,
                    feeData, requestId, mainTransaction.getToken());
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        currencyConversionTransaction.setReceiverWallet(""+receiverSrWallet.getWalletNo());
        currencyConversionTransaction.setSenderWallet(senderWallet.getWalletNo());
        currencyConversionTransaction.setReceiverGl(null);
        currencyConversionTransaction.setSenderGl(null);

        currencyConversionTransaction.setLogicalReceiver("" + receiverSrWallet.getWalletNo());
        currencyConversionTransaction.setLogicalSender("" + receiverSrWallet.getWalletNo());


        currencyConversionTransaction.setNotes("Currency conversion for transaction :" + mainTransaction.getToken());
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.DEBIT,
                "" + senderWallet.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.CREDIT,
                receiverSrWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(),
                FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.VAT_Payable,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction,
                    GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal senderWalletRunningBalance = clientService.updateClientCurrentBalance(senderWallet,requestId);
        BigDecimal receiverWalletRunningBalance = clientService.updateClientCurrentBalance(receiverSrWallet,requestId);

        currencyConversionTransaction.setReceiverRunningBalance(receiverWalletRunningBalance);
        currencyConversionTransaction.setSenderRunningBalance(senderWalletRunningBalance);
        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverWalletRunningBalance);
        detailsResponse.setSenderRunningBalance(senderWalletRunningBalance);
        detailsResponse.setNewTransactionToken(currencyConversionTransaction.getToken());

        return detailsResponse;
    }

    @Transactional(rollbackFor = {Exception.class}, propagation = Propagation.REQUIRES_NEW)
    @Override
    public FeeSharingTransactionDetailsResponse conversionRateShareTopUpAgent(TsFeeStakeholder feeStakeholder, TsTransaction mainTransaction, BigDecimal currentStakeholderAmount, TsFee tsFee, String requestId) {
        FeeData feeData = new FeeData();
        feeData.setFeePayer(FeePayer.CREDIT);
        feeData.setFeeAmount(BigDecimal.ZERO);


        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, currentStakeholderAmount);


        FeeSharingTransactionDetailsResponse detailsResponse = new FeeSharingTransactionDetailsResponse();
        detailsResponse.setSuccess(true);
        detailsResponse.setFeeShareMethod("DirectWalletToWallet");
        detailsResponse.setMainTransactionToken(mainTransaction.getToken());
        detailsResponse.setStakeholderAccount(feeStakeholder.getWalletNumber());
        detailsResponse.setPayeeName(feeStakeholder.getStakeholderName());
        detailsResponse.setStakeholderFeeAmount(transactionAmountData.getReceiverCreditAmount());
        detailsResponse.setTotalFeeAmount(mainTransaction.getFeeAmount());
        detailsResponse.setSenderAccount(feeStakeholder.getWalletNumber());

        if (feeStakeholder == null) {
            logWriterUtility.error(requestId, "Stakeholder can't be null");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Stakeholder can't be null");
            return detailsResponse;
        }

        detailsResponse.setTransactionType(mainTransaction.getTransactionType());

        logWriterUtility.trace(requestId, "Main transaction found :" + mainTransaction.getId());

        TsClient senderWallet = tsClientRepository.findLastByWalletNoOrderByIdDesc(mainTransaction.getReceiverWallet());

        if (senderWallet == null) {
            logWriterUtility.error(requestId, "Sender wallet not found :" + mainTransaction.getReceiverWallet());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Sender wallet not found :" + mainTransaction.getReceiverWallet());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Sender wallet found :" + senderWallet.getWalletNo());
        detailsResponse.setPayerName(senderWallet.getFullName());
        detailsResponse.setSenderAccount(senderWallet.getWalletNo());


        String senderGlName = CommonTasks.getGlNameByCustomerType(senderWallet);

        logWriterUtility.trace(requestId, "Sender gl name :" + senderGlName);


        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient receiverWallet = clientRepository.findFirstByWalletNoAndUserStatusNotIn(mainTransaction.getSenderWallet(), statusNotIn);

        if (receiverWallet == null) {
            logWriterUtility.error(requestId, "Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("Receiver wallet not found :" + feeStakeholder.getWalletNumber());
            return detailsResponse;
        }

        logWriterUtility.trace(requestId, "Receiver wallet found :" + receiverWallet.getWalletNo());
        detailsResponse.setPayerName(receiverWallet.getFullName());
        detailsResponse.setSenderAccount(receiverWallet.getWalletNo());


        String receiverGlName = CommonTasks.getGlNameByCustomerType(receiverWallet);
        logWriterUtility.trace(requestId, "Receiver gl name :" + receiverGlName);

        detailsResponse.setPayeeName(receiverWallet.getFullName());
        detailsResponse.setStakeholderAccount("" + receiverWallet.getWalletNo());


        TsService cfeService = serviceRepository.findFirstByServiceName(TsEnums.Services.CurrencyConversion.name());
        if (cfeService == null) {
            logWriterUtility.error(requestId, "CurrencyConversion Service not found");
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason("CurrencyConversion Service not found");
            return detailsResponse;
        }


        //Add entry on transaction
        TsTransaction currencyConversionTransaction = null;
        try {
            currencyConversionTransaction = buildTransaction(senderWallet,receiverWallet,
                    cfeService.getServiceCode(), transactionAmountData,
                    feeData, requestId, mainTransaction.getToken());
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }


//        feeShareTransaction.setPayee(receiverWallet.getId());
        currencyConversionTransaction.setReceiverWallet(""+receiverWallet.getWalletNo());
        currencyConversionTransaction.setSenderWallet(senderWallet.getWalletNo());
        currencyConversionTransaction.setReceiverGl(null);
        currencyConversionTransaction.setSenderGl(null);

        currencyConversionTransaction.setLogicalReceiver("" + receiverWallet.getWalletNo());
        currencyConversionTransaction.setLogicalSender("" + receiverWallet.getWalletNo());


        currencyConversionTransaction.setNotes("Currency conversion for transaction :" + mainTransaction.getToken());
//        feeShareTransaction.setRemarks("Fee Shared for transaction :" + mainTxToken);

        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Transaction Save done");

        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();
        TsTransactionDetails senderDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.DEBIT,
                "" + senderWallet.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderDetails);

        TsTransactionDetails receiverDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(), FeePayer.CREDIT,
                receiverWallet.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverDetails);

        TsTransactionDetails feeDetails = buildTransactionDetailItem(currencyConversionTransaction.getId(),
                FeePayer.CREDIT, "FEE",
                transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeDetails);

        transactionDetailRepository.saveAll(transactionDetailList);

        logWriterUtility.trace(requestId, "Transaction details Save done");

        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGlTxDetails = null;
        try {
            debitGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        debitGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(debitGlTxDetails);

        TsGlTransactionDetails creditGlTxDetails = null;
        try {
            creditGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, receiverGlName,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getReceiverCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        creditGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(creditGlTxDetails);

        TsGlTransactionDetails vatPayableGlTxDetails = null;
        try {
            vatPayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.VAT_Payable,
                    BigDecimal.valueOf(0),
                    transactionAmountData.getVatCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        vatPayableGlTxDetails.setProcess("FEE_SHARED");

        glTransactionDetailsList.add(vatPayableGlTxDetails);

        TsGlTransactionDetails feePayableGlTxDetails = null;
        try {
            feePayableGlTxDetails = buildGlTransactionDetailsItem(currencyConversionTransaction,
                    GlConstants.Fee_payable_for_FundMovement,
                    BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        feePayableGlTxDetails.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(feePayableGlTxDetails);

        TsGlTransactionDetails aitPayableGl = null;
        try {
            aitPayableGl = buildGlTransactionDetailsItem(currencyConversionTransaction, GlConstants.AIT_Payable,
                    BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), requestId);
        } catch (PocketException e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            detailsResponse.setSuccess(false);
            detailsResponse.setFailureReason(e.getMessage());
            return detailsResponse;
        }
//        aitPayableGl.setProcess("FEE_SHARED");
        glTransactionDetailsList.add(aitPayableGl);

        glTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        logWriterUtility.trace(requestId, "GlTransactionDetails Save done");

        BigDecimal senderWalletRunningBalance = clientService.updateClientCurrentBalance(senderWallet,requestId);
        BigDecimal receiverWalletRunningBalance = clientService.updateClientCurrentBalance(receiverWallet,requestId);

        currencyConversionTransaction.setReceiverRunningBalance(receiverWalletRunningBalance);
        currencyConversionTransaction.setSenderRunningBalance(senderWalletRunningBalance);
        transactionRepository.save(currencyConversionTransaction);
        logWriterUtility.trace(requestId, "Sender balance update successful");

        detailsResponse.setSuccess(true);
        detailsResponse.setReceiverRunningBalance(receiverWalletRunningBalance);
        detailsResponse.setSenderRunningBalance(senderWalletRunningBalance);
        detailsResponse.setNewTransactionToken(currencyConversionTransaction.getToken());

        return detailsResponse;
    }

    private TsTransactionDetails buildTransactionDetailItem(long transactionId, String transactionType,
                                                            String reference, BigDecimal transactionAmount) {

        TsTransactionDetails debitDetail = new TsTransactionDetails();
        debitDetail.setTransactionId((int) transactionId);
        debitDetail.setTransactionType(transactionType);
        debitDetail.setWalletReference(reference);
        debitDetail.setTransactionAmount(transactionAmount);
        return debitDetail;
    }


    private TsTransaction buildTransaction(TsGl senderGl, String receiverAccount, String serviceCode,
                                            TransactionAmountData transactionAmountData,
                                            FeeData feeData, String requestId, String oldTransactionToken) throws PocketException {
        TsTransaction transaction = new TsTransaction();
        transaction.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());

        transaction.setTransactionAmount(transactionAmountData.getOriginalTransactionAmount());
        transaction.setSenderDebitAmount(transactionAmountData.getSenderDebitAmount());
        transaction.setReceiverCreditAmount(transactionAmountData.getReceiverCreditAmount());

        transaction.setSenderGl("" + senderGl.getGlCode());
        transaction.setReceiverGl(null);

        transaction.setCreatedDate(new java.util.Date());
        transaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        transaction.setRefTransactionToken(oldTransactionToken);
        transaction.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
        transaction.setSenderWallet("" + senderGl.getGlCode());
        transaction.setReceiverWallet(receiverAccount);

        transaction.setTransactionType(serviceCode);

        transaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus()); // transactionStatus ==1 Captured
        transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());

        transaction.setFeeAmount(transactionAmountData.getTotalFeeAmount());
        transaction.setFeeCode(feeData.getFeeCode());
        transaction.setFeePayer(feeData.getFeePayer());
        transaction.setRequestId(requestId);
        transaction.setIsDisputable(false);

        return transaction;
    }

    private TsTransaction buildTransaction(TsClient senderClient, String receiverAccount, String serviceCode,
                                            TransactionAmountData transactionAmountData,
                                            FeeData feeData, String requestId, String oldTransactionToken) throws PocketException {
        TsTransaction transaction = new TsTransaction();
        transaction.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());

        transaction.setTransactionAmount(transactionAmountData.getOriginalTransactionAmount());
        transaction.setSenderDebitAmount(transactionAmountData.getSenderDebitAmount());
        transaction.setReceiverCreditAmount(transactionAmountData.getReceiverCreditAmount());

        transaction.setSenderGl(null);
        transaction.setReceiverGl(null);

        transaction.setCreatedDate(new java.util.Date());
        transaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        transaction.setRefTransactionToken(oldTransactionToken);
        transaction.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
        transaction.setSenderWallet("" + senderClient.getWalletNo());
        transaction.setReceiverWallet(receiverAccount);

        transaction.setTransactionType(serviceCode);

        transaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus()); // transactionStatus ==1 Captured
        transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());

        transaction.setFeeAmount(transactionAmountData.getTotalFeeAmount());
        transaction.setFeeCode(feeData.getFeeCode());
        transaction.setFeePayer(feeData.getFeePayer());
        transaction.setRequestId(requestId);
        transaction.setIsDisputable(false);

        return transaction;
    }

    private TsTransaction buildTransaction(TsClient senderClient, TsClient receiverClient, String serviceCode,
                                            TransactionAmountData transactionAmountData,
                                            FeeData feeData, String requestId, String oldTransactionToken) throws PocketException {
        TsTransaction transaction = new TsTransaction();
        transaction.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());

        transaction.setTransactionAmount(transactionAmountData.getOriginalTransactionAmount());
        transaction.setSenderDebitAmount(transactionAmountData.getSenderDebitAmount());
        transaction.setReceiverCreditAmount(transactionAmountData.getReceiverCreditAmount());

        transaction.setSenderGl(null);
        transaction.setReceiverGl(null);

        transaction.setCreatedDate(new java.util.Date());
        transaction.setToken(RandomGenerator.getInstance().generateTransactionTokenAsString());
        transaction.setRefTransactionToken(oldTransactionToken);
        transaction.setCreatedDate(new java.sql.Date(System.currentTimeMillis()));
        transaction.setSenderWallet("" + senderClient.getWalletNo());
        transaction.setReceiverWallet(receiverClient.getWalletNo());

        transaction.setTransactionType(serviceCode);

        transaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus()); // transactionStatus ==1 Captured
        transaction.setEodStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());

        transaction.setFeeAmount(transactionAmountData.getTotalFeeAmount());
        transaction.setFeeCode(feeData.getFeeCode());
        transaction.setFeePayer(feeData.getFeePayer());
        transaction.setRequestId(requestId);
        transaction.setIsDisputable(false);

        return transaction;
    }

    private TsTransactionDetails buildCfeTransactionDetailsItem(long transactionId, String transactionType, String walletRef,
                                                                BigDecimal transactionAmount) {

        TsTransactionDetails cfeTransactionDetail = new TsTransactionDetails();
        cfeTransactionDetail.setTransactionId((int) transactionId);
        cfeTransactionDetail.setTransactionType(transactionType);
        cfeTransactionDetail.setWalletReference(walletRef);
        cfeTransactionDetail.setTransactionAmount(transactionAmount);
        return cfeTransactionDetail;
    }

    private TsGlTransactionDetails buildGlTransactionDetailsItem(TsTransaction transaction, String glName, BigDecimal debitAmount, BigDecimal creditAmount,
                                                                 String requestId) throws PocketException {

        TsGl depositClientGl = generalLedgerRepository.findFirstByGlName(glName);

        if (depositClientGl == null) {
            logWriterUtility.error(requestId, glName + " GL not found");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }

        TsGlTransactionDetails debitGlTransactionDetails = new TsGlTransactionDetails();
        debitGlTransactionDetails.setGlCode(depositClientGl.getGlCode());
        debitGlTransactionDetails.setDebitCard(debitAmount);
        debitGlTransactionDetails.setCreditAmount(creditAmount);
        debitGlTransactionDetails.setTsTransactionByTransactionId(transaction);
        return debitGlTransactionDetails;
    }
}
