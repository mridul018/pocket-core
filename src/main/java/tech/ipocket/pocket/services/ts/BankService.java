package tech.ipocket.pocket.services.ts;

import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.bank.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface BankService {
    BaseResponseObject createBank(CreateBankRequest request) throws PocketException;

    BaseResponseObject getBanks(EmptyRequest request);
    BaseResponseObject updateBank(CreateBankRequest request) throws PocketException;

    BaseResponseObject createBranch(CreateBranchRequest request) throws PocketException;

    BaseResponseObject getBranchList(GetBranchByBankCodeRequest request);

    BaseResponseObject updateBranch(UpdateBranchRequest request) throws PocketException;
}
