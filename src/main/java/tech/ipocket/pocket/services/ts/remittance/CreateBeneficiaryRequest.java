package tech.ipocket.pocket.services.ts.remittance;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateBeneficiaryRequest extends BaseRequestObject {
    private String givenName;
    private String familyName;

    private String address;
    private String relationship;
    private String contactNumber;
    private String identificationNumber;
    private String type;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(givenName==null){
            baseResponseObject.setErrorCode(PocketErrorCode.GivenNameRequired);
            return;
        }

        if(contactNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ContactNumberRequired);
            return;
        }

        if(identificationNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.IdentificationNumberRequired);
            return;
        }

        if(type==null){
            baseResponseObject.setErrorCode(PocketErrorCode.BeneficiaryTypeRequired);
            return;
        }

    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
