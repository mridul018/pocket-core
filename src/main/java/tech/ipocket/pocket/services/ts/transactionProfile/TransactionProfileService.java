package tech.ipocket.pocket.services.ts.transactionProfile;

import tech.ipocket.pocket.request.ts.transactionProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface TransactionProfileService {
    BaseResponseObject createLimit(CreateLimitRequest request) throws PocketException;

    BaseResponseObject getLimit(GetLimitRequest request) throws PocketException;

    BaseResponseObject createTransactionProfile(CreateTransactionProfileRequest request) throws PocketException;

    BaseResponseObject getTransactionProfile(GetTransactionProfileRequest request);

    BaseResponseObject mapTransactionProfileServiceLimit(CreateTransactionProfileLimitServiceMapRequest request) throws PocketException;

    BaseResponseObject getServiceLimitByTxProfile(GetServiceLimitByTxProfileRequest request) throws PocketException;

    BaseResponseObject updateTransactionProfile(CreateTransactionProfileRequest request) throws PocketException;

    BaseResponseObject updateLimit(CreateLimitRequest request) throws PocketException;

    BaseResponseObject getTransactionLimitByTxProfileCode(GetLimitRequest request) throws PocketException;
}
