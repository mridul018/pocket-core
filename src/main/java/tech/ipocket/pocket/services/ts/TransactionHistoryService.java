package tech.ipocket.pocket.services.ts;

import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionDetailsRequest;
import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionHistoryRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface TransactionHistoryService {
    BaseResponseObject getTransactionHistory(GetTransactionHistoryRequest request) throws PocketException;

    BaseResponseObject getTransactionDetailsByToken(GetTransactionDetailsRequest request) throws PocketException;
}
