package tech.ipocket.pocket.services.ts.irregular_transaction;

import tech.ipocket.pocket.request.ts.irregular_transaction.RefundRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface IrregularTransactionService {
    BaseResponseObject createRefund(RefundRequest refundRequest) throws PocketException;

    BaseResponseObject createReversal(ReversalRequest reversalRequest) throws PocketException;
}
