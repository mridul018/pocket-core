package tech.ipocket.pocket.services.ts.transactionProfile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsLimit;
import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.entity.TsTransactionProfile;
import tech.ipocket.pocket.entity.TsTransactionProfileLimitMapping;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.transactionProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.transactionProfile.TransactionProfileServiceLimitMapItem;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TransactionProfileServiceImpl implements TransactionProfileService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsLimitRepository tsLimitRepository;

    @Autowired
    private TsTransactionProfileRepository tsTransactionProfileRepository;

    @Autowired
    private TsTransactionProfileLimitMappingRepository tsTransactionProfileLimitMappingRepository;
    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsClientRepository tsClientRepository;

    @Override
    public BaseResponseObject createLimit(CreateLimitRequest request) throws PocketException {

        if(request.getId()>0){
            return updateLimit(request);
        }

        TsLimit tsLimit = tsLimitRepository.findFirstByLimitCodeAndStatusOrderByIdDesc(request.getLimitCode(),"1");
        if (tsLimit != null) {
            logWriterUtility.error(request.getRequestId(), "Limit code already exists");
            throw new PocketException(PocketErrorCode.LimitCodeAlreadyExists);
        }

        tsLimit = tsLimitRepository.findFirstByLimitNameAndStatusOrderByIdDesc(request.getLimitName(),"1");
        if (tsLimit != null) {
            logWriterUtility.error(request.getRequestId(), "Limit name already exists");
            throw new PocketException(PocketErrorCode.LimitNameAlreadyExists);
        }

        tsLimit = new TsLimit();
        tsLimit.setLimitName(request.getLimitName());
        tsLimit.setLimitCode(request.getLimitCode());
        tsLimit.setMinAmount(request.getMinAmount());
        tsLimit.setMaxAmount(request.getMaxAmount());

        tsLimit.setDailyMaxNoOfTransaction(request.getDailyMaxNoOfTransaction());
        tsLimit.setDailyMaxTransactionAmount(request.getDailyMaxTransactionAmount());

        tsLimit.setMontlyMaxNoOfTransaction(request.getMontlyMaxNoOfTransaction());
        tsLimit.setMonthlyMaxTransactionAmount(request.getMonthlyMaxTransactionAmount());

        tsLimit.setStatus("1");

        tsLimitRepository.save(tsLimit);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateLimit(CreateLimitRequest request) throws PocketException {
        TsLimit tsLimit = tsLimitRepository.findFirstById(request.getId());
        if (tsLimit == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid limit ID :"+request.getId());
            throw new PocketException(PocketErrorCode.InvalidLimitId);
        }

        if(request.getStatus()!=null&&!tsLimit.getStatus().equalsIgnoreCase(request.getStatus())){

            // check is it deletion
            if(request.getStatus().equalsIgnoreCase("0")){
                // check is it mapped with profile

                Integer countLimitMap=tsTransactionProfileLimitMappingRepository.countAllByLimitCode(tsLimit.getLimitCode());
                if(countLimitMap!=null&&countLimitMap>0){
                    logWriterUtility.error(request.getRequestId(),"Limit is mapped with transaction failed :"+tsLimit.getLimitCode());
                    throw new PocketException(PocketErrorCode.LimitIsMappedWithTransactionProfile);
                }
            }
            tsLimit.setStatus(request.getStatus());
        }

        if(request.getLimitCode()!=null&&!tsLimit.getLimitCode().equalsIgnoreCase(request.getLimitCode())){
            logWriterUtility.error(request.getRequestId(),"Limit code edit not possible");
            throw new PocketException(PocketErrorCode.LimitCodeEditNotSupported);
        }

        tsLimit.setLimitName(request.getLimitName());
        tsLimit.setMinAmount(request.getMinAmount());
        tsLimit.setMaxAmount(request.getMaxAmount());

        tsLimit.setDailyMaxNoOfTransaction(request.getDailyMaxNoOfTransaction());
        tsLimit.setDailyMaxTransactionAmount(request.getDailyMaxTransactionAmount());

        tsLimit.setMontlyMaxNoOfTransaction(request.getMontlyMaxNoOfTransaction());
        tsLimit.setMonthlyMaxTransactionAmount(request.getMonthlyMaxTransactionAmount());

        tsLimitRepository.save(tsLimit);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }


    @Override
    public BaseResponseObject getLimit(GetLimitRequest request) throws PocketException {

        List<TsLimit> limits=null;
        if(request.getLimitCode()!=null){

            List<String> codes=new ArrayList<>();
            codes.add(request.getLimitCode());

            limits = tsLimitRepository.findAllByLimitCodeInAndStatusOrderByIdDesc(codes,"1");
        }else{
            limits = (List<TsLimit>) tsLimitRepository.findAllByStatusOrderByIdDesc("1");
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(limits);

        return baseResponseObject;
    }


    @Override
    public BaseResponseObject getTransactionLimitByTxProfileCode(GetLimitRequest request) throws PocketException {

        TsTransactionProfile tsTransactionProfile=tsTransactionProfileRepository.findFirstByProfileCodeAndStatusOrderByIdDesc(request.getTransactionProfileCode(),"1");

        if(tsTransactionProfile==null){
            logWriterUtility.error(request.getRequestId(),"Transaction profile doesn't found:"+request.getTransactionProfileCode());
            throw new PocketException(PocketErrorCode.TransactionProfileCodeDoesNotExists);
        }

        List<TsLimit> limits=new ArrayList<>();

        List<TsTransactionProfileLimitMapping> tsTransactionProfileLimitMappings=tsTransactionProfileLimitMappingRepository.findAllByTxProfileCodeOrderByIdDesc(request.getTransactionProfileCode());


        if(tsTransactionProfileLimitMappings!=null&&tsTransactionProfileLimitMappings.size()>0){

            List<String> limitCodes=new ArrayList<>();

            for (TsTransactionProfileLimitMapping mapping:tsTransactionProfileLimitMappings) {
                limitCodes.add(mapping.getLimitCode());
            }

            limits=tsLimitRepository.findAllByLimitCodeInAndStatusOrderByIdDesc(limitCodes,"1");
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(limits);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject createTransactionProfile(CreateTransactionProfileRequest request) throws PocketException {

        if(request.getId()>0){
            return updateTransactionProfile(request);
        }

        TsTransactionProfile tsTransactionProfile = tsTransactionProfileRepository.
                findFirstByProfileCodeAndStatusOrderByIdDesc(request.getProfileCode(),"1");

        if (tsTransactionProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile code exis");
            throw new PocketException(PocketErrorCode.TransactionProfileCodeExists);
        }

        tsTransactionProfile = tsTransactionProfileRepository.
                findFirstByProfileNameAndStatusOrderByIdDesc(request.getProfileName(),"1");

        if (tsTransactionProfile != null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile name exis");
            throw new PocketException(PocketErrorCode.TransactionProfileNameExists);
        }

        tsTransactionProfile = new TsTransactionProfile();
        tsTransactionProfile.setProfileCode(request.getProfileCode());
        tsTransactionProfile.setProfileName(request.getProfileName());
        tsTransactionProfile.setStatus("1");
        tsTransactionProfile.setCreatedDate(new Date());

        tsTransactionProfileRepository.save(tsTransactionProfile);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateTransactionProfile(CreateTransactionProfileRequest request) throws PocketException {
        TsTransactionProfile tsTransactionProfile = tsTransactionProfileRepository.
                findFirstById(request.getId());

        if (tsTransactionProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid Transaction profile ID :"+request.getId());
            throw new PocketException(PocketErrorCode.InvalidTxProfileId);
        }

        if(request.getProfileName()!=null||!tsTransactionProfile.getProfileName().equalsIgnoreCase(request.getProfileName())){
            tsTransactionProfile.setProfileName(request.getProfileName());
        }

        if(request.getProfileCode()!=null||!tsTransactionProfile.getProfileCode().equalsIgnoreCase(request.getProfileCode())){
            tsTransactionProfile.setProfileCode(request.getProfileCode());
        }

        if(request.getStatus()!=null&&!request.getStatus().equalsIgnoreCase(tsTransactionProfile.getStatus())){

            // check profile is assigned to user
            Integer userCount=tsClientRepository.countAllByTransactionProfileCode(tsTransactionProfile.getProfileCode());
            if(userCount!=null&& userCount>0){
                logWriterUtility.error(request.getRequestId(),"Transaction profile is assigned to user :"+tsTransactionProfile.getProfileCode());
                throw new PocketException(PocketErrorCode.TransactionProfileIsAssignedToUser);
            }

            tsTransactionProfile.setStatus(request.getStatus());
        }

        tsTransactionProfileRepository.save(tsTransactionProfile);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getTransactionProfile(GetTransactionProfileRequest request) {
        List<TsTransactionProfile> transactionProfiles = (List<TsTransactionProfile>) tsTransactionProfileRepository.findAllByStatus("1");

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(transactionProfiles);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject mapTransactionProfileServiceLimit(CreateTransactionProfileLimitServiceMapRequest request) throws PocketException {

        TsTransactionProfile tsTransactionProfile = tsTransactionProfileRepository.
                findFirstByProfileCodeAndStatusOrderByIdDesc(request.getTxProfileCode(),"1");

        if (tsTransactionProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile code invalid");
            throw new PocketException(PocketErrorCode.InvalidTxProfileCode);
        }


        TsLimit tsLimit = tsLimitRepository.
                findFirstByLimitCodeAndStatusOrderByIdDesc(request.getLimitCode(),"1");

        if (tsLimit == null) {
            logWriterUtility.error(request.getRequestId(), "Limit code invalid");
            throw new PocketException(PocketErrorCode.InvalidLimitCode);
        }


        TsService tsService = tsServiceRepository.
                findFirstByServiceCode(request.getServiceCode());

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service code invalid");
            throw new PocketException(PocketErrorCode.InvalidServiceCode);
        }

        TsTransactionProfileLimitMapping tsTransactionProfileLimitMapping =
                tsTransactionProfileLimitMappingRepository.findFirstByTxProfileCodeAndServiceCodeAndLimitCode(request.getTxProfileCode(), request.getServiceCode(), request.getLimitCode());

        if (tsTransactionProfileLimitMapping != null) {
            logWriterUtility.error(request.getRequestId(), "Transaction profile,limit and service map already exists");
            throw new PocketException(PocketErrorCode.TxProfileServiceAndLimitMapExists);
        }

        tsTransactionProfileLimitMapping = new TsTransactionProfileLimitMapping();
        tsTransactionProfileLimitMapping.setLimitCode(request.getLimitCode());
        tsTransactionProfileLimitMapping.setServiceCode(request.getServiceCode());
        tsTransactionProfileLimitMapping.setTxProfileCode(request.getTxProfileCode());
        tsTransactionProfileLimitMapping.setCreatedDate(new Date());

        tsTransactionProfileLimitMappingRepository.save(tsTransactionProfileLimitMapping);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getServiceLimitByTxProfile(GetServiceLimitByTxProfileRequest request) throws PocketException {

        // check tx profile code

        TsTransactionProfile transactionProfile = tsTransactionProfileRepository.findFirstByProfileCodeAndStatusOrderByIdDesc(request.getTxProfileCode(),"1");
        if (transactionProfile == null) {
            logWriterUtility.error(request.getRequestId(), "Invalid Tx profile code :" + request.getTxProfileCode());
            throw new PocketException(PocketErrorCode.InvalidTxProfileCode);
        }

        List<TsTransactionProfileLimitMapping> tsTransactionProfileLimitMapping = tsTransactionProfileLimitMappingRepository
                .findAllByTxProfileCodeOrderByIdDesc(request.getTxProfileCode());

        List<TransactionProfileServiceLimitMapItem> mapItems = new ArrayList<>();

        if (tsTransactionProfileLimitMapping != null && tsTransactionProfileLimitMapping.size() > 0) {

            for (TsTransactionProfileLimitMapping map : tsTransactionProfileLimitMapping) {
                TransactionProfileServiceLimitMapItem temp = new TransactionProfileServiceLimitMapItem();
                temp.setLimitCode(map.getLimitCode());

                TsLimit tsLimit = tsLimitRepository.findFirstByLimitCodeAndStatusOrderByIdDesc(map.getLimitCode(),"1");
                if (tsLimit != null) {
                    temp.setLimitName(tsLimit.getLimitName());
                }

                TsService tsService = tsServiceRepository.findFirstByServiceCode(map.getServiceCode());
                temp.setServiceCode(map.getServiceCode());
                if (tsService != null) {
                    temp.setServiceName(tsService.getServiceName());
                }
                mapItems.add(temp);
            }
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(mapItems);

        return baseResponseObject;
    }
}
