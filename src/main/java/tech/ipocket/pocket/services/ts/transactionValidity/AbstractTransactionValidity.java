package tech.ipocket.pocket.services.ts.transactionValidity;

import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsMerchantDetails;
import tech.ipocket.pocket.entity.TsService;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.ts.TsMerchantDetailsRepository;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.response.ts.transactionValidity.TransactionValidityResponse;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.TopUpMedium;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTransactionValidity {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(AbstractTransactionValidity.class);

    private TsClientRepository clientRepository;
    private TsServiceRepository serviceRepository;
    private FeeManager feeManager;
    private TransactionValidator validator;
    private TsMerchantDetailsRepository merchantDetailsRepository;

    private TsClient senderCfeClient, receiverCfeClient;
    private TsService cfeService;

    public void setDependency(TsClientRepository clientRepository,
                              TsServiceRepository serviceRepository,
                              FeeManager feeManager, TransactionValidator validator,
                              TsMerchantDetailsRepository merchantDetailsRepository) {
        this.clientRepository = clientRepository;
        this.serviceRepository = serviceRepository;
        this.feeManager = feeManager;
        this.validator = validator;
        this.merchantDetailsRepository = merchantDetailsRepository;
    }

    public abstract TransactionValidityResponse getTransactionValidity(TransactionValidityRequest transactionValidityRequest) throws PocketException;

    Boolean validateRequestAccountsForFundTransfer(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        if(request.getType().equalsIgnoreCase(ServiceCodeConstants.BillPay)){
            TsMerchantDetails tsMerchantDetails=merchantDetailsRepository.findFirstByShortCode(PocketConstants.PALLI_BIDYUT);

            if(tsMerchantDetails==null){
                logWriterUtility.error(request.getRequestId(),"PALLI_BIDYUT Merchant not found");
                throw new PocketException(PocketErrorCode.PallyBidyutMerchantNotFound);
            }

            receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsMerchantDetails.getWalletNo(), clientStatus);
            if (receiverCfeClient == null) {
                logWriterUtility.error(request.getRequestId(), "Receiver client not found");
                throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
            }
        }else{
            receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), clientStatus);
            if (receiverCfeClient == null) {
                logWriterUtility.error(request.getRequestId(), "Receiver client not found");
                throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
            }
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        if(senderCfeClient.getWalletNo().equals(receiverCfeClient.getWalletNo())){
            logWriterUtility.error(request.getRequestId(),
                    "Sender and receiver must be different");
            throw new PocketException(PocketErrorCode.SenderReceiverMustDifferent);
        }

        return true;

    }

    Boolean validateRequestAccountsForCashIn(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        boolean isSupportedSender=(senderCfeClient.getGroupCode().equals(TsEnums.UserType.AGENT.getUserTypeString()))||(senderCfeClient.getGroupCode().equals(TsEnums.UserType.MERCHANT.getUserTypeString()));

        if(!isSupportedSender){
            logWriterUtility.error(request.getRequestId(),"Sender must be agent");
            throw new PocketException(PocketErrorCode.SenderMustBeAgent);
        }

        receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), clientStatus);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }


        if(!receiverCfeClient.getGroupCode().equals(TsEnums.UserType.CUSTOMER.getUserTypeString())){
            logWriterUtility.error(request.getRequestId(),"Receiver must be customer");
            throw new PocketException(PocketErrorCode.ReceiverMustBeCustomer);
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }


    Boolean validateRequestAccountsForBankCashIn(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//
//        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
//        if (senderCfeClient == null) {
//            logWriterUtility.error(request.getRequestId(), "Sender client not found");
//            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
//        }
//
//        boolean isSupportedSender=(senderCfeClient.getGroupCode().equals(TsEnums.UserType.AGENT.getUserTypeString()))||(senderCfeClient.getGroupCode().equals(TsEnums.UserType.MERCHANT.getUserTypeString()));
//
//        if(!isSupportedSender){
//            logWriterUtility.error(request.getRequestId(),"Sender must be agent");
//            throw new PocketException(PocketErrorCode.SenderMustBeAgent);
//        }

        receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }


//        if(!receiverCfeClient.getGroupCode().equals(TsEnums.UserType.CUSTOMER.getUserTypeString())){
//            logWriterUtility.error(request.getRequestId(),"Receiver must be customer");
//            throw new PocketException(PocketErrorCode.ReceiverMustBeCustomer);
//        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }


    Boolean validateRequestAccountsForCashOut(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        if(!senderCfeClient.getGroupCode().equals(TsEnums.UserType.CUSTOMER.getUserTypeString())){
            logWriterUtility.error(request.getRequestId(),"Sender must be customer");
            throw new PocketException(PocketErrorCode.SenderMustBeCustomer);
        }

        receiverCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), clientStatus);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }


        boolean isReceiverValid=receiverCfeClient.getGroupCode().equals(TsEnums.UserType.AGENT.getUserTypeString())||receiverCfeClient.getGroupCode().equals(TsEnums.UserType.MERCHANT.getUserTypeString());
        if(!isReceiverValid){
            logWriterUtility.error(request.getRequestId(),"Sender must be agent");
            throw new PocketException(PocketErrorCode.ReceiverMustBeAgent);
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }

    Boolean validateRequestAccountsForTopUp(TransactionValidityRequest request) throws PocketException {


        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        senderCfeClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), clientStatus);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletIsRequired);
        }

        TsMerchantDetails merchantDetails = merchantDetailsRepository.findFirstByShortCode(PocketConstants.TOP_UP);
        if (merchantDetails == null) {
            logWriterUtility.error(request.getRequestId(), "TopUp Merchant client not found");
            throw new PocketException(PocketErrorCode.MerchantNotFound);
        }

        receiverCfeClient = merchantDetails.getTsClientByClientId();
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        cfeService = serviceRepository.findFirstByServiceCode(request.getType());
        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        return true;

    }

    TransactionValidityResponse getTransactionValidityResponse(TransactionValidityRequest transactionValidityRequest) throws PocketException {

        BigDecimal transactionAmount = new BigDecimal(transactionValidityRequest.getTransferAmount());


        FeeData feeData = feeManager.calculateFee(senderCfeClient, cfeService.getServiceCode(), transactionAmount,
                transactionValidityRequest.getRequestId());



        if (feeData == null) {
            logWriterUtility.error(transactionValidityRequest.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        if (feeData.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
            transactionAmount = transactionAmount.add(feeData.getFeeAmount());
        }

        if(senderCfeClient!=null){
            validator.isSenderProfileValidForTransaction(senderCfeClient, cfeService, transactionAmount, transactionValidityRequest.getRequestId());
        }

        if(receiverCfeClient!=null){
            validator.isReceiverProfileValidForTransaction(receiverCfeClient, cfeService, transactionValidityRequest.getRequestId());
        }

        TransactionValidityResponse validityResponse = new TransactionValidityResponse();
        validityResponse.setFee(feeData.getFeeAmount());
        validityResponse.setFeePayer(getFeePayer(feeData.getFeePayer(), transactionValidityRequest.getType(), transactionValidityRequest.getRequestId()));
        return validityResponse;
    }

    TransactionValidityResponse getTransactionValidityResponseTopUp(TransactionValidityRequest transactionValidityRequest) throws PocketException {

        BigDecimal transactionAmount = new BigDecimal(transactionValidityRequest.getTransferAmount());

        FeeData feeData = null;
        if (transactionValidityRequest.getCurrencyCode()==null||transactionValidityRequest.getCurrencyCode().equalsIgnoreCase("AED")) {

            if(transactionValidityRequest.getCountryCode()!=null&&transactionValidityRequest.getCountryCode().equalsIgnoreCase("BD")){
                if (transactionValidityRequest.getSource() == null || transactionValidityRequest.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                    feeData = feeManager.calculateFeeByFeeCode(  TsEnums.FeeCode.TOPUP_BD_CUSTOMER_APP.getFeeCode(),
                            new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
                } else {
                    feeData = feeManager.calculateFeeByFeeCode( TsEnums.FeeCode.TOPUP_BD_AGENT_APP.getFeeCode(),
                            new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
                }

            }else {
                if (transactionValidityRequest.getSource() == null || transactionValidityRequest.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                    feeData = feeManager.calculateFeeByFeeCode( TsEnums.FeeCode.TOPUP_LOCAL_CUSTOMER_APP.getFeeCode(),
                            new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
                } else {
                    feeData = feeManager.calculateFeeByFeeCode(  TsEnums.FeeCode.TOPUP_LOCAL_AGENT_APP.getFeeCode(),
                            new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
                }
            }
        }else if (transactionValidityRequest.getCurrencyCode().equalsIgnoreCase("BDT")) {
            if (transactionValidityRequest.getSource() == null || transactionValidityRequest.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                feeData = feeManager.calculateFeeByFeeCode(  TsEnums.FeeCode.TOPUP_BD_CUSTOMER_APP.getFeeCode(),
                        new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
            } else {
                feeData = feeManager.calculateFeeByFeeCode( TsEnums.FeeCode.TOPUP_BD_AGENT_APP.getFeeCode(),
                        new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
            }
        }else{
            if (transactionValidityRequest.getSource() == null || transactionValidityRequest.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
                feeData = feeManager.calculateFeeByFeeCode( TsEnums.FeeCode.TOPUP_LOCAL_CUSTOMER_APP.getFeeCode(),
                        new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
            } else {
                feeData = feeManager.calculateFeeByFeeCode(  TsEnums.FeeCode.TOPUP_LOCAL_AGENT_APP.getFeeCode(),
                        new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
            }
        }


        if (feeData == null) {
            logWriterUtility.error(transactionValidityRequest.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        if (feeData.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
            transactionAmount = transactionAmount.add(feeData.getFeeAmount());
        }

        if(senderCfeClient!=null){
            validator.isSenderProfileValidForTransaction(senderCfeClient, cfeService, transactionAmount, transactionValidityRequest.getRequestId());
        }

        if(receiverCfeClient!=null){
            validator.isReceiverProfileValidForTransaction(receiverCfeClient, cfeService, transactionValidityRequest.getRequestId());
        }

        TransactionValidityResponse validityResponse = new TransactionValidityResponse();
        validityResponse.setFee(feeData.getFeeAmount());
        validityResponse.setFeePayer(getFeePayer(feeData.getFeePayer(), transactionValidityRequest.getType(), transactionValidityRequest.getRequestId()));
        return validityResponse;
    }

    TransactionValidityResponse getTransactionValidityResponseBillPay(TransactionValidityRequest transactionValidityRequest) throws PocketException {

        BigDecimal transactionAmount = new BigDecimal(transactionValidityRequest.getTransferAmount());

        FeeData feeData;
        if (transactionValidityRequest.getSource() == null || transactionValidityRequest.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
            feeData = feeManager.calculateFeeByFeeCode( TsEnums.FeeCode.BILLPAY_PALLI_BIDDUT_BD_CUSTOMER_APP.getFeeCode(),
                    new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
        } else {
            feeData = feeManager.calculateFeeByFeeCode(  TsEnums.FeeCode.BILLPAY_PALLI_BIDDUT_BD_AGENT_APP.getFeeCode(),
                    new BigDecimal(transactionValidityRequest.getTransferAmount()), transactionValidityRequest.getRequestId());
        }

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(transactionValidityRequest.getRequestId(), "Fee calculation failed");

            feeData=new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeeCode(null);
            feeData.setFeePayer("dr");
        }



        if (feeData.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
            transactionAmount = transactionAmount.add(feeData.getFeeAmount());
        }

        if(senderCfeClient!=null){
            validator.isSenderProfileValidForTransaction(senderCfeClient, cfeService, transactionAmount, transactionValidityRequest.getRequestId());
        }

        if(receiverCfeClient!=null){
            validator.isReceiverProfileValidForTransaction(receiverCfeClient, cfeService, transactionValidityRequest.getRequestId());
        }

        TransactionValidityResponse validityResponse = new TransactionValidityResponse();
        validityResponse.setFee(feeData.getFeeAmount());
        validityResponse.setFeePayer(getFeePayer(feeData.getFeePayer(), transactionValidityRequest.getType(), transactionValidityRequest.getRequestId()));
        return validityResponse;
    }

    TransactionValidityResponse getTransactionValidityResponseBankCashIn(TransactionValidityRequest transactionValidityRequest) throws PocketException {

        BigDecimal transactionAmount = new BigDecimal(transactionValidityRequest.getTransferAmount());

        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(), transactionAmount,
                transactionValidityRequest.getRequestId());

        if (feeData == null) {
            logWriterUtility.error(transactionValidityRequest.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        if (feeData.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
            transactionAmount = transactionAmount.add(feeData.getFeeAmount());
        }

        if(receiverCfeClient!=null){
            validator.isReceiverProfileValidForTransaction(receiverCfeClient, cfeService, transactionValidityRequest.getRequestId());
        }

        TransactionValidityResponse validityResponse = new TransactionValidityResponse();
        validityResponse.setFee(feeData.getFeeAmount());
        validityResponse.setFeePayer(getFeePayer(feeData.getFeePayer(), transactionValidityRequest.getType(), transactionValidityRequest.getRequestId()));
        return validityResponse;
    }

    public String getFeePayer(String genericPayer, String transactionType, String requestId) {
        logWriterUtility.trace(requestId, "Payer :" + genericPayer + " transactionType :" + transactionType);
        switch (transactionType) {
            case ServiceCodeConstants.Fund_Transfer:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.Merchant_Payment:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.Top_Up:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.CashIn:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            case ServiceCodeConstants.CashOut:
                if (genericPayer.equalsIgnoreCase(FeePayer.DEBIT)) {
                    return "Sender";
                } else {
                    return "Receiver";
                }
            default:
                return null;
        }

    }

    public TsClient getSenderCfeClient() {
        return senderCfeClient;
    }

    public TsClient getReceiverCfeClient() {
        return receiverCfeClient;
    }
}