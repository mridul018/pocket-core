package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.integration.UPayTransactionRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.IntermediateTxResponse;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.utils.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class SingleTransactionServiceImpl implements SingleTransactionService {

    LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());


    @Autowired
    private TsClientRepository clientRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FeeManager feeManager;

    @Autowired
    private TsBankRepository tsBankRepository;

    @Autowired
    private TsGlRepository tsGlRepository;
    @Autowired
    private TsServiceRepository serviceRepository;

    @Autowired
    private TsTransactionRepository tsTransactionRepository;
    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;
    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private AccountService clientService;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;

    @Transactional(rollbackFor = {Exception.class,PocketException.class})
    @Override
    public BaseResponseObject doBankToMasterTransferIntermediate(TransactionRequest request, TsSessionObject tsSessionObject)
            throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromBankIntermediate);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        // Check sender
        TsBanks cfeSenderBankDetail = tsBankRepository.findFirstByBankCode(request.getBankCode());
        if (cfeSenderBankDetail == null) {
            cfeSenderBankDetail  = tsBankRepository.findFirstByBankCode("5001");
//            logWriterUtility.error(request.getRequestId(), "TsBanks not found .");
//            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        TsGl senderBankGl = tsGlRepository.findFirstByGlCode(cfeSenderBankDetail.getGlCode());


        if (senderBankGl == null) {
            logWriterUtility.error(request.getRequestId(), "sender bankGl not found .");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }


        //calculate fee
        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransferAmount()));

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(receiverCfeClient, request, cfeService.getServiceCode(), feeData,
                receiverCfeClient.getWalletNo(), transactionAmountData, cfeSenderBankDetail.getGlCode());
        cfeTransaction.setLogicalSender(cfeSenderBankDetail.getBankCode());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCfeClient.getWalletNo(), cfeSenderBankDetail.getBankCode(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, senderBankGl.getGlName(),
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterReceiverRunningBalance = clientService.updateClientCurrentBalance(receiverCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeSenderBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(masterReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(bankReceiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }


    @Transactional(rollbackFor = {Exception.class,PocketException.class})
    @Override
    public BaseResponseObject doMasterToCustomerWalletTransfer(TransactionRequest request, TsSessionObject tsSessionObject
            , String prevTxReference, String receiverWallet, TsBanks tsBanks) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.MasterWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                receiverWallet, statusNotIn);
        if (receiverCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Receiver Wallet client found");


        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromBank);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }


        //calculate fee
        FeeData feeData = feeManager.calculateFee(senderMasterClient, tsService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransferAmount()));

        //balance validation
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderMasterClient);

        if (clientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionWithRef(senderMasterClient, receiverCustomerClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData,prevTxReference,tsBanks.getBankName(),receiverCustomerClient.getWalletNo());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCustomerClient.getWalletNo(), senderMasterClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(senderMasterClient);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                senderGlName,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverCustomerClient);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderMasterClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverCustomerClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    @Override
    public BaseResponseObject doCashOutToBankCustomerToMasterWalletTransferIntermediate(TransactionRequest request, TsSessionObject tsSessionObject, String senderWallet, String bankName) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.MasterWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                senderWallet, statusNotIn);
        if (senderCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "Sender client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Sender Wallet client found");

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashOutToBank);
        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }


        //calculate fee
        FeeData feeData = feeManager.calculateFee(receiverMasterClient, tsService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransferAmount()));

        //balance validation
        TsClientBalance senderClientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderCustomerClient);

        if (senderClientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionWithRef(senderCustomerClient,receiverMasterClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData,null,senderCustomerClient.getWalletNo(),bankName);
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                 receiverMasterClient.getWalletNo(),senderCustomerClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(senderCustomerClient);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                senderGlName,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverMasterClient);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_CashOutToBankIntermediate,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderCustomerClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverMasterClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);


        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doMasterToBankGlTransfer(TransactionRequest request, TsSessionObject tsSessionObject, String prevTransactionReference, TsBanks tsBanks) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashOutToBank);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        // Check sender
        TsBanks cfeBankDetail = tsBankRepository.findFirstByBankCode(request.getBankCode());
        if (cfeBankDetail == null) {
            logWriterUtility.error(request.getRequestId(), "TsBanks not found .");
            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        TsGl receiverBankGl = tsGlRepository.findFirstByGlCode(cfeBankDetail.getGlCode());

        if (receiverBankGl == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver bank gl not found .");
            throw new PocketException(PocketErrorCode.BankGlNotFound);
        }

        //calculate fee
        FeeData feeData = feeManager.calculateFee(senderCfeClient, cfeService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransferAmount()));

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(senderCfeClient, request, cfeService.getServiceCode(), feeData,
                 transactionAmountData, cfeBankDetail.getGlCode(),prevTransactionReference,tsBanks.getBankName());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction
                , cfeBankDetail.getBankCode(),senderCfeClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, receiverBankGl.getGlName(),
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_CashOutToBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterSenderRunningBalance = clientService.updateClientCurrentBalance(senderCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(bankReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(masterSenderRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doUpayKioskToMasterTransferIntermediate(UPayTransactionRequest request) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromUPayKioskIntermediate);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** Check sender ***//
        TsBanks cfeSenderBankDetail = tsBankRepository.findFirstByBankCode("6001");
        if (cfeSenderBankDetail == null) {
            cfeSenderBankDetail  = tsBankRepository.findFirstByBankCode("5001");
        }

        TsGl senderBankGl = tsGlRepository.findFirstByGlCode(cfeSenderBankDetail.getGlCode());

        if (senderBankGl == null) {
            logWriterUtility.error(request.getRequestId(), "sender bankGl not found .");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(receiverCfeClient, request, cfeService.getServiceCode(), feeData,
                receiverCfeClient.getWalletNo(), transactionAmountData, cfeSenderBankDetail.getGlCode());
        cfeTransaction.setLogicalSender(cfeSenderBankDetail.getBankCode());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCfeClient.getWalletNo(), cfeSenderBankDetail.getBankCode(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, senderBankGl.getGlName(),
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterReceiverRunningBalance = clientService.updateClientCurrentBalance(receiverCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeSenderBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(masterReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(bankReceiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doMasterToCustomerWalletTransfer(UPayTransactionRequest request, String prevTxReference, String receiverWallet)
            throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.MasterWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                receiverWallet, statusNotIn);
        if (receiverCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Receiver Wallet client found");

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromUPayKiosk);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(senderMasterClient, tsService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        //*** balance validation ***//
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderMasterClient);

        if (clientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionWithRef(senderMasterClient, receiverCustomerClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData,prevTxReference,"UPay Bank",
                receiverCustomerClient.getWalletNo());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCustomerClient.getWalletNo(), senderMasterClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(senderMasterClient);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,senderGlName,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverCustomerClient);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderMasterClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverCustomerClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doPaynetKioskToMasterTransferIntermediate(PaynetPaymentWalletRefillRequest request) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromPaynetKioskIntermediate);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** Check sender ***//
        TsBanks cfeSenderBankDetail = tsBankRepository.findFirstByBankCode("6002");
        if (cfeSenderBankDetail == null) {
            cfeSenderBankDetail  = tsBankRepository.findFirstByBankCode("5001");
        }

        TsGl senderBankGl = tsGlRepository.findFirstByGlCode(cfeSenderBankDetail.getGlCode());

        if (senderBankGl == null) {
            logWriterUtility.error(request.getRequestId(), "sender bankGl not found .");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(receiverCfeClient, request, cfeService.getServiceCode(), feeData,
                receiverCfeClient.getWalletNo(), transactionAmountData, cfeSenderBankDetail.getGlCode());
        cfeTransaction.setLogicalSender(cfeSenderBankDetail.getBankCode());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCfeClient.getWalletNo(), cfeSenderBankDetail.getBankCode(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, senderBankGl.getGlName(),
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterReceiverRunningBalance = clientService.updateClientCurrentBalance(receiverCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeSenderBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(masterReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(bankReceiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doMasterToUserWalletTransfer(PaynetPaymentWalletRefillRequest request, String prevTxReference, String receiverWallet) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(
                TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.MasterWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                receiverWallet, statusNotIn);
        if (receiverCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Receiver Wallet client found");

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromPaynetKiosk);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(senderMasterClient, tsService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        //*** balance validation ***//
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderMasterClient);

        if (clientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionWithRef(senderMasterClient, receiverCustomerClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData,prevTxReference,"Paynet Bank",
                receiverCustomerClient.getWalletNo());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCustomerClient.getWalletNo(), senderMasterClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(senderMasterClient);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,senderGlName,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverCustomerClient);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderMasterClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverCustomerClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doEasyPayKioskToMasterTransferIntermediate(EasyPayPaymentWalletRefillRequest request) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromEasyPayKioskIntermediate);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** Check sender ***//
        TsBanks cfeSenderBankDetail = tsBankRepository.findFirstByBankCode("6003");
        if (cfeSenderBankDetail == null) {
            cfeSenderBankDetail  = tsBankRepository.findFirstByBankCode("5001");
        }

        TsGl senderBankGl = tsGlRepository.findFirstByGlCode(cfeSenderBankDetail.getGlCode());

        if (senderBankGl == null) {
            logWriterUtility.error(request.getRequestId(), "sender bankGl not found .");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(receiverCfeClient, request, cfeService.getServiceCode(), feeData,
                receiverCfeClient.getWalletNo(), transactionAmountData, cfeSenderBankDetail.getGlCode());
        cfeTransaction.setLogicalSender(cfeSenderBankDetail.getBankCode());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCfeClient.getWalletNo(), cfeSenderBankDetail.getBankCode(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, senderBankGl.getGlName(),
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterReceiverRunningBalance = clientService.updateClientCurrentBalance(receiverCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeSenderBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(masterReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(bankReceiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doMasterToUserWalletTransfer(EasyPayPaymentWalletRefillRequest request, String prevTxReference, String receiverWallet) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.MasterWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                receiverWallet, statusNotIn);
        if (receiverCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Receiver Wallet client found");

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromEasyPayKiosk);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(senderMasterClient, tsService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        //*** balance validation ***//
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderMasterClient);

        if (clientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionWithRef(senderMasterClient, receiverCustomerClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData,prevTxReference,"EasyPay Bank",
                receiverCustomerClient.getWalletNo());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCustomerClient.getWalletNo(), senderMasterClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(senderMasterClient);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,senderGlName,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverCustomerClient);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderMasterClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverCustomerClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doZenZeroKioskToMasterTransferIntermediate(ZenZeroPayPaymentWalletRefillRequest request) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCfeClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (receiverCfeClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletIsRequired);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromZenZeroKioskIntermediate);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** Check sender ***//
        TsBanks cfeSenderBankDetail = tsBankRepository.findFirstByBankCode("6004");
        if (cfeSenderBankDetail == null) {
            cfeSenderBankDetail  = tsBankRepository.findFirstByBankCode("5001");
        }

        TsGl senderBankGl = tsGlRepository.findFirstByGlCode(cfeSenderBankDetail.getGlCode());

        if (senderBankGl == null) {
            logWriterUtility.error(request.getRequestId(), "sender bankGl not found .");
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(receiverCfeClient, cfeService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(receiverCfeClient, request, cfeService.getServiceCode(), feeData,
                receiverCfeClient.getWalletNo(), transactionAmountData, cfeSenderBankDetail.getGlCode());
        cfeTransaction.setLogicalSender(cfeSenderBankDetail.getBankCode());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCfeClient.getWalletNo(), cfeSenderBankDetail.getBankCode(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, senderBankGl.getGlName(),
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Deposit_of_Master,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal masterReceiverRunningBalance = clientService.updateClientCurrentBalance(receiverCfeClient, request.getRequestId());
        BigDecimal bankReceiverRunningBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(cfeSenderBankDetail.getGlCode());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(masterReceiverRunningBalance);
        cfeTransaction.setSenderRunningBalance(bankReceiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject doMasterToUserWalletTransfer(ZenZeroPayPaymentWalletRefillRequest request, String prevTxReference, String receiverWallet) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient senderMasterClient = clientRepository.findFirstByGroupCodeAndUserStatusNotIn(TsEnums.UserType.Master.getUserTypeString(), statusNotIn);
        if (senderMasterClient == null) {
            logWriterUtility.error(request.getRequestId(), "Master client not found");
            throw new PocketException(PocketErrorCode.MasterWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Master Wallet client found");

        statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        TsClient receiverCustomerClient = clientRepository.findFirstByWalletNoAndUserStatusNotIn(
                receiverWallet, statusNotIn);
        if (receiverCustomerClient == null) {
            logWriterUtility.error(request.getRequestId(), "receiver client not found");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        logWriterUtility.trace(request.getRequestId(), "Receiver Wallet client found");

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromZenZeroKiosk);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //*** calculate fee ***//
        FeeData feeData = feeManager.calculateFee(senderMasterClient, tsService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            feeData = new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeePayer(TsEnums.TransactionDrCrType.DEBIT.getTransactionDrCrType());
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransactionAmount()));

        //*** balance validation ***//
        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(senderMasterClient);

        if (clientBalance.getBalance().compareTo(transactionAmountData.getSenderDebitAmount()) < 0) {
            logWriterUtility.error(request.getRequestId(), "Insufficient account balance.");
            throw new PocketException(PocketErrorCode.InsufficientAccountBalance);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //*** build transaction ***//
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionWithRef(senderMasterClient, receiverCustomerClient,
                request, tsService.getServiceCode(), feeData, transactionAmountData,prevTxReference,"ZenZero Bank",
                receiverCustomerClient.getWalletNo());
        tsTransactionRepository.save(cfeTransaction);
        logWriterUtility.trace(request.getRequestId(), "TsTransaction saved");

        //*** build transaction detail ***//
        List<TsTransactionDetails> transactionDetailList = transactionBuilder.buildTransactionDetail(cfeTransaction,
                receiverCustomerClient.getWalletNo(), senderMasterClient.getWalletNo(), transactionAmountData);
        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "TsTransactionDetails list saved");

        //*** build gl entry ***//
        List<TsGlTransactionDetails> glTransactionDetailsList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(senderMasterClient);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }

        TsGlTransactionDetails debitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,senderGlName,
                transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(debitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverCustomerClient);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails creditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName,
                BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(creditGl);

        TsGlTransactionDetails vatPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0),
                transactionAmountData.getVatCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(vatPayableGl);

        TsGlTransactionDetails feePayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.Fee_payable_for_CashInFromBank,
                BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(), request.getRequestId(), tsGlRepository);
        feePayableGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTransactionDetailsList.add(feePayableGl);

        TsGlTransactionDetails aitPayableGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction, GlConstants.AIT_Payable,
                BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(), request.getRequestId(), tsGlRepository);
        glTransactionDetailsList.add(aitPayableGl);

        tsGlTransactionDetailsRepository.saveAll(glTransactionDetailsList);

        BigDecimal senderMasterRunningBalance = clientService.updateClientCurrentBalance(senderMasterClient, request.getRequestId());
        BigDecimal receiverCustomerRunningBalance = clientService.updateClientCurrentBalance(receiverCustomerClient, request.getRequestId());

        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setReceiverRunningBalance(receiverCustomerRunningBalance);
        cfeTransaction.setSenderRunningBalance(senderMasterRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        IntermediateTxResponse intermediateTxResponse=new IntermediateTxResponse();
        intermediateTxResponse.setTxReference(cfeTransaction.getToken());
        intermediateTxResponse.setTxId(cfeTransaction.getId());
        intermediateTxResponse.setTxCreateDate(cfeTransaction.getCreatedDate().toString());

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(intermediateTxResponse);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

}
