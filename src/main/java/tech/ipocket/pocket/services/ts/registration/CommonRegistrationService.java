package tech.ipocket.pocket.services.ts.registration;

import tech.ipocket.pocket.response.ts.RegistrationResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface CommonRegistrationService {
    RegistrationResponse doRegister(CustomerRegistration customerRegistration) throws PocketException;
}
