package tech.ipocket.pocket.services.ts.transactionValidity;

import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.response.ts.transactionValidity.TransactionValidityResponse;
import tech.ipocket.pocket.utils.GroupCodes;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

public class FundTransferTransactionValidity extends AbstractTransactionValidity {

    @Override
    public TransactionValidityResponse getTransactionValidity(TransactionValidityRequest transactionValidityRequest) throws PocketException {

        validateRequestAccountsForFundTransfer(transactionValidityRequest);

        isSenderTypeValid(getSenderCfeClient().getGroupCode());
        isReceiverTypeValid(getReceiverCfeClient().getGroupCode());

        TransactionValidityResponse validityResponse = getTransactionValidityResponse(transactionValidityRequest);
        return validityResponse;
    }

    private Boolean isSenderTypeValid(String senderType) throws PocketException {

        switch (senderType) {
            case GroupCodes.CUSTOMER:
                return true;
        }
        throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
    }

    private Boolean isReceiverTypeValid(String receiverType) throws PocketException {

        switch (receiverType) {
            case GroupCodes.CUSTOMER:
                return true;
        }
        throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
    }
}