package tech.ipocket.pocket.services.ts.feeShairing;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface FeeSharingService {
    BaseResponseObject feeSharing(String requestId) throws PocketException;
}
