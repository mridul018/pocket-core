package tech.ipocket.pocket.services.ts.registration;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;
import tech.ipocket.pocket.response.ts.RegistrationResponse;
import tech.ipocket.pocket.utils.GroupCodes;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

@Component
public class RegistrationFactory implements ApplicationContextAware {

    ApplicationContext applicationContext;

    public RegistrationResponse doRegistration(WalletCreateRequest walletCreateRequest, String userType) throws PocketException {

        switch (userType) {
            case GroupCodes.SR:
            case GroupCodes.AGENT:
            case GroupCodes.MERCHANT:
            case GroupCodes.CUSTOMER:

                // TODO: 2019-04-22 Have to extra code for masterwallet.
            case GroupCodes.MASTER:
                CustomerRegistration registration = new CustomerRegistration();
                registration.setData(walletCreateRequest);
                CommonRegistrationService registrationService = applicationContext.getBean(CommonRegistrationServiceImpl.class);
                return registrationService.doRegister(registration);

            default:
                throw new PocketException(PocketErrorCode.UnSupportedUserType);
        }
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
