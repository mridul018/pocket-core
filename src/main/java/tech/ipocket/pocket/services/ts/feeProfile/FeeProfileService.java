package tech.ipocket.pocket.services.ts.feeProfile;

import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.feeProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface FeeProfileService {
    BaseResponseObject createFeeProfile(CreateFeeProfileRequest request) throws PocketException;

    BaseResponseObject getFeeProfile(GetFeeProfileRequest request);
    BaseResponseObject updateFeeProfile(CreateFeeProfileRequest request) throws PocketException;

    BaseResponseObject createFee(CreateFeeRequest request) throws PocketException;

    BaseResponseObject getFee(GetFeeRequest request) throws PocketException;

    BaseResponseObject createStakeHolder(CreateStakeHolder request) throws PocketException;

    BaseResponseObject getStakeHolder(GetStakeHolderRequest request);

    BaseResponseObject createFeeFeeProfileServiceMap(CreateFeeProfileLimitMapRequest request) throws PocketException;
    BaseResponseObject getConversionRate(GetConversionRate request) throws PocketException;


    BaseResponseObject createOrUpdateConversionRate(CreateConversionRate request) throws PocketException;

    BaseResponseObject updateFee(CreateFeeRequest request) throws PocketException;

    BaseResponseObject getFeeProfileFeeList(GetFeeProfileRequest request) throws PocketException;
}
