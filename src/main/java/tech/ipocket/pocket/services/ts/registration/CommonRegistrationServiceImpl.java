package tech.ipocket.pocket.services.ts.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsClientBalance;
import tech.ipocket.pocket.entity.TsDefaultProfile;
import tech.ipocket.pocket.repository.ts.TsClientBalanceRepository;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.ts.TsDefaultProfileRepository;
import tech.ipocket.pocket.response.ts.RegistrationResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.TsEnums;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service

public class CommonRegistrationServiceImpl implements CommonRegistrationService {

    LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;

    @Autowired
    private TsDefaultProfileRepository defaultProfileRepository;

    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;

    @Override
    @Transactional(rollbackFor = {Exception.class, PocketException.class})
    public RegistrationResponse doRegister(CustomerRegistration customerRegistration) throws PocketException {

        // check wallet exists
        // Load profile-> fee, transaction
        // Save data on PocketWallet
        // Save data on ClientBalance
        // Send response

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

        Integer userCount = clientRepository.countAllByWalletNoAndUserStatusNotIn(customerRegistration.getMobileNumber(), clientStatus);

        if (userCount > 0) {
            logWriterUtility.error(customerRegistration.getRequestId(), "Active user count :" + userCount);
            throw new PocketException(PocketErrorCode.WalletAlreadyExists);

        }

        logWriterUtility.trace(customerRegistration.getRequestId(), "No client found");

        TsDefaultProfile defaultProfile = defaultProfileRepository.findFirstByGroupCode(customerRegistration.getGroupCode());

        if (defaultProfile == null) {
            logWriterUtility.error(customerRegistration.getRequestId(), "Default profile not found");
            throw new PocketException(PocketErrorCode.DefaultProfileNotFound);
        }

        TsClient client = new TsClient();
        client.setFeeProfileCode(defaultProfile.getFeeProfile());
        client.setTransactionProfileCode(defaultProfile.getTransactionProfile());

        client.setWalletNo(customerRegistration.getMobileNumber());
        client.setFullName(customerRegistration.getFullName());
        client.setGroupCode(customerRegistration.getGroupCode());
        client.setUserStatus(customerRegistration.getUserStatus());
        client.setCreatedDate(new Date());

        clientRepository.save(client);

        TsClientBalance clientBalance = new TsClientBalance();
        clientBalance.setBalance(BigDecimal.ZERO);
        clientBalance.setTsClientByClientId(client);

        clientBalanceRepository.save(clientBalance);

        RegistrationResponse registrationResponse = new RegistrationResponse();
        registrationResponse.setClientId(client.getId());
        registrationResponse.setStatus(200);

        return registrationResponse;
    }
}
