package tech.ipocket.pocket.services.ts;

import tech.ipocket.pocket.entity.TsBanks;
import tech.ipocket.pocket.request.integration.UPayTransactionRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface SingleTransactionService {

    BaseResponseObject doBankToMasterTransferIntermediate(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException;
    BaseResponseObject doMasterToCustomerWalletTransfer(TransactionRequest request, TsSessionObject tsSessionObject, String prevTxReference, String receiverWallet, TsBanks tsBanks) throws PocketException;
    BaseResponseObject doCashOutToBankCustomerToMasterWalletTransferIntermediate(TransactionRequest request, TsSessionObject tsSessionObject, String senderWallet, String bankName) throws PocketException;

    BaseResponseObject doMasterToBankGlTransfer(TransactionRequest request, TsSessionObject tsSessionObject, String prevTransactionReference, TsBanks tsBanks)throws PocketException;

    BaseResponseObject doUpayKioskToMasterTransferIntermediate(UPayTransactionRequest request) throws PocketException;
    BaseResponseObject doMasterToCustomerWalletTransfer(UPayTransactionRequest request, String prevTxReference, String receiverWallet) throws PocketException;

    BaseResponseObject doPaynetKioskToMasterTransferIntermediate(PaynetPaymentWalletRefillRequest request) throws PocketException;
    BaseResponseObject doMasterToUserWalletTransfer(PaynetPaymentWalletRefillRequest request, String prevTxReference, String receiverWallet) throws PocketException;

    BaseResponseObject doEasyPayKioskToMasterTransferIntermediate(EasyPayPaymentWalletRefillRequest request) throws PocketException;
    BaseResponseObject doMasterToUserWalletTransfer(EasyPayPaymentWalletRefillRequest request, String prevTxReference, String receiverWallet) throws PocketException;

    BaseResponseObject doZenZeroKioskToMasterTransferIntermediate(ZenZeroPayPaymentWalletRefillRequest request) throws PocketException;
    BaseResponseObject doMasterToUserWalletTransfer(ZenZeroPayPaymentWalletRefillRequest request, String prevTxReference, String receiverWallet) throws PocketException;
}
