package tech.ipocket.pocket.services.ts.remittance;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface BeneficiaryService {

    BaseResponseObject createBeneficiary(CreateBeneficiaryRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject getBeneficiary(GetBeneficiaryRequest request, TsSessionObject tsSessionObject);
}
