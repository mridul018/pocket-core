package tech.ipocket.pocket.services.ts.reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.*;
import tech.ipocket.pocket.repository.web.UserGroupRepository;
import tech.ipocket.pocket.request.ts.AccountStatementRequest;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.GetAggregateBalanceRequest;
import tech.ipocket.pocket.request.ts.TransactionDashboardBalanceRequest;
import tech.ipocket.pocket.request.ts.reporting.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SearchData;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.reporting.*;
import tech.ipocket.pocket.response.um.UserListItem;
import tech.ipocket.pocket.utils.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

@Service
public class ReportServiceImpl implements ReportService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository transactionRepository;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;

    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsBankRepository tsBankRepository;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private TsGlRepository generalLedgerRepository;
    @Autowired
    private TsMerchantDetailsRepository merchantDetailsRepository;
    @Autowired
    private TsFeeStakeholderRepository feeStakeholderRepository;
    @Autowired
    private TsTransactionProfileLimitMappingRepository tsTransactionProfileLimitMappingRepository;
    @Autowired
    private TsLimitRepository limitRepository;
    @Autowired
    private UmCountryRepository countryRepository;
    @Autowired
    private UmStateRepository umStateRepository;
    @Autowired
    private UmCountryStateMappingRepository umCountryStateMappingRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;
    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    private UmSrAgentMappingRepository umSrAgentMappingRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private UmStateRepository stateRepository;


    @Override
    public BaseResponseObject getAccountStatement(AccountStatementRequest accountStatementRequest,
                                                  TsSessionObject umSessionResponse, Sort.Direction direction) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(accountStatementRequest.getMobileNumber(),
                statusNotIn);


//        TsClient client = clientRepository.findByWalletNo(accountStatementRequest.getMobileNumber());

        if (client == null) {
            logWriterUtility.error(accountStatementRequest.getRequestId(), "Account not found");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        AccountStatementResponse finalResponse = new AccountStatementResponse();

        UmUserInfo umUserInfo = userInfoRepository.findByLoginId(client.getWalletNo());
        if (umUserInfo == null) {
            finalResponse.setFullName(client.getFullName());
            finalResponse.setUserType(UmEnums.UserGroup.getGroupNameByCode(client.getGroupCode()));
        } else {

            finalResponse.setFullName(umUserInfo.getFullName());
            finalResponse.setUserType(UmEnums.UserGroup.getGroupNameByCode(umUserInfo.getGroupCode()));
        }


        finalResponse.setAccountOpeningDate(client.getCreatedDate().toString());


        PageRequest pageRequest = PaginationUtil.makePageRequest(accountStatementRequest.getPageId(),
                accountStatementRequest.getNumberOfItemPerPage(),
                direction, "id");

        Date startDate = DateUtil.parseDate(accountStatementRequest.getFromDate());
        Date endDate = DateUtil.parseDate(accountStatementRequest.getToDate());

        endDate = DateUtil.addDay(endDate, 1);

        List<String> txFailedStatus = new ArrayList<>();
        txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
        txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

        List<TsTransaction> transactionList = transactionRepository
                .findByPayerIdOrPayeeIdAndProductCodeAndTransactionDateBetweenAndTransactionStatusNotIn(
                        client.getWalletNo(), client.getWalletNo(),
                        startDate, endDate, txFailedStatus);


        //Load current balance
        AccountStatementBalanceData accountStatementBalanceData = new AccountStatementBalanceData();
        TsClientBalance tsClientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
        accountStatementBalanceData.setCurrentBalance(tsClientBalance.getBalance());

        finalResponse.setBalance(accountStatementBalanceData);

        TsTransaction previousTransaction = transactionRepository.findPreviousTransactionByStartDateAndTransactionStatusNotIn(accountStatementRequest.getMobileNumber(),
                startDate,
                txFailedStatus);

        BigDecimal startBalance = BigDecimal.ZERO;

        if (previousTransaction != null) {
            if (previousTransaction.getSenderWallet().equals(accountStatementRequest.getMobileNumber())) {
                startBalance = previousTransaction.getSenderRunningBalance();
            } else {
                startBalance = previousTransaction.getReceiverRunningBalance();
            }
        }


        TsTransaction endTransaction = transactionRepository.findLastTransactionByDateAndTransactionStatusNotIn(accountStatementRequest.getMobileNumber(),
                endDate,
                txFailedStatus);

        BigDecimal endBalance = BigDecimal.ZERO;

        if (endTransaction != null) {
            if (endTransaction.getSenderWallet() != null && endTransaction.getSenderWallet().equals(accountStatementRequest.getMobileNumber())) {
                endBalance = endTransaction.getSenderRunningBalance();
            } else {
                endBalance = endTransaction.getReceiverRunningBalance();
            }
        }


        accountStatementBalanceData.setStartAmount(startBalance);
        accountStatementBalanceData.setEndAmount(endBalance);

        if (transactionList == null || transactionList.size() == 0) {


            finalResponse.setTotalDebit(BigDecimal.ZERO);
            finalResponse.setTotalCredit(BigDecimal.ZERO);

            BaseResponseObject baseResponseObject = new BaseResponseObject(accountStatementRequest.getRequestId());
            baseResponseObject.setStatus(PocketConstants.OK);
            baseResponseObject.setData(finalResponse);
            baseResponseObject.setError(null);

            return baseResponseObject;
        }

        List<AccountStatementListItemData> statementList = new ArrayList<>();

        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;


        int totalNoOfTransaction = transactionList.size();
        BigDecimal totalTransactionAmount = BigDecimal.ZERO;

        for (TsTransaction tsTransaction : transactionList) {
            if (tsTransaction.getToken() == null) {
                logWriterUtility.error(accountStatementRequest.getRequestId(), "No token found for transaction :" + tsTransaction.getId());
                continue;
            }

            totalTransactionAmount = totalTransactionAmount.add(tsTransaction.getTransactionAmount());

            AccountStatementListItemData statementListItem = getAccountStatementListItem(tsTransaction, accountStatementRequest.getMobileNumber(), false, tsServiceRepository);


            if (tsTransaction.getSenderWallet() != null &&
                    tsTransaction.getSenderWallet().equals(accountStatementRequest.getMobileNumber())) {
                totalDebit = totalDebit.add(statementListItem.getWithdraw_amount());
            }

            if (tsTransaction.getReceiverWallet() != null &&
                    tsTransaction.getReceiverWallet().equals(accountStatementRequest.getMobileNumber())) {
                totalCredit = totalCredit.add(statementListItem.getDeposit_Amount());
            }

            statementList.add(statementListItem);
        }

        if (totalDebit == null) {
            totalDebit = BigDecimal.ZERO;
        }
        if (totalCredit == null) {
            totalCredit = BigDecimal.ZERO;
        }


        finalResponse.setTotalDebit(totalDebit == null ? BigDecimal.ZERO : totalDebit);
        finalResponse.setTotalCredit(totalCredit == null ? BigDecimal.ZERO : totalCredit);
        finalResponse.setStatementhistory(statementList);
        finalResponse.setTotal(transactionList.size());

        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);

        SearchData searchData = new SearchData();
        searchData.setName(client.getFullName());

        if (clientBalance != null) {
            searchData.setCurrentBalance(clientBalance.getBalance());
        }

        searchData.setTotalNoOfTransaction(totalNoOfTransaction);
        searchData.setTotalTransactionAmount(totalTransactionAmount);

        searchData.setWalletNo(client.getWalletNo());
        searchData.setStartDate(accountStatementRequest.getFromDate());
        searchData.setEndDate(accountStatementRequest.getToDate());
        finalResponse.setSearchData(searchData);


        BaseResponseObject baseResponseObject = new BaseResponseObject(accountStatementRequest.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(finalResponse);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getDashboardAggregateBalance(GetAggregateBalanceRequest aggregateBalanceRequest, TsSessionObject umSessionResponse) {
        DashboardAggregateBalanceResponse dashboardAggregateBalanceResponse = new DashboardAggregateBalanceResponse();

        BigDecimal aggregateBalance = BigDecimal.ZERO;

        BigDecimal masterBalance = BigDecimal.ZERO;
        TsGl masterGl = generalLedgerRepository.findFirstByGlName(GlConstants.Deposit_of_Master);
        if (masterGl != null) {
            masterBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(masterGl.getGlCode());
            dashboardAggregateBalanceResponse.setCustomerBalance(masterBalance);
        } else {
            masterBalance = BigDecimal.ZERO;
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Deposit_of_Master gl not found");
        }

        if (masterBalance == null) {
            masterBalance = BigDecimal.ZERO;
        }
        aggregateBalance = aggregateBalance.add(masterBalance);


        dashboardAggregateBalanceResponse.setMasterWalletBalance(masterBalance);


        List<TsBanks> bankList = tsBankRepository.findAllByStatus("1");

        if (bankList == null || bankList.size() == 0) {
            logWriterUtility.trace(aggregateBalanceRequest.getRequestId(), "Bank List size : 0");
            dashboardAggregateBalanceResponse.setBankGLBalance(BigDecimal.ZERO);
        } else {
            logWriterUtility.trace(aggregateBalanceRequest.getRequestId(), "Bank List size :" + bankList.size());

            List<String> glCodes = new ArrayList<>();

            bankList.forEach(bankItem -> {
                glCodes.add(bankItem.getGlCode());
            });

            BigDecimal bankGlBalance = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(glCodes,
                    aggregateBalanceRequest.getRequestId());

            if (bankGlBalance == null) {
                bankGlBalance = BigDecimal.ZERO;
            }
            dashboardAggregateBalanceResponse.setBankGLBalance(bankGlBalance);

        }

        BigDecimal customerBalance = BigDecimal.ZERO;
        TsGl customerGl = generalLedgerRepository.findFirstByGlName(GlConstants.Deposit_of_Customer);
        if (customerGl != null) {
            customerBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(customerGl.getGlCode());
        } else {
            customerBalance = BigDecimal.ZERO;
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Deposit_of_Customer gl not found");
        }

        if (customerBalance == null) {
            customerBalance = BigDecimal.ZERO;
        }
        dashboardAggregateBalanceResponse.setCustomerBalance(customerBalance);
        aggregateBalance = aggregateBalance.add(customerBalance);


        BigDecimal srBalance = BigDecimal.ZERO;
        TsGl srGl = generalLedgerRepository.findFirstByGlName(GlConstants.Deposit_of_SR);
        if (srGl != null) {
            srBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(srGl.getGlCode());
        } else {
            srBalance = BigDecimal.ZERO;
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Deposit_of_SR gl not found");
        }

        if (srBalance == null) {
            srBalance = BigDecimal.ZERO;
        }
        dashboardAggregateBalanceResponse.setSrBalance(srBalance);
        aggregateBalance = aggregateBalance.add(srBalance);


        BigDecimal agentGlBalance = BigDecimal.ZERO;
        TsGl agentDepositGl = generalLedgerRepository.findFirstByGlName(GlConstants.Deposit_of_Agent);
        if (agentDepositGl != null) {
            agentGlBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(agentDepositGl.getGlCode());

        } else {
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Deposit_of_Merchant gl not found");
        }

        if (agentGlBalance == null) {
            agentGlBalance = BigDecimal.ZERO;
        }
        dashboardAggregateBalanceResponse.setAgentGlBalance(agentGlBalance);

        aggregateBalance = aggregateBalance.add(agentGlBalance);


        BigDecimal balanceOfTopUpService=BigDecimal.ZERO;

        BigDecimal merchantGlBalance = BigDecimal.ZERO;
        TsGl merchantGl = generalLedgerRepository.findFirstByGlName(GlConstants.Deposit_of_Merchant);
        if (merchantGl != null) {
            merchantGlBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(merchantGl.getGlCode());

        } else {
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Deposit_of_Merchant gl not found");
        }
        if (merchantGlBalance == null) {
            merchantGlBalance = BigDecimal.ZERO;
        }

        merchantGlBalance = merchantGlBalance.add(balanceOfTopUpService);

        List<TsMerchantDetails> merchantDetailsList = merchantDetailsRepository.findAll();

        BigDecimal onlineMerchantBalance = BigDecimal.ZERO;
        if (merchantDetailsList != null && merchantDetailsList.size() > 0) {

            for (TsMerchantDetails cfeMerchantDetails : merchantDetailsList) {
                TsGl billerGl = generalLedgerRepository.findFirstByGlCode(cfeMerchantDetails.getGlCode());
                if (billerGl != null) {
                    BigDecimal billerGlBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(billerGl.getGlCode());

                    if (billerGlBalance != null) {
                        onlineMerchantBalance = onlineMerchantBalance.add(billerGlBalance);
                    }

                } else {
                    logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Balance_Of_SSL gl not found");
                }
            }
        }


        if (onlineMerchantBalance == null) {
            onlineMerchantBalance = BigDecimal.ZERO;
        }
        merchantGlBalance = merchantGlBalance.add((onlineMerchantBalance));

        dashboardAggregateBalanceResponse.setMerchantBalance(merchantGlBalance);

        aggregateBalance = aggregateBalance.add((merchantGlBalance));

        List<String> pocketStakeHolderGlCodes = feeStakeholderRepository.findAllGlCode();

        BigDecimal stakeHolderBalance = BigDecimal.ZERO;
        if (pocketStakeHolderGlCodes != null && pocketStakeHolderGlCodes.size() > 0) {

            Set<String> uniqueStakeHolder = new HashSet<>(pocketStakeHolderGlCodes);

            BigDecimal stakeHolderBalanceTemp = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(new ArrayList<>(uniqueStakeHolder),
                    aggregateBalanceRequest.getRequestId());

            if (stakeHolderBalanceTemp != null) {
                stakeHolderBalance = stakeHolderBalance.add(stakeHolderBalanceTemp);
            }

        } else {
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Fee_Payable gl not found");
            dashboardAggregateBalanceResponse.setStakeholderGlBalance(BigDecimal.ZERO);
        }

        if (stakeHolderBalance == null) {
            stakeHolderBalance = BigDecimal.ZERO;
        }

        dashboardAggregateBalanceResponse.setStakeholderGlBalance(stakeHolderBalance);
        aggregateBalance = aggregateBalance.add((stakeHolderBalance));


        List<String> feePayableGlCodes = new CustomQuery(entityManager)
                .getDistinctGlCodesUsingGlNameLike("Fee_payable%", aggregateBalanceRequest.getRequestId());

        TsGl unresolvedFeeGl = generalLedgerRepository.findFirstByGlName(GlConstants.Fee_payable_for_Unresolved);

        BigDecimal unresolvedFeeGlBalance = BigDecimal.ZERO;

        if (unresolvedFeeGl != null) {
            feePayableGlCodes.remove(unresolvedFeeGl.getGlCode());

            List<String> glCode = new ArrayList<>();
            glCode.add(unresolvedFeeGl.getGlCode());
            BigDecimal unresolvedFeeGlBalanceTemp = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(glCode,
                    aggregateBalanceRequest.getRequestId());
            unresolvedFeeGlBalance = unresolvedFeeGlBalance.add(unresolvedFeeGlBalanceTemp);
        }

        dashboardAggregateBalanceResponse.setUnResolvedGlbalance(unresolvedFeeGlBalance);
        aggregateBalance = aggregateBalance.add(unresolvedFeeGlBalance);

        BigDecimal feePayableBalance = BigDecimal.ZERO;

        if (feePayableGlCodes != null && feePayableGlCodes.size() > 0) {
            feePayableBalance = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(feePayableGlCodes,
                    aggregateBalanceRequest.getRequestId());

        } else {
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Fee_Payable gl not found");
        }

        if (feePayableBalance == null) {
            feePayableBalance = BigDecimal.ZERO;
        }

        dashboardAggregateBalanceResponse.setFeePayableBalance(feePayableBalance);
        aggregateBalance = aggregateBalance.add(feePayableBalance);

        List<String> vatPayableGlCodes = new CustomQuery(entityManager)
                .getDistinctGlCodesUsingGlNameLike("VAT_Payable%", aggregateBalanceRequest.getRequestId());

        BigDecimal vatPayableBalance = BigDecimal.ZERO;
        if (vatPayableGlCodes != null && vatPayableGlCodes.size() > 0) {
            vatPayableBalance = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(vatPayableGlCodes,
                    aggregateBalanceRequest.getRequestId());

        } else {
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "Fee_Payable gl not found");
            dashboardAggregateBalanceResponse.setVatBalance(BigDecimal.ZERO);

        }

        if (vatPayableBalance == null) {
            vatPayableBalance = BigDecimal.ZERO;
        }

        dashboardAggregateBalanceResponse.setVatBalance(vatPayableBalance);
        aggregateBalance = aggregateBalance.add(vatPayableBalance);

        List<String> aitPayableGlCodes = new CustomQuery(entityManager)
                .getDistinctGlCodesUsingGlNameLike("AIT_Payable%", aggregateBalanceRequest.getRequestId());

        BigDecimal aitPayableBalance = BigDecimal.ZERO;

        if (aitPayableGlCodes != null && aitPayableGlCodes.size() > 0) {
            aitPayableBalance = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(aitPayableGlCodes,
                    aggregateBalanceRequest.getRequestId());

        } else {
            logWriterUtility.error(aggregateBalanceRequest.getRequestId(), "AIT_Payable gl not found");
        }

        if (aitPayableBalance == null) {
            aitPayableBalance = BigDecimal.ZERO;
        }

        dashboardAggregateBalanceResponse.setAitBalance(aitPayableBalance);
        aggregateBalance = aggregateBalance.add(aitPayableBalance);

        BigDecimal incomeGlBalance = BigDecimal.ZERO;
        List<TsGl> incomeGls = generalLedgerRepository.findAllByParentGlCode(GlConstants.POCKET_INCOME_PARENT_GL_CODE);

        if (incomeGls != null && incomeGls.size() > 0) {
            List<String> glCodes = new ArrayList<>();
            for (TsGl gl : incomeGls) {
                glCodes.add(gl.getGlCode());
            }

            incomeGlBalance = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(glCodes,
                    aggregateBalanceRequest.getRequestId());
        }

        if (incomeGlBalance == null) {
            incomeGlBalance = BigDecimal.ZERO;
        }

        dashboardAggregateBalanceResponse.setIncomeBalance(incomeGlBalance);


        BigDecimal expenseGlBalance = BigDecimal.ZERO;

        List<TsGl> expenseGls = generalLedgerRepository.findAllByParentGlCode(GlConstants.POCKET_EXPENSE_PARENT_GL_CODE);

        if (expenseGls != null && expenseGls.size() > 0) {
            List<String> glCodes = new ArrayList<>();
            for (TsGl gl : expenseGls) {
                glCodes.add(gl.getGlCode());
            }

            expenseGlBalance = new CustomQuery(entityManager).getGlAggregateBalanceUsingGlCodes(glCodes,
                    aggregateBalanceRequest.getRequestId());
        }

        if (expenseGlBalance == null) {
            expenseGlBalance = BigDecimal.ZERO;
        }

        dashboardAggregateBalanceResponse.setExpenseBalance(expenseGlBalance);
        aggregateBalance = aggregateBalance.add(expenseGlBalance);

        aggregateBalance = aggregateBalance.add(expenseGlBalance);

        dashboardAggregateBalanceResponse.setAggregateBalance(aggregateBalance);

        BaseResponseObject baseResponseObject = new BaseResponseObject(aggregateBalanceRequest.getRequestId());
        baseResponseObject.setData(dashboardAggregateBalanceResponse);
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getTransactionDashboardBalance(TransactionDashboardBalanceRequest aggregateBalanceRequest, TsSessionObject umSessionResponse) throws PocketException {


        Date fromDate = DateUtil.parseDate(DateUtil.getTodayDate());
        Date toDate = DateUtil.parseDate(DateUtil.getTodayDate());

        if (aggregateBalanceRequest.getFromDate() != null) {
            fromDate = DateUtil.parseDate(aggregateBalanceRequest.getFromDate());
        }

        if (aggregateBalanceRequest.getToDate() != null) {
            toDate = DateUtil.parseDate(aggregateBalanceRequest.getToDate());
        }

        toDate = DateUtil.addDay(toDate, 1);

        List<TransactionDashboardListItem> listItems = new ArrayList<>();

        TsEnums.Services[] transactionTypes = TsEnums.Services.values();

        for (TsEnums.Services type : transactionTypes) {


            TransactionDashboardListItem refillFromCardList = getTransactionDetails(type, fromDate, toDate, aggregateBalanceRequest);
            if (refillFromCardList != null) {
                listItems.add(refillFromCardList);
            }
        }

//        getTransactionDetailsForReversalTx(listItems, Enums.TransactionTypes.Reversal, fromDate, toDate, aggregateBalanceRequest);


        BaseResponseObject baseResponseObject = new BaseResponseObject(aggregateBalanceRequest.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setData(listItems);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getSummaryOfTransactions(GetSummeryOfTransactionRequest getSummeryOfTransactionRequest, TsSessionObject umSessionResponse) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(getSummeryOfTransactionRequest.getRequestId());

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(getSummeryOfTransactionRequest.getMobileNumber(), statusNotIn);


//        TsClient client = clientRepository.findByWalletNo(getSummeryOfTransactionRequest.getMobileNumber());

        if (client == null) {
            logWriterUtility.error(getSummeryOfTransactionRequest.getRequestId(), "Account not found");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }


        Date startDate = DateUtil.parseDate(getSummeryOfTransactionRequest.getFromDate());
        Date endDate = DateUtil.parseDate(getSummeryOfTransactionRequest.getToDate());

        endDate = DateUtil.addDay(endDate, 1);

        List<String> txFailedStatus = new ArrayList<>();
        txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
        txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

        List<TsTransaction> transactionList = null;

        switch (getSummeryOfTransactionRequest.getTransactionCategory()) {
            case "IN":
                //query for in

                transactionList = transactionRepository
                        .findByTransactionDateBetweenAndPayeeIdAndTransactionStatusNotIn(startDate, endDate, client.getId(), txFailedStatus);

                break;
            case "OUT":
                // query for out

                transactionList = transactionRepository
                        .findByTransactionDateBetweenAndPayerIdAndTransactionStatusNotIn(
                                startDate, endDate, client.getId(), txFailedStatus);

                break;
            default:
                throw new PocketException(PocketErrorCode.InvalidTransactionCategory);
        }

        GetSummaryOfTransactionResponse summaryOfTransactionResponse = new GetSummaryOfTransactionResponse();
        summaryOfTransactionResponse.setTransactionCategory(getSummeryOfTransactionRequest.getTransactionCategory());

        Map<String, GetSummaryOfTransactionListItem> txSummaryMap = new HashMap<>();

        if (transactionList != null && transactionList.size() > 0) {
            // Prepare data for response


            for (TsTransaction cfeTransaction : transactionList) {
                BigDecimal txAmount = BigDecimal.ZERO;
                boolean isDebit = false;


                switch (getSummeryOfTransactionRequest.getTransactionCategory()) {
                    case "IN":
                        isDebit = false;
                        txAmount = cfeTransaction.getReceiverCreditAmount();

                        break;
                    case "OUT":
                        isDebit = true;
                        txAmount = cfeTransaction.getSenderDebitAmount();

                        break;
                    default:

                }

                GetSummaryOfTransactionListItem transactionListItem = txSummaryMap.get(cfeTransaction.getTransactionType());

                if (txAmount == null) {
                    txAmount = BigDecimal.ZERO;
                }

                if (transactionListItem == null) {
                    transactionListItem = new GetSummaryOfTransactionListItem();
                    transactionListItem.setDebit(isDebit);
                    transactionListItem.setTotalTransactionAmount(txAmount);
                    transactionListItem.setTransactionCount(1);
                } else {
                    transactionListItem.setDebit(isDebit);
                    transactionListItem.setTotalTransactionAmount(transactionListItem.getTotalTransactionAmount().add(txAmount));
                    transactionListItem.setTransactionCount(transactionListItem.getTransactionCount() + 1);
                }

                txSummaryMap.put(cfeTransaction.getTransactionType(), transactionListItem);
            }
        }

        List<GetSummaryOfTransactionListItem> listData = new ArrayList<>();

        txSummaryMap.forEach((key, transactionListItem) -> {

            TsService cfeService = tsServiceRepository.findFirstByServiceCode(key);
            if (cfeService != null) {
                transactionListItem.setTransactionType(cfeService.getServiceName());
                transactionListItem.setTransactionTypeName(cfeService.getDescription());
                listData.add(transactionListItem);
            }

        });
        summaryOfTransactionResponse.setListData(listData);

        baseResponseObject.setError(null);
        baseResponseObject.setData(summaryOfTransactionResponse);
        baseResponseObject.setStatus(PocketConstants.OK);


        return baseResponseObject;
    }

    @Override
    public BaseResponseObject transactionQuery(TransactionHistoryRequest transactionHistoryRequest, TsSessionObject umSessionResponse) throws PocketException {

        List<TsTransaction> transactionList = null;
        Date startDate = null;
        Date endDate = null;
        List<String> txFailedStatus = null;

        SearchData searchData = new SearchData();
        TsClient client = null;

        switch (transactionHistoryRequest.getFilterType()) {


            case PocketFilterConstants.TRANSACTION_TOKEN:

                if (transactionHistoryRequest.getTransactionToken() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Token required");
                    throw new PocketException(PocketErrorCode.TxTokenRequired);
                }

                searchData.setSearchedBy("Token :" + transactionHistoryRequest.getTransactionToken());

                transactionList = transactionRepository
                        .findAllByToken(transactionHistoryRequest.getTransactionToken());

                break;
            case PocketFilterConstants.MOBILE_NO_WITH_DATE_RANGE:

                searchData.setSearchedBy("Mobile Number with date range");

                if (transactionHistoryRequest.getMobileNumber() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Mobile number required");
                    throw new PocketException(PocketErrorCode.MobileNumberRequired);
                }

                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }


                List<String> statusNotIn = new ArrayList<>();
                statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
                statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

                client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(transactionHistoryRequest.getMobileNumber(), statusNotIn);

                //client=clientRepository.findByWalletNo(transactionHistoryRequest.getMobileNumber());


                if (client != null) {
                    searchData.setName(client.getFullName());
                    searchData.setWalletNo(client.getWalletNo());
                    searchData.setStartDate(transactionHistoryRequest.getFromDate());
                    searchData.setEndDate(transactionHistoryRequest.getToDate());
                }

                TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
                if (clientBalance != null) {
                    searchData.setCurrentBalance(clientBalance.getBalance());
                }

                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

//                startDate = DateUtil.addDay(startDate, 1);
                endDate = DateUtil.addDay(endDate, 1);

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

                transactionList = transactionRepository
                        .findByMobileNumberAndTransactionDateBetweenAndTransactionStatusNotIn(transactionHistoryRequest.getMobileNumber(),
                                startDate, endDate, txFailedStatus);


                break;
            case PocketFilterConstants.DATE_RANGE:
                searchData.setSearchedBy("Date Range");


                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }


                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

                endDate = DateUtil.addDay(endDate, 1);

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

                searchData.setStartDate(transactionHistoryRequest.getFromDate());
                searchData.setEndDate(transactionHistoryRequest.getToDate());

                transactionList = transactionRepository
                        .findByTransactionDateBetweenAndTransactionStatusNotIn(startDate, endDate, txFailedStatus);

                break;
            case PocketFilterConstants.UPAY_DATE_RANGE:
                searchData.setSearchedBy("Upay Date Range");

                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }

                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

                endDate = DateUtil.addDay(endDate, 1);

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

                searchData.setStartDate(transactionHistoryRequest.getFromDate());
                searchData.setEndDate(transactionHistoryRequest.getToDate());

                try {
                    Date createdDateAfter = DateUtil.parseDate("26/02/2022");  //Transaction started after 26-02-22 from UPAY Kiosk
                    transactionList = transactionRepository
                            .findByCreatedDateAfterAndCreatedDateBetweenAndTransactionStatusNotIn(createdDateAfter, startDate, endDate, txFailedStatus);
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case PocketFilterConstants.TX_TYPES_WITH_DATE_RANGE:

                searchData.setSearchedBy("Transaction Types:" + getTransactionTypes(transactionHistoryRequest.getTransactionTypes()));


                if (transactionHistoryRequest.getTransactionTypes() == null || transactionHistoryRequest.getTransactionTypes().size() == 0) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Transaction Type required");
                    throw new PocketException(PocketErrorCode.TransactionTypeRequired);
                }

                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }


                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

                endDate = DateUtil.addDay(endDate, 1);

                searchData.setStartDate(transactionHistoryRequest.getFromDate());
                searchData.setEndDate(transactionHistoryRequest.getToDate());

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

                List<TsTransaction> collectedTransactionList = transactionRepository
                        .findByTransactionTypeInAndTransactionDateBetweenAndTransactionStatusNotIn(transactionHistoryRequest.getTransactionTypes(),
                                startDate, endDate, txFailedStatus);

                if (transactionHistoryRequest.getUserTypes() != null && transactionHistoryRequest.getUserTypes().size() > 0) {

                    List<TsTransaction> filteredCollectedTransactionList = new ArrayList<>();
                    for (TsTransaction tsTransaction : collectedTransactionList
                    ) {

                        boolean isTransactionInValidForQuery = false;
                        if (tsTransaction.getSenderWallet() != null) {

                            String senderGroupCode = new CustomQuery(entityManager).getGroupCodeByWalletNo(tsTransaction.getSenderWallet());

                            if (senderGroupCode != null && transactionHistoryRequest.getUserTypes().contains(senderGroupCode)) {
                                isTransactionInValidForQuery = true;
                            }
                        }

                        if (!isTransactionInValidForQuery) {
                            if (tsTransaction.getReceiverWallet() != null) {

                                String receiverGroupCode = new CustomQuery(entityManager).getGroupCodeByWalletNo(tsTransaction.getReceiverWallet());

                                if (receiverGroupCode != null && transactionHistoryRequest.getUserTypes().contains(receiverGroupCode)) {
                                    isTransactionInValidForQuery = true;
                                }
                            }
                        }

                        if (isTransactionInValidForQuery) {
                            filteredCollectedTransactionList.add(tsTransaction);
                        }

                    }
                    transactionList = filteredCollectedTransactionList;
                } else {
                    transactionList = collectedTransactionList;
                }

                break;

            case PocketFilterConstants.MOBILE_TX_TYPES_WITH_DATE_RANGE:

                searchData.setSearchedBy("Transaction Types:" + getTransactionTypes(transactionHistoryRequest.getTransactionTypes()));

                if (transactionHistoryRequest.getMobileNumber() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Mobile number required");
                    throw new PocketException(PocketErrorCode.MobileNumberRequired);
                }


                if (transactionHistoryRequest.getTransactionTypes() == null || transactionHistoryRequest.getTransactionTypes().size() == 0) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Transaction Type required");
                    throw new PocketException(PocketErrorCode.TransactionTypeRequired);
                }

                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }

                statusNotIn = new ArrayList<>();
                statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
                statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

                client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(transactionHistoryRequest.getMobileNumber(),
                        statusNotIn);


                if (client != null) {
                    searchData.setName(client.getFullName());
                    searchData.setName(client.getFullName());
                    searchData.setWalletNo(client.getWalletNo());
                    searchData.setStartDate(transactionHistoryRequest.getFromDate());
                    searchData.setEndDate(transactionHistoryRequest.getToDate());
                }


                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

                endDate = DateUtil.addDay(endDate, 1);

                searchData.setStartDate(transactionHistoryRequest.getFromDate());
                searchData.setEndDate(transactionHistoryRequest.getToDate());

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());


                transactionList = transactionRepository
                        .findByPayerOrPayeeTransactionTypeInAndTransactionDateBetweenAndTransactionStatusNotIn(
                                transactionHistoryRequest.getMobileNumber(),
                                transactionHistoryRequest.getTransactionTypes(),
                                startDate, endDate, txFailedStatus);

                break;

            case PocketFilterConstants.HSS_DATE_RANGE:
                searchData.setSearchedBy("HSS Date Range");

                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }

                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

                endDate = DateUtil.addDay(endDate, 1);

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

                searchData.setStartDate(transactionHistoryRequest.getFromDate());
                searchData.setEndDate(transactionHistoryRequest.getToDate());

                try {
                    transactionList = transactionRepository.findByCreatedDateBetweenAndTransactionStatusNotIn(startDate, endDate, txFailedStatus);
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;

            case PocketFilterConstants.PAYNET_DATE_RANGE:
                searchData.setSearchedBy("PAYNET DATE RANGE");

                if (transactionHistoryRequest.getFromDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "From date required");
                    throw new PocketException(PocketErrorCode.FromDateRequired);
                }
                if (transactionHistoryRequest.getToDate() == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "To date required");
                    throw new PocketException(PocketErrorCode.ToDateRequired);
                }

                startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
                endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

                endDate = DateUtil.addDay(endDate, 1);

                txFailedStatus = new ArrayList<>();
                txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
                txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

                searchData.setStartDate(transactionHistoryRequest.getFromDate());
                searchData.setEndDate(transactionHistoryRequest.getToDate());

                try {
                    transactionList = transactionRepository.findByCreatedDateBetweenAndTransactionStatusNotIn(startDate, endDate, txFailedStatus);
                }catch (Exception e){
                    e.printStackTrace();
                }

                break;

            default:
                throw new PocketException(PocketErrorCode.UnsupportedFilterType);
        }

        List<TransactionHistoryModel> transactionHistoryModels = new ArrayList<>();

        int totalNoOfTransaction = 0;
        BigDecimal totalTransactionAmount = BigDecimal.ZERO;

        if (transactionList != null && transactionList.size() > 0) {
            totalNoOfTransaction = transactionList.size();

            for (int i = 0; i < transactionList.size(); i++) {

                TsTransaction tsTransaction = transactionList.get(i);

                if (transactionHistoryRequest.getCountryCode() != null) {

                    String userWalletNumber = getWalletUser(tsTransaction);

                    UmUserInfo umUserInfo = userInfoRepository.findByLoginId(userWalletNumber);

                    if (umUserInfo == null) {
                        continue;
                    }

                    UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);
                    if (umUserDetails == null) {
                        continue;
                    }

                    if (umUserDetails.getCountryCode() == null || !umUserDetails.getCountryCode().equals(transactionHistoryRequest.getCountryCode())) {
                        continue;
                    }

                    if (transactionHistoryRequest.getStateList() != null && transactionHistoryRequest.getStateList().size() > 0) {

                        if (umUserDetails.getStateId() == null || !transactionHistoryRequest.getStateList().contains(umUserDetails.getStateId())) {
                            continue;
                        }

                    }

                }

                totalTransactionAmount = totalTransactionAmount.add(tsTransaction.getTransactionAmount());

                TransactionHistoryModel transactionHistoryModel = new TransactionHistoryModel();
                transactionHistoryModel.setTransactionId("" + tsTransaction.getId());
                transactionHistoryModel.setTransactionToken(tsTransaction.getToken());
                transactionHistoryModel.setAmount(tsTransaction.getTransactionAmount());
                transactionHistoryModel.setFeeAmount(tsTransaction.getFeeAmount());
                transactionHistoryModel.setFeePayer(tsTransaction.getFeePayer());
                transactionHistoryModel.setRefTransactionToken(tsTransaction.getRefTransactionToken());


                if (tsTransaction.getTransactionType().equalsIgnoreCase(TsEnums.Services.Master_Wallet_Deposit.getServiceCode())) {
                    transactionHistoryModel.setPayeeWallet(tsTransaction.getLogicalReceiver());

                    TsBanks tsBanks = tsBankRepository.findFirstByBankCode(tsTransaction.getLogicalSender());
                    if (tsBanks != null) {
                        transactionHistoryModel.setPayerWallet(tsBanks.getBankName());
                    } else {
                        transactionHistoryModel.setPayerWallet(tsTransaction.getLogicalSender());
                    }

                } else if (tsTransaction.getTransactionType().equalsIgnoreCase(TsEnums.Services.Commission.getServiceCode())) {

                    transactionHistoryModel.setPayeeWallet(tsTransaction.getLogicalReceiver());

                    TsGl tsGl = generalLedgerRepository.findFirstByGlCode(tsTransaction.getLogicalSender());
                    if (tsGl != null) {
                        transactionHistoryModel.setPayerWallet(tsGl.getGlName());
                    } else {
                        transactionHistoryModel.setPayerWallet(tsTransaction.getLogicalSender());
                    }

                } else {

                    transactionHistoryModel.setPayeeWallet(tsTransaction.getLogicalReceiver());
                    transactionHistoryModel.setPayerWallet(tsTransaction.getLogicalSender());
                }


                String txStatusName = TsEnums.TransactionStatus.getNameByValue(tsTransaction.getTransactionStatus());

                transactionHistoryModel.setTransactionStatus(txStatusName);

                String txType = TsEnums.Services.getServiceNameByCode(tsTransaction.getTransactionType());
                transactionHistoryModel.setTransactionType(txType);

                transactionHistoryModel.setTransactionDate(DateUtil.convertDate(tsTransaction.getCreatedDate()));
                transactionHistoryModel.setPreviousTransactionReference(tsTransaction.getRefTransactionToken());
                transactionHistoryModel.setNote(tsTransaction.getNotes());
                transactionHistoryModel.setDisputeable(tsTransaction.getIsDisputable());

                transactionHistoryModels.add(transactionHistoryModel);
            }
        }

        if (searchData != null) {
            searchData.setTotalNoOfTransaction(totalNoOfTransaction);
            searchData.setTotalTransactionAmount(totalTransactionAmount);
        }

        TransactionHistoryModelResponse transactionHistoryModelResponse = new TransactionHistoryModelResponse();
        transactionHistoryModelResponse.setTransactionHistoryModels(transactionHistoryModels);
        transactionHistoryModelResponse.setSearchData(searchData);

        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setData(transactionHistoryModelResponse);
        baseResponseObject.setStatus(PocketConstants.OK);

        return baseResponseObject;
    }

    private String getWalletUser(TsTransaction tsTransaction) {

        switch (tsTransaction.getTransactionType()) {
            case ServiceCodeConstants.Fund_Transfer:
            case ServiceCodeConstants.Top_Up:
            case ServiceCodeConstants.CashIn:
            case ServiceCodeConstants.BillPay:
            case ServiceCodeConstants.AdminCashOut:
            case ServiceCodeConstants.CashOutToBank:

                return tsTransaction.getSenderWallet();

            case ServiceCodeConstants.Merchant_Payment:
            case ServiceCodeConstants.CashOut:
            case ServiceCodeConstants.AdminCashIn:
            case ServiceCodeConstants.CashInFromBank:

                return tsTransaction.getReceiverWallet();
            default:
                return tsTransaction.getReceiverWallet();
        }
    }

    private String getTransactionTypes(List<String> transactionTypes) {

        if (transactionTypes == null || transactionTypes.size() == 0) {
            return "No Type Selected";
        }

        String txTypes = "";

        for (int i = 0; i < transactionTypes.size(); i++) {
            String type = transactionTypes.get(i);
            TsService tsService = tsServiceRepository.findFirstByServiceCode(type);
            if (tsService == null) {
                continue;
            }
            txTypes += tsService.getDescription();

            if (i < transactionTypes.size() - 1) {
                txTypes += ", ";
            }

        }

        return txTypes + " ";
    }

    @Override
    public List<TransactionHistoryModel> getTransactionSummeryDetails(GetTransactionSummeryDetailsRequest transactionHistoryRequest, TsSessionObject umSessionResponse) throws PocketException {

        Date startDate = Date.valueOf(transactionHistoryRequest.getFromDate());
        Date endDate = Date.valueOf(transactionHistoryRequest.getToDate());

        endDate = DateUtil.addDay(endDate, 1);


        List<String> txFailedStatus = new ArrayList<>();
        txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
        txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

        String service = String.valueOf(transactionHistoryRequest.getTransactionType());

        List<TsTransaction> transactionList;

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(transactionHistoryRequest.getMobileNumber(), statusNotIn);


//        TsClient client = clientRepository.findByWalletNo(transactionHistoryRequest.getMobileNumber());

        if (client == null) {
            logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Account not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        List<String> serviceList = new ArrayList<>();
        serviceList.add(service);
        transactionList = transactionRepository
                .findByTransactionTypeAndTransactionDateBetweenAndPayerIdOrPayeeIdAndTransactionStatusNotInAll(serviceList,
                        startDate, endDate, client.getId(),
                        client.getId(), txFailedStatus);

        List<TransactionHistoryModel> transactionHistoryModels = new ArrayList<>();
        if (transactionList != null && transactionList.size() > 0) {

            for (int i = 0; i < transactionList.size(); i++) {

                TsTransaction tsTransaction = transactionList.get(i);

                TransactionHistoryModel transactionHistoryModel = new TransactionHistoryModel();

                // TODO: 2019-07-21 Have to work for is debit

                boolean isDebit = getIsDebit(transactionHistoryRequest.getMobileNumber(), tsTransaction);

                transactionHistoryModel.setIs_debit(isDebit);

                transactionHistoryModel.setTransactionId("" + tsTransaction.getId());
                transactionHistoryModel.setTransactionToken(tsTransaction.getToken());
                transactionHistoryModel.setAmount(tsTransaction.getTransactionAmount());
                transactionHistoryModel.setFeeAmount(tsTransaction.getFeeAmount());
                transactionHistoryModel.setFeePayer(tsTransaction.getFeePayer());

                transactionHistoryModel.setPayeeWallet(tsTransaction.getLogicalReceiver());
                transactionHistoryModel.setPayerWallet(tsTransaction.getLogicalSender());

                String txStatusName = TsEnums.TransactionStatus.getNameByValue(tsTransaction.getTransactionStatus());

                transactionHistoryModel.setTransactionStatus(txStatusName);

                String txType = TsEnums.Services.getServiceNameByCode(tsTransaction.getTransactionType());
                transactionHistoryModel.setTransactionType(txType);

                transactionHistoryModel.setTransactionDate(tsTransaction.getCreatedDate().toString());
                transactionHistoryModel.setPreviousTransactionReference(tsTransaction.getRefTransactionToken());
                transactionHistoryModel.setNote(tsTransaction.getNotes());
                transactionHistoryModel.setDisputeable(tsTransaction.getIsDisputable());

                transactionHistoryModels.add(transactionHistoryModel);
            }
        }

        List<TransactionHistoryModel> finalResponse = new ArrayList<>();

        if (transactionHistoryModels.size() > 0) {
            for (TransactionHistoryModel historyModel : transactionHistoryModels) {

                if (transactionHistoryRequest.getTransactionCategory().equalsIgnoreCase("OUT") && historyModel.isIs_debit()) {
                    finalResponse.add(historyModel);
                } else if (transactionHistoryRequest.getTransactionCategory().equalsIgnoreCase("IN") && !historyModel.isIs_debit()) {
                    finalResponse.add(historyModel);
                }
            }
        }


        return finalResponse;
    }

    @Override
    public BaseResponseObject getCustomerTransactionLimit(GetCustomerLimitRequest transactionHistoryRequest, String mobileNumber) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(transactionHistoryRequest.getMobileNumber(), statusNotIn);


        //TsClient client=clientRepository.findByWalletNo(mobileNumber);
        if (client == null) {
            logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Account not found");
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        List<TsTransactionProfileLimitMapping> tsTransactionProfileLimitMapping =
                tsTransactionProfileLimitMappingRepository.findAllByTxProfileCodeOrderByIdDesc(client.getTransactionProfileCode());

        if (tsTransactionProfileLimitMapping == null) {
            logWriterUtility.error(transactionHistoryRequest.getRequestId(), "No limit found");
            throw new PocketException(PocketErrorCode.NoLimitFound);
        }

        Map<String, String> serviceLimitMap = new HashMap<>();

        for (TsTransactionProfileLimitMapping mapping : tsTransactionProfileLimitMapping) {
            if (!serviceLimitMap.containsKey(mapping.getServiceCode())) {
                serviceLimitMap.put(mapping.getServiceCode(), mapping.getLimitCode());
            }
        }

        List<LimitListItem> finalLimitList = new ArrayList<>();

        for (Map.Entry<String, String> item : serviceLimitMap.entrySet()) {

            TsService tsService = tsServiceRepository.findFirstByServiceCode(item.getKey());
            if (tsService == null) {
                logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Invalid service code :" + item.getKey());
                continue;
            }

            TsLimit tsLimit = limitRepository.findFirstByLimitCodeAndStatusOrderByIdDesc(item.getValue(), "1");
            if (tsLimit == null) {
                logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Invalid limit code :" + item.getValue());
                continue;
            }

            LocalDate localdate = LocalDate.now();
            Date firstDay = Date.valueOf(localdate.withDayOfMonth(1));

            Date today = Date.valueOf(LocalDate.now());
            today = DateUtil.addDay(today, 1);

            List<ITsTransaction> transactionList = transactionRepository
                    .findAllByTransactionTypeAndSenderWalletDateBetween(item.getKey(), mobileNumber, firstDay, today);


            today = DateUtil.addDay(today, -1);

            LimitListItem limitListItem = new LimitListItem();
            limitListItem.setServiceName(tsService.getDescription());
            limitListItem.setLimitName(tsLimit.getLimitName());

            limitListItem.setDailyMaxCount(tsLimit.getDailyMaxNoOfTransaction());
            limitListItem.setDailyMaxAmount(tsLimit.getDailyMaxTransactionAmount());

            limitListItem.setMonthlyMaxCount(tsLimit.getMontlyMaxNoOfTransaction());
            limitListItem.setMonthlyMaxAmount(tsLimit.getMonthlyMaxTransactionAmount());

            if (transactionList == null || transactionList.size() == 0) {
                limitListItem.setCurrentDailyTransactionCount(0);
                limitListItem.setCurrentDailyTransactionAmount(0);

                limitListItem.setCurrentlyMonthlyTransactionCount(0);
                limitListItem.setCurrentMonthlyTransactionAmount(0);

            } else {

                //insert for 1 month
                //insert for 1 day

                int todayTxCount = 0;
                double todayTxAmount = 0;

                int monthlyTxCount = 0;
                double monthlyTxAmount = 0;

                for (ITsTransaction tsTransaction : transactionList) {

                    if (tsTransaction.getCreatedDate().compareTo(new java.util.Date(today.getTime())) > 0) {
                        todayTxCount += 1;

                        todayTxAmount += tsTransaction.getTransactionAmount().doubleValue();
                        monthlyTxAmount += tsTransaction.getTransactionAmount().doubleValue();

                        monthlyTxCount += 1;
                    } else {
                        monthlyTxCount += 1;
                        monthlyTxAmount += tsTransaction.getTransactionAmount().doubleValue();
                    }

                }

                limitListItem.setCurrentDailyTransactionCount(todayTxCount);
                limitListItem.setCurrentDailyTransactionAmount(todayTxAmount);

                limitListItem.setCurrentlyMonthlyTransactionCount(monthlyTxCount);
                limitListItem.setCurrentMonthlyTransactionAmount(monthlyTxAmount);

            }


            finalLimitList.add(limitListItem);

        }


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(finalLimitList);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getTopUpReport(TopUpReportRequest transactionHistoryRequest) throws PocketException {

        Date startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
        Date endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

        endDate = DateUtil.addDay(endDate, 1);

        List<String> txFailedStatus = new ArrayList<>();
        txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
        txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

        String service = String.valueOf(TsEnums.Services.Top_Up.getServiceCode());

        List<TsTransaction> transactionList;

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());


        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(transactionHistoryRequest.getMobileNumber(), clientStatus);

        SearchData searchData = new SearchData();

        if (client != null) {

            searchData.setSearchedBy("Mobile Number");
            searchData.setName(client.getFullName());
            searchData.setWalletNo(client.getWalletNo());
            searchData.setStartDate(transactionHistoryRequest.getFromDate());
            searchData.setEndDate(transactionHistoryRequest.getToDate());

            TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
            if (clientBalance != null) {
                searchData.setCurrentBalance(clientBalance.getBalance());
            }

        } else {
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }


        if (transactionHistoryRequest.getMobileNumber() == null || transactionHistoryRequest.getMobileNumber().trim().length() == 0) {

            if (transactionHistoryRequest.getOperatorList() == null || transactionHistoryRequest.getOperatorList().size() == 0) {
                transactionList = transactionRepository
                        .findAllByTransactionTypeAndDateBetween(service,
                                startDate, endDate, txFailedStatus);
            } else {
                transactionList = transactionRepository
                        .findAllByTransactionTypeAndDateBetweenAndSkuCodes(service,
                                startDate, endDate, txFailedStatus, transactionHistoryRequest.getOperatorList());
            }


        } else {


            if (transactionHistoryRequest.getOperatorList() == null || transactionHistoryRequest.getOperatorList().size() == 0) {
                transactionList = transactionRepository
                        .findAllByTransactionTypeAndSenderWalletDateBetweenAndStatusNotIn(service,
                                transactionHistoryRequest.getMobileNumber(),
                                startDate, endDate, txFailedStatus);
            } else {
                transactionList = transactionRepository
                        .findAllByTransactionTypeAndSenderWalletDateBetweenAndStatusNotInAndSkuCodeIn(service,
                                transactionHistoryRequest.getMobileNumber(),
                                startDate, endDate, txFailedStatus, transactionHistoryRequest.getOperatorList());
            }
        }

        int totalNoOfTransaction = transactionList.size();
        BigDecimal totalTransactionAmount = BigDecimal.ZERO;


        List<TransactionHistoryModel> transactionHistoryModels = new ArrayList<>();
        if (transactionList != null && transactionList.size() > 0) {

            for (int i = 0; i < transactionList.size(); i++) {

                TsTransaction tsTransaction = transactionList.get(i);

                totalTransactionAmount = totalTransactionAmount.add(tsTransaction.getTransactionAmount());


                TransactionHistoryModel transactionHistoryModel = new TransactionHistoryModel();

                // TODO: 2019-07-21 Have to work for is debit

                boolean isDebit = getIsDebit(transactionHistoryRequest.getMobileNumber(), tsTransaction);

                transactionHistoryModel.setIs_debit(isDebit);

                transactionHistoryModel.setTransactionId("" + tsTransaction.getId());
                transactionHistoryModel.setTransactionToken(tsTransaction.getToken());
                transactionHistoryModel.setAmount(tsTransaction.getTransactionAmount());
                transactionHistoryModel.setFeeAmount(tsTransaction.getFeeAmount());
                transactionHistoryModel.setFeePayer(tsTransaction.getFeePayer());

                transactionHistoryModel.setPayeeWallet(tsTransaction.getLogicalReceiver());
                transactionHistoryModel.setPayerWallet(tsTransaction.getLogicalSender());

                String txStatusName = TsEnums.TransactionStatus.getNameByValue(tsTransaction.getTransactionStatus());

                transactionHistoryModel.setTransactionStatus(txStatusName);

                String txType = TsEnums.Services.getServiceNameByCode(tsTransaction.getTransactionType());
                transactionHistoryModel.setTransactionType(txType);

                transactionHistoryModel.setTransactionDate(DateUtil.convertDate(tsTransaction.getCreatedDate()));
                transactionHistoryModel.setPreviousTransactionReference(tsTransaction.getRefTransactionToken());
                transactionHistoryModel.setNote(tsTransaction.getNotes());
                transactionHistoryModel.setDisputeable(tsTransaction.getIsDisputable());

                transactionHistoryModels.add(transactionHistoryModel);
            }
        }

        if (searchData != null) {
            searchData.setTotalNoOfTransaction(totalNoOfTransaction);
            searchData.setTotalTransactionAmount(totalTransactionAmount);
        }

        TopUpTransactionHistoryModelResponse topUpTransactionHistoryModelResponse = new TopUpTransactionHistoryModelResponse();
        topUpTransactionHistoryModelResponse.setSearchData(searchData);
        topUpTransactionHistoryModelResponse.setTransactionHistoryModels(transactionHistoryModels);


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setData(topUpTransactionHistoryModelResponse);

        return baseResponseObject;

    }

    @Override
    public BaseResponseObject filterTopUpReport(FilterTopUpReportRequest transactionHistoryRequest) throws PocketException {

        Date startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
        Date endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());

        endDate = DateUtil.addDay(endDate, 1);

        List<String> txFailedStatus = new ArrayList<>();
        txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
        txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

        String service = String.valueOf(TsEnums.Services.Top_Up.getServiceCode());

        List<TsTransaction> transactionList;

        if (transactionHistoryRequest.getOperatorList() == null || transactionHistoryRequest.getOperatorList().size() == 0) {

            if (transactionHistoryRequest.getMobileNumber() != null && transactionHistoryRequest.getMobileNumber().trim().length() > 0) {
                transactionList = transactionRepository
                        .findAllByTransactionTypeAndDateBetweenAndCountryCodeAndMobileNumber(service,
                                startDate, endDate, txFailedStatus, transactionHistoryRequest.getCountryCodes(), transactionHistoryRequest.getMobileNumber());

            } else {
                transactionList = transactionRepository
                        .findAllByTransactionTypeAndDateBetweenAndCountryCode(service,
                                startDate, endDate, txFailedStatus, transactionHistoryRequest.getCountryCodes());
            }


        } else {

            if (transactionHistoryRequest.getMobileNumber() != null && transactionHistoryRequest.getMobileNumber().trim().length() > 0) {
                transactionList = transactionRepository
                        .findAllByTransactionTypeDateBetweenAndStatusNotInAndSkuCodeInAndTopupReceiverCountryCodeInAndMobileNo(service,
                                startDate, endDate, txFailedStatus, transactionHistoryRequest.getOperatorList(),
                                transactionHistoryRequest.getCountryCodes(), transactionHistoryRequest.getMobileNumber());

            } else {
                List<String> txTypes = new ArrayList<>();
                txTypes.add(service);
//                transactionList = transactionRepository
//                        .filterTopUpReports(txTypes,txFailedStatus,transactionHistoryRequest.getOperatorList(),
//                                startDate, endDate, transactionHistoryRequest.getStateList());
                transactionList = transactionRepository
                        .filterTopUpReportsByOperatorName(txTypes, txFailedStatus, transactionHistoryRequest.getOperatorList(),
                                startDate, endDate, transactionHistoryRequest.getStateList());

            }

        }

        SearchData searchData = new SearchData();
        searchData.setSearchedBy("Country codes: " + transactionHistoryRequest.getCountryCodes());
        searchData.setStartDate(transactionHistoryRequest.getFromDate());
        searchData.setEndDate(transactionHistoryRequest.getToDate());


        int totalNoOfTransaction = transactionList.size();
        BigDecimal totalTransactionAmount = BigDecimal.ZERO;


        List<TransactionHistoryModel> transactionHistoryModels = new ArrayList<>();
        if (transactionList != null && transactionList.size() > 0) {

            for (int i = 0; i < transactionList.size(); i++) {

                TsTransaction tsTransaction = transactionList.get(i);

                if (transactionHistoryRequest.getStateList() != null && transactionHistoryRequest.getStateList().size() > 0) {

                    List<String> clientStatus = new ArrayList<>();
                    clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());

                    UmUserInfo senderInfo = userInfoRepository.findByLoginId(tsTransaction.getSenderWallet());

                    if (senderInfo == null) {
                        logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Sender client not found");
                        throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
                    }

                    UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(senderInfo);

                    if (umUserDetails == null) {
                        logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Sender client details not found");
                        throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
                    }

                    if (umUserDetails.getStateId() == null || umUserDetails.getStateId().trim().equals("")) {
                        continue;
                    }

                    if (!transactionHistoryRequest.getStateList().contains("" + umUserDetails.getStateId())) {
                        continue;
                    }
                }


                totalTransactionAmount = totalTransactionAmount.add(tsTransaction.getTransactionAmount());


                TransactionHistoryModel transactionHistoryModel = new TransactionHistoryModel();

                boolean isDebit = getIsDebit(transactionHistoryRequest.getMobileNumber(), tsTransaction);

                transactionHistoryModel.setIs_debit(isDebit);

                transactionHistoryModel.setTransactionId("" + tsTransaction.getId());
                transactionHistoryModel.setTransactionToken(tsTransaction.getToken());
                transactionHistoryModel.setAmount(tsTransaction.getTransactionAmount());
                transactionHistoryModel.setFeeAmount(tsTransaction.getFeeAmount());
                transactionHistoryModel.setFeePayer(tsTransaction.getFeePayer());

                transactionHistoryModel.setPayeeWallet(tsTransaction.getLogicalReceiver());
                transactionHistoryModel.setPayerWallet(tsTransaction.getLogicalSender());

                String txStatusName = TsEnums.TransactionStatus.getNameByValue(tsTransaction.getTransactionStatus());

                transactionHistoryModel.setTransactionStatus(txStatusName);

                String txType = TsEnums.Services.getServiceNameByCode(tsTransaction.getTransactionType());
                transactionHistoryModel.setTransactionType(txType);

                transactionHistoryModel.setTransactionDate(DateUtil.convertDate(tsTransaction.getCreatedDate()));
                transactionHistoryModel.setPreviousTransactionReference(tsTransaction.getRefTransactionToken());
                transactionHistoryModel.setNote(tsTransaction.getNotes());
                transactionHistoryModel.setDisputeable(tsTransaction.getIsDisputable());

                transactionHistoryModels.add(transactionHistoryModel);
            }
        }

        if (searchData != null) {
            searchData.setTotalNoOfTransaction(totalNoOfTransaction);
            searchData.setTotalTransactionAmount(totalTransactionAmount);
        }

        TopUpTransactionHistoryModelResponse topUpTransactionHistoryModelResponse = new TopUpTransactionHistoryModelResponse();
        topUpTransactionHistoryModelResponse.setSearchData(searchData);
        topUpTransactionHistoryModelResponse.setTransactionHistoryModels(transactionHistoryModels);


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        baseResponseObject.setData(topUpTransactionHistoryModelResponse);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getStateWiseUser(GetStateWiseUserRequest transactionHistoryRequest,
                                               TsSessionObject umSessionResponse) throws PocketException {

        UmCountry country = countryRepository.findAllByCountryCodeAndStatus(transactionHistoryRequest.getCountryCode(), "1");

        if (country == null) {
            logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Invalid country code :" + transactionHistoryRequest.getCountryCode());
            throw new PocketException(PocketErrorCode.InvalidCountryCode);
        }


        java.sql.Date startDate = null;

        java.sql.Date endDate = null;

        List<UserListItem> userList;

        if (transactionHistoryRequest.getFromDate() == null || transactionHistoryRequest.getToDate() == null) {
            userList = new CustomQuery(entityManager)
                    .getUserListByUserTypeAndCountryCodeAndStateId(transactionHistoryRequest.getUserType(),
                            transactionHistoryRequest.getCountryCode(), transactionHistoryRequest.getStateId(),
                            transactionHistoryRequest.getRequestId(), startDate, endDate);

        } else {
            startDate = DateUtil.parseDate(transactionHistoryRequest.getFromDate());
            endDate = DateUtil.parseDate(transactionHistoryRequest.getToDate());
            endDate = DateUtil.addDay(endDate, 1);

            userList = new CustomQuery(entityManager)
                    .getUserListByUserTypeAndCountryCodeAndStateId(transactionHistoryRequest.getUserType(),
                            transactionHistoryRequest.getCountryCode(), transactionHistoryRequest.getStateId(),
                            transactionHistoryRequest.getRequestId(), startDate, endDate);
        }

        List<UserListItem> finalResponse = new ArrayList<>();
        if (userList != null && userList.size() > 0) {


            for (UserListItem listResponse : userList
            ) {
                List<String> statusNotIn = new ArrayList<>();
                statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
                statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

                listResponse.setAccountStatus((listResponse.getAccountStatus()));
                listResponse.setGroupCode((listResponse.getGroupCode()));
                listResponse.setGroupName(UmEnums.UserGroup.getGroupNameByCode(listResponse.getGroupCode()));

                listResponse.setCreatedDate(DateUtil.convertDate(listResponse.getCreatedDate()));

                listResponse.setCountryCode(transactionHistoryRequest.getCountryCode());
                if (listResponse.getStateId() != null) {
                    UmState umState = stateRepository.findFirstById(Integer.parseInt(listResponse.getStateId()));
                    if (umState != null) {
                        listResponse.setStateName(umState.getStateName());
                    }
                }

                TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(listResponse.getMobileNumber(), statusNotIn);

                if (client != null) {
                    TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
                    if (clientBalance != null && clientBalance.getBalance() != null) {
                        listResponse.setWalletBalance(clientBalance.getBalance());
                    } else {
                        listResponse.setWalletBalance(BigDecimal.ZERO);
                    }
                } else {
                    listResponse.setWalletBalance(BigDecimal.ZERO);
                }

                UmUserInfo umUserInfo=userInfoRepository.findByLoginId(listResponse.getMobileNumber());

                if(umUserInfo!=null){
                    UmUserDetails umUserDetails=userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(umUserInfo);
                    if(umUserDetails!=null){
                        listResponse.setCountryCode(umUserDetails.getCountryCode());
                        listResponse.setNationality(umUserDetails.getNationality());
                        listResponse.setPermanentAddress(umUserDetails.getPermanentAddress());
                        listResponse.setPresentAddress(umUserDetails.getPresentAddress());
                        listResponse.setStateId(umUserDetails.getStateId());

                        UmState umState=umStateRepository.findFirstById(Integer.parseInt(umUserDetails.getStateId()));

                        if(umState!=null) {
                            listResponse.setStateName(umState.getStateName());
                        }

                    }

                }

                listResponse.setAccountStatusString(UmEnums.UserStatus.getStatusNameByStatus(listResponse.getAccountStatus()));

                UmUserGroup umUserGroup = userGroupRepository.findByGroupCodeAndGroupStatus(listResponse.getGroupCode(), "1");
                if (umUserGroup == null) {
                    logWriterUtility.error(transactionHistoryRequest.getRequestId(), "Invalid group code :" + listResponse.getGroupCode());
                } else {
                    listResponse.setGroupName(umUserGroup.getGroupName());
                }


                finalResponse.add(listResponse);
            }
        }


        logWriterUtility.trace(transactionHistoryRequest.getRequestId(), "User Size :" + userList.size());

        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        baseResponseObject.setData(finalResponse);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getUserListBySrWallet(GetUserListBySrWalletRequest srWalletRequest, TsSessionObject umSessionResponse) throws PocketException {

        UmUserInfo umSrInfo = userInfoRepository.findByLoginId(srWalletRequest.getSrWalletId());
        if (umSrInfo == null) {
            logWriterUtility.error(srWalletRequest.getRequestId(), "SR info not found :" + srWalletRequest.getSrWalletId());
            throw new PocketException(PocketErrorCode.SrWalletNotFound);
        }

        UmUserGroup umUserGroup = userGroupRepository.findByGroupCodeAndGroupStatus(srWalletRequest.getUserType(), "1");
        if (umUserGroup == null) {
            logWriterUtility.error(srWalletRequest.getRequestId(), "Invalid group code :" + srWalletRequest.getUserType());
            throw new PocketException(PocketErrorCode.GROUP_CODE_NOT_FOUND);
        }

        List<UmSrAgentMapping> userList = umSrAgentMappingRepository.findAllBySrIdAndStatus(umSrInfo.getId(), "1");

        BaseResponseObject baseResponseObject = new BaseResponseObject(srWalletRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        if (userList == null || userList.size() == 0) {
            logWriterUtility.trace(srWalletRequest.getRequestId(), "User Size :0");
            baseResponseObject.setData(null);
            return baseResponseObject;
        } else {
            logWriterUtility.trace(srWalletRequest.getRequestId(), "User Size :" + userList.size());

            List<UserListItem> userListItems = new ArrayList<>();

            for (UmSrAgentMapping umSrAgentMapping : userList) {

                UmUserInfo agentInfo = userInfoRepository.findFirstById(umSrAgentMapping.getAgentId());

                if (agentInfo == null) {
                    continue;
                }

                UmUserDetails umUserDetails = userDetailsRepository.findFirstByUmUserInfoByUserIdOrderByIdDesc(agentInfo);

                UserListItem userListItem = new UserListItem();
                userListItem.setId(agentInfo.getId());
                userListItem.setFullName(agentInfo.getFullName());
                userListItem.setMobileNumber(agentInfo.getMobileNumber());
                userListItem.setAccountStatus(agentInfo.getAccountStatus());
                userListItem.setAccountStatusString(UmEnums.UserStatus.getStatusNameByStatus(agentInfo.getAccountStatus()));
                userListItem.setEmail(agentInfo.getEmail());
                userListItem.setGroupName(umUserGroup.getGroupName());
                userListItem.setGroupCode(umUserGroup.getGroupCode());

                userListItem.setCountryCode(umUserDetails.getCountryCode());
                userListItem.setNationality(umUserDetails.getNationality());
                userListItem.setStateId(umUserDetails.getStateId());
                userListItem.setPresentAddress(umUserDetails.getPresentAddress());
                userListItem.setPermanentAddress(umUserDetails.getPermanentAddress());

                UmState umState = umStateRepository.findFirstById(Integer.parseInt(umUserDetails.getStateId()));
                if (umState != null) {
                    userListItem.setStateName(umState.getStateName());
                }


                List<String> statusNotIn = new ArrayList<>();
                statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
                statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

                TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(agentInfo.getMobileNumber(), statusNotIn);

                if (client != null) {
                    TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);
                    if (clientBalance != null && clientBalance.getBalance() != null) {
                        userListItem.setWalletBalance(clientBalance.getBalance());
                    } else {
                        userListItem.setWalletBalance(BigDecimal.ZERO);
                    }
                } else {
                    userListItem.setWalletBalance(BigDecimal.ZERO);
                }


                userListItems.add(userListItem);

            }
            baseResponseObject.setData(userListItems);
            return baseResponseObject;
        }
    }

    @Override
    public BaseResponseObject getGlList(EmptyRequest request) throws PocketException {

        List<TsFeeStakeholder> stakeholderList = (List<TsFeeStakeholder>) feeStakeholderRepository.findAll();

        Set<String> glCodes = new HashSet<>();

        for (TsFeeStakeholder stakeholder : stakeholderList
        ) {
            if (stakeholder.getGlCode() != null && stakeholder.getGlCode().trim().length() > 0) {
                glCodes.add(stakeholder.getGlCode());
            }
        }

        List<TsGl> glList = (List<TsGl>) generalLedgerRepository.findAll();


        for (TsGl tsGl : glList
        ) {
            glCodes.add(tsGl.getGlCode());
        }

        List<TsGl> glListTemp = generalLedgerRepository.findAllByGlCodeInAndParentGl(glCodes, false);

        List<TsGl> finalGlList = new ArrayList<>();

        if (glListTemp != null && glListTemp.size() > 0) {
            //BANK_OF_

            for (TsGl tsGl : glListTemp
            ) {
                /*if(tsGl.getGlName().contains("BANK_OF")){

                    TsBanks tsBanks=tsBankRepository.findFirstByGlCode(tsGl.getGlCode());
                    if(tsBanks!=null){
                        tsGl.setGlName(tsBanks.getBankName());
                    }
                }*/
                finalGlList.add(tsGl);
            }

        }


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(finalGlList);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getGlStatement(GetGlStatementRequest accountStatementRequest) throws PocketException {

        TsGl tsGl = generalLedgerRepository.findFirstByGlCode(accountStatementRequest.getGlCode());

        if (tsGl == null) {
            logWriterUtility.error(accountStatementRequest.getRequestId(), "Gl code not found :" + accountStatementRequest.getGlCode());
            throw new PocketException(PocketErrorCode.GlCodeNotFound);
        }
        logWriterUtility.trace(accountStatementRequest.getRequestId(), "Gl code found :" + tsGl.getGlName());

        Date startDate = Date.valueOf(accountStatementRequest.getFromDate());


        GlStatementResponse finalResponse = new GlStatementResponse();

        finalResponse.setGlCode(tsGl.getGlCode());
        finalResponse.setGlName(tsGl.getGlName());


        //Load current balance
        AccountStatementBalanceData accountStatementBalanceData = new AccountStatementBalanceData();

        BigDecimal glCurrentBalance = new CustomQuery(entityManager)
                .getGlBalanceUsingGlCode(accountStatementRequest.getGlCode());
        accountStatementBalanceData.setCurrentBalance(glCurrentBalance);

        finalResponse.setBalance(accountStatementBalanceData);

        // Get opening balance
//        Date tempStartDate=DateUtil.addDay(startDate,0);
        BigDecimal openingBalance = new CustomQuery(entityManager)
                .getGlBalanceUsingGlCodeAndDateRange(Long.parseLong(accountStatementRequest.getGlCode()),
                        null, startDate);
        accountStatementBalanceData.setStartAmount(openingBalance);


        // Load bounded transaction list
        startDate = DateUtil.addDay(startDate, 0);

        Date endDate = Date.valueOf(accountStatementRequest.getToDate());
        endDate = DateUtil.addDay(endDate, 1);


        List<GlStatementListItemData> boundedTransactionList = new CustomQuery(entityManager)
                .getTransactionsUsingGlCodeAndDateRange(Long.parseLong(accountStatementRequest.getGlCode()),
                        startDate, endDate, tsServiceRepository);

        List<GlStatementListItemData> finalTxList = new ArrayList<>();

        BigDecimal totalDebit = BigDecimal.ZERO;
        BigDecimal totalCredit = BigDecimal.ZERO;
        int count = 0;

        BigDecimal closingBalance = openingBalance;

        if (boundedTransactionList != null && boundedTransactionList.size() > 0) {
            for (GlStatementListItemData listItemData : boundedTransactionList
            ) {

                count++;
                TsTransaction tsTransaction = transactionRepository.findFirstByToken(listItemData.getTransaction_reference());

                listItemData.setPayerMobile(tsTransaction.getSenderWallet());
                listItemData.setPayeeMobile(tsTransaction.getReceiverWallet());
                listItemData.setDescription(tsTransaction.getNotes());

                if (accountStatementRequest.getGlCode().equalsIgnoreCase(tsTransaction.getSenderWallet())) {
                    listItemData.setBalance(tsTransaction.getSenderRunningBalance());
                    totalDebit = totalDebit.add(tsTransaction.getSenderDebitAmount());

                } else if (accountStatementRequest.getGlCode().equalsIgnoreCase(tsTransaction.getReceiverWallet())) {
                    listItemData.setBalance(tsTransaction.getReceiverRunningBalance());
                    totalCredit = totalCredit.add(tsTransaction.getReceiverCreditAmount());
                }

                BigDecimal depositAmount = listItemData.getDeposit_Amount();
                BigDecimal withdrawAmount = listItemData.getWithdraw_amount();

                if (depositAmount != null && depositAmount.compareTo(BigDecimal.ZERO) > 0) {
                    closingBalance = closingBalance.add(depositAmount);
                }
                if (withdrawAmount != null && withdrawAmount.compareTo(BigDecimal.ZERO) > 0) {
                    closingBalance = closingBalance.subtract(withdrawAmount);
                }


                finalTxList.add(listItemData);
            }
        }

        accountStatementBalanceData.setEndAmount(closingBalance);

        finalResponse.setCount(count);
        finalResponse.setTotalDebit(totalDebit);
        finalResponse.setTotalCredit(totalCredit);
        finalResponse.setTransactionList(finalTxList);

        BaseResponseObject baseResponseObject = new BaseResponseObject(accountStatementRequest.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(finalResponse);


        return baseResponseObject;
    }

    private boolean getIsDebit(String mobileNumber, TsTransaction tsTransaction) {

        if (tsTransaction.getSenderWallet().equals(mobileNumber)) {
            return true;
        }

        return false;
    }

    private AccountStatementListItemData getAccountStatementListItem(TsTransaction tsTransaction, String userMobileNymber,
                                                                     Boolean isForGl, TsServiceRepository tsServiceRepository) {
        AccountStatementListItemData accountStatementListItemData = new AccountStatementListItemData(userMobileNymber, tsTransaction, isForGl, tsServiceRepository);
        return accountStatementListItemData;
    }

    private TransactionDashboardListItem getTransactionDetails(TsEnums.Services transactionTypeName,
                                                               Date fromDate, Date toDate,
                                                               TransactionDashboardBalanceRequest aggregateBalanceRequest) {

        TsService cfeService = tsServiceRepository.findFirstByServiceCode(transactionTypeName.getServiceCode());

        String transactionType;
        if (cfeService == null) {
            transactionType = "Unknown";
        } else {
            transactionType = cfeService.getDescription();

        }

        if (transactionType == null || transactionType.equals("Unknown")) {
            return null;
        }

        logWriterUtility.trace(aggregateBalanceRequest.getRequestId(), "" + transactionTypeName.getServiceCode() + " " + transactionTypeName.name());
        logWriterUtility.trace(aggregateBalanceRequest.getRequestId(), "" + fromDate + " " + toDate);

        List<TsTransaction> transactionList = transactionRepository
                .findByTransactionByTxTypeAndTransactionDateBetween(transactionTypeName.getServiceCode(),
                        fromDate, toDate, null);

        TransactionDashboardListItem transactionDashboardListItem = new TransactionDashboardListItem();
        transactionDashboardListItem.setTransactionType(transactionType);
        transactionDashboardListItem.setTransactionCount(Long.parseLong("" + transactionList.size()));

        BigDecimal transactionSuccessCount = BigDecimal.ZERO;
        BigDecimal transactionFailureCount = BigDecimal.ZERO;
        BigDecimal transactionReversalCount = BigDecimal.ZERO;
        BigDecimal transactionVolume = BigDecimal.ZERO;
        BigDecimal successTransactionVolume = BigDecimal.ZERO;
        BigDecimal failureTransactionVolume = BigDecimal.ZERO;
        BigDecimal fee = BigDecimal.ZERO;

        List<Integer> transactionIdList = new ArrayList<>();

        for (int index = 0; index < transactionList.size(); index++) {
            TsTransaction tempTransaction = transactionList.get(index);
            transactionIdList.add(tempTransaction.getId());

            if (tempTransaction.getTransactionStatus().equalsIgnoreCase(""+TsEnums.TransactionStatus.COMPLETED.getTransactionStatus())||
                    tempTransaction.getTransactionStatus().equalsIgnoreCase(""+TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus())) {
                successTransactionVolume = successTransactionVolume.add(tempTransaction.getTransactionAmount());
            }

            if (tempTransaction.getTransactionAmount() != null) {
                transactionVolume = transactionVolume.add(tempTransaction.getTransactionAmount());
            }

            if (tempTransaction.getFeeAmount() != null) {
                fee = fee.add(tempTransaction.getFeeAmount());
            }

            if (tempTransaction.getTransactionStatus().equals(TsEnums.TransactionStatus.FAILED.getTransactionStatus()) ||
                    tempTransaction.getTransactionStatus().equals(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus())) {
                transactionFailureCount = transactionFailureCount.add(BigDecimal.ONE);
            } else {
                transactionSuccessCount = transactionSuccessCount.add(BigDecimal.ONE);
            }

            if (tempTransaction.getEodStatus() != null && tempTransaction.getEodStatus().equalsIgnoreCase(TsEnums.EodStatus.REVERSED.getEodStatus())) {
                if (transactionTypeName.getServiceCode().equals(TsEnums.Services.Reversal.getServiceCode())
                        && tempTransaction.getTransactionAmount() != null) {
                    transactionVolume = transactionVolume.subtract(tempTransaction.getTransactionAmount());
                    successTransactionVolume = successTransactionVolume.subtract(tempTransaction.getTransactionAmount());
                }

                if (transactionTypeName.getServiceCode().equals(TsEnums.Services.Reversal.getServiceCode()) && tempTransaction.getFeeAmount() != null) {
                    fee = fee.subtract(tempTransaction.getFeeAmount());
                }
                if (transactionTypeName.getServiceCode().equals(TsEnums.Services.Reversal.getServiceCode())) {
                    transactionReversalCount = transactionReversalCount.add(BigDecimal.ONE);
                }
            }
        }

        transactionDashboardListItem.setSuccessTransactionVolume(successTransactionVolume);
        transactionDashboardListItem.setTransactionVolume(transactionVolume);
        transactionDashboardListItem.setTransactionSuccessCount(transactionSuccessCount.longValue());
        transactionDashboardListItem.setTransactionFailourCount(transactionFailureCount.longValue());
        transactionDashboardListItem.setTransactionReversalCount(transactionReversalCount);
        transactionDashboardListItem.setFee(fee);
        return transactionDashboardListItem;
    }
}
