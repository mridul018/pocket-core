package tech.ipocket.pocket.services.ts.transaction;

import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface CashInOutService {
    BaseResponseObject doAgentCashIn(TransactionRequest transactionRequest, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject doCashInFromBank(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException;

    BaseResponseObject doCashOutToBank(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException;
}
