package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.TsBankBranchMapping;
import tech.ipocket.pocket.entity.TsBanks;
import tech.ipocket.pocket.entity.TsBranch;
import tech.ipocket.pocket.entity.TsGl;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.bank.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.BankData;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.utils.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BankServiceImpl implements BankService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsBankRepository tsBankRepository;
    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private TsBranchRepository tsBranchRepository;
    @Autowired
    private TsBankBranchMapRepository tsBankBranchMapRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(rollbackFor = {PocketException.class, Exception.class})
    @Override
    public BaseResponseObject createBank(CreateBankRequest request) throws PocketException {

        if (request.getId() > 0) {
            return updateBank(request);
        }

        TsBanks tsBanks = tsBankRepository.findFirstByBankName(request.getBankName());
        if (tsBanks != null) {
            logWriterUtility.error(request.getRequestId(), "bank name already exists");
            throw new PocketException(PocketErrorCode.BankAlreadyExists);
        }

        tsBanks = tsBankRepository.findFirstByGlCode(request.getBankGlCode());
        if (tsBanks != null) {
            logWriterUtility.error(request.getRequestId(), "bank gl already exists");
            throw new PocketException(PocketErrorCode.BankGlCodeExists);
        }

        tsBanks = tsBankRepository.findFirstByBankCode(request.getBankCode());
        if (tsBanks != null) {
            logWriterUtility.error(request.getRequestId(), "bank code already exists");
            throw new PocketException(PocketErrorCode.BankCodeExists);
        }

        TsGl tsGl = tsGlRepository.findFirstByGlCode(request.getBankGlCode());
        if (tsGl != null) {
            logWriterUtility.error(request.getRequestId(), "gl code already exists");
            throw new PocketException(PocketErrorCode.GlCodeAlreadyExists);
        }

        tsGl = new TsGl();
        tsGl.setSystemDefault(true);
        tsGl.setParentGlCode(GlConstants.BANK_PARENT_GL);
        tsGl.setGlCode(request.getBankGlCode());
        tsGl.setParentGl(false);
        tsGl.setGlName(request.getBankName());

        tsGlRepository.save(tsGl);

        tsBanks = new TsBanks();
        tsBanks.setBankName(request.getBankName());
        tsBanks.setBankCode(request.getBankCode());
        tsBanks.setGlCode(request.getBankGlCode());
        tsBanks.setRoutingNo(request.getBankRoutingNumber());
        tsBanks.setStatus("1");

        tsBankRepository.save(tsBanks);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateBank(CreateBankRequest request) throws PocketException {

        TsBanks tsBanks = tsBankRepository.findFirstById(request.getId());

        if (tsBanks == null) {
            logWriterUtility.error(request.getRequestId(), "Bank not found by id :" + request.getId());
            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        if (request.getBankCode() != null && !tsBanks.getBankCode().equalsIgnoreCase(request.getBankCode())) {
            TsBanks newTsBanks = tsBankRepository.findFirstByBankCode(request.getBankCode());
            if (newTsBanks != null) {
                logWriterUtility.error(request.getRequestId(), "bank code already exists :" + request.getBankCode());
                throw new PocketException(PocketErrorCode.BankCodeExists);
            }
            tsBanks.setBankCode(request.getBankCode());
        }

        if(request.getBankStatus()!=null){
            boolean isStatusOk = (request.getBankStatus().equals("0") || request.getBankStatus().equals("1"));

            if (!isStatusOk) {
                logWriterUtility.error(request.getRequestId(), "Invalid bank status.");
                throw new PocketException(PocketErrorCode.InvalidBankStatus);
            }

            tsBanks.setStatus(request.getBankStatus());
        }

        if (request.getBankName() != null) {
            tsBanks.setBankName(request.getBankName());
        }

        if (request.getBankRoutingNumber() != null) {
            tsBanks.setRoutingNo(request.getBankRoutingNumber());
        }

        tsBanks.setStatus(request.getBankStatus());
        tsBankRepository.save(tsBanks);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getBanks(EmptyRequest request) {

        List<TsBanks> banks = (List<TsBanks>) tsBankRepository.findAllByStatus("1");

        List<BankData> bankDataList = new ArrayList<>();
        if (banks.size() > 0) {
            for (TsBanks tsBanks : banks
            ) {
                BankData bankData = new BankData(tsBanks);

                BigDecimal bankBalance = new CustomQuery(entityManager).getGlBalanceUsingGlCode(tsBanks.getGlCode());
                bankData.setBalance(bankBalance);

                bankDataList.add(bankData);
            }

        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(bankDataList);
        return baseResponseObject;
    }


    @Transactional(rollbackFor = {PocketException.class, Exception.class})
    @Override
    public BaseResponseObject createBranch(CreateBranchRequest request) throws PocketException {


        if(request.getId()>0){
            UpdateBranchRequest updateBranchRequest=new UpdateBranchRequest();
            updateBranchRequest.setBankCode(request.getBankCode());
            updateBranchRequest.setBranchStatus(request.getBranchStatus());
            updateBranchRequest.setBranchSwiftCode(request.getBranchSwiftCode());
            updateBranchRequest.setId(request.getId());
            return updateBranch(updateBranchRequest);
        }

        TsBranch tsBranch = tsBranchRepository.findFirstBySwiftCodeAndStatus(request.getBranchSwiftCode(), "1");
        if (tsBranch != null) {
            logWriterUtility.error(request.getRequestId(), "Branch already exists.");
            throw new PocketException(PocketErrorCode.BranchAlreadyExists);
        }

        TsBanks tsBanks = tsBankRepository.findFirstByBankCode(request.getBankCode());
        if (tsBanks == null) {
            logWriterUtility.error(request.getRequestId(), "Bank not exists.");
            throw new PocketException(PocketErrorCode.BankNotFound);
        }

        tsBranch = new TsBranch();
        tsBranch.setBranchName(request.getBranchName());
        tsBranch.setSwiftCode(request.getBranchSwiftCode());
        tsBranch.setStatus("1");

        tsBranchRepository.save(tsBranch);

        TsBankBranchMapping tsBankBranchMapping = new TsBankBranchMapping();
        tsBankBranchMapping.setBankCode(tsBanks.getBankCode());
        tsBankBranchMapping.setBranchSwiftCode(tsBranch.getSwiftCode());
        tsBankBranchMapping.setStatus("1");

        tsBankBranchMapRepository.save(tsBankBranchMapping);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getBranchList(GetBranchByBankCodeRequest request) {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        List<TsBankBranchMapping> bankBranchMappings = tsBankBranchMapRepository
                .findAllByBankCodeAndStatus(request.getBankCode(), "1");

        if (bankBranchMappings == null) {
            logWriterUtility.trace(request.getRequestId(), "no branch found");
            baseResponseObject.setData(null);
            return baseResponseObject;
        }

        List<String> swiftCodes = bankBranchMappings.stream()
                .map(TsBankBranchMapping::getBranchSwiftCode)
                .collect(Collectors.toList());

        List<TsBranch> branchList = tsBranchRepository.findAllBySwiftCodeIn(swiftCodes);
        baseResponseObject.setData(branchList);
        return baseResponseObject;
    }

    @Override
    public BaseResponseObject updateBranch(UpdateBranchRequest request) throws PocketException {


        Optional<TsBranch> optBranch=tsBranchRepository.findById(request.getId());

        if(!optBranch.isPresent()){
            logWriterUtility.error(request.getRequestId(), "Branch not found.");
            throw new PocketException(PocketErrorCode.BranchNotFound);
        }

        TsBranch tsBranch = optBranch.get();


        if(request.getBranchStatus()!=null){
            boolean isStatusOk = (request.getBranchStatus().equals("0")
                    || request.getBranchStatus().equals("1")
                    || request.getBranchStatus().equalsIgnoreCase("InActive")
                    || request.getBranchStatus().equalsIgnoreCase("Active"));

            if (!isStatusOk) {
                logWriterUtility.error(request.getRequestId(), "Invalid branch status.");
                throw new PocketException(PocketErrorCode.InvalidBranchStatus);
            }


            TsBankBranchMapping tsBankBranchMapping = tsBankBranchMapRepository
                    .findFirstByBankCodeAndBranchSwiftCode(request.getBankCode(), tsBranch.getSwiftCode());

            if (tsBankBranchMapping == null) {
                logWriterUtility.error(request.getRequestId(), "Bank branch map not found");
                throw new PocketException(PocketErrorCode.BankBranchMapNotFound);
            }


            if(request.getBranchSwiftCode()!=null && !request.getBranchSwiftCode().equalsIgnoreCase(tsBranch.getSwiftCode())){
                tsBranch.setSwiftCode(request.getBranchSwiftCode());
                tsBankBranchMapping.setBranchSwiftCode(request.getBranchSwiftCode());
            }

            if(request.getBranchStatus().equalsIgnoreCase("Active")||request.getBranchStatus().equalsIgnoreCase("1")){
                tsBankBranchMapping.setStatus("1");
            }else if(request.getBranchStatus().equalsIgnoreCase("InActive")||request.getBranchStatus().equalsIgnoreCase("0")){
                tsBankBranchMapping.setStatus("0");
            }

            tsBranchRepository.save(tsBranch);
            tsBankBranchMapRepository.save(tsBankBranchMapping);
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        return baseResponseObject;
    }
}
