package tech.ipocket.pocket.services.ts.transaction;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.configuration.PayWellConfig;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.ReversalRequest;
import tech.ipocket.pocket.request.ts.transaction.BillPayRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.transaction.TransactionResponseData;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.services.ts.irregular_transaction.IrregularTransactionService;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.BillPayTypeConstants;
import tech.ipocket.pocket.utils.constants.CurrencyCode;
import tech.ipocket.pocket.utils.constants.TopUpMedium;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class BillPayServiceImpl implements BillPayService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;

    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TsServiceRepository serviceRepository;

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private TsMerchantDetailsRepository tsMerchantDetailsRepository;
    @Autowired
    private AppConfiguration appConfiguration;
    @Autowired
    PayWellConfig payWellConfig;
    @Autowired
    private TsConversionRateRepository tsConversionRateRepository;
    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;

    @Autowired
    private IrregularTransactionService irregularTransactionService;

    @Override
    @Transactional(rollbackFor = {PocketException.class,Exception.class})
    public BaseResponseObject doBillPayPallyBiddut(BillPayRequest request, TsSessionObject tsSessionObject) throws PocketException {


        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
//        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientSender = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsSessionObject.getMobileNumber(),
                statusNotIn);

        if (clientSender == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        TsMerchantDetails tsMerchantDetails=tsMerchantDetailsRepository.findFirstByShortCode(PocketConstants.PALLI_BIDYUT);

        if(tsMerchantDetails==null){
            logWriterUtility.error(request.getRequestId(),"PALLE_BIDYUT Merchant not found");
            throw new PocketException(PocketErrorCode.PallyBidyutMerchantNotFound);
        }

        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(tsMerchantDetails.getWalletNo(),
                statusNotIn);

        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.BillPay);

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //calculate fee
        FeeData feeData = null;

        String conversionFeeCode=null;


        if (request.getSource() == null || request.getSource().equalsIgnoreCase(TopUpMedium.CUSTOMER_APP)) {
            feeData = feeManager.calculateFeeByFeeCode( TsEnums.FeeCode.BILLPAY_PALLI_BIDDUT_BD_CUSTOMER_APP.getFeeCode(),
                    new BigDecimal(request.getTransferAmount()), request.getRequestId());
            conversionFeeCode=TsEnums.FeeCode.BILLPAY_CONVERSION_DISTRIBUTION_CUSTOMER_APP.getFeeCode();

        } else {

            feeData = feeManager.calculateFeeByFeeCode(  TsEnums.FeeCode.BILLPAY_PALLI_BIDDUT_BD_AGENT_APP.getFeeCode(),
                    new BigDecimal(request.getTransferAmount()), request.getRequestId());

            conversionFeeCode=TsEnums.FeeCode.BILLPAY_CONVERSION_DISTRIBUTION_AGENT_APP.getFeeCode();

        }

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");

            feeData=new FeeData();
            feeData.setFeeAmount(BigDecimal.ZERO);
            feeData.setFeeCode(null);
            feeData.setFeePayer("dr");
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransferAmount()));

        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");

        //transaction profile validation for sender + check balance limit for sender
        Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSender, cfeService,
                transactionAmountData.getSenderDebitAmount(),
                request.getRequestId());
        if (!isSenderProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
            throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
        }

        //transaction profile validation for receiver
        Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(clientReceiver, cfeService,
                request.getRequestId());
        if (!isReceiverProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
            throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransactionBillPay(clientSender, clientReceiver,
                cfeService.getServiceCode(),
                feeData, transactionAmountData);

        tsTransactionRepository.save(cfeTransaction);

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();

        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.DEBIT,
                clientSender.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                clientReceiver.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(clientSender);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found");
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }


        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                senderGlName, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(clientReceiver);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_BillPay, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        tsGlTransactionDetailsRepository.saveAll(glTxDetailList);


        // update balance
        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSender, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(clientReceiver, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
        cfeTransaction.setSenderRunningBalance(senderRunningBalance);
        cfeTransaction.setReceiverRunningBalance(receiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        BigInteger convertedAmount = new BigInteger(""+request.getTransferAmount().intValue());
        BigDecimal pocketConversionProfit = BigDecimal.ZERO;


        try {

            BigInteger receiverCreditAmount=new BigInteger(""+transactionAmountData.getReceiverCreditAmount().intValue());

            BigInteger actualBillAmount= BigInteger.valueOf(0);

            BaseResponseObject baseResponseObject=null;

            if(request.getCurrencyCode()!=null&&request.getCurrencyCode().equalsIgnoreCase(CurrencyCode.AED)){
                Double actualConvertedAmount = (double) 0;

                TsConversionRate tsConversionRate = tsConversionRateRepository
                        .findFirstByServiceCodeAndStatusOrderByIdDesc(BillPayTypeConstants.PALLI_BIDYUT, "1");
                if(tsConversionRate!=null){

                    Double bill=request.getTransferAmount()*tsConversionRate.getConversionRate();

                    actualBillAmount= BigInteger.valueOf(bill.intValue());

                    convertedAmount=new BigDecimal(tsConversionRate.getConversionRate())
                            .multiply(new BigDecimal(receiverCreditAmount)).toBigInteger();

                    Double actualAmount = (transactionAmountData.getReceiverCreditAmount().doubleValue() * tsConversionRate.getActualRate());
                    actualConvertedAmount = Double.valueOf(actualAmount.toString());

                }else{
                    convertedAmount=receiverCreditAmount.multiply(BigInteger.valueOf(20));
                    actualConvertedAmount = Double.valueOf(convertedAmount.toString());

                    Double bill=request.getTransferAmount()*20;

                    actualBillAmount= BigInteger.valueOf(bill.intValue());


                }

                if (tsConversionRate != null) {
                    Double value = (actualConvertedAmount - Double.valueOf("" + convertedAmount)) / tsConversionRate.getActualRate();
                    pocketConversionProfit = new BigDecimal(value);
                }

               /* if(true){

                    baseResponseObject=new BaseResponseObject(request.getRequestId());
                    baseResponseObject.setError(null);
                    baseResponseObject.setStatus("200");
                }else {
                    baseResponseObject=new ExternalCall(appConfiguration.getExternalUrl())
                            .sendPayWellPollibudditBill(
                                    payWellConfig.getClientId(),
                                    payWellConfig.getClientPassword(),
                                    request.getBillNumber(),
                                    actualBillAmount,
                                    cfeTransaction.getToken(),
                                    ExternalType.POLLIBUDDIT_BILLPAY,
                                    request.getAccountNumber(),
                                    request.getMobileNumber(),
                                    request.getLogedInUserMobileNumber(),
                                    request.getFullName());
                }*/
//                version 2.1
//                baseResponseObject=new ExternalCall(appConfiguration.getExternalUrl())
//                        .sendPayWellPollibudditBill(
//                                payWellConfig.getClientId(),
//                                payWellConfig.getClientPassword(),
//                                request.getBillNumber(),
//                                actualBillAmount,
//                                cfeTransaction.getToken(),
//                                ExternalType.POLLIBUDDIT_BILLPAY,
//                                request.getAccountNumber(),
//                                request.getMobileNumber(),
//                                request.getLogedInUserMobileNumber(),
//                                request.getFullName());
                //API consume version 2.2
                baseResponseObject=new ExternalCall(appConfiguration.getExternalUrl())
                        .sendPayWellPollyBiddyutBillPayment(
                                payWellConfig.getClientId(),
                                payWellConfig.getClientPassword(),
                                request.getBillNumber(),
                                actualBillAmount,
                                cfeTransaction.getToken(),
                                ExternalType.POLLIBUDDIT_BILLPAY,
                                request.getAccountNumber(),
                                request.getMobileNumber(),
                                request.getLogedInUserMobileNumber(),
                                request.getFullName(),
                                request.getBillMonth(),
                                request.getBillYear());

            }else{
                convertedAmount=receiverCreditAmount;

//                baseResponseObject=new ExternalCall(appConfiguration.getExternalUrl())
//                        .sendPayWellPollibudditBill(
//                                payWellConfig.getClientId(),
//                                payWellConfig.getClientPassword(),
//                                request.getBillNumber(),
//                                actualBillAmount,
//                                cfeTransaction.getToken(),
//                                ExternalType.POLLIBUDDIT_BILLPAY,
//                                request.getAccountNumber(),
//                                request.getMobileNumber(),
//                                request.getLogedInUserMobileNumber(),
//                                request.getFullName());

                //API consume version 2.2
                baseResponseObject=new ExternalCall(appConfiguration.getExternalUrl())
                        .sendPayWellPollyBiddyutBillPayment(
                                payWellConfig.getClientId(),
                                payWellConfig.getClientPassword(),
                                request.getBillNumber(),
                                actualBillAmount,
                                cfeTransaction.getToken(),
                                ExternalType.POLLIBUDDIT_BILLPAY,
                                request.getAccountNumber(),
                                request.getMobileNumber(),
                                request.getLogedInUserMobileNumber(),
                                request.getFullName(),
                                request.getBillMonth(),
                                request.getBillYear());
            }

            logWriterUtility.trace("Bill pay service ",new Gson().toJson(baseResponseObject));

            boolean isReversalRequired = false;

            if(baseResponseObject==null||baseResponseObject.getStatus()==null||
                    !baseResponseObject.getStatus().equals(PocketConstants.OK)){

                isReversalRequired = true;

                logWriterUtility.error(request.getRequestId(),"Bill pay external response failed");

                if(baseResponseObject==null){
                    baseResponseObject=new BaseResponseObject(request.getRequestId());
                    cfeTransaction.setNotes("Bill pay external response failed. Response Code. No status code provided");
                    baseResponseObject.setError(new ErrorObject("400","Bill pay failed"));
                }else{
                    if(baseResponseObject.getStatus()==null){
                        cfeTransaction.setNotes("Bill pay external response failed. Response Code. No status code provided");

                        if(baseResponseObject.getError()==null){
                            baseResponseObject.setError(new ErrorObject("400","Bill pay failed"));
                        }else{
                            baseResponseObject.setError(new ErrorObject(PocketErrorCode.BillPayFailed.getKeyString(),
                                    "Bill pay failed. Reason : "+baseResponseObject.getError().getErrorMessage()));
                        }

                    }else{
                        cfeTransaction.setNotes("Bill pay external response failed. Response Code :"+baseResponseObject.getStatus());

                        baseResponseObject.setError(new ErrorObject(PocketErrorCode.BillPayFailed.getKeyString(),
                                "Bill pay failed. Reason :"+baseResponseObject.getError().getErrorMessage()));

                    }
                }

                tsTransactionRepository.save(cfeTransaction);

//                baseResponseObject.setRequestId(request.getRequestId());
//                baseResponseObject.setStatus(PocketConstants.ERROR);
//                baseResponseObject.setData(null);
//                return baseResponseObject;
            }else{
                cfeTransaction.setTransactionStatus(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
            }

            logWriterUtility.trace(request.getRequestId(),"Bill pay external response success");

            if (isReversalRequired) {

                logWriterUtility.debug(request.getRequestId(), "Reversal required.");

                ReversalRequest reversalRequest = new ReversalRequest();
                reversalRequest.setReason("Unsuccessful polly biddyut bill via API");
                reversalRequest.setTransactionToken(cfeTransaction.getToken());
                try {
                    irregularTransactionService.createReversal(reversalRequest);
                } catch (PocketException e) {
                    e.printStackTrace();
                }

                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return baseResponseObject;
            }

        } catch (Exception e) {
            logWriterUtility.error(request.getRequestId(),""+e.getMessage());

            cfeTransaction.setTransactionStatus(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
            cfeTransaction.setNotes(e.getMessage());
            tsTransactionRepository.save(cfeTransaction);

            BaseResponseObject baseResponseObject = new BaseResponseObject();

            baseResponseObject.setError(new ErrorObject(PocketErrorCode.BillPayFailed.getKeyString(),
                    "Bill pay failed. Reason : "+baseResponseObject.getError().getErrorMessage()));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return baseResponseObject;
        }

        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setSenderRunningBalance(senderRunningBalance);
        fundTransferResponseData.setTxToken(cfeTransaction.getToken());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(new BigDecimal(convertedAmount),
                        clientReceiver.getWalletNo(),
                        clientSender.getWalletNo(), cfeTransaction.getToken(), cfeService.getServiceCode(),
                        feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes());
        notificationRequest.setBillNumber(request.getBillNumber());
        notificationRequest.setAccountNumber(request.getAccountNumber());

        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        CompletableFuture.runAsync(() -> {
            mfsFeeSharingService.doFeeShareForIndividualTransaction(cfeTransaction.getToken(),
                    request.getRequestId());
        });

        if (pocketConversionProfit.compareTo(BigDecimal.ZERO) > 0) {
            mfsFeeSharingService.shareConversionProfitForIndividualTransaction(conversionFeeCode,pocketConversionProfit, cfeTransaction.getToken(),
                    request.getRequestId());
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(fundTransferResponseData);
        return baseResponseObject;
    }
}
