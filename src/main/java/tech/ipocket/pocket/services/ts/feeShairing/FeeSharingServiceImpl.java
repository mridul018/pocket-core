package tech.ipocket.pocket.services.ts.feeShairing;

import org.apache.axis.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.TsFeeShairingRecordDetails;
import tech.ipocket.pocket.entity.TsGlTransactionDetails;
import tech.ipocket.pocket.entity.TsTransaction;
import tech.ipocket.pocket.repository.ts.TsFeeShairingRecordDetailsRepository;
import tech.ipocket.pocket.repository.ts.TsGlTransactionDetailsRepository;
import tech.ipocket.pocket.repository.ts.TsTransactionRepository;
import tech.ipocket.pocket.request.external.MultipleEmailRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.utils.*;

import java.time.LocalDate;
import java.util.*;

@Service
public class FeeSharingServiceImpl implements FeeSharingService {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private Environment environment;

    private List<Integer> successTxList = new ArrayList<>();
    private List<Integer> failedTxList = new ArrayList<>();
    private List<Integer> fatalTxList = new ArrayList<>();
    private Set<Integer> totalNoOfTx = new HashSet<>();
    private Set<Integer> totalNoOFFeeShared = new HashSet<>();
    private Long totalTime = 0L;
    private Long time = 0L;
    private Long endTime = 0L;


    @Autowired
    private TsGlTransactionDetailsRepository glTransactionDetailsRepository;
    @Autowired
    private TsTransactionRepository transactionRepository;
    @Autowired
    private EodTransactionalService eodTransactionalService;

    @Autowired
    private TsFeeShairingRecordDetailsRepository tsFeeShairingRecordDetailsRepository;

    @Override
    public BaseResponseObject feeSharing(String requestId) throws PocketException {

        logWriterUtility.info(requestId, "EOD started. Time :" + new Date());
        TsTransaction transaction = null;

        successTxList = new ArrayList<>();
        failedTxList = new ArrayList<>();
        fatalTxList = new ArrayList<>();
        totalNoOfTx = new HashSet<>();
        totalNoOFFeeShared = new HashSet<>();
        totalTime = 0L;

        time = System.currentTimeMillis();
        List<TsGlTransactionDetails> details = glTransactionDetailsRepository.findAllByEodStatus("I");

        List<String> statusList = new ArrayList<>();
            statusList.add(TsEnums.TransactionStatus.INITIALIZE.getTransactionStatus());
            statusList.add(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());

        for (int i = 0; i < details.size(); i++) {
            try {
                transaction = transactionRepository.findCfeTransactionByIdAndTransactionStatusInAndEodStatus
                        (details.get(i).getTsTransactionByTransactionId().getId(), statusList, "I");

                if(transaction==null){
                    fatalTxList.add(details.get(i).getId());
                    logWriterUtility.error(requestId, "Transaction id " + details.get(i).getId() +
                            " is not found in Transaction table for status " + statusList);
                    continue;
                }


                logWriterUtility.info(requestId, "EOD started for Tr id : " + transaction.getId());
                eodTransactionalService.singleTransactionFeeSharing(transaction, details.get(i),requestId);
                successTxList.add(transaction.getId());


            } catch (Exception e) {
                failedTxList.add(transaction.getId());
                logWriterUtility.error(requestId, "EOD failed for : " + transaction.getId() + " Reason : " + e);
            }
        }

        endTime = System.currentTimeMillis();
        totalTime = (endTime - time);

        for (TsGlTransactionDetails glTransactionDetails : details) {
            totalNoOfTx.add(glTransactionDetails.getId());
            if (successTxList.contains(glTransactionDetails.getId())) {
                totalNoOFFeeShared.add(glTransactionDetails.getId());
            }
        }

        updateEodStatisticsForFeeSharing(requestId);

        sendReport(requestId);

        BaseResponseObject baseResponseObject=new BaseResponseObject(requestId);
        baseResponseObject.setData(new SuccessBoolResponse(true));
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        return baseResponseObject;
    }

    private void updateEodStatisticsForFeeSharing(String requestId) {
        try {
            TsFeeShairingRecordDetails feeSharingRecordDetails = new TsFeeShairingRecordDetails();

            feeSharingRecordDetails.setWhichDay(java.sql.Date.valueOf(LocalDate.now()));
            feeSharingRecordDetails.setDateOfFeeShared(java.sql.Date.valueOf(LocalDate.now().minusDays(1)));
//            feeSharingRecordDetails.setIsFeeShareCompleted(true);
            feeSharingRecordDetails.setFeeShairingStatus(totalNoOfTx.size() > totalNoOFFeeShared.size() ? "0" : "1");
            feeSharingRecordDetails.setStartTime(new java.sql.Date(time));
            feeSharingRecordDetails.setEndTime(new java.sql.Date(endTime));
            feeSharingRecordDetails.setTotalNoOfTransaction(totalNoOfTx.size());
            feeSharingRecordDetails.setTotalNoOfFeeShared(totalNoOFFeeShared.size());

            tsFeeShairingRecordDetailsRepository.save(feeSharingRecordDetails);

            logWriterUtility.info(requestId, "EOD Completed ");
            logWriterUtility.info(requestId, "Total Tx :: " + totalNoOfTx.size());
            logWriterUtility.info(requestId, "Total Fee Shared :: " + totalNoOFFeeShared.size());
            logWriterUtility.info(requestId, "Success Tx List :: " + successTxList);
            logWriterUtility.info(requestId, "Failed Tx Size :: " + failedTxList.size() + " List :: " + failedTxList);
            logWriterUtility.info(requestId, "Fatal Tx Size :: " + fatalTxList.size() + " List :: " + fatalTxList);
            logWriterUtility.info(requestId, "Total time needed :: " + totalTime + " ms");
        } catch (Exception ex) {
            logWriterUtility.error(requestId, "Updating EOD statistics in DB failed failed");
        }
    }

    private void sendReport(String requestId) {
        try {
            String[] eodEmailList = StringUtils.split(environment.getProperty("EOD_EMAIL_LIST"), ',');
            String eodEnvironment = environment.getProperty("EOD_ENVIRONMENT");

            List<String> emailList = new ArrayList<>();

            for (String email : eodEmailList) {
                emailList.add(email);
            }

            String emailBody = "EOD Environment : " + eodEnvironment + "\n"
                    + "RequestId : " + requestId + "\n"
                    + "EOD completed for " + String.valueOf(LocalDate.now().minusDays(1)) + "\n"
                    + "Total time needed " + totalTime + "\n"
                    + "EOD run for " + totalNoOfTx.size() + "\n"
                    + "EOD successful for " + totalNoOFFeeShared.size() + "\n"
                    + "EOD Failed for " + failedTxList.size() + "\n"
                    + "EOD Fatal error " + fatalTxList.size() + "\n";

            MultipleEmailRequest request = new MultipleEmailRequest();
            request.setRequestId(requestId);
            request.setEmailSubject("EOD Completed");
            request.setReceiverEmailList(emailList);
            request.setEmailBody(emailBody);


            String notificationServiceURL = environment.getProperty("notificationGatewayURL");
            new ExternalCall(notificationServiceURL).sendMultyEmail(request);

        } catch (Exception ex) {
            logWriterUtility.error(requestId, "EOD email sending failed");
        }
    }


}
