package tech.ipocket.pocket.services.ts.paynetService;

import tech.ipocket.pocket.entity.TsTransaction;
import tech.ipocket.pocket.repository.ts.paynet.ConfirmFindTransaction;
import tech.ipocket.pocket.request.ts.paynet.*;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.paynet.*;
import tech.ipocket.pocket.utils.PocketException;

import java.io.IOException;

public interface PaynetService {
    BaseResponseObject createPaynetTransaction(PaynetBillPayRequest request, TsSessionObject tsSessionObject) throws PocketException;

    ConfirmFindTransaction confirmTransaction(PaynetBillPayRequest request) throws PocketException;

    BaseResponseObject findTransaction(PaynetBillPayRequest request) throws PocketException;

    PaynetServiceResponse getPaynetAllServices(PaynetServicesRequest request) throws PocketException, IOException;

    PaynetCountryResponse getPaynetAllCountries(PaynetEmptyRequest request) throws PocketException, IOException;

    PaynetCountryProviderResponse getPaynetCountryProviders(PaynetCountryProviderRequest request) throws  PocketException, IOException;

    PaynetProviderOperatorResponse getPaynetProviderOperators(PaynetProviderOperatorRequest request) throws PocketException, IOException;

    PaynetBalanceResponse getPaynetBalance(PaynetEmptyRequest request) throws PocketException, IOException;

    PaynetInfoResponse getPaynetInfoDetail(PaynetEmptyRequest request) throws PocketException, IOException;

    BaseResponseObject doMobilePostPaidTopup(TransactionRequest request, TsTransaction tsTransaction) throws PocketException;
}
