package tech.ipocket.pocket.services.ts.transaction;

import com.google.gson.Gson;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.UserDeviceMapRepository;
import tech.ipocket.pocket.repository.um.UserDeviceRepository;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.transaction.TransactionResponseData;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.utils.TransactionBuilder;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class FundTransferServiceImpl implements FundTransferService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;

    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TsServiceRepository serviceRepository;

    @Autowired
    private TransactionValidator transactionValidator;

    @Autowired
    private TsGlRepository tsGlRepository;

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;


    @Override
    public BaseResponseObject doFundTransfer(TransactionRequest request, TsSessionObject tsSessionObject) throws PocketException {

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientSender = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getSenderMobileNumber(), statusNotIn);

        if (clientSender == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getReceiverMobileNumber(), statusNotIn);

        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Receiver not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound);
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(request.getTransactionType());

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //calculate fee
        FeeData feeData = feeManager.calculateFee(clientSender, cfeService.getServiceCode(),
                new BigDecimal(request.getTransferAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData, new BigDecimal(request.getTransferAmount()));


        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");

        //transaction profile validation for sender + check balance limit for sender
        Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSender, cfeService, transactionAmountData.getSenderDebitAmount(),
                request.getRequestId());
        if (!isSenderProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
            throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
        }

        //transaction profile validation for receiver
        Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(clientReceiver, cfeService,
                request.getRequestId());
        if (!isReceiverProfileValid) {
            logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
            throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
        }

        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(clientSender, clientReceiver, cfeService.getServiceCode(),
                feeData, transactionAmountData);
        tsTransactionRepository.save(cfeTransaction);

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();

        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.DEBIT,
                clientSender.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                clientReceiver.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(), FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();

        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Deposit_of_Customer, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Deposit_of_Customer, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_FundTransfer, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        tsGlTransactionDetailsRepository.saveAll(glTxDetailList);

        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSender, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(clientReceiver, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setSenderRunningBalance(senderRunningBalance);
        cfeTransaction.setReceiverRunningBalance(receiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        TransactionResponseData fundTransferResponseData = new TransactionResponseData();
        fundTransferResponseData.setSenderRunningBalance(senderRunningBalance);
        fundTransferResponseData.setTxToken(cfeTransaction.getToken());
        fundTransferResponseData.setDateTime(String.valueOf(LocalDate.now()));

        NotificationBuilder notificationBuilder = new NotificationBuilder();

        List<String> status=new ArrayList<>();
        status.add("1");




        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(transactionAmountData.getOriginalTransactionAmount(),
                        clientReceiver.getWalletNo(),
                        clientSender.getWalletNo(), cfeTransaction.getToken(), cfeService.getServiceCode(),
                        feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(fundTransferResponseData), request.getRequestId(), request.getNotes());
        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        CompletableFuture.runAsync(() -> {
            mfsFeeSharingService.doFeeShareForIndividualTransaction(cfeTransaction.getToken(),
                    request.getRequestId());
        });

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(null);
        baseResponseObject.setRequestId(request.getRequestId());
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(fundTransferResponseData);
        return baseResponseObject;
    }


}
