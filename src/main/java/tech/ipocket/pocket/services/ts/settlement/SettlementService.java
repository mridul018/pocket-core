package tech.ipocket.pocket.services.ts.settlement;

import tech.ipocket.pocket.request.ts.settlement.AdminCashInToCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.AdminCashOutFromCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.FundMovementWalletToWalletRequest;
import tech.ipocket.pocket.request.ts.settlement.MasterDepositFromBankGlRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface SettlementService {
    BaseResponseObject depositToMasterFromBankGl(MasterDepositFromBankGlRequest request) throws PocketException;

    BaseResponseObject adminCashInToCustomer(AdminCashInToCustomerRequest request) throws PocketException;
    BaseResponseObject adminCashOutFromCustomer(AdminCashOutFromCustomerRequest request) throws PocketException;

    BaseResponseObject fundMovementWalletToWallet(FundMovementWalletToWalletRequest request) throws PocketException;
}
