package tech.ipocket.pocket.services.ts.transactionValidity;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.repository.ts.TsClientRepository;
import tech.ipocket.pocket.repository.ts.TsMerchantDetailsRepository;
import tech.ipocket.pocket.repository.ts.TsServiceRepository;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.response.ts.transactionValidity.TransactionValidityResponse;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.ServiceCodeConstants;

@Service
public class TransactionValidityFactory implements ApplicationContextAware {

    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TransactionValidator validator;
    @Autowired
    private TsMerchantDetailsRepository tsMerchantDetailsRepository;

    private ApplicationContext context;


    public TransactionValidityResponse getTransactionvalidity(TransactionValidityRequest request) throws PocketException {

        if (request.getType() == null || request.getType().trim().length() == 0) {
            throw new PocketException(PocketErrorCode.InvalidTransactionType);
        }

        switch (request.getType()) {
            case ServiceCodeConstants
                    .Fund_Transfer:
                FundTransferTransactionValidity fundTransferTransactionValidity = new FundTransferTransactionValidity();
                fundTransferTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return fundTransferTransactionValidity.getTransactionValidity(request);

            case ServiceCodeConstants
                    .Merchant_Payment:
                MerchantPaymentTransactionValidity merchantPaymentTransactionValidity = new MerchantPaymentTransactionValidity();
                merchantPaymentTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return merchantPaymentTransactionValidity.getTransactionValidity(request);

            case ServiceCodeConstants
                    .BillPay:
                BillPayTransactionValidity billPayTransactionValidity = new BillPayTransactionValidity();
                billPayTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return billPayTransactionValidity.getTransactionValidity(request);

            case ServiceCodeConstants
                    .Top_Up:
                TopUpTransactionValidity topUpTransactionValidity = new TopUpTransactionValidity();
                topUpTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return topUpTransactionValidity.getTransactionValidity(request);

            case ServiceCodeConstants
                    .CashIn:
                CashInTransactionValidity cashInTransactionValidity = new CashInTransactionValidity();
                cashInTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return cashInTransactionValidity.getTransactionValidity(request);



            case ServiceCodeConstants
                    .CashInFromBank:
                CashInFromBankTransactionValidity cashInBankTransactionValidity = new CashInFromBankTransactionValidity();
                cashInBankTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return cashInBankTransactionValidity.getTransactionValidity(request);

            case ServiceCodeConstants
                    .CashOutToBank:
                CashOutToBankTransactionValidity cashOutToBankTransactionValidity = new CashOutToBankTransactionValidity();
                cashOutToBankTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return cashOutToBankTransactionValidity.getTransactionValidity(request);


            case ServiceCodeConstants
                    .CashOut:
                CashOutTransactionValidity cashOutTransactionValidity = new CashOutTransactionValidity();
                cashOutTransactionValidity.setDependency(clientRepository, serviceRepository, feeManager, validator, tsMerchantDetailsRepository);

                return cashOutTransactionValidity.getTransactionValidity(request);

            default:
                throw new PocketException(PocketErrorCode.UnsupportedService);

        }
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
