package tech.ipocket.pocket.services.ts.transaction;

import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.PocketException;

public interface FundTransferService {
    BaseResponseObject doFundTransfer(TransactionRequest transactionRequest, TsSessionObject tsSessionObject) throws PocketException;
}
