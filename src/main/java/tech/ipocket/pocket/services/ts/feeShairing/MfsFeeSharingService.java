package tech.ipocket.pocket.services.ts.feeShairing;

import tech.ipocket.pocket.request.ts.fee_share.GetSharedFeeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

import java.math.BigDecimal;

public interface MfsFeeSharingService {
    BaseResponseObject exploreFeeSharingItems();

    BaseResponseObject doFeeShareForIndividualTransaction(String mainTransactionReference, String requestId);

    BaseResponseObject getSharedFee(GetSharedFeeRequest request) throws PocketException;

    BaseResponseObject shareConversionProfitForIndividualTransaction(BigDecimal pocketConversionProfit, String transactionToken, String requestId);
    BaseResponseObject shareConversionProfitForIndividualTransaction(String feeCode,BigDecimal pocketConversionProfit, String transactionToken, String requestId);
}
