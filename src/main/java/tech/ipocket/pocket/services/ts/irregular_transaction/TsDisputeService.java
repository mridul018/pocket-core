package tech.ipocket.pocket.services.ts.irregular_transaction;

import tech.ipocket.pocket.request.ts.irregular_transaction.CreateDisputeRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.GetDisputeRequest;
import tech.ipocket.pocket.request.ts.irregular_transaction.UpdateDisputeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

public interface TsDisputeService {
    BaseResponseObject createDispute(CreateDisputeRequest getDisputeRequest) throws PocketException;
    BaseResponseObject updateDispute(UpdateDisputeRequest getDisputeRequest) throws PocketException;

    BaseResponseObject findAllDisputeByStatus(GetDisputeRequest getDisputeRequest);
    BaseResponseObject getDisputeByTransactionToken(String txToken, String requestId);
}
