package tech.ipocket.pocket.services.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionDetailsRequest;
import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionHistoryRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.SearchData;
import tech.ipocket.pocket.response.ts.transaction.TransactionHistoryResponse;
import tech.ipocket.pocket.response.ts.transaction.TransactionHistoryResponseRoot;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionHistoryServiceImpl implements TransactionHistoryService {

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private TsTransactionRepository tsTransactionRepository;

    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsClientBalanceRepository clientBalanceRepository;
    @Autowired
    private TsDisputeRepository disputeRepository;
    @Autowired
    private Environment environment;

    @Override
    public BaseResponseObject getTransactionHistory(GetTransactionHistoryRequest request) throws PocketException {

//        Page<ITsTransaction> transactions;
        List<TsTransaction> transactions = null;

        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNo(), statusNotIn);


//        TsClient client = clientRepository.findByWalletNo(request.getWalletNo());

        if (client == null) {
            logWriterUtility.error(request.getRequestId(), "Account not found");
            throw new PocketException(PocketErrorCode.WalletNotFound);
        }

        TsClientBalance clientBalance = clientBalanceRepository.findFirstByTsClientByClientId(client);


        SearchData searchData = new SearchData();
        searchData.setName(client.getFullName());
        if (clientBalance != null) {
            searchData.setCurrentBalance(clientBalance.getBalance());
        }
        searchData.setWalletNo(client.getWalletNo());
        searchData.setStartDate(request.getFromDate());
        searchData.setEndDate(request.getToDate());

        PageRequest pageRequest = PaginationUtil.makePageRequest(request.getPageId(), request.getNumberOfItemPerPage(),
                Sort.Direction.DESC, "id");

        List<String> txFailedStatus = new ArrayList<>();
        txFailedStatus.add(TsEnums.TransactionStatus.FAILED.getTransactionStatus());
        txFailedStatus.add(TsEnums.TransactionStatus.INCOMPLETE.getTransactionStatus());

        if (request.getFromDate() == null || request.getToDate() == null) {
            if (request.getTransactionTypes() == null
                    || request.getTransactionTypes().size() == 0) {
                transactions = tsTransactionRepository.findAllByReceiverWalletOrSenderWallet(request.getWalletNo(), request.getWalletNo(),txFailedStatus, pageRequest);
            } else {
                transactions = tsTransactionRepository.findAllByTransactionTypeInAndReceiverWalletOrSenderWallet(request.getTransactionTypes(),
                        request.getWalletNo(), request.getWalletNo(),txFailedStatus, pageRequest);
            }
        } else {

            Date fromDate = DateUtil.parseDate(request.getFromDate());

            Date toDate = DateUtil.parseDate(request.getToDate());
            toDate = DateUtil.addDay(toDate, 1);

            if (request.getTransactionTypes() == null
                    || request.getTransactionTypes().size() == 0) {
                transactions = tsTransactionRepository.findAllByReceiverWalletOrSenderWalletDateBetween(request.getWalletNo(),
                        request.getWalletNo(), fromDate, toDate,txFailedStatus, pageRequest);
            } else {
                transactions = tsTransactionRepository.findAllByTransactionTypeInAndReceiverWalletOrSenderWalletDateBetween(request.getTransactionTypes(),
                        request.getWalletNo(), request.getWalletNo(), fromDate, toDate,txFailedStatus, pageRequest);
            }
        }

        List<TransactionHistoryResponse> historyResponseList = new ArrayList<>();
        TransactionHistoryResponse historyResponseItem = null;


//        int totalNoOfTransaction = (int) transactions.getTotalElements();
        int totalNoOfTransaction = (int) transactions.size();
        BigDecimal totalTransactionAmount = BigDecimal.ZERO;

//        for (ITsTransaction iTsTransaction : transactions.getContent()) {
        for (TsTransaction iTsTransaction : transactions) {
            historyResponseItem = new TransactionHistoryResponse();

            totalTransactionAmount = totalTransactionAmount.add(iTsTransaction.getTransactionAmount());


            historyResponseItem.setTransactionType(iTsTransaction.getTransactionType());

            TsService tsService = tsServiceRepository.findFirstByServiceCode(iTsTransaction.getTransactionType());
            if (tsService != null) {
                historyResponseItem.setTransactionType(tsService.getDescription());
            }

            historyResponseItem.setId(iTsTransaction.getId());
            historyResponseItem.setCreatedDate(iTsTransaction.getCreatedDate().toString());
            historyResponseItem.setDisputable(iTsTransaction.getIsDisputable());

            if (iTsTransaction.getFeeAmount() == null) {
                historyResponseItem.setFeeAmount(BigDecimal.ZERO.doubleValue());
            } else {
                historyResponseItem.setFeeAmount(iTsTransaction.getFeeAmount().doubleValue());

            }

            historyResponseItem.setFeeCode(iTsTransaction.getFeeCode());
            historyResponseItem.setFeePayer(iTsTransaction.getFeePayer());
            historyResponseItem.setTransactionAmount(iTsTransaction.getTransactionAmount().doubleValue());
            historyResponseItem.setSenderDebitAmount(iTsTransaction.getSenderDebitAmount().doubleValue());
            historyResponseItem.setReceiverCreditAmount(iTsTransaction.getReceiverCreditAmount().doubleValue());
            historyResponseItem.setToken(iTsTransaction.getToken());
            historyResponseItem.setSenderWallet(iTsTransaction.getSenderWallet());

            if(iTsTransaction.getTransactionType().equals(TsEnums.Services.Top_Up.getServiceCode())){
                historyResponseItem.setReceiverWallet(iTsTransaction.getLogicalReceiver());
            }else{
                historyResponseItem.setReceiverWallet(iTsTransaction.getReceiverWallet());
            }

//            historyResponseItem.setReceiverWallet(iTsTransaction.getReceiverWallet());
            historyResponseItem.setTransactionStatus(iTsTransaction.getTransactionStatus());
            historyResponseItem.setPrevTransactionRef(iTsTransaction.getRefTransactionToken());
            historyResponseItem.setFeeCode(iTsTransaction.getFeeCode());
            historyResponseItem.setLogicalSender(iTsTransaction.getLogicalSender());
            historyResponseItem.setLogicalReceiver(iTsTransaction.getLogicalReceiver());
            historyResponseList.add(historyResponseItem);
        }

        if (searchData != null) {
            searchData.setTotalNoOfTransaction(totalNoOfTransaction);
            searchData.setTotalTransactionAmount(totalTransactionAmount);
        }

        TransactionHistoryResponseRoot responseRoot = new TransactionHistoryResponseRoot();
        responseRoot.setSearchData(searchData);
        responseRoot.setHistory(historyResponseList);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(responseRoot);

        return baseResponseObject;
    }

    @Override
    public BaseResponseObject getTransactionDetailsByToken(GetTransactionDetailsRequest request) throws PocketException {

        TsTransaction tsTransaction = tsTransactionRepository.findFirstByToken(request.getTransactionTokenNo());
        if (tsTransaction == null) {
            logWriterUtility.error(request.getRequestId(), "No transaction foudn by provided token :" + request.getTransactionTokenNo());
            throw new PocketException(PocketErrorCode.InvalidTransactionToken);
        }

        TsDispute tsDispute = disputeRepository.findFirstByTransactionTokenNoOrderByIdDesc(tsTransaction.getToken());

        TransactionHistoryResponse response = new TransactionHistoryResponse();
        response.setTransactionType(tsTransaction.getTransactionType());

        TsService tsService = tsServiceRepository.findFirstByServiceCode(tsTransaction.getTransactionType());
        if (tsService != null) {
            response.setTransactionType(tsService.getDescription());
        }

        response.setId(tsTransaction.getId());
        response.setCreatedDate(tsTransaction.getCreatedDate().toString());
        response.setDisputable(tsTransaction.getIsDisputable());

        response.setFeeAmount(BigDecimal.ZERO.doubleValue());
        if (tsTransaction.getFeeAmount() == null) {
            response.setFeeAmount(BigDecimal.ZERO.doubleValue());
        } else {
            if (tsTransaction.getFeePayer() != null && tsTransaction.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
                if (request.getWalletNo().equalsIgnoreCase(tsTransaction.getSenderWallet())) {
                    response.setFeeAmount(tsTransaction.getFeeAmount().doubleValue());
                }
            }

        }

        response.setFeeCode(tsTransaction.getFeeCode());
        response.setFeePayer(tsTransaction.getFeePayer());
        response.setTransactionAmount(tsTransaction.getTransactionAmount().doubleValue());
        response.setSenderDebitAmount(tsTransaction.getSenderDebitAmount().doubleValue());
        response.setReceiverCreditAmount(tsTransaction.getReceiverCreditAmount().doubleValue());
        response.setToken(tsTransaction.getToken());
        response.setSenderWallet(tsTransaction.getSenderWallet());

        if(tsTransaction.getTransactionType().equals(TsEnums.Services.Top_Up.getServiceCode())){
            response.setReceiverWallet(tsTransaction.getLogicalReceiver());
        }else{
            response.setReceiverWallet(tsTransaction.getReceiverWallet());
        }

        response.setTransactionStatus(tsTransaction.getTransactionStatus());
        response.setPrevTransactionRef(tsTransaction.getRefTransactionToken());
        response.setFeeCode(tsTransaction.getFeeCode());
        response.setLogicalSender(tsTransaction.getLogicalSender());
        if(tsTransaction.getTransactionType().equals(PocketConstants.PASSPORT_TRANSACTION_TYPE)){
            response.setLogicalReceiver(environment.getProperty("emb.name"));
        }else {
            response.setLogicalReceiver(tsTransaction.getLogicalReceiver());
        }

        if (tsDispute != null) {
            response.setDisputable(false);
            response.setDisputeStatus(getDisputeStatusByStatus(Integer.parseInt(tsDispute.getStatus())));
            response.setDisputeReason(tsDispute.getReason());
            response.setDisputeResolvedMessage(tsDispute.getResolvedMessage());
        }else{
            response.setDisputable(tsTransaction.getIsDisputable());
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(response);

        return baseResponseObject;
    }

    private String getDisputeStatusByStatus(int status){
        switch (status){
            case 1:
                return "INITIALIZED";
            case 2:
                return "RESOLVED";
            case 3:
                return "REVERSED";
        }
        return ""+status;
    }
}
