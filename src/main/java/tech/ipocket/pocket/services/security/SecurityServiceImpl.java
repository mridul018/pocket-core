package tech.ipocket.pocket.services.security;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.UmUserDevice;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.entity.UmUserSession;
import tech.ipocket.pocket.repository.um.UserDeviceRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.repository.um.UserSessionRepository;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.security.TokenGenerateRequest;
import tech.ipocket.pocket.request.security.TokenPinValidateRequest;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenGenerateResponse;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.UmConstants;

import java.sql.Timestamp;
import java.util.*;

import static tech.ipocket.pocket.utils.SecuritySettings.EXPIRATION_TIME;


@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    UserInfoRepository userInfoRepository;
    @Autowired
    UserDeviceRepository userDeviceRepository;
    @Autowired
    UserSessionRepository userSessionRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public TokenGenerateResponse generateToken(TokenGenerateRequest request) throws PocketException {
        TokenGenerateResponse response = new TokenGenerateResponse();

        UmUserInfo userInfo = userInfoRepository.findByLoginId(request.getMobileNumber());
        if (userInfo == null) {
            response.setError(new ErrorObject(PocketErrorCode.USER_NOT_FOUND));
            return response;
        }

        if(request.getHardwareSignature()!=null && request.getHardwareSignature().equals(UmConstants.POCKET_WEB_PORTAL)){

        }else{
            UmUserDevice userDevice = userDeviceRepository.findFirstByHardwareSignature(request.getHardwareSignature());
            if (userDevice == null) {
                response.setError(new ErrorObject(PocketErrorCode.DEVICE_NOT_FOUND));
                return response;
            }
        }


        UmUserSession userSession = userSessionRepository.findByUmUserInfoByUserId(userInfo);
        if (userSession != null) {
            userSession.setCreatedDate(new Timestamp(System.currentTimeMillis()));

            userSession = userSessionRepository.saveAndFlush(userSession);
        } else {
            UmUserSession session = new UmUserSession();
            session.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            session.setStatus("1");
            session.setUmUserInfoByUserId(userInfo);
            userSession = userSessionRepository.saveAndFlush(session);
        }

        // generate token
        Map<String, Object> claims = new HashMap<>();
        claims.put("user-name", userInfo.getMobileNumber());
        claims.put("user-password", userInfo.getPassword());

        String token = Jwts.builder()
                .setClaims(claims)
                .setSubject(String.valueOf(userSession.getId()))
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, Base64.getEncoder().encodeToString(request.getHardwareSignature().getBytes()))
                .compact();

        response.setSessionId(userSession.getId());
        response.setToken(token);
        return response;
    }

    @Override
    public TokenValidateUmResponse validateToken(BaseRequestObject request) throws PocketException {


        UmUserSession umUserSession = validateSession(request);

        TokenValidateUmResponse response = new TokenValidateUmResponse();

        /*String sessionId = Jwts.parser()
                .setSigningKey(Base64.getEncoder().encodeToString(request.getHardwareSignature().getBytes()))
                .parseClaimsJws(request.getSessionToken())
                .getBody()
                .getSubject();*/

        long currentTime = System.currentTimeMillis();
        long sessionCreateTime = umUserSession.getCreatedDate().getTime();
        if ((currentTime - sessionCreateTime) > EXPIRATION_TIME) {
            throw new PocketException(PocketErrorCode.SESSION_TIME_EXPIRE);
        }

        response.setUserId(umUserSession.getUmUserInfoByUserId().getId());
        response.setValid(Boolean.TRUE);
        response.setGroupCode(umUserSession.getUmUserInfoByUserId().getGroupCode());
        response.setMobileNumber(umUserSession.getUmUserInfoByUserId().getMobileNumber());
        response.setName(umUserSession.getUmUserInfoByUserId().getFullName());
        response.setPhotoId("");

        return response;
    }

    @Override
    public TokenValidateUmResponse verifyTokenAndCredential(TokenPinValidateRequest request) throws PocketException {

        UmUserSession umUserSession = validateSession(request);

        validateCredential(umUserSession, request);

        TokenValidateUmResponse response = new TokenValidateUmResponse();

        long currentTime = System.currentTimeMillis();
        long sessionCreateTime = umUserSession.getCreatedDate().getTime();
        if ((currentTime - sessionCreateTime) > EXPIRATION_TIME) {
            throw new PocketException(PocketErrorCode.SESSION_TIME_EXPIRE);
        }

        response.setValid(Boolean.TRUE);
        response.setGroupCode(umUserSession.getUmUserInfoByUserId().getGroupCode());
        response.setMobileNumber(umUserSession.getUmUserInfoByUserId().getMobileNumber());
        response.setName(umUserSession.getUmUserInfoByUserId().getFullName());
        response.setPhotoId("");

        return response;
    }

    private Boolean validateCredential(UmUserSession umUserSession, TokenPinValidateRequest request) throws PocketException {

        if (passwordEncoder.matches(request.getCredential(),umUserSession.getUmUserInfoByUserId().getPassword())) {
            return Boolean.TRUE;
        }
        throw new PocketException(PocketErrorCode.CredentialNotMatched);
    }

    private UmUserSession validateSession(BaseRequestObject request) throws PocketException {
        TokenValidateUmResponse response = new TokenValidateUmResponse();

        String sessionId = null;
        try {
            sessionId = Jwts.parser()
                    .setSigningKey(Base64.getEncoder().encodeToString(request.getHardwareSignature().getBytes()))
                    .parseClaimsJws(request.getSessionToken())
                    .getBody()
                    .getSubject();
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            throw new PocketException(PocketErrorCode.SessionTokenExpired);
        }

        if (sessionId == null) {
            throw new PocketException(PocketErrorCode.SessionTokenExpired);
        }

        Optional<UmUserSession> userSession = userSessionRepository.findById(Integer.parseInt(sessionId));
        if (!userSession.isPresent()) {
            throw new PocketException(PocketErrorCode.SessionTokenExpired);
        }

        return userSession.get();

    }
}
