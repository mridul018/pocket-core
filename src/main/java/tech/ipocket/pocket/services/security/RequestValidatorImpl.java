package tech.ipocket.pocket.services.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.RestCallerTask;

@Service
public class RequestValidatorImpl implements RequestValidator {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());

    @Autowired
    private Environment environment;

     @Override
    public TsSessionObject validateSession(BaseRequestObject baseRequestObject) throws PocketException {

        String sessionCheckUrl=environment.getProperty("UM_SESSION_CHECK");

        TsSessionObject tsSessionObject= (TsSessionObject) new RestCallerTask()
                .callToRestService(HttpMethod.POST,sessionCheckUrl,baseRequestObject,TsSessionObject.class,baseRequestObject.getRequestId());


        if(tsSessionObject==null){
            logWriterUtility.error(baseRequestObject.getRequestId(),"Null from session check");
            throw new PocketException(PocketErrorCode.SessionTokenExpired);
        }

//        TsSessionObject tsSessionObject = new TsSessionObject();
//        tsSessionObject.setGroupCode("1001");
//        tsSessionObject.setName("Md Sajedul Karim");
//        tsSessionObject.setMobileNumber("01737186096");
//        tsSessionObject.setUserId(991);

        return tsSessionObject;
    }

    @Override
    public TsSessionObject validateSessionWithCredential(SessionCheckWithCredentialRequest baseRequestObject) throws PocketException {
        String sessionCheckUrl=environment.getProperty("verifyTokenAndCredential");

        TokenValidateUmResponse response= (TokenValidateUmResponse) new RestCallerTask()
                .callToRestService(HttpMethod.POST,sessionCheckUrl,baseRequestObject,TokenValidateUmResponse.class,baseRequestObject.getRequestId());

        if(response==null){
            logWriterUtility.error(baseRequestObject.getRequestId(),"Null from session check");
            throw new PocketException(PocketErrorCode.SessionTokenExpired);
        }

        TsSessionObject tsSessionObject=new TsSessionObject();
        tsSessionObject.setUserId(response.getUserId());
        tsSessionObject.setMobileNumber(response.getMobileNumber());
        tsSessionObject.setGroupCode(response.getGroupCode());
        tsSessionObject.setName(response.getName());
        tsSessionObject.setPhotoId(response.getPhotoId());
        return tsSessionObject;
    }
}
