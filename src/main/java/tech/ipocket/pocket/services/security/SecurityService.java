package tech.ipocket.pocket.services.security;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.security.TokenGenerateRequest;
import tech.ipocket.pocket.request.security.TokenPinValidateRequest;
import tech.ipocket.pocket.response.security.TokenGenerateResponse;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface SecurityService {
    TokenGenerateResponse generateToken(TokenGenerateRequest request) throws PocketException;

    TokenValidateUmResponse validateToken(BaseRequestObject request) throws PocketException;

    TokenValidateUmResponse verifyTokenAndCredential(TokenPinValidateRequest request) throws PocketException;
}
