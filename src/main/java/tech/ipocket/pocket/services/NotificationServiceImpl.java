package tech.ipocket.pocket.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.configuration.AppConfiguration;
import tech.ipocket.pocket.entity.UmNotification;
import tech.ipocket.pocket.entity.UmUserDeviceMapping;
import tech.ipocket.pocket.entity.UmUserInfo;
import tech.ipocket.pocket.repository.um.UmNotificationRepository;
import tech.ipocket.pocket.repository.um.UserDeviceMapRepository;
import tech.ipocket.pocket.repository.um.UserDeviceRepository;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.request.um.GetNotificationRequestByWalletRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class NotificationServiceImpl implements NotificationService {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    private UserDeviceRepository userDeviceRepository;

    @Autowired
    private UserDeviceMapRepository userDeviceMapRepository;
    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UmNotificationRepository umNotificationRepository;


    @Override
    public BaseResponseObject getUserNotificationByWallet(GetNotificationRequestByWalletRequest request, String walletNumber) throws PocketException {
        UmUserInfo umUserInfo=userInfoRepository.findByLoginId(walletNumber);
        if(umUserInfo==null){
            logWriterUtility.error(request.getRequestId(),"User info not found :"+walletNumber);
            throw new PocketException(PocketErrorCode.USER_NOT_FOUND);
        }

        PageRequest pageRequest = PaginationUtil.makePageRequest(request.getPageId(), request.getNumberOfItemPerPage(),
                Sort.Direction.DESC, "id");


        List<UmNotification> notifications=umNotificationRepository.findAllByReceiver(walletNumber,pageRequest);

        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());
        baseResponseObject.setData(notifications);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setError(null);

        return baseResponseObject;
    }

    @Override
    public void sendNotification(NotificationRequest notificationRequest) {

        if(notificationRequest==null){
            logWriterUtility.error("noRequestId","Invalid notification request");
            return;
        }

        if(notificationRequest.getServiceCode()==null||notificationRequest.getServiceCode().equals("")){
            logWriterUtility.error(notificationRequest.getRequestId(),"Invalid Service code");
            return;
        }


        String bodySmsSender,bodySmsReceiver;


        String feeSenderText="";
        String feeReceiverText="";

        if(notificationRequest.getFeeAmount() != null && notificationRequest.getFeeAmount().compareTo(BigDecimal.ZERO)>0){
            if(notificationRequest.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)){
                feeSenderText="Fee: "+notificationRequest.getFeeAmount();
            }else {
                feeReceiverText="Fee: "+notificationRequest.getFeeAmount();

            }
        }

        List<String> senderFcmKeys=null;
        List<String> receiverFcmKeys=null;

        switch (notificationRequest.getServiceCode()){
            case ServiceCodeConstants.CashIn:

                bodySmsSender="Cash in successful to :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+" Fee :"+notificationRequest.getFeeAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);

                bodySmsReceiver="Cash in successful from :"+notificationRequest.getSenderWalletNo()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+" Fee :"+notificationRequest.getFeeAmount()+" " +
                        " "+feeReceiverText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(),bodySmsReceiver, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "CashIn successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }


                receiverFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(receiverFcmKeys!=null){
                    sendPushNotification(receiverFcmKeys, "CashIn successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }


                break;
            case ServiceCodeConstants.CashOut:

                bodySmsSender="Cash out successful to :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);

                bodySmsReceiver="Cash out successful from :"+notificationRequest.getSenderWalletNo()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeReceiverText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(), bodySmsReceiver, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Cashout successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }


                receiverFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(receiverFcmKeys!=null){
                    sendPushNotification(receiverFcmKeys, "Cashout successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }


                break;
            case ServiceCodeConstants.Fund_Transfer:

                bodySmsSender="Fund transfer successful. To :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeSenderText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);

                bodySmsReceiver="Fund transfer successful from :"+notificationRequest.getSenderWalletNo()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeReceiverText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(), bodySmsReceiver, ExternalType.SMS);


                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Fund transfer successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                receiverFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(receiverFcmKeys!=null){
                    sendPushNotification(receiverFcmKeys, "Fund transfer successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;
            case ServiceCodeConstants.FundMovementWalletToWallet:

                bodySmsSender="Fund movement successful. To :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeSenderText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);

                bodySmsReceiver="Fund movement successful from :"+notificationRequest.getSenderWalletNo()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeReceiverText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(), bodySmsReceiver, ExternalType.SMS);


                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Fund movement successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                receiverFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(receiverFcmKeys!=null){
                    sendPushNotification(receiverFcmKeys, "Fund movement successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;
            case ServiceCodeConstants.Merchant_Payment:

                bodySmsSender="Merchant payment successful. To :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(), bodySmsSender, ExternalType.SMS);

                bodySmsReceiver="Payment receiver successful from :"+notificationRequest.getSenderWalletNo()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+"" +
                        " "+feeReceiverText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(),bodySmsReceiver, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Merchant payment successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                receiverFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(receiverFcmKeys!=null){
                    sendPushNotification(receiverFcmKeys, "Payment received successfully", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }



                break;
            case ServiceCodeConstants.Top_Up:

                bodySmsSender="Mobile top-up successful. To :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "TopUp successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;
            case ServiceCodeConstants.BillPay:

                bodySmsSender="Bill pay successful. To :"+notificationRequest.getReceiverWallet()+" " +
                        " Bill number :" +notificationRequest.getBillNumber()+
                        " Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                  sendSMSBody(notificationRequest, bodySmsSender);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Bill pay successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;
            case ServiceCodeConstants.Refill:
                break;
            case ServiceCodeConstants.AdminCashIn:
                bodySmsSender="Admin cashin successful. "+
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(), bodySmsSender, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Admin cashin successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }
                break;
            case ServiceCodeConstants.AdminCashOut:
                bodySmsSender="Admin cashout successful. "+
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(), bodySmsSender, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Admin cashout successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }
                break;
            case ServiceCodeConstants.CashInFromBank:

                bodySmsSender="Refill successful. From Bank:"+notificationRequest.getBankName()+"" +
                        " Account Number: " +notificationRequest.getBankAccountNumber()+" "+
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(), bodySmsSender, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Cash In from bank successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;
            case ServiceCodeConstants.CashOutToBank:


                bodySmsSender="Withdrawal successful. To Bank:"+notificationRequest.getBankName()+"" +
                        " Account Number: " +notificationRequest.getBankAccountNumber()+" "+
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Cash Out to bank successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }


                break;

            case ServiceCodeConstants.CashInFromUPayKiosk:

                bodySmsReceiver="Refill successful from Upay ATM Kiosk Machine :" +
                        "Amount :"+notificationRequest.getTransactionAmount()+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(),bodySmsReceiver, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Refill successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;

            case ServiceCodeConstants.CashInToEmbassyForPassport:

                bodySmsSender="Passport fee payment successful. To : Embassy consulate wallet" +
                        "for :"+notificationRequest.getPinServiceType()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();
                sendSMS(notificationRequest.getSenderWalletNo(), bodySmsSender, ExternalType.SMS);


                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());
                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Passport fee payment successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;

            case ServiceCodeConstants.HelloSuperStar:

                bodySmsSender="Hello super start product payment successful. To :"+notificationRequest.getReceiverWallet()+" " +
                        "Amount :"+notificationRequest.getTransactionAmount()+" " +
                        " "+feeSenderText +" "+

                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getSenderWalletNo(), bodySmsSender, ExternalType.SMS);

                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getSenderWalletNo());
                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Hello super start product payment successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }

                break;

            case ServiceCodeConstants.CashInFromPaynetKiosk:
                bodySmsReceiver="Wallet refill successful from Paynet Kiosk Machine :" +
                        "Amount :"+notificationRequest.getTransactionAmount()+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(),bodySmsReceiver, ExternalType.SMS);
                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Wallet refill successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }
                break;

            case ServiceCodeConstants.CashInFromEasyPayKiosk:
                bodySmsReceiver="Wallet refill successful from EasyPay Kiosk Machine :" +
                        "Amount :"+notificationRequest.getTransactionAmount()+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(),bodySmsReceiver, ExternalType.SMS);
                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Wallet refill successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }
                break;

            case ServiceCodeConstants.CashInFromZenZeroKiosk:
                bodySmsReceiver="Wallet refill successful from ZenZero Kiosk Machine :" +
                        "Amount :"+notificationRequest.getTransactionAmount()+
                        " Transaction reference :"+notificationRequest.getTransactionToken() +" "+
                        "Date-Time:"+DateUtil.getDateTimeForNotifiction();

                sendSMS(notificationRequest.getReceiverWallet(),bodySmsReceiver, ExternalType.SMS);
                senderFcmKeys=getFcmKeys(notificationRequest,notificationRequest.getReceiverWallet());

                if(senderFcmKeys!=null){
                    sendPushNotification(senderFcmKeys, "Wallet refill successful", ExternalType.NOTIFICATION,notificationRequest.getRequestId());
                }
                break;

            case ServiceCodeConstants.Master_Wallet_Deposit:
                break;
                default:
                    logWriterUtility.error(notificationRequest.getRequestId(),"Invalid service code");
                    return;
        }

    }


    private List<String> getFcmKeys(NotificationRequest notificationRequest,String wallet) {
        List<String> keys=new ArrayList<>();

        UmUserInfo umUserInfo=userInfoRepository.findByLoginId(wallet);
        if(umUserInfo==null){
            return null;
        }

        List<String> statusIn=new ArrayList<>();
        statusIn.add("1");

        List<UmUserDeviceMapping> maps=userDeviceMapRepository
                .findAllByUmUserInfoByUserIdAndStatusIn(umUserInfo,statusIn);

        if(maps==null){
            return null;
        }

        for (UmUserDeviceMapping umUserDeviceMapping:maps) {
            if(umUserDeviceMapping.getFcmKey()==null){
                continue;
            }

            keys.add(umUserDeviceMapping.getFcmKey());
        }

        return keys;
    }

    private void sendSMS(String number, String message, String type){
        CompletableFuture.runAsync(() -> {
            saveNotification(number,message,type);

            new ExternalCall(appConfiguration.getExternalUrl()).sendSMS(number,message,type);

        });
    }

    private void sendPushNotification(List<String> fcmKeys, String message, String type,String requestId){
        CompletableFuture.runAsync(() -> {

            for (String key:fcmKeys) {
                logWriterUtility.trace(requestId,"FCM KEY :"+key+" message :"+message);
                new ExternalCall(appConfiguration.getExternalUrl()).sendNotification(key,message,type);
            }


        });
    }

    private void saveNotification(String receiver,String message,String type){

        UmNotification umNotification=new UmNotification();
        umNotification.setCreatedDate(new Date());
        umNotification.setText(message);
        umNotification.setStatus("0");
        umNotification.setNotificationType(type);
        umNotification.setReceiver(receiver);

        umNotificationRepository.save(umNotification);

    }

    public void sendSMSBody(NotificationRequest notificationRequest, String bodySmsSender){
        if(notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("SLK")){
            String pinSerial = "Your Salik Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("VIRGIN")){
            String pinSerial = "Your VIRGIN Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("ETISALAT")){
            String pinSerial = "Your ETISALAT Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("DU")){
            String pinSerial = "Your DU Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("PUBG")){
            String pinSerial = "Your PUBG Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("NETFLIX")){
            String pinSerial = "Your NETFLIX Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("STEAM")){
            String pinSerial = "Your STEAM Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("HELLO_CARD")){
            String pinSerial = "Your Hello Card Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("FIVE_CARD")){
            String pinSerial = "Your Five Card Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("XBOX")){
            String pinSerial = "Your XBOX Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("AMAZON")){
            String pinSerial = "Your AMAZON Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("PSN")){
            String pinSerial = "Your Play Station Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("GPLAY")){
            String pinSerial = "Your Google Play Store Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("ITUNES")){
            String pinSerial = "Your ITUNES Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("MINECRAFT")){
            String pinSerial = "Your Minecraft Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("NINTENDO")){
            String pinSerial = "Your Nintendo Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("VIP_BALOOT")){
            String pinSerial = "Your Vip Baloot Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("BIGO_LIKE")){
            String pinSerial = "Your Bigo Like Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("BIGO_LIVE")){
            String pinSerial = "Your Bigo Live Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("SPACETOON_GO")){
            String pinSerial = "Your Spacetoon Go Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("BEIN")){
            String pinSerial = "Your BEIN Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("CRUNCHYROLL")){
            String pinSerial = "Your Crunchyroll Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("MOBILE_LEGENDS")){
            String pinSerial = "Your Mobile Legends Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("VIBER")){
            String pinSerial = "Your Viber Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("FREEFIRE")){
            String pinSerial = "Your Freefire Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("STARZPLAY")){
            String pinSerial = "Your Starz play Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("ROBLOX")){
            String pinSerial = "Your ROBLOX Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("MCAFEE")){
            String pinSerial = "Your MCAFEE Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("SKYPE")){
            String pinSerial = "Your SKYPE Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("EBAY")){
            String pinSerial = "Your EBAY Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("SHAHID_VIP")){
            String pinSerial = "Your Shahid vip Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("SPOTIFY")){
            String pinSerial = "Your SPOTIFY Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }else if (notificationRequest.getPinNumber() != null && notificationRequest.getPinServiceType().equalsIgnoreCase("RAZER")){
            String pinSerial = "Your RAZER Voucher PIN : " +  notificationRequest.getPinNumber();
            sendSMS(notificationRequest.getSenderWalletNo(),pinSerial, ExternalType.SMS);
        }
        else {
            sendSMS(notificationRequest.getSenderWalletNo(),bodySmsSender, ExternalType.SMS);
        }
    }

}
