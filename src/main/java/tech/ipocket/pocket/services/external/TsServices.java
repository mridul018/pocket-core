package tech.ipocket.pocket.services.external;

import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.um.TransactionValidityUmRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.um.TransactionValidityUmResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface TsServices {
    BaseResponseObject getTransactionValidity(TransactionValidityUmRequest request) throws PocketException;

    boolean callTxForValidatingWalletBalance(BalanceCheckRequest balanceInqueryRequest);

    TransactionValidityUmResponse callTxForTransactionVisibility(TransactionValidityUmRequest visibilityCheckRequest) throws PocketException;
}
