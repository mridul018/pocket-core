package tech.ipocket.pocket.services.external;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TopupConfig {

    @Value("${country_list}")
    private String countryListUrl;

    @Value("${api_key}")
    private String apiKey;

    @Value("${provider_list}")
    private String providerListUrl;

    @Value("${products_info}")
    private String productInfoUrl;

    @Value("${send_transfer}")
    private String sendTransfer;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCountryListUrl() {
        return countryListUrl;
    }

    public void setCountryListUrl(String countryListUrl) {
        this.countryListUrl = countryListUrl;
    }

    public String getProviderListUrl() {
        return providerListUrl;
    }

    public void setProviderListUrl(String providerListUrl) {
        this.providerListUrl = providerListUrl;
    }

    public String getProductInfoUrl() {
        return productInfoUrl;
    }

    public void setProductInfoUrl(String productInfoUrl) {
        this.productInfoUrl = productInfoUrl;
    }

    public String getSendTransfer() {
        return sendTransfer;
    }

    public void setSendTransfer(String sendTransfer) {
        this.sendTransfer = sendTransfer;
    }
}
