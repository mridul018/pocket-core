package tech.ipocket.pocket.services.external;

import org.json.JSONException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import tech.ipocket.pocket.request.external.ExternalRequest;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.utils.ExternalType;
import tech.ipocket.pocket.utils.PocketException;

@Component
public class ExternalFactory implements ApplicationContextAware {
    private ApplicationContext context;

    @Autowired
    private ExternalService externalService;

    public ExternalResponse externalServices(ExternalRequest request) throws PocketException, JSONException {

        switch (request.getType()) {
            case ExternalType.EMAIL:
                return externalService.sendEmail(request);
            case ExternalType.SMS:
                try {
                    if(request.getTo().substring(0, 3).equalsIgnoreCase("+97")){
                        return externalService.sendSMS(request);
                    }else if(request.getTo().substring(0, 3).equalsIgnoreCase("+88")){
                        return externalService.sendBDSMS(request);
                    }else {
                        return new ExternalResponse();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            case ExternalType.NOTIFICATION:
                return externalService.sendNotification(request);
            case ExternalType.TOPUP_DING:
                return externalService.sendTopUp(request.getDingTopUpRequest().getSkuCode(),
                        request.getAmount(),
                        request.getTo(),
                        request.getDingTopUpRequest().getRefNumber(), request.getDingTopUpRequest().getTransferAmount());
            case ExternalType.SSL_TOPUP:
                return externalService.sslTopUp(request.getSslTopUpRequest().getClient_id(),
                        request.getSslTopUpRequest().getClient_pass(),
                        request.getSslTopUpRequest().getGuid(),
                        request.getSslTopUpRequest().getOperator_id(),
                        request.getSslTopUpRequest().getRecipient_msisdn(),
                        request.getAmount(),
                        request.getSslTopUpRequest().getConnection_type(),
                        request.getSslTopUpRequest().getSender_id(),
                        request.getSslTopUpRequest().getPriority(),
                        request.getSslTopUpRequest().getSuccess_url(),
                        request.getSslTopUpRequest().getFailure_url());
            case ExternalType.PAYWELL_TOPUP:
                return externalService.payWellTopUp(request.getPayWellTopUpRequest().getClientid(),
                        request.getPayWellTopUpRequest().getClientpass(),
                        request.getPayWellTopUpRequest().getCrid(),
                        request.getPayWellTopUpRequest().getMsisdn(),
                        request.getAmount().toString(),
                        request.getPayWellTopUpRequest().getConnection(),
                        request.getPayWellTopUpRequest().getOperator(),
                        request.getPayWellTopUpRequest().getSender());
            case ExternalType.POLLIBUDDIT_BILLPAY:
//                return externalService.payWellPollibudditBillPay(request.getPolliBudditBillRequest(),request.getTo());
                return externalService.payWellPollyBiddyutBillPayment(request.getPolliBudditBillRequest(),request.getTo());
            case ExternalType.DPDC_BILLPAY:
                return null;
            case ExternalType.DESCO_BILLPAY:
                return null;
            case ExternalType.WASA_BILLPAY:
                return null;
            case ExternalType.FLIGHT_BOOKING:
                return externalService.flightBooking(request.getFlightBookingRequest());
            case ExternalType.FLIGHT_ISSUE:
                return externalService.flightIssue(request.getFlightIssueRequest());
            case ExternalType.POLLIBUDDIT_BILLPAY_CONFIRMATION:
                return externalService.findBillPayStatus(request.getPolliBudditBillRequest());

        }


        return null;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
