package tech.ipocket.pocket.services.external;

import org.json.JSONException;
import tech.ipocket.pocket.entity.DapiAccessToken;
import tech.ipocket.pocket.repository.topup.Product;
import tech.ipocket.pocket.repository.topup.Provider;
import tech.ipocket.pocket.request.external.*;
import tech.ipocket.pocket.request.external.amy.*;
import tech.ipocket.pocket.request.external.banks.*;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.request.um.UmUpdateClientStatusRequest;
import tech.ipocket.pocket.response.external.amy.*;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.response.external.banks.*;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.PocketException;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public interface ExternalService {

    ExternalResponse sendEmail(ExternalRequest request) throws PocketException;

    ExternalResponse sendSMS(ExternalRequest request) throws PocketException, IOException;

    ExternalResponse sendBDSMS(ExternalRequest request) throws PocketException;

    ExternalResponse sendNotification(ExternalRequest request) throws PocketException;

    ExternalResponse getCountryList() throws PocketException, IOException;

    Provider getProviderList(String countryIsos) throws PocketException;

    Product getProductInfo(ProductInfo request) throws PocketException;

    ExternalResponse sendTopUp(String skuCode, BigInteger amount, String senderNumber, String topUpTransactionNumber,BigDecimal transferAmount) throws PocketException, JSONException;

    TransactionSummaryViewResponse callTxForTransactionVisibility(TransactionValidityRequest transactionSummaryRequest) throws PocketException;

    Boolean callTxForUpdateClientStatus(UmUpdateClientStatusRequest updateClientStatusRequest) throws PocketException;

    ExternalResponse sslTopUp(String client_id, String client_pass, String guid, BigInteger operator_id, String recipient_msisdn, BigInteger amount, String connection_type, String sender_id, BigInteger priority, String success_url, String failure_url) throws PocketException;

    ExternalResponse payWellTopUp(String clientid, String clientpass, String crid, String msisdn, String amount, String connection, String operator, String sender) throws PocketException;

    ExternalResponse payWellPollibudditBillPay(PolliBudditBillRequest request, String phn) throws PocketException;
    ExternalResponse payWellPollyBiddyutBillPayment(PolliBudditBillRequest request, String phn) throws PocketException;

    DepartureListResponse getDepartureList(DepartureListRequest request) throws PocketException;

    DestinationListResponse getDestinationList(DestinationListRequest request) throws PocketException;

    FlightSearchResponse getFlightSearchResponse(FlightSearchRequest request) throws PocketException;

    FlightPriceResponse getFlightPrices(FlightPriceRequest request) throws PocketException;

    PassengerValidationResponse passengerValidation(PassengerValidationRequest request) throws PocketException;

    FlightDocumentUploadResponse documentUpload(FlightDocumentUploadRequest request) throws PocketException;

    ExternalResponse flightBooking(FlightBookingRequest request) throws PocketException;

    ExternalResponse flightIssue(FlightIssueRequest request) throws PocketException;

    ExternalResponse findBillPayStatus(PolliBudditBillRequest request) throws PocketException;

    BankListResponse bankListByUserPhone(BankListRequest request) throws PocketException;

    BankAddResponse addANewBank(BankAddRequest request) throws PocketException;

    BankMetadataResponse bankData(BankDataRequest request) throws PocketException;

    BankUserInputResponse sendBankUserInput(BankUserInputRequest request) throws PocketException;

    BankBeneficiaryAddResponse bankBeneficiaryAdd(BankBeneficiaryAddRequest request) throws PocketException;

    BankBeneficiaryGetResponse bankBeneficiaryGet(BankBeneficiaryGetRequest request) throws PocketException;

    Boolean storeAccessToken(StoreAccessToken storeAccessToken);
    DapiAccessToken getAccessToken(String tokenId);
    List<DapiAccessToken> getAccessToken();
}
