package tech.ipocket.pocket.services.external;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SMSConfig {
    @Value("${spring.sms.username}")
    private String username;
    @Value("${spring.sms.password}")
    private String password;
    @Value("${spring.sms.url}")
    private String url;
    @Value("${spring.sms.from}")
    private String from;
    @Value("${spring.sms.api.key}")
    private String apikey;
    @Value("${spring.bd.sms.url}")
    private String bdUrl;
    @Value("${spring.bd.sms.api.key}")
    private String bdApikey;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getBdUrl() {
        return bdUrl;
    }

    public void setBdUrl(String bdUrl) {
        this.bdUrl = bdUrl;
    }

    public String getBdApikey() {
        return bdApikey;
    }

    public void setBdApikey(String bdApikey) {
        this.bdApikey = bdApikey;
    }
}
