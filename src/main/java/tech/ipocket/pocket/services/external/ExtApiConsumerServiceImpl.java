package tech.ipocket.pocket.services.external;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;
import tech.ipocket.pocket.entity.ExtApiConsumer;
import tech.ipocket.pocket.repository.external.ExtApiConsumerRepository;
import tech.ipocket.pocket.request.integration.UPayBaseRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayBaseRequest;
import tech.ipocket.pocket.request.integration.hss.HssBaseRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetBaseRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayBaseRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;

import static tech.ipocket.pocket.utils.integration.UpayCommonTask.*;

@Service
public class ExtApiConsumerServiceImpl implements ExtApiConsumerService{

    LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());
    private ExtApiConsumerRepository extApiConsumerRepository;

    public ExtApiConsumerServiceImpl(ExtApiConsumerRepository extApiConsumerRepository) {
        this.extApiConsumerRepository = extApiConsumerRepository;
    }

    @Override
    public boolean checkConsumerValidity(HttpServletRequest httpServletRequest, UPayBaseRequest request) throws PocketException {

        String signature = httpServletRequest.getHeader("UPAY_SIGNATURE");
        String ipAddress = httpServletRequest.getRemoteAddr(); // for remote address

        // convert input into string (payload)
        String payload = new Gson().toJson(request);

        logWriterUtility.error(request.getRequestId(), "API consumer info -> Signature :"+signature+ " ipAddress :"+ipAddress + " Date :"+new Date());

        ExtApiConsumer apiConsumer = extApiConsumerRepository
                .findFirstByIpAddressAndStatus(ipAddress, 1);

        if(apiConsumer == null){
            logWriterUtility.error(request.getRequestId(), "API consumer info invalid ."+ ipAddress +":+ipAddress");
            throw new PocketException(PocketErrorCode.API_CONSUMER_INFO_INVALID.getKeyString(), "Invalid API consumer provided information.");
        }

        //TODO: Verify consumer RSA key
        if (signature == null){
            logWriterUtility.error(request.getRequestId(), "API consumer RSA key invalid.");
            throw new PocketException(PocketErrorCode.RSA_KEY_NOT_FOUND, "API consumer RSA key invalid.");
        }

        try {
            if (!verify(payload,signature)){
                logWriterUtility.error(request.getRequestId(), "Signature verification failed");
                throw new PocketException(PocketErrorCode.RSA_VERIFICATION_FAILED, "Signature verification failed");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | IOException | InvalidKeySpecException e) {
            throw new PocketException(PocketErrorCode.RSA_OTHERS, "Others problem. Please try again.");
        }

        return true;
    }

    @Override
    public String signPayload(BaseResponseObject responseObject) throws PocketException {
        String signature = null;
        String payload = new Gson().toJson(responseObject);
        try {
            signature = sign(payload);
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | InvalidKeySpecException | SignatureException e) {
            throw new PocketException(PocketErrorCode.RSA_OTHERS, "Others problem. Please try again.");
        }
        return signature;
    }

    private String sign(String message) throws NoSuchAlgorithmException, InvalidKeyException, IOException, InvalidKeySpecException, SignatureException {
        logWriterUtility.info("Signature Method", "Message: "+message);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(getPrivateKey());
        signature.update(message.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(signature.sign()),StandardCharsets.UTF_8);
    }

    private boolean verify(String message, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
        logWriterUtility.info("Verify Method", "Message: "+message+" and signature: "+signature);
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getPublicKey());
        sign.update(message.getBytes(StandardCharsets.UTF_8));
        return sign.verify(Base64.decodeBase64(signature));
    }

    @Override
    public boolean checkHssConsumerValidity(HttpServletRequest httpServletRequest, HssBaseRequest request) throws PocketException {
        String signature = httpServletRequest.getHeader("HSS_SIGNATURE");
        String ipAddress = httpServletRequest.getRemoteAddr();

        // convert input into string (payload)
        String payload = new Gson().toJson(request);

        logWriterUtility.error(request.getRequestId(), "API consumer info -> Signature :"+signature+ " ipAddress :"+ipAddress + " Date :"+new Date());

        ExtApiConsumer apiConsumer = extApiConsumerRepository.findFirstByIpAddressAndClientTokenIgnoreCaseAndStatus(ipAddress, request.getClientToken(), 1);

        if(apiConsumer == null){
            logWriterUtility.error(request.getRequestId(), "API consumer info invalid ."+ ipAddress +":+ipAddress");
            throw new PocketException(PocketErrorCode.API_CONSUMER_INFO_INVALID.getKeyString(), "Invalid API consumer provided information.");
        }

        // Verify consumer RSA key
        if (signature == null){
            logWriterUtility.error(request.getRequestId(), "API consumer RSA key invalid.");
            throw new PocketException(PocketErrorCode.RSA_KEY_NOT_FOUND, "API consumer RSA key invalid.");
        }

        try {
            if (!verifyHss(payload,signature)){
                logWriterUtility.error(request.getRequestId(), "Signature verification failed");
                throw new PocketException(PocketErrorCode.RSA_VERIFICATION_FAILED, "Signature verification failed");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | IOException | InvalidKeySpecException e) {
            throw new PocketException(PocketErrorCode.RSA_OTHERS, "Others problem. Please try again.");
        }

        return true;
    }

    @Override
    public String signHssPayload(BaseResponseObject responseObject) throws PocketException {
        String signature = null;
        String payload = new Gson().toJson(responseObject);
        try {
            signature = signHss(payload);
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | InvalidKeySpecException | SignatureException e) {
            throw new PocketException(PocketErrorCode.RSA_OTHERS, "Others problem. Please try again.");
        }
        return signature;
    }


    private String signHss(String message) throws NoSuchAlgorithmException, InvalidKeyException, IOException, InvalidKeySpecException, SignatureException {
        logWriterUtility.info("Signature Method", "Message: "+message);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(getHssPrivateKey());
        signature.update(message.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(signature.sign()),StandardCharsets.UTF_8);
    }

    private boolean verifyHss(String message, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
        logWriterUtility.info("Verify Method", "Message: "+message+" and signature: "+signature);
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getHssPublicKey());
        sign.update(message.getBytes(StandardCharsets.UTF_8));
        return sign.verify(Base64.decodeBase64(signature));
    }


    @Override
    public boolean checkPaynetConsumerValidity(HttpServletRequest httpServletRequest, PaynetBaseRequest request) throws PocketException {
        String signature = httpServletRequest.getHeader("PAYNET_SIGNATURE");
        String ipAddress = httpServletRequest.getRemoteAddr(); // remote address

        // convert input into string (payload)
        String payload = new Gson().toJson(request);

        logWriterUtility.error(request.getRequestId(), "API consumer info -> Signature :"+signature+ " ipAddress :"+ipAddress + " Date :"+new Date());

        ExtApiConsumer apiConsumer = extApiConsumerRepository.findFirstByIpAddressAndStatus(ipAddress, 1);

        if(apiConsumer == null){
            logWriterUtility.error(request.getRequestId(), "API consumer info invalid ."+ ipAddress +":+ipAddress");
            throw new PocketException(PocketErrorCode.PAYNET_API_CONSUMER_INFO_INVALID.getKeyString(), "Invalid API consumer provided information.");
        }

        //TODO: Verify consumer RSA key
        if (signature == null){
            logWriterUtility.error(request.getRequestId(), "API consumer RSA key invalid.");
            throw new PocketException(PocketErrorCode.PAYNET_RSA_KEY_NOT_FOUND, "API consumer RSA key invalid.");
        }

        try {
            if (!verifyPaynetPublicKey(payload,signature)){
                logWriterUtility.error(request.getRequestId(), "Signature verification failed");
                throw new PocketException(PocketErrorCode.PAYNET_RSA_VERIFICATION_FAILED, "Signature verification failed");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | IOException | InvalidKeySpecException e) {
            throw new PocketException(PocketErrorCode.PAYNET_RSA_OTHERS, "Others problem. Please try again.");
        }

        return true;
    }
    @Override
    public String signPaynetPayload(BaseResponseObject responseObject) throws PocketException {
        String signature = null;
        String payload = new Gson().toJson(responseObject);
        try {
            signature = signPaynetPrivateKey(payload);
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | InvalidKeySpecException | SignatureException e) {
            throw new PocketException(PocketErrorCode.PAYNET_RSA_OTHERS, "Others problem. Please try again.");
        }
        return signature;
    }
    private String signPaynetPrivateKey(String message) throws NoSuchAlgorithmException, InvalidKeyException, IOException, InvalidKeySpecException, SignatureException {
        logWriterUtility.info("Signature Method", "Message: "+message);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(getPaynetPrivateKey());
        signature.update(message.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(signature.sign()),StandardCharsets.UTF_8);
    }

    private boolean verifyPaynetPublicKey(String message, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
        logWriterUtility.info("Verify Method", "Message: "+message+" and signature: "+signature);
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getPaynetPublicKey());
        sign.update(message.getBytes(StandardCharsets.UTF_8));
        return sign.verify(Base64.decodeBase64(signature));
    }

    @Override
    public boolean checkEasyPayConsumerValidity(HttpServletRequest httpServletRequest, EasyPayBaseRequest request) throws PocketException {
        String signature = httpServletRequest.getHeader("EASYPAY_SIGNATURE");
        String ipAddress = httpServletRequest.getRemoteAddr(); // remote address

        // convert input into string (payload)
        String payload = new Gson().toJson(request);

        logWriterUtility.error(request.getRequestId(), "API consumer info -> Signature :"+signature+ " ipAddress :"+ipAddress + " Date :"+new Date());

        ExtApiConsumer apiConsumer = extApiConsumerRepository.findFirstByIpAddressAndStatus(ipAddress, 1);

        if(apiConsumer == null){
            logWriterUtility.error(request.getRequestId(), "API consumer info invalid ."+ ipAddress +":+ipAddress");
            throw new PocketException(PocketErrorCode.EASYPAY_API_CONSUMER_INFO_INVALID.getKeyString(), "Invalid API consumer provided information.");
        }

        //Verify RSA key
        if (signature == null){
            logWriterUtility.error(request.getRequestId(), "API consumer RSA key invalid.");
            throw new PocketException(PocketErrorCode.EASYPAY_RSA_KEY_NOT_FOUND, "API consumer RSA key invalid.");
        }

        try {
            if (!verifyEasyPayPublicKey(payload,signature)){
                logWriterUtility.error(request.getRequestId(), "Signature verification failed");
                throw new PocketException(PocketErrorCode.EASYPAY_RSA_VERIFICATION_FAILED, "Signature verification failed");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | IOException | InvalidKeySpecException e) {
            throw new PocketException(PocketErrorCode.EASYPAY_RSA_OTHERS, "Others problem. Please try again.");
        }

        return true;
    }
    @Override
    public String signEasyPayPayload(BaseResponseObject responseObject) throws PocketException {
        String signature = null;
        String payload = new Gson().toJson(responseObject);
        try {
            signature = signEasyPayPrivateKey(payload);
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | InvalidKeySpecException | SignatureException e) {
            throw new PocketException(PocketErrorCode.EASYPAY_RSA_OTHERS, "Others problem. Please try again.");
        }
        return signature;
    }
    private String signEasyPayPrivateKey(String message) throws NoSuchAlgorithmException, InvalidKeyException, IOException, InvalidKeySpecException, SignatureException {
        logWriterUtility.info("Signature Method", "Message: "+message);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(getEasyPayPrivateKey());
        signature.update(message.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(signature.sign()),StandardCharsets.UTF_8);
    }

    private boolean verifyEasyPayPublicKey(String message, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
        logWriterUtility.info("Verify Method", "Message: "+message+" and signature: "+signature);
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getEasyPayPublicKey());
        sign.update(message.getBytes(StandardCharsets.UTF_8));
        return sign.verify(Base64.decodeBase64(signature));
    }
    @Override
    public boolean checkZenZeroConsumerValidity(HttpServletRequest httpServletRequest, ZenZeroPayBaseRequest request) throws PocketException {
        String signature = httpServletRequest.getHeader("ZENZEROPAY_SIGNATURE");
        String ipAddress = httpServletRequest.getRemoteAddr(); // remote address

        // convert input into string (payload)
        String payload = new Gson().toJson(request);

        logWriterUtility.error(request.getRequestId(), "API consumer info -> Signature :"+signature+ " ipAddress :"+ipAddress + " Date :"+new Date());

        ExtApiConsumer apiConsumer = extApiConsumerRepository.findFirstByIpAddressAndStatus(ipAddress, 1);

        if(apiConsumer == null){
            logWriterUtility.error(request.getRequestId(), "API consumer info invalid ."+ ipAddress +":+ipAddress");
            throw new PocketException(PocketErrorCode.ZENZERO_API_CONSUMER_INFO_INVALID.getKeyString(), "Invalid API consumer provided information.");
        }

        // Verify RSA key
        if (signature == null){
            logWriterUtility.error(request.getRequestId(), "API consumer RSA key invalid.");
            throw new PocketException(PocketErrorCode.ZENZERO_RSA_KEY_NOT_FOUND, "API consumer RSA key invalid.");
        }

        try {
            if (!verifyZenZeroPublicKey(payload,signature)){
                logWriterUtility.error(request.getRequestId(), "Signature verification failed");
                throw new PocketException(PocketErrorCode.ZENZERO_RSA_VERIFICATION_FAILED, "Signature verification failed");
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | IOException | InvalidKeySpecException e) {
            throw new PocketException(PocketErrorCode.ZENZERO_RSA_OTHERS, "Others problem. Please try again.");
        }

        return true;
    }

    @Override
    public String signZenZeroPayload(BaseResponseObject responseObject) throws PocketException {
        String signature = null;
        String payload = new Gson().toJson(responseObject);
        try {
            signature = signZenZeroPrivateKey(payload);
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException | InvalidKeySpecException | SignatureException e) {
            throw new PocketException(PocketErrorCode.ZENZERO_RSA_OTHERS, "Others problem. Please try again.");
        }
        return signature;
    }
    private String signZenZeroPrivateKey(String message) throws NoSuchAlgorithmException, InvalidKeyException, IOException, InvalidKeySpecException, SignatureException {
        logWriterUtility.info("Signature Method", "Message: "+message);
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(getZenZeroPrivateKey());
        signature.update(message.getBytes(StandardCharsets.UTF_8));
        return new String(Base64.encodeBase64(signature.sign()),StandardCharsets.UTF_8);
    }

    private boolean verifyZenZeroPublicKey(String message, String signature) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException, IOException, InvalidKeySpecException {
        logWriterUtility.info("Verify Method", "Message: "+message+" and signature: "+signature);
        Signature sign = Signature.getInstance("SHA1withRSA");
        sign.initVerify(getZenZeroPublicKey());
        sign.update(message.getBytes(StandardCharsets.UTF_8));
        return sign.verify(Base64.decodeBase64(signature));
    }
}
