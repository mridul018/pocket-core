package tech.ipocket.pocket.services.external;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import tech.ipocket.pocket.configuration.AmyConfig;
import tech.ipocket.pocket.configuration.BankConfig;
import tech.ipocket.pocket.configuration.PayWellConfig;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.topup.*;
import tech.ipocket.pocket.repository.topup.Country;
import tech.ipocket.pocket.repository.ts.AccessTokenRepository;
import tech.ipocket.pocket.repository.ts.TsConversionRateRepository;
import tech.ipocket.pocket.repository.um.UmAmySessionRepository;
import tech.ipocket.pocket.repository.um.UmBankListRepository;
import tech.ipocket.pocket.repository.um.UmBeneficiaryRepository;
import tech.ipocket.pocket.request.external.*;
import tech.ipocket.pocket.request.external.banks.*;
import tech.ipocket.pocket.request.ts.paywell.PollyBiddyutBillPay;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.request.um.UmUpdateClientStatusRequest;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.external.*;
import tech.ipocket.pocket.response.external.amy.*;
import tech.ipocket.pocket.request.external.amy.*;
import tech.ipocket.pocket.response.external.banks.*;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.TransactionSummaryViewResponse;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.BillPayTypeConstants;
import tech.ipocket.pocket.wsdl.paywell.RechargerequestserverLocator;
import tech.ipocket.pocket.wsdl.paywell.RechargerequestserverPortType;
import tech.ipocket.pocket.wsdl.paywell.ResultObject1;
//import tech.ipocket.pocket.wsdl.sms.SendSmsLocator;
//import tech.ipocket.pocket.wsdl.sms.SendSmsSoap_PortType;
import tech.ipocket.pocket.wsdl.ssl.RechargeResponseDataType;
import tech.ipocket.pocket.wsdl.ssl.VirtualrechargePortType;
import tech.ipocket.pocket.wsdl.ssl.VirtualrechargeServiceLocator;

//import javax.crypto.Mac;
//import javax.crypto.spec.SecretKeySpec;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ExternalServiceImpl implements ExternalService {

    LogWriterUtility logWriterUtility = new LogWriterUtility(ExternalServiceImpl.class);

    @Autowired
    private EmailConfig emailConfig;

    @Autowired
    private SMSConfig smsConfig;

    @Autowired
    private NotificationConfig notificationConfig;

    @Autowired
    private Environment environment;

    @Autowired
    private TopupConfig topupConfig;

    @Autowired
    private PayWellConfig payWellConfig;

    @Autowired
    private AmyConfig amyConfig;

    @Autowired
    private BankConfig bankConfig;

    @Autowired
    private UmBeneficiaryRepository beneficiaryRepository;

    @Autowired
    private TsConversionRateRepository tsConversionRateRepository;

    @Autowired
    private UmAmySessionRepository amySessionRepository;

    @Autowired
    private UmBankListRepository bankListRepository;

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private UmBeneficiaryRepository umBeneficiaryRepository;

    /**
     *
     * FOR EMAIL
     */
    @Override
    public ExternalResponse sendEmail(ExternalRequest request) throws PocketException {

        try {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setPort(emailConfig.getPort());
            mailSender.setHost(emailConfig.getHost());
            mailSender.setUsername(emailConfig.getUserName());
            mailSender.setPassword(emailConfig.getPassword());
            mailSender.setJavaMailProperties(getProperties());
            mailSender.setSession(getSession());


            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setFrom(emailConfig.getUserName());
            mailMessage.setTo(request.getTo());
            mailMessage.setSubject(request.getSubject());
            mailMessage.setText(request.getBody());

            mailSender.send(mailMessage);



            return getResponse("Email send successfully","200","Email send successfully");
        }catch (Exception ex){
            System.err.println("Email sending error: "+ex.getMessage());
        }

        return getResponse(null,PocketErrorCode.EMAIL_NOT_SEND.getKeyString(),"Email not send");

    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.starttls.enable", "true");

        return properties;
    }

    private Session getSession() {
        return Session.getDefaultInstance(getProperties(), new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailConfig.getUserName(), emailConfig.getPassword());
            }
        });
    }


    /**
     * FOR Internal SMS
     */
    @Override
    public ExternalResponse sendSMS(ExternalRequest request) throws PocketException, IOException {

        ExternalResponse response = new ExternalResponse();

//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.setBasicAuth(smsConfig.getUsername(), smsConfig.getPassword());


        SmsSendRequest smsSendRequest = new SmsSendRequest();
        smsSendRequest.setFrom(smsConfig.getFrom());
        smsSendRequest.setTo(request.getTo());
        smsSendRequest.setText(request.getBody());

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        String bodyJson = String.format("{\"messages\":[{\"from\":\"%s\",\"destinations\":[{\"to\":\"%s\"}],\"text\":\"%s\"}]}",
                          smsSendRequest.getFrom(),
                          smsSendRequest.getTo(),
                          smsSendRequest.getText());

        okhttp3.MediaType mediaType = okhttp3.MediaType.parse(String.valueOf(MediaType.APPLICATION_JSON));
        RequestBody requestBody = RequestBody.create(bodyJson, mediaType);

        Request requestSMS = new Request.Builder()
                .url(String.format(smsConfig.getUrl()))
                .method("POST", requestBody)
                .addHeader("Authorization", "App " + smsConfig.getApikey())
                .addHeader("Content-Type", String.valueOf(MediaType.APPLICATION_JSON))
                .addHeader("Accept", String.valueOf(MediaType.APPLICATION_JSON))
                .build();

        Response responseSMS = client.newCall(requestSMS).execute();
        logWriterUtility.debug(request.getRequestId(), "-------- sms api call finish ---------");

        if(responseSMS.code() == HttpStatus.OK.value()){
            logWriterUtility.debug(request.getRequestId(), "-------- Successfully send sms ---------");
            response.setError(null);
            response.setAbleToSend(true);
            response.setData("SMS send successfully");
            response.setStatus(PocketConstants.OK);
        }else{
            logWriterUtility.debug(request.getRequestId(), "-------- failed to send sms ---------");
            response.setError(new ErrorObject(PocketErrorCode.SMS_NOT_SEND));
            response.setAbleToSend(false);
            response.setData("SMS not send");
            response.setStatus(PocketConstants.ERROR);
        }

        // for testing
//        logWriterUtility.debug(request.getRequestId(), "-------- before call sms api ---------");
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<String> exchange = restTemplate.exchange(smsConfig.getUrl(),
//                HttpMethod.POST, new HttpEntity<>(smsSendRequest, headers),
//                String.class);
//        logWriterUtility.debug(request.getRequestId(), "-------- sms api call finish ---------");
//        if(exchange.getStatusCode()==HttpStatus.OK){
//            logWriterUtility.debug(request.getRequestId(), "-------- Successfully send sms ---------");
//            response.setError(null);
//            response.setAbleToSend(true);
//            response.setData("SMS send successfully");
//            response.setStatus(PocketConstants.OK);
//        }else{
//            logWriterUtility.debug(request.getRequestId(), "-------- failed to send sms ---------");
//            response.setError(new ErrorObject(PocketErrorCode.SMS_NOT_SEND));
//            response.setAbleToSend(false);
//            response.setData("SMS not send");
//            response.setStatus(PocketConstants.ERROR);
//        }

        return response;
    }

    /**
     *
     * FOR LOCAL SMS
     */

    @Override
    public ExternalResponse sendBDSMS(ExternalRequest request) throws PocketException {
        ExternalResponse response = new ExternalResponse();
        try {
            SmsSendRequest smsSendRequest = new SmsSendRequest();
            smsSendRequest.setFrom(smsConfig.getFrom());
            smsSendRequest.setTo(request.getTo());
            smsSendRequest.setText(request.getBody());

            Unirest.setTimeouts(0,0);
            HttpResponse<String> smsResponse = Unirest.post(smsConfig.getBdUrl())
                    .field("api_key", smsConfig.getBdApikey())
                    .field("msg", smsSendRequest.getText())
                    .field("to", smsSendRequest.getTo())
                    .asString();

            logWriterUtility.info(smsResponse.getBody(), "Response Status Code : " +smsResponse.getStatus());
            if(smsResponse.getStatus() == HttpStatus.OK.value()){
                logWriterUtility.debug(request.getRequestId(), "-------- Successfully send sms ---------");
                response.setError(null);
                response.setAbleToSend(true);
                response.setData("SMS send successfully");
                response.setStatus(PocketConstants.OK);
            }else{
                logWriterUtility.debug(request.getRequestId(), "-------- failed to send sms ---------");
                response.setError(new ErrorObject(PocketErrorCode.SMS_NOT_SEND));
                response.setAbleToSend(false);
                response.setData("SMS not send");
                response.setStatus(PocketConstants.ERROR);
            }

//            SendSmsLocator smsLocator = new SendSmsLocator();
//            SendSmsSoap_PortType smsSoap_portType = smsLocator.getSendSmsSoap();
//            String text = smsSoap_portType.oneToOne(smsConfig.getUsername(), smsConfig.getPassword(), request.getTo(), request.getBody(), "TEXT", "", "");
//
//            logWriterUtility.info(request.getRequestId(),text);
//            if(text!=null&&!text.isEmpty()){
//                if(text.substring(0,4).equalsIgnoreCase("1900")){
//                    response.setError(null);
//                    response.setAbleToSend(true);
//                    response.setData("SMS send successfully");
//                    response.setStatus(PocketConstants.OK);
//                }else{
//                    response.setError(new ErrorObject(PocketErrorCode.SMS_NOT_SEND));
//                    response.setAbleToSend(false);
//                    response.setData("SMS not send");
//                    response.setStatus(PocketConstants.ERROR);
//                }
//            }else{
//                response.setError(new ErrorObject(PocketErrorCode.SMS_NOT_SEND));
//                response.setAbleToSend(false);
//                response.setData("SMS not send");
//                response.setStatus(PocketConstants.ERROR);
//            }
        }catch (Exception ex){
            throw new PocketException(ex.getMessage());
        }
        return response;
    }

    /**
     * FOR Mobile Notification
     */

    @Override
    public ExternalResponse sendNotification(ExternalRequest request) throws PocketException {

        ExternalResponse response = new ExternalResponse();
        try {
            JSONObject object = new JSONObject();

            JSONObject notificationObject=new JSONObject();

            notificationObject.put("title",request.getSubject());
            notificationObject.put("body",request.getBody());

            object.put("notification",notificationObject);
            object.put("data",notificationObject);
            object.put("to",request.getTo());


            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Authorization","key=" + notificationConfig.getServerKey());
            httpHeaders.set("Content-type","application/json");
            HttpEntity<String> httpEntity = new HttpEntity<>(object.toString(), httpHeaders);


            CompletableFuture<String> fcmResponse = send(httpEntity);
            CompletableFuture.allOf(fcmResponse).join();

            try {
                fcmResponse.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            response.setAbleToSend(true);
        }catch (Exception ex){
            response.setAbleToSend(false);
        }

        return response;
    }

    @Async
    public CompletableFuture<String> send(HttpEntity<String> entity){
        RestTemplate restTemplate = new RestTemplate();

        String fireballResponse = restTemplate.postForObject(notificationConfig.getUrl(),entity,String.class);
        return CompletableFuture.completedFuture(fireballResponse);
    }

    /**
     * DING (Top Up) related API implementation
     */

    @Override
    public ExternalResponse getCountryList() throws PocketException, IOException {

        ExternalResponse response = new ExternalResponse();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("api_key",topupConfig.getApiKey());

        RestTemplate countryListCall = new RestTemplate();
        ResponseEntity<String> exchange = countryListCall.exchange(topupConfig.getCountryListUrl(), HttpMethod.GET, new HttpEntity<>(headers), String.class);
        if(exchange.getStatusCode()==HttpStatus.OK){
            response.setData(new Gson().fromJson(exchange.getBody(), Country.class));
            response.setError(null);
            response.setStatus(PocketConstants.OK);
            return response;
        }
        throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
    }

    @Override
    public Provider getProviderList(String countryIsos) throws PocketException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("api_key",topupConfig.getApiKey());

        String url = String.format(topupConfig.getProviderListUrl(),countryIsos);
        RestTemplate providerCall = new RestTemplate();
        ResponseEntity<String> exchange = providerCall.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
        if(exchange.getStatusCode()==HttpStatus.OK){
            Provider provider = new Gson().fromJson(exchange.getBody(), Provider.class);

            //Filter Only for AE(DU, Etisalt prepaid)
            if(countryIsos.equalsIgnoreCase("AE")){
                List<ProviderItem> providerItemList = provider.providerItems;
                List<ProviderItem> providerItemsTemp = new ArrayList<ProviderItem>();
                if(providerItemList.size()>0){
                   for (ProviderItem p : providerItemList){
                       if(p.providerCode.equalsIgnoreCase("E6AE") || p.providerCode.equalsIgnoreCase("ETAE")){
                           providerItemsTemp.add(p);
                       }
                   }
                    provider.providerItems = providerItemsTemp;
                }
                return provider;
            }

            return provider;
        }
        return null;
    }

    @Override
    public Product getProductInfo(ProductInfo request) throws PocketException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("api_key",topupConfig.getApiKey());

        String url = String.format(topupConfig.getProductInfoUrl(),request.getCountryIsos(),request.getProviderCode());
        RestTemplate productCall = new RestTemplate();
        ResponseEntity<String> exchange = productCall.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), String.class);
        if(exchange.getStatusCode()==HttpStatus.OK){
            try {
                Product product = new Gson().fromJson(exchange.getBody(), Product.class);

                if(product!=null){
                    if(product.productItems!=null&&product.productItems.size()>0&&product.productItems.get(0).maximum!=null){

                        Double receiveValue= (Double) product.productItems.get(0).maximum.receiveValue;
                        Double sendValue= (Double) product.productItems.get(0).maximum.sendValue;

                        Double conversionRate= (double) 0;

                        if(request.getCurrencyCode()!=null&&request.getCurrencyCode().equalsIgnoreCase("AED")
                                &&request.getCountryIsos().equalsIgnoreCase("BD")){
                            //todo convert for bangladesh

                            TsConversionRate tsConversionRate = tsConversionRateRepository
                                    .findFirstByServiceCodeAndStatusOrderByIdDesc(BillPayTypeConstants.TOPUP, "1");
                            if(tsConversionRate==null){
                                logWriterUtility.error(request.getRequestId(),"Conversion rate for TopUp not found");
                                throw new PocketException(PocketErrorCode.TopUpConversionRateNotFound);
                            }
                            logWriterUtility.trace(request.getRequestId(),"Conversion rate for TopUp found:"+tsConversionRate.getConversionRate());

                            conversionRate=tsConversionRate.getConversionRate();

                        }else{
                            if(receiveValue!=null&&sendValue!=null){
                                conversionRate=receiveValue/sendValue;
                            }
                        }
                        product.setConversionRate(conversionRate);

                    }else {
                        product.setConversionRate((double) 0);
                    }
                }else{
                    product.setConversionRate((double) 0);
                }

                return product;
            }catch (Exception ex){
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }

        }
        return null;
    }

    @Override
    public ExternalResponse sendTopUp(String skuCode, BigInteger amount, String senderNumber, String topUpTransactionNumber,BigDecimal transferAmount) throws PocketException, JSONException {

        ExternalResponse response = new ExternalResponse();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("api_key",topupConfig.getApiKey());

        TopUpSendRequest request = new TopUpSendRequest();
        request.setSkuCode(skuCode);
        request.setSendValue(transferAmount);
        request.setAccountNumber(senderNumber);
        request.setDistributorRef(topUpTransactionNumber);
        request.setValidateOnly(false);

        String output = (String) new RestCallerTask().callToRestService(HttpMethod.POST, topupConfig.getSendTransfer(), headers, request, String.class);

        SendTopup topup = new Gson().fromJson(output, SendTopup.class);
        if(topup.resultCode==1) {
            logWriterUtility.info("TOPUP_DING","------ Topup seccessfully send -----");
            response.setAbleToSend(true);
            response.setData(topup);
            response.setError(null);
            response.setStatus(PocketConstants.OK);
        }else {
            logWriterUtility.info("TOPUP_DING","------ Topup failed to send -----");
            response.setAbleToSend(false);
            response.setData(null);
            response.setError(new ErrorObject((String.valueOf(topup.resultCode)),
                    topup.errorCodes.get(0).code));
            response.setStatus(PocketConstants.ERROR);
        }

        logWriterUtility.info("TOPUP_DING",new Gson().toJson(response));

        return response;
    }



    /**
     *
     * Transaction Service for External
     */

    @Override
    public TransactionSummaryViewResponse callTxForTransactionVisibility(TransactionValidityRequest transactionSummaryRequest) throws PocketException {

        String url=environment.getProperty("GET_TX_VALIDITY");

        logWriterUtility.trace(transactionSummaryRequest.getRequestId(),"Calling url :"+url);

        TransactionSummaryViewResponse response= null;
        try {
            response = (TransactionSummaryViewResponse) new RestCallerTask()
                    .callToRestService(HttpMethod.POST,url,transactionSummaryRequest,
                            TransactionSummaryViewResponse.class,transactionSummaryRequest.getRequestId());
            if(response==null){
                logWriterUtility.error(transactionSummaryRequest.getRequestId(),"Response null from TS");
                return null;
            }
            return response;
        } catch (PocketException e) {
            logWriterUtility.error(transactionSummaryRequest.getRequestId(),e.getMessage());
            throw e;
        }
    }

    @Override
    public Boolean callTxForUpdateClientStatus(UmUpdateClientStatusRequest updateClientStatusRequest) throws PocketException {

        String url=environment.getProperty("TS_UPDATE_CLIENT_STATUS");

        SuccessBoolResponse successBoolResponse= null;
        try {
            successBoolResponse = (SuccessBoolResponse) new RestCallerTask()
                    .callToRestService(HttpMethod.POST,url,updateClientStatusRequest,
                    SuccessBoolResponse.class,updateClientStatusRequest.getRequestId());
            if(successBoolResponse==null){
                logWriterUtility.error(updateClientStatusRequest.getRequestId(),"Response null from TS");
                return false;
            }
            return successBoolResponse.isResult();
        } catch (PocketException e) {
            logWriterUtility.error(updateClientStatusRequest.getRequestId(),e.getMessage());
            return false;
        }
    }



    /**
     *
     * SSL Demo test API implementation
     */

    @Override
    public ExternalResponse sslTopUp(String client_id, String client_pass, String guid, BigInteger operator_id, String recipient_msisdn, BigInteger amount, String connection_type, String sender_id, BigInteger priority, String success_url, String failure_url) throws PocketException {
        ExternalResponse response = null;
        try {
            VirtualrechargeServiceLocator serviceLocator = new VirtualrechargeServiceLocator();
            VirtualrechargePortType virtualrechargePortType = serviceLocator.getvirtualrechargePort();
            /**
             * create recharge request
             */
            RechargeResponseDataType createRechargeResponse = virtualrechargePortType.createRecharge(
                    client_id, client_pass,guid, operator_id, recipient_msisdn,
                    amount, connection_type, sender_id,priority, success_url, failure_url);
            if (createRechargeResponse.getRecharge_status().intValue() != 100) {
                response = new ExternalResponse();
                response.setStatus(PocketConstants.ERROR);
                response.setData(null);
                response.setAbleToSend(false);
                response.setError(new ErrorObject(PocketErrorCode.SSLCreateRechargeError));
                return response;
            }

            /**
             * init recharge request
             */

            RechargeResponseDataType initRechargeResponse = virtualrechargePortType.initRecharge(
                    client_id, client_pass,
                    guid, createRechargeResponse.getVr_guid(), success_url, failure_url);
            if (initRechargeResponse.getRecharge_status().intValue() != 200) {
                response = new ExternalResponse();
                response.setError(new ErrorObject(PocketErrorCode.SSLInitRechargeError));
                response.setAbleToSend(false);
                response.setData(null);
                response.setStatus(PocketConstants.ERROR);
                return response;
            }

            response = new ExternalResponse();
            response.setAbleToSend(true);
            response.setStatus(PocketConstants.OK);
            response.setError(null);
            response.setData(initRechargeResponse);

        } catch (Exception ex) {
            throw new PocketException(ex.getMessage());
        }

        return response;
    }



    /**
     * PayWell (Top Up) related API implementation
     */

    @Override
    public ExternalResponse payWellTopUp(String clientid, String clientpass, String crid, String msisdn, String amount, String connection, String operator, String sender) throws PocketException {
        try {
            logWriterUtility.info("payWellTopUp", "Before call to RechargerequestserverLocator");
            RechargerequestserverLocator payWellServiceLocator =
                    new RechargerequestserverLocator();
            logWriterUtility.info("payWellTopUp", "After call to RechargerequestserverLocator");
            logWriterUtility.info("payWellTopUp", "Before call to RechargerequestserverPortType");
            RechargerequestserverPortType payWellPortType =
                    payWellServiceLocator.getrechargerequestserverPort();
            logWriterUtility.info("payWellTopUp", "After call to RechargerequestserverPortType");
            logWriterUtility.info("payWellTopUp", "Before call to topupResponse");
            ResultObject1 topupResponse =
                    payWellPortType.topup(clientid, clientpass, crid,
                            msisdn, amount, connection, operator, sender);
            logWriterUtility.info("payWellTopUp", "After call to topupResponse");
            return getResponse(topupResponse, topupResponse.getStatus_code(),topupResponse.getStatus_details());
        }catch (Exception ex){
            throw new PocketException(ex.getMessage());
        }
    }

    /**
     * Paywell (billpay) related API implementation
     */

    @Override
    public ExternalResponse payWellPollibudditBillPay(PolliBudditBillRequest request, String logedInUserPhoneNumber) throws PocketException {
        String tag = "PAYWELL POLLIBUDDYUT BIIL PAY";
        String token = null;
        try {
            logWriterUtility.info("payWellPollibudditBillPay","Get a token from the server");
            token = getPayWellSecurityToken(request);

            // check time frame between 8:00 to before 18:00

            /*if(!DateUtil.isCorrectTimeForBillPay()){
                logWriterUtility.info(tag,"Time limit exit");
                return getResponse(null,"00001","Bill pay time is between 8:00 morning to before 6:00 evening");
            }*/
            logWriterUtility.info("payWellPollibudditBillPay service","Enter payWellPollibudditBillPay Service");
            // get information about customer bill pay

            logWriterUtility.info("payWellPollibudditBillPay","Call to Pay conformation information");
            ResponseDetailsOfBillPayConfirmation billPayConfirmation = getBillPayConfirmation(request,token);
            logWriterUtility.info("payWellPollibudditBillPay","Getting response from bill confirmation response");

            // check bill already paid or not

            if(billPayConfirmation == null){
                return getResponse(null,PocketConstants.ERROR,"Something wrong! Please try again.");
            }

            logWriterUtility.info(tag, "Bill pay confirmation status : "+billPayConfirmation.status+" and status name: "+billPayConfirmation.statusName);


            if(billPayConfirmation.status.equalsIgnoreCase("607")){
                return getResponse(null,billPayConfirmation.status,billPayConfirmation.statusName);
            }

            if(billPayConfirmation.status.equalsIgnoreCase("303") ||
                    billPayConfirmation.status.equalsIgnoreCase("302") ||
                    billPayConfirmation.status.equalsIgnoreCase("316") ||
                    billPayConfirmation.status.equalsIgnoreCase("329")){
                return getResponse(null,billPayConfirmation.status,billPayConfirmation.statusName);
            }

            if(billPayConfirmation.status.equalsIgnoreCase(PocketConstants.OK)){
                return getResponse(null,"303","Bill already paid");
            }

            if(billPayConfirmation.status.equalsIgnoreCase(PocketConstants.BILL_PENDING)){
                return getResponse(null, PocketConstants.BILL_PENDING, billPayConfirmation.statusName);
            }


            logWriterUtility.info(tag, "Bill not paid. So next to pay bill");

            logWriterUtility.info(tag,"Checkout our service about the bill pay user");
            // check user in our system
            boolean userCheck = checkUserBeneficiary(request.getAccount_no(), request.getPhoneNo(), Integer.parseInt(ExternalType.CUSTOMER_REGISTRATION));
            if(!userCheck){
                logWriterUtility.info(tag, "This user is not register in our system, so register first");
               saveBeneficiaryIntoDatabase(request,logedInUserPhoneNumber);
               logWriterUtility.info(tag, "Check also the paywell system about this bill pay user");
               doCustomerRegistraiton(request,token);
            }

            return payBillPolliBuddyut(request,token);

            //ResponseDetailsOfCustRegInq customerRegistration = getCustomerRegistrationInquiry(request);
            /*if(customerRegistration!=null&&customerRegistration.status.equalsIgnoreCase("326")){
                return getResponse(customerRegistration,customerRegistration.status,customerRegistration.statusName);
            }*/
            /*if(customerRegistration != null && !customerRegistration.status.equalsIgnoreCase(PocketConstants.OK)){
                customerRegistration = doCustomerRegistraiton(request);
            }*/
            /*if(customerRegistration!=null&&customerRegistration.status.equalsIgnoreCase(PocketConstants.OK)){
                return payBillPolliBuddyut(request);
            }*/
            /*return getResponse(
                    customerRegistration,
                    customerRegistration==null?"00001":customerRegistration.status,
                    customerRegistration==null?"We cannot proceed with your bill payment right now. Please try again or call customer care for further information.":customerRegistration.statusName);*/
        }catch (Exception ex){
            throw new PocketException(ex.getMessage());
        }
    }

    private ExternalResponse payBillPolliBuddyut(PolliBudditBillRequest request,String token) throws PocketException {
        try {
            logWriterUtility.info("payBillPolliBuddyut","payBillPolliBuddyut enter");
            PolliBiddyutResponse response = null;

            PollyBiddyutBillPay pollyBiddyutBillPay = new PollyBiddyutBillPay();
            pollyBiddyutBillPay.setUsername(request.getUsername());
            pollyBiddyutBillPay.setPassword(request.getPassword());
            pollyBiddyutBillPay.setBill_no(request.getAccount_no());
            pollyBiddyutBillPay.setPayerMobileNo(request.getPhoneNo());
            pollyBiddyutBillPay.setAmount(request.getAmount());
            pollyBiddyutBillPay.setRef_id(request.getRef_id());
            pollyBiddyutBillPay.setFormat(request.getFormat());
            pollyBiddyutBillPay.setTimestamp(request.getTimestamp());
            logWriterUtility.info("payBillPolliBuddyut request obj", new Gson().toJson(pollyBiddyutBillPay));

            RestTemplate restTemplate = new RestTemplate();
            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
            String polliBuddyutHashedData = CommonTasks.requestHashedDataGenerate(new Gson().toJson(pollyBiddyutBillPay), payWellConfig.getEncryptionkey().getBytes());
            HttpHeaders polliBuddyutBearerHeader = CommonTasks.createHeaders(null, null, token,payWellConfig.getApiKey(), polliBuddyutHashedData);

            ResponseEntity<String> exchange = restTemplate.exchange(
                    payWellConfig.getBillPay(),
                    HttpMethod.POST,
                    new HttpEntity<>(pollyBiddyutBillPay, polliBuddyutBearerHeader), String.class);
            if((exchange != null) && (exchange.getStatusCode()==HttpStatus.OK)){

                BillPayBaseResponse baseResponse = getBaseResponse(exchange.getBody());
//                if(baseResponse.apiStatus==HttpStatus.OK.value()) {    //pollyBiddyutBillPay api return ApiStatus = 100
                if(baseResponse.apiStatus == 100) {

                    logWriterUtility.info("PAYWELL POLLIBUDDYUT BIIL PAY","after bill pay response from paywell "+exchange.getBody());

                    response = new Gson().fromJson(exchange.getBody(), PolliBiddyutResponse.class);

                    if (response.responseDetails != null && response.responseDetails.status == 100) {

                        ResponseDetailsOfBillPayConfirmation billPayConfirmation = getBillPayConfirmation(request,token);
                        logWriterUtility.info("PAYWELL POLLIBUDDYUT BILL PAY","Bill pay confirmation check from paywell after bill pay: "+billPayConfirmation.status+" and "+billPayConfirmation.statusName);
                        if (billPayConfirmation != null)
                            return getResponse(billPayConfirmation, billPayConfirmation.status, billPayConfirmation.statusName);
                    }
                }
                return getResponse(response, String.valueOf(response.responseDetails.status),response.responseDetails.statusName);
            }else{
                return getResponse(exchange,PocketConstants.ERROR,"API Connectivity Problem");
            }
        }catch (Exception ex){
            logWriterUtility.info("payBillPolliBuddyut error",ex.getMessage());
            return  getResponse(null,PocketConstants.ERROR,"Bill process is pending.");
        }
    }

    private boolean checkUserBeneficiary(String accountNo, String mobileNo, int type) throws PocketException {
        try{
            UmBeneficiary userBeneficiary = beneficiaryRepository.findByAccountNumberAndMobileNumberAndType(accountNo, mobileNo, type);
            if(userBeneficiary!=null){
                return Boolean.TRUE;
            }
        }catch (Exception ex){
            logWriterUtility.info("Check Beneficiary Error: ", ex.getMessage());
            throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
        }

        return Boolean.FALSE;
    }

    private void saveBeneficiaryIntoDatabase(PolliBudditBillRequest request, String logedInUserMobileNo) {
        try {
            UmBeneficiary umBeneficiary = new UmBeneficiary();
            umBeneficiary.setAccountNumber(request.getAccount_no());
            umBeneficiary.setCustomerFullName(request.getFullName());
            umBeneficiary.setMobileNumber(request.getPhoneNo());
            umBeneficiary.setStatus(true);
            umBeneficiary.setType(Integer.parseInt(ExternalType.CUSTOMER_REGISTRATION));
            umBeneficiary.setUserMobileNo(logedInUserMobileNo);

            beneficiaryRepository.save(umBeneficiary);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    private void doCustomerRegistraiton(PolliBudditBillRequest request,String token) throws PocketException {
        ResponseDetailsOfCustRegInq responseDetailsOfCustRegInq = null;
        try {

            logWriterUtility.info("doCustomerRegistraiton","doCustomerRegistraiton enter");
            //String url = "https://agentapi.paywellonline.com/PollyBiddyutSystem/pollybuddutRegistration";

            BillPayCustomerRegistration customerRegistration = new BillPayCustomerRegistration();
            customerRegistration.setTimestamp(request.getTimestamp());
            customerRegistration.setFormat(request.getFormat());
            customerRegistration.setRef_id(request.getRef_id());
            customerRegistration.setUsername(request.getUsername());
            customerRegistration.setPassword(request.getPassword());
            customerRegistration.setAccount_no(request.getAccount_no());
            customerRegistration.setCust_name(request.getFullName());
            customerRegistration.setCust_phn(request.getPhoneNo());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.set("Authorization","Bearer "+ token);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> exchange = restTemplate.exchange(
                    payWellConfig.getCustomerRegistration(),
                    HttpMethod.POST,
                    new HttpEntity<>(customerRegistration, headers), String.class);
            if((exchange != null) && (exchange.getStatusCode() == HttpStatus.OK)){
                logWriterUtility.info("PAYWELL POLLIBUDDYUT BIIL PAY","Customer Registration response: "+exchange.getBody());
                BillPayBaseResponse baseResponse = getBaseResponse(exchange.getBody());
                if(baseResponse.apiStatus==HttpStatus.OK.value()) {
                    PolliBiddyutResponse response = new Gson().fromJson(exchange.getBody(), PolliBiddyutResponse.class);
                    if (response.responseDetails != null && response.responseDetails.custRegStatus == 100) {
                        logWriterUtility.info("From customer registration server","Customer registration is OK in the Paywell server");
                    }
                }
            }
        }catch (Exception ex){
            logWriterUtility.error("doCustomerRegistraiton error",ex.getMessage());
        }
    }

    private ResponseDetailsOfBillPayConfirmation getBillPayConfirmation(PolliBudditBillRequest request, String token) throws PocketException {
        ResponseDetailsOfBillPayConfirmation responseDetailsOfBillPayConfirmation = null;
        try {
            logWriterUtility.info("getBillPayConfirmation","Enter getBillPayConfirmation service");
            //String url = "https://agentapi.paywellonline.com/PollyBiddyutSystem/getPollyBillEnquiryData";

            BillPayConfiramtionRequest confiramtionRequest = new BillPayConfiramtionRequest();
            confiramtionRequest.setUsername(payWellConfig.getClientId());
            confiramtionRequest.setPassword(payWellConfig.getClientPassword());
            confiramtionRequest.setBill_no(request.getAccount_no());
            confiramtionRequest.setRef_id(request.getRef_id());
            confiramtionRequest.setFormat(request.getFormat());
            confiramtionRequest.setTimestamp(request.getTimestamp());

            RestTemplate restTemplate = new RestTemplate();
            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
            String billEnqHashedData = CommonTasks.requestHashedDataGenerate(new Gson().toJson(confiramtionRequest), payWellConfig.getEncryptionkey().getBytes());
            HttpHeaders billEnqDataBearerHeader = CommonTasks.createHeaders(null, null, token,payWellConfig.getApiKey(), billEnqHashedData);

            ResponseEntity<String> outputOfCustRegInquery = restTemplate.exchange(
                    payWellConfig.getBillEnquiryData(),
                    HttpMethod.POST,
                    new HttpEntity<>(confiramtionRequest, billEnqDataBearerHeader),
                    String.class);
            if(outputOfCustRegInquery.getStatusCode()==HttpStatus.OK){
                logWriterUtility.info("PAYWELL POLLIBUDDYUT BILL PAY","Bill pay confirmation : "+ outputOfCustRegInquery.getBody());
                BillPayBaseResponse baseResponse = getBaseResponse(outputOfCustRegInquery.getBody());
                if(baseResponse.apiStatus==HttpStatus.OK.value()) {
                    BillPayConfirmationResponse response = new Gson().fromJson(outputOfCustRegInquery.getBody(),
                            BillPayConfirmationResponse.class);
                    if (response != null && response.apiStatus == HttpStatus.OK.value()) {
                        Stream<ResponseDetailsOfBillPayConfirmation> stream =
                                response.billPayConfiramtions.stream();
                        return stream.skip((long) response.billPayConfiramtions.size() - 1).findFirst().get();
                    }
                }
                responseDetailsOfBillPayConfirmation =
                        new ResponseDetailsOfBillPayConfirmation();
                responseDetailsOfBillPayConfirmation.status = baseResponse.apiStatus.toString();
                responseDetailsOfBillPayConfirmation.statusName = baseResponse.apiStatusName;
                return responseDetailsOfBillPayConfirmation;
            }
        }catch (Exception ex){
            logWriterUtility.error("getBillPayConfirmation error",ex.getMessage());
            responseDetailsOfBillPayConfirmation =
                    new ResponseDetailsOfBillPayConfirmation();
            responseDetailsOfBillPayConfirmation.status = PocketConstants.BILL_PENDING;
            responseDetailsOfBillPayConfirmation.statusName = "Bill pay in pending now.";
            return responseDetailsOfBillPayConfirmation;
        }

        return null;

    }

    private ResponseDetailsOfCustRegInq getCustomerRegistrationInquiry(PolliBudditBillRequest request){
        ResponseDetailsOfCustRegInq responseDetailsOfCustRegInq = null;
        try {
            logWriterUtility.info("getCustomerRegistrationInquiry","getCustomerRegistrationInquiry enter");
            //String url = "https://agentapi.paywellonline.com/PollyBiddyutSystem/getPollyBillRegEnquiryData";
            BillPayCustomerRegInquery requestData = new BillPayCustomerRegInquery();

            requestData.setUsername(request.getUsername());
            requestData.setPassword(request.getPassword());
            requestData.setAccount_no(request.getAccount_no());
            requestData.setFormat(request.getFormat());
            requestData.setRef_id(request.getRef_id());
            requestData.setTimestamp(request.getTimestamp());


            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.set("Authorization","Bearer "+ getPayWellSecurityToken(requestData));

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> outputOfCustRegInquery = restTemplate.exchange(payWellConfig.getCustomerRegEnquiry(), HttpMethod.POST, new HttpEntity<>(requestData, headers), String.class);
            if(outputOfCustRegInquery.getStatusCode()==HttpStatus.OK){
                logWriterUtility.info("PAYWELL POLLIBUDDYUT BIIL PAY","Customer enquiry response "+ outputOfCustRegInquery.getBody());
                BillPayBaseResponse baseResponse = getBaseResponse(outputOfCustRegInquery.getBody());
                if(baseResponse.apiStatus==HttpStatus.OK.value()) {
                    PolliBuddyutCustRegInqResponse response = new Gson().fromJson(outputOfCustRegInquery.getBody(), PolliBuddyutCustRegInqResponse.class);
                    if (response != null && response.apiStatus == HttpStatus.OK.value()) {

                        Stream<ResponseDetailsOfCustRegInq> stream =
                                response.responseDetailsList.stream();
                        return stream.skip((long) response.responseDetailsList.size() - 1).findFirst().get();

                    }
                }

                responseDetailsOfCustRegInq =
                        new ResponseDetailsOfCustRegInq();
                responseDetailsOfCustRegInq.status = baseResponse.apiStatus.toString();
                responseDetailsOfCustRegInq.statusName = baseResponse.apiStatusName;

                return responseDetailsOfCustRegInq;
            }
        }catch (Exception ex) {
            logWriterUtility.error("getCustomerRegistrationInquiry error", ex.getMessage());
            responseDetailsOfCustRegInq =
                    new ResponseDetailsOfCustRegInq();
            responseDetailsOfCustRegInq.status = PocketConstants.BILL_PENDING;
            responseDetailsOfCustRegInq.statusName = "Bill pay in pending now.";

            return responseDetailsOfCustRegInq;
        }

        return null;
    }

    private String getPayWellSecurityToken(Object request){
        try{

            logWriterUtility.info("getPayWellSecurityToken","Enter getPayWellSecurityToken service");

            String data = new Gson().toJson(request);

            logWriterUtility.info("PayWell","Calling Url :"+payWellConfig.getAuthentication());

            RestTemplate restTemplate = new RestTemplate();
            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
            HttpHeaders headers = CommonTasks.createHeaders(payWellConfig.getApiUserName(), payWellConfig.getApiPassword(), null, null, null);

            ResponseEntity<String> exchange = restTemplate.exchange(
                    payWellConfig.getAuthentication(), HttpMethod.POST,
                    new HttpEntity<>(headers), String.class);
            if(exchange.getStatusCode()==HttpStatus.OK) {
                logWriterUtility.info("getPayWellSecurityToken","Response of paywell token: "+exchange.getBody());
                PaywellTokenResponse response = new Gson().fromJson(exchange.getBody(), PaywellTokenResponse.class);

//                Mac mac = Mac.getInstance("HmacSHA256");
//                SecretKeySpec keySpec = new SecretKeySpec(payWellConfig.getEncryptionkey().getBytes(),mac.getAlgorithm());
//                mac.init(keySpec);
//                byte[] bytesData = mac.doFinal(data.getBytes());
//                return Base64.encodeBase64String((response.token.securityToken+":"+payWellConfig.getApiKey()+":"+CommonTasks.byteToString(bytesData)).getBytes());

                return response.token.securityToken;
            }else{
                logWriterUtility.error("PayWell","Got invalid status code from Paywell Token :"+exchange.getStatusCode());
            }

        }catch (Exception ex){
            logWriterUtility.error("PayWell","Error from paywell token request API: "+ex.getMessage());
        }

        return null;
    }

    private BillPayBaseResponse getBaseResponse(String data){
        return new Gson().fromJson(data,BillPayBaseResponse.class);
    }
    private PollyBiddyutBillEnquiryResponse getBillEnquiryResponse(String data){
        return new Gson().fromJson(data,PollyBiddyutBillEnquiryResponse.class);
    }
    private PollyBiddyutBillPaymentResponse getBillPaymentResponse(String data){
        return new Gson().fromJson(data, PollyBiddyutBillPaymentResponse.class);
    }
    @Override
    public ExternalResponse findBillPayStatus(PolliBudditBillRequest request) throws PocketException {
        String token = getPayWellSecurityToken(request);
        ResponseDetailsOfBillPayConfirmation billPayConfirmation = getBillPayConfirmation(request,token);
        return getResponse(billPayConfirmation, billPayConfirmation.status,billPayConfirmation.statusName);
    }

    /******
     *
     * Bank Related API implementation
     */

    @Override
    public BankListResponse bankListByUserPhone(BankListRequest request) throws PocketException {
        BankListResponse response = new BankListResponse();
        response.setBankLists(bankListRepository.findByUserMobileNumber(request.getUserPhoneNumber()));
        return response;
    }

    @Override
    public BankAddResponse addANewBank(BankAddRequest request) throws PocketException {
        BankAddResponse response = new BankAddResponse();

        UmBankList bankList = bankListRepository.findByUserMobileNumberAndBankId(request.getUserMobileNumber(), request.getBankId());
        if(bankList == null)
            bankList = new UmBankList();


        String userSecretNew = request.getUserSecret().replaceAll(" ", "").trim();


        bankList.setUserMobileNumber(request.getUserMobileNumber());
        bankList.setBankFullName(request.getBankFullName());
        bankList.setBankShortName(request.getBankShortName());
        bankList.setBankLogoUrl(request.getBankLogoUrl());
        bankList.setBankId(request.getBankId());
        bankList.setAccessCode(request.getAccessCode());
        bankList.setConnectionId(request.getConnectionId());
        bankList.setStatus("A");
        bankList.setUserBankId(request.getUserID());
        bankList.setUserSecret(userSecretNew);
        bankList.setCreateAt(new Timestamp(new Date().getTime()));
        bankList.setUpdateAt(new Timestamp(new Date().getTime()));




        // getting Access Code from Exchange Token Request
        // Firstly, I need a token, Which is come from "Exchange Token" API.
        ExchangeTokenRequest exchangeTokenRequest = new ExchangeTokenRequest();
        exchangeTokenRequest.setAccessCode(request.getAccessCode());
        exchangeTokenRequest.setConnectionID(request.getConnectionId());
        exchangeTokenRequest.setAppSecret(bankConfig.getAppSecret());

        ExchangeTokenResponse token = getToken(exchangeTokenRequest);

        if(token==null){
            throw new PocketException(PocketErrorCode.BANK_TOKEN_NOT_FOUND);
        }

        logWriterUtility.info("BANK TOKEN",token.accessToken);

        bankList.setAccessToken(token.accessToken);

        bankListRepository.saveAndFlush(bankList);

        ArrayList<UmBankList> byUserMobileNumber = bankListRepository.findByUserMobileNumber(request.getUserMobileNumber());
        response.setBankLists(byUserMobileNumber);

        return response;
    }

    @Override
    public BankMetadataResponse bankData(BankDataRequest request) throws PocketException {
        // Before call to Exchange Token api, need some data, which is come from bank list
        UmBankList bankInformation = bankListRepository.findByUserMobileNumberAndBankId(request.getUserMobileNumber(), request.getBankID());
        if(bankInformation == null){
            throw new PocketException(PocketErrorCode.BANK_INFO_NOT_FOUND);
        }

        if(bankInformation.getAccessToken()==null){
            throw new PocketException(PocketErrorCode.BANK_ACCESS_TOKEN_NOT_FOUND);
        }

        // login before call to DAPI

        BankLoginRequest bankLoginRequest = new BankLoginRequest();
        bankLoginRequest.setAppSecret(bankConfig.getAppSecret());
        bankLoginRequest.setUserSecret(bankInformation.getUserSecret());
        bankLoginRequest.setSync(true);

        BankLoginResponse bankLoginResponse = DAPIBankLogin(bankLoginRequest, bankInformation.getAccessToken());

        if(bankLoginResponse == null){
            throw  new PocketException(PocketErrorCode.BANK_LOGIN_FAILED);
        }


        // I have my token, now call to "Account Data" API

        String url = "https://api.dapi.co/v1/metadata/accounts/get";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("Authorization","Bearer "+bankInformation.getAccessToken());

        AccountMetadataRequest metadataRequest = new AccountMetadataRequest();
        metadataRequest.setAppSecret(bankConfig.getAppSecret());
        metadataRequest.setUserSecret(bankInformation.getUserSecret());
        metadataRequest.setSync(true);

        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(metadataRequest, httpHeaders), String.class);

        if(exchange != null && exchange.getStatusCode() == HttpStatus.OK){
            return new Gson().fromJson(exchange.getBody(),BankMetadataResponse.class);
        }

        return null;
    }

    @Override
    public BankUserInputResponse sendBankUserInput(BankUserInputRequest request) throws PocketException {
        UmBankList bankInformation = bankListRepository.findByUserMobileNumberAndBankId(request.getMobileNumber(), request.getBankID());
        if(bankInformation == null){
            throw new PocketException(PocketErrorCode.BANK_INFO_NOT_FOUND);
        }

        UserInputData userInputData = new UserInputData();
        userInputData.setJobID(request.getJobID());
        userInputData.setUserInputs(request.getUserInputs());
        userInputData.setAppSecret(bankConfig.getAppSecret());
        userInputData.setUserSecret(bankInformation.getUserSecret());
        userInputData.setSync(false);

        String url = "https://api.dapi.co/v1/job/resume";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(userInputData), String.class);
        if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
            return new Gson().fromJson(exchange.getBody(),BankUserInputResponse.class);
        }

        return null;
    }

    @Override
    public BankBeneficiaryAddResponse bankBeneficiaryAdd(BankBeneficiaryAddRequest request) throws PocketException {
        UmBankList bankInformation = bankListRepository.findByUserMobileNumberAndBankId(request.getMobileNo(), request.getBankID());
        if(bankInformation == null){
            throw new PocketException(PocketErrorCode.BANK_INFO_NOT_FOUND);
        }

        String url = "https://api.dapi.co/v1/payment/beneficiaries/create";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization","bearer "+bankInformation.getAccessToken());

        request.getBankBeneficiaryAddInfo().setAppSecret(bankConfig.getAppSecret());
        request.getBankBeneficiaryAddInfo().setUserSecret(bankInformation.getUserSecret());

        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(request.getBankBeneficiaryAddInfo(), httpHeaders), String.class);

        if(exchange != null && exchange.getStatusCode() == HttpStatus.OK){
            return new Gson().fromJson(exchange.getBody(), BankBeneficiaryAddResponse.class);
        }


        return null;
    }

    @Override
    public BankBeneficiaryGetResponse bankBeneficiaryGet(BankBeneficiaryGetRequest request) throws PocketException {
        UmBankList bankInformation = bankListRepository.findByUserMobileNumberAndBankId(request.getMobileNumber(), request.getBankID());
        if(bankInformation == null){
            throw new PocketException(PocketErrorCode.BANK_INFO_NOT_FOUND);
        }

        String url = "https://api.dapi.co/v1/payment/beneficiaries/get";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("Authorization","bearer "+bankInformation.getAccessToken());

        request.getBankBeneficiaryGetInformation().setAppSecret(bankConfig.getAppSecret());
        request.getBankBeneficiaryGetInformation().setUserSecret(bankInformation.getUserSecret());
        request.getBankBeneficiaryGetInformation().setSync(true);

        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(request.getBankBeneficiaryGetInformation(), httpHeaders), String.class);

        if(exchange != null && exchange.getStatusCode() == HttpStatus.OK){
            return new Gson().fromJson(exchange.getBody(), BankBeneficiaryGetResponse.class);
        }


        return null;
    }

    @Override
    public Boolean storeAccessToken(StoreAccessToken storeAccessToken) {
        if(storeAccessToken == null){
            return false;
        }

        DapiAccessToken dapiAccessToken = new DapiAccessToken();
        dapiAccessToken.setTokenId(storeAccessToken.getId());
        dapiAccessToken.setAccessToken(storeAccessToken.getAccessToken());
        dapiAccessToken.setDateCreated(DateUtil.currentDate());
        accessTokenRepository.save(dapiAccessToken);

        return true;
    }

    @Override
    public DapiAccessToken getAccessToken(String tokenId) {
        DapiAccessToken dapiAccessToken = accessTokenRepository.findFirstByTokenId(tokenId);
        if(dapiAccessToken==null){
            return null;
        }
        return dapiAccessToken;
    }
    @Override
    public List<DapiAccessToken> getAccessToken() {
        List<DapiAccessToken> dapiAccessTokens = accessTokenRepository.findAllByOrderByIdDesc();

        return dapiAccessTokens;
    }


    private ExchangeTokenResponse getToken(ExchangeTokenRequest exchangeTokenRequest){
        try{
            RestTemplate restTemplate = new RestTemplate();

            String url = "https://api.dapi.co/v1/auth/ExchangeToken";

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);

            ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(exchangeTokenRequest,httpHeaders), String.class);

            if(exchange!= null && exchange.getStatusCode()==HttpStatus.OK){
                ExchangeTokenResponse exchangeTokenResponse = new Gson().fromJson(exchange.getBody(), ExchangeTokenResponse.class);

                if(exchangeTokenResponse.success){
                    return exchangeTokenResponse;
                }
            }

        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }

    private BankLoginResponse DAPIBankLogin(BankLoginRequest request, String accessToken){

        try {
            RestTemplate restTemplate = new RestTemplate();

            String url = "https://api.dapi.co/v1/auth/login/check";

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Authorization","Bearer "+accessToken);

            ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(request,httpHeaders), String.class);

            if(exchange!= null && exchange.getStatusCode()==HttpStatus.OK){
                BankLoginResponse loginResponse = new Gson().fromJson(exchange.getBody(), BankLoginResponse.class);

                if(loginResponse.success){
                    return loginResponse;
                }
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        return null;

    }


    /**
     * amy (Flight) related API implementation
     */

    @Override
    public DepartureListResponse getDepartureList(DepartureListRequest request) throws PocketException{
        DepartureListResponse response = null;
        logWriterUtility.info("DepartureList","Enter into departure list service impl");

        String url = String.format(Locale.getDefault(),"http://119.18.148.10/amyapi/api.aspx?CMND=_ROUTEFROM_&dom=%d&pref=%d",request.getRouteType(),request.getPreference());
        logWriterUtility.info("DepartureList url",url);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
            response = new DepartureListResponse();
            logWriterUtility.info("DepartureList response",exchange.getBody());

            response.setRouteLists(Arrays.asList(new Gson().fromJson(exchange.getBody(), RouteList[].class)));
        }

        return response;
    }

    @Override
    public DestinationListResponse getDestinationList(DestinationListRequest request) throws PocketException {
        DestinationListResponse response = null;
        logWriterUtility.info("DestinationList","Enter into destination list service impl");

        String url = String.format(Locale.getDefault(),"http://119.18.148.10/amyapi/api.aspx?CMND=_ROUTETO_&frm=%s&dom=%d&pref=%d",request.getFrom(),request.getRouteType(),request.getPreference());
        logWriterUtility.info("Destination url",url);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
            response = new DestinationListResponse();
            logWriterUtility.info("DestinationList response",exchange.getBody());
            response.setRouteLists(Arrays.asList(new Gson().fromJson(exchange.getBody(),RouteList[].class)));

        }

        return response;
    }

    @Override
    public FlightSearchResponse getFlightSearchResponse(FlightSearchRequest request) throws PocketException {
        try {
            logWriterUtility.info("FlightSearch","Enter into Flight Search service impl");
            UmAmySession currentSession = getAMYCurrentSession();

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Cookie", currentSession.getToken());

            ResponseEntity<String> exchange = restTemplate.exchange("http://119.18.148.10/amyapi/api.aspx",
                    HttpMethod.POST, new HttpEntity<>(request,httpHeaders), String.class);
            if(exchange != null && exchange.getStatusCode() == HttpStatus.OK){
                logWriterUtility.info("FlightSearch response",exchange.getBody());
                return new Gson().fromJson(exchange.getBody(), FlightSearchResponse.class);
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public FlightPriceResponse getFlightPrices(FlightPriceRequest request) throws PocketException {
        logWriterUtility.info("FlightPrice","Enter into Flight Price service impl");
        UmAmySession currentSession = getAMYCurrentSession();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", currentSession.getToken());

        String url = String.format(Locale.getDefault(),
                "http://119.18.148.10/amyapi/api.aspx?CMND=_GETPRICE_&sid=%d&aid1=%d&aid2=%d&disp=0",
                request.getSearchId(),request.getDepartureFlightID(),request.getReturnFlightID());
        ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(httpHeaders), String.class);
        if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
            logWriterUtility.info("FlightPrice response",exchange.getBody());
            try {
                return new Gson().fromJson(exchange.getBody(), FlightPriceResponse.class);
            }catch (Exception ex){
                throw new PocketException(PocketErrorCode.FLIGHT_PRICE_ERROR);
            }
        }

        throw new PocketException(PocketErrorCode.FLIGHT_PRICE_ERROR);
    }

    @Override
    public PassengerValidationResponse passengerValidation(PassengerValidationRequest request) throws PocketException {
        logWriterUtility.info("Passenger Validation","Enter into passenger validation service impl");
        UmAmySession currentSession = getAMYCurrentSession();

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", currentSession.getToken());

        ResponseEntity<String> exchange = restTemplate.exchange("http://119.18.148.10/amyapi/api.aspx", HttpMethod.POST, new HttpEntity<>(request, httpHeaders), String.class);
        if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
            logWriterUtility.info("Passenger Validation response",exchange.getBody());
            return new Gson().fromJson(exchange.getBody(), PassengerValidationResponse.class);
        }
        throw new PocketException(PocketErrorCode.PASSENGER_VALIDATION_FAILED);
    }

    @Override
    public FlightDocumentUploadResponse documentUpload(FlightDocumentUploadRequest request) throws PocketException {
        try {
            logWriterUtility.info("document upload","Enter into document upload service impl");
            UmAmySession currentSession = getAMYCurrentSession();

            Resource resourceFile = getImageFile(request);
            if(resourceFile==null){
                throw new PocketException(PocketErrorCode.DOCUMENT_NOT_UPLOADED);
            }

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
            httpHeaders.add("Cookie",currentSession.getToken());

            MultiValueMap<String, Object> imageBody = new LinkedMultiValueMap<>();
            imageBody.add("Content-Type","image/jpeg");
            imageBody.add("file",resourceFile);

            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(imageBody, httpHeaders);

            String url = String.format(Locale.getDefault(),"http://119.18.148.10/amyapi/api.aspx?file=ok&typ=%s&id=%d",
                    request.getType(),request.getPassengerId());


            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
            if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
                logWriterUtility.info("Document upload response",exchange.getBody());

                return new Gson().fromJson(exchange.getBody(),FlightDocumentUploadResponse.class);

            }


        }catch (Exception ex){
            logWriterUtility.error("Flight Document Upload: ", ex.getMessage());
            throw  new PocketException(PocketErrorCode.DOCUMENT_UPLOAD_ERROR);
        }

        throw new PocketException(PocketErrorCode.DOCUMENT_UPLOAD_ERROR);
    }

    @Override
    public ExternalResponse flightBooking(FlightBookingRequest request) throws PocketException {
        logWriterUtility.info("Flight Booking","Enter flight booking service impl");
        UmAmySession umAmySession = getAMYCurrentSession();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie",umAmySession.getToken());

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> exchange = restTemplate.exchange("http://119.18.148.10/amyapi/api.aspx", HttpMethod.POST, new HttpEntity<>(request, httpHeaders), String.class);
        if(exchange != null && exchange.getStatusCode() == HttpStatus.OK){
            logWriterUtility.info("Flight Booking Response",exchange.getBody());
            FlightBookingAndIssueResponse flightBookingAndIssueResponse = new Gson().fromJson(exchange.getBody(), FlightBookingAndIssueResponse.class);
            return getResponse(flightBookingAndIssueResponse,PocketConstants.OK,PocketConstants.SUCCESS);
        }
        return getResponse(null,PocketConstants.ERROR,PocketConstants.FAILED);
    }

    @Override
    public ExternalResponse flightIssue(FlightIssueRequest request) throws PocketException {
        logWriterUtility.info("Flight Issue","Enter flight issue service impl");
        UmAmySession umAmySession = getAMYCurrentSession();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie",umAmySession.getToken());

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> exchange = restTemplate.exchange("http://119.18.148.10/amyapi/api.aspx", HttpMethod.POST, new HttpEntity<>(request, httpHeaders), String.class);
        if(exchange != null && exchange.getStatusCode() == HttpStatus.OK){
            logWriterUtility.info("Flight Issue",exchange.getBody());
            FlightBookingAndIssueResponse response = new Gson().fromJson(exchange.getBody(),FlightBookingAndIssueResponse.class);
            return getResponse(response,PocketConstants.OK,PocketConstants.SUCCESS);
        }
        return getResponse(null,PocketConstants.ERROR,PocketConstants.FAILED);
    }

    private Resource getImageFile(FlightDocumentUploadRequest request) throws PocketException {
        try {
            String fileName = "PSN_"+request.getPassengerId()+"_"+System.currentTimeMillis();
            String dir = System.getProperty("user.home") +
                    "" +
                    environment.getProperty("file_base_path")+
                    "/" +
                    request.getType();
            File file = new File(dir);
            if(!file.exists()){
                file.mkdirs();
            }

            byte[] imageByte = Base64.decodeBase64(request.getImage());
            /*String filePath = dir+fileName+request.getFileExtension();

            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            fileOutputStream.write(imageByte);*/
            Path path = Paths.get(dir+"/"+fileName+request.getFileExtension());
            path = Files.write(path,imageByte);

            return new FileSystemResource(path.toFile());

        }catch (Exception ex){
            logWriterUtility.error("File create for flight document upload",ex.getMessage());
            throw new PocketException(PocketErrorCode.DOCUMENT_UPLOAD_ERROR);
        }
    }

    private void doAMYLogin(List<UmAmySession> allSession){
        try {
            AmyLoginRequest loginRequest = new AmyLoginRequest();
            loginRequest.setCID("109");
            loginRequest.setUSER(amyConfig.getUsername());
            loginRequest.setPASS(amyConfig.getPassword());
            loginRequest.setDVID(CommonTasks.getIPAddress());
            loginRequest.setCMND("_LOGINONLY_");

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> exchange = restTemplate.exchange("http://119.18.148.10/amyapi/api.aspx", HttpMethod.POST, new HttpEntity<>(loginRequest), String.class);
            if(exchange != null && exchange.getStatusCode()==HttpStatus.OK){
                HttpHeaders headers = exchange.getHeaders();
                String cookiesOfAmy = headers.getFirst(HttpHeaders.SET_COOKIE);


                UmAmySession amySession = new UmAmySession();
                if(allSession.size()<=0){
                    amySession.setToken(cookiesOfAmy);
                    amySession.setTokenTime(new Timestamp(System.currentTimeMillis()));
                    amySession.setTokenType(ExternalType.AMY_SESSION);
                }else{
                    amySession = allSession.stream().findFirst().get();
                    amySession.setTokenTime(new Timestamp(System.currentTimeMillis()));
                    amySession.setToken(cookiesOfAmy);
                    amySession.setTokenType(ExternalType.AMY_SESSION);
                }

                amySessionRepository.save(amySession);
            }
        }catch (Exception ex){
            logWriterUtility.debug("amy Login Error: ", ex.getMessage());
        }
    }

    private UmAmySession getAMYCurrentSession(){
        // find the current cookies and if not then add a new one
        List<UmAmySession> allSession = amySessionRepository.findAll();
        if(allSession == null || allSession.size()==0){
            doAMYLogin(allSession);
            allSession = amySessionRepository.findAll();
        }

        // calculate the time difference
        if(allSession.stream().findFirst().isPresent()){
            long lastCookiesTime = allSession.stream().findFirst().get().getTokenTime().getTime();
            long timeDifference = System.currentTimeMillis() - lastCookiesTime;
            long timeLaps = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
            //long timeLaps = timeDifference/(60*1000)%60;
            if(timeLaps>=amyConfig.getSessionTime()){
                // add new cookies
                doAMYLogin(allSession);
                allSession = amySessionRepository.findAll();
            }
        }
        return allSession.stream().findFirst().get();
    }


    /**
     * General Response
     */
    private ExternalResponse getResponse(Object data, String status, String message){
        ExternalResponse response = new ExternalResponse();
        if(status.equals(PocketConstants.OK)){
            response.setData(data);
            response.setAbleToSend(true);
            response.setStatus(PocketConstants.OK);
            response.setError(null);
        }else{
            response.setData(null);
            response.setAbleToSend(false);
            response.setStatus(PocketConstants.ERROR);
            response.setError(new ErrorObject(status,message));
        }
        return response;
    }

    //PollyBiddyut API consume doc version 2.2
    @Override
    public ExternalResponse payWellPollyBiddyutBillPayment(PolliBudditBillRequest request, String logedInUserPhoneNumber) throws PocketException {
        String tag = "PAYWELL POLLIBUDDYUT BILL PAY";
        String token = null;
        try {
            logWriterUtility.info("payWellPollibudditBillPay","Get a token from the server");
            token = getPayWellSecurityToken(request);

            logWriterUtility.info("payWellPollibudditBillPay service","Enter detailsOfPollyBiddyutBillEnquiry Service");
            // get information about customer bill pay

            logWriterUtility.info("detailsOfPollyBiddyutBillEnquiry","Call to detailsOfPollyBiddyutBillEnquiry info");
            ResponseDetailsOfPollyBiddyutBillEnquiry detailsOfPollyBiddyutBillEnquiry = getPollyBiddyutBillEnquiry(request,token);
            //check user bill amount and paywell bill amount
            if(detailsOfPollyBiddyutBillEnquiry != null){
                if(detailsOfPollyBiddyutBillEnquiry.billAmount == 0){
                    return getResponse(null,PocketConstants.ERROR,
                            detailsOfPollyBiddyutBillEnquiry.statusName);
                }
                if(detailsOfPollyBiddyutBillEnquiry.billAmount != Integer.parseInt(request.getAmount())){
                    return getResponse(null,PocketConstants.ERROR,"Invalid bill amount.");
                }
            }
            logWriterUtility.info("detailsOfPollyBiddyutBillEnquiry","Getting response from bill Enquiry response");

            // check bill already paid or not
            if(detailsOfPollyBiddyutBillEnquiry == null){
                return getResponse(null,PocketConstants.ERROR,"Something wrong! Please try again.");
            }

            logWriterUtility.info(tag, "Bill pay confirmation status : "+detailsOfPollyBiddyutBillEnquiry.status+" and status name: "+detailsOfPollyBiddyutBillEnquiry.statusName);

            if(detailsOfPollyBiddyutBillEnquiry.status.toString().equalsIgnoreCase("607")){
                return getResponse(null, detailsOfPollyBiddyutBillEnquiry.status.toString(), detailsOfPollyBiddyutBillEnquiry.statusName);
            }

            //After successful billEnquiry API from paywell server return 815=Is processing
            if(detailsOfPollyBiddyutBillEnquiry.status != 815){
                return getResponse(null, detailsOfPollyBiddyutBillEnquiry.status.toString(), detailsOfPollyBiddyutBillEnquiry.statusName+ "\n"+
                        detailsOfPollyBiddyutBillEnquiry.msgText);
            }

            if(detailsOfPollyBiddyutBillEnquiry.status.toString().equalsIgnoreCase(PocketConstants.BILL_PENDING)){
                return getResponse(null, PocketConstants.BILL_PENDING, detailsOfPollyBiddyutBillEnquiry.statusName);
            }

            logWriterUtility.info(tag, "Bill not paid. So next to pay bill");
            logWriterUtility.info(tag,"Checkout our service about the bill pay user");
            // check user in our system
            boolean userCheck = checkUserBeneficiary(request.getAccount_no(), request.getPhoneNo(), Integer.parseInt(ExternalType.CUSTOMER_REGISTRATION));
            if(!userCheck){
                logWriterUtility.info(tag, "This user is not register in our system, so register first");
                saveBeneficiaryIntoDatabase(request,logedInUserPhoneNumber);
            }

            return payBillPaymentPollyBiddyut(request, token, detailsOfPollyBiddyutBillEnquiry);
        }catch (Exception ex){
            throw new PocketException(ex.getMessage());
        }
    }
    private ResponseDetailsOfPollyBiddyutBillEnquiry getPollyBiddyutBillEnquiry(PolliBudditBillRequest request, String token) throws PocketException {
        ResponseDetailsOfPollyBiddyutBillEnquiry responseDetailsOfBillEnquiry = null;
        try {
            logWriterUtility.info("getPollyBiddyutBillEnquiry","Enter getPollyBiddyutBillEnquiry service");

            PollyBiddyutBillEnquiryRequest billEnquiryRequest = new PollyBiddyutBillEnquiryRequest();
            billEnquiryRequest.setUsername(payWellConfig.getClientId());
            billEnquiryRequest.setPassword(payWellConfig.getClientPassword());
            billEnquiryRequest.setBill_no(request.getAccount_no());
            //For BillNumber = "BQB", AccountNumber ="BQS"
            if(request.getAccount_no() != null){
                billEnquiryRequest.setBill_type("BQS");
            }
            billEnquiryRequest.setBill_month(request.getBill_month());
            billEnquiryRequest.setBill_year(request.getBill_year());
            billEnquiryRequest.setRef_id(request.getRef_id());
            billEnquiryRequest.setFormat(request.getFormat());
            billEnquiryRequest.setTimestamp(request.getTimestamp());
            logWriterUtility.info("getPollyBiddyutBillEnquiry request object",new Gson().toJson(billEnquiryRequest));

            RestTemplate restTemplate = new RestTemplate();
            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
            String billEnqHashedData = CommonTasks.requestHashedDataGenerate(new Gson().toJson(billEnquiryRequest), payWellConfig.getEncryptionkey().getBytes());
            HttpHeaders billEnquiryBearerHeader = CommonTasks.createHeaders(null, null, token,payWellConfig.getApiKey(), billEnqHashedData);

            ResponseEntity<String> exchangeOfBillEnquiry = restTemplate.exchange(
                    payWellConfig.getBillEnquiry(), HttpMethod.POST,
                    new HttpEntity<>(billEnquiryRequest, billEnquiryBearerHeader), String.class);

            if(exchangeOfBillEnquiry.getStatusCode()==HttpStatus.OK){
                logWriterUtility.info("PAYWELL POLLIBUDDYUT BILL ENQUIRY","Bill ENQUIRY BODY RESPONSE : "+ exchangeOfBillEnquiry.getBody());
                PollyBiddyutBillEnquiryResponse billEnquiry = getBillEnquiryResponse(exchangeOfBillEnquiry.getBody());

                if(billEnquiry.apiStatus==HttpStatus.OK.value()) {
                    PollyBiddyutBillEnquiryResponse billEnquiryResponse = new Gson().fromJson(exchangeOfBillEnquiry.getBody(),
                            PollyBiddyutBillEnquiryResponse.class);
                    if (billEnquiryResponse != null && billEnquiryResponse.apiStatus == HttpStatus.OK.value()) {
                        responseDetailsOfBillEnquiry = new ResponseDetailsOfPollyBiddyutBillEnquiry();
                        responseDetailsOfBillEnquiry.status = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.status;
                        responseDetailsOfBillEnquiry.statusName = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.statusName;
                        responseDetailsOfBillEnquiry.trxId = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.trxId;
                        responseDetailsOfBillEnquiry.msgText = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.msgText;
                        responseDetailsOfBillEnquiry.billNo = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.billNo;
                        responseDetailsOfBillEnquiry.billAmount = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.billAmount;
                        responseDetailsOfBillEnquiry.extraCharge = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.extraCharge;
                        responseDetailsOfBillEnquiry.retailerCommission = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.retailerCommission;
                        responseDetailsOfBillEnquiry.totalAmount = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.totalAmount;
                        responseDetailsOfBillEnquiry.contactNo1 = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.contactNo1;
                        responseDetailsOfBillEnquiry.issueDate = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.issueDate;
                        responseDetailsOfBillEnquiry.billMonth = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.billMonth;
                        responseDetailsOfBillEnquiry.billYear = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.billYear;
                        responseDetailsOfBillEnquiry.dueDate = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.dueDate;
                        responseDetailsOfBillEnquiry.paymentDate = billEnquiryResponse.responseDetailsOfPollyBiddyutBillEnquiry.paymentDate;
                    }
                }
                return responseDetailsOfBillEnquiry;
            }
        }catch (Exception ex){
            logWriterUtility.error("getPollyBiddyutBillEnquiry error",ex.getMessage());
            responseDetailsOfBillEnquiry = new ResponseDetailsOfPollyBiddyutBillEnquiry();
            responseDetailsOfBillEnquiry.status = Integer.parseInt(PocketConstants.BILL_PENDING);
            responseDetailsOfBillEnquiry.statusName = "Bill enquiry fail or pending now.";
            return responseDetailsOfBillEnquiry;
        }
        return null;
    }
    private ExternalResponse payBillPaymentPollyBiddyut(PolliBudditBillRequest request, String token, ResponseDetailsOfPollyBiddyutBillEnquiry billEnquiry) throws PocketException {
        try {
            logWriterUtility.info("payBillPaymentPollyBiddyut","payBillPaymentPollyBiddyut enter");
            PollyBiddyutBillPaymentResponse pollyBiddyutBillPaymentResponse = null;

            PollyBiddyutBillPaymentRequest billPaymentRequest = new PollyBiddyutBillPaymentRequest();
            billPaymentRequest.setUsername(request.getUsername());
            billPaymentRequest.setPassword(passwordHashedByMD5(request.getPassword()));
            billPaymentRequest.setBill_no(request.getAccount_no());
            billPaymentRequest.setPayerMobileNo(request.getPhoneNo());
            //For BillNumber = "PIB", AccountNumber ="PIS"
            if(request.getAccount_no() != null){
                billPaymentRequest.setBill_type("PIS");
            }
            if(billEnquiry != null && billEnquiry.trxId !=null){
                billPaymentRequest.setTrx_id(billEnquiry.trxId);
            }
            billPaymentRequest.setRef_id(request.getRef_id());
            billPaymentRequest.setFormat(request.getFormat());
            billPaymentRequest.setTimestamp(request.getTimestamp());
            logWriterUtility.info("payBillPaymentPollyBiddyut request object", new Gson().toJson(billPaymentRequest));

            RestTemplate restTemplate = new RestTemplate();
            MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
            mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
            restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
            String polliBuddyutHashedData = CommonTasks.requestHashedDataGenerate(new Gson().toJson(billPaymentRequest), payWellConfig.getEncryptionkey().getBytes());
            HttpHeaders billPaymentBearerHeader = CommonTasks.createHeaders(null, null, token,payWellConfig.getApiKey(), polliBuddyutHashedData);

            ResponseEntity<String> exchangeBillPayment = restTemplate.exchange(
                    payWellConfig.getBillPayment(), HttpMethod.POST,
                    new HttpEntity<>(billPaymentRequest, billPaymentBearerHeader), String.class);

            if((exchangeBillPayment != null) && (exchangeBillPayment.getStatusCode()==HttpStatus.OK)){

                 pollyBiddyutBillPaymentResponse = getBillPaymentResponse(exchangeBillPayment.getBody());

                if(pollyBiddyutBillPaymentResponse.apiStatus == HttpStatus.OK.value()) {
                    logWriterUtility.info("PAYWELL POLLIBUDDYUT BILL PAYMENT","after bill payment response from paywell "+exchangeBillPayment.getBody());

                    if (pollyBiddyutBillPaymentResponse.responseDetailsOfPollyBiddyutBillPayment != null &&
                            pollyBiddyutBillPaymentResponse.responseDetailsOfPollyBiddyutBillPayment.status == HttpStatus.OK.value()) {

                        logWriterUtility.info("PAYWELL POLLIBUDDYUT BILL PAYMENT","Bill payment confirmation check from paywell after bill pay: ");
                        return getResponse(pollyBiddyutBillPaymentResponse,
                                pollyBiddyutBillPaymentResponse.responseDetailsOfPollyBiddyutBillPayment.status.toString(),
                                pollyBiddyutBillPaymentResponse.responseDetailsOfPollyBiddyutBillPayment.statusName);
                    }
                }
                return getResponse(pollyBiddyutBillPaymentResponse,
                        String.valueOf(pollyBiddyutBillPaymentResponse.responseDetailsOfPollyBiddyutBillPayment.status),
                        pollyBiddyutBillPaymentResponse.responseDetailsOfPollyBiddyutBillPayment.statusName);
            }else{
                return getResponse(exchangeBillPayment, PocketConstants.ERROR,"API Connectivity Problem");
            }
        }catch (Exception ex){
            logWriterUtility.info("payBillPaymentPollyBiddyut error",ex.getMessage());
            return  getResponse(null,PocketConstants.ERROR,"Bill process is failed due to server error.");
        }
    }


    public static String passwordHashedByMD5(String password) {
        String hash = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            hash = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash;
    }

}


