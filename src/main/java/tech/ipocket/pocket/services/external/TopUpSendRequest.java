package tech.ipocket.pocket.services.external;

import java.math.BigDecimal;

public class TopUpSendRequest {
    private String SkuCode;
    private BigDecimal SendValue;
    private String AccountNumber;
    private String DistributorRef;
    private boolean ValidateOnly;

    public String getSkuCode() {
        return SkuCode;
    }

    public void setSkuCode(String skuCode) {
        SkuCode = skuCode;
    }

    public BigDecimal getSendValue() {
        return SendValue;
    }

    public void setSendValue(BigDecimal sendValue) {
        SendValue = sendValue;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getDistributorRef() {
        return DistributorRef;
    }

    public void setDistributorRef(String distributorRef) {
        DistributorRef = distributorRef;
    }

    public boolean isValidateOnly() {
        return ValidateOnly;
    }

    public void setValidateOnly(boolean validateOnly) {
        ValidateOnly = validateOnly;
    }
}
