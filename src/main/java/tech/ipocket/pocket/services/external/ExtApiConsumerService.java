package tech.ipocket.pocket.services.external;

import tech.ipocket.pocket.request.integration.UPayBaseRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayBaseRequest;
import tech.ipocket.pocket.request.integration.hss.HssBaseRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetBaseRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayBaseRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketException;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;

public interface ExtApiConsumerService {
    boolean checkConsumerValidity(HttpServletRequest httpServletRequest, UPayBaseRequest request) throws PocketException;
    String signPayload(BaseResponseObject responseObject) throws PocketException;

    boolean checkHssConsumerValidity(HttpServletRequest httpServletRequest, HssBaseRequest request) throws PocketException;
    String signHssPayload(BaseResponseObject responseObject) throws PocketException;

    boolean checkPaynetConsumerValidity(HttpServletRequest httpServletRequest, PaynetBaseRequest request) throws PocketException;
    String  signPaynetPayload(BaseResponseObject responseObject) throws PocketException;

    boolean checkEasyPayConsumerValidity(HttpServletRequest httpServletRequest, EasyPayBaseRequest request) throws PocketException;
    String  signEasyPayPayload(BaseResponseObject responseObject) throws PocketException;

    boolean checkZenZeroConsumerValidity(HttpServletRequest httpServletRequest, ZenZeroPayBaseRequest request) throws PocketException;
    String  signZenZeroPayload(BaseResponseObject responseObject) throws PocketException;
}
