package tech.ipocket.pocket.services.integration.paynet;

import tech.ipocket.pocket.request.integration.paynet.PaynetPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetUserBalanceRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetUserInfoRequest;
import tech.ipocket.pocket.response.integration.paynet.PaynetPaymentWalletRefillFindResponse;
import tech.ipocket.pocket.response.integration.paynet.PaynetPaymentWalletRefillResponse;
import tech.ipocket.pocket.response.integration.paynet.PaynetUserBalanceResponse;
import tech.ipocket.pocket.response.integration.paynet.PaynetUserInfoResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface PaynetApiService {
    PaynetUserInfoResponse getUserInfo(PaynetUserInfoRequest request) throws  PocketException;
    PaynetPaymentWalletRefillResponse walletRefill(PaynetPaymentWalletRefillRequest request) throws  PocketException;
    PaynetPaymentWalletRefillFindResponse checkWalletRefillByToken(PaynetPaymentWalletRefillFindRequest request) throws PocketException;
    PaynetUserBalanceResponse checkCustomerWalletBalance(PaynetUserBalanceRequest request) throws  PocketException;
}
