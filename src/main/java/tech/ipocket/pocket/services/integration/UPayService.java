package tech.ipocket.pocket.services.integration;

import tech.ipocket.pocket.request.integration.UPayTransactionFindRequest;
import tech.ipocket.pocket.request.integration.UPayTransactionRequest;
import tech.ipocket.pocket.request.integration.UPayUserInfoRequest;
import tech.ipocket.pocket.request.integration.UpayUserBalanceRequest;
import tech.ipocket.pocket.response.integration.UPayTransactionFindResponse;
import tech.ipocket.pocket.response.integration.UPayTransactionResponse;
import tech.ipocket.pocket.response.integration.UPayUserBalanceResponse;
import tech.ipocket.pocket.response.integration.UPayUserInfoResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface UPayService {
    UPayUserInfoResponse getUserInfoDetails(UPayUserInfoRequest request) throws  PocketException;
    UPayTransactionFindResponse findUPayTransactionByToken(UPayTransactionFindRequest request) throws PocketException;
    UPayTransactionResponse refillUpayTransaction(UPayTransactionRequest request) throws  PocketException;
    UPayUserBalanceResponse getUserBalance(UpayUserBalanceRequest request) throws  PocketException;
}
