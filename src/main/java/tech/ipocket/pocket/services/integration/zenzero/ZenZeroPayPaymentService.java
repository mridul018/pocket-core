package tech.ipocket.pocket.services.integration.zenzero;

import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayUserBalanceRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayUserInfoRequest;
import tech.ipocket.pocket.response.zenzero.ZenZeroPayPaymentWalletRefillFindResponse;
import tech.ipocket.pocket.response.zenzero.ZenZeroPayPaymentWalletRefillResponse;
import tech.ipocket.pocket.response.zenzero.ZenZeroPayUserBalanceResponse;
import tech.ipocket.pocket.response.zenzero.ZenZeroPayUserInfoResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface ZenZeroPayPaymentService {
    ZenZeroPayUserInfoResponse getUserInfo(ZenZeroPayUserInfoRequest request) throws  PocketException;
    ZenZeroPayPaymentWalletRefillResponse walletRefill(ZenZeroPayPaymentWalletRefillRequest request) throws  PocketException;
    ZenZeroPayPaymentWalletRefillFindResponse checkWalletRefillByToken(ZenZeroPayPaymentWalletRefillFindRequest request) throws PocketException;
    ZenZeroPayUserBalanceResponse checkCustomerWalletBalance(ZenZeroPayUserBalanceRequest request) throws  PocketException;
}
