package tech.ipocket.pocket.services.integration.hss;

import tech.ipocket.pocket.request.integration.hss.HssPaymentFindRequest;
import tech.ipocket.pocket.request.integration.hss.HssPaymentRequest;
import tech.ipocket.pocket.request.integration.hss.HssUserBalanceRequest;
import tech.ipocket.pocket.request.integration.hss.HssUserInfoRequest;
import tech.ipocket.pocket.response.integration.hss.*;
import tech.ipocket.pocket.utils.PocketException;

import javax.servlet.http.HttpServletRequest;

public interface HssPaymentService {
    HssUserInfoResponse getUserInfoDetails(HssUserInfoRequest request) throws PocketException;
    HssUserBalanceResponse getUserBalanceDetails(HssUserBalanceRequest request) throws PocketException;
    HssPaymentResponse payment(HssPaymentRequest request)throws PocketException;
    HssPaymentFindResponse findPaymentDetails(HssPaymentFindRequest request)throws PocketException;
    UserTokenResponse getUserToken(HttpServletRequest httpServletRequest,UserTokenRequest request) throws PocketException;

    boolean validUserToken(String token) throws PocketException;
    boolean verifyUserWalletPassword(String walletNo, String userWalletPassword) throws PocketException;

}
