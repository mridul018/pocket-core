package tech.ipocket.pocket.services.integration.easypay;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayUserBalanceRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayUserInfoRequest;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.integration.easypay.EasyPayPaymentWalletRefillFindResponse;
import tech.ipocket.pocket.response.integration.easypay.EasyPayPaymentWalletRefillResponse;
import tech.ipocket.pocket.response.integration.easypay.EasyPayUserBalanceResponse;
import tech.ipocket.pocket.response.integration.easypay.EasyPayUserInfoResponse;
import tech.ipocket.pocket.response.ts.IntermediateTxResponse;
import tech.ipocket.pocket.response.ts.transaction.TransactionHistoryResponse;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.SingleTransactionService;
import tech.ipocket.pocket.utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional(rollbackFor = {PocketException.class,Exception.class})
public class EasyPayPaymentServiceImpl implements EasyPayPaymentService {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());
    @Autowired
    private UserInfoRepository userInfoRepository;
    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;
    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsDisputeRepository disputeRepository;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private SingleTransactionService singleTransactionService;
    @Autowired
    private TsClientBalanceRepository tsClientBalanceRepository;

    @Override
    public EasyPayUserInfoResponse getUserInfo(EasyPayUserInfoRequest request) throws PocketException {
        UmUserInfo umUserInfo = userInfoRepository.findFirstByMobileNumber(request.getWalletNumber());

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Wallet not found.");
        }

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(), clientStatus);

        if(client == null){
            logWriterUtility.error(request.getRequestId(), "Ts Client not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Ts Client not found");
        }

        EasyPayUserInfoResponse response = new EasyPayUserInfoResponse();
        response.setFullName(umUserInfo.getFullName());
        if(umUserInfo.getGender() == null){
            response.setGender(umUserInfo.getGender());
        }else{
            if(umUserInfo.getGender().equals("1")){
                response.setGender("Male");
            }else if(umUserInfo.getGender().equals("2")){
                response.setGender("Female");
            }else {
                response.setGender(umUserInfo.getGender());
            }
        }
        response.setEmail(umUserInfo.getEmail());
        response.setRequestId(request.getRequestId());
        response.setRequestDateTime(DateUtil.getCurrentDateTime());
        return response;
    }

    @Override
    public EasyPayPaymentWalletRefillResponse walletRefill(EasyPayPaymentWalletRefillRequest request) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        if(request.getTransactionAmount() <= 0){
            logWriterUtility.error(request.getTransactionAmount().toString(), "Transaction amount is less to minimum.");
            throw new PocketException(PocketErrorCode.EASYPAY_TRANSACTION_AMOUNT, "Transaction amount is less to minimum.");
        }

        TsClient clientReceiver = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(), statusNotIn);

        if (clientReceiver == null) {
            logWriterUtility.error(request.getRequestId(), "Sender not found.");
            throw new PocketException(PocketErrorCode.ReceiverWalletNotFound, "Receiver wallet not found.");
        }

        TsService tsService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.CashInFromEasyPayKiosk);

        if (tsService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound, "Service not found.");
        }

        //Blocking EasyPay transaction at twice time using same transactionRefId.
        if(request != null && (request.getTransactionRefId() != null &&
                !request.getTransactionRefId().isEmpty() && !request.getTransactionRefId().equalsIgnoreCase("string"))){

            TsTransaction tsTransactionUPayToMaster = tsTransactionRepository.findByRefTransactionTokenAndTransactionType(request.getTransactionRefId(), ServiceCodeConstants.CashInFromEasyPayKioskIntermediate);
            TsTransaction tsTransactionMasterToUser = tsTransactionRepository.findByRefTransactionTokenAndTransactionType(request.getTransactionRefId(), ServiceCodeConstants.CashInFromEasyPayKiosk);

            if(tsTransactionUPayToMaster != null && tsTransactionMasterToUser != null){
                logWriterUtility.error(request.getTransactionRefId(), "Wallet refill transaction is already done by referring this : " + tsTransactionMasterToUser.getRefTransactionToken());
                throw new PocketException(PocketErrorCode.EASYPAY_DUPLICATE_TRANSACTION_NOT_ALLOWED.getKeyString(),
                        "Wallet refill transaction is already done by referring this : " + tsTransactionMasterToUser.getRefTransactionToken());
            }
        }else {
            logWriterUtility.error(request.getTransactionRefId(), "Reference transactionId  is not found.");
            throw new PocketException(PocketErrorCode.EASYPAY_REFERENCE_TRANSACTION_ID.getKeyString(), "Reference transactionId  is not found.");
        }

        //*** TODO API call to settlement bank for fund transfer from bank to settlement bank ***//
        String prevTransactionReference;
        try{
            BaseResponseObject baseResponseObject=singleTransactionService.doEasyPayKioskToMasterTransferIntermediate(request);

            IntermediateTxResponse intermediateTxResponse= (IntermediateTxResponse) baseResponseObject.getData();
            if(intermediateTxResponse!=null){
                prevTransactionReference=intermediateTxResponse.getTxReference();
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction successful");
            }else{
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction failed");
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }
        }catch (PocketException e){
            logWriterUtility.error(request.getRequestId(),e.getMessage());
            throw e;
        }

        IntermediateTxResponse mainTxResponse;
        try{
            BaseResponseObject baseResponseObject=singleTransactionService.doMasterToUserWalletTransfer(request,prevTransactionReference, request.getWalletNumber());

            if(baseResponseObject!=null){
                logWriterUtility.error(request.getRequestId(),"Transaction successful");

                mainTxResponse= (IntermediateTxResponse) baseResponseObject.getData();
            }else{
                logWriterUtility.error(request.getRequestId(),"Intermediate transaction failed");
                throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
            }

        }catch (PocketException e){
            logWriterUtility.error(request.getRequestId(),e.getMessage());
            throw e;
        }

        EasyPayPaymentWalletRefillResponse easyPayPaymentWalletRefillResponse = new EasyPayPaymentWalletRefillResponse();
        easyPayPaymentWalletRefillResponse.setTokenNo(mainTxResponse.getTxReference());
        easyPayPaymentWalletRefillResponse.setRequestId(request.getRequestId());
        easyPayPaymentWalletRefillResponse.setRequestDateTime(request.getRequestDateTime());
        easyPayPaymentWalletRefillResponse.setTransactionStatusCode(TsEnums.TransactionStatus.COMPLETED.getTransactionStatus());
        easyPayPaymentWalletRefillResponse.setMessage("RequestId :"+ request.getRequestId() + "\n"
                + " Transaction refill successfully to "+ " " + request.getWalletNumber() + "\n"
                + " Amount transfer :" + " " + request.getTransactionAmount() +"\n"
                + " Transaction time : " + " " + mainTxResponse.getTxCreateDate());
        easyPayPaymentWalletRefillResponse.setTransactionRefId(request.getTransactionRefId());

        NotificationBuilder notificationBuilder = new NotificationBuilder();
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(new BigDecimal(request.getTransactionAmount()),
                        clientReceiver.getWalletNo(),
                        "EasyPay Bank", mainTxResponse.getTxReference(), tsService.getServiceCode(),
                        mainTxResponse.getFeePayer(), mainTxResponse.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(easyPayPaymentWalletRefillResponse), request.getRequestId(), request.getUserNotes(),
                        "EasyPay Bank",request.getWalletNumber());
        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        return easyPayPaymentWalletRefillResponse;
    }

    @Override
    public EasyPayPaymentWalletRefillFindResponse checkWalletRefillByToken(EasyPayPaymentWalletRefillFindRequest request) throws PocketException {
        TsTransaction tsTransaction = tsTransactionRepository.findFirstByToken(request.getTokenNo());
        if (tsTransaction == null) {
            logWriterUtility.error(request.getRequestId(), "No transaction found by provided token :" + request.getTokenNo());
            throw new PocketException(PocketErrorCode.InvalidTransactionToken, "Invalid transaction token.");
        }

        TsDispute tsDispute = disputeRepository.findFirstByTransactionTokenNoOrderByIdDesc(tsTransaction.getToken());

        TransactionHistoryResponse response = new TransactionHistoryResponse();
        response.setTransactionType(tsTransaction.getTransactionType());

        TsService tsService = tsServiceRepository.findFirstByServiceCode(tsTransaction.getTransactionType());
        if (tsService != null) {
            response.setTransactionType(tsService.getDescription());
        }

        response.setId(tsTransaction.getId());
        response.setCreatedDate(tsTransaction.getCreatedDate().toString());
        response.setDisputable(tsTransaction.getIsDisputable());

        response.setFeeAmount(BigDecimal.ZERO.doubleValue());
        if (tsTransaction.getFeeAmount() == null) {
            response.setFeeAmount(BigDecimal.ZERO.doubleValue());
        } else {
            if (tsTransaction.getFeePayer() != null && tsTransaction.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
                response.setFeeAmount(tsTransaction.getFeeAmount().doubleValue());
            }
        }

        response.setFeeCode(tsTransaction.getFeeCode());
        response.setFeePayer(tsTransaction.getFeePayer());
        response.setTransactionAmount(tsTransaction.getTransactionAmount().doubleValue());
        response.setSenderDebitAmount(tsTransaction.getSenderDebitAmount().doubleValue());
        response.setReceiverCreditAmount(tsTransaction.getReceiverCreditAmount().doubleValue());
        response.setToken(tsTransaction.getToken());
        response.setSenderWallet(tsTransaction.getSenderWallet());

        if(tsTransaction.getTransactionType().equals(TsEnums.Services.CashInFromEasyPayKiosk.getServiceCode())){
            response.setReceiverWallet(tsTransaction.getLogicalReceiver());
        }else{
            response.setReceiverWallet(tsTransaction.getReceiverWallet());
        }

        response.setTransactionStatus(tsTransaction.getTransactionStatus());
        response.setPrevTransactionRef(tsTransaction.getRefTransactionToken());
        response.setFeeCode(tsTransaction.getFeeCode());
        response.setLogicalSender(tsTransaction.getLogicalSender());
        response.setLogicalReceiver(tsTransaction.getLogicalReceiver());

        if (tsDispute != null) {
            response.setDisputable(false);
            response.setDisputeStatus(getDisputeStatusByStatus(Integer.parseInt(tsDispute.getStatus())));
            response.setDisputeReason(tsDispute.getReason());
            response.setDisputeResolvedMessage(tsDispute.getResolvedMessage());
        }else{
            response.setDisputable(tsTransaction.getIsDisputable());
        }

        EasyPayPaymentWalletRefillFindResponse easyPayPaymentWalletRefillFindResponse = new EasyPayPaymentWalletRefillFindResponse();
        easyPayPaymentWalletRefillFindResponse.setTokenNo(response.getToken());
        easyPayPaymentWalletRefillFindResponse.setRequestId(request.getRequestId());
        easyPayPaymentWalletRefillFindResponse.setTransactionAmount(response.getTransactionAmount());
        easyPayPaymentWalletRefillFindResponse.setWalletNumber(response.getReceiverWallet());
        easyPayPaymentWalletRefillFindResponse.setRequestDateTime(DateUtil.getCurrentDateTime());
        easyPayPaymentWalletRefillFindResponse.setTransactionStatusCode(response.getTransactionStatus());
        return easyPayPaymentWalletRefillFindResponse;
    }

    @Override
    public EasyPayUserBalanceResponse checkCustomerWalletBalance(EasyPayUserBalanceRequest request) throws PocketException {
        UmUserInfo umUserInfo = userInfoRepository.findFirstByMobileNumber(request.getWalletNumber());

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Wallet not found.");
        }

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(), clientStatus);

        if(client == null){
            logWriterUtility.error(request.getRequestId(), "Ts Client not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Ts Client not found");
        }

        Optional<TsClientBalance> clientBalance = tsClientBalanceRepository.findByTsClientByClientId(client);

        EasyPayUserBalanceResponse response = new EasyPayUserBalanceResponse();
        response.setRequestId(request.getRequestId());
        response.setBalance(clientBalance.get().getBalance());
        response.setRequestDateTime(DateUtil.getCurrentDateTime());

        return response;
    }

    private String getDisputeStatusByStatus(int status){
        switch (status){
            case 1:
                return "INITIALIZED";
            case 2:
                return "RESOLVED";
            case 3:
                return "REVERSED";
        }
        return ""+status;
    }
}
