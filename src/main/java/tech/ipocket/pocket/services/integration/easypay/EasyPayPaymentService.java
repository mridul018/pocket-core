package tech.ipocket.pocket.services.integration.easypay;

import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayUserBalanceRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayUserInfoRequest;
import tech.ipocket.pocket.response.integration.easypay.EasyPayPaymentWalletRefillFindResponse;
import tech.ipocket.pocket.response.integration.easypay.EasyPayPaymentWalletRefillResponse;
import tech.ipocket.pocket.response.integration.easypay.EasyPayUserBalanceResponse;
import tech.ipocket.pocket.response.integration.easypay.EasyPayUserInfoResponse;
import tech.ipocket.pocket.utils.PocketException;

public interface EasyPayPaymentService {
    EasyPayUserInfoResponse getUserInfo(EasyPayUserInfoRequest request) throws  PocketException;
    EasyPayPaymentWalletRefillResponse walletRefill(EasyPayPaymentWalletRefillRequest request) throws  PocketException;
    EasyPayPaymentWalletRefillFindResponse checkWalletRefillByToken(EasyPayPaymentWalletRefillFindRequest request) throws PocketException;
    EasyPayUserBalanceResponse checkCustomerWalletBalance(EasyPayUserBalanceRequest request) throws  PocketException;
}
