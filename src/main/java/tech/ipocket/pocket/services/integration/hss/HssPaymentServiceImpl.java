package tech.ipocket.pocket.services.integration.hss;

import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.ipocket.pocket.entity.*;
import tech.ipocket.pocket.repository.external.ExtApiConsumerRepository;
import tech.ipocket.pocket.repository.ts.*;
import tech.ipocket.pocket.repository.um.UserInfoRepository;
import tech.ipocket.pocket.request.integration.hss.HssPaymentFindRequest;
import tech.ipocket.pocket.request.integration.hss.HssPaymentRequest;
import tech.ipocket.pocket.request.integration.hss.HssUserBalanceRequest;
import tech.ipocket.pocket.request.integration.hss.HssUserInfoRequest;
import tech.ipocket.pocket.request.notification.NotificationRequest;
import tech.ipocket.pocket.response.integration.hss.*;
import tech.ipocket.pocket.response.ts.transaction.TransactionHistoryResponse;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.SingleTransactionService;
import tech.ipocket.pocket.services.ts.TransactionValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeManager;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.utils.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CompletableFuture;


@Service
@Transactional(rollbackFor = {PocketException.class,Exception.class})
public class HssPaymentServiceImpl implements HssPaymentService{

    private LogWriterUtility logWriterUtility = new LogWriterUtility(this.getClass());
    @Autowired
    private UserInfoRepository userInfoRepository;
    @Autowired
    private TsClientRepository clientRepository;
    @Autowired
    private TsTransactionRepository tsTransactionRepository;
    @Autowired
    private TsServiceRepository tsServiceRepository;
    @Autowired
    private TsDisputeRepository disputeRepository;
    @Autowired
    private TsServiceRepository serviceRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private SingleTransactionService singleTransactionService;
    @Autowired
    private TsClientBalanceRepository tsClientBalanceRepository;
    @Autowired
    private FeeManager feeManager;
    @Autowired
    private TransactionValidator transactionValidator;
    @Autowired
    private TsTransactionDetailsRepository tsTransactionDetailsRepository;
    @Autowired
    private TsGlRepository tsGlRepository;
    @Autowired
    private TsGlTransactionDetailsRepository tsGlTransactionDetailsRepository;
    @Autowired
    private AccountService accountService;
    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;
    @Autowired
    private ExtApiConsumerRepository extApiConsumerRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private Environment environment;

    @Override
    public HssUserInfoResponse getUserInfoDetails(HssUserInfoRequest request) throws PocketException {
        UmUserInfo umUserInfo = userInfoRepository.findFirstByMobileNumber(request.getWalletNumber());

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Wallet not found.");
        }

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(), clientStatus);

        if(client == null){
            logWriterUtility.error(request.getRequestId(), "Ts Client not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Ts Client not found");
        }

        HssUserInfoResponse response = new HssUserInfoResponse();
        response.setFullName(umUserInfo.getFullName());
        if(umUserInfo.getGender() == null){
            response.setGender(umUserInfo.getGender());
        }else{
            if(umUserInfo.getGender().equals("1")){
                response.setGender("Male");
            }else if(umUserInfo.getGender().equals("2")){
                response.setGender("Female");
            }else {
                response.setGender(umUserInfo.getGender());
            }
        }
        response.setEmail(umUserInfo.getEmail());
        response.setRequestId(request.getRequestId());
        response.setRequestDateTime(DateUtil.getCurrentDateTime());
        return response;
    }

    @Override
    public HssUserBalanceResponse getUserBalanceDetails(HssUserBalanceRequest request) throws PocketException {

        UmUserInfo umUserInfo = userInfoRepository.findFirstByMobileNumber(request.getWalletNumber());

        if (umUserInfo == null) {
            logWriterUtility.error(request.getRequestId(), "User not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Wallet not found.");
        }

        List<String> clientStatus = new ArrayList<>();
        clientStatus.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        TsClient client = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(), clientStatus);

        if(client == null){
            logWriterUtility.error(request.getRequestId(), "Ts Client not found : "+request.getWalletNumber());
            throw new PocketException(PocketErrorCode.WalletNotFound, "Ts Client not found");
        }

        Optional<TsClientBalance> clientBalance = tsClientBalanceRepository.findByTsClientByClientId(client);

        HssUserBalanceResponse response = new HssUserBalanceResponse();
        response.setRequestId(request.getRequestId());
        response.setBalance(clientBalance.get().getBalance());
        response.setRequestDateTime(DateUtil.getCurrentDateTime());

        return response;
    }

    @Override
    public HssPaymentResponse payment(HssPaymentRequest request) throws PocketException {
        List<String> statusNotIn = new ArrayList<>();
        statusNotIn.add(TsEnums.UserStatus.DELETED.getUserStatusType());
        statusNotIn.add(TsEnums.UserStatus.INITIALIZED.getUserStatusType());

        TsClient clientSenderCustomerOrMerchant = clientRepository.findFirstByWalletNoAndUserStatusNotIn(request.getWalletNumber(), statusNotIn);

        if (clientSenderCustomerOrMerchant == null) {
            logWriterUtility.error(request.getRequestId(), "Sender wallet not found.");
            throw new PocketException(PocketErrorCode.SenderWalletNotFound);
        }

        TsClient receiverMerchant = clientRepository.findFirstByWalletNoAndUserStatusNotIn(environment.getProperty("hssMerchantMobileNo"), statusNotIn);
        if(receiverMerchant == null){
            logWriterUtility.error(request.getRequestId(), "Receiver wallet not found.");
            throw new PocketException(PocketErrorCode.MerchantNotFound);
        }

        boolean isReceiverMerchantValid=(receiverMerchant.getGroupCode().equals(TsEnums.UserType.MERCHANT.getUserTypeString()));
        if(!isReceiverMerchantValid){
            logWriterUtility.error(request.getRequestId(), "Receiver wallet is not active merchant.");
            throw new PocketException(PocketErrorCode.MerchantNotFound);
        }

        TsService cfeService = serviceRepository.findFirstByServiceCode(ServiceCodeConstants.HelloSuperStar); //HSS Merchant_Payment

        if (cfeService == null) {
            logWriterUtility.error(request.getRequestId(), "Service not found.");
            throw new PocketException(PocketErrorCode.ServiceNotFound);
        }

        //calculate fee
        FeeData feeData = feeManager.calculateFee(clientSenderCustomerOrMerchant, cfeService.getServiceCode(),
                new BigDecimal(request.getTransactionAmount()), request.getRequestId());

        if (feeData == null || feeData.getFeeAmount() == null) {
            logWriterUtility.error(request.getRequestId(), "Fee calculation failed");
            throw new PocketException(PocketErrorCode.InvalidFeeConfiguration);
        }

        TransactionAmountData transactionAmountData = new TransactionAmountData(feeData,
                new BigDecimal(request.getTransactionAmount()));

        transactionAmountData.setSenderDebitAmount(new BigDecimal(request.getTransactionAmount()));
        transactionAmountData.setReceiverCreditAmount(new BigDecimal(request.getTransactionAmount()));


        logWriterUtility.trace(request.getRequestId(), "Fee calculation completed");

        // payment from customer
        if(clientSenderCustomerOrMerchant.getGroupCode().equalsIgnoreCase(GroupCodes.CUSTOMER)){

            //transaction profile validation for sender + check balance limit for sender
            Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSenderCustomerOrMerchant, cfeService,
                    transactionAmountData.getSenderDebitAmount(),request.getRequestId());
            if (!isSenderProfileValid) {
                logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
                throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
            }

            //transaction profile validation for receiver
            receiverMerchant.setTransactionProfileCode(receiverMerchant.getGroupCode());
            Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(receiverMerchant, cfeService,request.getRequestId());
            if (!isReceiverProfileValid) {
                logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
                throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
            }
        }

        // payment from merchant
        if(clientSenderCustomerOrMerchant.getGroupCode().equalsIgnoreCase(GroupCodes.MERCHANT)){

            //transaction profile validation for sender + check balance limit for sender
            clientSenderCustomerOrMerchant.setTransactionProfileCode(clientSenderCustomerOrMerchant.getGroupCode());
            Boolean isSenderProfileValid = transactionValidator.isSenderProfileValidForTransaction(clientSenderCustomerOrMerchant, cfeService,
                    transactionAmountData.getSenderDebitAmount(),request.getRequestId());
            if (!isSenderProfileValid) {
                logWriterUtility.error(request.getRequestId(), "Sender profile validation failed");
                throw new PocketException(PocketErrorCode.SenderProfileValidationFailed);
            }

            //transaction profile validation for receiver
            receiverMerchant.setTransactionProfileCode(receiverMerchant.getGroupCode());
            Boolean isReceiverProfileValid = transactionValidator.isReceiverProfileValidForTransaction(receiverMerchant, cfeService,request.getRequestId());
            if (!isReceiverProfileValid) {
                logWriterUtility.error(request.getRequestId(), "Receiver profile validation failed");
                throw new PocketException(PocketErrorCode.ReceiverProfileValidationFailed);
            }
        }



        TransactionBuilder transactionBuilder = new TransactionBuilder();

        //build transaction
        TsTransaction cfeTransaction = transactionBuilder.buildTransaction(clientSenderCustomerOrMerchant, receiverMerchant,
                                       cfeService.getServiceCode(),feeData, transactionAmountData);
        cfeTransaction.setSource("HSS");
        cfeTransaction.setRefTransactionToken(request.getTransactionRefId());
        cfeTransaction.setNotes(request.getHssUserId().concat(".").concat(request.getHssProductTypeName()));
        cfeTransaction.setRequestId(request.getRequestId());
        cfeTransaction.setTransactionStatus(TsEnums.EodStatus.COMPLETED.getEodStatus());
        tsTransactionRepository.save(cfeTransaction);

        logWriterUtility.trace(request.getRequestId(), "CfeTransaction saved");

        //build transaction detail
        List<TsTransactionDetails> transactionDetailList = new ArrayList<>();

        TsTransactionDetails senderTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(),
                FeePayer.DEBIT,
                clientSenderCustomerOrMerchant.getWalletNo(), transactionAmountData.getSenderDebitAmount());
        transactionDetailList.add(senderTransactionDetails);

        TsTransactionDetails receiverTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(),
                FeePayer.CREDIT,
                receiverMerchant.getWalletNo(), transactionAmountData.getReceiverCreditAmount());
        transactionDetailList.add(receiverTransactionDetails);

        TsTransactionDetails feeTransactionDetails = transactionBuilder.buildCfeTransactionDetailsItem(cfeTransaction.getId(),
                FeePayer.CREDIT,
                "FEE", transactionAmountData.getTotalFeeAmount());
        transactionDetailList.add(feeTransactionDetails);

        tsTransactionDetailsRepository.saveAll(transactionDetailList);
        logWriterUtility.trace(request.getRequestId(), "CfeTransaction details list saved");

        //build gl entry
        List<TsGlTransactionDetails> glTxDetailList = new ArrayList<>();

        String senderGlName=CommonTasks.getGlNameByCustomerType(clientSenderCustomerOrMerchant);

        TsGl senderGl=tsGlRepository.findFirstByGlName(senderGlName);

        if(senderGl==null){
            logWriterUtility.error(request.getRequestId(),"Sender gl not found :"+senderGlName);
            throw new PocketException(PocketErrorCode.SenderGlNotFound);
        }


        TsGlTransactionDetails senderDebitGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                senderGlName, transactionAmountData.getSenderDebitAmount(), BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(senderDebitGl);

        String receiverGlName=CommonTasks.getGlNameByCustomerType(receiverMerchant);

        TsGl receiverGl=tsGlRepository.findFirstByGlName(receiverGlName);

        if(receiverGl==null){
            logWriterUtility.error(request.getRequestId(),"Receiver gl not found :"+receiverGlName);
            throw new PocketException(PocketErrorCode.ReceiverGlNotFound);
        }

        TsGlTransactionDetails receiverCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                receiverGlName, BigDecimal.valueOf(0), transactionAmountData.getReceiverCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(receiverCreditGl);

        TsGlTransactionDetails vatCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.VAT_Payable, BigDecimal.valueOf(0), transactionAmountData.getVatCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(vatCreditGl);

        TsGlTransactionDetails expenseGlDebitEntry = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.POCKET_EXPENSE_GL_CODE_FOR_CASHIN,  transactionAmountData.getFeeCreditAmount(),BigDecimal.valueOf(0),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(expenseGlDebitEntry);

        TsGlTransactionDetails feeCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.Fee_payable_for_Cash_In, BigDecimal.valueOf(0), transactionAmountData.getFeeCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        feeCreditGl.setEodStatus(TsEnums.EodStatus.INITIALIZE.getEodStatus());
        glTxDetailList.add(feeCreditGl);

        TsGlTransactionDetails aitCreditGl = transactionBuilder.buildGlTransactionDetailsItem(cfeTransaction,
                GlConstants.AIT_Payable, BigDecimal.valueOf(0), transactionAmountData.getAitCreditAmount(),
                request.getRequestId(), true, tsGlRepository);
        glTxDetailList.add(aitCreditGl);

        tsGlTransactionDetailsRepository.saveAll(glTxDetailList);

        BigDecimal senderRunningBalance = accountService.updateClientCurrentBalance(clientSenderCustomerOrMerchant, request.getRequestId());
        BigDecimal receiverRunningBalance = accountService.updateClientCurrentBalance(receiverMerchant, request.getRequestId());
        logWriterUtility.trace(request.getRequestId(), "Sender Receiver balance update done");

        cfeTransaction.setSenderRunningBalance(senderRunningBalance);
        cfeTransaction.setReceiverRunningBalance(receiverRunningBalance);
        tsTransactionRepository.save(cfeTransaction);

        HssPaymentResponse hssPaymentResponse = new HssPaymentResponse();
        hssPaymentResponse.setTokenNo(cfeTransaction.getToken());
        hssPaymentResponse.setRequestId(cfeTransaction.getRequestId());
        hssPaymentResponse.setRequestDateTime(String.valueOf(LocalDate.now()));
        hssPaymentResponse.setTransactionStatusCode(cfeTransaction.getTransactionStatus());
        hssPaymentResponse.setTransactionRefId(cfeTransaction.getRefTransactionToken());
        hssPaymentResponse.setSenderWalletNumber(clientSenderCustomerOrMerchant.getWalletNo());
        hssPaymentResponse.setTransactionAmount(cfeTransaction.getTransactionAmount().doubleValue());
        hssPaymentResponse.setUserNotes(cfeTransaction.getNotes());
        hssPaymentResponse.setHssUserId(cfeTransaction.getNotes());
        hssPaymentResponse.setHssProductTypeName(cfeTransaction.getNotes());
        hssPaymentResponse.setSenderRunningBalance(senderRunningBalance);


        NotificationBuilder notificationBuilder = new NotificationBuilder();

        //send notification
        NotificationRequest notificationRequest = notificationBuilder
                .buildNotificationRequest(transactionAmountData.getOriginalTransactionAmount(),
                        receiverMerchant.getWalletNo(),
                        clientSenderCustomerOrMerchant.getWalletNo(), cfeTransaction.getToken(), cfeService.getServiceCode(),
                        feeData.getFeePayer(), feeData.getFeeAmount(), new Gson().toJson(request),
                        new Gson().toJson(hssPaymentResponse), request.getRequestId(), request.getUserNotes());

        CompletableFuture.runAsync(() -> notificationService.sendNotification(notificationRequest));

        CompletableFuture.runAsync(() -> {
            mfsFeeSharingService.doFeeShareForIndividualTransaction(cfeTransaction.getToken(),
                    request.getRequestId());
        });

        return hssPaymentResponse;
    }

    @Override
    public HssPaymentFindResponse findPaymentDetails(HssPaymentFindRequest request) throws PocketException {

        TsTransaction tsTransaction = tsTransactionRepository.findFirstByToken(request.getTokenNo());
        if (tsTransaction == null) {
            logWriterUtility.error(request.getRequestId(), "No transaction found by provided token :" + request.getTokenNo());
            throw new PocketException(PocketErrorCode.InvalidTransactionToken, "Invalid transaction token.");
        }

        TsDispute tsDispute = disputeRepository.findFirstByTransactionTokenNoOrderByIdDesc(tsTransaction.getToken());

        TransactionHistoryResponse response = new TransactionHistoryResponse();
        response.setTransactionType(tsTransaction.getTransactionType());

        TsService tsService = tsServiceRepository.findFirstByServiceCode(tsTransaction.getTransactionType());
        if (tsService != null) {
            response.setTransactionType(tsService.getDescription());
        }

        response.setId(tsTransaction.getId());
        response.setCreatedDate(tsTransaction.getCreatedDate().toString());
        response.setDisputable(tsTransaction.getIsDisputable());

        response.setFeeAmount(BigDecimal.ZERO.doubleValue());
        if (tsTransaction.getFeeAmount() == null) {
            response.setFeeAmount(BigDecimal.ZERO.doubleValue());
        } else {
            if (tsTransaction.getFeePayer() != null && tsTransaction.getFeePayer().equalsIgnoreCase(FeePayer.DEBIT)) {
                response.setFeeAmount(tsTransaction.getFeeAmount().doubleValue());
            }
        }

        response.setFeeCode(tsTransaction.getFeeCode());
        response.setFeePayer(tsTransaction.getFeePayer());
        response.setTransactionAmount(tsTransaction.getTransactionAmount().doubleValue());
        response.setSenderDebitAmount(tsTransaction.getSenderDebitAmount().doubleValue());
        response.setReceiverCreditAmount(tsTransaction.getReceiverCreditAmount().doubleValue());
        response.setToken(tsTransaction.getToken());
        response.setSenderWallet(tsTransaction.getSenderWallet());

        response.setReceiverWallet(tsTransaction.getLogicalReceiver());
        response.setReceiverWallet(tsTransaction.getReceiverWallet());

        response.setTransactionStatus(tsTransaction.getTransactionStatus());
        response.setPrevTransactionRef(tsTransaction.getRefTransactionToken());
        response.setFeeCode(tsTransaction.getFeeCode());
        response.setLogicalSender(tsTransaction.getLogicalSender());
        response.setLogicalReceiver(tsTransaction.getLogicalReceiver());

        if (tsDispute != null) {
            response.setDisputable(false);
            response.setDisputeStatus(getDisputeStatusByStatus(Integer.parseInt(tsDispute.getStatus())));
            response.setDisputeReason(tsDispute.getReason());
            response.setDisputeResolvedMessage(tsDispute.getResolvedMessage());
        }else{
            response.setDisputable(tsTransaction.getIsDisputable());
        }

        HssPaymentFindResponse hssPaymentFindResponse = new HssPaymentFindResponse();
        hssPaymentFindResponse.setTokenNo(response.getToken());
        hssPaymentFindResponse.setRequestId(request.getRequestId());
        hssPaymentFindResponse.setTransactionAmount(response.getTransactionAmount());
        hssPaymentFindResponse.setRequestDateTime(DateUtil.getCurrentDateTime());
        hssPaymentFindResponse.setTransactionStatusCode(response.getTransactionStatus());
        return hssPaymentFindResponse;
    }

    @Override
    public UserTokenResponse getUserToken(HttpServletRequest httpServletRequest, UserTokenRequest request) throws PocketException {
        UserTokenResponse userTokenResponse = new UserTokenResponse();

        String userId = environment.getProperty("hss.user");
        String password = environment.getProperty("hss.password");
        if(userId.equalsIgnoreCase(request.getUserId()) && password.equalsIgnoreCase(request.getPassword())){
            //IP verify
            String ipAddress = httpServletRequest.getRemoteAddr(); // remote local(0:0:0:0:0:0:0:1)

            ExtApiConsumer clientToken = extApiConsumerRepository.findFirstByClientTokenIgnoreCaseAndStatus(request.getClientToken(), 1);
            if(clientToken == null){
                logWriterUtility.error(request.getRequestId(), "Invalid API consumer client token."+ request.getClientToken() +":+clientToken");
                throw new PocketException(PocketErrorCode.HSS_CLIENT_TOKEN.getKeyString(), "Invalid API consumer client token.");
            }

            ExtApiConsumer apiConsumer = extApiConsumerRepository.findFirstByIpAddressAndClientTokenIgnoreCaseAndStatus(ipAddress, request.getClientToken(), 1);
            if(apiConsumer == null){
                logWriterUtility.error(request.getRequestId(), "API consumer info invalid ."+ ipAddress +":+ipAddress");
                throw new PocketException(PocketErrorCode.API_CONSUMER_INFO_INVALID.getKeyString(), "Invalid API consumer provided information.");
            }

            // generate token
            SecuritySettings securitySettings = new SecuritySettings();
            Map<String, Object> claims = new HashMap<>();
            claims.put("userId", request.getUserId());
            claims.put("password", request.getPassword());

            String originalInput = request.getUserId().concat(":"+request.getPassword());
            String token = Jwts.builder()
                    .setClaims(claims)
                    .setSubject(String.valueOf(request.getUserId()))
                    .setExpiration(new Date(System.currentTimeMillis() + securitySettings.EXPIRATION_TIME))
                    .signWith(SignatureAlgorithm.HS512, Base64.getEncoder().encodeToString(originalInput.getBytes()))
                    .compact();

            if(token != null){
                userTokenResponse.setStatus(PocketConstants.OK);
                userTokenResponse.setMessage("success");
                userTokenResponse.setSecurity_token(token);
                userTokenResponse.setToken_exp_time(String.valueOf(securitySettings.EXPIRATION_TIME));
            }

        }else {
            userTokenResponse.setStatus(PocketConstants.ERROR);
            userTokenResponse.setMessage("Failed. please check userId and password.");
            userTokenResponse.setSecurity_token(null);
            userTokenResponse.setToken_exp_time(null);
        }

        return userTokenResponse;
    }

    @Override
    public boolean validUserToken(String token) throws PocketException {
        String[] chunks = token.split("\\.");

        Base64.Decoder decoder = Base64.getUrlDecoder();
        String header = new String(decoder.decode(chunks[0]));
        String payload = new String(decoder.decode(chunks[1]));

        TokenPayloadObject tokenPayloadObject = new TokenPayloadObject();
        tokenPayloadObject = new Gson().fromJson(payload, TokenPayloadObject.class);

        String userId = environment.getProperty("hss.user");
        String password = environment.getProperty("hss.password");

        if(tokenPayloadObject != null){
            if(tokenPayloadObject.getUserId().equalsIgnoreCase(userId) && tokenPayloadObject.getPassword().equalsIgnoreCase(password)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean verifyUserWalletPassword(String walletNo, String userWalletPassword) throws PocketException{
        try {
            UmUserInfo umUserInfo = userInfoRepository.findFirstByMobileNumber(walletNo);
            boolean isPasswordMatches = passwordEncoder.matches(userWalletPassword, umUserInfo.getPassword());

            if(umUserInfo != null && isPasswordMatches){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private String getDisputeStatusByStatus(int status){
        switch (status){
            case 1:
                return "INITIALIZED";
            case 2:
                return "RESOLVED";
            case 3:
                return "REVERSED";
        }
        return ""+status;
    }

}
