package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "UM_USER_DEVICE", schema = "PocketWallet")
public class UmUserDevice {
    private int id;
    private String hardwareSignature;
    private String deviceName;
    private String metaData;
    private Date createdDate;
    private Collection<UmUserDeviceMapping> umUserDeviceMappingsById;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "hardware_signature")
    public String getHardwareSignature() {
        return hardwareSignature;
    }

    public void setHardwareSignature(String hardwareSignature) {
        this.hardwareSignature = hardwareSignature;
    }

    @Basic
    @Column(name = "device_name")
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Basic
    @Column(name = "meta_data")
    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    @OneToMany(mappedBy = "umUserDeviceByDeviceId")
    public Collection<UmUserDeviceMapping> getUmUserDeviceMappingsById() {
        return umUserDeviceMappingsById;
    }

    public void setUmUserDeviceMappingsById(Collection<UmUserDeviceMapping> umUserDeviceMappingsById) {
        this.umUserDeviceMappingsById = umUserDeviceMappingsById;
    }
}
