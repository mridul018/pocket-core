package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "UM_EXTERNAL_ACTIVITY_LOG", schema = "PocketWallet", catalog = "")
public class UmExternalActivityLog {
    private Integer id;
    private String type;
    private String country;
    private String sender;
    private String topupReceiver;
    private String operator;
    private String connection;
    private String billPayCustomerName;
    private String billPayReceiver;
    private String billNo;
    private String accountNo;
    private BigDecimal amount;
    private Integer status;
    private String message;
    private String refId;
    private Timestamp datetime;
    private String requestUrl;
    private Boolean requestDirectionIn;
    private Boolean requestDirectionOut;
    private String communicationMedium;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type", nullable = false, length = 30)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 45)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "sender", nullable = true, length = -1)
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Basic
    @Column(name = "topup_receiver", nullable = true, length = -1)
    public String getTopupReceiver() {
        return topupReceiver;
    }

    public void setTopupReceiver(String topupReceiver) {
        this.topupReceiver = topupReceiver;
    }

    @Basic
    @Column(name = "operator", nullable = true, length = 45)
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Basic
    @Column(name = "connection", nullable = true, length = 45)
    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    @Basic
    @Column(name = "bill_pay_customer_name", nullable = true, length = 500)
    public String getBillPayCustomerName() {
        return billPayCustomerName;
    }

    public void setBillPayCustomerName(String billPayCustomerName) {
        this.billPayCustomerName = billPayCustomerName;
    }

    @Basic
    @Column(name = "bill_pay_receiver", nullable = true, length = 15)
    public String getBillPayReceiver() {
        return billPayReceiver;
    }

    public void setBillPayReceiver(String billPayReceiver) {
        this.billPayReceiver = billPayReceiver;
    }

    @Basic
    @Column(name = "bill_no", nullable = true, length = -1)
    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Basic
    @Column(name = "account_no", nullable = true, length = -1)
    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Basic
    @Column(name = "amount", nullable = false, precision = 4)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "message", nullable = false, length = -1)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "ref_id", nullable = true, length = 200)
    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    @Basic
    @Column(name = "datetime", nullable = true)
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "request_url", nullable = true, length = -1)
    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    @Basic
    @Column(name = "request_direction_in", nullable = true)
    public Boolean getRequestDirectionIn() {
        return requestDirectionIn;
    }

    public void setRequestDirectionIn(Boolean requestDirectionIn) {
        this.requestDirectionIn = requestDirectionIn;
    }

    @Basic
    @Column(name = "request_direction_out", nullable = true)
    public Boolean getRequestDirectionOut() {
        return requestDirectionOut;
    }

    public void setRequestDirectionOut(Boolean requestDirectionOut) {
        this.requestDirectionOut = requestDirectionOut;
    }

    @Basic
    @Column(name = "communication_medium", nullable = true, length = 20)
    public String getCommunicationMedium() {
        return communicationMedium;
    }

    public void setCommunicationMedium(String communicationMedium) {
        this.communicationMedium = communicationMedium;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmExternalActivityLog that = (UmExternalActivityLog) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(type, that.type) &&
                Objects.equals(country, that.country) &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(topupReceiver, that.topupReceiver) &&
                Objects.equals(operator, that.operator) &&
                Objects.equals(connection, that.connection) &&
                Objects.equals(billPayCustomerName, that.billPayCustomerName) &&
                Objects.equals(billPayReceiver, that.billPayReceiver) &&
                Objects.equals(billNo, that.billNo) &&
                Objects.equals(accountNo, that.accountNo) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(status, that.status) &&
                Objects.equals(message, that.message) &&
                Objects.equals(refId, that.refId) &&
                Objects.equals(datetime, that.datetime) &&
                Objects.equals(requestUrl, that.requestUrl) &&
                Objects.equals(requestDirectionIn, that.requestDirectionIn) &&
                Objects.equals(requestDirectionOut, that.requestDirectionOut) &&
                Objects.equals(communicationMedium, that.communicationMedium);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, country, sender, topupReceiver, operator, connection, billPayCustomerName, billPayReceiver, billNo, accountNo, amount, status, message, refId, datetime, requestUrl, requestDirectionIn, requestDirectionOut, communicationMedium);
    }
}
