package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "UM_USER_DEVICE_MAPPING", schema = "PocketWallet")
public class UmUserDeviceMapping {
    private int id;
    private String fcmKey;
    private Date createdDate;
    private UmUserInfo umUserInfoByUserId;
    private UmUserDevice umUserDeviceByDeviceId;
    private String status;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fcm_key")
    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmUserDeviceMapping that = (UmUserDeviceMapping) o;
        return id == that.id &&
                Objects.equals(fcmKey, that.fcmKey) &&
                Objects.equals(status, that.status) &&
                Objects.equals(createdDate, that.createdDate);
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "id", nullable = false)
    public UmUserDevice getUmUserDeviceByDeviceId() {
        return umUserDeviceByDeviceId;
    }

    public void setUmUserDeviceByDeviceId(UmUserDevice umUserDeviceByDeviceId) {
        this.umUserDeviceByDeviceId = umUserDeviceByDeviceId;
    }
}
