package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "UM_BENEFICIARY")
public class UmBeneficiary {
    private Integer id;
    private String userMobileNo;
    private Integer type;
    private String customerFullName;
    private String mobileNumber;
    private String accountNumber;
    private Boolean status;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_mobile_no", nullable = false, length = 20)
    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Basic
    @Column(name = "customer_full_name", nullable = true, length = 100)
    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    @Basic
    @Column(name = "mobile_number", nullable = false, length = 20)
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Basic
    @Column(name = "account_number", nullable = false, length = 45)
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmBeneficiary that = (UmBeneficiary) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(userMobileNo, that.userMobileNo) &&
                Objects.equals(type, that.type) &&
                Objects.equals(customerFullName, that.customerFullName) &&
                Objects.equals(mobileNumber, that.mobileNumber) &&
                Objects.equals(accountNumber, that.accountNumber) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userMobileNo, type, customerFullName, mobileNumber, accountNumber, status);
    }
}
