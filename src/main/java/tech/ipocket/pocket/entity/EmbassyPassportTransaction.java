package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "UM_EMB_PASSPORT_TRANSACTION_INFO", schema = "PocketWallet")
public class EmbassyPassportTransaction {
    private int id;
    private String name;
    private String fatherName;
    private String bdMobileNo;
    private String uaeMobileNo;
    private String uniquePassportGeneratedId;
    private String mrpPassportNumber;
    private String previousPassportNumber;
    private String passportType;
    private String passportSubType;
    private BigDecimal embassyPassportFee;
    private BigDecimal pocketServiceCharge;
    private BigDecimal totalPassportFeeAmount;
    private String professionOrSkill;
    private String status;
    private Date createDate;
    private String emirateResidentialId;
    private String payMode;

    private String gender;
    private Date dateOfBirth;
    private Date currentPassportExpiryDate;

    private String tsTransactionId;
    private String tsTransactionToken;
    private String tsTransactionSender;
    private String tsTransactionReceiver;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "father_name")
    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    @Basic
    @Column(name = "bd_mobile_no")
    public String getBdMobileNo() {
        return bdMobileNo;
    }

    public void setBdMobileNo(String bdMobileNo) {
        this.bdMobileNo = bdMobileNo;
    }

    @Basic
    @Column(name = "uae_mobile_no")
    public String getUaeMobileNo() {
        return uaeMobileNo;
    }

    public void setUaeMobileNo(String uaeMobileNo) {
        this.uaeMobileNo = uaeMobileNo;
    }

    @Basic
    @Column(name = "unique_passport_generated_id")
    public String getUniquePassportGeneratedId() {
        return uniquePassportGeneratedId;
    }

    public void setUniquePassportGeneratedId(String uniquePassportGeneratedId) {
        this.uniquePassportGeneratedId = uniquePassportGeneratedId;
    }

    @Basic
    @Column(name = "mrp_passport_number")
    public String getMrpPassportNumber() {
        return mrpPassportNumber;
    }

    public void setMrpPassportNumber(String mrpPassportNumber) {
        this.mrpPassportNumber = mrpPassportNumber;
    }

    @Basic
    @Column(name = "previous_passport_number")
    public String getPreviousPassportNumber() {
        return previousPassportNumber;
    }

    public void setPreviousPassportNumber(String previousPassportNumber) {
        this.previousPassportNumber = previousPassportNumber;
    }

    @Basic
    @Column(name = "passport_type")
    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    @Basic
    @Column(name = "passport_sub_type")
    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    @Basic
    @Column(name = "embassy_passport_fee")
    public BigDecimal getEmbassyPassportFee() {
        return embassyPassportFee;
    }

    public void setEmbassyPassportFee(BigDecimal embassyPassportFee) {
        this.embassyPassportFee = embassyPassportFee;
    }

    @Basic
    @Column(name = "pocket_service_charge")
    public BigDecimal getPocketServiceCharge() {
        return pocketServiceCharge;
    }

    public void setPocketServiceCharge(BigDecimal pocketServiceCharge) {
        this.pocketServiceCharge = pocketServiceCharge;
    }

    @Basic
    @Column(name = "total_passport_fee_amount")
    public BigDecimal getTotalPassportFeeAmount() {
        return totalPassportFeeAmount;
    }

    public void setTotalPassportFeeAmount(BigDecimal totalPassportFeeAmount) {
        this.totalPassportFeeAmount = totalPassportFeeAmount;
    }

    @Basic
    @Column(name = "profession_or_skill")
    public String getProfessionOrSkill() {
        return professionOrSkill;
    }

    public void setProfessionOrSkill(String professionOrSkill) {
        this.professionOrSkill = professionOrSkill;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "create_date")
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }


    @Basic
    @Column(name = "emirate_or_residential_id")
    public String getEmirateResidentialId() {
        return emirateResidentialId;
    }

    public void setEmirateResidentialId(String emirateResidentialId) {
        this.emirateResidentialId = emirateResidentialId;
    }

    @Basic
    @Column(name = "pay_mode")
    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    @Basic
    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    @Basic
    @Column(name = "date_of_birth")
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    @Basic
    @Column(name = "current_passport_expiry_date")
    public Date getCurrentPassportExpiryDate() {
        return currentPassportExpiryDate;
    }

    public void setCurrentPassportExpiryDate(Date currentPassportExpiryDate) {
        this.currentPassportExpiryDate = currentPassportExpiryDate;
    }

    @Basic
    @Column(name = "ts_transaction_id")
    public String getTsTransactionId() {
        return tsTransactionId;
    }

    public void setTsTransactionId(String tsTransactionId) {
        this.tsTransactionId = tsTransactionId;
    }

    @Basic
    @Column(name = "ts_transaction_token")
    public String getTsTransactionToken() {
        return tsTransactionToken;
    }

    public void setTsTransactionToken(String tsTransactionToken) {
        this.tsTransactionToken = tsTransactionToken;
    }

    @Basic
    @Column(name = "ts_transaction_sender")
    public String getTsTransactionSender() {
        return tsTransactionSender;
    }

    public void setTsTransactionSender(String tsTransactionSender) {
        this.tsTransactionSender = tsTransactionSender;
    }

    @Basic
    @Column(name = "ts_transaction_receiver")
    public String getTsTransactionReceiver() {
        return tsTransactionReceiver;
    }

    public void setTsTransactionReceiver(String tsTransactionReceiver) {
        this.tsTransactionReceiver = tsTransactionReceiver;
    }
}
