package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "TS_GL", schema = "PocketWallet", catalog = "")
public class TsGl {
    private int id;
    private String glCode;
    private String glName;
    private boolean isSystemDefault;
    private boolean isParentGl;
    private String parentGlCode;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "gl_code")
    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    @Basic
    @Column(name = "gl_name")
    public String getGlName() {
        return glName;
    }

    public void setGlName(String glName) {
        this.glName = glName;
    }

    @Basic
    @Column(name = "is_system_default")
    public boolean isSystemDefault() {
        return isSystemDefault;
    }

    public void setSystemDefault(boolean systemDefault) {
        isSystemDefault = systemDefault;
    }

    @Basic
    @Column(name = "is_parent_gl")
    public boolean isParentGl() {
        return isParentGl;
    }

    public void setParentGl(boolean parentGl) {
        isParentGl = parentGl;
    }

    @Basic
    @Column(name = "parent_gl_code")
    public String getParentGlCode() {
        return parentGlCode;
    }

    public void setParentGlCode(String parentGlCode) {
        this.parentGlCode = parentGlCode;
    }
}
