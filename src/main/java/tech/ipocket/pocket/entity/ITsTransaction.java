package tech.ipocket.pocket.entity;

import java.math.BigDecimal;
import java.util.Date;

public interface ITsTransaction {

    public int getId();

    public String getToken();

    public BigDecimal getTransactionAmount()
            ;

    public String getSenderWallet();

    public String getSenderGl();

    public BigDecimal getSenderDebitAmount();

    public BigDecimal getSenderRunningBalance();

    public String getReceiverWallet();

    public String getReceiverGl();

    public BigDecimal getReceiverCreditAmount();

    public BigDecimal getReceiverRunningBalance();

    public String getTransactionType();

    public String getTransactionStatus();

    public Boolean getIsDisputable();

    public String getFeeCode();

    public String getFeePayer();

    public BigDecimal getFeeAmount();

    public String getLogicalSender();

    public String getLogicalReceiver();

    public String getEodStatus();

    public Date getCreatedDate();

    public Date getUpdatedDate();
    public String getRefTransactionToken();
}
