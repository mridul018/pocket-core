package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "TS_DEFAULT_PROFILE", schema = "PocketWallet")
public class TsDefaultProfile {
    private int id;
    private String transactionProfile;
    private String feeProfile;
    private String groupCode;
    private String groupName;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "transaction_profile")
    public String getTransactionProfile() {
        return transactionProfile;
    }

    public void setTransactionProfile(String transactionProfile) {
        this.transactionProfile = transactionProfile;
    }

    @Basic
    @Column(name = "fee_profile")
    public String getFeeProfile() {
        return feeProfile;
    }

    public void setFeeProfile(String feeProfile) {
        this.feeProfile = feeProfile;
    }

    @Column(name = "group_code")
    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Column(name = "group_name")
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
