package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "TS_FEE", schema = "PocketWallet")
public class TsFee {
    private int id;
    private String feeCode;
    private String feeName;
    private BigDecimal fixedFee;
    private BigDecimal percentageFee;
    private int isBoth;
    private String feePayer;
    private String appliedFeeMethod;
    private String status;
    private Date createdDate;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fee_code")
    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    @Basic
    @Column(name = "fee_name")
    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    @Basic
    @Column(name = "fixed_fee")
    public BigDecimal getFixedFee() {
        return fixedFee;
    }

    public void setFixedFee(BigDecimal fixedFee) {
        this.fixedFee = fixedFee;
    }

    @Basic
    @Column(name = "percentage_fee")
    public BigDecimal getPercentageFee() {
        return percentageFee;
    }

    public void setPercentageFee(BigDecimal percentageFee) {
        this.percentageFee = percentageFee;
    }

    @Basic
    @Column(name = "is_both")
    public int getBoth() {
        return isBoth;
    }

    public void setBoth(int both) {
        isBoth = both;
    }

    @Basic
    @Column(name = "fee_payer")
    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    @Basic
    @Column(name = "applied_fee_method")
    public String getAppliedFeeMethod() {
        return appliedFeeMethod;
    }

    public void setAppliedFeeMethod(String appliedFeeMethod) {
        this.appliedFeeMethod = appliedFeeMethod;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


}
