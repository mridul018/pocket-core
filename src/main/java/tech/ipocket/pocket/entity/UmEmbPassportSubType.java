package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "UM_EMB_PASSPORT_SUB_TYPE", schema = "PocketWallet")
public class UmEmbPassportSubType {
    private int id;
    private String passportSubType;
    private String status;
    private String description;
    private Boolean generalDelivery;
    private Boolean emergencyDelivery;
    private BigDecimal embPassportFee;
    private BigDecimal pocketServiceCharge;
    private BigDecimal totalAmount;
    private Date createdDate;
    private UmEmbPassportType umEmbPassportTypeByPassportType;  //fk


    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "passport_sub_type")
    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "general_delivery")
    public Boolean getGeneralDelivery() {
        return generalDelivery;
    }

    public void setGeneralDelivery(Boolean generalDelivery) {
        this.generalDelivery = generalDelivery;
    }

    @Basic
    @Column(name = "emergency_delivery")
    public Boolean getEmergencyDelivery() {
        return emergencyDelivery;
    }

    public void setEmergencyDelivery(Boolean emergencyDelivery) {
        this.emergencyDelivery = emergencyDelivery;
    }

    @Basic
    @Column(name = "emb_passport_fee")
    public BigDecimal getEmbPassportFee() {
        return embPassportFee;
    }

    public void setEmbPassportFee(BigDecimal embPassportFee) {
        this.embPassportFee = embPassportFee;
    }

    @Basic
    @Column(name = "pocket_service_charge")
    public BigDecimal getPocketServiceCharge() {
        return pocketServiceCharge;
    }

    public void setPocketServiceCharge(BigDecimal pocketServiceCharge) {
        this.pocketServiceCharge = pocketServiceCharge;
    }

    @Basic
    @Column(name = "total_amount")
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @ManyToOne
    @JoinColumn(name = "passport_type", referencedColumnName = "id", nullable = false)
    public UmEmbPassportType getUmEmbPassportTypeByPassportType() {
        return umEmbPassportTypeByPassportType;
    }

    public void setUmEmbPassportTypeByPassportType(UmEmbPassportType umEmbPassportTypeByPassportType) {
        this.umEmbPassportTypeByPassportType = umEmbPassportTypeByPassportType;
    }
}
