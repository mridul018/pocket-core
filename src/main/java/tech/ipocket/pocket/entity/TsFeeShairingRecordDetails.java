package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "TS_FEE_SHAIRING_RECORD_DETAILS")
public class TsFeeShairingRecordDetails {
    private int id;
    private Date whichDay;
    private Date dateOfFeeShared;
    private String feeShairingStatus;
    private Date startTime;
    private Date endTime;
    private int totalNoOfTransaction;
    private int totalNoOfFeeShared;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "WHICH_DAY")
    public Date getWhichDay() {
        return whichDay;
    }

    public void setWhichDay(Date whichDay) {
        this.whichDay = whichDay;
    }

    @Basic
    @Column(name = "DATE_OF_FEE_SHARED")
    public Date getDateOfFeeShared() {
        return dateOfFeeShared;
    }

    public void setDateOfFeeShared(Date dateOfFeeShared) {
        this.dateOfFeeShared = dateOfFeeShared;
    }

    @Basic
    @Column(name = "FEE_SHAIRING_STATUS")
    public String getFeeShairingStatus() {
        return feeShairingStatus;
    }

    public void setFeeShairingStatus(String feeShairingStatus) {
        this.feeShairingStatus = feeShairingStatus;
    }

    @Basic
    @Column(name = "START_TIME")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    @Basic
    @Column(name = "END_Time")
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Basic
    @Column(name = "TOTAL_NO_OfTRANSACTION")
    public int getTotalNoOfTransaction() {
        return totalNoOfTransaction;
    }

    public void setTotalNoOfTransaction(int totalNoOfTransaction) {
        this.totalNoOfTransaction = totalNoOfTransaction;
    }

    @Basic
    @Column(name = "TOTAL_NO_OfFEE_SHARED")
    public int getTotalNoOfFeeShared() {
        return totalNoOfFeeShared;
    }

    public void setTotalNoOfFeeShared(int totalNoOfFeeShared) {
        this.totalNoOfFeeShared = totalNoOfFeeShared;
    }

}
