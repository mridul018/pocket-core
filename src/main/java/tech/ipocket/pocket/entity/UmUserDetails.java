package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "UM_USER_DETAILS")
public class UmUserDetails {
    private int id;
    private String primaryIdNumber; // m
    private Date primaryIdIssueDate; // m
    private Date primaryIdExpiryDate; // m
    private Date primaryIdVerificationDate;
    private String givenName;
    private String familyName;
    private String primaryIdType; //m
    private String nationality; // m
    private String presentAddress;
    private String permanentAddress;
    private UmUserInfo umUserInfoByUserId;
    private String applicantsName;
    private String tradeLicenseNo;
    private String secondContactName;
    private String secondContactMobileNo;
    private String thirdContactName;
    private String thirdContactMobileNo;
    private String postCode;
    private String countryCode;
    private String stateId;
    private Integer nidId;
    private Integer tradeLicenseId;
    private String nidNo;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "primary_id_number")
    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    @Basic
    @Column(name = "primary_id_issue_date")
    public Date getPrimaryIdIssueDate() {
        return primaryIdIssueDate;
    }

    public void setPrimaryIdIssueDate(Date primaryIdIssueDate) {
        this.primaryIdIssueDate = primaryIdIssueDate;
    }

    @Basic
    @Column(name = "primary_id_expiry_date")
    public Date getPrimaryIdExpiryDate() {
        return primaryIdExpiryDate;
    }

    public void setPrimaryIdExpiryDate(Date primaryIdExpiryDate) {
        this.primaryIdExpiryDate = primaryIdExpiryDate;
    }

    @Basic
    @Column(name = "primary_id_verification_date")
    public Date getPrimaryIdVerificationDate() {
        return primaryIdVerificationDate;
    }

    public void setPrimaryIdVerificationDate(Date primaryIdVerificationDate) {
        this.primaryIdVerificationDate = primaryIdVerificationDate;
    }

    @Basic
    @Column(name = "given_name")
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @Basic
    @Column(name = "family_name")
    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Basic
    @Column(name = "primary_id_type")
    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    @Basic
    @Column(name = "nationality")
    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Basic
    @Column(name = "present_address")
    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    @Basic
    @Column(name = "permanent_address")
    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }

    @Basic
    @Column(name = "applicants_name")
    public String getApplicantsName() {
        return applicantsName;
    }

    public void setApplicantsName(String applicantsName) {
        this.applicantsName = applicantsName;
    }

    @Basic
    @Column(name = "trade_license_no")
    public String getTradeLicenseNo() {
        return tradeLicenseNo;
    }

    public void setTradeLicenseNo(String tradeLicenseNo) {
        this.tradeLicenseNo = tradeLicenseNo;
    }

    @Basic
    @Column(name = "second_contact_name")
    public String getSecondContactName() {
        return secondContactName;
    }

    public void setSecondContactName(String secondContactName) {
        this.secondContactName = secondContactName;
    }

    @Basic
    @Column(name = "second_contact_mobile_no")
    public String getSecondContactMobileNo() {
        return secondContactMobileNo;
    }

    public void setSecondContactMobileNo(String secondContactMobileNo) {
        this.secondContactMobileNo = secondContactMobileNo;
    }

    @Basic
    @Column(name = "third_contact_name")
    public String getThirdContactName() {
        return thirdContactName;
    }

    public void setThirdContactName(String thirdContactName) {
        this.thirdContactName = thirdContactName;
    }

    @Basic
    @Column(name = "third_contact_mobile_no")
    public String getThirdContactMobileNo() {
        return thirdContactMobileNo;
    }

    public void setThirdContactMobileNo(String thirdContactMobileNo) {
        this.thirdContactMobileNo = thirdContactMobileNo;
    }

    //post_code
    @Basic
    @Column(name = "post_code")
    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Basic
    @Column(name = "country_code")
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    @Basic
    @Column(name = "state_id")
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    @Basic
    @Column(name = "nid_id")
    public Integer getNidId() {
        return nidId;
    }

    public void setNidId(Integer nidId) {
        this.nidId = nidId;
    }


    @Basic
    @Column(name = "trade_license_id")
    public Integer getTradeLicenseId() {
        return tradeLicenseId;
    }

    public void setTradeLicenseId(Integer tradeLicenseId) {
        this.tradeLicenseId = tradeLicenseId;
    }

    //nid_no
    @Basic
    @Column(name = "nid_no")
    public String getNidNo() {
        return nidNo;
    }

    public void setNidNo(String nidNo) {
        this.nidNo = nidNo;
    }
}
