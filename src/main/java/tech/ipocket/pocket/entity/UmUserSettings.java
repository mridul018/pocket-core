package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "UM_USER_SETTINGS", schema = "PocketWallet")
public class UmUserSettings {
    private int id;
    private Boolean isMobileNumberVerified;
    private Boolean isPrimaryIdVerified;
    private String shouldChangeCredential;
    private UmUserInfo umUserInfoByUserId;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "is_mobile_number_verified")
    public Boolean getMobileNumberVerified() {
        return isMobileNumberVerified;
    }

    public void setMobileNumberVerified(Boolean mobileNumberVerified) {
        isMobileNumberVerified = mobileNumberVerified;
    }

    @Basic
    @Column(name = "is_primary_id_verified")
    public Boolean getPrimaryIdVerified() {
        return isPrimaryIdVerified;
    }

    public void setPrimaryIdVerified(Boolean primaryIdVerified) {
        isPrimaryIdVerified = primaryIdVerified;
    }

    @Basic
    @Column(name = "should_change_credential")
    public String getShouldChangeCredential() {
        return shouldChangeCredential;
    }

    public void setShouldChangeCredential(String shouldChangeCredential) {
        this.shouldChangeCredential = shouldChangeCredential;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }
}
