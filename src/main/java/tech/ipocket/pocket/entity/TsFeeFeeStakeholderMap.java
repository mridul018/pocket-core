package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "TS_FEE_FEE_STAKEHOLDER_MAP", schema = "PocketWallet")
public class TsFeeFeeStakeholderMap {
    private int id;
    private String feeProductCode;
    private int stakeholderId;
    private double stakeholderPercentage;
    private Date createdDate;
    private String stakeholderPaymentMethod;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fee_product_code")
    public String getFeeProductCode() {
        return feeProductCode;
    }

    public void setFeeProductCode(String feeProductCode) {
        this.feeProductCode = feeProductCode;
    }

    @Basic
    @Column(name = "stakeholder_id")
    public int getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(int stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    @Basic
    @Column(name = "stakeholder_percentage")
    public double getStakeholderPercentage() {
        return stakeholderPercentage;
    }

    public void setStakeholderPercentage(double stakeholderPercentage) {
        this.stakeholderPercentage = stakeholderPercentage;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    @Basic
    @Column(name = "stakeholder_payment_method")
    public String getStakeholderPaymentMethod() {
        return stakeholderPaymentMethod;
    }

    public void setStakeholderPaymentMethod(String stakeholderPaymentMethod) {
        this.stakeholderPaymentMethod = stakeholderPaymentMethod;
    }
}
