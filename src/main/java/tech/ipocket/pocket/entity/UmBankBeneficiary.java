package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "UM_BANK_BENEFICIARY")
public class UmBankBeneficiary {
    private Integer id;
    private String mobileNo;
    private String bName;
    private String bAccountNumber;
    private String bType;
    private String bAddressLine1;
    private String bAddressLine2;
    private String bAddressLine3;
    private String bCountry;
    private String bShortCode;
    private String bBranchAddress;
    private String bBranchName;
    private String bPhoneNumber;
    private String bIban;
    private String bSwiftCode;
    private Timestamp createAt;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "mobile_no", nullable = false, length = 15)
    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Basic
    @Column(name = "b_name", nullable = false, length = 200)
    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    @Basic
    @Column(name = "b_account_number", nullable = false, length = 20)
    public String getbAccountNumber() {
        return bAccountNumber;
    }

    public void setbAccountNumber(String bAccountNumber) {
        this.bAccountNumber = bAccountNumber;
    }

    @Basic
    @Column(name = "b_type", nullable = false, length = 5)
    public String getbType() {
        return bType;
    }

    public void setbType(String bType) {
        this.bType = bType;
    }

    @Basic
    @Column(name = "b_address_line1", nullable = false, length = 50)
    public String getbAddressLine1() {
        return bAddressLine1;
    }

    public void setbAddressLine1(String bAddressLine1) {
        this.bAddressLine1 = bAddressLine1;
    }

    @Basic
    @Column(name = "b_address_line2", nullable = false, length = 50)
    public String getbAddressLine2() {
        return bAddressLine2;
    }

    public void setbAddressLine2(String bAddressLine2) {
        this.bAddressLine2 = bAddressLine2;
    }

    @Basic
    @Column(name = "b_address_line3", nullable = false, length = 30)
    public String getbAddressLine3() {
        return bAddressLine3;
    }

    public void setbAddressLine3(String bAddressLine3) {
        this.bAddressLine3 = bAddressLine3;
    }

    @Basic
    @Column(name = "b_country", nullable = false, length = 50)
    public String getbCountry() {
        return bCountry;
    }

    public void setbCountry(String bCountry) {
        this.bCountry = bCountry;
    }

    @Basic
    @Column(name = "b_short_code", nullable = false, length = 10)
    public String getbShortCode() {
        return bShortCode;
    }

    public void setbShortCode(String bShortCode) {
        this.bShortCode = bShortCode;
    }

    @Basic
    @Column(name = "b_branch_address", nullable = false, length = -1)
    public String getbBranchAddress() {
        return bBranchAddress;
    }

    public void setbBranchAddress(String bBranchAddress) {
        this.bBranchAddress = bBranchAddress;
    }

    @Basic
    @Column(name = "b_branch_name", nullable = false, length = 50)
    public String getbBranchName() {
        return bBranchName;
    }

    public void setbBranchName(String bBranchName) {
        this.bBranchName = bBranchName;
    }

    @Basic
    @Column(name = "b_phone_number", nullable = false, length = 15)
    public String getbPhoneNumber() {
        return bPhoneNumber;
    }

    public void setbPhoneNumber(String bPhoneNumber) {
        this.bPhoneNumber = bPhoneNumber;
    }

    @Basic
    @Column(name = "b_iban", nullable = false, length = -1)
    public String getbIban() {
        return bIban;
    }

    public void setbIban(String bIban) {
        this.bIban = bIban;
    }

    @Basic
    @Column(name = "b_swift_code", nullable = false, length = 10)
    public String getbSwiftCode() {
        return bSwiftCode;
    }

    public void setbSwiftCode(String bSwiftCode) {
        this.bSwiftCode = bSwiftCode;
    }

    @Basic
    @Column(name = "createAt", nullable = true)
    public Timestamp getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Timestamp createAt) {
        this.createAt = createAt;
    }
}
