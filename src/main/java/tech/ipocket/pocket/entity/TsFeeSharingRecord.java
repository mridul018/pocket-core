package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "TS_FEE_SHARING_RECORD", schema = "PocketWallet")
public class TsFeeSharingRecord {
    private int id;
    private String mainTxToken;
    private String feeTxToken;
    private String txStatus;
    private String mainTxType;
    private BigDecimal stakeholderFeeAmount;
    private BigDecimal totalFeeAmount;
    private BigDecimal totalTxAmount;
    private String senderAccount;
    private String receiverAccount;
    private String requestId;
    private Timestamp shareDate;
    private String feeShareMethod;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "main_tx_token")
    public String getMainTxToken() {
        return mainTxToken;
    }

    public void setMainTxToken(String mainTxToken) {
        this.mainTxToken = mainTxToken;
    }

    @Basic
    @Column(name = "fee_tx_token")
    public String getFeeTxToken() {
        return feeTxToken;
    }

    public void setFeeTxToken(String feeTxToken) {
        this.feeTxToken = feeTxToken;
    }

    @Basic
    @Column(name = "tx_status")
    public String getTxStatus() {
        return txStatus;
    }

    public void setTxStatus(String txStatus) {
        this.txStatus = txStatus;
    }

    @Basic
    @Column(name = "main_tx_type")
    public String getMainTxType() {
        return mainTxType;
    }

    public void setMainTxType(String mainTxType) {
        this.mainTxType = mainTxType;
    }

    @Basic
    @Column(name = "stakeholder_fee_amount")
    public BigDecimal getStakeholderFeeAmount() {
        return stakeholderFeeAmount;
    }

    public void setStakeholderFeeAmount(BigDecimal stakeholderFeeAmount) {
        this.stakeholderFeeAmount = stakeholderFeeAmount;
    }

    @Basic
    @Column(name = "total_fee_amount")
    public BigDecimal getTotalFeeAmount() {
        return totalFeeAmount;
    }

    public void setTotalFeeAmount(BigDecimal totalFeeAmount) {
        this.totalFeeAmount = totalFeeAmount;
    }

    @Basic
    @Column(name = "total_tx_amount")
    public BigDecimal getTotalTxAmount() {
        return totalTxAmount;
    }

    public void setTotalTxAmount(BigDecimal totalTxAmount) {
        this.totalTxAmount = totalTxAmount;
    }

    @Basic
    @Column(name = "sender_account")
    public String getSenderAccount() {
        return senderAccount;
    }

    public void setSenderAccount(String senderAccount) {
        this.senderAccount = senderAccount;
    }

    @Basic
    @Column(name = "receiver_account")
    public String getReceiverAccount() {
        return receiverAccount;
    }

    public void setReceiverAccount(String receiverAccount) {
        this.receiverAccount = receiverAccount;
    }

    @Basic
    @Column(name = "request_id")
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "share_date")
    public Timestamp getShareDate() {
        return shareDate;
    }

    public void setShareDate(Timestamp shareDate) {
        this.shareDate = shareDate;
    }

    @Basic
    @Column(name = "FEE_SHARE_METHOD")
    public String getFeeShareMethod() {
        return feeShareMethod;
    }

    public void setFeeShareMethod(String feeShareMethod) {
        this.feeShareMethod = feeShareMethod;
    }
}
