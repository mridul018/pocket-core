package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "UM_ACTIVITY_LOG")
public class UmActivityLog {
    private int id;
    private String mobileNumber;
    private String logType;
    private String status;
    private String deviceInfo;
    private Date createdDate;
    private Date updatedDate;
    private String metaData;
    private UmUserInfo umUserInfoByUserId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "mobile_number")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Basic
    @Column(name = "log_type")
    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "device_info")
    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Basic
    @Column(name = "meta_data")
    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmActivityLog that = (UmActivityLog) o;
        return id == that.id &&
                Objects.equals(mobileNumber, that.mobileNumber) &&
                Objects.equals(logType, that.logType) &&
                Objects.equals(status, that.status) &&
                Objects.equals(deviceInfo, that.deviceInfo) &&
                Objects.equals(createdDate, that.createdDate) &&
                Objects.equals(updatedDate, that.updatedDate) &&
                Objects.equals(metaData, that.metaData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, mobileNumber, logType, status, deviceInfo, createdDate, updatedDate, metaData);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }
}
