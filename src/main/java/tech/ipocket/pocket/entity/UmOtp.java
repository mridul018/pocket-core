package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "UM_OTP", schema = "PocketWallet")
public class UmOtp {
    private int id;
    private String loginId;
    private String otpText;
    private String otpSecret;
    private String status;
    private Date createdDate;
    private UmUserInfo umUserInfoByUserId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "login_id")
    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "otp_text")
    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    @Basic
    @Column(name = "otp_secret")
    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmOtp umOtp = (UmOtp) o;
        return id == umOtp.id &&
                Objects.equals(loginId, umOtp.loginId) &&
                Objects.equals(otpText, umOtp.otpText) &&
                Objects.equals(otpSecret, umOtp.otpSecret) &&
                Objects.equals(status, umOtp.status) &&
                Objects.equals(createdDate, umOtp.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, loginId, otpText, otpSecret, status, createdDate);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }
}
