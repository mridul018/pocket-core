package tech.ipocket.pocket.entity;

import javax.persistence.*;

@Entity
@Table(name = "TS_PAYWELL_OPERATOR_MAP", schema = "PocketWallet")
public class TsPaywellOperatorMap {
    private int id;
    private String operatorCode;
    private String paywellCode;
    private String operatorName;
    private String status;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "operator_code")
    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    @Basic
    @Column(name = "paywell_code")
    public String getPaywellCode() {
        return paywellCode;
    }

    public void setPaywellCode(String paywellCode) {
        this.paywellCode = paywellCode;
    }

    @Basic
    @Column(name = "operator_name")
    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
