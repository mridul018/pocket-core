package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "UM_QR_CODES", schema = "PocketWallet", catalog = "")
public class UmQrCodes {
    private int id;
    private BigDecimal amount;
    private String qrCode;
    private String status;
    private Timestamp createdDate;
    private UmUserInfo umUserInfoByUserId;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "qr_code")
    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "created_date")
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UmQrCodes umQrCodes = (UmQrCodes) o;
        return id == umQrCodes.id &&
                Objects.equals(amount, umQrCodes.amount) &&
                Objects.equals(qrCode, umQrCodes.qrCode) &&
                Objects.equals(status, umQrCodes.status) &&
                Objects.equals(createdDate, umQrCodes.createdDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amount, qrCode, status, createdDate);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public UmUserInfo getUmUserInfoByUserId() {
        return umUserInfoByUserId;
    }

    public void setUmUserInfoByUserId(UmUserInfo umUserInfoByUserId) {
        this.umUserInfoByUserId = umUserInfoByUserId;
    }
}
