package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "TS_Client", schema = "PocketWallet", catalog = "")
public class TsClient {
    private int id;
    private String fullName;
    private String walletNo;
    private String groupCode;
    private String userStatus;
    private String feeProfileCode;
    private String transactionProfileCode;
    private Date createdDate;
    private Date verificationDate;
    private Collection<TsClientBalance> tsClientBalancesById;
    private Collection<TsMerchantDetails> tsMerchantDetailsById;
    private Collection<TsTransaction> tsTransactionsById;
    private Collection<TsTransaction> tsTransactionsById_0;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "full_name")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Basic
    @Column(name = "wallet_no")
    public String getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(String walletNo) {
        this.walletNo = walletNo;
    }

    @Basic
    @Column(name = "group_code")
    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    @Basic
    @Column(name = "user_status")
    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    @Basic
    @Column(name = "fee_profile_code")
    public String getFeeProfileCode() {
        return feeProfileCode;
    }

    public void setFeeProfileCode(String feeProfileCode) {
        this.feeProfileCode = feeProfileCode;
    }

    @Basic
    @Column(name = "transaction_profile_code")
    public String getTransactionProfileCode() {
        return transactionProfileCode;
    }

    public void setTransactionProfileCode(String transactionProfileCode) {
        this.transactionProfileCode = transactionProfileCode;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "verification_date")
    public Date getVerificationDate() {
        return verificationDate;
    }

    public void setVerificationDate(Date verificationDate) {
        this.verificationDate = verificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TsClient tsClient = (TsClient) o;
        return id == tsClient.id &&
                Objects.equals(fullName, tsClient.fullName) &&
                Objects.equals(walletNo, tsClient.walletNo) &&
                Objects.equals(groupCode, tsClient.groupCode) &&
                Objects.equals(userStatus, tsClient.userStatus) &&
                Objects.equals(feeProfileCode, tsClient.feeProfileCode) &&
                Objects.equals(transactionProfileCode, tsClient.transactionProfileCode) &&
                Objects.equals(createdDate, tsClient.createdDate) &&
                Objects.equals(verificationDate, tsClient.verificationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, walletNo, groupCode, userStatus, feeProfileCode, transactionProfileCode, createdDate, verificationDate);
    }

    @OneToMany(mappedBy = "tsClientByClientId")
    public Collection<TsClientBalance> getTsClientBalancesById() {
        return tsClientBalancesById;
    }

    public void setTsClientBalancesById(Collection<TsClientBalance> tsClientBalancesById) {
        this.tsClientBalancesById = tsClientBalancesById;
    }

    @OneToMany(mappedBy = "tsClientByClientId")
    public Collection<TsMerchantDetails> getTsMerchantDetailsById() {
        return tsMerchantDetailsById;
    }

    public void setTsMerchantDetailsById(Collection<TsMerchantDetails> tsMerchantDetailsById) {
        this.tsMerchantDetailsById = tsMerchantDetailsById;
    }

    @OneToMany(mappedBy = "tsClientBySenderClientId")
    public Collection<TsTransaction> getTsTransactionsById() {
        return tsTransactionsById;
    }

    public void setTsTransactionsById(Collection<TsTransaction> tsTransactionsById) {
        this.tsTransactionsById = tsTransactionsById;
    }

    @OneToMany(mappedBy = "tsClientByReceiverClientId")
    public Collection<TsTransaction> getTsTransactionsById_0() {
        return tsTransactionsById_0;
    }

    public void setTsTransactionsById_0(Collection<TsTransaction> tsTransactionsById_0) {
        this.tsTransactionsById_0 = tsTransactionsById_0;
    }
}
