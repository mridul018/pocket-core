package tech.ipocket.pocket.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "UM_EMB_PASSPORT_TYPE", schema = "PocketWallet")
public class UmEmbPassportType {
    private int id;
    private String passportType;
    private String description;
    private String status;
    private Date createdDate;
    private Collection<UmEmbPassportSubType>  umEmbPassportTypeByPassportType;;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "passport_type")
    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @OneToMany(mappedBy = "umEmbPassportTypeByPassportType")
    public Collection<UmEmbPassportSubType> getUmEmbPassportTypeByPassportType() {
        return umEmbPassportTypeByPassportType;
    }

    public void setUmEmbPassportTypeByPassportType(Collection<UmEmbPassportSubType> umEmbPassportTypeByPassportType) {
        this.umEmbPassportTypeByPassportType = umEmbPassportTypeByPassportType;
    }
}
