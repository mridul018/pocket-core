package tech.ipocket.pocket.repository.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.entity.UmUserInfo;

@Repository
public interface UserRepository extends JpaRepository<UmUserInfo, Integer> {
    UmUserInfo findByMobileNumber(String number);

    UmUserGroup findByGroupCode(String groupCode);
}
