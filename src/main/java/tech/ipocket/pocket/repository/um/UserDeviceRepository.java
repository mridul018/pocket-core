package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserDevice;

@Repository
public interface UserDeviceRepository extends JpaRepository<UmUserDevice, Integer> {
    UmUserDevice findFirstByHardwareSignature(String deviceNumber);
}
