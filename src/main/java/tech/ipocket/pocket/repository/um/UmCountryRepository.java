package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmCountry;

import java.util.List;

public interface UmCountryRepository extends CrudRepository<UmCountry,Integer> {
    List<UmCountry> findAllByStatus(String status);
    UmCountry findAllByCountryCodeAndStatus(String countryCode,String status);
}
