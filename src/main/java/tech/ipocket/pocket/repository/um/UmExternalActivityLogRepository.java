package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.ipocket.pocket.entity.UmExternalActivityLog;

import java.util.List;

public interface UmExternalActivityLogRepository extends JpaRepository<UmExternalActivityLog,Integer> {
    List<UmExternalActivityLog> findByTypeAndStatus(String type, int status);
}
