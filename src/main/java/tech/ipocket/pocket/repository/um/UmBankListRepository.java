package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmBankList;

import java.util.ArrayList;

@Repository
public interface UmBankListRepository extends JpaRepository<UmBankList,Integer> {
    ArrayList<UmBankList> findByUserMobileNumber(String number);

    UmBankList findByUserMobileNumberAndBankId(String number, String bankID);
}
