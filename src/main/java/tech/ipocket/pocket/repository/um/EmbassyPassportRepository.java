package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmEmbPassportType;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Repository
public interface EmbassyPassportRepository extends CrudRepository<UmEmbPassportType, Integer> {

    UmEmbPassportType findTopByOrderByIdDesc();
    UmEmbPassportType findTopByPassportTypeIgnoreCaseAndStatusOrderByIdDesc(String passportType, String status);
    Optional<UmEmbPassportType> findTopByIdOrderByIdDesc(Integer id);


    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update UM_EMB_PASSPORT_TYPE u set u.passportType = ?1,u.description = ?2,u.status = ?3,u.createdDate = ?4 where u.id = ?5",
            nativeQuery = true)
    void updateUmEmbPassportType(String passportType, String description, String status, Date createdDate, int id);

}
