package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmState;

import java.util.List;

public interface UmStateRepository extends CrudRepository<UmState,Integer> {
    List<UmState> findAllByStatus(String status);
    UmState findFirstById(Integer id);
}
