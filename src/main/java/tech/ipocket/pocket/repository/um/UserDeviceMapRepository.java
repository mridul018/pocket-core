package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserDevice;
import tech.ipocket.pocket.entity.UmUserDeviceMapping;
import tech.ipocket.pocket.entity.UmUserInfo;

import java.util.List;

@Repository
public interface UserDeviceMapRepository extends JpaRepository<UmUserDeviceMapping, Integer> {
    UmUserDeviceMapping findFirstByUmUserInfoByUserIdAndUmUserDeviceByDeviceIdAndStatus(UmUserInfo umUserInfo, UmUserDevice umUserDevice, String status);

    List<UmUserDeviceMapping> findByUmUserInfoByUserId(UmUserInfo userInfo);

    List<UmUserDeviceMapping> findAllByUmUserInfoByUserIdAndStatusIn(UmUserInfo umUserInfo, List<String> statusIn);

}
