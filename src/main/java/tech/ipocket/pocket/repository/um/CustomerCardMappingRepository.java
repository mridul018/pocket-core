package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmCustomerCardMapping;

@Repository
public interface CustomerCardMappingRepository extends CrudRepository<UmCustomerCardMapping,Integer> {

    UmCustomerCardMapping findFirstByCardNumber(String cardNumber);
    UmCustomerCardMapping findFirstByCardUid(String uid);
}
