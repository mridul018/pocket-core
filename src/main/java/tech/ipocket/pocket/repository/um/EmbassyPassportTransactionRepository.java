package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.EmbassyPassportTransaction;

import java.util.Date;
import java.util.List;


@Repository
public interface EmbassyPassportTransactionRepository extends CrudRepository<EmbassyPassportTransaction, Integer>{
    EmbassyPassportTransaction findTopByOrderByIdDesc();
    EmbassyPassportTransaction findByUaeMobileNoAndUniquePassportGeneratedIdAndTsTransactionIdAndTsTransactionToken(String uaeMobileNo, String uniquePassportGeneratedId, String tsTransactionId, String tsTransactionToken);

    List<EmbassyPassportTransaction> findByCreateDateAndCreateDate(Date fromDate, Date toDate);
    EmbassyPassportTransaction findByTsTransactionTokenIgnoreCaseAndStatus(String tsTransactionToken, String status);
    EmbassyPassportTransaction findByUniquePassportGeneratedIdIgnoreCaseAndStatus(String uniquePassportGeneratedId, String status);
    List<EmbassyPassportTransaction> findByPassportTypeIgnoreCaseAndStatus(String passportType, String status);
    List<EmbassyPassportTransaction> findAllByPassportSubTypeIgnoreCaseAndStatus(String passportSubType, String status);
    EmbassyPassportTransaction findByMrpPassportNumberIgnoreCaseAndStatus(String mrpPassportNumber, String status);
    List<EmbassyPassportTransaction> findByPayModeIgnoreCaseAndStatus(String payMode, String status);
    List<EmbassyPassportTransaction> findByTsTransactionSenderAndStatus(String tsTransactionSender, String status);

    EmbassyPassportTransaction findByEmirateResidentialIdIgnoreCaseAndStatus(String emirateResidentialId, String status);

    EmbassyPassportTransaction findByTsTransactionIdAndTsTransactionTokenAndStatus(String tsTransactionId, String tsTransactionToken, String status);

}

