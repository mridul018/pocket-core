package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmUserBankMapping;

public interface UmUserBankMappingRepository extends CrudRepository<UmUserBankMapping,Integer> {

    UmUserBankMapping findLastByUserIdOrderByIdDesc(int userInfoId);
}
