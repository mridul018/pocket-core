package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserDetails;
import tech.ipocket.pocket.entity.UmUserInfo;

@Repository
public interface UserDetailsRepository extends CrudRepository<UmUserDetails, Integer> {
    UmUserDetails findFirstByUmUserInfoByUserIdOrderByIdDesc(UmUserInfo umUserInfo);
    void deleteByUmUserInfoByUserId(UmUserInfo umUserInfoByUserId);
}
