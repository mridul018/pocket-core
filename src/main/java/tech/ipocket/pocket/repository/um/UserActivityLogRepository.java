package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmActivityLog;

import java.util.Date;
import java.util.List;

@Repository
public interface UserActivityLogRepository extends JpaRepository<UmActivityLog, Integer> {
    List<UmActivityLog> findAllByCreatedDateBetweenOrderByIdDesc(Date startDate, Date endDate);
    List<UmActivityLog> findAllByMobileNumberAndCreatedDateBetweenOrderByIdDesc(String mobileNumber, Date startDate, Date endDate);
}
