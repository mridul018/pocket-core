package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmAmySession;

@Repository
public interface UmAmySessionRepository extends JpaRepository<UmAmySession, Integer> {
    UmAmySession findByTokenType(String type);
}
