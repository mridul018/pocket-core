package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmCountryStateMapping;

import java.util.List;

public interface UmCountryStateMappingRepository extends CrudRepository<UmCountryStateMapping,Integer> {
    List<UmCountryStateMapping> findAllByCountryCodeAndStatus(String countryCode,String status);


    UmCountryStateMapping findFirstByCountryCodeAndStateId(String countryCode,String stateId);
}
