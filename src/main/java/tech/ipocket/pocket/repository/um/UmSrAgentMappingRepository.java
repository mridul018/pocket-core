package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import tech.ipocket.pocket.entity.UmSrAgentMapping;

import java.util.List;

public interface UmSrAgentMappingRepository extends CrudRepository<UmSrAgentMapping,Integer> {
    List<UmSrAgentMapping> findAllBySrIdAndStatus(int srId,String status);
    UmSrAgentMapping findFirstByAgentIdAndStatus(int agentId,String status);

    @Modifying
    @Query("UPDATE UmSrAgentMapping u SET u.srId = :srId WHERE u.agentId = :agentId")
    void updateUmSrAgentMappingByAgentId(@Param("agentId") int agentId, @Param("srId") int srId);
}
