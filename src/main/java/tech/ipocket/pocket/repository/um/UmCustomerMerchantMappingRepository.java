package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmCustomerMerchantMapping;

public interface UmCustomerMerchantMappingRepository extends CrudRepository<UmCustomerMerchantMapping, Integer> {

}
