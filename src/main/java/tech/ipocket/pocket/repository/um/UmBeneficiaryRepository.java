package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmBeneficiary;

@Repository
public interface UmBeneficiaryRepository extends JpaRepository<UmBeneficiary, Integer> {
    UmBeneficiary findByAccountNumberAndMobileNumberAndType(String accountNoString, String mobileNo, int type);
}
