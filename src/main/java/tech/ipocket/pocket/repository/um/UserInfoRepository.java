package tech.ipocket.pocket.repository.um;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserInfo;

import java.util.List;

@Repository
public interface UserInfoRepository extends JpaRepository<UmUserInfo, Integer> {
    UmUserInfo findByMobileNumberAndAccountStatusIn(String mobileNumber, List<Integer> statusIn);
    UmUserInfo findByLoginIdAndAccountStatusIn(String loginId, List<Integer> statusIn);

    UmUserInfo findByMobileNumberAndAccountStatusNotIn(String mobileNumber, List<Integer> statusNotIn);
    UmUserInfo findByMobileNumberOrEmail(String mobileNumber, String email);

    UmUserInfo findFirstByEmail(String email);

    List<UmUserInfo> findByLoginIdContaining(String loginId);

    List<UmUserInfo> findByFullNameContaining(String fullName);

    UmUserInfo findByLoginId(String loginId);
    UmUserInfo findByLoginIdAndGroupCode(String loginId,String groupCode);

    UmUserInfo findFirstById(Integer credentialId);

    Page<UmUserInfo> findAllByGroupCode(String groupCode, Pageable pageRequest);

    Long countAllByGroupCode(String groupCode);

    UmUserInfo findFirstByMobileNumber(String mobileNumber);

    UmUserInfo findFirstByLoginIdOrMobileNumber(String loginId, String mobileNumber);

    UmUserInfo findFirstByPassword(String password);
}
