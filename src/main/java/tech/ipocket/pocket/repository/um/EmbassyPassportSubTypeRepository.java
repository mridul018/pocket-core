package tech.ipocket.pocket.repository.um;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmEmbPassportSubType;
import tech.ipocket.pocket.entity.UmEmbPassportType;

import java.util.List;

@Repository
public interface EmbassyPassportSubTypeRepository extends CrudRepository<UmEmbPassportSubType, Integer> {
    UmEmbPassportSubType findTopByOrderByIdDesc();
    List<UmEmbPassportSubType> findByUmEmbPassportTypeByPassportType(UmEmbPassportType umEmbPassportTypeByPassportType);
    UmEmbPassportSubType findTopByPassportSubTypeIgnoreCaseAndStatusOrderByIdDesc(String passportSubType, String status);

}
