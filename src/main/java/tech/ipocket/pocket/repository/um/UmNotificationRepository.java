package tech.ipocket.pocket.repository.um;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmNotification;

import java.util.List;

public interface UmNotificationRepository extends CrudRepository<UmNotification,Integer> {

    List<UmNotification> findAllByReceiver(String receiver, Pageable pageRequest);
}
