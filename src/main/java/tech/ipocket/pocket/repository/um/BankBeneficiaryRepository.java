package tech.ipocket.pocket.repository.um;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmBankBeneficiary;

@Repository
public interface BankBeneficiaryRepository extends JpaRepository<UmBankBeneficiary, Integer> {
}
