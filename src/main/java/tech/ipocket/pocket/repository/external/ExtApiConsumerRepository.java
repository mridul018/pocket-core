package tech.ipocket.pocket.repository.external;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.ExtApiConsumer;

import java.util.Date;

@Repository
public interface ExtApiConsumerRepository extends CrudRepository<ExtApiConsumer,Integer> {

    ExtApiConsumer findFirstByClientTokenAndIpAddressAndStatus(String token, String ipAddress, int status);
    ExtApiConsumer findFirstByIpAddressAndStatus(String ipAddress, int status);
    ExtApiConsumer findFirstByIpAddressAndClientTokenIgnoreCaseAndStatus(String ipAddress, String clientToken, int status);
    ExtApiConsumer findFirstByClientTokenIgnoreCaseAndStatus(String clientToken, int status);
}
