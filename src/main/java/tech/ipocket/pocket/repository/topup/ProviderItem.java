package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProviderItem {
    @SerializedName("ProviderCode")
    public String providerCode;
//  public Object providerCode;
    @SerializedName("CountryIso")
    public Object countryIso;
    @SerializedName("Name")
    public Object name;
    @SerializedName("ShortName")
    public Object shortName;
    @SerializedName("ValidationRegex")
    public Object validationRegex;
    @SerializedName("CustomerCareNumber")
    public Object customerCareNumber;
    @SerializedName("RegionCodes")
    public List<Object> regionCodes = null;
}
