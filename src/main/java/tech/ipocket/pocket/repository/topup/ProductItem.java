package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductItem {
    @SerializedName("ProviderCode")
    public Object providerCode;
    @SerializedName("SkuCode")
    public Object skuCode;
    @SerializedName("LocalizationKey")
    public Object localizationKey;
    @SerializedName("SettingDefinitions")
    public List<Object> settingDefinitions = null;
    @SerializedName("Maximum")
    public ProductMaximum maximum;
    @SerializedName("Minimum")
    public ProductMinimum minimum;
    @SerializedName("CommissionRate")
    public Object commissionRate;
    @SerializedName("ProcessingMode")
    public Object processingMode;
    @SerializedName("RedemptionMechanism")
    public Object redemptionMechanism;
    @SerializedName("Benefits")
    public List<Object> benefits = null;
    @SerializedName("ValidityPeriodIso")
    public Object validityPeriodIso;
    @SerializedName("UatNumber")
    public Object uatNumber;
    @SerializedName("AdditionalInformation")
    public Object additionalInformation;
    @SerializedName("DefaultDisplayText")
    public Object defaultDisplayText;
    @SerializedName("RegionCode")
    public Object regionCode;


}
