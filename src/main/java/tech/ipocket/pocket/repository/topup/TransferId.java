package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

public class TransferId {
    @SerializedName("TransferRef")
    public Object transferRef;
    @SerializedName("DistributorRef")
    public Object distributorRef;
}
