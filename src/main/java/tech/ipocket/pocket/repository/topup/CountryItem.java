package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountryItem {
    @SerializedName("CountryIso")
    public Object countryIso;
    @SerializedName("CountryName")
    public Object countryName;
    @SerializedName("InternationalDialingInformation")
    public List<InternationalDialingInformation> internationalDialingInformation = null;
    @SerializedName("RegionCodes")
    public List<Object> regionCodes = null;
}
