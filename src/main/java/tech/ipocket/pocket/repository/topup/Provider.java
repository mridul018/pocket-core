package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Provider {
    @SerializedName("ResultCode")
    public int resultCode;
    @SerializedName("ErrorCodes")
    public List<ErrorCode> errorCodes = null;
    @SerializedName("Items")
    public List<ProviderItem> providerItems = null;
}
