package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

public class InternationalDialingInformation {
    @SerializedName("Prefix")
    public Object prefix;
    @SerializedName("MinimumLength")
    public Object minimumLength;
    @SerializedName("MaximumLength")
    public Object maximumLength;
}
