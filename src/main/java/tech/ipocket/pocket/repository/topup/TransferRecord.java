package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

public class TransferRecord {
    @SerializedName("TransferId")
    public TransferId transferId;
    @SerializedName("SkuCode")
    public Object skuCode;
    @SerializedName("Price")
    public Price price;
    @SerializedName("CommissionApplied")
    public Object commissionApplied;
    @SerializedName("StartedUtc")
    public Object startedUtc;
    @SerializedName("CompletedUtc")
    public Object completedUtc;
    @SerializedName("ProcessingState")
    public Object processingState;
    @SerializedName("ReceiptText")
    public Object receiptText;
   // @SerializedName("ReceiptParams")
    //public ReceiptParams receiptParams;
    @SerializedName("AccountNumber")
    public Object accountNumber;
}
