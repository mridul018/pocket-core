package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Country {
    @SerializedName("ResultCode")
    public Integer resultCode;
    @SerializedName("ErrorCodes")
    public List<ErrorCode> errorCodes = null;
    @SerializedName("Items")
    public List<CountryItem> countryItems = null;
}
