package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

public class ErrorCode {
    @SerializedName("Code")
    public String code;
    @SerializedName("Context")
    public String context;
}
