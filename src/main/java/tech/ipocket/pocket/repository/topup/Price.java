package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

public class Price {
    @SerializedName("CustomerFee")
    public Object customerFee;
    @SerializedName("DistributorFee")
    public Object distributorFee;
    @SerializedName("ReceiveValue")
    public Object receiveValue;
    @SerializedName("ReceiveCurrencyIso")
    public Object receiveCurrencyIso;
    @SerializedName("ReceiveValueExcludingTax")
    public Object receiveValueExcludingTax;
    @SerializedName("TaxRate")
    public Object taxRate;
    @SerializedName("TaxName")
    public Object taxName;
    @SerializedName("TaxCalculation")
    public Object taxCalculation;
    @SerializedName("SendValue")
    public Object sendValue;
    @SerializedName("SendCurrencyIso")
    public Object sendCurrencyIso;
}
