package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {
    @SerializedName("ResultCode")
    public int resultCode;
    @SerializedName("ErrorCodes")
    public List<ErrorCode> errorCodes = null;
    @SerializedName("Items")
    public List<ProductItem> productItems = null;
    public Double conversionRate;

    public Double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }
}
