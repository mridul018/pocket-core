package tech.ipocket.pocket.repository.topup;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SendTopup {
    @SerializedName("TransferRecord")
    public TransferRecord transferRecord;
    @SerializedName("ResultCode")
    public int resultCode;
    @SerializedName("ErrorCodes")
    public List<ErrorCode> errorCodes = null;
}
