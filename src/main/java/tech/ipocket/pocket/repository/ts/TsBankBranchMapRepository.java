package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsBankBranchMapping;

import java.util.List;

public interface TsBankBranchMapRepository extends CrudRepository<TsBankBranchMapping,Integer> {
    List<TsBankBranchMapping> findAllByBankCodeAndStatus(String bankCode,String status);
    TsBankBranchMapping findFirstByBankCodeAndBranchSwiftCodeAndStatus(String bankCode,String branchSwiftCode,String status);
    TsBankBranchMapping findFirstByBankCodeAndBranchSwiftCode(String bankCode,String branchSwiftCode);
}
