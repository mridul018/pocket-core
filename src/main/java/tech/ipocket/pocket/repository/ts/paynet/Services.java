package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Services {
    @SerializedName("services")
    public List<ServiceItem> services = null;

    @SerializedName("error")
    public ErrorItem error;
}
