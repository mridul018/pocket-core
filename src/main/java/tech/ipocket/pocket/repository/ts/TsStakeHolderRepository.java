package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeStakeholder;

public interface TsStakeHolderRepository extends CrudRepository<TsFeeStakeholder, Integer> {

    TsFeeStakeholder findFirstById(Integer stakeHolderId);
    TsFeeStakeholder findFirstByGlCode(String glCode);

    TsFeeStakeholder findFirstByStakeholderName(String stakeHolderName);
}
