package tech.ipocket.pocket.repository.ts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import tech.ipocket.pocket.entity.ITsTransaction;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsTransaction;

import java.sql.Date;
import java.util.List;

public interface TsTransactionRepository extends CrudRepository<TsTransaction, Integer> {
    List<TsTransaction> findByTransactionTypeAndTsClientBySenderClientIdAndCreatedDateBetween(String serviceCode, TsClient senderClient, Date startDate, Date endDate);

    List<TsTransaction> findByTransactionTypeAndTsClientByReceiverClientIdAndCreatedDateBetween(String serviceCode, TsClient receiverClient, Date startDate, Date endDate);


    @Query("select transaction from TsTransaction transaction" +
            " where  (transaction.receiverWallet=?1 OR transaction.senderWallet=?2 ) AND transaction.transactionStatus not in (?3) ")
//    Page<ITsTransaction> findAllByReceiverWalletOrSenderWallet(String receiverWallet, String senderWallet,
//                                                               List<String> statusNotIn, Pageable pageRequest);
    List<TsTransaction> findAllByReceiverWalletOrSenderWallet(String receiverWallet, String senderWallet,
                                                               List<String> statusNotIn, Pageable pageRequest);


    @Query("select transaction from TsTransaction transaction" +
            " where  (transaction.receiverWallet=?1 OR transaction.senderWallet=?2 ) AND transaction.createdDate between ?3 AND ?4 " +
            " AND transaction.transactionStatus not in (?5) ")
//    Page<ITsTransaction> findAllByReceiverWalletOrSenderWalletDateBetween(String receiverWallet, String senderWallet,
//                                                                          Date fromDate, Date toDate,List<String> statusNotIn, PageRequest pageRequest);
    List<TsTransaction> findAllByReceiverWalletOrSenderWalletDateBetween(String receiverWallet, String senderWallet,Date fromDate, Date toDate,
                                                                         List<String> statusNotIn,PageRequest pageRequest);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType in (?1) AND (transaction.receiverWallet=?2 OR transaction.senderWallet=?3 ) " +
            " AND transaction.transactionStatus not in (?4) ")
//    Page<ITsTransaction> findAllByTransactionTypeInAndReceiverWalletOrSenderWallet(List<String> txTypes,
//                                                                                   String receiverWallet, String senderWallet,List<String> statusNotIn, Pageable pageRequest);
    List<TsTransaction> findAllByTransactionTypeInAndReceiverWalletOrSenderWallet(List<String> txTypes,String receiverWallet, String senderWallet,
                                                                                  List<String> statusNotIn, Pageable pageRequest);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType in (?1) AND (transaction.receiverWallet=?2 OR transaction.senderWallet=?3 ) " +
            "AND transaction.createdDate between ?4 AND ?5 " +
            " AND transaction.transactionStatus not in (?6) ")
//    Page<ITsTransaction> findAllByTransactionTypeInAndReceiverWalletOrSenderWalletDateBetween(List<String> txTypes,
//                                                                                              String receiverWallet, String senderWallet,
//                                                                                              Date fromDate, Date toDate, List<String> txFailedStatus, Pageable pageRequest);
    List<TsTransaction> findAllByTransactionTypeInAndReceiverWalletOrSenderWalletDateBetween(List<String> txTypes,String receiverWallet, String senderWallet,
                                                                                              Date fromDate, Date toDate, List<String> txFailedStatus, Pageable pageRequest);

    @Query("select transaction from TsTransaction transaction " +
            "where ((transaction.logicalSender=?1 OR transaction.logicalReceiver=?2) AND " +
            " (transaction.createdDate between ?3 AND ?4) AND transaction.transactionStatus not in (?5)) order by transaction.id asc")
    List<TsTransaction> findByPayerIdOrPayeeIdAndProductCodeAndTransactionDateBetweenAndTransactionStatusNotIn(
            String payerId,
            String payeeId,
            Date startDates, Date endDate,
            List<String> statusNotIn);

    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where (transaction.sender_wallet=?1 OR receiver_wallet=?1) \n" +
            "AND created_date  <?2 \n" +
            "AND transaction.transaction_status not in(?3) \n" +
            "order by id desc limit 1", nativeQuery=true)
    TsTransaction findPreviousTransactionByStartDateAndTransactionStatusNotIn(String mobileNumber, Date startDate, List<String> statusNotIn);



    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where (transaction.sender_wallet=?1 OR receiver_wallet=?1) \n" +
            "AND created_date  <?2 \n" +
            "AND transaction.transaction_status not in(?3) \n" +
            "order by id desc limit 1", nativeQuery=true)
    TsTransaction findLastTransactionByDateAndTransactionStatusNotIn(String mobileNumber,Date endDate,List<String> statusNotIn);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType in (?1) AND transaction.createdDate between ?2 AND ?3")

    List<TsTransaction> findByTransactionByTxTypeAndTransactionDateBetween(String serviceCode, Date fromDate, Date toDate, Object o);


    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where ( transaction.receiver_client_id=?3) \n" +
            "AND (transaction.created_date between ?1 AND ?2) \n" +
            "AND transaction.transaction_status not in(?4) ", nativeQuery=true)
    List<TsTransaction> findByTransactionDateBetweenAndPayeeIdAndTransactionStatusNotIn(Date startDate, Date endDate, int clientId, List<String> txFailedStatus);


    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where ( transaction.sender_client_id=?3) \n" +
            "AND (transaction.created_date between ?1 AND ?2) \n" +
            "AND transaction.transaction_status not in(?4) ", nativeQuery=true)
    List<TsTransaction> findByTransactionDateBetweenAndPayerIdAndTransactionStatusNotIn(Date startDate,
                                                                                        Date endDate,
                                                                                        int clientId,
                                                                                        List<String> txFailedStatus);

    List<TsTransaction> findAllByToken(String txToken);


    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where  (transaction.created_date between ?1 AND ?2) \n" +
            "AND transaction.transaction_status not in(?3) ", nativeQuery=true)
    List<TsTransaction> findByTransactionDateBetweenAndTransactionStatusNotIn(Date startDate, Date endDate, List<String> txFailedStatus);

    List<TsTransaction> findByCreatedDateAfterAndCreatedDateBetweenAndTransactionStatusNotIn(Date createdDate, Date startDate, Date endDate, List<String> txFailedStatus);

    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where (transaction.sender_wallet=?1 OR receiver_wallet=?1) \n" +
            "AND  (transaction.created_date between ?2 AND ?3) \n" +
            "AND transaction.transaction_status not in(?4) ", nativeQuery=true)
    List<TsTransaction> findByMobileNumberAndTransactionDateBetweenAndTransactionStatusNotIn(String mobileNumber,
                                                                                             Date startDate, Date endDate, List<String> txFailedStatus);

    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where transaction.transaction_type in (?1) " +
            " AND (transaction.created_date between ?2 AND ?3) \n" +
            "AND transaction.transaction_status not in(?4) ", nativeQuery=true)
    List<TsTransaction> findByTransactionTypeInAndTransactionDateBetweenAndTransactionStatusNotIn(List<String> transactionType,
                                                                                                  Date startDate, Date endDate, List<String> txFailedStatus);


    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            " where (transaction.sender_wallet=?1 OR receiver_wallet=?1) " +
            " AND " +
            " transaction.transaction_type in (?2) " +
            " AND (transaction.created_date between ?3 AND ?4) \n" +
            "AND transaction.transaction_status not in(?5) ", nativeQuery=true)
    List<TsTransaction> findByPayerOrPayeeTransactionTypeInAndTransactionDateBetweenAndTransactionStatusNotIn(
            String payerMobileNo,List<String> transactionType,
            Date startDate,
            Date endDate, List<String> txFailedStatus);



    @Query(value = "select * from TS_TRANSACTION transaction \n" +
            "where transaction.transaction_type in (?1) " +
            " AND ( transaction.sender_client_id=?4 OR transaction.receiver_client_id=?5) \n "+
            " AND (transaction.created_date between ?2 AND ?3) \n" +
            "AND transaction.transaction_status not in(?4) ", nativeQuery=true)
    List<TsTransaction> findByTransactionTypeAndTransactionDateBetweenAndPayerIdOrPayeeIdAndTransactionStatusNotInAll(List<String> service,
                                                                                                                      Date startDate,
                                                                                                                      Date endDate,
                                                                                                                      int clientId,
                                                                                                                      int clientId1,
                                                                                                                      List<String> txFailedStatus);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1 AND (transaction.senderWallet=?2 ) AND transaction.createdDate between ?3 AND ?4")
    List<ITsTransaction> findAllByTransactionTypeAndSenderWalletDateBetween(String txType,
                                                                                               String senderWallet,
                                                                                              Date fromDate,Date toDate);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1 AND (transaction.senderWallet=?2 ) " +
            "AND transaction.createdDate between ?3 AND ?4" +
            " AND transaction.transactionStatus not in(?5) ")
    List<TsTransaction> findAllByTransactionTypeAndSenderWalletDateBetweenAndStatusNotIn(String txType,
                                                                                               String senderWallet,
                                                                                              Date fromDate,Date toDate,
                                                                                          List<String> txFailedStatus);


    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1 AND (transaction.senderWallet=?2 ) " +
            "AND transaction.createdDate between ?3 AND ?4" +
            " AND transaction.transactionStatus not in(?5) " +
            " AND transaction.topupSkuCode in (?6) ")
    List<TsTransaction> findAllByTransactionTypeAndSenderWalletDateBetweenAndStatusNotInAndSkuCodeIn(String txType,
                                                                                               String senderWallet,
                                                                                              Date fromDate,Date toDate,
                                                                                          List<String> txFailedStatus,
                                                                                         List<String> skuCodes);

    @Query(value = "  \n" +
            "                    select * from TS_TRANSACTION tx where tx.transaction_type in(?1) \n" +
            "                    AND tx.transaction_status not in(?2) \n" +
            "                    AND tx.topup_sku_code in (?3) \n" +
            "                    AND tx.created_date between ?4 AND ?5 \n" +
            "                     AND tx.sender_wallet in( \n" +
            "                    select ui.login_id from UM_USER_DETAILS ud  \n" +
            "                    left join UM_user_info ui on ui.id=ud.user_id \n" +
            "                    where ud.state_id in(?6) )  ",nativeQuery = true)
    List<TsTransaction> filterTopUpReports(List<String> txType,
                                                                                                                        List<String> txFailedStatus,
                                                                                                                        List<String> skuCodes,
                                                                                                                        Date fromDate,
                                                                                                                        Date toDate,
                                                                                                                        List<String> stateIds);

    @Query(value = "  \n" +
            "                    select * from TS_TRANSACTION tx where tx.transaction_type in(?1) \n" +
            "                    AND tx.transaction_status not in(?2) \n" +
            "                    AND tx.topup_provider_name in (?3) \n" +
            "                    AND tx.created_date between ?4 AND ?5 \n" +
            "                     AND tx.sender_wallet in( \n" +
            "                    select ui.login_id from UM_USER_DETAILS ud  \n" +
            "                    left join UM_user_info ui on ui.id=ud.user_id \n" +
            "                    where ud.state_id in(?6) )  ",nativeQuery = true)
    List<TsTransaction> filterTopUpReportsByOperatorName(List<String> txType,
                                                                                                                        List<String> txFailedStatus,
                                                                                                                        List<String> skuCodes,
                                                                                                                        Date fromDate,
                                                                                                                        Date toDate,
                                                                                                                        List<String> stateIds);
    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1 " +
            "AND transaction.createdDate between ?2 AND ?3" +
            " AND transaction.transactionStatus not in(?4) " +
            " AND transaction.topupSkuCode in (?5) " +
            " AND transaction.senderWallet=?6 ")
    List<TsTransaction> findAllByTransactionTypeDateBetweenAndStatusNotInAndSkuCodeInAndTopupReceiverCountryCodeInAndMobileNo(String txType,
                                                                                              Date fromDate,Date toDate,
                                                                                          List<String> txFailedStatus,
                                                                                         List<String> skuCodes,
                                                                                                                                  List<String> countryCodes,String mobileNo);


    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1   AND transaction.createdDate between ?2 AND ?3" +
            " AND transaction.transactionStatus not in(?4) ")
    List<TsTransaction> findAllByTransactionTypeAndDateBetween(String txType,
                                                                Date fromDate, Date toDate, List<String> txFailedStatus);
    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1   " +
            " AND transaction.createdDate between ?2 AND ?3" +
            " AND transaction.transactionStatus not in(?4)" +
            " AND transaction.topupReceiverCountryCode in(?5) ")
    List<TsTransaction> findAllByTransactionTypeAndDateBetweenAndCountryCode(String txType,
                                                                Date fromDate, Date toDate,
                                                                             List<String> txFailedStatus,
                                                                             List<String> countryCode);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1   " +
            " AND transaction.createdDate between ?2 AND ?3" +
            " AND transaction.transactionStatus not in(?4)" +
            " AND transaction.topupReceiverCountryCode in(?5) " +
            " AND transaction.senderWallet = ?6 ")
    List<TsTransaction> findAllByTransactionTypeAndDateBetweenAndCountryCodeAndWalletNo(String txType,
                                                                Date fromDate, Date toDate,
                                                                             List<String> txFailedStatus,
                                                                             List<String> countryCode,String mobileNumber);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1   " +
            " AND transaction.createdDate between ?2 AND ?3" +
            " AND transaction.transactionStatus not in(?4)" +
            " AND transaction.topupReceiverCountryCode in(?5) " +
            " AND transaction.senderWallet=?6 ")
    List<TsTransaction> findAllByTransactionTypeAndDateBetweenAndCountryCodeAndMobileNumber(String txType,
                                                                Date fromDate, Date toDate,
                                                                             List<String> txFailedStatus,
                                                                             List<String> countryCode,String mobileNo);

    @Query("select transaction from TsTransaction transaction" +
            " where transaction.transactionType =?1   AND transaction.createdDate between ?2 AND ?3" +
            " AND transaction.transactionStatus not in(?4) " +
            " AND transaction.topupSkuCode in (?5) ")
    List<TsTransaction> findAllByTransactionTypeAndDateBetweenAndSkuCodes(String txType,
                                                                Date fromDate, Date toDate, List<String> txFailedStatus,
                                                                          List<String> skuCodes);


    TsTransaction findCfeTransactionByIdAndTransactionStatusInAndEodStatus(int transactionId, List<String> statusList, String eodStatus);

    TsTransaction findFirstByToken(String mainTxToken);

    TsTransaction findByTokenOrRefTransactionToken(String token, String refTransactionToken);

    TsTransaction findTopByLogicalSenderOrderByIdDesc(String logicalSender);

    TsTransaction findFirstBySenderWalletOrReceiverWallet(String senderWallet, String receiverWallet);
    TsTransaction findFirstBySenderWalletAndLogicalSender(String senderWallet , String logicalSender);
    TsTransaction findTopByLogicalReceiverOrderByIdDesc(String logicalReceiver);

    List<TsTransaction> findAllBySenderWalletAndEodStatusAndTransactionStatusIn(String senderWallet, String eodStatus, List<String> transactionStatus);
    List<TsTransaction> findAllBySenderWalletAndTransactionStatusIn(String receiverWallet , List<String> transactionStatus);

    TsTransaction findByRefTransactionTokenAndTransactionType(String refTransactionToken, String transactionType);
    TsTransaction findFirstBySenderWalletAndTransactionType(String senderWallet, String transactionType);
    List<TsTransaction> findAllBySenderWalletAndTransactionType(String senderWallet, String transactionType);
    List<TsTransaction> findAllByTokenAndTransactionType(String token, String transactionType);

    TsTransaction findByTokenAndTransactionTypeIgnoreCase(String token, String transactionType);
    List<TsTransaction> findByCreatedDateBetweenAndTransactionType(java.util.Date fromDate, java.util.Date toDate, String transactionType);
    List<TsTransaction> findByTransactionType(String transactionType);
    List<TsTransaction> findByCreatedDateBetweenAndTransactionStatusNotIn(Date startDate, Date endDate, List<String> txFailedStatus);

}
