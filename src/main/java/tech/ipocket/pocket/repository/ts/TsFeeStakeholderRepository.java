package tech.ipocket.pocket.repository.ts;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeStakeholder;

import java.util.List;

public interface TsFeeStakeholderRepository extends CrudRepository<TsFeeStakeholder, Integer> {
    TsFeeStakeholder findFirstByStakeholderName(String stakeholderName);

    TsFeeStakeholder findFirstById(int stakeHolderId);

    @Query("select stakeHolder.glCode from TsFeeStakeholder stakeHolder")
    List<String> findAllGlCode();


}
