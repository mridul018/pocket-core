package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsTransactionDetails;

import java.util.List;

public interface TsTransactionDetailsRepository extends CrudRepository<TsTransactionDetails, Integer> {
    List<TsTransactionDetails> findAllByWalletReferenceAndTransactionTypeIgnoreCase(String walletReference, String transactionType);

    List<TsTransactionDetails> findAllByTransactionId(int oldTransactionId);

    TsTransactionDetails findByTransactionIdAndTransactionTypeIgnoreCase(int transactionId, String transactionType);
}
