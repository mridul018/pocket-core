package tech.ipocket.pocket.repository.ts;


import tech.ipocket.pocket.response.ts.reporting.GlStatementListItemData;
import tech.ipocket.pocket.response.um.UserListItem;
import tech.ipocket.pocket.response.web.UserListResponse;
import tech.ipocket.pocket.utils.LogWriterUtility;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class CustomQuery {
    private LogWriterUtility logWriterUtility = new LogWriterUtility(CustomQuery.class);
    private EntityManager entityManager;

    public CustomQuery(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public String getGroupCodeByWalletNo(String walletNo){
        String queryString ="select group_code from TS_Client where wallet_no=?1 ";

        Query query = entityManager.createNativeQuery(queryString);
        query.setParameter(1, walletNo);
        String groupCode=null;
        try {
            groupCode = (String) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return groupCode;
    }



    public HashMap<BigInteger,List<UserListResponse>> getUserListByTypeAndStatePage(String countryCode, List<String> groupCodes, List<String> stateIds,
                                                                int limit, int page,String requestId){


        HashMap<BigInteger,List<UserListResponse>> dataList=new HashMap<>();

        BigInteger totalElement= BigInteger.valueOf(0);


        try {
            String queryString="select count(ui.id) from UM_user_info ui \n" +
                    "left join UM_USER_DETAILS ud on ui.id=ud.user_id \n" +
                    "where ui.group_code in (?1) \n" +
                    "AND ud.country_code=?2\n" +
                    "AND ud.state_id in (?3)";

            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, groupCodes);
            query.setParameter(2, countryCode);
            query.setParameter(3, stateIds);

            totalElement = (BigInteger) query.getSingleResult();

        }catch (Exception e){
            logWriterUtility.error(requestId,e);
        }

        try{
            String queryString="select ui.id,ui.full_name,ui.login_id,ui.group_code,ui.account_status,ui.photo_id,\n" +
                    "ud.present_address,ud.permanent_address,ud.state_id,ui.created_date from UM_user_info ui \n" +
                    "left join UM_USER_DETAILS ud on ui.id=ud.user_id \n" +
                    "where ui.group_code in (?1) \n" +
                    "AND ud.country_code=?2\n" +
                    "AND ud.state_id in (?3)\n" +
                    "order by ui.id desc\n" +
                    "LIMIT ?4 OFFSET ?5\n";

            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, groupCodes);
            query.setParameter(2, countryCode);
            query.setParameter(3, stateIds);
            query.setParameter(4, limit);
            query.setParameter(5, page*limit);

            List<UserListResponse> responseList=parseResponse(query.getResultList());

            dataList.put(totalElement,responseList);

            return dataList;

        }catch (Exception e){
            logWriterUtility.error(requestId,e);
        }

        return null;
    }

    private List<UserListResponse> parseResponse(List<Object[]> resultList) {
        List<UserListResponse> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (Object[] data : resultList) {
            UserListResponse itemData = new UserListResponse(data);
            listItemData.add(itemData);
        }

        return listItemData;
    }

    public List<UserListItem> getUserListByUserTypeAndCountryCodeAndStateId(List<String> userType, String countryCode, List<String> stateId, String requestId,
                                                                            java.sql.Date startDate, java.sql.Date endDate) {

        try {

            String queryString;

            if(startDate==null||endDate==null){
                queryString = "select ui.id,ui.full_name,ui.mobile_number,ui.email,ui.account_status,ui.photo_id,ud.nationality,\n" +
                        "ud.country_code,ud.state_id,ui.group_code,ui.created_date \n" +
                        " from UM_user_info ui\n" +
                        "left join UM_USER_DETAILS ud on ui.id=ud.user_id\n" +
                        "where ui.group_code in (?1) AND (ud.country_code=?2 AND ud.state_id in(?3) ) \n ";
                Query query = entityManager.createNativeQuery(queryString);
                query.setParameter(1, userType);
                query.setParameter(2, countryCode);
                query.setParameter(3, stateId);
                return parseUserListData(query.getResultList());


            }else{
                queryString = "select ui.id,ui.full_name,ui.mobile_number,ui.email,ui.account_status,ui.photo_id,ud.nationality,\n" +
                        "ud.country_code,ud.state_id,ui.group_code,ui.created_date\n" +
                        " from UM_user_info ui\n" +
                        "left join UM_USER_DETAILS ud on ui.id=ud.user_id\n" +
                        "where ui.group_code in (?1) AND (ud.country_code=?2 AND ud.state_id in(?3) " +
                        "AND (ui.created_date >= ?4 AND ui.created_date <= ?5) ) \n ";
                Query query = entityManager.createNativeQuery(queryString);
                query.setParameter(1, userType);
                query.setParameter(2, countryCode);
                query.setParameter(3, stateId);
                query.setParameter(4, startDate);
                query.setParameter(5, endDate);
                return parseUserListData(query.getResultList());


            }


        } catch (NoResultException e) {
            logWriterUtility.error(requestId,e);
            return null;
        }
    }

    private List<UserListItem> parseUserListData(List<Object[]> resultList) {
        List<UserListItem> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (Object[] data : resultList) {
            UserListItem itemData = new UserListItem(data);
            listItemData.add(itemData);
        }

        return listItemData;
    }

    public BigDecimal getGlBalanceUsingGlCode(String glCode) {

        try {

            String queryString = "select coalesce( SUM(td.CREDIT_AMOUNT) , 0) as credit , coalesce( SUM(td.debit_card) , 0) as debit from TS_GL_TRANSACTION_DETAILS td\n" +
                    "    where td.GL_CODE=?1 ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, glCode);

            Object[] objectList = (Object[]) query.getSingleResult();

            BigDecimal creditAmount = ((BigDecimal) objectList[0]);
            BigDecimal debitAmount = ((BigDecimal) objectList[1]);

            BigDecimal balance = creditAmount.subtract(debitAmount);

            return balance == null ? BigDecimal.ZERO : balance;
        } catch (NoResultException e) {
            return BigDecimal.valueOf(0);
        }
    }

    public BigDecimal getGlAggregateBalanceUsingGlCodes(List<String> glCodeList, String requestid) {

        try {


            if (glCodeList == null || glCodeList.size() == 0) {
                logWriterUtility.trace(requestid, "Gl code list empty");
                return BigDecimal.ZERO;
            }

            List<String> distinctGlCodes = glCodeList.stream().distinct().collect(Collectors.toList());

            String queryString = "select coalesce( SUM(td.CREDIT_AMOUNT) , 0) as credit , coalesce( SUM(td.DEBIT_CARD) , 0) as debit from TS_GL_TRANSACTION_DETAILS td\n" +
                    "    where td.GL_CODE IN (?1) ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, distinctGlCodes);

            List<Object[]> resultList = query.getResultList();

            BigDecimal netBalance = BigDecimal.ZERO;

            for (int index = 0; index < resultList.size(); index++) {
                Object[] bankDebitCreditBalance = resultList.get(index);

                BigDecimal creditAmount = ((BigDecimal) bankDebitCreditBalance[0]);
                BigDecimal debitAmount = ((BigDecimal) bankDebitCreditBalance[1]);

                BigDecimal netBalanceTemp = creditAmount.subtract(debitAmount);
                netBalance = netBalance.add(netBalanceTemp);
            }

            return netBalance;
        } catch (NoResultException e) {
            logWriterUtility.error(requestid, "Error :" + e.getMessage());
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal getGlBalanceUsingGlCodeAndDateRange(Long glCode, Date startDate, Date endDate) {

        try {

            String queryString;


            Query query;
            if (startDate == null) {
                queryString = "SELECT COALESCE( SUM(gltd.CREDIT_AMOUNT) , 0) as credit , COALESCE( SUM(gltd.debit_card) , 0) as debit \n" +
                        "from TS_GL_TRANSACTION_DETAILS gltd " +
                        "left join TS_TRANSACTION transaction on transaction.ID=gltd.transaction_id  " +
                        "where gltd.GL_CODE=?1  " +
                        "AND transaction.created_date <=?2 ";


                query = entityManager.createNativeQuery(queryString);
                query.setParameter(1, glCode);
                query.setParameter(2, endDate);

            } else {
                queryString = "SELECT COALESCE( SUM(gltd.CREDIT_AMOUNT) , 0) as credit , COALESCE( SUM(gltd.debit_card) , 0) as debit \n" +
                        "from TS_GL_TRANSACTION_DETAILS gltd " +
                        "left join TS_TRANSACTION transaction  on transaction.ID=gltd.transaction_id  " +
                        "where gltd.GL_CODE=?1 AND transaction.transaction_date >=?2  " +
                        "AND transaction.created_date <=?3 ";


                query = entityManager.createNativeQuery(queryString);
                query.setParameter(1, glCode);
                query.setParameter(2, startDate);
                query.setParameter(3, endDate);
            }

            Object[] objectList = (Object[]) query.getSingleResult();

            BigDecimal creditAmount = ((BigDecimal) objectList[0]);
            BigDecimal debitAmount = ((BigDecimal) objectList[1]);


            BigDecimal netBalance = creditAmount.subtract(debitAmount);
            return netBalance;
        } catch (NoResultException e) {
            return BigDecimal.valueOf(0);
        }
    }

    public List<String> getDistinctGlCodesUsingGlNameLike(String searchQuery,String requestId) {

        try {

            String queryString = "SELECT DISTINCT GL_CODE from TS_GL \n" +
                    "where GL_NAME LIKE ?1 ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, searchQuery);

            List<String > glCodes = parseGlList(query.getResultList());

            return glCodes;
        } catch (NoResultException e) {
            logWriterUtility.error(requestId, "No result found :" + e.getLocalizedMessage());
            return null;
        }

    }

    private List<String> parseGlList(List<Object> resultList) {
        List<String> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (int i = 0; i < resultList.size(); i++) {

            String data = (String) resultList.get(i);

            listItemData.add(data);
        }

        return listItemData;
    }

    public List<GlStatementListItemData> getTransactionsUsingGlCodeAndDateRange(Long glCode, Date startDate,
                                                                                Date endDate, TsServiceRepository tsServiceRepository) {

        try {

            String queryString = "\n" +
                    "SELECT transaction.receiver_wallet, transaction.TRANSACTION_TYPE , transaction.created_date , \n" +
                    "gltd.CREDIT_AMOUNT , gltd.debit_card , transaction.ID, transaction.token  from TS_GL_TRANSACTION_DETAILS as gltd \n" +
                    "left join TS_TRANSACTION as transaction ON gltd.transaction_id=transaction.id  where gltd.GL_CODE= ?1 \n" +
                    "AND (transaction.created_date >= ?2 AND transaction.created_date <= ?3) \n" +
                    " order by transaction.id ASC ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, glCode);
            query.setParameter(2, startDate);
            query.setParameter(3, endDate);

            logWriterUtility.trace("", "Query :" + queryString);
            logWriterUtility.trace("", "Data :" + startDate + " " + endDate);

            List<GlStatementListItemData> data = parseGlStatementListData(query.getResultList(), tsServiceRepository);

            return data;
        } catch (NoResultException e) {
            return null;
        }

    }
    public Set<String> getDistinctOperatorsByCountryCode(String countryCode,String requestId) {

        try {

            String queryString = " SELECT DISTINCT(topup_sku_code) FROM TS_TRANSACTION\n " +
                    " where topup_receiver_country_code=?1 ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, countryCode);

            logWriterUtility.trace(requestId, "Query :" + queryString);

            Set<String> data = parseOperators(query.getResultList());

            return data;
        } catch (NoResultException e) {
            return new HashSet<>();
        }
    }
    public Set<String> getDistinctOperators(String requestId) {

        try {

            String queryString = " SELECT DISTINCT(topup_sku_code) FROM TS_TRANSACTION \n " +
                    "  ";
            Query query = entityManager.createNativeQuery(queryString);

            logWriterUtility.trace(requestId, "Query :" + queryString);

            Set<String> data = parseOperators(query.getResultList());

            return data;
        } catch (NoResultException e) {
            return new HashSet<>();
        }

    }

    public Set<String> getDistinctOperatorsName(String requestId,String countryCode) {

        try {

            String queryString = " SELECT DISTINCT(topup_provider_name) FROM TS_TRANSACTION \n " +
                    " where  topup_provider_name is not null ";
            Query query = entityManager.createNativeQuery(queryString);
//            query.setParameter(1, countryCode);

            logWriterUtility.trace(requestId, "Query :" + queryString);

            Set<String> data = parseOperators(query.getResultList());

            return data;
        } catch (NoResultException e) {
            return new HashSet<>();
        }

    }
    /*public Set<String> getDistinctOperatorsName(String requestId,String countryCode) {

        try {

            String queryString = " SELECT DISTINCT(topup_provider_name) FROM TS_TRANSACTION \n " +
                    " where topup_receiver_country_code=?1  and topup_provider_name is not null ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, countryCode);

            logWriterUtility.trace(requestId, "Query :" + queryString);

            Set<String> data = parseOperators(query.getResultList());

            return data;
        } catch (NoResultException e) {
            return new HashSet<>();
        }

    }*/

    private Set<String> parseOperators(List<Object[]> resultList) {
        if (resultList == null || resultList.size() == 0) {
            return new HashSet<>();
        }

        Set<String> result=new HashSet<>();

        for (int i = 0; i < resultList.size(); i++) {
            String op= String.valueOf(resultList.get(i));
            result.add(op);
        }

        return result;
    }
    private List<GlStatementListItemData> parseGlStatementListData(List<Object[]> resultList,
                                                                   TsServiceRepository tsServiceRepository) {
        List<GlStatementListItemData> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (Object[] data : resultList) {
            GlStatementListItemData itemData = new GlStatementListItemData(data, tsServiceRepository);
            listItemData.add(itemData);
        }

        return listItemData;
    }

    /*public List<GlStatementListItemData> getTransactionsUsingGlCodeAndDateRange(Long glCode, String productCode, Date startDate,
                                                                                Date endDate, DmoneyService cfeServicesService) {

        try {

            String queryString = "SELECT transaction.TOKEN_NO, transaction.TRANSACTION_TYPE , " +
                    "transaction.transaction_date , gltd.CREDIT_AMOUNT , gltd.DEBIT_AMOUNT , transaction.ID " +
                    "from CFE_GL_TRANSACTION_DETAILS gltd " +
                    "left join CFE_TRANSACTION transaction ON gltd.transaction_id=transaction.ID " +
                    "where gltd.GL_CODE= ?1 AND gltd.PRODUCT_CODE=?2 AND (transaction.transaction_date >= ?3" +
                    " AND transaction.transaction_date <= ?4)  order by TRANSACTION .ID ASC ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, glCode);
            query.setParameter(2, productCode);
            query.setParameter(3, startDate);
            query.setParameter(4, endDate);

            logWriterUtility.trace("", "Query :" + queryString);
            logWriterUtility.trace("", "Data :" + startDate + " " + endDate);

            List<GlStatementListItemData> data = parseGlStatementListData(query.getResultList(), productCode, cfeServicesService);

            return data;
        } catch (NoResultException e) {
            return null;
        }

    }


    private List<GlStatementListItemData> parseGlStatementListData(List<Object[]> resultList, String productCode, DmoneyService cfeServicesService) {
        List<GlStatementListItemData> listItemData = new ArrayList<>();
        if (resultList == null || resultList.size() == 0) {
            return listItemData;
        }

        for (Object[] data : resultList) {
            GlStatementListItemData itemData = new GlStatementListItemData(data, productCode, cfeServicesService);
            listItemData.add(itemData);
        }

        return listItemData;
    }

    public BigDecimal findBeforeTxOfDateRange(String mobileNumber, Long transactionId, String productCode, String requestId) {
        String queryString = "select id,AMOUNT,PAYER_MOBILE_NUMBER,PAYEE_MOBILE_NUMBER,FEE_AMOUNT,FEE_PAYER,transaction_date,transaction_status,transaction_type,WITHDRAWER_RUNNING_BALANCE,DEPOSITOR_RUNNING_BALANCE" +
                " from CFE_TRANSACTION transaction\n" +
                "where  transaction.id<?2 AND (transaction.PAYER_MOBILE_NUMBER=?1 or transaction.PAYEE_MOBILE_NUMBER=?1)  AND transaction.PRODUCT_CODE=?3\n" +
                "order by transaction.id desc OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";

        Query query = entityManager.createNativeQuery(queryString);
        query.setParameter(1, mobileNumber);
        query.setParameter(2, transactionId);
        query.setParameter(3, productCode);

        Object[] objectList;
        try {
            objectList = (Object[]) query.getSingleResult();
            if (objectList == null || objectList.length == 0) {
                logWriterUtility.trace(requestId, "No transaction found");
                return BigDecimal.valueOf(0);
            }

            CfeTransaction cfeTransaction = getTransaction(objectList);

            boolean isUserDepositor = mobileNumber.equals(cfeTransaction.getPayeeMobileNumber());
            if (isUserDepositor) {
                return cfeTransaction.getDepositorRunningBalance();
            } else {
                return cfeTransaction.getWithdrawerRunningBalance();
            }
        } catch (Exception e) {
            logWriterUtility.error(requestId, "Error :" + e.getMessage());
            return new BigDecimal(0);
        }


    }



    public List<String> getTransactionDetails(int minCount, java.sql.Date startDate, java.sql.Date endDate, String productCode, String requestId) {

        List<String> userList = new ArrayList<>();
        try {

            String searchQuery = "SELECT * FROM ( " +
                    "    SELECT tx.PAYER_MOBILE_NUMBER,COUNT(tx.PAYER_MOBILE_NUMBER) as count FROM CFE_TRANSACTION tx where tx.product_code=?1 AND tx.CREATED_DATE >= ?2 AND tx.CREATED_DATE <= ?3 GROUP BY PAYER_MOBILE_NUMBER\n" +
                    "    UNION ALL\n" +
                    "    SELECT tx.PAYEE_MOBILE_NUMBER ,COUNT(tx.PAYEE_MOBILE_NUMBER) as count FROM CFE_TRANSACTION tx where tx.product_code=?1 AND tx.CREATED_DATE >= ?2 AND tx.CREATED_DATE <= ?3 GROUP BY tx.PAYEE_MOBILE_NUMBER\n" +
                    "    \n" +
                    " ) tx ";

            Query query = entityManager.createNativeQuery(searchQuery);
            query.setParameter(1, productCode);
            query.setParameter(2, startDate);
            query.setParameter(3, endDate);

            userList = parseTxList(query.getResultList(), minCount);

        } catch (Exception e) {
            logWriterUtility.error(requestId, e);
        }

        return userList;
    }

    private List<String> parseTxList(List<Object[]> resultList, int minCount) {

        if (resultList == null || resultList.size() == 0) {
            return null;
        }

        HashMap<String, BigDecimal> dataSet = new HashMap<>();

        for (Object[] tempData : resultList) {

            if (tempData[0] == null) {
                continue;
            }

            String mobileNumber = (String) tempData[0];
            BigDecimal count = (BigDecimal) tempData[1];

            if (dataSet.size() == 0) {
                dataSet.put(mobileNumber, count);
                continue;
            }

            if (!dataSet.containsKey(mobileNumber)) {
                dataSet.put(mobileNumber, count);
            } else {
                BigDecimal prevValue = dataSet.get(mobileNumber);
                dataSet.put(mobileNumber, (prevValue.add(count)));
            }
        }

        List<String> finalResult = new ArrayList<>();

        dataSet.forEach((key, value) -> {
            if (value.intValue() >= minCount) {
                finalResult.add(key);
            }
        });

        return finalResult;
    }

    private CfeTransaction getTransaction(Object[] objectList) {
        try {
            CfeTransaction cfeTransaction = new CfeTransaction();

            BigDecimal id = (BigDecimal) objectList[0];

            cfeTransaction.setId(id.longValueExact());
            cfeTransaction.setAmount((BigDecimal) objectList[1]);
            cfeTransaction.setPayerMobileNumber((String) objectList[2]);
            cfeTransaction.setPayeeMobileNumber((String) objectList[3]);
            cfeTransaction.setFeeAmount((BigDecimal) objectList[4]);
            cfeTransaction.setFeePayer((String) objectList[5]);
            cfeTransaction.setTransactionDate((Date) objectList[6]);
            cfeTransaction.setTransactionStatus((String) objectList[7]);
            cfeTransaction.setTransactionType((String) objectList[8]);
            cfeTransaction.setWithdrawerRunningBalance((BigDecimal) objectList[9]);
            cfeTransaction.setDepositorRunningBalance((BigDecimal) objectList[10]);
            return cfeTransaction;
        } catch (Exception e) {
            return null;
        }
    }

    public String isDbConnected(String requestId) {

        try {

            String queryString = "select ?1 from dual ";
            Query query = entityManager.createNativeQuery(queryString);
            query.setParameter(1, "hello");

            List<Object[]> resultList = query.getResultList();

            if (resultList == null || resultList.size() == 0) {
                return "disconnected";
            }

            return "connected";
        } catch (NoResultException e) {
            logWriterUtility.error(requestId, "No result found :" + e.getLocalizedMessage());
            return "disconnected";

        }

    }*/




}