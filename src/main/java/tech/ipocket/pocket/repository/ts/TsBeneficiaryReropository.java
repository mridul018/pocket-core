package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsBeneficiary;

import java.util.List;

public interface TsBeneficiaryReropository extends CrudRepository<TsBeneficiary,Integer> {
    List<TsBeneficiary> findAllByCreatorWalletAndStatus(String creatorWallet,String status);
    List<TsBeneficiary> findAllByCreatorWalletAndStatusAndType(String creatorWallet,String status,String type);
}
