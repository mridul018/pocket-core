package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class Promo {
    @SerializedName("since")
    public Object since;

    @SerializedName("till")
    public Object till;

    @SerializedName("text")
    public Object text;
}
