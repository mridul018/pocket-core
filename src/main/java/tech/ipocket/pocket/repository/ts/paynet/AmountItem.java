package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class AmountItem {
    @SerializedName("id")
    public Object id;

    @SerializedName("type_id")
    public Object typeId;

    @SerializedName("name")
    public Object name;

    @SerializedName("amount")
    public Object amount;

    @SerializedName("currency")
    public Object currency;

    @SerializedName("sale_price")
    public Object salePrice;

    @SerializedName("sale_currency")
    public Object saleCurrency;


    @SerializedName("price")
    public Object price;


    @SerializedName("price_currency")
    public Object priceCurrency;
}
