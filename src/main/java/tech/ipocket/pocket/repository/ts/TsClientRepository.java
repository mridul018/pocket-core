package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsClient;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TsClientRepository extends CrudRepository<TsClient, Integer> {

    Integer countAllByWalletNoAndUserStatusNotIn(String mobileNumber, List<String> clientStatus);

    TsClient findFirstByWalletNoAndUserStatusNotIn(String mobileNumber, List<String> clientStatus);
    TsClient findLastByWalletNoOrderByIdDesc(String mobileNumber);

    TsClient findFirstByGroupCodeAndUserStatusNotIn(String groupCode, List<String> clientStatus);

    TsClient findFirstByGroupCode(String groupCode);
    Integer countAllByTransactionProfileCode(String transactionProfileCode);
    Integer countAllByFeeProfileCode(String feeProfileCode);

    Optional<TsClient> findByWalletNo(String walletNo);
    List<TsClient> findByGroupCodeAndUserStatusAndCreatedDateBetween(String groupCode, String userStatusOrAccountStatus, Date startDate, Date endDate);
}
