package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsLimit;

import java.util.List;

public interface TsLimitRepository extends CrudRepository<TsLimit,Integer> {
    TsLimit findFirstByLimitCodeAndStatusOrderByIdDesc(String limitCode,String status);

    TsLimit findFirstByLimitNameAndStatusOrderByIdDesc(String limitName,String status);

    List<TsLimit> findAllByLimitCodeInAndStatusOrderByIdDesc(List<String> codes,String status);

    TsLimit findFirstById(int id);

    List<TsLimit> findAllByStatusOrderByIdDesc(String status);
}
