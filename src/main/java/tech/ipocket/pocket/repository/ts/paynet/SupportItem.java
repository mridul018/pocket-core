package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class SupportItem {
    @SerializedName("phone")
    public Object phone;

    @SerializedName("email")
    public Object email;
}
