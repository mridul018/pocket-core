package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFee;
import tech.ipocket.pocket.entity.TsFeeProfile;
import tech.ipocket.pocket.entity.TsGl;

import java.util.List;
import java.util.Set;

public interface TsGlRepository extends CrudRepository<TsGl, Integer> {
    TsGl findFirstByGlCode(String glCode);
    List<TsGl> findAllByParentGlCode(String parentGlCode);

    TsGl findFirstByGlName(String glName);

    List<TsGl> findAllByGlCodeInAndParentGl(Set<String> glCodes,boolean isParentGl);
}
