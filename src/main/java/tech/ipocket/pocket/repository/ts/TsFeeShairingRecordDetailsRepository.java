package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeShairingRecordDetails;

public interface TsFeeShairingRecordDetailsRepository extends CrudRepository<TsFeeShairingRecordDetails,Integer> {
}
