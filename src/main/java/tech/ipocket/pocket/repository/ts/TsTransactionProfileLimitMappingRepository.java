package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsTransactionProfileLimitMapping;

import java.util.List;


public interface TsTransactionProfileLimitMappingRepository extends CrudRepository<TsTransactionProfileLimitMapping,Integer> {
    TsTransactionProfileLimitMapping findFirstByTxProfileCodeAndServiceCode(String transactionProfileCode, String serviceCode);
    List<TsTransactionProfileLimitMapping> findAllByTxProfileCodeOrderByIdDesc(String transactionProfileCode);
    TsTransactionProfileLimitMapping findFirstByTxProfileCodeAndServiceCodeAndLimitCode(String transactionProfileCode, String serviceCode,String limitCode);

    Integer countAllByLimitCode(String limitCode);



}
