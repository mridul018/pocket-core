package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsDispute;

import java.util.List;

public interface TsDisputeRepository extends CrudRepository<TsDispute,Integer> {
    TsDispute findFirstByTransactionTokenNoOrderByIdDesc(String transactionTokenNo);
    List<TsDispute> findAllByStatusOrderByIdDesc(String status);
    List<TsDispute> findAllByReceiverWalletOrSenderWallet(String receiverWallet,String senderWallet);

    TsDispute findFirstById(Integer disputeId);
}
