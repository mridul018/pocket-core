package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.DapiAccessToken;

import java.util.List;

public interface AccessTokenRepository extends CrudRepository<DapiAccessToken, Integer> {
    DapiAccessToken findFirstByTokenId(String tokenId);
    List<DapiAccessToken> findAllByOrderByIdDesc();

}
