package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsClient;
import tech.ipocket.pocket.entity.TsClientBalance;

import java.util.Optional;

public interface TsClientBalanceRepository extends CrudRepository<TsClientBalance, Integer> {
    TsClientBalance findFirstByTsClientByClientId(TsClient tsClient);

    Optional<TsClientBalance> findByTsClientByClientId(TsClient tsClientByClientId);
}
