package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsBanks;

import java.util.List;

public interface TsBankRepository extends CrudRepository<TsBanks, Integer> {

    TsBanks findFirstByBankName(String bankName);

    TsBanks findFirstByGlCode(String bankGlCode);

    TsBanks findFirstByBankCode(String bankCode);

    List<TsBanks> findAllByStatus(String status);

    TsBanks findFirstById(int bankId);
}
