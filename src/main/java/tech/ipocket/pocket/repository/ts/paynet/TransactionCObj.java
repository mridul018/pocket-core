package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class TransactionCObj {
    @SerializedName("external_transaction_id")
    public Object externalTransactionId;

    @SerializedName("id")
    public Object id;

    @SerializedName("state")
    public Object state;

    @SerializedName("time")
    public Object time;

    @SerializedName("price_min")
    public Object priceMin;

    @SerializedName("price_max")
    public Object priceMax;

    @SerializedName("service")
    public ServiceObj service;

    @SerializedName("account")
    public Object account;

    @SerializedName("amount")
    public Object amount;

    @SerializedName("amount_currency")
    public Object amountCurrency;

    @SerializedName("price")
    public Object price;

    @SerializedName("price_currency")
    public Object priceCurrency;
}
