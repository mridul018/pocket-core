package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class CountryItem {
    @SerializedName("id")
    public Object id;

    @SerializedName("name")
    public Object name;

    @SerializedName("phone_code")
    public Object phoneCode;

    @SerializedName("logo")
    public Object logo;

    @SerializedName("logo_large")
    public Object logoLarge;
}
