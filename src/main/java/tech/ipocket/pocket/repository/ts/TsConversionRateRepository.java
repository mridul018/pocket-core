package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsConversionRate;

import java.util.List;

public interface TsConversionRateRepository extends CrudRepository<TsConversionRate,Integer> {
    TsConversionRate findFirstByServiceCodeAndStatusOrderByIdDesc(String serviceCode,String status);
    List<TsConversionRate> findAllByStatusOrderByIdDesc(String status);
}
