package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class Receipt {
    @SerializedName("header")
    public Object header;

    @SerializedName("subheader")
    public Object subHeader;

    @SerializedName("info")
    public Object info;

    @SerializedName("instruction")
    public Object instruction;

    @SerializedName("promo")
    public Object promo;

    @SerializedName("footer")
    public Object footer;
}
