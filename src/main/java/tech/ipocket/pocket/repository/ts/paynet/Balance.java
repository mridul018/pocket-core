package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class Balance {
    @SerializedName("currency")
    public Object currency;

    @SerializedName("wallets")
    public Wallets wallets;

    @SerializedName("error")
    public ErrorItem error;
}
