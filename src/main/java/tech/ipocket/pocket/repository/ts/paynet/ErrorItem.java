package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class ErrorItem {
    @SerializedName("code")
    public Object code;

    @SerializedName("message")
    public Object message;
}
