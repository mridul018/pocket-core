package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProviderOperator {
    @SerializedName("country")
    public ProviderCountry country;

    @SerializedName("provider")
    public ProviderItem provider;

    @SerializedName("amounts")
    public List<AmountItem> amounts = null;

    @SerializedName("error")
    public ErrorItem error;
}
