package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFee;

import java.util.List;

public interface TsFeeRepository extends CrudRepository<TsFee, Integer> {
    TsFee findFirstById(Integer feeId);
    TsFee findFirstByFeeCodeAndStatus(String feeCode,String status);

    TsFee findFirstByFeeNameAndStatus(String feeName,String status);
    List<TsFee> findAllByStatusOrderByFeeCode(String status);
}
