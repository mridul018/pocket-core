package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InfoDetail {
    @SerializedName("agent")
    public AgentItem agent;

    @SerializedName("pos")
    public PosItem pos;

    @SerializedName("support")
    public SupportItem support;

    @SerializedName("error")
    public ErrorItem error;
}
