package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class ProviderCountry {
    @SerializedName("id")
    public Object id;

    @SerializedName("name")
    public Object name;
}
