package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Provider {
    @SerializedName("country")
    public ProviderCountry country;

    @SerializedName("providers")
    public List<ProviderItem> providers = null;

    @SerializedName("error")
    public ErrorItem error;
}
