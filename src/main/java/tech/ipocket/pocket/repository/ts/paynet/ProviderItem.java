package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class ProviderItem {
    @SerializedName("id")
    public Object id;

    @SerializedName("parent_id")
    public Object parentId;

    @SerializedName("type_id")
    public Object typeId;

    @SerializedName("is_pin_based")
    public boolean isPinBased;

    @SerializedName("name")
    public Object name;

    @SerializedName("logo")
    public Object logo;
}
