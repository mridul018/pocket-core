package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeProfileLimitMapping;

import java.util.List;

public interface TsFeeProfileLimitMappingRepository extends CrudRepository<TsFeeProfileLimitMapping, Integer> {
    TsFeeProfileLimitMapping findFirstByFeeCodeAndServiceCodeAndFeeProfileCodeOrderByIdDesc(String feeCode, String serviceCode, String profileCode);

    TsFeeProfileLimitMapping findFirstByFeeProfileCodeAndServiceCodeOrderByIdDesc(String profileCode, String serviceCode);
    List<TsFeeProfileLimitMapping> findAllByFeeCode(String feeCode);
    List<TsFeeProfileLimitMapping> findAllByFeeProfileCodeOrderByIdDesc(String feeCodeProfileCode);
    List<TsFeeProfileLimitMapping> findAllByFeeProfileCodeAndServiceCode(String feeCodeProfileCode,String serviceCode);
}
