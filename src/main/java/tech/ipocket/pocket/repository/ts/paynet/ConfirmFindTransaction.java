package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class ConfirmFindTransaction {
    @SerializedName("simulation")
    public Object simulation;

    @SerializedName("transaction")
    public TransactionObj transaction;

    @SerializedName("info")
    public InfoObj info;

    @SerializedName("receipt")
    public Receipt receipt;

    @SerializedName("error")
    public ErrorItem error;
}
