package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsBranch;

import java.util.List;

public interface TsBranchRepository extends CrudRepository<TsBranch,Integer> {
    TsBranch findFirstBySwiftCodeAndStatus(String branchSwiftCode, String status);
    TsBranch findFirstBySwiftCode(String branchSwiftCode);

    List<TsBranch> findAllBySwiftCodeIn(List<String> swiftCodes);
}
