package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsService;

import java.util.List;

public interface TsServiceRepository extends CrudRepository<TsService, Integer> {
    List<TsService> findAllByStatus(String status);

    TsService findFirstByServiceCode(String serviceCode);
    TsService findFirstByServiceName(String serviceName);
    TsService findFirstById(Integer id);
}
