package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Detail {
    @SerializedName("country")
    public Object country;

    @SerializedName("operator")
    public Object operator;

    @SerializedName("amounts")
    public List<Amount> amounts;
}
