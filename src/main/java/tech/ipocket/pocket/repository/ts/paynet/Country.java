package tech.ipocket.pocket.repository.ts.paynet;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Country {
    @SerializedName("countries")
    public List<CountryItem> countries = null;

    @SerializedName("error")
    public ErrorItem error;
}
