package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class InfoObj {
    @SerializedName("reference")
    public Object reference;

    @SerializedName("number")
    public Object number;

    @SerializedName("serial")
    public Object serial;
}
