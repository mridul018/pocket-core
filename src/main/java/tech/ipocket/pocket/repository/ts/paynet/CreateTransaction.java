package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class CreateTransaction {
    @SerializedName("simulation")
    public Object simulation;

    @SerializedName("promo")
    public Promo promo;

    @SerializedName("transaction")
    public TransactionCObj transaction;

    @SerializedName("details")
    public Detail details;
}
