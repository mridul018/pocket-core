package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsPaywellOperatorMap;

public interface TsPaywellOperatorMapRepository extends CrudRepository<TsPaywellOperatorMap,Integer> {
    TsPaywellOperatorMap findFirstByOperatorCodeAndStatus(String operatorCode, String status);
}
