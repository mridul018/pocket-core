package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class AgentItem {
    @SerializedName("name")
    public Object name;

    @SerializedName("address")
    public Object address;
}
