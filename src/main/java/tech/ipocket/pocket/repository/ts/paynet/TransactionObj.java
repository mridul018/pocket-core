package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class TransactionObj {
    @SerializedName("external_transaction_id")
    public Object external_transaction_id;

    @SerializedName("id")
    public Object id;

    @SerializedName("time")
    public Object time;

    @SerializedName("state")
    public Object state;

    @SerializedName("error")
    public Object error;

    @SerializedName("service")
    public ServiceObj service;

    @SerializedName("account")
    public Object account;

    @SerializedName("amount")
    public Object amount;

    @SerializedName("amount_currency")
    public Object amount_currency;

    @SerializedName("price")
    public Object price;

    @SerializedName("price_currency")
    public Object price_currency;

    @SerializedName("customer")
    public Customer customer;
}
