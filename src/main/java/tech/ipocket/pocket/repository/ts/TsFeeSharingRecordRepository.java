package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeSharingRecord;

import java.sql.Date;
import java.util.List;

public interface TsFeeSharingRecordRepository extends CrudRepository<TsFeeSharingRecord,Integer> {

    List<TsFeeSharingRecord> findAllByShareDateBetween(Date startDate, Date endDate);
}
