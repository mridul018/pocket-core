package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsGl;
import tech.ipocket.pocket.entity.TsGlTransactionDetails;
import tech.ipocket.pocket.entity.TsTransaction;

import java.util.List;

public interface TsGlTransactionDetailsRepository extends CrudRepository<TsGlTransactionDetails, Integer> {
    List<TsGlTransactionDetails> findAllByEodStatus(String eodStatus);

    TsGlTransactionDetails findFirstByTsTransactionByTransactionIdAndEodStatus(TsTransaction tsTransaction, String eodStatus);

    List<TsGlTransactionDetails> findAllByTsTransactionByTransactionId(TsTransaction tsTransaction);

}
