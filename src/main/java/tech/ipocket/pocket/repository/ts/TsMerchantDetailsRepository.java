package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsMerchantDetails;

import java.util.List;

public interface TsMerchantDetailsRepository extends CrudRepository<TsMerchantDetails, Integer> {
    TsMerchantDetails findFirstByShortCode(String shortCode);
    List<TsMerchantDetails> findAll();
    List<TsMerchantDetails> findByWalletNo(String walletNo);
}
