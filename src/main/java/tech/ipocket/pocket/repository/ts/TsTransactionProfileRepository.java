package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsTransactionProfile;

import java.util.List;

public interface TsTransactionProfileRepository extends CrudRepository<TsTransactionProfile, Integer> {

    TsTransactionProfile findFirstByProfileCodeAndStatusOrderByIdDesc(String profileCode, String status);

    TsTransactionProfile findFirstByProfileNameAndStatusOrderByIdDesc(String profileName, String status);

    TsTransactionProfile findFirstById(int id);

    List<TsTransactionProfile> findAllByStatus(String status);
}
