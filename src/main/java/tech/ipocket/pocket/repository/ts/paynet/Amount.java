package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class Amount {
    @SerializedName("amount")
    public Object amount;

    @SerializedName("currency")
    public Object currency;

    @SerializedName("sale_price")
    public Object salePrice;

    @SerializedName("sale_currency")
    public Object saleCurrency;

    @SerializedName("price")
    public Object price;

    @SerializedName("price_currency")
    public Object priceCurrency;
}
