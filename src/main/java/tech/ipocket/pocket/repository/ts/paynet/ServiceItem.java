package tech.ipocket.pocket.repository.ts.paynet;

import com.google.gson.annotations.SerializedName;

public class ServiceItem {
    @SerializedName("id")
    public Object id;

    @SerializedName("name")
    public Object name;

    @SerializedName("promo_name")
    public Object promoName;

    @SerializedName("amount_min")
    public Object amountMin;

    @SerializedName("amount_max")
    public Object amountMax;

    @SerializedName("active")
    public boolean isActive;
}
