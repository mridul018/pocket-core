package tech.ipocket.pocket.repository.ts;

import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.TsFeeProfile;

import java.util.List;

public interface TsFeeProfileRepository extends CrudRepository<TsFeeProfile, Integer> {
    TsFeeProfile findFirstByProfileCodeAndStatus(String profileCode,String status);

    TsFeeProfile findFirstByProfileNameAndStatus(String profileName,String status);

    List<TsFeeProfile> findAllByStatus(String status);
}
