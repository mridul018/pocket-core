package tech.ipocket.pocket.repository.web;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmUserGroup;

import java.util.List;

@Repository
public interface UserGroupRepository extends JpaRepository<UmUserGroup, Integer> {
    UmUserGroup findByGroupCodeAndGroupStatus(String groupCode,String status);

    List<UmUserGroup> findAllByGroupStatus(String status);
}
