package tech.ipocket.pocket.repository.web;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import tech.ipocket.pocket.entity.UmUserFunction;

import java.util.List;

public interface UserFunctionRepository extends JpaRepository<UmUserFunction, Integer> {
    List<UmUserFunction> findAllByFunctionCodeInAndFunctionStatus(List<String> codes,String status);

    UmUserFunction findByFunctionCodeAndFunctionStatus(String functionCode,String status);

    List<UmUserFunction> findAllByFunctionStatus(String status);
}
