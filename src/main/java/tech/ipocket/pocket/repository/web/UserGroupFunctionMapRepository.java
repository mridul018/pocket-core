package tech.ipocket.pocket.repository.web;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.ipocket.pocket.entity.UmGroupFunctionMapping;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserGroupFunctionMapRepository extends JpaRepository<UmGroupFunctionMapping, Integer> {
    List<UmGroupFunctionMapping> findAllByGroupCodeAndStatus(String groupCode, String status);

    UmGroupFunctionMapping findByGroupCodeAndFunctionCode(String groupCode, String functionCode);

    @Modifying
    @Transactional
    @Query("delete from UmGroupFunctionMapping map where map.groupCode = ?1")
    void deleteAllByGroupCode(String groupCode);

    Long countAllByFunctionCodeAndStatus(String functionCode,String status);
}
