package tech.ipocket.pocket.controllers.web.um.passport;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportSubTypeEmptyRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportSubTypeRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.um.passport.EmbassyPassportService;
import tech.ipocket.pocket.services.um.passport.EmbassyPassportSubTypeService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "embassy-passport")
public class EmbassyPassportSubTypeController extends BaseEndpoint {
    @Autowired
    private RequestValidator requestValidator;
    @Autowired
    private EmbassyPassportSubTypeService embassyPassportSubTypeService;
    @Autowired
    private EmbassyPassportService embassyPassportService;


    @PostMapping(value = "/createEmbassyPassportSubType")
    public @ResponseBody ResponseEntity<BaseResponseObject> createEmbassyPassportSubType(@RequestBody UmEmbPassportSubTypeRequest request) throws PocketException {
        request = (UmEmbPassportSubTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportSubTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportSubTypeService.createPassportSubType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/updatePassportSubType")
    public @ResponseBody ResponseEntity<BaseResponseObject> updatePassportSubType(@RequestBody UmEmbPassportSubTypeRequest request) throws PocketException {
        request = (UmEmbPassportSubTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportSubTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportSubTypeService.updatePassportSubType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/embassyPassportSubTypeListById")
    public @ResponseBody ResponseEntity<BaseResponseObject> embassyPassportSubTypeListById(@RequestBody UmEmbPassportTypeRequest request) throws PocketException {
        request = (UmEmbPassportTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

         baseResponseObject = embassyPassportSubTypeService.embassyPassportSubTypeListById(request, tsSessionObject);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/embassyPassportSubTypeList")
    public @ResponseBody ResponseEntity<BaseResponseObject> embassyPassportSubTypeList(@RequestBody UmEmbPassportSubTypeEmptyRequest request) throws PocketException {
        request = (UmEmbPassportSubTypeEmptyRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportSubTypeEmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportSubTypeService.embassyPassportSubTypeList(request, tsSessionObject);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchPassportSubTypeAndStatus")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchPassportSubTypeAndStatus(@RequestBody UmEmbPassportSubTypeRequest request) throws PocketException {
        request = (UmEmbPassportSubTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportSubTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportSubTypeService.searchPassportSubTypeAndStatus(request, tsSessionObject);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
