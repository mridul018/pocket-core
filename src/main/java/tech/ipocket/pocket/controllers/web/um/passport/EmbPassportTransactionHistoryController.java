package tech.ipocket.pocket.controllers.web.um.passport;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.passport.EmbPassportTransactionHistoryRequest;
import tech.ipocket.pocket.request.um.passport.EmbPassportTrxReportRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.transaction.*;
import tech.ipocket.pocket.services.um.passport.EmbPassportTransactionHistoryServiceImpl;
import tech.ipocket.pocket.services.um.passport.EmbassyPassportService;
import tech.ipocket.pocket.services.um.passport.EmbassyPassportTransactionService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "embassy-passport")
public class EmbPassportTransactionHistoryController extends BaseEndpoint {
    @Autowired
    private RequestValidator requestValidator;
    @Autowired
    private EmbassyPassportService embassyPassportService;
    @Autowired
    private EmbassyPassportTransactionService embassyPassportTransactionService;
    @Autowired
    private FundTransferService fundTransferService;
    @Autowired
    private MobileTopUpService mobileTopUpService;
    @Autowired
    private CashInOutService cashInOutService;
    @Autowired
    private CashOutService cashOutService;

    @Autowired
    private MerchantPaymentService merchantPaymentService;
    @Autowired
    private Environment environment;

    @Autowired
    private EmbPassportTransactionHistoryServiceImpl embPassportTransactionHistoryService;

    // Mobile
    @PostMapping(value = "/passportFeeTrxHistoryReport")
    public @ResponseBody ResponseEntity<BaseResponseObject> passportFeeTrxHistoryReport_Tested(@RequestBody EmbPassportTransactionHistoryRequest request) throws PocketException {
        request = (EmbPassportTransactionHistoryRequest) EncryptDecrypt.decryptRequest(request, EmbPassportTransactionHistoryRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.trxHistoryReport(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    // Web Portal
    @PostMapping(value = "/passportFeeTrxHistoryReportList")
    public @ResponseBody ResponseEntity<BaseResponseObject> trxHistoryReportList_Tested(@RequestBody EmptyRequest request) throws PocketException {
        request = (EmptyRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.trxHistoryReportList(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByDateRange")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByDateRange_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByDateRange(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByToken")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByToken_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByToken(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByQrCodeId")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByQrCodeId_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByQrCodeId(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByPType")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByPType_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByPType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportBySubType")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportBySubType_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportBySubType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByPassportNumber")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByPassportNumber_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByPassportId(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByPayMode")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByPayMode_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByPayMode(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByWalletNo")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByWalletNo_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByWalletNo(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchTrxHistoryReportByEmirateId")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchTrxHistoryReportByEmirateId_Tested(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.searchTrxHistoryReportByEmirateId(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    // load services
    @PostMapping(value = "/loadAllTokenByTransactionType")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllTokenByTransactionType(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllTokenByTransactionType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllUniqueQrCode")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllUniqueQrCode(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllUniqueQrCode(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllPassportType")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllPassportType(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllPassportType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllPassportSubType")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllPassportSubType(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllPassportSubType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllMrpPassport")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllMrpPassport(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllMrpPassport(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllPayMode")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllPayMode(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllPayMode(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllSenderWallet")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllSenderWallet(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllSenderWallet(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/loadAllEmirateID")
    public @ResponseBody ResponseEntity<BaseResponseObject> loadAllEmirateID(@RequestBody EmbPassportTrxReportRequest request) throws PocketException {
        request = (EmbPassportTrxReportRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embPassportTransactionHistoryService.loadAllEmirateID(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
