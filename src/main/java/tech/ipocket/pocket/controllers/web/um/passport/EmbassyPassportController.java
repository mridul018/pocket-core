package tech.ipocket.pocket.controllers.web.um.passport;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeEmptyRequest;
import tech.ipocket.pocket.request.um.passport.UmEmbPassportTypeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.um.passport.EmbassyPassportService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "embassy-passport")
public class EmbassyPassportController extends BaseEndpoint {
    @Autowired
    private RequestValidator requestValidator;
    @Autowired
    private EmbassyPassportService embassyPassportService;


    @PostMapping(value = "/createEmbassyPassportType")
    public @ResponseBody ResponseEntity<BaseResponseObject> createEmbassyPassport(@RequestBody UmEmbPassportTypeRequest request) throws PocketException {
        request = (UmEmbPassportTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportService.createPassportType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/embassyPassportTypeList")
    public @ResponseBody ResponseEntity<BaseResponseObject> createEmbassyPassportList(@RequestBody UmEmbPassportTypeEmptyRequest request) throws PocketException {
        request = (UmEmbPassportTypeEmptyRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportTypeEmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportService.createEmbassyPassportList(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/embassyPassportTypeById")
    public @ResponseBody ResponseEntity<BaseResponseObject> passportTypeById(@RequestBody UmEmbPassportTypeRequest request) throws PocketException {
        request = (UmEmbPassportTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportService.passportTypeById(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/updateEmbassyPassportType")
    public @ResponseBody ResponseEntity<BaseResponseObject> updateEmbassyPassportType(@RequestBody UmEmbPassportTypeRequest request) throws PocketException {
        request = (UmEmbPassportTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportService.updatePassportType(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/searchPassportTypeAndStatus")
    public @ResponseBody ResponseEntity<BaseResponseObject> searchPassportTypeAndStatus(@RequestBody UmEmbPassportTypeRequest request) throws PocketException {
        request = (UmEmbPassportTypeRequest) EncryptDecrypt.decryptRequest(request, UmEmbPassportTypeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        baseResponseObject = embassyPassportService.searchPassportTypeAndStatus(request, tsSessionObject);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
