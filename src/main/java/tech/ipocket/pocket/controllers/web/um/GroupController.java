package tech.ipocket.pocket.controllers.web.um;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.entity.UmUserGroup;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.web.GroupRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.web.GroupAndFunctionMapResponse;
import tech.ipocket.pocket.response.web.GroupCreateOrUpdateResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.web.group.GroupService;
import tech.ipocket.pocket.utils.*;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class GroupController extends BaseEndpoint {

    @Autowired
    private GroupService groupService;

    @Autowired
    private SecurityService securityService;


    @ApiOperation(value = "This api is working about group create, update and delete. All are post method. You " +
            "have to set all field to do this operation")
    @PostMapping(value = "/addUpdateDeleteGroup")
    public ResponseEntity<BaseResponseObject> groupActivity(@RequestBody GroupRequest request) throws PocketException {

        request = (GroupRequest) EncryptDecrypt.decryptRequest(request, GroupRequest.class);
        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        GroupCreateOrUpdateResponse groupCreateOrUpdateResponse = groupService.createOrUpdateOrDeleteGroup(request);
        if (groupCreateOrUpdateResponse == null) {
            throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
        }

        response.setStatus(PocketConstants.OK);
        response.setError(null);
        response.setData(groupCreateOrUpdateResponse.getMessage());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about group list. All group list will get provided" +
            " in this api")
    @PostMapping(value = "/group_list")
    public ResponseEntity<BaseResponseObject> groupList(@RequestBody EmptyRequest request) throws PocketException {

        request = (EmptyRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject response = new BaseResponseObject();


        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        List<GroupAndFunctionMapResponse> mapResponse = groupService.getAllGroupsWithFunctions();

        response.setData(mapResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "getCommonUserRegistrationGroup is for other users registration")
    @PostMapping(value = "/getCommonUserRegistrationGroup")
    public ResponseEntity<BaseResponseObject> getCommonUserRegistrationGroup(@RequestBody EmptyRequest request) throws PocketException {

        request = (EmptyRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject response = new BaseResponseObject();

        List<UmUserGroup> mapResponse = groupService.getCommonUserRegistrationGroup();

        response.setData(mapResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private boolean tokenVerify(BaseRequestObject request) throws PocketException {
        TokenValidateUmResponse tokenValidateUmResponse =
                securityService.validateToken(request);
        if (tokenValidateUmResponse != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

}
