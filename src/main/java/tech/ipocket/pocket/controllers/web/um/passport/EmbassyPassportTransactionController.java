package tech.ipocket.pocket.controllers.web.um.passport;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.request.um.passport.EmbassyPassportTransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.transaction.*;
import tech.ipocket.pocket.services.um.passport.EmbassyPassportTransactionService;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "embassy-passport")
public class EmbassyPassportTransactionController extends BaseEndpoint {
    @Autowired
    private RequestValidator requestValidator;
    @Autowired
    private EmbassyPassportTransactionService embassyPassportTransactionService;
    @Autowired
    private FundTransferService fundTransferService;
    @Autowired
    private MobileTopUpService mobileTopUpService;
    @Autowired
    private CashInOutService cashInOutService;
    @Autowired
    private CashOutService cashOutService;
    @Autowired
    private MerchantPaymentService merchantPaymentService;
    @Autowired
    private Environment environment;

    EmbassyPassportTransactionRequest embassyPassportTransactionRequest = new EmbassyPassportTransactionRequest();

    @PostMapping(value = "/createPassportFeeTransaction")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createPassportFeeTransaction_Tested(@RequestBody EmbassyPassportTransactionRequest request) throws Exception {

        request = (EmbassyPassportTransactionRequest) EncryptDecrypt.decryptRequest(request, EmbassyPassportTransactionRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        SessionCheckWithCredentialRequest requestWithPin = createRequest(request);

        TsSessionObject tsSessionObject = requestValidator.validateSessionWithCredential(requestWithPin);

        switch (request.getTransactionType()) {
            case ServiceCodeConstants.CashInToEmbassyForPassport:
                baseResponseObject = embassyPassportTransactionService.doPassportFeePayment(request, tsSessionObject);
                break;
            default:
                throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    private SessionCheckWithCredentialRequest createRequest(EmbassyPassportTransactionRequest request) {

        SessionCheckWithCredentialRequest withCredentialRequest=new SessionCheckWithCredentialRequest();
        withCredentialRequest.setCredential(request.getPin());
        withCredentialRequest.setOtpText(request.getOtpText());
        withCredentialRequest.setOtpSecret(request.getOtpSecret());
        withCredentialRequest.setHardwareSignature(request.getHardwareSignature());
        withCredentialRequest.setSessionToken(request.getSessionToken());
        withCredentialRequest.setRequestId(request.getRequestId());

        return withCredentialRequest;
    }

}
