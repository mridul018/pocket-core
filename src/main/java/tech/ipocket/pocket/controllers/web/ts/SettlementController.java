package tech.ipocket.pocket.controllers.web.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.settlement.AdminCashInToCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.AdminCashOutFromCustomerRequest;
import tech.ipocket.pocket.request.ts.settlement.FundMovementWalletToWalletRequest;
import tech.ipocket.pocket.request.ts.settlement.MasterDepositFromBankGlRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.settlement.SettlementService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class SettlementController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private SettlementService settlementService;

    @PostMapping(value = "/depositToMasterFromBankGl")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> depositToMasterFromBankGl(@RequestBody MasterDepositFromBankGlRequest request) throws PocketException {
        request = (MasterDepositFromBankGlRequest) EncryptDecrypt.decryptRequest(request, MasterDepositFromBankGlRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        requestValidator.validateSession(request);

        return new ResponseEntity<>(settlementService.depositToMasterFromBankGl(request), HttpStatus.OK);

    }

    @PostMapping(value = "/adminCashInToCustomer")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> adminCashInToCustomer(@RequestBody AdminCashInToCustomerRequest request) throws PocketException {
        request = (AdminCashInToCustomerRequest) EncryptDecrypt.decryptRequest(request, AdminCashInToCustomerRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        requestValidator.validateSession(request);

        return new ResponseEntity<>(settlementService.adminCashInToCustomer(request), HttpStatus.OK);

    }


    @PostMapping(value = "/adminCashOutFromCustomer")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> adminCashOutFromCustomer(@RequestBody AdminCashOutFromCustomerRequest request) throws PocketException {
        request = (AdminCashOutFromCustomerRequest) EncryptDecrypt.decryptRequest(request, AdminCashOutFromCustomerRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        requestValidator.validateSession(request);

        return new ResponseEntity<>(settlementService.adminCashOutFromCustomer(request), HttpStatus.OK);

    }

    @PostMapping(value = "/fundMovement")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> fundMovement(@RequestBody FundMovementWalletToWalletRequest request) throws PocketException {
        request = (FundMovementWalletToWalletRequest) EncryptDecrypt.decryptRequest(request, FundMovementWalletToWalletRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        requestValidator.validateSession(request);

        switch (request.getFundMovementType()){
            case PocketConstants.WALLET_TO_WALLET_FM:
                return new ResponseEntity<>(settlementService.fundMovementWalletToWallet(request), HttpStatus.OK);

            case PocketConstants.WALLET_TO_GL_FM:
            case PocketConstants.GL_TO_WALLET_FM:
            case PocketConstants.GL_TO_GL_FM:
                baseResponseObject=new BaseResponseObject(request.getRequestId());
                baseResponseObject.setError(new ErrorObject("400","Not integrated YET this type"));
                baseResponseObject.setData(null);
                baseResponseObject.setStatus(PocketConstants.ERROR);
                return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);

            default:

            baseResponseObject=new BaseResponseObject(request.getRequestId());
            baseResponseObject.setError(new ErrorObject("400","Invalid fund movement type"));
            baseResponseObject.setData(null);
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        }
    }

}
