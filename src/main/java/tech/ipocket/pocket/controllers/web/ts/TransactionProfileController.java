package tech.ipocket.pocket.controllers.web.ts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.transactionProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.transactionProfile.TransactionProfileService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = {"web","transactionProfile"})
public class TransactionProfileController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private TransactionProfileService transactionProfileService;

    @PostMapping(value = "/createLimit")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createLimit(@RequestBody CreateLimitRequest request)
            throws Exception {

        request = (CreateLimitRequest) EncryptDecrypt.decryptRequest(request, CreateLimitRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.createLimit(request), HttpStatus.OK);
    }
    @PostMapping(value = "/updateLimit")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateLimit(@RequestBody CreateLimitRequest request)
            throws Exception {

        request = (CreateLimitRequest) EncryptDecrypt.decryptRequest(request, CreateLimitRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.updateLimit(request), HttpStatus.OK);
    }

    @ApiOperation("if limit code profided then it will return that limit only otherwise all limit")
    @PostMapping(value = "/getLimit")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getLimit(@RequestBody GetLimitRequest request)
            throws Exception {
        request = (GetLimitRequest) EncryptDecrypt.decryptRequest(request, GetLimitRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.getLimit(request), HttpStatus.OK);
    }

    @ApiOperation("if limit code profided then it will return that limit only otherwise all limit")
    @PostMapping(value = "/getTransactionLimitByTxProfileCode")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getTransactionLimitByTxProfileCode(@RequestBody GetLimitRequest request)
            throws Exception {
        request = (GetLimitRequest) EncryptDecrypt.decryptRequest(request, GetLimitRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.getTransactionLimitByTxProfileCode(request), HttpStatus.OK);
    }


    @PostMapping(value = "/createTransactionProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createTransactionProfile(@RequestBody CreateTransactionProfileRequest request)
            throws Exception {
        request = (CreateTransactionProfileRequest) EncryptDecrypt.decryptRequest(request, CreateTransactionProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.createTransactionProfile(request), HttpStatus.OK);
    }

    @PostMapping(value = "/updateTransactionProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateTransactionProfile(@RequestBody CreateTransactionProfileRequest request)
            throws Exception {
        request = (CreateTransactionProfileRequest) EncryptDecrypt.decryptRequest(request, CreateTransactionProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.updateTransactionProfile(request), HttpStatus.OK);
    }

    @PostMapping(value = "/getTransactionProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getTransactionProfile(@RequestBody GetTransactionProfileRequest request)
            throws Exception {
        request = (GetTransactionProfileRequest) EncryptDecrypt.decryptRequest(request, GetTransactionProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.getTransactionProfile(request), HttpStatus.OK);
    }

    @PostMapping(value = "/mapTransactionProfileServiceLimit")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> mapTransactionProfileServiceLimit(@RequestBody CreateTransactionProfileLimitServiceMapRequest request)
            throws Exception {
        request = (CreateTransactionProfileLimitServiceMapRequest) EncryptDecrypt.decryptRequest(request, CreateTransactionProfileLimitServiceMapRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.mapTransactionProfileServiceLimit(request), HttpStatus.OK);
    }

    @PostMapping(value = "/getServiceLimitByTxProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getServiceLimitByTxProfile(@RequestBody GetServiceLimitByTxProfileRequest request)
            throws Exception {
        request = (GetServiceLimitByTxProfileRequest) EncryptDecrypt.decryptRequest(request, GetServiceLimitByTxProfileRequest.class);
        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());

        validateRequest(request,baseResponseObject);

        TsSessionObject tsSessionObject=requestValidator.validateSession(request);

        return new ResponseEntity<>(transactionProfileService.getServiceLimitByTxProfile(request), HttpStatus.OK);
    }

}
