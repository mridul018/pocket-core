package tech.ipocket.pocket.controllers.web.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.ts.reporting.GetStateWiseUserRequest;
import tech.ipocket.pocket.request.ts.reporting.GetUserListBySrWalletRequest;
import tech.ipocket.pocket.request.ts.reporting.TransactionHistoryRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.reporting.ReportService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class UmReportController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private ReportService reportService;


    @PostMapping("getStateWiseUser")
    public ResponseEntity<BaseResponseObject> getStateWiseUser(@RequestBody GetStateWiseUserRequest transactionHistoryRequest) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);


        TsSessionObject umSessionResponse = null;//requestValidator.validateSession(transactionHistoryRequest);

        baseResponseObject = reportService.getStateWiseUser(transactionHistoryRequest,umSessionResponse);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping("getUserListBySrWallet")
    public ResponseEntity<BaseResponseObject> getUserListBySrWallet(@RequestBody GetUserListBySrWalletRequest srWalletRequest) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(srWalletRequest.getRequestId());
        validateRequest(srWalletRequest, baseResponseObject);


        TsSessionObject umSessionResponse = null;//requestValidator.validateSession(transactionHistoryRequest);

        baseResponseObject = reportService.getUserListBySrWallet(srWalletRequest,umSessionResponse);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

}
