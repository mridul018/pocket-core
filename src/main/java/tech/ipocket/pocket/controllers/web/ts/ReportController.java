package tech.ipocket.pocket.controllers.web.ts;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.ts.AccountStatementRequest;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.GetAggregateBalanceRequest;
import tech.ipocket.pocket.request.ts.TransactionDashboardBalanceRequest;
import tech.ipocket.pocket.request.ts.reporting.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.reporting.TransactionHistoryModel;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.reporting.ReportService;
import tech.ipocket.pocket.services.ts.transaction.MobileTopUpService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class ReportController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private ReportService reportService;
    @Autowired
    private MobileTopUpService topUpService;


    @PostMapping("accountstatement")
    public ResponseEntity<BaseResponseObject> getAccountStatement(@RequestBody AccountStatementRequest accountStatementRequest) throws  PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(accountStatementRequest.getRequestId());
        validateRequest(accountStatementRequest, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(accountStatementRequest);

        baseResponseObject = reportService.getAccountStatement(accountStatementRequest, tsSessionObject, Sort.Direction.ASC);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }


    @PostMapping("getDashboardAggregateBalance")
    public ResponseEntity<BaseResponseObject> getDashboardAggregateBalance(@RequestBody GetAggregateBalanceRequest accountStatementRequest) throws  PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(accountStatementRequest.getRequestId());
        validateRequest(accountStatementRequest, baseResponseObject);

        TsSessionObject tsSessionObject =null;//requestValidator.validateSession(accountStatementRequest);

        baseResponseObject = reportService.getDashboardAggregateBalance(accountStatementRequest, tsSessionObject);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }

    @PostMapping("getTransactionDashboardBalance")
    public ResponseEntity<BaseResponseObject> getTransactionDashboardBalance(@RequestBody TransactionDashboardBalanceRequest accountStatementRequest) throws  PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(accountStatementRequest.getRequestId());
        validateRequest(accountStatementRequest, baseResponseObject);

        TsSessionObject tsSessionObject = null;//requestValidator.validateSession(accountStatementRequest);

        baseResponseObject = reportService.getTransactionDashboardBalance(accountStatementRequest, tsSessionObject);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }

    @PostMapping("getSummaryOfTransactions")
    public ResponseEntity<BaseResponseObject> getSummaryOfTransactions(@RequestBody GetSummeryOfTransactionRequest getSummeryOfTransactionRequest) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(getSummeryOfTransactionRequest.getRequestId());
        validateRequest(getSummeryOfTransactionRequest, baseResponseObject);

        TsSessionObject umSessionResponse = requestValidator.validateSession(getSummeryOfTransactionRequest);

        baseResponseObject = reportService.getSummaryOfTransactions(getSummeryOfTransactionRequest, umSessionResponse);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping("getTransactionSummeryDetails")
    public ResponseEntity<BaseResponseObject> getTransactionSummeryDetails(@RequestBody GetTransactionSummeryDetailsRequest transactionHistoryRequest) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);

        TsSessionObject umSessionResponse = requestValidator.validateSession(transactionHistoryRequest);


        transactionHistoryRequest.setMobileNumber(umSessionResponse.getMobileNumber());

        List<TransactionHistoryModel> response = reportService.getTransactionSummeryDetails(transactionHistoryRequest, umSessionResponse);

        baseResponseObject.setError(null);
        baseResponseObject.setData(response);
        baseResponseObject.setStatus(PocketConstants.OK);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }


    @PostMapping("transactionQuery")
    public ResponseEntity<BaseResponseObject> transactionQuery(@RequestBody TransactionHistoryRequest transactionHistoryRequest) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);


        TsSessionObject umSessionResponse = null;//requestValidator.validateSession(transactionHistoryRequest);

        baseResponseObject = reportService.transactionQuery(transactionHistoryRequest,umSessionResponse);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping("getCustomerTransactionLimit")
    public ResponseEntity<BaseResponseObject> getCustomerTransactionLimit(@RequestBody GetCustomerLimitRequest transactionHistoryRequest) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);

        TsSessionObject umSessionResponse = requestValidator.validateSession(transactionHistoryRequest);

        if(transactionHistoryRequest.getMobileNumber()==null){
            transactionHistoryRequest.setMobileNumber(umSessionResponse.getMobileNumber());
        }


        baseResponseObject = reportService.getCustomerTransactionLimit(transactionHistoryRequest,transactionHistoryRequest.getMobileNumber());

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping("getTopUpReport")
    public ResponseEntity<BaseResponseObject> getTopUpReport(@RequestBody TopUpReportRequest transactionHistoryRequest) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);

//        TsSessionObject umSessionResponse = requestValidator.validateSession(transactionHistoryRequest);
//
//        if(transactionHistoryRequest.getMobileNumber()==null){
//            transactionHistoryRequest.setMobileNumber(umSessionResponse.getMobileNumber());
//        }


        baseResponseObject = reportService.getTopUpReport(transactionHistoryRequest);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping("filterTopUpReport")
    public ResponseEntity<BaseResponseObject> filterTopUpReport(@RequestBody FilterTopUpReportRequest transactionHistoryRequest) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);

//        TsSessionObject umSessionResponse = requestValidator.validateSession(transactionHistoryRequest);
//
//        if(transactionHistoryRequest.getMobileNumber()==null){
//            transactionHistoryRequest.setMobileNumber(umSessionResponse.getMobileNumber());
//        }


        baseResponseObject = reportService.filterTopUpReport(transactionHistoryRequest);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping("getDistinctOperatorByCountryCode")
    public ResponseEntity<BaseResponseObject> getDistinctOperatorByCountryCode(@RequestBody GetDistinctOperatorListRequest transactionHistoryRequest) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(transactionHistoryRequest.getRequestId());
        validateRequest(transactionHistoryRequest, baseResponseObject);

        baseResponseObject = topUpService.getTopUpOperators(transactionHistoryRequest.getCountryCode(),transactionHistoryRequest.getRequestId());

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }


    @PostMapping("getGlList")
    public ResponseEntity<BaseResponseObject> getGlList(@RequestBody EmptyRequest
                                                                         request) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

//        TsSessionObject umSessionResponse = requestValidator.validateSession(transactionHistoryRequest);
//
//        if(transactionHistoryRequest.getMobileNumber()==null){
//            transactionHistoryRequest.setMobileNumber(umSessionResponse.getMobileNumber());
//        }


        baseResponseObject = reportService.getGlList(request);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }


    @PostMapping("getGlStatement")
    public ResponseEntity<BaseResponseObject> getGlStatement(@RequestBody GetGlStatementRequest
                                                                         request) throws  PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

//        TsSessionObject umSessionResponse = requestValidator.validateSession(transactionHistoryRequest);
//
//        if(transactionHistoryRequest.getMobileNumber()==null){
//            transactionHistoryRequest.setMobileNumber(umSessionResponse.getMobileNumber());
//        }


        baseResponseObject = reportService.getGlStatement(request);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

}
