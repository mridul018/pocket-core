package tech.ipocket.pocket.controllers.web.ts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.FeeShareRequest;
import tech.ipocket.pocket.request.ts.feeProfile.CreateStakeHolder;
import tech.ipocket.pocket.request.ts.fee_share.GetSharedFeeRequest;
import tech.ipocket.pocket.request.ts.fee_share.ShareConversionRateRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.ts.feeShairing.MfsFeeSharingService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class FeeSharingController extends BaseEndpoint {

    @Autowired
    private MfsFeeSharingService mfsFeeSharingService;

    @ApiOperation(value = "Share fee for individual transaction")
    @PostMapping(value = "/shareIndividualTransactionFee")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> shareIndividualTransactionFee(@RequestBody FeeShareRequest request) throws Exception {
        request = (FeeShareRequest) EncryptDecrypt.decryptRequest(request, CreateStakeHolder.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(mfsFeeSharingService.doFeeShareForIndividualTransaction(request.getTransactionReference(),request.getRequestId()), HttpStatus.OK);
    }



    @ApiOperation(value = "Get Shared fee")
    @PostMapping(value = "/getSharedFee")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getSharedFee(@RequestBody GetSharedFeeRequest request) throws Exception {
        request = (GetSharedFeeRequest) EncryptDecrypt.decryptRequest(request, GetSharedFeeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(mfsFeeSharingService.getSharedFee(request), HttpStatus.OK);
    }



    @ApiOperation(value = "Share conversion rate fee")
    @PostMapping(value = "/shareConversionRate")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> shareConversionProfitForIndividualTransaction(@RequestBody ShareConversionRateRequest request) throws Exception {
        request = (ShareConversionRateRequest) EncryptDecrypt.decryptRequest(request, ShareConversionRateRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(mfsFeeSharingService.shareConversionProfitForIndividualTransaction(request.getSharedAmount(),
                request.getMainTransactionToken(),
                request.getRequestId()), HttpStatus.OK);
    }
}
