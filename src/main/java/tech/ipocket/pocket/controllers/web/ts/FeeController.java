package tech.ipocket.pocket.controllers.web.ts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.feeProfile.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.feeProfile.FeeProfileService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class FeeController extends BaseEndpoint {


    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private FeeProfileService feeProfileService;

    @ApiOperation(value = "This api is about to create fee profile when transaction will be occur")
    @PostMapping(value = "/createFeeProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createFeeProfile(@RequestBody CreateFeeProfileRequest request) throws Exception {

        request = (CreateFeeProfileRequest) EncryptDecrypt.decryptRequest(request, CreateFeeProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);


        return new ResponseEntity<>(feeProfileService.createFeeProfile(request), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update fee profile when transaction will be occur")
    @PostMapping(value = "/updateFeeProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateFeeProfile(@RequestBody CreateFeeProfileRequest request) throws Exception {

        request = (CreateFeeProfileRequest) EncryptDecrypt.decryptRequest(request, CreateFeeProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);


        return new ResponseEntity<>(feeProfileService.updateFeeProfile(request), HttpStatus.OK);
    }


    @ApiOperation(value = "This api is about to get or show fee profile")
    @PostMapping(value = "/getFeeProfile")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getFeeProfile(@RequestBody GetFeeProfileRequest request) throws Exception {
        request = (GetFeeProfileRequest) EncryptDecrypt.decryptRequest(request, GetFeeProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);


        return new ResponseEntity<>(feeProfileService.getFeeProfile(request), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get or show fee profile and fee map")
    @PostMapping(value = "/getFeeProfileFeeList")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getFeeProfileFeeList(@RequestBody GetFeeProfileRequest request) throws Exception {
        request = (GetFeeProfileRequest) EncryptDecrypt.decryptRequest(request, GetFeeProfileRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);


        return new ResponseEntity<>(feeProfileService.getFeeProfileFeeList(request), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to create fee for transaction")
    @PostMapping(value = "/createFee")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createFee(@RequestBody CreateFeeRequest request) throws Exception {
        request = (CreateFeeRequest) EncryptDecrypt.decryptRequest(request, CreateFeeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);


        return new ResponseEntity<>(feeProfileService.createFee(request), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to show or get fee of transaction")
    @PostMapping(value = "/getFee")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getFee(@RequestBody GetFeeRequest request) throws Exception {
        request = (GetFeeRequest) EncryptDecrypt.decryptRequest(request, GetFeeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(feeProfileService.getFee(request), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to show or get fee of transaction")
    @PostMapping(value = "/updateFee")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateFee(@RequestBody CreateFeeRequest request) throws Exception {
        request = (CreateFeeRequest) EncryptDecrypt.decryptRequest(request, CreateFeeRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(feeProfileService.updateFee(request), HttpStatus.OK);
    }


    @ApiOperation(value = "This api is about to create a new stack holder")
    @PostMapping(value = "/createStakeHolder")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createStakeHolder(@RequestBody CreateStakeHolder request) throws Exception {
        request = (CreateStakeHolder) EncryptDecrypt.decryptRequest(request, CreateStakeHolder.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(feeProfileService.createStakeHolder(request), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get or show all stake holder")
    @PostMapping(value = "/getStakeHolder")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getStakeHolder(@RequestBody GetStakeHolderRequest request) throws Exception {
        request = (GetStakeHolderRequest) EncryptDecrypt.decryptRequest(request, GetStakeHolderRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(feeProfileService.getStakeHolder(request), HttpStatus.OK);
    }

    @PostMapping(value = "/createFeeFeeProfileServiceMap")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createFeeFeeProfileServiceMap(@RequestBody CreateFeeProfileLimitMapRequest request) throws Exception {
        request = (CreateFeeProfileLimitMapRequest) EncryptDecrypt.decryptRequest(request, CreateFeeProfileLimitMapRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(feeProfileService.createFeeFeeProfileServiceMap(request), HttpStatus.OK);
    }

    @PostMapping(value = "/getConversionRate")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getConversionRate(@RequestBody GetConversionRate request) throws Exception {
        request = (GetConversionRate) EncryptDecrypt.decryptRequest(request, GetConversionRate.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(feeProfileService.getConversionRate(request), HttpStatus.OK);
    }

    @PostMapping(value = "/createOrUpdateConversionRate")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createOrUpdateConversionRate(@RequestBody CreateConversionRate request) throws Exception {
        request = (CreateConversionRate) EncryptDecrypt.decryptRequest(request, CreateFeeProfileLimitMapRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(feeProfileService.createOrUpdateConversionRate(request), HttpStatus.OK);
    }
}
