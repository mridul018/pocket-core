package tech.ipocket.pocket.controllers.web.ts;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.ts.irregular_transaction.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.irregular_transaction.IrregularTransactionService;
import tech.ipocket.pocket.services.ts.irregular_transaction.TsDisputeService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "irregular transactions")
public class IrregularTransactionController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private IrregularTransactionService irregularTransactionService;
    @Autowired
    private TsDisputeService createDisputeService;


    @PostMapping("createRefund")
    public ResponseEntity<BaseResponseObject> createRefund(@RequestBody RefundRequest refundRequest) throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(refundRequest.getRequestId());
        validateRequest(refundRequest, baseResponseObject);

//        requestValidator.validateSessionToken(refundRequest);

        baseResponseObject = irregularTransactionService.createRefund(refundRequest);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);

    }


    @PostMapping("createReversal")
    public ResponseEntity<BaseResponseObject> createReversalWithOutSessionCheck(@RequestBody ReversalRequest reversalRequest)
            throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(reversalRequest.getRequestId());
        validateRequest(reversalRequest, baseResponseObject);


        baseResponseObject = irregularTransactionService.createReversal(reversalRequest);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }

    @PostMapping("createDispute")
    public ResponseEntity<BaseResponseObject> createDispute(@RequestBody CreateDisputeRequest createDisputeRequest)
            throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(createDisputeRequest.getRequestId());
        validateRequest(createDisputeRequest, baseResponseObject);

        baseResponseObject = createDisputeService.createDispute(createDisputeRequest);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }

    @PostMapping("updateDispute")
    public ResponseEntity<BaseResponseObject> updateDispute(@RequestBody UpdateDisputeRequest createDisputeRequest)
            throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(createDisputeRequest.getRequestId());
        validateRequest(createDisputeRequest, baseResponseObject);

        baseResponseObject = createDisputeService.updateDispute(createDisputeRequest);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }

    @PostMapping("getDisputes")
    public ResponseEntity<BaseResponseObject> getDisputes(@RequestBody GetDisputeRequest createDisputeRequest)
            throws PocketException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(createDisputeRequest.getRequestId());
        validateRequest(createDisputeRequest, baseResponseObject);

        baseResponseObject = createDisputeService.findAllDisputeByStatus(createDisputeRequest);

        if (baseResponseObject == null || !baseResponseObject.getStatus().equals(PocketConstants.OK)) {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }
}
