package tech.ipocket.pocket.controllers.web.um;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.*;
import tech.ipocket.pocket.request.web.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.web.CustomerResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.ProfileService;
import tech.ipocket.pocket.services.web.user.UserService;
import tech.ipocket.pocket.utils.*;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class UserController extends BaseEndpoint {

    @Autowired
    private UserService userService;
    @Autowired
    private SecurityService securityService;

    @Autowired
    private ProfileService profileService;

    @ApiOperation(value = "This api is about to get user info by loginId or mobile number")
    @PostMapping(value = "/getUserInfo")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserInfo(@RequestBody GetUserInfoRequest request) throws PocketException {

        request = (GetUserInfoRequest) EncryptDecrypt.decryptRequest(request, GetUserInfoRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = profileService.getUserInfo(request);

        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        baseResponseObject.setData(validateResponse);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get user info by loginId or mobile number")
    @PostMapping(value = "/getUserDetails")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserDetails(@RequestBody GetUserInfoRequest request) throws PocketException {
        request = (GetUserInfoRequest) EncryptDecrypt.decryptRequest(request, GetUserInfoRequest.class);
        BaseResponseObject response = new BaseResponseObject(request.getRequestId());

        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = profileService.getUserDetails(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get all users. In this " +
            " request you have to proved your page number and size of data " +
            "that you want to show")
    @PostMapping(value = "/listOfAllUser")
    public ResponseEntity<BaseResponseObject> listOfAllUser(@RequestBody UserListRequest request) throws PocketException {
        request = (UserListRequest) EncryptDecrypt.decryptRequest(request, UserListRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.getUserList(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to search any user using mobile number or " +
            "email address.")
    @PostMapping(value = "/searchUser")
    public ResponseEntity<BaseResponseObject> userSearch(@RequestBody UserSearchRequest request) throws PocketException {
        request = (UserSearchRequest) EncryptDecrypt.decryptRequest(request, UserSearchRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.searchUser(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update user")
    @PostMapping(value = "/editUser")
    public ResponseEntity<BaseResponseObject> userEdit(@RequestBody UserEditRequest request) throws PocketException {
        request = (UserEditRequest) EncryptDecrypt.decryptRequest(request, UserEditRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        /*if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }*/

        response = userService.editUser(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to change login credentials.")
    @PostMapping(value = "/changeCredential")
    public ResponseEntity<BaseResponseObject> changeCredential(@RequestBody ChangeCredentialRequest request) throws PocketException {
        request = (ChangeCredentialRequest) EncryptDecrypt.decryptRequest(request, ChangeCredentialRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        response = userService.changeCredential(request, validateResponse);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to reset PIN/Password")
    @PostMapping(value = "/pinOrPasswordReset")
    public ResponseEntity<BaseResponseObject> pinOrPasswordReset(@RequestBody UserPinPasswordResetRequest request) throws PocketException {
        request = (UserPinPasswordResetRequest) EncryptDecrypt.decryptRequest(request, UserPinPasswordResetRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.pinReset(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to lock or unlock user wallet status.")
    @PostMapping(value = "/lockOrUnlockWallet")
    public ResponseEntity<BaseResponseObject> lockOrUnlockWallet(@RequestBody UserLockUnlockWalletRequest request) throws PocketException {
        request = (UserLockUnlockWalletRequest) EncryptDecrypt.decryptRequest(request, UserLockUnlockWalletRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.localUnlockWallet(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to verify user primary identification number, issue date and expire date")
    @PostMapping(value = "/verifyPrimaryInformation")
    public ResponseEntity<BaseResponseObject> verifyPrimaryIdInfo(@RequestBody UserPrimaryIdInfoVerificationRequest request) throws PocketException {
        request = (UserPrimaryIdInfoVerificationRequest) EncryptDecrypt.decryptRequest(request, UserPrimaryIdInfoVerificationRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.verifyPrimaryIdInfo(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get user details with wallet information")
    @PostMapping(value = "/userDetailsWithWallet")
    public ResponseEntity<BaseResponseObject> userDetailsWithWallet(@RequestBody UserDetailsWithWalletRequest request) throws PocketException {
        request = (UserDetailsWithWalletRequest) EncryptDecrypt.decryptRequest(request, UserDetailsWithWalletRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.userDetailsWithWallet(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to show all device information which is being used in this apps.")
    @PostMapping(value = "/getUserDevices")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserDevices(@RequestBody EmptyRequest request) throws PocketException {
        request = (EmptyRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        baseResponseObject = profileService.getUserDevices(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }



    @ApiOperation(value = "This api is about to update user document")
    @PostMapping(value = "/updateUserDocuments")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateUserDocuments(@RequestBody UpdateDocumentRequest request) throws PocketException {
        request = (UpdateDocumentRequest) EncryptDecrypt.decryptRequest(request, UpdateDocumentRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        if (request.getMobileNumber() == null) {
            request.setMobileNumber(validateResponse.getMobileNumber());
        }

        baseResponseObject = profileService.updateUserDocuments(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    private boolean tokenVerify(BaseRequestObject request) throws PocketException {
        TokenValidateUmResponse tokenValidateUmResponse =
                securityService.validateToken(request);
        if (tokenValidateUmResponse != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @ApiOperation(value = "User account delete by walletNo using super-admin & admin role.")
    @PostMapping(value = "/deleteUser")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> deleteUser(@RequestBody GetUserInfoRequest request) throws PocketException {
        request = (GetUserInfoRequest) EncryptDecrypt.decryptRequest(request, GetUserInfoRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        if (request.getMobileNumber() != null &&
                (validateResponse.getGroupCode().matches(UmEnums.UserGroup.SUPER_ADMIN.getGroupCode()) ||
                 (validateResponse.getGroupCode().matches(UmEnums.UserGroup.ADMIN.getGroupCode())))){
            baseResponseObject = userService.deleteUser(request);
        }
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get all winning customer on UAE event.")
    @PostMapping(value = "/customerList")
    public ResponseEntity<BaseResponseObject> customerList(@RequestBody CustomerRequest request) throws PocketException {
        request = (CustomerRequest) EncryptDecrypt.decryptRequest(request, CustomerRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.getCustomerList(request);

        return new ResponseEntity<BaseResponseObject>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to get customer serial for entry purpose on event.")
    @PostMapping(value = "/customerSerial")
    public ResponseEntity<BaseResponseObject> customerSerial(@RequestBody CustomerSerialRequest request) throws PocketException {
        request = (CustomerSerialRequest) EncryptDecrypt.decryptRequest(request, CustomerSerialRequest.class);
        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        response = userService.getCustomerSerial(request);

        return new ResponseEntity<BaseResponseObject>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about (isChecked Merchant)")
    @PostMapping(value = "/isCheckedMerchant")
    public @ResponseBody ResponseEntity<BaseResponseObject> isCheckedMerchant(@RequestBody GetUserInfoRequest request) throws PocketException {
        request = (GetUserInfoRequest) EncryptDecrypt.decryptRequest(request, GetUserInfoRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        if (request == null || request.getSessionToken() == null || request.getHardwareSignature() == null) {
            baseResponseObject = new BaseResponseObject();
        }
        TokenValidateUmResponse validateResponse = profileService.getUserInfo(request);
        if(validateResponse != null){
            if (validateResponse.getGroupCode().equalsIgnoreCase(TsEnums.UserType.MERCHANT.getUserTypeString())){
                baseResponseObject.setError(null);
                baseResponseObject.setStatus(PocketConstants.OK);
                baseResponseObject.setData(validateResponse);
            }else {
                baseResponseObject.setError(new ErrorObject(PocketErrorCode.MerchantNotFound));
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
            }
        }else{
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MerchantNotFound));
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
        }
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about (isChecked SR/Partner Merchant)")
    @PostMapping(value = "/isCheckedPartnerMerchant")
    public @ResponseBody ResponseEntity<BaseResponseObject> isCheckedPartnerMerchant(@RequestBody GetUserInfoRequest request) throws PocketException {
        request = (GetUserInfoRequest) EncryptDecrypt.decryptRequest(request, GetUserInfoRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        if (request == null || request.getSessionToken() == null || request.getHardwareSignature() == null) {
            baseResponseObject = new BaseResponseObject();
        }
        TokenValidateUmResponse validateResponse = profileService.getUserInfo(request);
        if(validateResponse != null){
            if (validateResponse.getGroupCode().equalsIgnoreCase(TsEnums.UserType.SR.getUserTypeString())){
                baseResponseObject.setError(null);
                baseResponseObject.setStatus(PocketConstants.OK);
                baseResponseObject.setData(validateResponse);
            }else {
                baseResponseObject.setError(new ErrorObject(PocketErrorCode.SrWalletNotFound));
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
            }
        }else{
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.SrWalletNotFound));
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
        }
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
