package tech.ipocket.pocket.controllers.web.um;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.web.FunctionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.web.FunctionListResponse;
import tech.ipocket.pocket.response.web.FunctionResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.web.function.FunctionService;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/v1/web")
@Api(tags = "web")
public class FunctionController extends BaseEndpoint {

    @Autowired
    private FunctionService functionService;
    @Autowired
    private SecurityService securityService;

    @ApiOperation(value = "This api is about function add, update, and delete")
    @PostMapping(value = "/addUpdateDeleteFunction")
    public ResponseEntity<BaseResponseObject> functionActivity(@RequestBody FunctionRequest request) throws PocketException {
        request = (FunctionRequest) EncryptDecrypt.decryptRequest(request, FunctionRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        FunctionResponse functionResponse = functionService.addUpdateDeleteFunction(request);

        response.setData(functionResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);
        response.setRequestId(request.getRequestId());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about list of all function.")
    @PostMapping(value = "/allFunctions")
    public ResponseEntity<BaseResponseObject> allFunctionList(@RequestBody EmptyRequest request) throws PocketException {
        request = (EmptyRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);
        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request, response);

        if (!tokenVerify(request)) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        FunctionListResponse functionListResponse = functionService.listOfAllFunctions();
        if (functionListResponse == null) {
            throw new PocketException(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED);
        }

        response.setData(functionListResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private boolean tokenVerify(BaseRequestObject request) throws PocketException {
        TokenValidateUmResponse tokenValidateUmResponse =
                securityService.validateToken(request);
        if (tokenValidateUmResponse != null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


}
