package tech.ipocket.pocket.controllers.mobile.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.ipocket.pocket.request.um.TransactionSummaryViewRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.TransactionValidityService;
import tech.ipocket.pocket.services.um.transactionSummery.AbstractTransactionSummary;
import tech.ipocket.pocket.services.um.transactionSummery.TransactionSummaryService;
import tech.ipocket.pocket.services.um.transactionSummery.TransactionSummaryUmFactory;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketErrorCode;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.EncryptDecrypt;

import java.util.concurrent.ExecutionException;

@Controller
@RequestMapping(value = "/v1")
@Api(tags = "mobile")
public class UmTransactionValidityController extends BaseEndpoint {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private TransactionValidityService transactionvalidityService;

    @Autowired
    private TransactionSummaryService transactionSummaryService;
    @Autowired
    private Environment environment;


    @PostMapping(value = "/mobile/umCheckTransactionValidity")
    public ResponseEntity<BaseResponseObject> umCheckTransactionValidity(@RequestBody TransactionSummaryViewRequest request) throws PocketException, ExecutionException, InterruptedException {
        request = (TransactionSummaryViewRequest) EncryptDecrypt.decryptRequest(request, TransactionSummaryViewRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        validateRequest(request, baseResponseObject);

//        TokenValidateUmResponse validateResponse=securityService.validateToken(request);

        TransactionSummaryUmFactory transactionSummaryFactory = new TransactionSummaryUmFactory(transactionSummaryService, environment);
        AbstractTransactionSummary abstractTransactionSummary = transactionSummaryFactory.prepareTransactionSummary(request.getPurpose());
        if (abstractTransactionSummary == null) {
            throw new PocketException(PocketErrorCode.InvalidInput);
        }
        BaseResponseObject response = abstractTransactionSummary.getTransactionSummary(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
