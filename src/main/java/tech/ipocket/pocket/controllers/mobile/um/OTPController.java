package tech.ipocket.pocket.controllers.mobile.um;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.um.ResendOtpRequest;
import tech.ipocket.pocket.request.um.SendOtpRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.um.otp.AbstractOtp;
import tech.ipocket.pocket.services.um.otp.CommonOtpService;
import tech.ipocket.pocket.services.um.otp.OtpFactory;
import tech.ipocket.pocket.services.um.otp.OtpService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketException;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(value = "/v1")
@Api(tags = "mobile")
public class OTPController extends BaseEndpoint {


    @Autowired
    private OtpService otpService;

    @Autowired
    private CommonOtpService commonOtpService;
    @Autowired
    private Environment environment;

    @PostMapping(value = "/mobile/resendOtp")
    public ResponseEntity<BaseResponseObject> resendOtp(@RequestBody ResendOtpRequest request) throws PocketException {

        request = (ResendOtpRequest) EncryptDecrypt.decryptRequest(request, ResendOtpRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        validateRequest(request, baseResponseObject);

        baseResponseObject = otpService.resendOtp(request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/mobile/sendOtp")
    public ResponseEntity<BaseResponseObject> sendOtp(@RequestBody SendOtpRequest request) throws PocketException, ExecutionException, InterruptedException {
        request = (SendOtpRequest) EncryptDecrypt.decryptRequest(request, SendOtpRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject();
        validateRequest(request, baseResponseObject);

        // TODO: 2019-05-06 Have to work here

        OtpFactory otpFactory=new OtpFactory(commonOtpService,environment,otpService);
        AbstractOtp abstractOtp =otpFactory.prepareOtpService(request.getPurpose());
        BaseResponseObject response = abstractOtp.sendOtp(request,null);

        return new ResponseEntity<>(response,HttpStatus.OK);
    }
}
