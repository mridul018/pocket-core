package tech.ipocket.pocket.controllers.mobile.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.transaction.*;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/v1/mobile")
@Api(tags = "mobile")
public class TransactionController extends BaseEndpoint {


    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private FundTransferService fundTransferService;
    @Autowired
    private MobileTopUpService mobileTopUpService;

    @Autowired
    private CashInOutService cashInOutService;

    @Autowired
    private CashOutService cashOutService;

    @Autowired
    private MerchantPaymentService merchantPaymentService;
    @Autowired
    private Environment environment;


    @PostMapping(value = "/createTransaction")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createTransaction(@RequestBody TransactionRequest request) throws Exception {

        request = (TransactionRequest) EncryptDecrypt.decryptRequest(request, TransactionRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        SessionCheckWithCredentialRequest requestWithPin=createRequest(request);

        TsSessionObject tsSessionObject =null;

        if(request.getTransactionType().equalsIgnoreCase(ServiceCodeConstants
                .CashInFromBank)){
            tsSessionObject =requestValidator.validateSession(requestWithPin);
        }else{
            tsSessionObject =requestValidator.validateSessionWithCredential(requestWithPin);
        }

//        tsSessionObject=new TsSessionObject();
//        tsSessionObject.setMobileNumber("+8801737186095");

        switch (request.getTransactionType()) {
            case ServiceCodeConstants
                    .Fund_Transfer:
                baseResponseObject = fundTransferService.doFundTransfer(request, tsSessionObject);
                break;
            case ServiceCodeConstants
                    .CashIn:
                baseResponseObject = cashInOutService.doAgentCashIn(request, tsSessionObject);
                break;
            case ServiceCodeConstants
                    .CashInFromBank:
                String isBankCashInOpen=environment.getProperty("IS_BANK_CASH_IN_OPEN");
            if(isBankCashInOpen!=null && isBankCashInOpen.equalsIgnoreCase("false")){
                throw new PocketException(PocketErrorCode.ThisFeatureIsOffNow);
            }else{
                baseResponseObject = cashInOutService.doCashInFromBank(request, tsSessionObject);

            }
                break;

            case ServiceCodeConstants
                    .CashOutToBank:
                baseResponseObject = cashInOutService.doCashOutToBank(request, tsSessionObject);
                break;

            case ServiceCodeConstants
                    .CashOut:
                baseResponseObject = cashOutService.doAgentCashOut(request, tsSessionObject);
                break;
            case ServiceCodeConstants.Top_Up:
                baseResponseObject = mobileTopUpService.doMobileTopUp(request, tsSessionObject);
                break;
            case ServiceCodeConstants.Merchant_Payment:
                baseResponseObject = merchantPaymentService.doMerchantPayment(request, tsSessionObject);
                break;
            default:
                throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    private SessionCheckWithCredentialRequest createRequest(TransactionRequest request) {

        SessionCheckWithCredentialRequest withCredentialRequest=new SessionCheckWithCredentialRequest();
        withCredentialRequest.setCredential(request.getPin());
        withCredentialRequest.setOtpText(request.getOtpText());
        withCredentialRequest.setOtpSecret(request.getOtpSecret());
        withCredentialRequest.setHardwareSignature(request.getHardwareSignature());
        withCredentialRequest.setSessionToken(request.getSessionToken());
        withCredentialRequest.setRequestId(request.getRequestId());

        return withCredentialRequest;
    }

}
