package tech.ipocket.pocket.controllers.mobile.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.request.ts.transaction.BillPayRequest;
import tech.ipocket.pocket.request.ts.transaction.TransactionRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.transaction.*;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.BillPayTypeConstants;

@RestController
@RequestMapping(value = "/v1/mobile")
@Api(tags = "mobile")
public class BillPayController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private BillPayService billPayService;

    @PostMapping(value = "/billPay")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createTransaction(@RequestBody BillPayRequest request) throws PocketException {

        request = (BillPayRequest) EncryptDecrypt.decryptRequest(request, TransactionRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        SessionCheckWithCredentialRequest requestWithPin=createRequest(request);

        TsSessionObject tsSessionObject =requestValidator.validateSessionWithCredential(requestWithPin);

        switch (request.getBillPayType()) {
            case BillPayTypeConstants
                    .PALLI_BIDYUT:
                baseResponseObject = billPayService.doBillPayPallyBiddut(request, tsSessionObject);
                break;
            default:
                throw new PocketException(PocketErrorCode.UnSupportedBillPayType);
        }

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    private SessionCheckWithCredentialRequest createRequest(BillPayRequest request) {

        SessionCheckWithCredentialRequest withCredentialRequest=new SessionCheckWithCredentialRequest();
        withCredentialRequest.setCredential(request.getPin());
        withCredentialRequest.setOtpText(request.getOtpText());
        withCredentialRequest.setOtpSecret(request.getOtpSecret());
        withCredentialRequest.setHardwareSignature(request.getHardwareSignature());
        withCredentialRequest.setSessionToken(request.getSessionToken());
        withCredentialRequest.setRequestId(request.getRequestId());

        return withCredentialRequest;
    }
}
