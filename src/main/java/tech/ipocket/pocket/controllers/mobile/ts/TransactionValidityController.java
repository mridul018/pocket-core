package tech.ipocket.pocket.controllers.mobile.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.transactionValidity.TransactionValidityRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.transactionValidity.TransactionValidityResponse;
import tech.ipocket.pocket.services.ts.transactionValidity.TransactionValidityFactory;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketErrorCode;

@RestController
@RequestMapping(value = "/v1/mobile")
@Api(tags = "mobile")
public class TransactionValidityController extends BaseEndpoint implements ApplicationContextAware {

    private ApplicationContext context;

    @Autowired
    private TransactionValidityFactory transactionValidityFactory;

    @PostMapping(value = "/getTransactionValidity")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getTransactionValidity(@RequestBody TransactionValidityRequest request) throws Exception {

        request = (TransactionValidityRequest) EncryptDecrypt.decryptRequest(request, TransactionValidityRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        TransactionValidityFactory transactionValidityFactory=context.getBean(TransactionValidityFactory.class);

        TransactionValidityResponse transactionValidityResponse = transactionValidityFactory
                .getTransactionvalidity(request);

        baseResponseObject.setData(transactionValidityResponse);

        if (transactionValidityResponse == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.UnspecifiedErrorOccured);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
