package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.account.BalanceCheckRequest;
import tech.ipocket.pocket.request.ts.account.WalletCreateRequest;
import tech.ipocket.pocket.request.ts.account.TsUpdateClientStatusRequest;
import tech.ipocket.pocket.request.um.UpdateFcmKeyRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.ts.RegistrationResponse;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.ts.AccountService;
import tech.ipocket.pocket.services.ts.registration.RegistrationFactory;
import tech.ipocket.pocket.services.um.ProfileService;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "app")
public class AccountController extends BaseEndpoint {

    @Autowired
    private AccountService accountService;

    @Autowired
    private RegistrationFactory registrationFactory;

    @Autowired
    private RequestValidator requestValidator;


    @Autowired
    private SecurityService securityService;

    @Autowired
    private ProfileService profileService;

    @PostMapping(value = "/createWallet")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createWallet(@RequestBody WalletCreateRequest request) throws PocketException {

        request = (WalletCreateRequest) EncryptDecrypt.decryptRequest(request, WalletCreateRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);


        RegistrationResponse response = registrationFactory
                .doRegistration(request, request.getGroupCode());

        if (response == null || response.getStatus() != 200) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping(value = "/checkBalance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkBalance(@RequestBody BalanceCheckRequest request) throws Exception {

        request = (BalanceCheckRequest) EncryptDecrypt.decryptRequest(request, BalanceCheckRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(accountService.checkBalance(tsSessionObject.getMobileNumber(),
                request.getRequestId()), HttpStatus.OK);
    }


    @PostMapping(value = "/updateClientStatus")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateClientStatus(@RequestBody TsUpdateClientStatusRequest request) throws Exception {

        request = (TsUpdateClientStatusRequest) EncryptDecrypt.decryptRequest(request, TsUpdateClientStatusRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(accountService.updateClientStatus(request.getWalletId(), request.getNewStatus(),
                request.getRequestId()), HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update user FCM or cloud messaging key")
    @PostMapping(value = "/updateFcmKey")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateFcmKey(@RequestBody UpdateFcmKeyRequest request) throws PocketException {
        request = (UpdateFcmKeyRequest) EncryptDecrypt.decryptRequest(request, UpdateFcmKeyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);

        baseResponseObject = profileService.updateFcmKey(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
