package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.um.GetWalletByCardRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.um.card.CardServices;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketException;

import java.io.IOException;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = {"app","Card"})
public class CardController extends BaseEndpoint{

    @Autowired
    private CardServices cardServices;

    @PostMapping(value = "/getWalletByCardNumber")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getWalletByCardNumber(@RequestBody GetWalletByCardRequest request) throws PocketException, IOException {

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        if(request.getNumber().length()==16){
            baseResponseObject=cardServices.getWalletUsingCardNumber(request.getNumber(),request.getRequestId());

        }else{
            baseResponseObject=cardServices.getWalletUsingCardUid(request.getNumber(),request.getRequestId());

        }
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
