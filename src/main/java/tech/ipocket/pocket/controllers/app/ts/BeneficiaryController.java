package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.bank.CreateBankRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.remittance.CreateBeneficiaryRequest;
import tech.ipocket.pocket.services.ts.remittance.GetBeneficiaryRequest;
import tech.ipocket.pocket.services.ts.remittance.BeneficiaryService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketException;
import tech.ipocket.pocket.utils.EncryptDecrypt;


@RestController
@RequestMapping(value = "/v1/app/beneficiary")
@Api(tags = "app")
public class BeneficiaryController extends BaseEndpoint {

    @Autowired
    private BeneficiaryService beneficiaryService;


    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/createBeneficiary")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createBeneficiary(@RequestBody CreateBeneficiaryRequest request) throws PocketException {

        request = (CreateBeneficiaryRequest) EncryptDecrypt.decryptRequest(request, CreateBankRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(beneficiaryService.createBeneficiary(request,tsSessionObject), HttpStatus.OK);

    }


    @PostMapping(value = "/getBeneficiary")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getBeneficiary(@RequestBody GetBeneficiaryRequest request) throws PocketException {

        request = (GetBeneficiaryRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(beneficiaryService.getBeneficiary(request,tsSessionObject), HttpStatus.OK);

    }
}
