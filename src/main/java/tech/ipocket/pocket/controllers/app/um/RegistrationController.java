package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.um.OtpVerificationRequest;
import tech.ipocket.pocket.request.um.RegistrationRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.um.RegistrationResponse;
import tech.ipocket.pocket.services.um.registration.RegistrationService;
import tech.ipocket.pocket.services.um.registration.UMRegistrationFactory;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/v1")
public class RegistrationController extends BaseEndpoint {

    @Autowired
    private UMRegistrationFactory umRegistrationFactory;

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private Environment environment;

    @ApiOperation(value = "", tags = "mobile")
    @PostMapping(value = "/mobile/registration")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> mobileRegistration(@RequestBody RegistrationRequest registrationRequest) throws PocketException {
        return doRegistraiton(registrationRequest);
    }

    @ApiOperation(value = "", tags = "web")
    @PostMapping(value = "/web/registration")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> webRegistration(@RequestBody RegistrationRequest registrationRequest) throws PocketException {
        return doRegistraiton(registrationRequest);
    }

    private ResponseEntity<BaseResponseObject> doRegistraiton(RegistrationRequest registrationRequest) throws PocketException {

        registrationRequest = (RegistrationRequest) EncryptDecrypt.decryptRequest(registrationRequest,RegistrationRequest.class);

        BaseResponseObject registrationResponse = new BaseResponseObject();

        validateRequest(registrationRequest, registrationResponse);

        RegistrationResponse response = umRegistrationFactory.doRegistration(registrationRequest, registrationRequest.getGroupCode());

        // Remove this after upcoming event on April/22 start
        if(registrationRequest.getSource() != null && registrationRequest.getSource().matches("CUSTOMER_APP") &&
                registrationRequest.getGroupCode().matches(UmEnums.UserGroup.CUSTOMER.getGroupCode())){

            OtpVerificationRequest otpVerificationRequest = new OtpVerificationRequest();
            otpVerificationRequest.setOtpText(environment.getProperty("RANDOM_OTP"));
            otpVerificationRequest.setOtpSecret(response.getOtpSecret());

            otpVerificationRequest.setRequestId(registrationRequest.getRequestId());
            otpVerificationRequest.setHardwareSignature(registrationRequest.getHardwareSignature());
            otpVerificationRequest.setSessionToken(registrationRequest.getSessionToken());
            otpVerificationRequest.setDeviceName(registrationRequest.getDeviceName());
            otpVerificationRequest.setEncryptedTextString(registrationRequest.getEncryptedTextString());
            otpVerificationRequest.setEncryptedSecretKeyString(registrationRequest.getEncryptedSecretKeyString());
            otpVerificationRequest.setCurrencyCode(registrationRequest.getCurrencyCode());
            otpVerificationRequest.setSource(registrationRequest.getSource());

            registrationResponse =registrationService.doVerifyUser(otpVerificationRequest);
        }
        // End
        // send success response

        if (response == null) {
            registrationResponse.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            registrationResponse.setData(null);
            registrationResponse.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(registrationResponse, HttpStatus.BAD_REQUEST);
        } else {
            registrationResponse.setError(null);
            registrationResponse.setData(response);
            registrationResponse.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(registrationResponse, HttpStatus.OK);
        }
    }

    @ApiOperation(value = "", tags = "mobile")
    @PostMapping(value = "/mobile/regOtpVerify")
    public ResponseEntity<BaseResponseObject> regOtpVerify(@RequestBody OtpVerificationRequest otpVerificationRequest) throws PocketException {

        otpVerificationRequest = (OtpVerificationRequest) EncryptDecrypt.decryptRequest(otpVerificationRequest, OtpVerificationRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        validateRequest(otpVerificationRequest, baseResponseObject);

        baseResponseObject=registrationService.doVerifyUser(otpVerificationRequest);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);

    }
}
