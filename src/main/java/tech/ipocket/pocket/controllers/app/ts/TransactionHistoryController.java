package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionDetailsRequest;
import tech.ipocket.pocket.request.ts.transactionHistory.GetTransactionHistoryRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.TransactionHistoryService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "app")
public class TransactionHistoryController extends BaseEndpoint {


    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private TransactionHistoryService transactionHistoryService;


    @PostMapping(value = "/getTransactionHistory")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getTransactionHistory(@RequestBody GetTransactionHistoryRequest request) throws Exception {

        request = (GetTransactionHistoryRequest) EncryptDecrypt.decryptRequest(request, GetTransactionHistoryRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        if(request.getWalletNo()==null){
            request.setWalletNo(tsSessionObject.getMobileNumber());
        }

        baseResponseObject = transactionHistoryService.getTransactionHistory(request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/getTransactionDetailsByToken")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getTransactionDetailsByToken(@RequestBody GetTransactionDetailsRequest request) throws Exception {

        request = (GetTransactionDetailsRequest) EncryptDecrypt.decryptRequest(request, GetTransactionHistoryRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

//        TsSessionObject tsSessionObject = requestValidator.validateSession(request);
//        request.setWalletNo(tsSessionObject.getMobileNumber());

        baseResponseObject = transactionHistoryService.getTransactionDetailsByToken(request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
