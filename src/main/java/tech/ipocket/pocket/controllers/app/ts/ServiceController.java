package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.services.CreateUpdateServiceRequest;
import tech.ipocket.pocket.request.ts.services.GetServicesRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.ts.ServicesService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = {"app","web"})
public class ServiceController extends BaseEndpoint {

    @Autowired
    private ServicesService servicesService;

    @PostMapping(value = "/getServices")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getServices(@RequestBody GetServicesRequest request) throws Exception {

        request = (GetServicesRequest) EncryptDecrypt.decryptRequest(request, GetServicesRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(servicesService.getAllService(request), HttpStatus.OK);
    }


    @PostMapping(value = "/createUpdateService")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createService(@RequestBody CreateUpdateServiceRequest request) throws Exception {

        request = (CreateUpdateServiceRequest) EncryptDecrypt.decryptRequest(request, CreateUpdateServiceRequest.class);
        BaseResponseObject baseResponseObject=new BaseResponseObject(request.getRequestId());

        validateRequest(request,baseResponseObject);

        return new ResponseEntity<>(servicesService.createUpdateService(request), HttpStatus.OK);
    }

}
