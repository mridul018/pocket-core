package tech.ipocket.pocket.controllers.app.um;


import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.um.GetStateByCountryCodeRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.um.address.AddressService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = {"address"})
public class AddressController extends BaseEndpoint {


    @Autowired
    private AddressService addressService;


    @PostMapping(value = "/getAllCountry")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getAllCountry(@RequestBody EmptyRequest request) throws PocketException{

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        baseResponseObject=addressService.getAllCountry(request);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/getAllState")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getAllState(@RequestBody EmptyRequest request) throws PocketException{

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        baseResponseObject=addressService.getAllState(request);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }


    @PostMapping(value = "/getAllStateByCountryCode")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getAllStateByCountryCode(@RequestBody GetStateByCountryCodeRequest request) throws PocketException{

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        baseResponseObject=addressService.getAllStateByCountryCode(request);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
