package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import tech.ipocket.pocket.request.um.ForgotPinStepOneRequest;
import tech.ipocket.pocket.request.um.ForgotPinStepTwoRequest;
import tech.ipocket.pocket.request.um.LoginRequest;
import tech.ipocket.pocket.request.um.LoginStepTwoRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.SuccessBoolResponse;
import tech.ipocket.pocket.response.um.ForgotPinStepOneResponse;
import tech.ipocket.pocket.response.um.LoginResponse;
import tech.ipocket.pocket.services.um.login.LoginFactory;
import tech.ipocket.pocket.services.um.login.LoginService;
import tech.ipocket.pocket.utils.*;

import java.io.IOException;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "app")
public class LoginController extends BaseEndpoint implements ApplicationContextAware {


    private LoginFactory loginFactory;
    private ApplicationContext applicationContext;

    private LoginService loginService;

    @PostMapping(value = "/login")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> login(@RequestBody LoginRequest request) throws PocketException, IOException {
        request = (LoginRequest) EncryptDecrypt.decryptRequest(request,LoginRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);


        loginFactory = applicationContext.getBean(LoginFactory.class);

        LoginResponse response = loginFactory
                .doLogin(request);



        if (response == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(EncryptDecrypt.encryptResponse(baseResponseObject), HttpStatus.OK);
        }

    }

    @PostMapping(value = "/loginOtpVerification")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> loginOtpVerification(@RequestBody LoginStepTwoRequest request) throws PocketException, IOException {

        request = (LoginStepTwoRequest) EncryptDecrypt.decryptRequest(request,LoginStepTwoRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);


        loginService = applicationContext.getBean(LoginService.class);

        LoginResponse response = loginService.doLoginWithOtp(request);

        if (response == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(EncryptDecrypt.encryptResponse(baseResponseObject), HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping(value = "/forgotPinStepOneRequest")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> forgotPinStepOneRequest(@RequestBody ForgotPinStepOneRequest request) throws PocketException {

        request = (ForgotPinStepOneRequest) EncryptDecrypt.decryptRequest(request,ForgotPinStepOneRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);


        loginService = applicationContext.getBean(LoginService.class);

        ForgotPinStepOneResponse response = loginService.forgotPinStepOneRequest(request);

        if (response == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(EncryptDecrypt.encryptResponse(baseResponseObject), HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            baseResponseObject.setData(response);
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

    }

    @PostMapping(value = "/forgotPinStepTwoRequest")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> forgotPinStepTwoRequest(@RequestBody ForgotPinStepTwoRequest request) throws PocketException {

        request = (ForgotPinStepTwoRequest) EncryptDecrypt.decryptRequest(request,ForgotPinStepTwoRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);


        loginService = applicationContext.getBean(LoginService.class);

        //Remove this after upcoming event on April/22 start
        if(request.getOtpText() == null || request.getOtpText().isEmpty()){
            request.setOtpText("3456");
        }
        // *** End ***

        boolean response = loginService.forgotPinStepTwoRequest(request);

        if (!response) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UnspecifiedErrorOccured));
            baseResponseObject.setData(new SuccessBoolResponse(false));
            baseResponseObject.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
        } else {
            baseResponseObject.setError(null);
            baseResponseObject.setData(new SuccessBoolResponse(true));
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(EncryptDecrypt.encryptResponse(baseResponseObject), HttpStatus.OK);
        }

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
