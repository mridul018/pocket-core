package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.um.GetNotificationRequestByWalletRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.services.NotificationService;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = {"app"})
public class NotificationController extends BaseEndpoint{

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/getUserNotificationByWallet")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserNotificationByWallet(@RequestBody GetNotificationRequestByWalletRequest request) throws PocketException{

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());


        TsSessionObject umSessionResponse = requestValidator.validateSession(request);

        if(request.getWalletNumber()==null){
            request.setWalletNumber(umSessionResponse.getMobileNumber());
        }

        baseResponseObject=notificationService.getUserNotificationByWallet(request,request.getWalletNumber());


        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

}
