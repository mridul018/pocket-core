package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.repository.ts.paynet.ServiceItem;
import tech.ipocket.pocket.repository.ts.paynet.Services;
import tech.ipocket.pocket.request.ts.SessionCheckWithCredentialRequest;
import tech.ipocket.pocket.request.ts.paynet.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.ts.TsSessionObject;
import tech.ipocket.pocket.response.ts.paynet.PaynetServiceResponse;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.paynetService.PaynetService;
import tech.ipocket.pocket.services.ts.paynetService.PaynetServiceImpl;
import tech.ipocket.pocket.utils.*;
import tech.ipocket.pocket.utils.constants.BillPayTypeConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/app/paynet")
@Api(tags = "app")
public class PaynetBillPayController extends BaseEndpoint {

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private PaynetService paynetService;

    @Autowired
    private PaynetServiceImpl paynetServiceImpl;


    @PostMapping(value = "/transaction")
    public ResponseEntity<BaseResponseObject> createTransaction(@RequestBody PaynetBillPayRequest request) throws PocketException {

        request = (PaynetBillPayRequest) EncryptDecrypt.decryptRequest(request, PaynetTransactionRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        SessionCheckWithCredentialRequest requestWithPin=createRequest(request);
        TsSessionObject tsSessionObject =requestValidator.validateSessionWithCredential(requestWithPin);

        switch (request.getBillPayType()) {
            case BillPayTypeConstants.SLK_PIN:
            case BillPayTypeConstants.SLK50:
            case BillPayTypeConstants.SLK100:
            case BillPayTypeConstants.SLK200:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.NOL_CART:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.VIRGIN_M20:
            case BillPayTypeConstants.VIRGIN_M50:
            case BillPayTypeConstants.VIRGIN_M100:
            case BillPayTypeConstants.VIRGIN_M150:
            case BillPayTypeConstants.VIRGIN_M200:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.E_DIRHAM_BLUE:
            case BillPayTypeConstants.E_DIRHAM_GOLD:
            case BillPayTypeConstants.E_DIRHAM_GREEN:
            case BillPayTypeConstants.E_DIRHAM_RED:
            case BillPayTypeConstants.E_DIRHAM_SILVER:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.ET_POST_MOBILE:
            case BillPayTypeConstants.ET_POST_DEL:
            case BillPayTypeConstants.ET_POST_DIALUP:
            case BillPayTypeConstants.ET_POST_BROADBAND:
            case BillPayTypeConstants.ET_POST_EVISION:
            case BillPayTypeConstants.ET_POST_ELIFE:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.ET_MC:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.ET_MONTHLY_500_MB_DATA_AED_30:
            case BillPayTypeConstants.ET_MONTHLY_DATA_PLAN_AED_33_FIVE_SIM:
            case BillPayTypeConstants.ET_MONTHLY_500_INTERNATIONAL_MINS_AED_49:
            case BillPayTypeConstants.ET_MONTHLY_NONSTOP_50:
            case BillPayTypeConstants.ET_MONTHLY_1_GB_DATA_AED_50:
            case BillPayTypeConstants.ET_MONTHLY_1000_INTERNATIONAL_MINS_AED_79:
            case BillPayTypeConstants.ET_MONTHLY_2000_INTERNATIONAL_MINS_AED_99:
            case BillPayTypeConstants.ET_MONTHLY_3_GB_DATA_AED_100:
            case BillPayTypeConstants.ET_MONTHLY_6_GB_DATA_AED_150:
            case BillPayTypeConstants.ET_WEEKLY_200_INTERNATIONAL_MINS_AED_25:
            case BillPayTypeConstants.ET_2_GB_AND_30_FLEXI_MINS_AED_50:
            case BillPayTypeConstants.ET_4_GB_AND_60_FLEXI_MINS_AED_90:
            case BillPayTypeConstants.ET_6_GB_90_FLEXI_ICP_AED_120:
            case BillPayTypeConstants.ET_COMBO_55_WITH_1GB_AND_50_FLEXI_MINS:
            case BillPayTypeConstants.ET_COMBO_35_WITH_500MB_AND_25_FLEXI_MINS:
            case BillPayTypeConstants.ET_COMBO_110_WITH_3GB_AND_150_FLEXI_MINS:
            case BillPayTypeConstants.ET_COMBO_165_WITH_6GB_AND_200_FLEXI_MINS:
            case BillPayTypeConstants.ET_BANGLADESH_60_MINUTES_DAILY_PACK:
            case BillPayTypeConstants.ET_NEW_DAILY_30_INTERNATIONAL_MINUTES:
            case BillPayTypeConstants.ET_DAILY_SOCIAL_PLAN:
            case BillPayTypeConstants.ET_DAILY_NON_STOP_PLAN:
            case BillPayTypeConstants.ET_100_MB_DAILY_DATA_PACK:
            case BillPayTypeConstants.ET_250_MB_DAILY_DATA_PACK:
            case BillPayTypeConstants.ET_500_MB_DAILY_DATA_PACK:
            case BillPayTypeConstants.ET_VISITOR_LINE:
            case BillPayTypeConstants.ET_VISITOR_LINE_PLUS_79_AED:
            case BillPayTypeConstants.ET_VISITOR_LINE_PREMIUM:
            case BillPayTypeConstants.ET_VISITOR_LINE_PREMIUM_PLUS:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.ET30:
            case BillPayTypeConstants.ET55:
            case BillPayTypeConstants.ET110:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.DU_POST_MOBILE:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.DU_MT_TRANSFER:
            case BillPayTypeConstants.DU_MT:
            case BillPayTypeConstants.DU_MD_25AED:
            case BillPayTypeConstants.DU_MD_55AED:
            case BillPayTypeConstants.DU_MD_110AED:
            case BillPayTypeConstants.DU_MD_210AED:
            case BillPayTypeConstants.DU_MD_525AED:
            case BillPayTypeConstants.DU_MI:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.DU25:
            case BillPayTypeConstants.DU55:
            case BillPayTypeConstants.DU110:
            case BillPayTypeConstants.DU210:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.PUBG_MOBILE_5_AED:
            case BillPayTypeConstants.PUBG_MOBILE_20_AED:
            case BillPayTypeConstants.PUBG_MOBILE_40_AED:
            case BillPayTypeConstants.PUBG_MOBILE_95_AED:
            case BillPayTypeConstants.PUBG_MOBILE_185_AED:
            case BillPayTypeConstants.PUBG_MOBILE_370_AED:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.NETFLIX_AED_100:
            case BillPayTypeConstants.NETFLIX_AED_500:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.STEAM_AED_40:
            case BillPayTypeConstants.STEAM_AED_50:
            case BillPayTypeConstants.STEAM_AED_75:
            case BillPayTypeConstants.STEAM_AED_100:
            case BillPayTypeConstants.STEAM_AED_200:
            case BillPayTypeConstants.STEAM_AED_400:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.VO_H15:
            case BillPayTypeConstants.VO_H30:
            case BillPayTypeConstants.VO_H50:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.VO_F15:
            case BillPayTypeConstants.VO_F30:
            case BillPayTypeConstants.VO_F50:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.XBOX_LIVE_USD_15:
            case BillPayTypeConstants.XBOX_LIVE_USD_25:
            case BillPayTypeConstants.XBOX_LIVE_USD_50:
            case BillPayTypeConstants.XBOX_LIVE_3_MONTH:
            case BillPayTypeConstants.XBOX_LIVE_12_MONTH:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.AMAZON_AE_50_AED:
            case BillPayTypeConstants.AMAZON_AE_100_AED:
            case BillPayTypeConstants.AMAZON_AE_250_AED:
            case BillPayTypeConstants.AMAZON_AE_500_AED:
            case BillPayTypeConstants.AMAZON_USD_10:
            case BillPayTypeConstants.AMAZON_USD_25:
            case BillPayTypeConstants.AMAZON_USD_35:
            case BillPayTypeConstants.AMAZON_USD_50:
            case BillPayTypeConstants.AMAZON_USD_100:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.PSN_AED_1M:
            case BillPayTypeConstants.PSN_AED_3M:
            case BillPayTypeConstants.PSN_AED_1Y:
            case BillPayTypeConstants.PSN_USD_5:
            case BillPayTypeConstants.PSN_USD_10:
            case BillPayTypeConstants.PSN_USD_15:
            case BillPayTypeConstants.PSN_USD_20:
            case BillPayTypeConstants.PSN_USD_30:
            case BillPayTypeConstants.PSN_USD_40:
            case BillPayTypeConstants.PSN_USD_50:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_5:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_10:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_20:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_50:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_3_MONTHS:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_12_MONTHS:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_1_MONTH_MEMBERSHIP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_5_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_10_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_20_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_50_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_3_MONTHS:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_12_MONTHS:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.GPLAY_AED_30:
            case BillPayTypeConstants.GPLAY_AED_50:
            case BillPayTypeConstants.GPLAY_AED_100:
            case BillPayTypeConstants.GPLAY_AED_300:
            case BillPayTypeConstants.GPLAY_AED_500:
            case BillPayTypeConstants.GPLAY_USD_10:
            case BillPayTypeConstants.GPLAY_USD_15:
            case BillPayTypeConstants.GPLAY_USD_25:
            case BillPayTypeConstants.GPLAY_USD_50:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.ITUNES_AED_50:
            case BillPayTypeConstants.ITUNES_AED_100:
            case BillPayTypeConstants.ITUNES_AED_250:
            case BillPayTypeConstants.ITUNES_AED_500:
            case BillPayTypeConstants.ITUNES_USD_5:
            case BillPayTypeConstants.ITUNES_USD_10:
            case BillPayTypeConstants.ITUNES_USD_15:
            case BillPayTypeConstants.ITUNES_USD_20:
            case BillPayTypeConstants.ITUNES_USD_25:
            case BillPayTypeConstants.ITUNES_USD_30:
            case BillPayTypeConstants.ITUNES_USD_50:
            case BillPayTypeConstants.ITUNES_USD_100:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_10_FR:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_15_FR:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_25_FR:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_50_FR:
            case BillPayTypeConstants.ITUNES_UK_GBP_10:
            case BillPayTypeConstants.ITUNES_UK_GBP_15:
            case BillPayTypeConstants.ITUNES_UK_GBP_25:
            case BillPayTypeConstants.ITUNES_UK_GBP_50:
            case BillPayTypeConstants.ITUNES_UK_GBP_100:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.MINECRAFT:
            case BillPayTypeConstants.MINECRAFT_1720_MINECOINS:
            case BillPayTypeConstants.MINECRAFT_3500_MINECOINS:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.NINTENDO_US_USD_10:
            case BillPayTypeConstants.NINTENDO_US_USD_20:
            case BillPayTypeConstants.NINTENDO_US_USD_35:
            case BillPayTypeConstants.NINTENDO_US_USD_50:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.VIP_BALOOT_960_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_2000_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_4200_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_11400_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_25000_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_48000_CHIPS:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.BIGO_LIKE_USD_250_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_500_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_1000_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_1500_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_2000_AE:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.BIGO_LIVE_USD_250_AE:
            case BillPayTypeConstants.BIGO_LIVE_USD_1000_AE:
            case BillPayTypeConstants.BIGO_LIVE_USD_1500_AE:
            case BillPayTypeConstants.BIGO_LIVE_USD_2000_AE:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.SPACETOON_GO_3_MONTHS_SUBSCRIPTION_UAE:
            case BillPayTypeConstants.SPACETOON_GO_12_MONTHS_SUBSCRIPTION_UAE:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.BEIN_1_DAY_SUBSCRIPTION_INT:
            case BillPayTypeConstants.BEIN_1_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.BEIN_12_MONTH_SUBSCRIPTION_INT:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.CRUNCHYROLL_1_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.CRUNCHYROLL_3_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.CRUNCHYROLL_12_MONTHS_SUBSCRIPTION_INT:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.MOBILE_LEGENDS_56_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_278_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_571_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_1167_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_1783_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_3005_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_4770_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_6012_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP:
            case BillPayTypeConstants.MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP_PLUS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_56_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_278_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_571_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_1167_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_1783_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_3005_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_4770_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_6012_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP_PLUS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_TWILIGHT_PASS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TWILIGHT_PASS:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.VIBER_USD_10_INT:
            case BillPayTypeConstants.VIBER_USD_25_INT:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.FREEFIRE_USD_1_100_10_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_2_210_21_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_5_530_53_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_10_1080_108_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_20_2200_220_DIAMONDS:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.STARZPLAY_3_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.STARZPLAY_6_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.STARZPLAY_12_MONTHS_SUBSCRIPTION_INT:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.ROBLOX_USD_10_INT:
            case BillPayTypeConstants.ROBLOX_USD_25_INT:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.MCAFEE_MCAFEE_ANTIVIRUS_1_USER:
            case BillPayTypeConstants.MCAFEE_MCAFEE_INTERNET_SECURITY_1_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_INTERNET_SECURITY_3_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_INTERNET_SECURITY_10_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_TOTAL_PROTECTION_5_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_TOTAL_PROTECTION_10_DEVICE:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.SKYPE_USD_10_INT:
            case BillPayTypeConstants.SKYPE_USD_25_INT:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.EBAY_USD_10_US:
            case BillPayTypeConstants.EBAY_USD_50_US:
            case BillPayTypeConstants.EBAY_USD_100_US:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.SHAHID_VIP_3_MONTHS_SUBSCRIPTION_GCC:
            case BillPayTypeConstants.SHAHID_VIP_6_MONTHS_SUBSCRIPTION_GCC:
            case BillPayTypeConstants.SHAHID_VIP_12_MONTHS_SUBSCRIPTION_GCC:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.SPOTIFY_US_USD_10:
            case BillPayTypeConstants.SPOTIFY_US_USD_30:
            case BillPayTypeConstants.SPOTIFY_US_USD_60:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_10:
            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_20:
            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_50:
            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_100:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_5_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_10_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_20_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_50_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_100_USD:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            case BillPayTypeConstants.E_WALLET_CASH_IN:
                baseResponseObject = paynetServiceImpl.createPaynetTransaction(request, tsSessionObject);
                break;

            default:
                throw new PocketException(PocketErrorCode.UnSupportedBillPayType);
        }
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/findTransaction")
    public ResponseEntity<BaseResponseObject> findTransaction(@RequestBody PaynetBillPayRequest request) throws PocketException {

        request = (PaynetBillPayRequest) EncryptDecrypt.decryptRequest(request, PaynetTransactionRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        SessionCheckWithCredentialRequest requestWithPin=createRequest(request);
        TsSessionObject tsSessionObject =requestValidator.validateSessionWithCredential(requestWithPin);

        switch (request.getBillPayType()) {
            case BillPayTypeConstants.SLK_PIN:
            case BillPayTypeConstants.SLK50:
            case BillPayTypeConstants.SLK100:
            case BillPayTypeConstants.SLK200:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.NOL_CART:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.VIRGIN_M20:
            case BillPayTypeConstants.VIRGIN_M50:
            case BillPayTypeConstants.VIRGIN_M100:
            case BillPayTypeConstants.VIRGIN_M150:
            case BillPayTypeConstants.VIRGIN_M200:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.E_DIRHAM_BLUE:
            case BillPayTypeConstants.E_DIRHAM_GOLD:
            case BillPayTypeConstants.E_DIRHAM_GREEN:
            case BillPayTypeConstants.E_DIRHAM_RED:
            case BillPayTypeConstants.E_DIRHAM_SILVER:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.ET_POST_MOBILE:
            case BillPayTypeConstants.ET_POST_DEL:
            case BillPayTypeConstants.ET_POST_DIALUP:
            case BillPayTypeConstants.ET_POST_BROADBAND:
            case BillPayTypeConstants.ET_POST_EVISION:
            case BillPayTypeConstants.ET_POST_ELIFE:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.ET_MC:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.ET_MONTHLY_500_MB_DATA_AED_30:
            case BillPayTypeConstants.ET_MONTHLY_DATA_PLAN_AED_33_FIVE_SIM:
            case BillPayTypeConstants.ET_MONTHLY_500_INTERNATIONAL_MINS_AED_49:
            case BillPayTypeConstants.ET_MONTHLY_NONSTOP_50:
            case BillPayTypeConstants.ET_MONTHLY_1_GB_DATA_AED_50:
            case BillPayTypeConstants.ET_MONTHLY_1000_INTERNATIONAL_MINS_AED_79:
            case BillPayTypeConstants.ET_MONTHLY_2000_INTERNATIONAL_MINS_AED_99:
            case BillPayTypeConstants.ET_MONTHLY_3_GB_DATA_AED_100:
            case BillPayTypeConstants.ET_MONTHLY_6_GB_DATA_AED_150:
            case BillPayTypeConstants.ET_WEEKLY_200_INTERNATIONAL_MINS_AED_25:
            case BillPayTypeConstants.ET_2_GB_AND_30_FLEXI_MINS_AED_50:
            case BillPayTypeConstants.ET_4_GB_AND_60_FLEXI_MINS_AED_90:
            case BillPayTypeConstants.ET_6_GB_90_FLEXI_ICP_AED_120:
            case BillPayTypeConstants.ET_COMBO_55_WITH_1GB_AND_50_FLEXI_MINS:
            case BillPayTypeConstants.ET_COMBO_35_WITH_500MB_AND_25_FLEXI_MINS:
            case BillPayTypeConstants.ET_COMBO_110_WITH_3GB_AND_150_FLEXI_MINS:
            case BillPayTypeConstants.ET_COMBO_165_WITH_6GB_AND_200_FLEXI_MINS:
            case BillPayTypeConstants.ET_BANGLADESH_60_MINUTES_DAILY_PACK:
            case BillPayTypeConstants.ET_NEW_DAILY_30_INTERNATIONAL_MINUTES:
            case BillPayTypeConstants.ET_DAILY_SOCIAL_PLAN:
            case BillPayTypeConstants.ET_DAILY_NON_STOP_PLAN:
            case BillPayTypeConstants.ET_100_MB_DAILY_DATA_PACK:
            case BillPayTypeConstants.ET_250_MB_DAILY_DATA_PACK:
            case BillPayTypeConstants.ET_500_MB_DAILY_DATA_PACK:
            case BillPayTypeConstants.ET_VISITOR_LINE:
            case BillPayTypeConstants.ET_VISITOR_LINE_PLUS_79_AED:
            case BillPayTypeConstants.ET_VISITOR_LINE_PREMIUM:
            case BillPayTypeConstants.ET_VISITOR_LINE_PREMIUM_PLUS:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.ET30:
            case BillPayTypeConstants.ET55:
            case BillPayTypeConstants.ET110:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.DU_POST_MOBILE:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.DU_MT_TRANSFER:
            case BillPayTypeConstants.DU_MT:
            case BillPayTypeConstants.DU_MD_25AED:
            case BillPayTypeConstants.DU_MD_55AED:
            case BillPayTypeConstants.DU_MD_110AED:
            case BillPayTypeConstants.DU_MD_210AED:
            case BillPayTypeConstants.DU_MD_525AED:
            case BillPayTypeConstants.DU_MI:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.DU25:
            case BillPayTypeConstants.DU55:
            case BillPayTypeConstants.DU110:
            case BillPayTypeConstants.DU210:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.PUBG_MOBILE_5_AED:
            case BillPayTypeConstants.PUBG_MOBILE_20_AED:
            case BillPayTypeConstants.PUBG_MOBILE_40_AED:
            case BillPayTypeConstants.PUBG_MOBILE_95_AED:
            case BillPayTypeConstants.PUBG_MOBILE_185_AED:
            case BillPayTypeConstants.PUBG_MOBILE_370_AED:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.NETFLIX_AED_100:
            case BillPayTypeConstants.NETFLIX_AED_500:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.STEAM_AED_40:
            case BillPayTypeConstants.STEAM_AED_50:
            case BillPayTypeConstants.STEAM_AED_75:
            case BillPayTypeConstants.STEAM_AED_100:
            case BillPayTypeConstants.STEAM_AED_200:
            case BillPayTypeConstants.STEAM_AED_400:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.VO_H15:
            case BillPayTypeConstants.VO_H30:
            case BillPayTypeConstants.VO_H50:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.VO_F15:
            case BillPayTypeConstants.VO_F30:
            case BillPayTypeConstants.VO_F50:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.XBOX_LIVE_USD_15:
            case BillPayTypeConstants.XBOX_LIVE_USD_25:
            case BillPayTypeConstants.XBOX_LIVE_USD_50:
            case BillPayTypeConstants.XBOX_LIVE_3_MONTH:
            case BillPayTypeConstants.XBOX_LIVE_12_MONTH:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.AMAZON_AE_50_AED:
            case BillPayTypeConstants.AMAZON_AE_100_AED:
            case BillPayTypeConstants.AMAZON_AE_250_AED:
            case BillPayTypeConstants.AMAZON_AE_500_AED:
            case BillPayTypeConstants.AMAZON_USD_10:
            case BillPayTypeConstants.AMAZON_USD_25:
            case BillPayTypeConstants.AMAZON_USD_35:
            case BillPayTypeConstants.AMAZON_USD_50:
            case BillPayTypeConstants.AMAZON_USD_100:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.PSN_AED_1M:
            case BillPayTypeConstants.PSN_AED_3M:
            case BillPayTypeConstants.PSN_AED_1Y:
            case BillPayTypeConstants.PSN_USD_5:
            case BillPayTypeConstants.PSN_USD_10:
            case BillPayTypeConstants.PSN_USD_15:
            case BillPayTypeConstants.PSN_USD_20:
            case BillPayTypeConstants.PSN_USD_30:
            case BillPayTypeConstants.PSN_USD_40:
            case BillPayTypeConstants.PSN_USD_50:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.GPLAY_AED_30:
            case BillPayTypeConstants.GPLAY_AED_50:
            case BillPayTypeConstants.GPLAY_AED_100:
            case BillPayTypeConstants.GPLAY_AED_300:
            case BillPayTypeConstants.GPLAY_AED_500:
            case BillPayTypeConstants.GPLAY_USD_10:
            case BillPayTypeConstants.GPLAY_USD_15:
            case BillPayTypeConstants.GPLAY_USD_25:
            case BillPayTypeConstants.GPLAY_USD_50:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_5:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_10:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_20:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_USD_50:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_3_MONTHS:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_12_MONTHS:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UAE_1_MONTH_MEMBERSHIP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_5_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_10_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_20_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_50_GBP:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_3_MONTHS:
            case BillPayTypeConstants.PLAYSTATION_NETWORK_UK_12_MONTHS:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.ITUNES_AED_50:
            case BillPayTypeConstants.ITUNES_AED_100:
            case BillPayTypeConstants.ITUNES_AED_250:
            case BillPayTypeConstants.ITUNES_AED_500:
            case BillPayTypeConstants.ITUNES_USD_5:
            case BillPayTypeConstants.ITUNES_USD_10:
            case BillPayTypeConstants.ITUNES_USD_15:
            case BillPayTypeConstants.ITUNES_USD_20:
            case BillPayTypeConstants.ITUNES_USD_25:
            case BillPayTypeConstants.ITUNES_USD_30:
            case BillPayTypeConstants.ITUNES_USD_50:
            case BillPayTypeConstants.ITUNES_USD_100:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_10_FR:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_15_FR:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_25_FR:
            case BillPayTypeConstants.ITUNES_FRANCE_EUR_50_FR:
            case BillPayTypeConstants.ITUNES_UK_GBP_10:
            case BillPayTypeConstants.ITUNES_UK_GBP_15:
            case BillPayTypeConstants.ITUNES_UK_GBP_25:
            case BillPayTypeConstants.ITUNES_UK_GBP_50:
            case BillPayTypeConstants.ITUNES_UK_GBP_100:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.MINECRAFT:
            case BillPayTypeConstants.MINECRAFT_1720_MINECOINS:
            case BillPayTypeConstants.MINECRAFT_3500_MINECOINS:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.NINTENDO_US_USD_10:
            case BillPayTypeConstants.NINTENDO_US_USD_20:
            case BillPayTypeConstants.NINTENDO_US_USD_35:
            case BillPayTypeConstants.NINTENDO_US_USD_50:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.VIP_BALOOT_960_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_2000_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_4200_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_11400_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_25000_CHIPS:
            case BillPayTypeConstants.VIP_BALOOT_48000_CHIPS:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.BIGO_LIKE_USD_250_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_500_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_1000_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_1500_AE:
            case BillPayTypeConstants.BIGO_LIKE_USD_2000_AE:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.BIGO_LIVE_USD_250_AE:
            case BillPayTypeConstants.BIGO_LIVE_USD_1000_AE:
            case BillPayTypeConstants.BIGO_LIVE_USD_1500_AE:
            case BillPayTypeConstants.BIGO_LIVE_USD_2000_AE:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.SPACETOON_GO_3_MONTHS_SUBSCRIPTION_UAE:
            case BillPayTypeConstants.SPACETOON_GO_12_MONTHS_SUBSCRIPTION_UAE:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.BEIN_1_DAY_SUBSCRIPTION_INT:
            case BillPayTypeConstants.BEIN_1_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.BEIN_12_MONTH_SUBSCRIPTION_INT:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.CRUNCHYROLL_1_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.CRUNCHYROLL_3_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.CRUNCHYROLL_12_MONTHS_SUBSCRIPTION_INT:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.MOBILE_LEGENDS_56_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_278_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_571_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_1167_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_1783_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_3005_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_4770_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_6012_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP:
            case BillPayTypeConstants.MOBILE_LEGENDS_STARLIGHT_MEMBERSHIP_PLUS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_56_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_278_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_571_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_1167_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_1783_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_3005_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_4770_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_6012_DIAMONDS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_STARLIGHT_MEMBERSHIP_PLUS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TOP_UP_TWILIGHT_PASS:
            case BillPayTypeConstants.MOBILE_LEGENDS_TWILIGHT_PASS:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.VIBER_USD_10_INT:
            case BillPayTypeConstants.VIBER_USD_25_INT:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.FREEFIRE_USD_1_100_10_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_2_210_21_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_5_530_53_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_10_1080_108_DIAMONDS:
            case BillPayTypeConstants.FREEFIRE_USD_20_2200_220_DIAMONDS:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.STARZPLAY_3_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.STARZPLAY_6_MONTH_SUBSCRIPTION_INT:
            case BillPayTypeConstants.STARZPLAY_12_MONTHS_SUBSCRIPTION_INT:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.ROBLOX_USD_10_INT:
            case BillPayTypeConstants.ROBLOX_USD_25_INT:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.MCAFEE_MCAFEE_ANTIVIRUS_1_USER:
            case BillPayTypeConstants.MCAFEE_MCAFEE_INTERNET_SECURITY_1_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_INTERNET_SECURITY_3_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_INTERNET_SECURITY_10_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_TOTAL_PROTECTION_5_DEVICE:
            case BillPayTypeConstants.MCAFEE_MCAFEE_TOTAL_PROTECTION_10_DEVICE:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.SKYPE_USD_10_INT:
            case BillPayTypeConstants.SKYPE_USD_25_INT:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.EBAY_USD_10_US:
            case BillPayTypeConstants.EBAY_USD_50_US:
            case BillPayTypeConstants.EBAY_USD_100_US:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.SHAHID_VIP_3_MONTHS_SUBSCRIPTION_GCC:
            case BillPayTypeConstants.SHAHID_VIP_6_MONTHS_SUBSCRIPTION_GCC:
            case BillPayTypeConstants.SHAHID_VIP_12_MONTHS_SUBSCRIPTION_GCC:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.SPOTIFY_US_USD_10:
            case BillPayTypeConstants.SPOTIFY_US_USD_30:
            case BillPayTypeConstants.SPOTIFY_US_USD_60:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_10:
            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_20:
            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_50:
            case BillPayTypeConstants.RAZER_GOLD_PINS_USD_100:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_5_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_10_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_20_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_50_USD:
            case BillPayTypeConstants.RAZER_GOLD_TOP_UP_100_USD:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            case BillPayTypeConstants.E_WALLET_CASH_IN:
                baseResponseObject = paynetService.findTransaction(request);
                break;

            default:
                throw new PocketException(PocketErrorCode.UnSupportedBillPayType);
        }
        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/serviceList")
    public ResponseEntity<BaseResponseObject> paynetServices(@RequestBody PaynetServicesRequest request) throws PocketException, IOException {

        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        request = (PaynetServicesRequest) EncryptDecrypt.decryptRequest(request, PaynetServicesRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        PaynetServiceResponse serviceResponse = paynetService.getPaynetAllServices(request);

        if(request.getServiceType().matches("salik") || request.getServiceType().toUpperCase().matches("SALIK") ||
            request.getServiceType().matches("nolcard") || request.getServiceType().toUpperCase().matches("NOLCARD") ||
            request.getServiceType().matches("virgin") || request.getServiceType().toUpperCase().matches("VIRGIN") ||
            request.getServiceType().matches("edirham") || request.getServiceType().toUpperCase().matches("EDIRHAM") ||
            request.getServiceType().matches("etisalat_postpaid") || request.getServiceType().toUpperCase().matches("ETISALAT_POSTPAID") ||
            request.getServiceType().matches("etisalat_prepaid") || request.getServiceType().toUpperCase().matches("ETISALAT_PREPAID") ||
            request.getServiceType().matches("etisalat_packages") || request.getServiceType().toUpperCase().matches("ETISALAT_PACKAGES") ||
            request.getServiceType().matches("etisalat_voucher") || request.getServiceType().toUpperCase().matches("ETISALAT_VOUCHER") ||
            request.getServiceType().matches("du_postpaid") || request.getServiceType().toUpperCase().matches("DU_POSTPAID") ||
            request.getServiceType().matches("du_prepaid") || request.getServiceType().toUpperCase().matches("DU_PREPAID") ||
            request.getServiceType().matches("du_voucher") || request.getServiceType().toUpperCase().matches("DU_VOUCHER") ||
            request.getServiceType().matches("pubg") || request.getServiceType().toUpperCase().matches("PUBG") ||
            request.getServiceType().matches("netflix") || request.getServiceType().toUpperCase().matches("NETFLIX") ||
            request.getServiceType().matches("steam") || request.getServiceType().toUpperCase().matches("STEAM") ||
            request.getServiceType().matches("hello_card") || request.getServiceType().toUpperCase().matches("HELLO_CARD") ||
            request.getServiceType().matches("five_card") || request.getServiceType().toUpperCase().matches("FIVE_CARD") ||
            request.getServiceType().matches("xbox") || request.getServiceType().toUpperCase().matches("XBOX") ||
            request.getServiceType().matches("amazon") || request.getServiceType().toUpperCase().matches("AMAZON") ||
            request.getServiceType().matches("psn") || request.getServiceType().toUpperCase().matches("PSN") ||
            request.getServiceType().matches("gplay") || request.getServiceType().toUpperCase().matches("GPLAY") ||
            request.getServiceType().matches("itunes") || request.getServiceType().toUpperCase().matches("ITUNES") ||
            request.getServiceType().matches("minecraft") || request.getServiceType().toUpperCase().matches("MINECRAFT")||
            request.getServiceType().matches("nintendo") || request.getServiceType().toUpperCase().matches("NINTENDO") ||
            request.getServiceType().matches("vip_baloot") || request.getServiceType().toUpperCase().matches("VIP_BALOOT") ||
            request.getServiceType().matches("bigo_like") || request.getServiceType().toUpperCase().matches("BIGO_LIKE") ||
            request.getServiceType().matches("bigo_live") || request.getServiceType().toUpperCase().matches("BIGO_LIVE") ||
            request.getServiceType().matches("spacetoon_go") || request.getServiceType().toUpperCase().matches("SPACETOON_GO") ||
            request.getServiceType().matches("bein") || request.getServiceType().toUpperCase().matches("BEIN") ||
            request.getServiceType().matches("crunchyroll") || request.getServiceType().toUpperCase().matches("CRUNCHYROLL") ||
            request.getServiceType().matches("mobile_legends") || request.getServiceType().toUpperCase().matches("MOBILE_LEGENDS") ||
            request.getServiceType().matches("viber") || request.getServiceType().toUpperCase().matches("VIBER") ||
            request.getServiceType().matches("freefire") || request.getServiceType().toUpperCase().matches("FREEFIRE") ||
            request.getServiceType().matches("starzplay") || request.getServiceType().toUpperCase().matches("STARZPLAY") ||
            request.getServiceType().matches("roblox") || request.getServiceType().toUpperCase().matches("ROBLOX") ||
            request.getServiceType().matches("mcafee") || request.getServiceType().toUpperCase().matches("MCAFEE") ||
            request.getServiceType().matches("skype") || request.getServiceType().toUpperCase().matches("SKYPE") ||
            request.getServiceType().matches("ebay") || request.getServiceType().toUpperCase().matches("EBAY") ||
            request.getServiceType().matches("shahid_vip") || request.getServiceType().toUpperCase().matches("SHAHID_VIP") ||
            request.getServiceType().matches("spotify") || request.getServiceType().toUpperCase().matches("SPOTIFY") ||
            request.getServiceType().matches("razer_gold") || request.getServiceType().toUpperCase().matches("RAZER_GOLD") ||
            request.getServiceType().matches("ewallet") || request.getServiceType().toUpperCase().matches("EWALLET")){

            List<ServiceItem> salikServices = new ArrayList<>();
            List<ServiceItem> nolcardServices = new ArrayList<>();
            List<ServiceItem> virginServices = new ArrayList<>();
            List<ServiceItem> edirhamServices = new ArrayList<>();
            List<ServiceItem> etisalatPostpaidServices = new ArrayList<>();
            List<ServiceItem> etisalatPrepaidServices = new ArrayList<>();
            List<ServiceItem> etisalatPackagesServices = new ArrayList<>();
            List<ServiceItem> etisalatVoucherServices = new ArrayList<>();
            List<ServiceItem> duPostpaidServices = new ArrayList<>();
            List<ServiceItem> duPrepaidServices = new ArrayList<>();
            List<ServiceItem> duVoucherServices = new ArrayList<>();
            List<ServiceItem> pubgServices = new ArrayList<>();
            List<ServiceItem> netflixServices = new ArrayList<>();
            List<ServiceItem> steamServices = new ArrayList<>();
            List<ServiceItem> helloCardServices = new ArrayList<>();
            List<ServiceItem> fiveCardServices = new ArrayList<>();
            List<ServiceItem> xboxServices = new ArrayList<>();
            List<ServiceItem> amazonServices = new ArrayList<>();
            List<ServiceItem> playStationServices = new ArrayList<>();
            List<ServiceItem> googlePlayStoreServices = new ArrayList<>();
            List<ServiceItem> itunesServices = new ArrayList<>();
            List<ServiceItem> mineCraftServices = new ArrayList<>();
            List<ServiceItem> nintendoServices = new ArrayList<>();
            List<ServiceItem> vipBalootServices = new ArrayList<>();
            List<ServiceItem> bigoLikeServices = new ArrayList<>();
            List<ServiceItem> bigoLiveServices = new ArrayList<>();
            List<ServiceItem> spacetoonGoServices = new ArrayList<>();
            List<ServiceItem> beinServices = new ArrayList<>();
            List<ServiceItem> chrunchyRollServices = new ArrayList<>();
            List<ServiceItem> mobileLegendsServices = new ArrayList<>();
            List<ServiceItem> viberServices = new ArrayList<>();
            List<ServiceItem> freeFireServices = new ArrayList<>();
            List<ServiceItem> starzPlayServices = new ArrayList<>();
            List<ServiceItem> robLoxServices = new ArrayList<>();
            List<ServiceItem> mcafeeServices = new ArrayList<>();
            List<ServiceItem> skypeServices = new ArrayList<>();
            List<ServiceItem> ebayServices = new ArrayList<>();
            List<ServiceItem> shahidVipServices = new ArrayList<>();
            List<ServiceItem> spotifyServices = new ArrayList<>();
            List<ServiceItem> razerGoldServices = new ArrayList<>();
            List<ServiceItem> ewalletServices = new ArrayList<>();

            Services services = (Services)serviceResponse.getData();

            for(ServiceItem serviceItem : services.services){
                if(serviceItem.id.equals("slk-pin") || serviceItem.id.equals("slk50") || serviceItem.id.equals("slk100") || serviceItem.id.equals("slk200")){
                    salikServices.add(serviceItem);
                }
                if(serviceItem.id.equals("...")){   //have to do after taking full service from nolcard.
                    nolcardServices.add(serviceItem);
                }
                if(serviceItem.id.equals("virgin-m20") || serviceItem.id.equals("virgin-m50") || serviceItem.id.equals("virgin-m100")
                        || serviceItem.id.equals("virgin-m150") || serviceItem.id.equals("virgin-m200")){
                    virginServices.add(serviceItem);
                }
                if(serviceItem.id.equals("e-dirham-blue") || serviceItem.id.equals("e-dirham-gold") || serviceItem.id.equals("e-dirham-green")
                        || serviceItem.id.equals("e-dirham-red") || serviceItem.id.equals("e-dirham-silver")){
                    edirhamServices.add(serviceItem);
                }
                if(serviceItem.id.equals("et-post-mobile") || serviceItem.id.equals("et-post-del") || serviceItem.id.equals("et-post-dialup")
                        || serviceItem.id.equals("et-post-broadband") || serviceItem.id.equals("et-post-evision")
                        || serviceItem.id.equals("et-post-elife")){
                    etisalatPostpaidServices.add(serviceItem);
                }
                if(serviceItem.id.equals("et-mc")){
                    etisalatPrepaidServices.add(serviceItem);
                }
                if(serviceItem.id.equals("et-monthly-500-mb-data-aed-30") || serviceItem.id.equals("et-monthly-data-plan-aed-33-five-sim") ||
                        serviceItem.id.equals("et-monthly-500-international-mins-aed-49") || serviceItem.id.equals("et-monthly-nonstop-50") ||
                        serviceItem.id.equals("et-monthly-1-gb-data-aed-50") || serviceItem.id.equals("et-monthly-1000-international-mins-aed-79") ||
                        serviceItem.id.equals("et-monthly-2000-international-mins-aed-99") || serviceItem.id.equals("et-monthly-3-gb-data-aed-100") ||
                        serviceItem.id.equals("et-monthly-6-gb-data-aed-150") || serviceItem.id.equals("et-weekly-200-international-mins-aed-25") ||
                        serviceItem.id.equals("et-2-gb-and-30-flexi-mins-aed-50") || serviceItem.id.equals("et-4-gb-and-60-flexi-mins-aed-90") ||
                        serviceItem.id.equals("et-6-gb-90-flexi-icp-aed-120") || serviceItem.id.equals("et-combo-55-with-1gb-and-50-flexi-mins") ||
                        serviceItem.id.equals("et-combo-35-with-500mb-and-25-flexi-mins") || serviceItem.id.equals("et-combo-110-with-3gb-and-150-flexi-mins") ||
                        serviceItem.id.equals("et-combo-165-with-6gb-and-200-flexi-mins") || serviceItem.id.equals("et-bangladesh-60-minutes-daily-pack") ||
                        serviceItem.id.equals("et-new-daily-30-international-minutes") || serviceItem.id.equals("et-daily-social-plan") ||
                        serviceItem.id.equals("et-daily-non-stop-plan") || serviceItem.id.equals("et-100-mb-daily-data-pack") ||
                        serviceItem.id.equals("et-250-mb-daily-data-pack") || serviceItem.id.equals("et-500-mb-daily-data-pack") ||
                        serviceItem.id.equals("et-visitor-line") || serviceItem.id.equals("et-visitor-line-plus-79-aed") ||
                        serviceItem.id.equals("et-visitor-line-premium") || serviceItem.id.equals("et-visitor-line-premium-plus")){
                    etisalatPackagesServices.add(serviceItem);
                }
                if(serviceItem.id.equals("et30") || serviceItem.id.equals("et55") || serviceItem.id.equals("et110")){
                    etisalatVoucherServices.add(serviceItem);
                }

                if(serviceItem.id.equals("du-post-mobile")){
                    duPostpaidServices.add(serviceItem);
                }
                if(serviceItem.id.equals("du-mt-transfer") || serviceItem.id.equals("du-mt") ||
                        serviceItem.id.equals("du-md-25aed") || serviceItem.id.equals("du-md-55aed") ||
                        serviceItem.id.equals("du-md-110aed") || serviceItem.id.equals("du-md-210aed") ||
                        serviceItem.id.equals("du-md-525aed") || serviceItem.id.equals("du-mi")){
                    duPrepaidServices.add(serviceItem);
                }
                if(serviceItem.id.equals("du25") || serviceItem.id.equals("du55") ||
                        serviceItem.id.equals("du110") || serviceItem.id.equals("du210")){
                    duVoucherServices.add(serviceItem);
                }
                if(serviceItem.id.equals("pubg_mobile_5_aed") || serviceItem.id.equals("pubg_mobile_20_aed")
                        || serviceItem.id.equals("pubg_mobile_40_aed") || serviceItem.id.equals("pubg_mobile_95_aed")
                        || serviceItem.id.equals("pubg_mobile_185_aed") || serviceItem.id.equals("pubg_mobile_370_aed") ){
                    pubgServices.add(serviceItem);
                }
                if(serviceItem.id.equals("netflix-aed-100") || serviceItem.id.equals("netflix-aed-500")){
                    netflixServices.add(serviceItem);
                }
                if(serviceItem.id.equals("steam-aed-40") || serviceItem.id.equals("steam-aed-50")
                        || serviceItem.id.equals("steam-aed-75") || serviceItem.id.equals("steam-aed-100")
                        || serviceItem.id.equals("steam-aed-200") || serviceItem.id.equals("steam-aed-400")){
                    steamServices.add(serviceItem);
                }
                if(serviceItem.id.equals("vo-h15") || serviceItem.id.equals("vo-h30") || serviceItem.id.equals("vo-h50")){
                    helloCardServices.add(serviceItem);
                }
                if(serviceItem.id.equals("vo-f15") || serviceItem.id.equals("vo-f30") || serviceItem.id.equals("vo-f50")){
                    fiveCardServices.add(serviceItem);
                }
                if(serviceItem.id.equals("xbox_live_usd_15") || serviceItem.id.equals("xbox_live_usd_25")
                        || serviceItem.id.equals("xbox_live_usd_50") || serviceItem.id.equals("xbox_live_3_month")
                        || serviceItem.id.equals("xbox_live_12_month")){
                    xboxServices.add(serviceItem);
                }
                if(serviceItem.id.equals("amazon_ae_50_aed") || serviceItem.id.equals("amazon_ae_100_aed")
                        || serviceItem.id.equals("amazon_ae_250_aed") || serviceItem.id.equals("amazon_ae_500_aed")
                        || serviceItem.id.equals("amazon_usd_10") || serviceItem.id.equals("amazon_usd_25")
                        || serviceItem.id.equals("amazon_usd_35") || serviceItem.id.equals("amazon_usd_50")
                        || serviceItem.id.equals("amazon_usd_100")){
                    amazonServices.add(serviceItem);
                }
                if(serviceItem.id.equals("psn_aed_1m") || serviceItem.id.equals("psn_aed_3m")
                        || serviceItem.id.equals("psn_aed_1y") || serviceItem.id.equals("psn_usd_5")
                        || serviceItem.id.equals("psn_usd_10") || serviceItem.id.equals("psn_usd_15")
                        || serviceItem.id.equals("psn_usd_20") || serviceItem.id.equals("psn_usd_30")
                        || serviceItem.id.equals("psn_usd_40") || serviceItem.id.equals("psn_usd_50")
                        || serviceItem.id.equals("playstation_network_uae_usd_5") || serviceItem.id.equals("playstation_network_uae_usd_10")
                        || serviceItem.id.equals("playstation_network_uae_usd_20") || serviceItem.id.equals("playstation_network_uae_usd_50")
                        || serviceItem.id.equals("playstation_network_uae_3_months") || serviceItem.id.equals("playstation_network_uae_12_months")
                        || serviceItem.id.equals("playstation_network_uae_1_month_membership") || serviceItem.id.equals("playstation_network_uk_5_gbp")
                        || serviceItem.id.equals("playstation_network_uk_10_gbp") || serviceItem.id.equals("playstation_network_uk_20_gbp")
                        || serviceItem.id.equals("playstation_network_uk_50_gbp") || serviceItem.id.equals("playstation_network_uk_3_months")
                        || serviceItem.id.equals("playstation_network_uk_12_months")){
                    playStationServices.add(serviceItem);
                }
                if(serviceItem.id.equals("gplay_aed_30") || serviceItem.id.equals("gplay_aed_50")
                        || serviceItem.id.equals("gplay_aed_100") || serviceItem.id.equals("gplay_aed_300")
                        || serviceItem.id.equals("gplay_aed_500") || serviceItem.id.equals("gplay_usd_10")
                        || serviceItem.id.equals("gplay_usd_15") || serviceItem.id.equals("gplay_usd_25")
                        || serviceItem.id.equals("gplay_usd_50")){
                    googlePlayStoreServices.add(serviceItem);
                }
                if(serviceItem.id.equals("itunes_aed_50") || serviceItem.id.equals("itunes_aed_100")
                        || serviceItem.id.equals("itunes_aed_250") || serviceItem.id.equals("itunes_aed_500")
                        || serviceItem.id.equals("itunes_usd_5") || serviceItem.id.equals("itunes_usd_10")
                        || serviceItem.id.equals("itunes_usd_15") || serviceItem.id.equals("itunes_usd_20")
                        || serviceItem.id.equals("itunes_usd_25") || serviceItem.id.equals("itunes_usd_30")
                        || serviceItem.id.equals("itunes_usd_50")  || serviceItem.id.equals("itunes_usd_100")){
                    itunesServices.add(serviceItem);
                }
                if(serviceItem.id.equals("minecraft") || serviceItem.id.equals("minecraft_1720_minecoins")
                        || serviceItem.id.equals("minecraft_3500_minecoins")){
                    mineCraftServices.add(serviceItem);
                }
                if(serviceItem.id.equals("nintendo_us_usd_10") || serviceItem.id.equals("nintendo_us_usd_20")
                        || serviceItem.id.equals("nintendo_us_usd_35")|| serviceItem.id.equals("nintendo_us_usd_50")){
                    nintendoServices.add(serviceItem);
                }
                if(serviceItem.id.equals("vip_baloot_960_chips") || serviceItem.id.equals("vip_baloot_2000_chips")
                        || serviceItem.id.equals("vip_baloot_4200_chips")|| serviceItem.id.equals("vip_baloot_11400_chips")
                        || serviceItem.id.equals("vip_baloot_25000_chips")|| serviceItem.id.equals("vip_baloot_48000_chips")){
                    vipBalootServices.add(serviceItem);
                }
                if(serviceItem.id.equals("bigo_like_usd_250_ae") || serviceItem.id.equals("bigo_like_usd_500_ae")
                        || serviceItem.id.equals("bigo_like_usd_1000_ae")|| serviceItem.id.equals("bigo_like_usd_1500_ae")
                        || serviceItem.id.equals("bigo_like_usd_2000_ae")){
                    bigoLikeServices.add(serviceItem);
                }
                if(serviceItem.id.equals("bigo_live_usd_250_ae") || serviceItem.id.equals("bigo_live_usd_1000_ae")
                        || serviceItem.id.equals("bigo_live_usd_1500_ae")|| serviceItem.id.equals("bigo_live_usd_2000_ae")){
                    bigoLiveServices.add(serviceItem);
                }
                if(serviceItem.id.equals("spacetoon_go_3_months_subscription_uae") || serviceItem.id.equals("spacetoon_go_12_months_subscription_uae")){
                    spacetoonGoServices.add(serviceItem);
                }
                if(serviceItem.id.equals("bein_1_day_subscription_int") || serviceItem.id.equals("bein_1_month_subscription_int")
                        ||serviceItem.id.equals("bein_12_month_subscription_int")){
                    beinServices.add(serviceItem);
                }
                if(serviceItem.id.equals("crunchyroll_1_month_subscription_int") || serviceItem.id.equals("crunchyroll_3_month_subscription_int")
                        ||serviceItem.id.equals("crunchyroll_12_month_subscription_int")){
                    chrunchyRollServices.add(serviceItem);
                }
                if(serviceItem.id.equals("mobile_legends_56_diamonds") || serviceItem.id.equals("mobile_legends_278_diamonds")
                        || serviceItem.id.equals("mobile_legends_571_diamonds") || serviceItem.id.equals("mobile_legends_1167_diamonds")
                        || serviceItem.id.equals("mobile_legends_1783_diamonds") || serviceItem.id.equals("mobile_legends_3005_diamonds")
                        || serviceItem.id.equals("mobile_legends_4770_diamonds") || serviceItem.id.equals("mobile_legends_6012_diamonds")
                        || serviceItem.id.equals("mobile_legends_starlight_membership") || serviceItem.id.equals("mobile_legends_starlight_membership_plus")
                        || serviceItem.id.equals("mobile_legends_top_up_56_diamonds") || serviceItem.id.equals("mobile_legends_top_up_278_diamonds")
                        || serviceItem.id.equals("mobile_legends_top_up_571_diamonds") || serviceItem.id.equals("mobile_legends_top_up_1167_diamonds")
                        || serviceItem.id.equals("mobile_legends_top_up_1783_diamonds") || serviceItem.id.equals("mobile_legends_top_up_3005_diamonds")
                        || serviceItem.id.equals("mobile_legends_top_up_4770_diamonds") || serviceItem.id.equals("mobile_legends_top_up_6012_diamonds")
                        || serviceItem.id.equals("mobile_legends_top_up_starlight_membership") || serviceItem.id.equals("mobile_legends_top_up_starlight_membership_plus")
                        || serviceItem.id.equals("mobile_legends_top_up_twilight_pass") || serviceItem.id.equals("mobile_legends_twilight_pass")){
                    mobileLegendsServices.add(serviceItem);
                }
                if(serviceItem.id.equals("viber_usd_10_int") || serviceItem.id.equals("viber_usd_25_int")){
                    viberServices.add(serviceItem);
                }
                if(serviceItem.id.equals("freefire_usd_1_100_10_diamonds") || serviceItem.id.equals("freefire_usd_2_210_21_diamonds")
                        || serviceItem.id.equals("freefire_usd_5_530_53_diamonds") || serviceItem.id.equals("freefire_usd_10_1080_108_diamonds")
                        || serviceItem.id.equals("freefire_usd_20_2200_220_diamonds")){
                    freeFireServices.add(serviceItem);
                }
                if(serviceItem.id.equals("starzplay_3_month_subscription_int") || serviceItem.id.equals("starzplay_6_month_subscription_int")
                        || serviceItem.id.equals("starzplay_12_month_subscription_int")){
                    starzPlayServices.add(serviceItem);
                }
                if(serviceItem.id.equals("roblox_usd_10_int") || serviceItem.id.equals("roblox_usd_25_int")){
                    robLoxServices.add(serviceItem);
                }
                if(serviceItem.id.equals("mcafee_mcafee_antivirus_1_user") || serviceItem.id.equals("mcafee_mcafee_internet_security_1_device")
                        || serviceItem.id.equals("mcafee_mcafee_internet_security_3_device") || serviceItem.id.equals("mcafee_mcafee_internet_security_10_device")
                        || serviceItem.id.equals("mcafee_mcafee_total_protection_5_device")  || serviceItem.id.equals("mcafee_mcafee_total_protection_10_device")){
                    mcafeeServices.add(serviceItem);
                }
                if(serviceItem.id.equals("skype_usd_10_int") || serviceItem.id.equals("skype_usd_25_int")){
                    skypeServices.add(serviceItem);
                }
                if(serviceItem.id.equals("ebay_usd_10_us") || serviceItem.id.equals("ebay_usd_50_us")
                        || serviceItem.id.equals("ebay_usd_100_us")){
                    ebayServices.add(serviceItem);
                }
                if(serviceItem.id.equals("shahid_vip_3_months_subscription_gcc") || serviceItem.id.equals("shahid_vip_6_months_subscription_gcc")
                        || serviceItem.id.equals("shahid_vip_12_months_subscription_gcc")){
                    shahidVipServices.add(serviceItem);
                }
                if(serviceItem.id.equals("spotify_us_usd_10") || serviceItem.id.equals("spotify_us_usd_30")
                        || serviceItem.id.equals("spotify_us_usd_60")){
                    spotifyServices.add(serviceItem);
                }
                if(serviceItem.id.equals("razer_gold_pins_usd_10") || serviceItem.id.equals("razer_gold_pins_usd_20")
                        || serviceItem.id.equals("razer_gold_pins_usd_50") || serviceItem.id.equals("razer_gold_pins_usd_100")
                        || serviceItem.id.equals("razer_gold_top_up_5_usd")  || serviceItem.id.equals("razer_gold_top_up_10_usd")
                        || serviceItem.id.equals("razer_gold_top_up_20_usd")  || serviceItem.id.equals("razer_gold_top_up_50_usd")
                        || serviceItem.id.equals("razer_gold_top_up_100_usd")){
                    razerGoldServices.add(serviceItem);
                }
                if(serviceItem.id.equals("e-wallet-cash-in")){
                    ewalletServices.add(serviceItem);
                }
            }

            if(request.getServiceType().matches("salik") || request.getServiceType().toUpperCase().matches("SALIK")){
                serviceResponse.setData(salikServices);
            }
            if(request.getServiceType().matches("nolcard") || request.getServiceType().toUpperCase().matches("NOLCARD")){
                serviceResponse.setData(nolcardServices);
            }
            if(request.getServiceType().matches("virgin") || request.getServiceType().toUpperCase().matches("VIRGIN")){
                serviceResponse.setData(virginServices);
            }
            if(request.getServiceType().matches("edirham") || request.getServiceType().toUpperCase().matches("EDIRHAM")){
                serviceResponse.setData(edirhamServices);
            }
            if(request.getServiceType().matches("etisalat_postpaid") || request.getServiceType().toUpperCase().matches("ETISALAT_POSTPAID")){
                serviceResponse.setData(etisalatPostpaidServices);
            }
            if(request.getServiceType().matches("etisalat_prepaid") || request.getServiceType().toUpperCase().matches("ETISALAT_PREPAID")){
                serviceResponse.setData(etisalatPrepaidServices);
            }
            if(request.getServiceType().matches("etisalat_packages") || request.getServiceType().toUpperCase().matches("ETISALAT_PACKAGES")){
                serviceResponse.setData(etisalatPackagesServices);
            }
            if(request.getServiceType().matches("etisalat_voucher") || request.getServiceType().toUpperCase().matches("ETISALAT_VOUCHER")){
                serviceResponse.setData(etisalatVoucherServices);
            }
            if(request.getServiceType().matches("du_postpaid") || request.getServiceType().toUpperCase().matches("DU_POSTPAID")){
                serviceResponse.setData(duPostpaidServices);
            }
            if(request.getServiceType().matches("du_prepaid") || request.getServiceType().toUpperCase().matches("DU_PREPAID")){
                serviceResponse.setData(duPrepaidServices);
            }
            if(request.getServiceType().matches("du_voucher") || request.getServiceType().toUpperCase().matches("DU_VOUCHER")){
                serviceResponse.setData(duVoucherServices);
            }
            if(request.getServiceType().matches("pubg") || request.getServiceType().toUpperCase().matches("PUBG")){
                serviceResponse.setData(pubgServices);
            }
            if(request.getServiceType().matches("netflix") || request.getServiceType().toUpperCase().matches("NETFLIX")){
                serviceResponse.setData(netflixServices);
            }
            if(request.getServiceType().matches("steam") || request.getServiceType().toUpperCase().matches("STEAM")){
                serviceResponse.setData(steamServices);
            }
            if(request.getServiceType().matches("hello_card") || request.getServiceType().toUpperCase().matches("HELLO_CARD")){
                serviceResponse.setData(helloCardServices);
            }
            if(request.getServiceType().matches("five_card") || request.getServiceType().toUpperCase().matches("FIVE_CARD")){
                serviceResponse.setData(fiveCardServices);
            }
            if(request.getServiceType().matches("xbox") || request.getServiceType().toUpperCase().matches("XBOX")){
                serviceResponse.setData(xboxServices);
            }
            if(request.getServiceType().matches("amazon") || request.getServiceType().toUpperCase().matches("AMAZON")){
                serviceResponse.setData(amazonServices);
            }
            if(request.getServiceType().matches("psn") || request.getServiceType().toUpperCase().matches("PSN")){
                serviceResponse.setData(playStationServices);
            }
            if(request.getServiceType().matches("gplay") || request.getServiceType().toUpperCase().matches("GPLAY")){
                serviceResponse.setData(googlePlayStoreServices);
            }
            if(request.getServiceType().matches("itunes") || request.getServiceType().toUpperCase().matches("ITUNES")){
                serviceResponse.setData(itunesServices);
            }
            if(request.getServiceType().matches("minecraft") || request.getServiceType().toUpperCase().matches("MINECRAFT")){
                serviceResponse.setData(mineCraftServices);
            }
            if(request.getServiceType().matches("nintendo") || request.getServiceType().toUpperCase().matches("NINTENDO")){
                serviceResponse.setData(nintendoServices);
            }
            if(request.getServiceType().matches("vip_baloot") || request.getServiceType().toUpperCase().matches("VIP_BALOOT")){
                serviceResponse.setData(vipBalootServices);
            }
            if(request.getServiceType().matches("bigo_like") || request.getServiceType().toUpperCase().matches("BIGO_LIKE")){
                serviceResponse.setData(bigoLikeServices);
            }
            if(request.getServiceType().matches("bigo_live") || request.getServiceType().toUpperCase().matches("BIGO_LIVE")){
                serviceResponse.setData(bigoLiveServices);
            }
            if(request.getServiceType().matches("spacetoon_go") || request.getServiceType().toUpperCase().matches("SPACETOON_GO")){
                serviceResponse.setData(spacetoonGoServices);
            }
            if(request.getServiceType().matches("bein") || request.getServiceType().toUpperCase().matches("BEIN")){
                serviceResponse.setData(beinServices);
            }
            if(request.getServiceType().matches("crunchyroll") || request.getServiceType().toUpperCase().matches("CRUNCHYROLL")){
                serviceResponse.setData(chrunchyRollServices);
            }
            if(request.getServiceType().matches("mobile_legends") || request.getServiceType().toUpperCase().matches("MOBILE_LEGENDS")){
                serviceResponse.setData(mobileLegendsServices);
            }
            if(request.getServiceType().matches("viber") || request.getServiceType().toUpperCase().matches("VIBER")){
                serviceResponse.setData(viberServices);
            }
            if(request.getServiceType().matches("freefire") || request.getServiceType().toUpperCase().matches("FREEFIRE")){
                serviceResponse.setData(freeFireServices);
            }
            if(request.getServiceType().matches("starzplay") || request.getServiceType().toUpperCase().matches("STARZPLAY")){
                serviceResponse.setData(starzPlayServices);
            }
            if(request.getServiceType().matches("roblox") || request.getServiceType().toUpperCase().matches("ROBLOX")){
                serviceResponse.setData(robLoxServices);
            }
            if(request.getServiceType().matches("mcafee") || request.getServiceType().toUpperCase().matches("MCAFEE")){
                serviceResponse.setData(mcafeeServices);
            }
            if(request.getServiceType().matches("skype") || request.getServiceType().toUpperCase().matches("SKYPE")){
                serviceResponse.setData(skypeServices);
            }
            if(request.getServiceType().matches("ebay") || request.getServiceType().toUpperCase().matches("EBAY")){
                serviceResponse.setData(ebayServices);
            }
            if(request.getServiceType().matches("shahid_vip") || request.getServiceType().toUpperCase().matches("SHAHID_VIP")){
                serviceResponse.setData(shahidVipServices);
            }
            if(request.getServiceType().matches("spotify") || request.getServiceType().toUpperCase().matches("SPOTIFY")){
                serviceResponse.setData(spotifyServices);
            }
            if(request.getServiceType().matches("razer_gold") || request.getServiceType().toUpperCase().matches("RAZER_GOLD")){
                serviceResponse.setData(razerGoldServices);
            }
            if(request.getServiceType().matches("ewallet") || request.getServiceType().toUpperCase().matches("EWALLET")){
                serviceResponse.setData(ewalletServices);
            }

            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }

        if(request.getServiceType() == null || request.getServiceType().matches("string") || request.getServiceType().isEmpty()){
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }else{
            serviceResponse.setError(new ErrorObject(PocketErrorCode.PAYNET_SERVICE_TYPE.getKeyString(), "Paynet service type isn't matched."));
            serviceResponse.setRequestId(request.getRequestId());
            serviceResponse.setStatus(PocketConstants.ERROR);
            serviceResponse.setData(null);
            return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
        }
    }

    @PostMapping(value = "/countryList")
    public ResponseEntity<BaseResponseObject> paynetCountries(@RequestBody PaynetEmptyRequest request) throws PocketException, IOException {
        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        request = (PaynetEmptyRequest) EncryptDecrypt.decryptRequest(request, PaynetEmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(paynetService.getPaynetAllCountries(request), HttpStatus.OK);
    }

    @PostMapping(value = "/providerListByCountryId")
    public ResponseEntity<BaseResponseObject> providerListByCountryId(@RequestBody PaynetCountryProviderRequest request) throws PocketException, IOException {
        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        request = (PaynetCountryProviderRequest) EncryptDecrypt.decryptRequest(request, PaynetCountryProviderRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(paynetService.getPaynetCountryProviders(request), HttpStatus.OK);
    }

    @PostMapping(value = "/operatorListByProviderId")
    public ResponseEntity<BaseResponseObject> operatorPriceListByProviderId(@RequestBody PaynetProviderOperatorRequest request) throws PocketException, IOException {
        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        request = (PaynetProviderOperatorRequest) EncryptDecrypt.decryptRequest(request, PaynetProviderOperatorRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(paynetService.getPaynetProviderOperators(request), HttpStatus.OK);
    }

    @PostMapping(value = "/balance")
    public ResponseEntity<BaseResponseObject> paynetBalance(@RequestBody PaynetEmptyRequest request) throws PocketException, IOException {
        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        request = (PaynetEmptyRequest) EncryptDecrypt.decryptRequest(request, PaynetEmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(paynetService.getPaynetBalance(request), HttpStatus.OK);
    }

    @PostMapping(value = "/infoDetail")
    public ResponseEntity<BaseResponseObject> paynetInfoDetail(@RequestBody PaynetEmptyRequest request) throws PocketException, IOException {
        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        request = (PaynetEmptyRequest) EncryptDecrypt.decryptRequest(request, PaynetEmptyRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);
        TsSessionObject tsSessionObject = requestValidator.validateSession(request);

        return new ResponseEntity<>(paynetService.getPaynetInfoDetail(request), HttpStatus.OK);
    }

    private SessionCheckWithCredentialRequest createRequest(PaynetBillPayRequest request) {
        if(request.getRequestId() == null || request.getRequestId().matches("string") || request.getRequestId().isEmpty()){
            request.setRequestId(paynetServiceImpl.requestId());
        }
        SessionCheckWithCredentialRequest withCredentialRequest=new SessionCheckWithCredentialRequest();
        withCredentialRequest.setCredential(request.getPin());
        withCredentialRequest.setOtpText(request.getOtpText());
        withCredentialRequest.setOtpSecret(request.getOtpSecret());
        withCredentialRequest.setHardwareSignature(request.getHardwareSignature());
        withCredentialRequest.setSessionToken(request.getSessionToken());
        withCredentialRequest.setRequestId(request.getRequestId());
        return withCredentialRequest;
    }

}
