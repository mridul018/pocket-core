package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "app")
public class DownloadFileEndpoint extends BaseEndpoint {
    static LogWriterUtility logWriterUtility = new LogWriterUtility(DownloadFileEndpoint.class);

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/pocket_documents", method = RequestMethod.GET)
    public HttpEntity<?> getImageWithValue(
            @RequestParam(value = "fileName") String fileName,
            @RequestParam(value = "isEncodingNeeded", defaultValue = "false") String isEncodingNeeded) throws IOException, PocketException {

        String requestId = UUID.randomUUID().toString();
        logWriterUtility.debug(requestId, "File Name :" + fileName);

        if (fileName == null || fileName.trim().length() == 0) {
            logWriterUtility.error(requestId, "File Name null");
            return new HttpEntity<byte[]>(null, null);
        }

        String home=System.getProperty("user.home");
        String basePath = environment.getProperty("file_base_path");
        Path path = Paths.get(home+""+basePath, fileName);

        logWriterUtility.info(requestId, "Home :"+home);
        logWriterUtility.info(requestId, "basePath :"+basePath);


        if (path.toString().length() > 0) {
            byte[] data;
            try {
                data = Files.readAllBytes(path);
            } catch (IOException e) {
                logWriterUtility.error(requestId, e.getMessage());
                return new HttpEntity<>(null, null);
            }

            if (isEncodingNeeded != null && isEncodingNeeded.equalsIgnoreCase("true")) {
                String reData = Base64.getEncoder().encodeToString(data);
                HttpHeaders header = new HttpHeaders();
                header.set(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=" + fileName.replace(" ", "_"));
                header.setContentLength(reData.length());
                return new HttpEntity<>(reData, header);
            }

            HttpHeaders header = new HttpHeaders();
            header.set(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=" + fileName.replace(" ", "_"));
            header.setContentLength(data.length);
            return new HttpEntity<>(data, header);
        }else{
            logWriterUtility.error(requestId, "Path null");

        }
        return new HttpEntity<byte[]>(null, null);

    }

    @RequestMapping(value = "/pocket_documents_object", method = RequestMethod.GET)
    public ResponseEntity<BaseResponseObject> getImageWithValueObject(
            @RequestParam(value = "fileName") String fileName,
            @RequestParam(value = "isEncodingNeeded", defaultValue = "false") String isEncodingNeeded) throws IOException, PocketException {

        String requestId = UUID.randomUUID().toString();
        String basePath = environment.getProperty("file_base_path");
        Path path = Paths.get(basePath, fileName);

        if (path != null && path.toString().length() > 0) {
            byte[] data;
            try {
                data = Files.readAllBytes(path);
            } catch (IOException e) {
                logWriterUtility.error(requestId, e.getMessage());
                throw new PocketException(PocketErrorCode.FileOrFolderNotFound);
            }

            if (isEncodingNeeded != null && isEncodingNeeded.equalsIgnoreCase("true")) {
                String reData = Base64.getEncoder().encodeToString(data);
                HttpHeaders header = new HttpHeaders();
                header.set(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=" + fileName.replace(" ", "_"));
                header.setContentLength(reData.length());
                BaseResponseObject baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(null);
                baseResponseObject.setData(reData);
                baseResponseObject.setStatus(PocketConstants.OK);
                return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);

//                return new HttpEntity<>(reData, header);
            }

            HttpHeaders header = new HttpHeaders();
            header.set(HttpHeaders.CONTENT_DISPOSITION,
                    "attachment; filename=" + fileName.replace(" ", "_"));
            header.setContentLength(data.length);
            BaseResponseObject baseResponseObject = new BaseResponseObject();
            baseResponseObject.setError(null);
            baseResponseObject.setData(data);
            baseResponseObject.setStatus(PocketConstants.OK);
            return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
        }

        BaseResponseObject baseResponseObject = new BaseResponseObject();
        baseResponseObject.setError(new ErrorObject(PocketErrorCode.UNEXPECTED_ERROR_OCCURRED));
        baseResponseObject.setData(null);
        baseResponseObject.setStatus(PocketConstants.OK);
        return new ResponseEntity<>(baseResponseObject, HttpStatus.BAD_REQUEST);
    }
}