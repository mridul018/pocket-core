package tech.ipocket.pocket.controllers.app.ts;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.ts.feeShairing.FeeSharingService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.LogWriterUtility;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

import java.util.UUID;

@RestController
@RequestMapping(value = "/v1")
public class FeeSharingControllerOld extends BaseEndpoint {

    private LogWriterUtility logWriterUtility=new LogWriterUtility(this.getClass());

    @Autowired
    private FeeSharingService feeSharingService;

    @GetMapping("feeSharing")
    public ResponseEntity<BaseResponseObject> makeTransactionProfile() throws PocketException {

        String requestId= UUID.randomUUID().toString();

        BaseResponseObject response = feeSharingService.feeSharing(requestId);
        if (response != null && response.getStatus().equals(PocketConstants.OK)) {
            logWriterUtility.debug(requestId, "EOD Fee Sharing Successful");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            logWriterUtility.error(requestId, "EOD Fee Sharing failed");
            return new ResponseEntity<>(response, HttpStatus.EXPECTATION_FAILED);
        }
    }
}
