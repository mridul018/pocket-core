package tech.ipocket.pocket.controllers.app.ts;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.request.ts.bank.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.services.security.RequestValidator;
import tech.ipocket.pocket.services.ts.BankService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.EncryptDecrypt;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "app")
public class BankController extends BaseEndpoint {

    @Autowired
    private BankService bankService;


    @Autowired
    private RequestValidator requestValidator;

    @PostMapping(value = "/createBank")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createBank(@RequestBody CreateBankRequest request) throws PocketException {

        request = (CreateBankRequest) EncryptDecrypt.decryptRequest(request, CreateBankRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(bankService.createBank(request), HttpStatus.OK);

    }

    @PostMapping(value = "/getBanks")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getBanks(@RequestBody EmptyRequest request) throws PocketException {

        request = (EmptyRequest) EncryptDecrypt.decryptRequest(request, EmptyRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(bankService.getBanks(request), HttpStatus.OK);

    }

    @PostMapping(value = "/createBranch")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createBranch(@RequestBody CreateBranchRequest request) throws PocketException {

        request = (CreateBranchRequest) EncryptDecrypt.decryptRequest(request, CreateBranchRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(bankService.createBranch(request), HttpStatus.OK);

    }

    @PostMapping(value = "/getBranchList")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getBranchList(@RequestBody GetBranchByBankCodeRequest request) throws PocketException {

        request = (GetBranchByBankCodeRequest) EncryptDecrypt.decryptRequest(request, GetBranchByBankCodeRequest.class);

        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(bankService.getBranchList(request), HttpStatus.OK);

    }


    @PostMapping(value = "/updateBranch")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateBranch(@RequestBody UpdateBranchRequest request) throws PocketException {


        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        return new ResponseEntity<>(bankService.updateBranch(request), HttpStatus.OK);
    }

}
