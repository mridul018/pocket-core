package tech.ipocket.pocket.controllers.app.um;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.security.TokenValidateRequest;
import tech.ipocket.pocket.request.um.ProfilePicUpdateRequest;
import tech.ipocket.pocket.request.um.UpdateDocumentRequest;
import tech.ipocket.pocket.request.um.UserSettingsRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.response.um.UserSettingsResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.ProfileService;
import tech.ipocket.pocket.services.um.settings.UserSettingsService;
import tech.ipocket.pocket.utils.*;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "app")
public class UserSettingsController extends BaseEndpoint {

    @Autowired
    private UserSettingsService userSettingsService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private ProfileService profileService;

    @PostMapping(value = "/settings")
    public ResponseEntity<BaseResponseObject> userSettings(@RequestBody UserSettingsRequest request) throws PocketException {
        request = (UserSettingsRequest) EncryptDecrypt.decryptRequest(request, UserSettingsRequest.class);
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);
        //TODO verify token
        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(
                new TokenValidateRequest(request.getSessionToken(), request.getHardwareSignature())
        );
        if (tokenValidateUmResponse == null) {
            response.setError(new ErrorObject(PocketErrorCode.TOKEN_EXPIRE));
            response.setStatus(PocketConstants.ERROR);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        UserSettingsResponse userSettings = userSettingsService.getUserSettings(request.getLoginId());
        response.setData(userSettings);
        response.setError(null);
        response.setStatus(PocketConstants.OK);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ApiOperation(value = "This api is about to update user document")
    @PostMapping(value = "/updateProfilePicture")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> updateProfilePicture(@RequestBody ProfilePicUpdateRequest request) throws PocketException {
        request = (ProfilePicUpdateRequest) EncryptDecrypt.decryptRequest(request, UpdateDocumentRequest.class);
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        TokenValidateUmResponse validateResponse = securityService.validateToken(request);


        baseResponseObject = profileService.updateProfilePicture(validateResponse, request);

        return new ResponseEntity<>(baseResponseObject, HttpStatus.OK);
    }
}
