package tech.ipocket.pocket.controllers.external;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.swagger.annotations.Api;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.configuration.PayWellConfig;
import tech.ipocket.pocket.entity.DapiAccessToken;
import tech.ipocket.pocket.entity.UmExternalActivityLog;
import tech.ipocket.pocket.repository.topup.Product;
import tech.ipocket.pocket.repository.topup.Provider;
import tech.ipocket.pocket.repository.um.UmBeneficiaryRepository;
import tech.ipocket.pocket.request.external.*;
import tech.ipocket.pocket.request.external.amy.*;
import tech.ipocket.pocket.request.external.banks.*;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.external.amy.*;
import tech.ipocket.pocket.response.external.ExternalResponse;
import tech.ipocket.pocket.response.external.banks.*;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.services.external.ExternalFactory;
import tech.ipocket.pocket.services.external.ExternalService;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.services.um.CommonService;
import tech.ipocket.pocket.utils.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import co.dapi.Config;
import co.dapi.DapiApp;
import okhttp3.Response;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/v1/external")
@Api(tags = "external")
public class ExternalController extends BaseEndpoint{

    private static final String TAG = "ExternalController";
    LogWriterUtility log = new LogWriterUtility(ExternalController.class);

    @Autowired
    private ExternalFactory externalFactory;
    @Autowired
    private ExternalService externalService;
    @Autowired
    CommonService commonService;
    @Autowired
    UmBeneficiaryRepository umBeneficiaryRepository;
    @Autowired
    private SecurityService securityService;
    @Autowired
    PayWellConfig payWellConfig;
    private UmExternalActivityLog externalActivityLog;

    @Autowired
    private Environment environment;

    /***
     *
     * General Request for external activity
     */

    DapiApp dapiApp;

    @PostConstruct
    public void init(){
        log.info(TAG, "init :  PostConstruct before");

        dapiApp = new DapiApp(new Config(environment.getProperty("dapi.appsecret")));
        log.info(TAG, "init :  PostConstruct after");

    }

    @PostMapping(value = "handleSDKRequest", produces = MediaType.APPLICATION_JSON_VALUE)
    public String handleSDKRequest(@RequestBody String reqBodyJSON, @RequestHeader HashMap<String, String> headersMap) throws IOException {
        log.info(TAG, "handleSDKRequest reqBodyJSON (PostMapping):"+reqBodyJSON);
        System.out.println( "handleSDKRequest reqBodyJSON (PostMapping):"+reqBodyJSON);
        System.out.println( "Hello test");

        for (String key: headersMap.keySet()) {
            System.out.println("key : " + key);
            System.out.println("value : " + headersMap.get(key));
        }

        Response response = null;
        try{
            response =  dapiApp.handleSDKRequest(reqBodyJSON, headersMap);

        }catch (Exception e){
            log.info(TAG, "handleSDKRequest Response:"+e.getMessage());
            throw e;
        }

        String responseBodyJSON = "";
        if(response != null){
            okhttp3.ResponseBody responseBody = response.body();
            if (responseBody != null) responseBodyJSON = responseBody.string();
            System.out.println(" Response body : " + responseBodyJSON);

            ObjectMapper mapper = new ObjectMapper();
            log.info(TAG, " Response : "+mapper.writeValueAsString(response));
            log.info(TAG, " RespBodyJSON Response : "+mapper.writeValueAsString(responseBodyJSON));
        }else{
            log.info(TAG, " Response : is null ");
        }

        return responseBodyJSON;
    }

    @PostMapping(value = "/services")
    public ResponseEntity<BaseResponseObject> externalServices(@RequestBody ExternalRequest request) throws PocketException, JSONException {

        log.info(TAG,"Enter external service method");

        log.info(TAG,"Call activity log with status: 10 and message: pending");

        if(!request.getType().equalsIgnoreCase(ExternalType.POLLIBUDDIT_BILLPAY_CONFIRMATION)) {
            externalActivityLog = sendExternalActivityPreLog(request, 10, "Pending");

            if(externalActivityLog != null){
                log.info(TAG, "After set pre activity log, response id is: " + externalActivityLog.getId());
            }
        }

        log.info(TAG, "Call External Service " + request.getType() + " this method");

        ExternalResponse responseObject = externalFactory.externalServices(request);

        log.info(TAG, "After getting response from external server, the response is: "+new Gson().toJson(responseObject));

        log.info(TAG, "Now change status into external activity log ");

        if(externalActivityLog != null){
            if(!request.getType().equalsIgnoreCase(ExternalType.POLLIBUDDIT_BILLPAY_CONFIRMATION))
                sendExternalActivityPostLog(request, responseObject, externalActivityLog.getId());
        }

        log.info(TAG, "Send response to requester");

        BaseResponseObject response = new BaseResponseObject();

        if(responseObject.isAbleToSend()){
            response.setData(responseObject.getData());
            response.setError(null);
            response.setStatus(responseObject.getStatus());
        }else{
            response.setData(null);
            response.setError(responseObject.getError());
            response.setStatus(responseObject.getStatus());
        }



        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * Bank connection throw external
     */
    @PostMapping(value = "/bankList/get")
    public ResponseEntity<BaseResponseObject> getBankList(@RequestBody BankListRequest request) throws PocketException {

        request = (BankListRequest) EncryptDecrypt.decryptRequest(request, BankListRequest.class);

        log.info(TAG,"Enter bank list (get) service");

        BaseResponseObject response = new BaseResponseObject();
        log.info(TAG, "Bank List (get) validation process complete");

        validateRequest(request,response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);
        if(!tokenValidateUmResponse.isValid())
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);

        BankListResponse bankListResponse = externalService.bankListByUserPhone(request);

        log.info(TAG, "Bank list from server: "+ bankListResponse.getBankLists());

        response.setData(bankListResponse);
        response.setStatus(PocketConstants.OK);

        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @PostMapping(value = "/bank/add")
    public ResponseEntity<BaseResponseObject> addANewBank(@RequestBody BankAddRequest request) throws PocketException {

        request = (BankAddRequest) EncryptDecrypt.decryptRequest(request, BankAddRequest.class);

        log.info(TAG, "Enter bank add service");
        BaseResponseObject response = new BaseResponseObject();
        log.info(TAG,"Validation of a new bank entry is complete");

        validateRequest(request,response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);
        if(!tokenValidateUmResponse.isValid())
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);

        BankAddResponse bankAddResponse = externalService.addANewBank(request);
        log.info(TAG, "Response after save a new bank in the server: "+new Gson().toJson(bankAddResponse));

        response.setData(bankAddResponse);
        response.setStatus(PocketConstants.OK);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/bank/meta-data/get")
    public ResponseEntity<BaseResponseObject> getBankMetaData(@RequestBody BankDataRequest request) throws PocketException{

        request = (BankDataRequest) EncryptDecrypt.decryptRequest(request, BankDataRequest.class);

        log.info(TAG, "Request for Bank Meta data");
        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request,response);

        /*TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);
        if(!tokenValidateUmResponse.isValid())
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);*/

        log.info(TAG, "Call to external impl for response");
        BankMetadataResponse bankMetadataResponse = externalService.bankData(request);

        log.info(TAG, "Response from service is: "+new Gson().toJson(bankMetadataResponse));

        if(bankMetadataResponse!=null){
            response.setData(bankMetadataResponse);
            response.setError(null);
            response.setStatus(PocketConstants.OK);
        }else{
            response.setData(null);
            response.setErrorCode(PocketErrorCode.BANK_DATA_NOT_FOUND);
            response.setStatus(PocketConstants.ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/bank/user-input/send")
    public ResponseEntity<BaseResponseObject> sendUserInputInformation(@RequestBody BankUserInputRequest request) throws PocketException {

        request = (BankUserInputRequest) EncryptDecrypt.decryptRequest(request,BankUserInputRequest.class);

        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request,response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);
        if(!tokenValidateUmResponse.isValid())
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);

        BankUserInputResponse bankUserInputResponse = externalService.sendBankUserInput(request);

        if(bankUserInputResponse == null){
            response.setData(null);
            response.setErrorCode(PocketErrorCode.BANK_USER_INPUT_ERROR);
            response.setStatus(PocketConstants.ERROR);
        }else{
            response.setData(bankUserInputResponse);
            response.setError(null);
            response.setStatus(PocketConstants.OK);
        }


        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @PostMapping(value = "/bank/beneficiary/add")
    public ResponseEntity<BaseResponseObject> addANewBeneficiary(@RequestBody BankBeneficiaryAddRequest request) throws PocketException {

        request = (BankBeneficiaryAddRequest) EncryptDecrypt.decryptRequest(request,BankBeneficiaryAddRequest.class);

        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request,response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);
        if(!tokenValidateUmResponse.isValid())
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);

        BankBeneficiaryAddResponse bankBeneficiaryAddResponse = externalService.bankBeneficiaryAdd(request);

        if(bankBeneficiaryAddResponse == null){
            response.setData(null);
            response.setErrorCode(PocketErrorCode.BANK_BENEFICIARY_ADD_ERROR);
            response.setStatus(PocketConstants.ERROR);
        }else{
            response.setData(bankBeneficiaryAddResponse);
            response.setError(null);
            response.setStatus(PocketConstants.OK);
        }


        return new ResponseEntity<>(response,HttpStatus.OK);
    }


    @PostMapping(value = "/bank/beneficiary/get")
    public ResponseEntity<BaseResponseObject> getBankBeneficiary(@RequestBody BankBeneficiaryGetRequest request) throws PocketException{
        request = (BankBeneficiaryGetRequest) EncryptDecrypt.decryptRequest(request,BankBeneficiaryGetRequest.class);

        BaseResponseObject response = new BaseResponseObject();

        validateRequest(request,response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);
        if(!tokenValidateUmResponse.isValid())
            throw new PocketException(PocketErrorCode.TOKEN_EXPIRE);

        BankBeneficiaryGetResponse bankBeneficiaryGetResponse = externalService.bankBeneficiaryGet(request);

        if(bankBeneficiaryGetResponse == null){
            response.setData(null);
            response.setErrorCode(PocketErrorCode.BANK_BENEFICIARY_GET_ERROR);
            response.setStatus(PocketConstants.ERROR);
        }else{
            response.setData(bankBeneficiaryGetResponse);
            response.setError(null);
            response.setStatus(PocketConstants.OK);
        }


        return new ResponseEntity<>(response,HttpStatus.OK);
    }


    /**
     *
     * DING
     */
    @PostMapping(value = "/countryList")
    public ResponseEntity<BaseResponseObject> getCountryList(@RequestBody EmptyRequest request) throws PocketException, IOException {
        return new ResponseEntity<>(externalService.getCountryList(), HttpStatus.OK);
    }

    @PostMapping(value = "/providerList")
    public ResponseEntity<BaseResponseObject> getProviderList(@RequestBody ProviderListRequest request) throws PocketException, IOException {
        BaseResponseObject responseObject = new BaseResponseObject();
        Provider provider = externalService.getProviderList(request.getCountryIsos());
        if(provider != null){
            responseObject.setData(provider);
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
        }else{
            responseObject.setData(null);
            responseObject.setErrorCode(PocketErrorCode.PROVIDER_NOT_FOUND);
            responseObject.setStatus(PocketConstants.ERROR);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/productInfo")
    public ResponseEntity<BaseResponseObject> getProductInfo(@RequestBody ProductInfo request) throws PocketException, IOException {
        BaseResponseObject responseObject = new BaseResponseObject();
        Product product = externalService.getProductInfo(request);
        if(product != null){
            responseObject.setData(product);
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
        }else{
            responseObject.setData(null);
            responseObject.setErrorCode(PocketErrorCode.PRODUCT_NOT_FOUND);
            responseObject.setStatus(PocketConstants.ERROR);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    /**
     * amy for AirPlane Ticket purchase
     */
    @PostMapping(value = "/departureList")
    public ResponseEntity<BaseResponseObject> getDepartureList(@RequestBody DepartureListRequest request) throws PocketException {
        BaseResponseObject responseObject = new BaseResponseObject();
        DepartureListResponse departureList = externalService.getDepartureList(request);
        if(departureList == null){
            responseObject.setError(new ErrorObject(PocketErrorCode.DEPARTURE_LIST_NOT_FOUND));
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }else{
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
            responseObject.setData(departureList);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/destinationList")
    public ResponseEntity<BaseResponseObject> getDestinationList(@RequestBody DestinationListRequest request) throws PocketException{
        BaseResponseObject responseObject = new BaseResponseObject();
        DestinationListResponse destinationList = externalService.getDestinationList(request);
        if(destinationList == null){
            responseObject.setError(new ErrorObject(PocketErrorCode.DESTINATION_LIST_NOT_FOUND));
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }else{
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
            responseObject.setData(destinationList);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/flightSearch")
    public ResponseEntity<BaseResponseObject> doFlightSearch(@RequestBody FlightSearchRequest request) throws PocketException {
        BaseResponseObject responseObject = new BaseResponseObject();
        FlightSearchResponse flightSearchResponse = externalService.getFlightSearchResponse(request);
        if(flightSearchResponse == null){
            responseObject.setError(new ErrorObject(PocketErrorCode.FLIGHT_NOT_FOUND));
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }else{
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
            responseObject.setData(flightSearchResponse);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/flightPrice")
    public ResponseEntity<BaseResponseObject> getFlightPrice(@RequestBody FlightPriceRequest request) throws PocketException {
        BaseResponseObject responseObject = new BaseResponseObject();
        FlightPriceResponse flightPriceResponse = externalService.getFlightPrices(request);
        if(flightPriceResponse == null){
            responseObject.setError(new ErrorObject(PocketErrorCode.FLIGHT_PRICE_ERROR));
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }else{
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
            responseObject.setData(flightPriceResponse);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/flightPassengerValidation")
    public ResponseEntity<BaseResponseObject> doPassengerValidation(@RequestBody PassengerValidationRequest request) throws PocketException {
        BaseResponseObject responseObject = new BaseResponseObject();
        PassengerValidationResponse response = externalService.passengerValidation(request);
        if(response == null){
            responseObject.setError(new ErrorObject(PocketErrorCode.PASSENGER_VALIDATION_FAILED));
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }else{
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
            responseObject.setData(response);
        }

        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping(value = "/flightDocumentUpload")
    public ResponseEntity<BaseResponseObject> documentUpload(@RequestBody FlightDocumentUploadRequest request) throws PocketException {
        BaseResponseObject responseObject = new BaseResponseObject();
        FlightDocumentUploadResponse flightDocumentUploadResponse = externalService.documentUpload(request);
        if(flightDocumentUploadResponse == null){
            responseObject.setError(new ErrorObject(PocketErrorCode.DOCUMENT_UPLOAD_ERROR));
            responseObject.setStatus(PocketConstants.ERROR);
            responseObject.setData(null);
        }else{
            responseObject.setError(null);
            responseObject.setStatus(PocketConstants.OK);
            responseObject.setData(flightDocumentUploadResponse);
        }
        return null;
    }


    /**
     * Send activity into server
     */
    private UmExternalActivityLog sendExternalActivityPreLog(ExternalRequest request, int status, String message){

        Timestamp sendTime = new Timestamp(new Date().getTime());

        if(request.getType().equalsIgnoreCase(ExternalType.TOPUP_DING) || request.getType().equalsIgnoreCase(ExternalType.PAYWELL_TOPUP)){
            BigDecimal amountToSend = new BigDecimal(request.getAmount());
            if(request.getType().equalsIgnoreCase(ExternalType.TOPUP_DING)){
                return commonService.addExternalActivityPreLog(
                        ExternalType.TOPUP_DING,
                        "UAE",
                        request.getFrom(),request.getTo(),request.getDingTopUpRequest().getSkuCode(),
                        request.getDingTopUpRequest().getSkuCode(),null,null,"","",amountToSend,
                        status,
                        message,
                        sendTime,"https://api.dingconnect.com/api/V1",
                        false,true,"Ding",
                        request.getDingTopUpRequest().getRefNumber()
                );
            }else{
                return commonService.addExternalActivityPreLog(
                        ExternalType.PAYWELL_TOPUP,
                        "BD",
                        request.getFrom(),request.getPayWellTopUpRequest().getMsisdn(),
                        request.getPayWellTopUpRequest().getOperator(),
                        request.getPayWellTopUpRequest().getConnection(),null,null,"","",amountToSend,
                        status,
                        message,
                        sendTime,"https://www.paywellonline.com:443/National_topupsoap/paywellservice.php",
                        false, true,"PayWell",
                        request.getPayWellTopUpRequest().getCrid()
                );
            }


        }else if(request.getType().equalsIgnoreCase(ExternalType.POLLIBUDDIT_BILLPAY)){
            BigDecimal amount = new BigDecimal(request.getPolliBudditBillRequest().getAmount());
            return commonService.addExternalActivityPreLog(
                    ExternalType.POLLIBUDDIT_BILLPAY,
                    "BD",
                    request.getTo(),
                    null,
                    null,
                    null,
                    request.getPolliBudditBillRequest().getFullName(),
                    request.getPolliBudditBillRequest().getPhoneNo(),
                    request.getPolliBudditBillRequest().getBill_no(),
                    request.getPolliBudditBillRequest().getAccount_no(),
                    amount,
                    status,
                    message,
//                    sendTime,"https://agentapi.paywellonline.com/PollyBiddyutSystem/pollyBiddyutBillPay",
                    sendTime,"https://agentapi.paywellonline.com/Utility/PollyBiddyut/billPayment",
                    false,true,"PayWell BillPay",
                    request.getPolliBudditBillRequest().getRef_id()
            );
        }
        return null;
    }


    private void sendExternalActivityPostLog(ExternalRequest request, ExternalResponse response, int id){
        Timestamp sendTime = new Timestamp(new Date().getTime());
        int status = response.getError()==null?200:Integer.parseInt(response.getError().getErrorCode());
        String message = response.getError()==null?"Success":response.getError().getErrorMessage();
        if(request.getType().equalsIgnoreCase(ExternalType.TOPUP_DING) || request.getType().equalsIgnoreCase(ExternalType.PAYWELL_TOPUP)){
            BigDecimal amountToSend = new BigDecimal(request.getAmount());
            if(request.getType().equalsIgnoreCase(ExternalType.TOPUP_DING)){
                commonService.addExternalActivityPostLog(
                        id,
                        ExternalType.TOPUP_DING,
                        "UAE",
                        request.getFrom(),request.getTo(),request.getDingTopUpRequest().getSkuCode(),
                        request.getDingTopUpRequest().getSkuCode(),null,null,"","",amountToSend,
                        status,
                        message,
                        sendTime,"https://api.dingconnect.com/api/V1",
                        false,true,"Ding",
                        request.getDingTopUpRequest().getRefNumber()
                );
            }else{
                commonService.addExternalActivityPostLog(
                        id,
                        ExternalType.PAYWELL_TOPUP,
                        "BD",
                        request.getFrom(),request.getPayWellTopUpRequest().getMsisdn(),
                        request.getPayWellTopUpRequest().getOperator(),
                        request.getPayWellTopUpRequest().getConnection(),null,null,"","",amountToSend,
                        status,
                        message,
                        sendTime,"https://www.paywellonline.com:443/National_topupsoap/paywellservice.php",
                        false,true,"PayWell",
                        request.getPayWellTopUpRequest().getCrid()
                );
            }


        }else if(request.getType().equalsIgnoreCase(ExternalType.POLLIBUDDIT_BILLPAY)){
            BigDecimal amount = new BigDecimal(request.getPolliBudditBillRequest().getAmount());
            commonService.addExternalActivityPostLog(
                    id,
                    ExternalType.POLLIBUDDIT_BILLPAY,
                    "BD",
                    request.getTo(),
                    null,
                    null,
                    null,
                    request.getPolliBudditBillRequest().getFullName(),
                    request.getPolliBudditBillRequest().getPhoneNo(),
                    request.getPolliBudditBillRequest().getBill_no(),
                    request.getPolliBudditBillRequest().getAccount_no(),
                    amount,
                    status,
                    message,
                    sendTime,"https://agentapi.paywellonline.com/PollyBiddyutSystem/pollyBiddyutBillPay",
                    false,true,"PayWell BillPay",
                    request.getPolliBudditBillRequest().getRef_id()
            );
        }
    }

    @GetMapping("Ping")
    public ResponseEntity<String> Ping() throws PocketException {

        return new ResponseEntity<>("Pocket Server", HttpStatus.OK);
    }

    @GetMapping("Health")
    public ResponseEntity<String> Health() throws PocketException {

        return new ResponseEntity<>("Pocket Server", HttpStatus.OK);
    }

    @PostMapping(value = "/StoreAccessToken")
    public ResponseEntity<String> StoreAccessToken(@RequestBody StoreAccessToken request) throws PocketException {
        log.info(TAG,"Dapi Storning token");

        Boolean response = externalService.storeAccessToken(request);
        String responseMessage = "";
        if(response == null){
            responseMessage = "Token store failed";
            return new ResponseEntity<>(responseMessage, HttpStatus.OK);
        }else{
            responseMessage = "Token store Success";
            return new ResponseEntity<>(responseMessage, HttpStatus.BAD_REQUEST);
        }

    }
    @PostMapping(value = "/ApiResponse")
    public ResponseEntity<String> ApiResponse(HttpServletRequest requestHeader, @RequestBody Object request) throws PocketException {

        String dapiUrl = requestHeader.getHeader("Dapi-Url");
        log.info(TAG,"Dapi Request URL : "+dapiUrl);
        log.info(TAG,"Dapi Response : "+new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create().toJson(request));

        return new ResponseEntity<>("Ok pocket server", HttpStatus.OK);

    }

    @GetMapping("GetAccessToken")
    public ResponseEntity<DapiAccessToken> GetAccessToken(@RequestParam String id) throws PocketException {
        log.info(TAG,"Dapi Getting token for : "+id);
        DapiAccessToken dapiAccessToken = externalService.getAccessToken(id);
        return new ResponseEntity<>(dapiAccessToken, HttpStatus.OK);
    }
    @GetMapping("GetAccessTokenAll")
    public ResponseEntity<List<DapiAccessToken>> GetAccessTokenAll() throws PocketException {
        List<DapiAccessToken> dapiAccessTokens = externalService.getAccessToken();
        return new ResponseEntity<>(dapiAccessTokens, HttpStatus.OK);
    }
}
