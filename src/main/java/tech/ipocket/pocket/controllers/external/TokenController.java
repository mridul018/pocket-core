package tech.ipocket.pocket.controllers.external;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.ipocket.pocket.request.security.TokenPinValidateRequest;
import tech.ipocket.pocket.request.ts.EmptyRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.security.TokenValidateUmResponse;
import tech.ipocket.pocket.services.security.SecurityService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

@RestController
@RequestMapping(value = "/v1/external")
@Api(tags = "external")
public class TokenController extends BaseEndpoint {

    @Autowired
    private SecurityService securityService;

    @PostMapping(value = "/verifyToken")
    public ResponseEntity<BaseResponseObject> tokenVerification(@RequestBody EmptyRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.validateToken(request);

        response.setRequestId(request.getRequestId());
        response.setData(tokenValidateUmResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);


        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/verifyTokenAndCredential")
    public ResponseEntity<BaseResponseObject> verifyTokenAndCredential(@RequestBody TokenPinValidateRequest request) throws PocketException {
        BaseResponseObject response = new BaseResponseObject();
        validateRequest(request, response);

        TokenValidateUmResponse tokenValidateUmResponse = securityService.verifyTokenAndCredential(request);

        response.setRequestId(request.getRequestId());
        response.setData(tokenValidateUmResponse);
        response.setError(null);
        response.setStatus(PocketConstants.OK);


        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
