package tech.ipocket.pocket.controllers.integration.zenzero;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayUserBalanceRequest;
import tech.ipocket.pocket.request.integration.zenzero.ZenZeroPayUserInfoRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.zenzero.ZenZeroPayPaymentWalletRefillResponse;
import tech.ipocket.pocket.services.external.ExtApiConsumerService;
import tech.ipocket.pocket.services.integration.zenzero.ZenZeroPayPaymentService;
import tech.ipocket.pocket.utils.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/v1/app/zenzero")
@Api(tags = "zenzero")
public class ZenZeroPayPaymentController extends BaseEndpoint {
    @Autowired
    private ZenZeroPayPaymentService zenZeroPayPaymentService;
    @Autowired
    private ExtApiConsumerService apiConsumerService;
    @Autowired
    private Environment environment;

    @ApiOperation(value = "This api is accessed by ZenZero server to get user info by requestId or walletNumber")
    @PostMapping(value = "/userInfo")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserInfo(HttpServletRequest httpServletRequest,
                                                   @RequestBody ZenZeroPayUserInfoRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkZenZeroConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(zenZeroPayPaymentService.getUserInfo(request));
        String signature = apiConsumerService.signZenZeroPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("ZENZEROPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by ZenZero server for amount refill or transaction")
    @PostMapping(value = "/walletRefill")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createWalletRefill(HttpServletRequest httpServletRequest,
                                                               @RequestBody ZenZeroPayPaymentWalletRefillRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkZenZeroConsumerValidity(httpServletRequest, request);

        if(ServiceCodeConstants.CashInFromZenZeroKiosk.equals("1023")){
            String isBankCashInOpen = environment.getProperty("IS_BANK_CASH_IN_OPEN");
            if(isBankCashInOpen!=null && isBankCashInOpen.equalsIgnoreCase("false")){
                throw new PocketException(PocketErrorCode.ThisFeatureIsOffNow);
            }else{
                ZenZeroPayPaymentWalletRefillResponse zenZeroPayPaymentWalletRefillResponse = zenZeroPayPaymentService.walletRefill(request);
                baseResponseObject.setRequestId(zenZeroPayPaymentWalletRefillResponse.getRequestId());
                baseResponseObject.setData(zenZeroPayPaymentWalletRefillResponse);
            }
        }else {
            throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        String signature = apiConsumerService.signZenZeroPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("ZENZEROPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by ZenZero server for refill or transaction details by requestId or tokenNo")
    @PostMapping(value = "/checkWalletRefillByToken")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkWalletRefillByToken(HttpServletRequest httpServletRequest,
                                                                 @RequestBody ZenZeroPayPaymentWalletRefillFindRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkZenZeroConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(zenZeroPayPaymentService.checkWalletRefillByToken(request));

        String signature = apiConsumerService.signZenZeroPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("ZENZEROPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }

    @ApiOperation(value = "This API is for getting user balance. An ZenZero`s user check their current balance through this service")
    @PostMapping(value = "/checkWalletBalance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkCustomerWalletBalance(HttpServletRequest httpServletRequest,
                                                      @RequestBody ZenZeroPayUserBalanceRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkZenZeroConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(zenZeroPayPaymentService.checkCustomerWalletBalance(request));

        String signature = apiConsumerService.signZenZeroPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("ZENZEROPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }
}
