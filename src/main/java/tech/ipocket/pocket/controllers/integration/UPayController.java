package tech.ipocket.pocket.controllers.integration;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.integration.*;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.integration.UPayTransactionResponse;
import tech.ipocket.pocket.services.external.ExtApiConsumerService;
import tech.ipocket.pocket.services.integration.UPayService;
import tech.ipocket.pocket.utils.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/v1/app")
@Api(tags = "upay")
public class UPayController extends BaseEndpoint {

    @Autowired
    private UPayService upayService;

    @Autowired
    private ExtApiConsumerService apiConsumerService;

    @Autowired
    private Environment environment;

    @ApiOperation(value = "This api is accessed by upay server to get user details by requestId or walletNumber")
    @PostMapping(value = "/userDetails")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> userDetails(HttpServletRequest httpServletRequest,
                                                   @RequestBody UPayUserInfoRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(upayService.getUserInfoDetails(request));
        String signature = apiConsumerService.signPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("UPAY_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by upay server for amount refill or transaction")
    @PostMapping(value = "/refillTransaction")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createRefillTransaction(HttpServletRequest httpServletRequest,
                                                               @RequestBody UPayTransactionRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkConsumerValidity(httpServletRequest, request);

        if(ServiceCodeConstants.CashInFromUPayKiosk.equals("1015")){
            String isBankCashInOpen=environment.getProperty("IS_BANK_CASH_IN_OPEN");
            if(isBankCashInOpen!=null && isBankCashInOpen.equalsIgnoreCase("false")){
                throw new PocketException(PocketErrorCode.ThisFeatureIsOffNow);
            }else{
                UPayTransactionResponse uPayTransactionResponse = upayService.refillUpayTransaction(request);
                baseResponseObject.setRequestId(uPayTransactionResponse.getRequestId());
                baseResponseObject.setData(uPayTransactionResponse);
            }
        }else {
            throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        String signature = apiConsumerService.signPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("UPAY_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by upay server for refill or transaction details by requestId or tokenNo")
    @PostMapping(value = "/transactionFindByToken")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> transactionDetailsByToken(HttpServletRequest httpServletRequest,
                                                                 @RequestBody UPayTransactionFindRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(upayService.findUPayTransactionByToken(request));

        String signature = apiConsumerService.signPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("UPAY_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }


    @ApiOperation(value = "This API is for getting user balance. An Upay`s user check their current balance through this service")
    @PostMapping(value = "/balance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserBalance(HttpServletRequest httpServletRequest,
                                                      @RequestBody UpayUserBalanceRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(upayService.getUserBalance(request));

        String signature = apiConsumerService.signPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("UPAY_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }
}
