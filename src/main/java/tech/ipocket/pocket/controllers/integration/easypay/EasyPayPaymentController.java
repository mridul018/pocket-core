package tech.ipocket.pocket.controllers.integration.easypay;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayUserBalanceRequest;
import tech.ipocket.pocket.request.integration.easypay.EasyPayUserInfoRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.integration.easypay.EasyPayPaymentWalletRefillResponse;
import tech.ipocket.pocket.services.external.ExtApiConsumerService;
import tech.ipocket.pocket.services.integration.easypay.EasyPayPaymentService;
import tech.ipocket.pocket.utils.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/v1/app/easypay")
@Api(tags = "easypay")
public class EasyPayPaymentController extends BaseEndpoint {
    @Autowired
    private EasyPayPaymentService easyPayPaymentService;
    @Autowired
    private ExtApiConsumerService apiConsumerService;
    @Autowired
    private Environment environment;

    @ApiOperation(value = "This api is accessed by EasyPay server to get user info by requestId or walletNumber")
    @PostMapping(value = "/userInfo")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserInfo(HttpServletRequest httpServletRequest,
                                                   @RequestBody EasyPayUserInfoRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkEasyPayConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(easyPayPaymentService.getUserInfo(request));
        String signature = apiConsumerService.signEasyPayPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("EASYPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by EasyPay server for amount refill or transaction")
    @PostMapping(value = "/walletRefill")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createWalletRefill(HttpServletRequest httpServletRequest,
                                                               @RequestBody EasyPayPaymentWalletRefillRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkEasyPayConsumerValidity(httpServletRequest, request);

        if(ServiceCodeConstants.CashInFromEasyPayKiosk.equals("2021")){
            String isBankCashInOpen = environment.getProperty("IS_BANK_CASH_IN_OPEN");
            if(isBankCashInOpen!=null && isBankCashInOpen.equalsIgnoreCase("false")){
                throw new PocketException(PocketErrorCode.ThisFeatureIsOffNow);
            }else{
                EasyPayPaymentWalletRefillResponse easyPayPaymentWalletRefillResponse = easyPayPaymentService.walletRefill(request);
                baseResponseObject.setRequestId(easyPayPaymentWalletRefillResponse.getRequestId());
                baseResponseObject.setData(easyPayPaymentWalletRefillResponse);
            }
        }else {
            throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        String signature = apiConsumerService.signEasyPayPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("EASYPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by EasyPay server for refill or transaction details by requestId or tokenNo")
    @PostMapping(value = "/checkWalletRefillByToken")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkWalletRefillByToken(HttpServletRequest httpServletRequest,
                                                                 @RequestBody EasyPayPaymentWalletRefillFindRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkEasyPayConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(easyPayPaymentService.checkWalletRefillByToken(request));

        String signature = apiConsumerService.signEasyPayPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("EASYPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }


    @ApiOperation(value = "This API is for getting user balance. An EasyPay`s user check their current balance through this service")
    @PostMapping(value = "/checkWalletBalance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkCustomerWalletBalance(HttpServletRequest httpServletRequest,
                                                      @RequestBody EasyPayUserBalanceRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkEasyPayConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(easyPayPaymentService.checkCustomerWalletBalance(request));

        String signature = apiConsumerService.signEasyPayPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("EASYPAY_SIGNATURE",signature);
        return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
    }
}
