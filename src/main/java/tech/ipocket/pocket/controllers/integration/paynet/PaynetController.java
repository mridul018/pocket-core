package tech.ipocket.pocket.controllers.integration.paynet;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.integration.paynet.PaynetPaymentWalletRefillFindRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetPaymentWalletRefillRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetUserBalanceRequest;
import tech.ipocket.pocket.request.integration.paynet.PaynetUserInfoRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.integration.paynet.PaynetPaymentWalletRefillResponse;
import tech.ipocket.pocket.services.external.ExtApiConsumerService;
import tech.ipocket.pocket.services.integration.paynet.PaynetApiService;
import tech.ipocket.pocket.utils.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/v1/app/paynet")
@Api(tags = "paynet")
public class PaynetController extends BaseEndpoint {
    @Autowired
    private PaynetApiService paynetService;
    @Autowired
    private ExtApiConsumerService apiConsumerService;
    @Autowired
    private Environment environment;

    @ApiOperation(value = "This api is accessed by Paynet server to get user info by requestId or walletNumber")
    @PostMapping(value = "/userInfo")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> getUserInfo(HttpServletRequest httpServletRequest,
                                                   @RequestBody PaynetUserInfoRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkPaynetConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(paynetService.getUserInfo(request));
        String signature = apiConsumerService.signPaynetPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("PAYNET_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by Paynet server for amount refill or transaction")
    @PostMapping(value = "/walletRefill")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> createWalletRefill(HttpServletRequest httpServletRequest,
                                                               @RequestBody PaynetPaymentWalletRefillRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkPaynetConsumerValidity(httpServletRequest, request);

        if(ServiceCodeConstants.CashInFromPaynetKiosk.equals("1019")){
            String isBankCashInOpen=environment.getProperty("IS_BANK_CASH_IN_OPEN");
            if(isBankCashInOpen!=null && isBankCashInOpen.equalsIgnoreCase("false")){
                throw new PocketException(PocketErrorCode.ThisFeatureIsOffNow);
            }else{
                PaynetPaymentWalletRefillResponse paynetPaymentWalletRefillResponse = paynetService.walletRefill(request);
                baseResponseObject.setRequestId(paynetPaymentWalletRefillResponse.getRequestId());
                baseResponseObject.setData(paynetPaymentWalletRefillResponse);
            }
        }else {
            throw new PocketException(PocketErrorCode.UnSupportedTransactionType);
        }

        baseResponseObject.setError(null);
        baseResponseObject.setStatus(PocketConstants.OK);

        String signature = apiConsumerService.signPaynetPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("PAYNET_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }

    @ApiOperation(value = "This api is accessed by Paynet server for refill or transaction details by requestId or tokenNo")
    @PostMapping(value = "/checkWalletRefillByToken")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkWalletRefillByToken(HttpServletRequest httpServletRequest,
                                                                 @RequestBody PaynetPaymentWalletRefillFindRequest request) throws Exception {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkPaynetConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(paynetService.checkWalletRefillByToken(request));

        String signature = apiConsumerService.signPaynetPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("PAYNET_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }


    @ApiOperation(value = "This API is for getting user balance. An Paynet`s user check their current balance through this service")
    @PostMapping(value = "/checkWalletBalance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> checkCustomerWalletBalance(HttpServletRequest httpServletRequest,
                                                      @RequestBody PaynetUserBalanceRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        apiConsumerService.checkPaynetConsumerValidity(httpServletRequest, request);

        baseResponseObject.setData(paynetService.checkCustomerWalletBalance(request));

        String signature = apiConsumerService.signPaynetPayload(baseResponseObject);

        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.set("PAYNET_SIGNATURE",signature);
        return ResponseEntity.ok()
                .headers(responseHeader)
                .body(baseResponseObject);
    }
}
