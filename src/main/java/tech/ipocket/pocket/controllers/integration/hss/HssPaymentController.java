package tech.ipocket.pocket.controllers.integration.hss;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.ipocket.pocket.request.integration.hss.HssPaymentFindRequest;
import tech.ipocket.pocket.request.integration.hss.HssPaymentRequest;
import tech.ipocket.pocket.request.integration.hss.HssUserBalanceRequest;
import tech.ipocket.pocket.request.integration.hss.HssUserInfoRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.response.integration.hss.HssPaymentResponse;
import tech.ipocket.pocket.response.integration.hss.UserTokenRequest;
import tech.ipocket.pocket.services.external.ExtApiConsumerService;
import tech.ipocket.pocket.services.integration.hss.HssPaymentService;
import tech.ipocket.pocket.utils.BaseEndpoint;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketException;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/v1/app/hss")
@Api(tags = "hello-super-star")
public class HssPaymentController extends BaseEndpoint {
    @Autowired
    private HssPaymentService hssPaymentService;
    @Autowired
    private ExtApiConsumerService apiConsumerService;

    @Autowired
    private Environment environment;

    @ApiOperation(value = "This api is shown for user details by requestId or walletNumber")
    @PostMapping(value = "/verifyUserDetail")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> verifyUserDetail(HttpServletRequest httpServletRequest,
                                                   @RequestBody HssUserInfoRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);

        boolean verifyUserWalletPassword = hssPaymentService.verifyUserWalletPassword(request.getWalletNumber(), request.getUserWalletPassword());  //verify user wallet and password

        if(verifyUserWalletPassword){
            boolean tokenVerify = hssPaymentService.validUserToken(request.getSecurityToken()); //token verify
            if(tokenVerify){
                apiConsumerService.checkHssConsumerValidity(httpServletRequest, request);  //RSA verify

                baseResponseObject.setData(hssPaymentService.getUserInfoDetails(request));
                String signature = apiConsumerService.signHssPayload(baseResponseObject);

                HttpHeaders responseHeader = new HttpHeaders();
                responseHeader.set("HSS_SIGNATURE",signature);
                return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
            }else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject("400","Failed to verify security token."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return ResponseEntity.ok().body(baseResponseObject);
            }

        }else {
            baseResponseObject = new BaseResponseObject();
            baseResponseObject.setError(new ErrorObject("400","User Wallet Password Not Valid."));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return ResponseEntity.ok().body(baseResponseObject);
        }
    }

    @ApiOperation(value = "This API is for getting user balance. An user can check their current balance through this service")
    @PostMapping(value = "/balance")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> balance(HttpServletRequest httpServletRequest,
                                                      @RequestBody HssUserBalanceRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        boolean verifyUserWalletPassword = hssPaymentService.verifyUserWalletPassword(request.getWalletNumber(), request.getUserWalletPassword());  //verify user wallet and password
        if(verifyUserWalletPassword){
            boolean tokenVerify = hssPaymentService.validUserToken(request.getSecurityToken()); //token verify
            if(tokenVerify){
                apiConsumerService.checkHssConsumerValidity(httpServletRequest, request); //RSA verify

                baseResponseObject.setData(hssPaymentService.getUserBalanceDetails(request));
                String signature = apiConsumerService.signHssPayload(baseResponseObject);

                HttpHeaders responseHeader = new HttpHeaders();
                responseHeader.set("HSS_SIGNATURE",signature);
                return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
            }else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject("400","Failed to verify security token."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return ResponseEntity.ok().body(baseResponseObject);
            }
        }else {
            baseResponseObject = new BaseResponseObject();
            baseResponseObject.setError(new ErrorObject("400","User Wallet Password Not Valid."));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return ResponseEntity.ok().body(baseResponseObject);
        }
    }

    @ApiOperation(value = "This api is accessed for hello super star product payment")
    @PostMapping(value = "/payment")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> payment(HttpServletRequest httpServletRequest,
                                                               @RequestBody HssPaymentRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        boolean verifyUserWalletPassword = hssPaymentService.verifyUserWalletPassword(request.getWalletNumber(), request.getUserWalletPassword());  //verify user wallet and password
        if(verifyUserWalletPassword){
            boolean tokenVerify = hssPaymentService.validUserToken(request.getSecurityToken()); //token verify
            if(tokenVerify){
                apiConsumerService.checkHssConsumerValidity(httpServletRequest, request);  //RSA verify

                HssPaymentResponse hssPaymentResponse = hssPaymentService.payment(request);
                if(hssPaymentResponse != null){
                    baseResponseObject.setRequestId(request.getRequestId());
                    baseResponseObject.setStatus(PocketConstants.OK);
                    baseResponseObject.setData(hssPaymentResponse);

                    String signature = apiConsumerService.signHssPayload(baseResponseObject);

                    HttpHeaders responseHeader = new HttpHeaders();
                    responseHeader.set("HSS_SIGNATURE",signature);
                    return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
                }

            }else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject("400","Failed to verify security token."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return ResponseEntity.ok().body(baseResponseObject);
            }
        }else {
            baseResponseObject = new BaseResponseObject();
            baseResponseObject.setError(new ErrorObject("400","User Wallet Password Not Valid."));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return ResponseEntity.ok().body(baseResponseObject);
        }
        return null;
    }

    @ApiOperation(value = "This api is accessed for hello super star product payment details by requestId or tokenNo")
    @PostMapping(value = "/paymentVerifyDetailByToken")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> paymentVerifyDetailByToken(HttpServletRequest httpServletRequest,
                                                                 @RequestBody HssPaymentFindRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());
        validateRequest(request, baseResponseObject);

        boolean verifyUserWalletPassword = hssPaymentService.verifyUserWalletPassword(request.getWalletNumber(), request.getUserWalletPassword());  //verify user wallet and password
        if(verifyUserWalletPassword){
            boolean tokenVerify = hssPaymentService.validUserToken(request.getSecurityToken()); //token verify
            if(tokenVerify){
                apiConsumerService.checkHssConsumerValidity(httpServletRequest, request); //RSA verify

                baseResponseObject.setData(hssPaymentService.findPaymentDetails(request));
                baseResponseObject.setStatus(PocketConstants.OK);
                String signature = apiConsumerService.signHssPayload(baseResponseObject);

                HttpHeaders responseHeader = new HttpHeaders();
                responseHeader.set("HSS_SIGNATURE",signature);
                return ResponseEntity.ok().headers(responseHeader).body(baseResponseObject);
            }else {
                baseResponseObject = new BaseResponseObject();
                baseResponseObject.setError(new ErrorObject("400","Failed to verify security token."));
                baseResponseObject.setRequestId(request.getRequestId());
                baseResponseObject.setStatus(PocketConstants.ERROR);
                baseResponseObject.setData(null);
                return ResponseEntity.ok().body(baseResponseObject);
            }
        }else {
            baseResponseObject = new BaseResponseObject();
            baseResponseObject.setError(new ErrorObject("400","User Wallet Password Not Valid."));
            baseResponseObject.setRequestId(request.getRequestId());
            baseResponseObject.setStatus(PocketConstants.ERROR);
            baseResponseObject.setData(null);
            return ResponseEntity.ok().body(baseResponseObject);
        }
    }

    @ApiOperation(value = "This API is for getting user token")
    @PostMapping(value = "/token")
    public @ResponseBody
    ResponseEntity<BaseResponseObject> trxToken(HttpServletRequest httpServletRequest,
                                                @RequestBody UserTokenRequest request) throws PocketException {
        BaseResponseObject baseResponseObject = new BaseResponseObject(request.getRequestId());

        validateRequest(request, baseResponseObject);
        baseResponseObject.setData(hssPaymentService.getUserToken(httpServletRequest, request));
        return ResponseEntity.ok().body(baseResponseObject);
    }
}
