package tech.ipocket.pocket.request.integration.hss;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class HssPaymentRequest extends HssBaseRequest{
    private String walletNumber;
    private Double transactionAmount;
    private String userNotes;
    private String transactionRefId;

    private String hssUserId;
    private String hssProductTypeName;

    private String securityToken;
    private String userWalletPassword;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (walletNumber == null && walletNumber.equalsIgnoreCase("string") && walletNumber.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.WalletNotFound);
            return;
        }
        if (transactionAmount == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_PAYMENT_AMOUNT);
            return;
        }
        if (userNotes == null && userNotes.equalsIgnoreCase("string") && userNotes.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_USER_NOTES);
            return;
        }
        if (transactionRefId == null && transactionRefId.equalsIgnoreCase("string") && transactionRefId.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_TRANSACTION_REF_ID);
            return;
        }
        if (hssUserId == null && hssUserId.equalsIgnoreCase("string") && hssUserId.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_USER_ID);
            return;
        }
        if (hssProductTypeName == null && hssProductTypeName.equalsIgnoreCase("string") && hssProductTypeName.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_PRODUCT_TYPE_NAME);
            return;
        }
        if (securityToken == null && securityToken.equalsIgnoreCase("string") && securityToken.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_SECURITY_TOKEN);
            return;
        }
        if (userWalletPassword == null && userWalletPassword.equalsIgnoreCase("string") && userWalletPassword.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_CLIENT_USER_PASSWORD);
            return;
        }

    }


    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getUserNotes() {
        return userNotes;
    }

    public void setUserNotes(String userNotes) {
        this.userNotes = userNotes;
    }

    public String getTransactionRefId() {
        return transactionRefId;
    }

    public void setTransactionRefId(String transactionRefId) {
        this.transactionRefId = transactionRefId;
    }

    public String getHssUserId() {
        return hssUserId;
    }

    public void setHssUserId(String hssUserId) {
        this.hssUserId = hssUserId;
    }

    public String getHssProductTypeName() {
        return hssProductTypeName;
    }

    public void setHssProductTypeName(String hssProductTypeName) {
        this.hssProductTypeName = hssProductTypeName;
    }

    public String getUserWalletPassword() {
        return userWalletPassword;
    }

    public void setUserWalletPassword(String userWalletPassword) {
        this.userWalletPassword = userWalletPassword;
    }
}
