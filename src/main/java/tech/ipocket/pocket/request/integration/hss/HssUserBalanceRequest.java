package tech.ipocket.pocket.request.integration.hss;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class HssUserBalanceRequest extends HssBaseRequest{

    private String walletNumber;
    private String securityToken;
    private String userWalletPassword;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.WalletNotFound);
            return;
        }
        if (securityToken == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_SECURITY_TOKEN);
            return;
        }
        if (userWalletPassword == null && userWalletPassword.equalsIgnoreCase("string") && userWalletPassword.isEmpty()) {
            baseResponseObject.setErrorCode(PocketErrorCode.HSS_CLIENT_USER_PASSWORD);
            return;
        }
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    public String getUserWalletPassword() {
        return userWalletPassword;
    }

    public void setUserWalletPassword(String userWalletPassword) {
        this.userWalletPassword = userWalletPassword;
    }
}
