package tech.ipocket.pocket.request.integration;

import tech.ipocket.pocket.response.BaseResponseObject;

public abstract class UPayBaseRequest {
    private String requestId;
    private String requestDateTime;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public abstract void validate(BaseResponseObject baseResponseObject);
}
