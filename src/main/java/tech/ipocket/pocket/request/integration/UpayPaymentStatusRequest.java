package tech.ipocket.pocket.request.integration;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UpayPaymentStatusRequest extends UPayBaseRequest{
    private String paymentRefNumber;
    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (paymentRefNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.PaymentReferenceNumberNotFound);
            return;
        }
    }

    public String getPaymentRefNumber() {
        return paymentRefNumber;
    }

    public void setPaymentRefNumber(String paymentRefNumber) {
        this.paymentRefNumber = paymentRefNumber;
    }
}
