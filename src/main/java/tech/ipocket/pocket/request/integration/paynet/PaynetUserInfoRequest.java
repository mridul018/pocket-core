package tech.ipocket.pocket.request.integration.paynet;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class PaynetUserInfoRequest extends PaynetBaseRequest {
    private String walletNumber;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.WalletNotFound);
            return;
        }
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }
}
