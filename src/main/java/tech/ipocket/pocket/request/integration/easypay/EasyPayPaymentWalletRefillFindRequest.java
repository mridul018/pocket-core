package tech.ipocket.pocket.request.integration.easypay;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class EasyPayPaymentWalletRefillFindRequest extends EasyPayBaseRequest {
    private String tokenNo;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (tokenNo == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.EASYPAY_TOKEN_NOT_FOUND);
            return;
        }
    }

    public String getTokenNo() {
        return tokenNo;
    }

    public void setTokenNo(String tokenNo) {
        this.tokenNo = tokenNo;
    }
}
