package tech.ipocket.pocket.request.integration.paynet;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class PaynetPaymentWalletRefillRequest extends PaynetBaseRequest {

    private String walletNumber;
    private Double transactionAmount;
    private String userNotes;
    private String transactionRefId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.WalletNotFound);
            return;
        }
        if (transactionAmount == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }
        if (userNotes == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_USER_NOTES);
            return;
        }
        if (transactionRefId == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_REFERENCE_TRANSACTION_ID);
            return;
        }
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getUserNotes() {
        return userNotes;
    }

    public void setUserNotes(String userNotes) {
        this.userNotes = userNotes;
    }

    public String getTransactionRefId() {
        return transactionRefId;
    }

    public void setTransactionRefId(String transactionRefId) {
        this.transactionRefId = transactionRefId;
    }
}
