package tech.ipocket.pocket.request.integration.hss;

import tech.ipocket.pocket.response.BaseResponseObject;

public abstract class HssBaseRequest {

    private String requestId;
    private String requestDateTime;
    private String clientToken;

    public abstract void validate(BaseResponseObject baseResponseObject);


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public String getClientToken() {
        return clientToken;
    }

    public void setClientToken(String clientToken) {
        this.clientToken = clientToken;
    }
}
