package tech.ipocket.pocket.request.integration;

import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UPayTransactionFindRequest extends UPayBaseRequest {
    private String tokenNo;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (tokenNo == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.UPAY_TOKEN_NOT_FOUND);
            return;
        }
    }

    public String getTokenNo() {
        return tokenNo;
    }

    public void setTokenNo(String tokenNo) {
        this.tokenNo = tokenNo;
    }
}
