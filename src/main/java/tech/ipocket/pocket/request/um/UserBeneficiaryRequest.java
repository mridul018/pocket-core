package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserBeneficiaryRequest extends BaseRequestObject {

    String type;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(type == null){
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_TYPE_NOT_FOUND));
            return;
        }
        if (getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }
        if (getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
