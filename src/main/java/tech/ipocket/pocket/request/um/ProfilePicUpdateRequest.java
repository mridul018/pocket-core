package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class ProfilePicUpdateRequest extends BaseRequestObject {

    private String documentAsString;
    private String documentType;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (documentAsString == null ) {
            baseResponseObject.setErrorCode(PocketErrorCode.DocumentRequired);
            return;
        }
        if (documentType == null ) {
            baseResponseObject.setErrorCode(PocketErrorCode.DocumentTypeRequired);
            return;
        }
    }

    public String getDocumentAsString() {
        return documentAsString;
    }

    public void setDocumentAsString(String documentAsString) {
        this.documentAsString = documentAsString;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
}
