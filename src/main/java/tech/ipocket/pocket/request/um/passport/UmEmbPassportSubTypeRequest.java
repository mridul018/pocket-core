package tech.ipocket.pocket.request.um.passport;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;
import java.util.Date;

public class UmEmbPassportSubTypeRequest  extends BaseRequestObject {
    private int id;
    private String passportSubType;
    private String status;
    private String description;
    private Boolean generalDelivery;
    private Boolean emergencyDelivery;
    private BigDecimal embPassportFee;
    private BigDecimal pocketServiceCharge;
    private BigDecimal totalAmount;
    private Date createdDate;
    private UmEmbPassportTypeRequest umEmbPassportTypeById;  //fk

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getGeneralDelivery() {
        return generalDelivery;
    }

    public void setGeneralDelivery(Boolean generalDelivery) {
        this.generalDelivery = generalDelivery;
    }

    public Boolean getEmergencyDelivery() {
        return emergencyDelivery;
    }

    public void setEmergencyDelivery(Boolean emergencyDelivery) {
        this.emergencyDelivery = emergencyDelivery;
    }

    public BigDecimal getEmbPassportFee() {
        return embPassportFee;
    }

    public void setEmbPassportFee(BigDecimal embPassportFee) {
        this.embPassportFee = embPassportFee;
    }

    public BigDecimal getPocketServiceCharge() {
        return pocketServiceCharge;
    }

    public void setPocketServiceCharge(BigDecimal pocketServiceCharge) {
        this.pocketServiceCharge = pocketServiceCharge;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public UmEmbPassportTypeRequest getUmEmbPassportTypeById() {
        return umEmbPassportTypeById;
    }

    public void setUmEmbPassportTypeById(UmEmbPassportTypeRequest umEmbPassportTypeById) {
        this.umEmbPassportTypeById = umEmbPassportTypeById;
    }


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (umEmbPassportTypeById == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassPortType));
            return;
        }
        if (passportSubType == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubType));
            return;
        }
        if (status == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportSubTypeStatus));
            return;
        }
        if (generalDelivery == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportGeneralDelivery));
            return;
        }
        if (emergencyDelivery == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassportEmergencyDelivery));
            return;
        }
        if (embPassportFee == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EmbPassportFee));
            return;
        }
        if (pocketServiceCharge == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PocketServiceCharge));
            return;
        }
    }

}
