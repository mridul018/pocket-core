package tech.ipocket.pocket.request.um.passport;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class UmEmbPassportSubTypeEmptyRequest extends BaseRequestObject {
    private UmEmbPassportTypeRequest typeRequest;

    public UmEmbPassportTypeRequest getTypeRequest() {
        return typeRequest;
    }

    public void setTypeRequest(UmEmbPassportTypeRequest typeRequest) {
        this.typeRequest = typeRequest;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }
}
