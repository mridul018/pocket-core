package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.external.BillPayCustomerRegistration;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BillPayCustomerRegistrationRequest {
    private String logedInUserMobileNumber;
    private BillPayCustomerRegistration customerRegistration;

    public String getLogedInUserMobileNumber() {
        return logedInUserMobileNumber;
    }

    public void setLogedInUserMobileNumber(String logedInUserMobileNumber) {
        this.logedInUserMobileNumber = logedInUserMobileNumber;
    }

    public BillPayCustomerRegistration getCustomerRegistration() {
        return customerRegistration;
    }

    public void setCustomerRegistration(BillPayCustomerRegistration customerRegistration) {
        this.customerRegistration = customerRegistration;
    }
}
