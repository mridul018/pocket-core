package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class TransactionSummaryViewRequest extends BaseRequestObject {
    private String purpose;
    private String mobileNumber;
    private String receiverMobileNumber;
    private String bankCode;
    private double amount;
    private String rechargeMobileNumber;
    private String billId;
    private Integer mobileOperator;
    private String rechargeType;
    private String countryCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getReceiverMobileNumber() {
        return receiverMobileNumber;
    }

    public void setReceiverMobileNumber(String receiverMobileNumber) {
        this.receiverMobileNumber = receiverMobileNumber;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRechargeMobileNumber() {
        return rechargeMobileNumber;
    }

    public void setRechargeMobileNumber(String rechargeMobileNumber) {
        this.rechargeMobileNumber = rechargeMobileNumber;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public Integer getMobileOperator() {
        return mobileOperator;
    }

    public void setMobileOperator(Integer mobileOperator) {
        this.mobileOperator = mobileOperator;
    }

    public String getRechargeType() {
        return rechargeType;
    }

    public void setRechargeType(String rechargeType) {
        this.rechargeType = rechargeType;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}