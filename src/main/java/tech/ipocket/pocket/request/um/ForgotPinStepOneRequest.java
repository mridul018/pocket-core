package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ForgotPinStepOneRequest extends BaseRequestObject {

    private String mobileNumber;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (mobileNumber == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired));
            return;
        }

    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
