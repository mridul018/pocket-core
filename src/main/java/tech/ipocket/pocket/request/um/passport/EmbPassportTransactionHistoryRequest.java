package tech.ipocket.pocket.request.um.passport;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class EmbPassportTransactionHistoryRequest extends BaseRequestObject {

    private String transactionType;
    private String senderWalletNo;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSenderWalletNo() {
        return senderWalletNo;
    }

    public void setSenderWalletNo(String senderWalletNo) {
        this.senderWalletNo = senderWalletNo;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (transactionType == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidTransactionType);
            return;
        }
        if (senderWalletNo == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.WalletNotFound);
            return;
        }

    }

}
