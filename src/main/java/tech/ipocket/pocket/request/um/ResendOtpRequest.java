package tech.ipocket.pocket.request.um;


import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ResendOtpRequest extends BaseRequestObject {
    private String otpSecret;

    public ResendOtpRequest() {
    }


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (otpSecret == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.OtpSecretRequired);
            return;
        }
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }
}
