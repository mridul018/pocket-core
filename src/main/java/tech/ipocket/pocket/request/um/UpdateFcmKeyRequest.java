package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UpdateFcmKeyRequest extends BaseRequestObject {
    private String fcmKey;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (fcmKey == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.FcmKeyRequired);
            return;
        }
    }

    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }
}
