package tech.ipocket.pocket.request.um.passport;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;
public class UmEmbPassportTypeRequest extends BaseRequestObject {
    private int id;
    private String passportType;
    private String description;
    private String status;
    private String createdDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
            if (passportType == null) {
                baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassPortType));
                return;
            }
            if (status == null) {
                baseResponseObject.setError(new ErrorObject(PocketErrorCode.PassPortStatus));
                return;
            }
    }

}
