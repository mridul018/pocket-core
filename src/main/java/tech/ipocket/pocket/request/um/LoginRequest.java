package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class LoginRequest extends BaseRequestObject {

    private String loginId;
    private String credential;
    private String fcmKey;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (loginId == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.LoginIdRequired));
            return;
        }

        if (credential == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.CredentialRequired));
            return;
        }
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }
}
