package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.repository.um.UserDocumentRepository;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.ArrayList;
import java.util.List;


public class RegistrationRequest extends BaseRequestObject {

    private String fullName;
    private String mobileNo;
    private String email;
    private String password;
    private String groupCode;
    private String primaryIdNumber; // m
    private String primaryIdIssueDate; // m
    private String primaryIdExpiryDate; // m
    private String primaryIdType; //m
    private String nationality; // m
    private String metaData;

    // for merchant api
    private String applicantsName;
    private String tradeLicenseNo;
    private String secondContactName;
    private String secondContactMobileNo;
    private String thirdContactName;
    private String thirdContactMobileNo;


    private BankData bankData;
    private String gender;
    private String presentAddress;
    private String permanentAddress;
    private String postCode;
    private String srWallet;

    private String countryCode;
    private String stateId;


    private String accountNumber;
    private String accountName;
    private String bankCode;
    private String bankName;
    private String bankRoutingNo;
    private String branchName;
    private String branchSwiftCode;

    private String dateOfBirth;

    private List<UserDocuments> userDocuments;

    public RegistrationRequest() {
        userDocuments=new ArrayList<>();
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getPrimaryIdNumber() {
        return primaryIdNumber;
    }

    public void setPrimaryIdNumber(String primaryIdNumber) {
        this.primaryIdNumber = primaryIdNumber;
    }

    public String getPrimaryIdIssueDate() {
        return primaryIdIssueDate;
    }

    public void setPrimaryIdIssueDate(String primaryIdIssueDate) {
        this.primaryIdIssueDate = primaryIdIssueDate;
    }

    public String getPrimaryIdExpiryDate() {
        return primaryIdExpiryDate;
    }

    public void setPrimaryIdExpiryDate(String primaryIdExpiryDate) {
        this.primaryIdExpiryDate = primaryIdExpiryDate;
    }

    public String getPrimaryIdType() {
        return primaryIdType;
    }

    public void setPrimaryIdType(String primaryIdType) {
        this.primaryIdType = primaryIdType;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (fullName == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.FullNameRequired);
            return;
        }
        if (password == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.CredentialRequired);
            return;
        }
        if (groupCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.GroupCodeRequired);
            return;
        }

        /*if(countryCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.CountryCodeRequired);
            return;
        }
        if(stateId==null){
            baseResponseObject.setErrorCode(PocketErrorCode.StateIdRequired);
            return;
        }*/
    }

    public String getApplicantsName() {
        return applicantsName;
    }

    public void setApplicantsName(String applicantsName) {
        this.applicantsName = applicantsName;
    }

    public String getTradeLicenseNo() {
        return tradeLicenseNo;
    }

    public void setTradeLicenseNo(String tradeLicenseNo) {
        this.tradeLicenseNo = tradeLicenseNo;
    }

    public String getSecondContactName() {
        return secondContactName;
    }

    public void setSecondContactName(String secondContactName) {
        this.secondContactName = secondContactName;
    }

    public String getSecondContactMobileNo() {
        return secondContactMobileNo;
    }

    public void setSecondContactMobileNo(String secondContactMobileNo) {
        this.secondContactMobileNo = secondContactMobileNo;
    }

    public String getThirdContactName() {
        return thirdContactName;
    }

    public void setThirdContactName(String thirdContactName) {
        this.thirdContactName = thirdContactName;
    }

    public String getThirdContactMobileNo() {
        return thirdContactMobileNo;
    }

    public void setThirdContactMobileNo(String thirdContactMobileNo) {
        this.thirdContactMobileNo = thirdContactMobileNo;
    }

    public BankData getBankData() {
        return bankData;
    }

    public void setBankData(BankData bankData) {
        this.bankData = bankData;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getSrWallet() {
        return srWallet;
    }

    public void setSrWallet(String srWallet) {
        this.srWallet = srWallet;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public List<UserDocuments> getUserDocuments() {
        return userDocuments;
    }

    public void setUserDocuments(List<UserDocuments> userDocuments) {
        this.userDocuments = userDocuments;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankRoutingNo() {
        return bankRoutingNo;
    }

    public void setBankRoutingNo(String bankRoutingNo) {
        this.bankRoutingNo = bankRoutingNo;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public void setBranchSwiftCode(String branchSwiftCode) {
        this.branchSwiftCode = branchSwiftCode;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
