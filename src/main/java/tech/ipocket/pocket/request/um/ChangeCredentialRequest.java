package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ChangeCredentialRequest extends BaseRequestObject {

    private String oldCredential;
    private String newCredential;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (oldCredential == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.OldCredentialRequired));
            return;
        }
        if (newCredential == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.NewCredentialRequired));
            return;
        }
        if (oldCredential.equals(newCredential)) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.OldAndNewCredentialMustDifferent));
            return;
        }
    }

    public String getOldCredential() {
        return oldCredential;
    }

    public void setOldCredential(String oldCredential) {
        this.oldCredential = oldCredential;
    }

    public String getNewCredential() {
        return newCredential;
    }

    public void setNewCredential(String newCredential) {
        this.newCredential = newCredential;
    }
}
