package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetUserInfoRequest extends BaseRequestObject {
    private String mobileNumber;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (mobileNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
