package tech.ipocket.pocket.request.um;

public class UserDocuments {
    private String documentType;
    private String documentBase64String;
    private String documentExtension;

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentBase64String() {
        return documentBase64String;
    }

    public void setDocumentBase64String(String documentBase64String) {
        this.documentBase64String = documentBase64String;
    }

    public String getDocumentExtension() {
        return documentExtension;
    }

    public void setDocumentExtension(String documentExtension) {
        this.documentExtension = documentExtension;
    }
}
