package tech.ipocket.pocket.request.um.passport;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

import java.util.Date;

public class EmbPassportTrxReportRequest extends BaseRequestObject {
    private String uaeMobileNo;
    private String uniquePassportGeneratedId;
    private String mrpPassportNumber;
    private String previousPassportNumber;
    private String passportType;
    private String passportSubType;
    private String professionOrSkill;
    private String status;
    private Date createDate;
    private String emirateOrResidentialId;
    private String payMode;
    private String tsTransactionId;
    private String tsTransactionToken;
    private String tsTransactionSender;
    private String tsTransactionReceiver;

    private Date fromDate;
    private Date toDate;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getUaeMobileNo() {
        return uaeMobileNo;
    }

    public void setUaeMobileNo(String uaeMobileNo) {
        this.uaeMobileNo = uaeMobileNo;
    }

    public String getUniquePassportGeneratedId() {
        return uniquePassportGeneratedId;
    }

    public void setUniquePassportGeneratedId(String uniquePassportGeneratedId) {
        this.uniquePassportGeneratedId = uniquePassportGeneratedId;
    }

    public String getMrpPassportNumber() {
        return mrpPassportNumber;
    }

    public void setMrpPassportNumber(String mrpPassportNumber) {
        this.mrpPassportNumber = mrpPassportNumber;
    }

    public String getPreviousPassportNumber() {
        return previousPassportNumber;
    }

    public void setPreviousPassportNumber(String previousPassportNumber) {
        this.previousPassportNumber = previousPassportNumber;
    }

    public String getPassportType() {
        return passportType;
    }

    public void setPassportType(String passportType) {
        this.passportType = passportType;
    }

    public String getPassportSubType() {
        return passportSubType;
    }

    public void setPassportSubType(String passportSubType) {
        this.passportSubType = passportSubType;
    }

    public String getProfessionOrSkill() {
        return professionOrSkill;
    }

    public void setProfessionOrSkill(String professionOrSkill) {
        this.professionOrSkill = professionOrSkill;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getEmirateOrResidentialId() {
        return emirateOrResidentialId;
    }

    public void setEmirateOrResidentialId(String emirateOrResidentialId) {
        this.emirateOrResidentialId = emirateOrResidentialId;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getTsTransactionId() {
        return tsTransactionId;
    }

    public void setTsTransactionId(String tsTransactionId) {
        this.tsTransactionId = tsTransactionId;
    }

    public String getTsTransactionToken() {
        return tsTransactionToken;
    }

    public void setTsTransactionToken(String tsTransactionToken) {
        this.tsTransactionToken = tsTransactionToken;
    }

    public String getTsTransactionSender() {
        return tsTransactionSender;
    }

    public void setTsTransactionSender(String tsTransactionSender) {
        this.tsTransactionSender = tsTransactionSender;
    }

    public String getTsTransactionReceiver() {
        return tsTransactionReceiver;
    }

    public void setTsTransactionReceiver(String tsTransactionReceiver) {
        this.tsTransactionReceiver = tsTransactionReceiver;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }
}
