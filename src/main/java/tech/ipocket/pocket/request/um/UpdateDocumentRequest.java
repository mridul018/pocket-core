package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class UpdateDocumentRequest extends BaseRequestObject {
    private String mobileNumber;
    private List<DocumentItem> documentList;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (documentList == null || documentList.size() == 0) {
            baseResponseObject.setErrorCode(PocketErrorCode.DocumentRequired);
            return;
        }
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public List<DocumentItem> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<DocumentItem> documentList) {
        this.documentList = documentList;
    }
}
