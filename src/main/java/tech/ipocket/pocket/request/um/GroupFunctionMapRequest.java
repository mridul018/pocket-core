package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.web.GroupFunctionMap;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class GroupFunctionMapRequest extends BaseRequestObject {

    private List<GroupFunctionMap> groupFunctionMaps;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (groupFunctionMaps == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.GROUP_FUNCTION_MAPPING_NOT_FOUND);
            return;
        }

        if (getHardwareSignature() == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.INVALID_HARDWARE_SIGNATURE);
            return;
        }

        if (getSessionToken() == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.TOKEN_NOT_FOUND);
            return;
        }
    }

    public List<GroupFunctionMap> getGroupFunctionMaps() {
        return groupFunctionMaps;
    }

    public void setGroupFunctionMaps(List<GroupFunctionMap> groupFunctionMaps) {
        this.groupFunctionMaps = groupFunctionMaps;
    }
}
