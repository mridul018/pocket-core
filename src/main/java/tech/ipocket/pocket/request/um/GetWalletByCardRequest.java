package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetWalletByCardRequest extends BaseRequestObject {

    private String number;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (number == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.CardNumberRequired));
            return;
        }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
