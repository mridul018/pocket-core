package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class GetStateByCountryCodeRequest extends BaseRequestObject {

    private String countryCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
