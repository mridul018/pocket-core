package tech.ipocket.pocket.request.um;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class LoginStepTwoRequest extends BaseRequestObject {

    private String otpSecret;
    private String otpText;
    private String fcmKey;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (otpSecret == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.InvalidInput));
            return;
        }

        if (otpText == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.OtpRequired));
            return;
        }
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getFcmKey() {
        return fcmKey;
    }

    public void setFcmKey(String fcmKey) {
        this.fcmKey = fcmKey;
    }
}
