package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

import java.util.Date;

public class CustomerRequest extends BaseRequestObject {
    private String groupCode;
    private String accountStatus;
    private int customerWinnerNumber;
    private Date startDate;
    private Date endDate;

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public int getCustomerWinnerNumber() {
        return customerWinnerNumber;
    }

    public void setCustomerWinnerNumber(int customerWinnerNumber) {
        this.customerWinnerNumber = customerWinnerNumber;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
