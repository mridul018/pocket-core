package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.ArrayList;
import java.util.List;

public class UserListRequest extends PageItemBaseRequestObject {
    private String groupCode;
    private List<String> walletStatusList;

    private String countryCode;
    private List<String> stateList;

    public UserListRequest() {
        stateList=new ArrayList<>();
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(countryCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.CountryCodeRequired);
            return;
        }

        if(groupCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidGroupCode);
            return;
        }

        if(stateList==null||stateList.size()==0){
            baseResponseObject.setErrorCode(PocketErrorCode.StateListRequired);
            return;
        }

    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public List<String> getWalletStatusList() {
        return walletStatusList;
    }

    public void setWalletStatusList(List<String> walletStatusList) {
        this.walletStatusList = walletStatusList;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<String> getStateList() {
        return stateList;
    }

    public void setStateList(List<String> stateList) {
        this.stateList = stateList;
    }
}
