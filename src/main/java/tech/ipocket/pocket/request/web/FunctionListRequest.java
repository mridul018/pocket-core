package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class FunctionListRequest extends BaseRequestObject {

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (getHardwareSignature() == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.INVALID_HARDWARE_SIGNATURE);
            return;
        }
        if (getSessionToken() == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.TOKEN_NOT_FOUND);
            return;
        }

    }
}
