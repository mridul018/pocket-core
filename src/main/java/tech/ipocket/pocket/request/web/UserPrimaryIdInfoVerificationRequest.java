package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserPrimaryIdInfoVerificationRequest extends BaseRequestObject {

    private String walletId;
    private Boolean verifyPrimaryId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletId == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.WalletIdRequired));
            return;
        }

        if (verifyPrimaryId == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.VERIFY_PRIMARYID_STATUS_NOT_FOUND));
            return;
        }

        if (getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }

        if (getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public Boolean getVerifyPrimaryId() {
        return verifyPrimaryId;
    }

    public void setVerifyPrimaryId(Boolean verifyPrimaryId) {
        this.verifyPrimaryId = verifyPrimaryId;
    }

    public boolean isVerifyPrimaryId() {
        return verifyPrimaryId;
    }

    public void setVerifyPrimaryId(boolean verifyPrimaryId) {
        this.verifyPrimaryId = verifyPrimaryId;
    }
}
