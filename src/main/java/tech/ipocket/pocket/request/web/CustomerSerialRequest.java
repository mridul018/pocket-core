package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CustomerSerialRequest extends BaseRequestObject {

    private String walletNo;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletNo == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired));
            return;
        }
    }

    public String getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(String walletNo) {
        this.walletNo = walletNo;
    }
}
