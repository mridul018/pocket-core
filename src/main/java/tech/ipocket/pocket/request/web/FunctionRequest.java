package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class FunctionRequest extends BaseRequestObject {

    private int id;
    private String functionCode;
    private String functionName;
    private String description;
    private String functionStatus;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (functionCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.FunctionCodeRequired);
            return;
        }

        if (functionCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.FunctionCodeLengthMismatch);
            return;
        }

        if (functionName == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.FunctionNameRequired);
            return;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFunctionCode() {
        return functionCode;
    }

    public void setFunctionCode(String functionCode) {
        this.functionCode = functionCode;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFunctionStatus() {
        return functionStatus;
    }

    public void setFunctionStatus(String functionStatus) {
        this.functionStatus = functionStatus;
    }
}
