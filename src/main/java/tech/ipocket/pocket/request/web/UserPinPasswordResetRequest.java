package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserPinPasswordResetRequest extends BaseRequestObject {

    private String loginId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (loginId == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.LoginIdRequired));
            return;
        }
        if (getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }
        if (getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }
}
