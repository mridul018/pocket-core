package tech.ipocket.pocket.request.web;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UserLockUnlockWalletRequest extends BaseRequestObject {

    private String walletNumber;
    private String walletStatus;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletNumber == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.WALLET_NUMBER_NOT_FOUND));
            return;
        }

        if (walletStatus == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.WALLET_STATUS_NOT_FOUND));
            return;
        }

        if (getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.INVALID_HARDWARE_SIGNATURE));
            return;
        }

        if (getSessionToken() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TOKEN_NOT_FOUND));
            return;
        }
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getWalletStatus() {
        return walletStatus;
    }

    public void setWalletStatus(String walletStatus) {
        this.walletStatus = walletStatus;
    }
}
