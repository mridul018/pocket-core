package tech.ipocket.pocket.request.external.banks;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BankAddRequest extends BaseRequestObject {
    private String userMobileNumber;
    private String bankId;
    private String bankFullName;
    private String bankShortName;
    private String bankLogoUrl;
    private String accessCode;
    private String userSecret;
    private String userID;
    private String connectionId;

    public BankAddRequest() {
    }

    public BankAddRequest(String userMobileNumber, String bankId, String bankFullName, String bankShortName, String bankLogoUrl, String accessCode, String userSecret, String userID, String connectionId) {
        this.userMobileNumber = userMobileNumber;
        this.bankId = bankId;
        this.bankFullName = bankFullName;
        this.bankShortName = bankShortName;
        this.bankLogoUrl = bankLogoUrl;
        this.accessCode = accessCode;
        this.userSecret = userSecret;
        this.userID = userID;
        this.connectionId = connectionId;
    }

    public String getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankFullName() {
        return bankFullName;
    }

    public void setBankFullName(String bankFullName) {
        this.bankFullName = bankFullName;
    }

    public String getBankShortName() {
        return bankShortName;
    }

    public void setBankShortName(String bankShortName) {
        this.bankShortName = bankShortName;
    }

    public String getBankLogoUrl() {
        return bankLogoUrl;
    }

    public void setBankLogoUrl(String bankLogoUrl) {
        this.bankLogoUrl = bankLogoUrl;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getUserSecret() {
        return userSecret;
    }

    public void setUserSecret(String userSecret) {
        this.userSecret = userSecret;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(userMobileNumber == null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if(bankId == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_ID_REQUIRED);
            return;
        }
        if(bankFullName == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BankNameRequired);
            return;
        }
        if(bankLogoUrl == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_LOGO_REQUIRED);
            return;
        }
        if(bankShortName == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_SHORT_NAME_REQUIRED);
            return;
        }
        if(accessCode == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_ACCESSCODE_REQUIRED);
            return;
        }
        if(userSecret == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_USER_SECRET_REQUIRED);
            return;
        }
        if(userID == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_USER_ID_REQUIRED);
            return;
        }
        if(connectionId == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_CONNECTION_ID_REQUIRED);
            return;
        }
    }
}
