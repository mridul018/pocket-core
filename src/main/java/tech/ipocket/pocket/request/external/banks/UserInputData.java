package tech.ipocket.pocket.request.external.banks;

import java.util.ArrayList;

public class UserInputData extends AccountMetadataRequest{
    private String jobID;
    private ArrayList<UserInputRequest> userInputs;

    public UserInputData() {
    }

    public UserInputData(String appSecret, String userSecret, boolean sync, String jobID, ArrayList<UserInputRequest> userInputs) {
        super(appSecret, userSecret, sync);
        this.jobID = jobID;
        this.userInputs = userInputs;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public ArrayList<UserInputRequest> getUserInputs() {
        return userInputs;
    }

    public void setUserInputs(ArrayList<UserInputRequest> userInputs) {
        this.userInputs = userInputs;
    }
}
