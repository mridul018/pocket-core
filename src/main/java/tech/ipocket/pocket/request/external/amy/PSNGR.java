package tech.ipocket.pocket.request.external.amy;

public class PSNGR {
    private String iD;
    private String typ;
    private String title;
    private String fNam;
    private String lNam;
    private String mobile;
    private String eMail;
    private String dOBD;
    private String dOBM;
    private String dOBY;
    private String vIP;
    private String fFP;
    private String pP;
    private String pPEX;
    private String nAT;
    private String oK;
    private String nATC;

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getfNam() {
        return fNam;
    }

    public void setfNam(String fNam) {
        this.fNam = fNam;
    }

    public String getlNam() {
        return lNam;
    }

    public void setlNam(String lNam) {
        this.lNam = lNam;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getdOBD() {
        return dOBD;
    }

    public void setdOBD(String dOBD) {
        this.dOBD = dOBD;
    }

    public String getdOBM() {
        return dOBM;
    }

    public void setdOBM(String dOBM) {
        this.dOBM = dOBM;
    }

    public String getdOBY() {
        return dOBY;
    }

    public void setdOBY(String dOBY) {
        this.dOBY = dOBY;
    }

    public String getvIP() {
        return vIP;
    }

    public void setvIP(String vIP) {
        this.vIP = vIP;
    }

    public String getfFP() {
        return fFP;
    }

    public void setfFP(String fFP) {
        this.fFP = fFP;
    }

    public String getpP() {
        return pP;
    }

    public void setpP(String pP) {
        this.pP = pP;
    }

    public String getpPEX() {
        return pPEX;
    }

    public void setpPEX(String pPEX) {
        this.pPEX = pPEX;
    }

    public String getnAT() {
        return nAT;
    }

    public void setnAT(String nAT) {
        this.nAT = nAT;
    }

    public String getoK() {
        return oK;
    }

    public void setoK(String oK) {
        this.oK = oK;
    }

    public String getnATC() {
        return nATC;
    }

    public void setnATC(String nATC) {
        this.nATC = nATC;
    }
}
