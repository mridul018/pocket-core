package tech.ipocket.pocket.request.external.banks;

public class ExchangeTokenRequest {
    private String connectionID;
    private String appSecret;
    private String accessCode;

    public ExchangeTokenRequest() {

    }

    public ExchangeTokenRequest(String connectionID, String appSecret, String accessCode) {
        this.connectionID = connectionID;
        this.appSecret = appSecret;
        this.accessCode = accessCode;
    }

    public String getConnectionID() {
        return connectionID;
    }

    public void setConnectionID(String connectionID) {
        this.connectionID = connectionID;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }
}
