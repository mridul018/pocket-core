package tech.ipocket.pocket.request.external.amy;

public class FlightIssueRequest {
    private String uSER;
    private String mOBILE;
    private String eMAIL;
    private Integer fR;
    private String pNR;
    private String cMND;

    public String getuSER() {
        return uSER;
    }

    public void setuSER(String uSER) {
        this.uSER = uSER;
    }

    public String getmOBILE() {
        return mOBILE;
    }

    public void setmOBILE(String mOBILE) {
        this.mOBILE = mOBILE;
    }

    public String geteMAIL() {
        return eMAIL;
    }

    public void seteMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }

    public Integer getfR() {
        return fR;
    }

    public void setfR(Integer fR) {
        this.fR = fR;
    }

    public String getpNR() {
        return pNR;
    }

    public void setpNR(String pNR) {
        this.pNR = pNR;
    }

    public String getcMND() {
        return cMND;
    }

    public void setcMND(String cMND) {
        this.cMND = cMND;
    }
}
