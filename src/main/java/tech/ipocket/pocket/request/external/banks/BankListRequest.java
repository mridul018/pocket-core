package tech.ipocket.pocket.request.external.banks;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BankListRequest extends BaseRequestObject{
    private String userPhoneNumber;

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(userPhoneNumber == null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
    }
}
