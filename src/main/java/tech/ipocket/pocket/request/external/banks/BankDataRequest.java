package tech.ipocket.pocket.request.external.banks;


import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BankDataRequest extends BaseRequestObject {
    private String userMobileNumber;
    private String bankID;

    public BankDataRequest() {
    }

    public BankDataRequest(String userMobileNumber, String bankID) {
        this.userMobileNumber = userMobileNumber;
        this.bankID = bankID;
    }

    public String getUserMobileNumber() {
        return userMobileNumber;
    }

    public void setUserMobileNumber(String userMobileNumber) {
        this.userMobileNumber = userMobileNumber;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(userMobileNumber == null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if(bankID == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_ID_REQUIRED);
            return;
        }
    }
}
