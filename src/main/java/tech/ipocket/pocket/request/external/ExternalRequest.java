package tech.ipocket.pocket.request.external;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.external.amy.FlightBookingRequest;
import tech.ipocket.pocket.request.external.amy.FlightIssueRequest;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigInteger;

public class ExternalRequest extends BaseRequestObject {

    private String to;
    private String from;
    private String body;
    private String subject;
    private String type;
    private BigInteger amount;

    private DingTopUpRequest dingTopUpRequest;

    private SSLTopUpRequest sslTopUpRequest;

    private PolliBudditBillRequest polliBudditBillRequest;

    private PayWellTopUpRequest payWellTopUpRequest;

    private DPDCBillPayRequest dpdcBillPayRequest;

    private DESCOBillPayRequest descoBillPayRequest;

    private WASABillPayRequest wasaBillPayRequest;

    private FlightBookingRequest flightBookingRequest;

    private FlightIssueRequest flightIssueRequest;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (type == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.EXTERNAL_TYPE_NOT_FOUND));
            return;
        }
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public SSLTopUpRequest getSslTopUpRequest() {
        return sslTopUpRequest;
    }

    public void setSslTopUpRequest(SSLTopUpRequest sslTopUpRequest) {
        this.sslTopUpRequest = sslTopUpRequest;
    }

    public PayWellTopUpRequest getPayWellTopUpRequest() {
        return payWellTopUpRequest;
    }

    public void setPayWellTopUpRequest(PayWellTopUpRequest payWellTopUpRequest) {
        this.payWellTopUpRequest = payWellTopUpRequest;
    }

    public PolliBudditBillRequest getPolliBudditBillRequest() {
        return polliBudditBillRequest;
    }

    public void setPolliBudditBillRequest(PolliBudditBillRequest polliBudditBillRequest) {
        this.polliBudditBillRequest = polliBudditBillRequest;
    }

    public DingTopUpRequest getDingTopUpRequest() {
        return dingTopUpRequest;
    }

    public void setDingTopUpRequest(DingTopUpRequest dingTopUpRequest) {
        this.dingTopUpRequest = dingTopUpRequest;
    }

    public DPDCBillPayRequest getDpdcBillPayRequest() {
        return dpdcBillPayRequest;
    }

    public void setDpdcBillPayRequest(DPDCBillPayRequest dpdcBillPayRequest) {
        this.dpdcBillPayRequest = dpdcBillPayRequest;
    }

    public DESCOBillPayRequest getDescoBillPayRequest() {
        return descoBillPayRequest;
    }

    public void setDescoBillPayRequest(DESCOBillPayRequest descoBillPayRequest) {
        this.descoBillPayRequest = descoBillPayRequest;
    }

    public WASABillPayRequest getWasaBillPayRequest() {
        return wasaBillPayRequest;
    }

    public void setWasaBillPayRequest(WASABillPayRequest wasaBillPayRequest) {
        this.wasaBillPayRequest = wasaBillPayRequest;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public FlightBookingRequest getFlightBookingRequest() {
        return flightBookingRequest;
    }

    public void setFlightBookingRequest(FlightBookingRequest flightBookingRequest) {
        this.flightBookingRequest = flightBookingRequest;
    }

    public FlightIssueRequest getFlightIssueRequest() {
        return flightIssueRequest;
    }

    public void setFlightIssueRequest(FlightIssueRequest flightIssueRequest) {
        this.flightIssueRequest = flightIssueRequest;
    }
}
