package tech.ipocket.pocket.request.external.amy;

public class PassengerValidationRequest {
    private String fRoutType;
    private String dAirline;
    private String fSoft;
    private String iD;
    private String cMND;
    private PSN pSN;
    private String dDepTime;

    public String getfRoutType() {
        return fRoutType;
    }

    public void setfRoutType(String fRoutType) {
        this.fRoutType = fRoutType;
    }

    public String getdAirline() {
        return dAirline;
    }

    public void setdAirline(String dAirline) {
        this.dAirline = dAirline;
    }

    public String getfSoft() {
        return fSoft;
    }

    public void setfSoft(String fSoft) {
        this.fSoft = fSoft;
    }

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getcMND() {
        return cMND;
    }

    public void setcMND(String cMND) {
        this.cMND = cMND;
    }

    public PSN getpSN() {
        return pSN;
    }

    public void setpSN(PSN pSN) {
        this.pSN = pSN;
    }

    public String getdDepTime() {
        return dDepTime;
    }

    public void setdDepTime(String dDepTime) {
        this.dDepTime = dDepTime;
    }
}
