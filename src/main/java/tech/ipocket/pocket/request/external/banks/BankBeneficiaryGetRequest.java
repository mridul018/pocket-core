package tech.ipocket.pocket.request.external.banks;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BankBeneficiaryGetRequest extends BaseRequestObject {
    private String mobileNumber;
    private String bankID;
    private BankBeneficiaryGetInformation bankBeneficiaryGetInformation;

    public BankBeneficiaryGetRequest() {
    }

    public BankBeneficiaryGetRequest(String mobileNumber, String bankID, BankBeneficiaryGetInformation bankBeneficiaryGetInformation) {
        this.mobileNumber = mobileNumber;
        this.bankID = bankID;
        this.bankBeneficiaryGetInformation = bankBeneficiaryGetInformation;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public BankBeneficiaryGetInformation getBankBeneficiaryGetInformation() {
        return bankBeneficiaryGetInformation;
    }

    public void setBankBeneficiaryGetInformation(BankBeneficiaryGetInformation bankBeneficiaryGetInformation) {
        this.bankBeneficiaryGetInformation = bankBeneficiaryGetInformation;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(mobileNumber == null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if(bankID == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_ID_REQUIRED);
            return;
        }
    }
}
