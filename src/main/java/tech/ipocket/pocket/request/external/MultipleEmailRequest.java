package tech.ipocket.pocket.request.external;

import java.util.List;

public class MultipleEmailRequest {
    private String requestId;
    private String emailSubject;
    private List<String> receiverEmailList;
    private String emailBody;


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setReceiverEmailList(List<String> receiverEmailList) {
        this.receiverEmailList = receiverEmailList;
    }

    public List<String> getReceiverEmailList() {
        return receiverEmailList;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public String getEmailBody() {
        return emailBody;
    }
}
