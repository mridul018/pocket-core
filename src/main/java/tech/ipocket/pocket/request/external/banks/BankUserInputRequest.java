package tech.ipocket.pocket.request.external.banks;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.ArrayList;

public class BankUserInputRequest extends BaseRequestObject {
    private String bankID;
    private String mobileNumber;
    private String jobID;
    private ArrayList<UserInputRequest> userInputs;

    public BankUserInputRequest() {
    }

    public BankUserInputRequest(String bankID, String mobileNumber, String jobID, ArrayList<UserInputRequest> userInputs) {
        this.bankID = bankID;
        this.mobileNumber = mobileNumber;
        this.jobID = jobID;
        this.userInputs = userInputs;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public ArrayList<UserInputRequest> getUserInputs() {
        return userInputs;
    }

    public void setUserInputs(ArrayList<UserInputRequest> userInputs) {
        this.userInputs = userInputs;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(bankID == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_ID_REQUIRED);
            return;
        }

        if(mobileNumber == null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }

        if(jobID == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_JOB_ID_REQUIRED);
            return;
        }
    }
}
