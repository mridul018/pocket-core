package tech.ipocket.pocket.request.external.amy;

public class FlightSearchRequest {
    private String CMND = "_FLIGHTSEARCH_";
    private String TRIP;
    private String FROM;
    private String DEST;
    private String JDT;
    private String RDT;
    private String ACLASS;
    private int AD;
    private int CH;
    private int INF;

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }

    public String getTRIP() {
        return TRIP;
    }

    public void setTRIP(String TRIP) {
        this.TRIP = TRIP;
    }

    public String getFROM() {
        return FROM;
    }

    public void setFROM(String FROM) {
        this.FROM = FROM;
    }

    public String getDEST() {
        return DEST;
    }

    public void setDEST(String DEST) {
        this.DEST = DEST;
    }

    public String getJDT() {
        return JDT;
    }

    public void setJDT(String JDT) {
        this.JDT = JDT;
    }

    public String getRDT() {
        return RDT;
    }

    public void setRDT(String RDT) {
        this.RDT = RDT;
    }

    public String getACLASS() {
        return ACLASS;
    }

    public void setACLASS(String ACLASS) {
        this.ACLASS = ACLASS;
    }

    public int getAD() {
        return AD;
    }

    public void setAD(int AD) {
        this.AD = AD;
    }

    public int getCH() {
        return CH;
    }

    public void setCH(int CH) {
        this.CH = CH;
    }

    public int getINF() {
        return INF;
    }

    public void setINF(int INF) {
        this.INF = INF;
    }
}
