package tech.ipocket.pocket.request.external;

public class DESCOBillPayRequest {
    private String username;
    private String password;
    private String billNo;
    private String payerMobileNo;
    private String trx_id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getPayerMobileNo() {
        return payerMobileNo;
    }

    public void setPayerMobileNo(String payerMobileNo) {
        this.payerMobileNo = payerMobileNo;
    }

    public String getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(String trx_id) {
        this.trx_id = trx_id;
    }
}
