package tech.ipocket.pocket.request.external;

public class PayWellTopUpRequest {
    private String clientid;
    private String clientpass;
    private String crid;
    private String msisdn;
    private String connection;
    private String operator;
    private String sender;

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getClientpass() {
        return clientpass;
    }

    public void setClientpass(String clientpass) {
        this.clientpass = clientpass;
    }

    public String getCrid() {
        return crid;
    }

    public void setCrid(String crid) {
        this.crid = crid;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
