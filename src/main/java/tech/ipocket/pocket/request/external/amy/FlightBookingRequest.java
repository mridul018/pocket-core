package tech.ipocket.pocket.request.external.amy;

import java.util.List;

public class FlightBookingRequest {
    private String cUSID;
    private List<PSNGR> pSNGR;
    private Integer fLT1;
    private Integer fLT2;
    private String xBI;
    private String bILLNO;
    private String cMND;

    public String getcUSID() {
        return cUSID;
    }

    public void setcUSID(String cUSID) {
        this.cUSID = cUSID;
    }

    public List<PSNGR> getpSNGR() {
        return pSNGR;
    }

    public void setpSNGR(List<PSNGR> pSNGR) {
        this.pSNGR = pSNGR;
    }

    public Integer getfLT1() {
        return fLT1;
    }

    public void setfLT1(Integer fLT1) {
        this.fLT1 = fLT1;
    }

    public Integer getfLT2() {
        return fLT2;
    }

    public void setfLT2(Integer fLT2) {
        this.fLT2 = fLT2;
    }

    public String getxBI() {
        return xBI;
    }

    public void setxBI(String xBI) {
        this.xBI = xBI;
    }

    public String getbILLNO() {
        return bILLNO;
    }

    public void setbILLNO(String bILLNO) {
        this.bILLNO = bILLNO;
    }

    public String getcMND() {
        return cMND;
    }

    public void setcMND(String cMND) {
        this.cMND = cMND;
    }
}
