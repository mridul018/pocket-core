package tech.ipocket.pocket.request.external;

import java.math.BigInteger;

public class SSLTopUpRequest {
    private String client_id;
    private String client_pass;
    private String guid;
    private BigInteger operator_id;
    private String recipient_msisdn;
    private String connection_type;
    private String sender_id;
    private BigInteger priority;
    private String success_url;
    private String failure_url;

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_pass() {
        return client_pass;
    }

    public void setClient_pass(String client_pass) {
        this.client_pass = client_pass;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public BigInteger getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(BigInteger operator_id) {
        this.operator_id = operator_id;
    }

    public String getRecipient_msisdn() {
        return recipient_msisdn;
    }

    public void setRecipient_msisdn(String recipient_msisdn) {
        this.recipient_msisdn = recipient_msisdn;
    }

    public String getConnection_type() {
        return connection_type;
    }

    public void setConnection_type(String connection_type) {
        this.connection_type = connection_type;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public BigInteger getPriority() {
        return priority;
    }

    public void setPriority(BigInteger priority) {
        this.priority = priority;
    }

    public String getSuccess_url() {
        return success_url;
    }

    public void setSuccess_url(String success_url) {
        this.success_url = success_url;
    }

    public String getFailure_url() {
        return failure_url;
    }

    public void setFailure_url(String failure_url) {
        this.failure_url = failure_url;
    }
}
