package tech.ipocket.pocket.request.external.amy;

public class FlightPriceRequest {
    private int searchId;
    private long departureFlightID;
    private long returnFlightID;

    public int getSearchId() {
        return searchId;
    }

    public void setSearchId(int searchId) {
        this.searchId = searchId;
    }

    public long getDepartureFlightID() {
        return departureFlightID;
    }

    public void setDepartureFlightID(long departureFlightID) {
        this.departureFlightID = departureFlightID;
    }

    public long getReturnFlightID() {
        return returnFlightID;
    }

    public void setReturnFlightID(long returnFlightID) {
        this.returnFlightID = returnFlightID;
    }
}
