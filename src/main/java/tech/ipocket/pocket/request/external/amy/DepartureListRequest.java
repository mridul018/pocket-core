package tech.ipocket.pocket.request.external.amy;

public class DepartureListRequest {
    private int routeType;
    private int preference;

    public int getRouteType() {
        return routeType;
    }

    public void setRouteType(int routeType) {
        this.routeType = routeType;
    }

    public int getPreference() {
        return preference;
    }

    public void setPreference(int preference) {
        this.preference = preference;
    }
}
