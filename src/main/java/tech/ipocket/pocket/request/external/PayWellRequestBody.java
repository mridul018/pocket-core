package tech.ipocket.pocket.request.external;

public class PayWellRequestBody {
    private String requestBody;

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
}
