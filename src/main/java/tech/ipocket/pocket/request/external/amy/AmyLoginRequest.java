package tech.ipocket.pocket.request.external.amy;

public class AmyLoginRequest {
    private String USER;
    private String PASS;
    private String DVID;
    private String CID;
    private String CMND = "_LOGINONLY_";

    public String getUSER() {
        return USER;
    }

    public void setUSER(String USER) {
        this.USER = USER;
    }

    public String getPASS() {
        return PASS;
    }

    public void setPASS(String PASS) {
        this.PASS = PASS;
    }

    public String getDVID() {
        return DVID;
    }

    public void setDVID(String DVID) {
        this.DVID = DVID;
    }

    public String getCID() {
        return CID;
    }

    public void setCID(String CID) {
        this.CID = CID;
    }

    public String getCMND() {
        return CMND;
    }

    public void setCMND(String CMND) {
        this.CMND = CMND;
    }
}
