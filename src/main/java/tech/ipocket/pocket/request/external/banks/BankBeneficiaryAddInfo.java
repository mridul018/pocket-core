package tech.ipocket.pocket.request.external.banks;

public class BankBeneficiaryAddInfo {
    private String appSecret;
    private String userSecret;
    private String name;
    private String accountNumber;
    private String type;
    private Address address;
    private String country;
    private String sortCode;
    private String branchAddress;
    private String branchName;
    private String phoneNumber;
    private String iban;
    private String swiftCode;
    private String sync;

    public BankBeneficiaryAddInfo() {
    }

    public BankBeneficiaryAddInfo(String appSecret, String userSecret, String name, String accountNumber, String type, Address address, String country, String sortCode, String branchAddress, String branchName, String phoneNumber, String iban, String swiftCode, String sync) {
        this.appSecret = appSecret;
        this.userSecret = userSecret;
        this.name = name;
        this.accountNumber = accountNumber;
        this.type = type;
        this.address = address;
        this.country = country;
        this.sortCode = sortCode;
        this.branchAddress = branchAddress;
        this.branchName = branchName;
        this.phoneNumber = phoneNumber;
        this.iban = iban;
        this.swiftCode = swiftCode;
        this.sync = sync;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getUserSecret() {
        return userSecret;
    }

    public void setUserSecret(String userSecret) {
        this.userSecret = userSecret;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
        this.sortCode = sortCode;
    }

    public String getBranchAddress() {
        return branchAddress;
    }

    public void setBranchAddress(String branchAddress) {
        this.branchAddress = branchAddress;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
