package tech.ipocket.pocket.request.external;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ProductInfo extends BaseRequestObject {

    private String countryIsos;
    private String providerCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(countryIsos == null){
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.COUNTRYISOS_NOT_FOUND));
            return;
        }

        if(providerCode == null){
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.PROVIDER_CODE_NOT_FOUND));
            return;
        }
    }

    public String getCountryIsos() {
        return countryIsos;
    }

    public void setCountryIsos(String countryIsos) {
        this.countryIsos = countryIsos;
    }

    public String getProviderCode() {
        return providerCode;
    }

    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }
}
