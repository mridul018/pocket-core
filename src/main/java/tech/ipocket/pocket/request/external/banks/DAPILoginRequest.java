package tech.ipocket.pocket.request.external.banks;

public class DAPILoginRequest {
    private String appSecret;
    private String userSecret;
    private boolean sync;

    public DAPILoginRequest() {
    }

    public DAPILoginRequest(String appSecret, String userSecret, boolean sync) {
        this.appSecret = appSecret;
        this.userSecret = userSecret;
        this.sync = sync;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getUserSecret() {
        return userSecret;
    }

    public void setUserSecret(String userSecret) {
        this.userSecret = userSecret;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }
}
