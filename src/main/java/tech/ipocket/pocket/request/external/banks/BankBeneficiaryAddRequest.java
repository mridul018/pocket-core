package tech.ipocket.pocket.request.external.banks;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BankBeneficiaryAddRequest extends BaseRequestObject {
    private String mobileNo;
    private String bankID;
    private BankBeneficiaryAddInfo bankBeneficiaryAddInfo;

    public BankBeneficiaryAddRequest() {
    }

    public BankBeneficiaryAddRequest(String mobileNo, String bankID, BankBeneficiaryAddInfo bankBeneficiaryAddInfo) {
        this.mobileNo = mobileNo;
        this.bankID = bankID;
        this.bankBeneficiaryAddInfo = bankBeneficiaryAddInfo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    public BankBeneficiaryAddInfo getBankBeneficiaryAddInfo() {
        return bankBeneficiaryAddInfo;
    }

    public void setBankBeneficiaryAddInfo(BankBeneficiaryAddInfo bankBeneficiaryAddInfo) {
        this.bankBeneficiaryAddInfo = bankBeneficiaryAddInfo;
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(mobileNo == null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if(bankID == null){
            baseResponseObject.setErrorCode(PocketErrorCode.BANK_ID_REQUIRED);
            return;
        }
    }
}
