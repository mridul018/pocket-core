package tech.ipocket.pocket.request.external;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ProviderListRequest extends BaseRequestObject {

    private String countryIsos;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(countryIsos == null ){
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.COUNTRYISOS_NOT_FOUND));
            return;
        }
    }

    public String getCountryIsos() {
        return countryIsos;
    }

    public void setCountryIsos(String countryIsos) {
        this.countryIsos = countryIsos;
    }
}
