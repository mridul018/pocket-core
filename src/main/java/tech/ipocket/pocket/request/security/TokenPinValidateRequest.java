package tech.ipocket.pocket.request.security;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class TokenPinValidateRequest extends BaseRequestObject {
    private String credential;
    private String otpText;
    private String otpSecret;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (credential == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.CredentialRequired);
            return;
        }
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }
}
