package tech.ipocket.pocket.request.security;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class TokenGenerateRequest extends BaseRequestObject {

    private String mobileNumber;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (this.mobileNumber == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired));
            return;
        }
        if (getHardwareSignature() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.InvalidInput));
            return;
        }
        if (getRequestId() == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.InvalidInput));
            return;
        }
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
