package tech.ipocket.pocket.request.security;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class TokenValidateRequest extends BaseRequestObject {


    public TokenValidateRequest(String sessionToken, String hardwareSignature) {
        this.setSessionToken(sessionToken);
        this.setHardwareSignature(hardwareSignature);
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }
}
