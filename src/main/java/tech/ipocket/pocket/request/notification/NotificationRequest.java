package tech.ipocket.pocket.request.notification;

import java.math.BigDecimal;

public class NotificationRequest {
    private BigDecimal transactionAmount;
    private String receiverWallet;
    private String senderWalletNo;
    private String transactionToken;
    private String serviceCode;
    private String feePayer;
    private BigDecimal feeAmount;
    private String request;
    private String response;
    private String requestId;
    private String notes;
    private String bankName;
    private String bankAccountNumber;
    private String senderFcmKey;
    private String receiverFcmKey;

    private String billNumber;
    private String accountNumber;

    private String pinNumber;
    private String pinServiceType;

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getReceiverWallet() {
        return receiverWallet;
    }

    public void setReceiverWallet(String receiverWallet) {
        this.receiverWallet = receiverWallet;
    }

    public String getSenderWalletNo() {
        return senderWalletNo;
    }

    public void setSenderWalletNo(String senderWalletNo) {
        this.senderWalletNo = senderWalletNo;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getSenderFcmKey() {
        return senderFcmKey;
    }

    public void setSenderFcmKey(String senderFcmKey) {
        this.senderFcmKey = senderFcmKey;
    }

    public String getReceiverFcmKey() {
        return receiverFcmKey;
    }

    public void setReceiverFcmKey(String receiverFcmKey) {
        this.receiverFcmKey = receiverFcmKey;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public String getPinServiceType() {
        return pinServiceType;
    }

    public void setPinServiceType(String pinServiceType) {
        this.pinServiceType = pinServiceType;
    }
}
