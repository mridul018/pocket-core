package tech.ipocket.pocket.request.ts.account;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class WalletCreateRequest extends BaseRequestObject {
    private String mobileNumber;
    private String fullName;
    private String groupCode;
    private String userStatus;
    private String dob;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (mobileNumber == null || mobileNumber.trim().equals("")) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MobileNumberRequired));
            return;
        }

        if (fullName == null || fullName.trim().equals("")) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FullNameRequired));
            return;
        }

        if (groupCode == null || groupCode.trim().equals("")) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.GroupCodeRequired));
            return;
        }
        if (groupCode.length() != 4) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.GroupCodeLengthError));
            return;
        }

        if (userStatus == null || userStatus.trim().equals("")) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.UserStatusRequired));
            return;
        }

    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
