package tech.ipocket.pocket.request.ts.settlement;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class FundMovementWalletToWalletRequest extends BaseRequestObject {
    private String sender;
    private String receiver;
    private BigDecimal amount;

    private String fundMovementType;
    private String remarks;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (fundMovementType == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.FundMovementTypeRequired);
            return;
        }

        if (sender == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.SenderIsRequired);
            return;
        }

        if (receiver == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.ReceiverIsRequired);
            return;
        }

        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getFundMovementType() {
        return fundMovementType;
    }

    public void setFundMovementType(String fundMovementType) {
        this.fundMovementType = fundMovementType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
