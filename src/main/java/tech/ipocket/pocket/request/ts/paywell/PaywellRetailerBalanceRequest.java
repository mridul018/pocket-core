package tech.ipocket.pocket.request.ts.paywell;

public class PaywellRetailerBalanceRequest {
    private String username;  //ClientUserName
    private String password;  //ClientPassword

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
