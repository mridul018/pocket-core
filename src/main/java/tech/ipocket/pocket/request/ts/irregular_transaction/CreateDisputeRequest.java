package tech.ipocket.pocket.request.ts.irregular_transaction;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class CreateDisputeRequest extends BaseRequestObject {
    private String transactionToken;
    private String reason;
    private String requesterWallet;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRequesterWallet() {
        return requesterWallet;
    }

    public void setRequesterWallet(String requesterWallet) {
        this.requesterWallet = requesterWallet;
    }
}
