package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetTransactionSummeryDetailsRequest extends BaseRequestObject {

    private String mobileNumber;
    private String transactionType;
    private String fromDate;
    private String toDate;
    private String deviceNo;
    private String transactionCategory;// IN,OUT


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(transactionCategory==null){
            baseResponseObject.setErrorCode(PocketErrorCode.TransactionCategoryRequired);
            return;
        }

        if(transactionType==null){
            baseResponseObject.setErrorCode(PocketErrorCode.TransactionTypeRequired);
            return;
        }

        if(fromDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.FromDateRequired);
            return;
        }

        if(toDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ToDateRequired);
            return;
        }

    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(String transactionCategory) {
        this.transactionCategory = transactionCategory;
    }
}
