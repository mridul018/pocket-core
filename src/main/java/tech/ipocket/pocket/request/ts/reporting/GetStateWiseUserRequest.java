package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;


public class GetStateWiseUserRequest extends PageItemBaseRequestObject {

    private List<String> userType;
    private String fromDate;
    private String toDate;
    private String countryCode;
    private List<String> stateId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(userType==null){
            baseResponseObject.setErrorCode(PocketErrorCode.UserTypeRequired);
            return;
        }

    }

    public List<String> getUserType() {
        return userType;
    }

    public void setUserType(List<String> userType) {
        this.userType = userType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<String> getStateId() {
        return stateId;
    }

    public void setStateId(List<String> stateId) {
        this.stateId = stateId;
    }
}
