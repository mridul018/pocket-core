package tech.ipocket.pocket.request.ts.settlement;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class AdminCashInToCustomerRequest extends BaseRequestObject {
    private String receiverWallet;
    private String notes;
    private BigDecimal amount;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (receiverWallet == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.ReceiverWalletIsRequired);
            return;
        }

        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public String getReceiverWallet() {
        return receiverWallet;
    }

    public void setReceiverWallet(String receiverWallet) {
        this.receiverWallet = receiverWallet;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
