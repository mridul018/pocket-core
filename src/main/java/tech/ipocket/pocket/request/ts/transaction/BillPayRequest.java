package tech.ipocket.pocket.request.ts.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketConstants;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class BillPayRequest extends BaseRequestObject {
    private Double transferAmount;
    private String billPayType;
    private String accountNumber;
    private String mobileNumber;
    private String billNumber;
    private String fullName;
    private String logedInUserMobileNumber;

    private String billMonth;
    private String billYear;

    private String otpText;
    private String otpSecret;
    private String pin;
    private String notes;
    private String countryCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (billPayType == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BillPayTypeRequired);
            return;
        }
        if (accountNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AccountNumberRequired);
            return;
        }
        if (mobileNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if (billNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BillNumberRequired);
            return;
        }


        if (transferAmount == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

        if(logedInUserMobileNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }

        if(fullName==null){
            baseResponseObject.setErrorCode(PocketErrorCode.FullNameRequired);
            return;
        }

        if(billMonth==null){
            baseResponseObject.setErrorCode(PocketErrorCode.PalliBidyutBillPayMonthNotFound);
            return;
        }
        if(billYear==null){
            baseResponseObject.setErrorCode(PocketErrorCode.PalliBidyutBillPayYearNotFound);
            return;
        }
    }

    public Double getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(Double transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getBillPayType() {
        return billPayType;
    }

    public void setBillPayType(String billPayType) {
        this.billPayType = billPayType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLogedInUserMobileNumber() {
        return logedInUserMobileNumber;
    }

    public void setLogedInUserMobileNumber(String logedInUserMobileNumber) {
        this.logedInUserMobileNumber = logedInUserMobileNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBillMonth() {
        return billMonth;
    }

    public void setBillMonth(String billMonth) {
        this.billMonth = billMonth;
    }

    public String getBillYear() {
        return billYear;
    }

    public void setBillYear(String billYear) {
        this.billYear = billYear;
    }
}
