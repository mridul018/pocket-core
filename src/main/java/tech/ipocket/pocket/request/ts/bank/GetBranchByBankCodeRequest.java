package tech.ipocket.pocket.request.ts.bank;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetBranchByBankCodeRequest extends BaseRequestObject {
    private String bankCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(bankCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeRequired);
            return;
        }
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
