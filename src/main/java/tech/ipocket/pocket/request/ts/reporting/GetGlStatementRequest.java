package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetGlStatementRequest extends PageItemBaseRequestObject {

    private String glCode;
    private String fromDate;
    private String toDate;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(glCode==null||glCode.trim().length()==0){
            baseResponseObject.setErrorCode(PocketErrorCode.GlCodeRequired);
            return;
        }

        if(fromDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.FromDateRequired);
            return;
        }

        if(toDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ToDateRequired);
            return;
        }

    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
