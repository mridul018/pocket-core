package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class GetCustomerLimitRequest extends BaseRequestObject {

    private String mobileNumber;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {


    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
