package tech.ipocket.pocket.request.ts.transactionValidity;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class TransactionValidityRequest extends BaseRequestObject {
    private String transferAmount;
    private String type;
    private String senderMobileNumber;
    private String receiverMobileNumber;
    private String countryCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (senderMobileNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.SenderWalletIsRequired);
            return;
        }
        if (receiverMobileNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.ReceiverWalletIsRequired);
            return;
        }

        if (type == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidTransactionType);
            return;
        }


        if (transferAmount == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public String getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(String transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getReceiverMobileNumber() {
        return receiverMobileNumber;
    }

    public void setReceiverMobileNumber(String receiverMobileNumber) {
        this.receiverMobileNumber = receiverMobileNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
