package tech.ipocket.pocket.request.ts.settlement;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class AdminCashOutFromCustomerRequest extends BaseRequestObject {
    private String senderWallet;
    private BigDecimal amount;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (senderWallet == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.SenderWalletIsRequired);
            return;
        }

        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public String getSenderWallet() {
        return senderWallet;
    }

    public void setSenderWallet(String senderWallet) {
        this.senderWallet = senderWallet;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
