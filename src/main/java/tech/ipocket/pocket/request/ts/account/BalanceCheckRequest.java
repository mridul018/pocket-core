package tech.ipocket.pocket.request.ts.account;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class BalanceCheckRequest extends BaseRequestObject {

    private String mobilenumber;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }
}
