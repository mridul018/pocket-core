package tech.ipocket.pocket.request.ts.transactionProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class GetLimitRequest extends BaseRequestObject {
    private String limitCode;
    private String transactionProfileCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {


    }

    public String getLimitCode() {
        return limitCode;
    }

    public void setLimitCode(String limitCode) {
        this.limitCode = limitCode;
    }

    public String getTransactionProfileCode() {
        return transactionProfileCode;
    }

    public void setTransactionProfileCode(String transactionProfileCode) {
        this.transactionProfileCode = transactionProfileCode;
    }
}
