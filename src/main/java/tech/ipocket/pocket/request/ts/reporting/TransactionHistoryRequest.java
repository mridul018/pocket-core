package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryRequest extends PageItemBaseRequestObject {

    private String filterType;
    private String transactionToken;
    private List<String> transactionTypes;
    private String mobileNumber;
    private String fromDate;
    private String toDate;

    List<String> txStatus;

    private String countryCode;
    private List<String> stateList;
    private List<String> userTypes;

    public TransactionHistoryRequest() {
        stateList=new ArrayList<>();
        userTypes=new ArrayList<>();
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(filterType==null){
            baseResponseObject.setErrorCode(PocketErrorCode.FilterTypeRequired);
            return;
        }

    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public List<String> getTransactionTypes() {
        return transactionTypes;
    }

    public void setTransactionTypes(List<String> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<String> getTxStatus() {
        return txStatus;
    }

    public void setTxStatus(List<String> txStatus) {
        this.txStatus = txStatus;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public List<String> getStateList() {
        return stateList;
    }

    public void setStateList(List<String> stateList) {
        this.stateList = stateList;
    }

    public List<String> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(List<String> userTypes) {
        this.userTypes = userTypes;
    }
}
