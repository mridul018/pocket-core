package tech.ipocket.pocket.request.ts.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class TransactionRequest extends BaseRequestObject {
    private Double transferAmount;
    private Integer convertedAmount;
    private String transactionType;
    private String senderMobileNumber;
    private String receiverMobileNumber;
    private String otpText;
    private String otpSecret;
    private String pin;
    private String notes;

    private String operatorCode;
    private String countryCode;
    @JsonProperty
    private Boolean isPrePaid;
    private String skuCode;
    private String topUpMedium; //1= customer self, 2=Agent app

    private String topupProviderName;
    private String topupProviderCode;

    private String bankCode;
    private String branchSwiftCode;
    private String bankAccountNumber;



    @Override
    public void validate(BaseResponseObject baseResponseObject) {
//        if (senderMobileNumber == null) {
//            baseResponseObject.setErrorCode(PocketErrorCode.SenderWalletIsRequired);
//            return;
//        }
//        if (receiverMobileNumber == null) {
//            baseResponseObject.setErrorCode(PocketErrorCode.ReceiverWalletIsRequired);
//            return;
//        }

        if (transactionType == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidTransactionType);
            return;
        }


        if (transferAmount == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public Double getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(Double transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSenderMobileNumber() {
        return senderMobileNumber;
    }

    public void setSenderMobileNumber(String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }

    public String getReceiverMobileNumber() {
        return receiverMobileNumber;
    }

    public void setReceiverMobileNumber(String receiverMobileNumber) {
        this.receiverMobileNumber = receiverMobileNumber;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public void setBranchSwiftCode(String branchSwiftCode) {
        this.branchSwiftCode = branchSwiftCode;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getPrePaid() {
        return isPrePaid;
    }

    public void setPrePaid(Boolean prePaid) {
        isPrePaid = prePaid;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public Integer getConvertedAmount() {
        return convertedAmount;
    }

    public void setConvertedAmount(Integer convertedAmount) {
        this.convertedAmount = convertedAmount;
    }

    public String getTopUpMedium() {
        return topUpMedium;
    }

    public void setTopUpMedium(String topUpMedium) {
        this.topUpMedium = topUpMedium;
    }



    public String getTopupProviderName() {
        return topupProviderName;
    }

    public void setTopupProviderName(String topupProviderName) {
        this.topupProviderName = topupProviderName;
    }

    public String getTopupProviderCode() {
        return topupProviderCode;
    }

    public void setTopupProviderCode(String topupProviderCode) {
        this.topupProviderCode = topupProviderCode;
    }
}
