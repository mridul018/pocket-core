package tech.ipocket.pocket.request.ts;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class FeeShareRequest extends BaseRequestObject {

    private String transactionReference;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(transactionReference==null){
            baseResponseObject.setErrorCode(PocketErrorCode.TransactionTypeRequired);
            return;
        }
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }
}
