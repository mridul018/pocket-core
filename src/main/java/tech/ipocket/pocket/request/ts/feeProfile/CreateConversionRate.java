package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateConversionRate extends BaseRequestObject {
    private int id;
    private String serviceCode;
    private String serviceName;
    private Double conversionRate;
    private Double actualRate;
    private String status;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (serviceCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ServiceCodeRequired));
            return;
        }

        if (serviceName == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ServiceNameRequired));
            return;
        }

        if (conversionRate == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ConversionRateRequired));
            return;
        }
        if (actualRate == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ActualRateRequired));
            return;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

    public Double getActualRate() {
        return actualRate;
    }

    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
