package tech.ipocket.pocket.request.ts.bank;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateBranchRequest extends BaseRequestObject {
    private int id;
    private String branchName;
    private String branchSwiftCode;
    private String bankCode;
    private String branchStatus;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (branchName == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BranchNameRequired);
            return;
        }

        if (branchSwiftCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BranchSwiftCodeRequired);
            return;
        }

        if (bankCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeRequired);
            return;
        }
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public void setBranchSwiftCode(String branchSwiftCode) {
        this.branchSwiftCode = branchSwiftCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBranchStatus() {
        return branchStatus;
    }

    public void setBranchStatus(String branchStatus) {
        this.branchStatus = branchStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
