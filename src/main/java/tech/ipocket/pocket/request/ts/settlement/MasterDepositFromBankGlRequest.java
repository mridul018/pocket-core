package tech.ipocket.pocket.request.ts.settlement;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class MasterDepositFromBankGlRequest extends BaseRequestObject {
    private BigDecimal amount;
    private String bankCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if(bankCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeRequired);
            return;

        }

        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
