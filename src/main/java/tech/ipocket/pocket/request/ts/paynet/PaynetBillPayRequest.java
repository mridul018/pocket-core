package tech.ipocket.pocket.request.ts.paynet;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class PaynetBillPayRequest extends BaseRequestObject {
    private String billPayType;
    private String accountNumber;
    private String logedInUserMobileNumber;

    private String otpText;
    private String otpSecret;
    private String pin;
    private String notes;

    private int simulation;
    private String transactionId;
    private String externalTransactionId;
    private String product;
    private Double amount;
    private String pinReceiverMobileNumber;

    //For active a salik tag online
    private String salikTagNumber;
    private String salikTagKey;
    private String mobilePhoneNumber;
    private String TcNumber;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (billPayType == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BillPayTypeRequired);
            return;
        }
        if (accountNumber == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.AccountNumberRequired);
            return;
        }
        if(logedInUserMobileNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if(otpText==null){
            baseResponseObject.setErrorCode(PocketErrorCode.OtpRequired);
            return;
        }
        if(otpSecret==null){
            baseResponseObject.setErrorCode(PocketErrorCode.OtpSecretRequired);
            return;
        }
        if(pin==null){
            baseResponseObject.setErrorCode(PocketErrorCode.CredentialRequired);
            return;
        }
        if(amount==null){
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

        if(salikTagNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_SALIK_TAG_NUMBER);
            return;
        }
        if(salikTagKey==null){
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_SALIK_TAG_KEY);
            return;
        }
        if(mobilePhoneNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }
        if(TcNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_SALIK_TC_NUMBER);
            return;
        }

    }

    public String getBillPayType() {
        return billPayType;
    }

    public void setBillPayType(String billPayType) {
        this.billPayType = billPayType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getLogedInUserMobileNumber() {
        return logedInUserMobileNumber;
    }

    public void setLogedInUserMobileNumber(String logedInUserMobileNumber) {
        this.logedInUserMobileNumber = logedInUserMobileNumber;
    }

    public String getOtpText() {
        return otpText;
    }

    public void setOtpText(String otpText) {
        this.otpText = otpText;
    }

    public String getOtpSecret() {
        return otpSecret;
    }

    public void setOtpSecret(String otpSecret) {
        this.otpSecret = otpSecret;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getSimulation() {
        return simulation;
    }

    public void setSimulation(int simulation) {
        this.simulation = simulation;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSalikTagNumber() {
        return salikTagNumber;
    }

    public void setSalikTagNumber(String salikTagNumber) {
        this.salikTagNumber = salikTagNumber;
    }

    public String getSalikTagKey() {
        return salikTagKey;
    }

    public void setSalikTagKey(String salikTagKey) {
        this.salikTagKey = salikTagKey;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getTcNumber() {
        return TcNumber;
    }

    public void setTcNumber(String tcNumber) {
        TcNumber = tcNumber;
    }

    public String getPinReceiverMobileNumber() {
        return pinReceiverMobileNumber;
    }

    public void setPinReceiverMobileNumber(String pinReceiverMobileNumber) {
        this.pinReceiverMobileNumber = pinReceiverMobileNumber;
    }
}
