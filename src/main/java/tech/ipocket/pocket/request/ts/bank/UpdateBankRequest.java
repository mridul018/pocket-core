package tech.ipocket.pocket.request.ts.bank;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UpdateBankRequest extends BaseRequestObject {
    private String bankCode;
    private String bankStatus;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (bankCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeRequired);
            return;
        }

        if (bankCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeLengthError);
            return;
        }

    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankStatus() {
        return bankStatus;
    }

    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus;
    }
}
