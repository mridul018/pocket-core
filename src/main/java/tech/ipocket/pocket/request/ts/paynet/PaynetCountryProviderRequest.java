package tech.ipocket.pocket.request.ts.paynet;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class PaynetCountryProviderRequest extends BaseRequestObject {

    int countryId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (String.valueOf(countryId) == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_COUNTRY_ID_REQUIRED);
            return;
        }
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
