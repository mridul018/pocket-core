package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;
import java.util.List;

public class CreateFeeRequest extends BaseRequestObject {
    private Integer id;
    private String feeCode;
    private String feeName;
    private BigDecimal fixedFee;
    private BigDecimal percentageFee;
    private int isBoth;
    private String feePayer;
    private String appliedFeeMethod;
    private List<FeeStakeHolderItem> stakeHolders;
    private String status;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (feeCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeCodeRequired));
            return;
        }

        if (feeCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.FeeCodeLengthError);
            return;
        }


        if (feeName == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeNameRequired));
            return;
        }

        if (feePayer == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeePayerRequired));
            return;
        }

        if (feePayer.length()!=2) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeePayerLengthMustTwo));
            return;
        }

        if (appliedFeeMethod == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.AppliedMethodRequired));
            return;
        }

        if (appliedFeeMethod .length()!=1) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.AppliedMethodLengthMustOne));
            return;
        }


    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getFeeName() {
        return feeName;
    }

    public void setFeeName(String feeName) {
        this.feeName = feeName;
    }

    public BigDecimal getFixedFee() {
        return fixedFee;
    }

    public void setFixedFee(BigDecimal fixedFee) {
        this.fixedFee = fixedFee;
    }

    public BigDecimal getPercentageFee() {
        return percentageFee;
    }

    public void setPercentageFee(BigDecimal percentageFee) {
        this.percentageFee = percentageFee;
    }

    public int getBoth() {
        return isBoth;
    }

    public void setBoth(int both) {
        isBoth = both;
    }

    public String getFeePayer() {
        return feePayer;
    }

    public void setFeePayer(String feePayer) {
        this.feePayer = feePayer;
    }

    public String getAppliedFeeMethod() {
        return appliedFeeMethod;
    }

    public void setAppliedFeeMethod(String appliedFeeMethod) {
        this.appliedFeeMethod = appliedFeeMethod;
    }

    public List<FeeStakeHolderItem> getStakeHolders() {
        return stakeHolders;
    }

    public void setStakeHolders(List<FeeStakeHolderItem> stakeHolders) {
        this.stakeHolders = stakeHolders;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
