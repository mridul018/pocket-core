package tech.ipocket.pocket.request.ts.account;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class TsUpdateClientStatusRequest extends BaseRequestObject {
    private String walletId;
    private String newStatus;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (walletId == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.WalletNotFound));
            return;
        }

        if (newStatus == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.NewStatusRequired));
            return;
        }
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }
}
