package tech.ipocket.pocket.request.ts.irregular_transaction;


import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class ReversalRequest extends BaseRequestObject {
    private String transactionToken;
    private String reason;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(transactionToken==null){
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidTransactionToken);
            return;
        }

    }

    public String getTransactionToken() {
        return transactionToken;
    }

    public void setTransactionToken(String transactionToken) {
        this.transactionToken = transactionToken;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
