package tech.ipocket.pocket.request.ts.fee_share;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class ShareConversionRateRequest extends BaseRequestObject {
    private String mainTransactionToken;
    private BigDecimal sharedAmount;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getMainTransactionToken() {
        return mainTransactionToken;
    }

    public void setMainTransactionToken(String mainTransactionToken) {
        this.mainTransactionToken = mainTransactionToken;
    }

    public BigDecimal getSharedAmount() {
        return sharedAmount;
    }

    public void setSharedAmount(BigDecimal sharedAmount) {
        this.sharedAmount = sharedAmount;
    }
}
