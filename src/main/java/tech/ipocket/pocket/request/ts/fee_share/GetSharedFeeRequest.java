package tech.ipocket.pocket.request.ts.fee_share;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetSharedFeeRequest extends BaseRequestObject {
    private String startDate;
    private String endDate;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(startDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.StartDateRequired);
            return;
        }
        if(endDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.EndDateRequired);
            return;
        }
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
