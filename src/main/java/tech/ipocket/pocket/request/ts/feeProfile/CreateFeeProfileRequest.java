package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateFeeProfileRequest extends BaseRequestObject {
    private String profileCode;
    private String profileName;
    private String status;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (profileCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeProfileCodeRequired));
            return;
        }


        if (profileCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.ProfileCodeLengthError);
            return;
        }

        if (profileName == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeProfileNameRequired));
            return;
        }

        /*if(status == null){
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeProfileStatusRequired));
            return;
        }*/
    }

    public String getProfileCode() {
        return profileCode;
    }

    public void setProfileCode(String profileCode) {
        this.profileCode = profileCode;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
