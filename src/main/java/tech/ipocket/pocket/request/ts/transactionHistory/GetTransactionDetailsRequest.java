package tech.ipocket.pocket.request.ts.transactionHistory;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class GetTransactionDetailsRequest extends BaseRequestObject {

    private String transactionTokenNo;
    private String walletNo;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(transactionTokenNo==null){
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidTransactionToken);
        }

    }

    public String getTransactionTokenNo() {
        return transactionTokenNo;
    }

    public void setTransactionTokenNo(String transactionTokenNo) {
        this.transactionTokenNo = transactionTokenNo;
    }

    public String getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(String walletNo) {
        this.walletNo = walletNo;
    }
}
