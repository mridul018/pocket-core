package tech.ipocket.pocket.request.ts.irregular_transaction;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.math.BigDecimal;

public class RefundRequest extends BaseRequestObject {
    private String txReferenceToken;
    private String reason;
    private BigDecimal amount;
    private String refundSenderWallet;
    private String refundReceiverWallet;
    private String notes;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(amount==null||amount.compareTo(BigDecimal.ZERO)==0){
            baseResponseObject.setErrorCode(PocketErrorCode.AmountRequired);
            return;
        }

        if(txReferenceToken==null){
            baseResponseObject.setErrorCode(PocketErrorCode.TxTokenRequired);
            return;
        }

    }

    public String getTxReferenceToken() {
        return txReferenceToken;
    }

    public void setTxReferenceToken(String txReferenceToken) {
        this.txReferenceToken = txReferenceToken;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRefundSenderWallet() {
        return refundSenderWallet;
    }

    public void setRefundSenderWallet(String refundSenderWallet) {
        this.refundSenderWallet = refundSenderWallet;
    }

    public String getRefundReceiverWallet() {
        return refundReceiverWallet;
    }

    public void setRefundReceiverWallet(String refundReceiverWallet) {
        this.refundReceiverWallet = refundReceiverWallet;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
