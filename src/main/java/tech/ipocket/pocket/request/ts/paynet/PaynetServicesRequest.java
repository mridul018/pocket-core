package tech.ipocket.pocket.request.ts.paynet;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class PaynetServicesRequest extends BaseRequestObject {

    private String serviceType;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
