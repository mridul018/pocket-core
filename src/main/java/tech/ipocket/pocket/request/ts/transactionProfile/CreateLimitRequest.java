package tech.ipocket.pocket.request.ts.transactionProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateLimitRequest extends BaseRequestObject {
    private int id;
    private String limitCode;
    private String limitName;
    private String status;
    private double minAmount;
    private double maxAmount;
    private int dailyMaxNoOfTransaction;
    private double dailyMaxTransactionAmount;
    private int montlyMaxNoOfTransaction;
    private double monthlyMaxTransactionAmount;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (limitCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.LimitCodeRequired));
            return;
        }


        if (limitCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.LimitCodeLengthError);
            return;
        }

        if (limitName == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.LimitNameRequired));
            return;
        }

        if (minAmount < 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MinAmountFailed));
            return;
        }

        if (maxAmount <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MaxAmountFailed));
            return;
        }

        if (dailyMaxNoOfTransaction <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.DailyMaxNoOfTxRequired));
            return;
        }

        if (dailyMaxTransactionAmount <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.DailyMaxTxAmountRequired));
            return;
        }

        if (montlyMaxNoOfTransaction <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MonthlyMaxNoOfTxRequired));
            return;
        }

        if (monthlyMaxTransactionAmount <= 0) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.MonthlyMaxTxAmountRequired));
            return;
        }
    }

    public String getLimitCode() {
        return limitCode;
    }

    public void setLimitCode(String limitCode) {
        this.limitCode = limitCode;
    }

    public String getLimitName() {
        return limitName;
    }

    public void setLimitName(String limitName) {
        this.limitName = limitName;
    }

    public double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }

    public double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public int getDailyMaxNoOfTransaction() {
        return dailyMaxNoOfTransaction;
    }

    public void setDailyMaxNoOfTransaction(int dailyMaxNoOfTransaction) {
        this.dailyMaxNoOfTransaction = dailyMaxNoOfTransaction;
    }

    public double getDailyMaxTransactionAmount() {
        return dailyMaxTransactionAmount;
    }

    public void setDailyMaxTransactionAmount(double dailyMaxTransactionAmount) {
        this.dailyMaxTransactionAmount = dailyMaxTransactionAmount;
    }

    public int getMontlyMaxNoOfTransaction() {
        return montlyMaxNoOfTransaction;
    }

    public void setMontlyMaxNoOfTransaction(int montlyMaxNoOfTransaction) {
        this.montlyMaxNoOfTransaction = montlyMaxNoOfTransaction;
    }

    public double getMonthlyMaxTransactionAmount() {
        return monthlyMaxTransactionAmount;
    }

    public void setMonthlyMaxTransactionAmount(double monthlyMaxTransactionAmount) {
        this.monthlyMaxTransactionAmount = monthlyMaxTransactionAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
