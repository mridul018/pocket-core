package tech.ipocket.pocket.request.ts.transactionHistory;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

import java.util.List;

public class GetTransactionHistoryRequest extends PageItemBaseRequestObject {

    private List<String> transactionTypes;
    private String walletNo;
    private String fromDate;
    private String toDate;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(String walletNo) {
        this.walletNo = walletNo;
    }

    public List<String> getTransactionTypes() {
        return transactionTypes;
    }

    public void setTransactionTypes(List<String> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
