package tech.ipocket.pocket.request.ts.bank;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateBankRequest extends BaseRequestObject {
    private int id;
    private String bankName;
    private String bankRoutingNumber;
    private String bankGlCode;
    private String bankCode;
    private String bankStatus;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (bankName == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankNameRequired);
            return;
        }

        if (bankGlCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankGlCodeRequired);
            return;
        }

        if (bankCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeRequired);
            return;
        }

        if (bankCode.length() != 4) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeLengthError);
            return;
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankRoutingNumber() {
        return bankRoutingNumber;
    }

    public void setBankRoutingNumber(String bankRoutingNumber) {
        this.bankRoutingNumber = bankRoutingNumber;
    }

    public String getBankGlCode() {
        return bankGlCode;
    }

    public void setBankGlCode(String bankGlCode) {
        this.bankGlCode = bankGlCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankStatus() {
        return bankStatus;
    }

    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus;
    }
}
