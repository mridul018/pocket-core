package tech.ipocket.pocket.request.ts.transactionProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateTransactionProfileLimitServiceMapRequest extends BaseRequestObject {

    private String txProfileCode;
    private String serviceCode;
    private String limitCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (txProfileCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.TransactionProfileCodeRequired));
            return;
        }

        if (serviceCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ServiceCodeRequired));
            return;
        }

        if (limitCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.LimitCodeRequired));
            return;
        }

    }

    public String getTxProfileCode() {
        return txProfileCode;
    }

    public void setTxProfileCode(String txProfileCode) {
        this.txProfileCode = txProfileCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getLimitCode() {
        return limitCode;
    }

    public void setLimitCode(String limitCode) {
        this.limitCode = limitCode;
    }
}
