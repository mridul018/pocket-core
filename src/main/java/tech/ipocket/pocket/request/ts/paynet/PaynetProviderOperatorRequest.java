package tech.ipocket.pocket.request.ts.paynet;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class PaynetProviderOperatorRequest extends BaseRequestObject {

    int providerId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (String.valueOf(providerId) == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.PAYNET_PROVIDER_ID_REQUIRED);
            return;
        }
    }

    public int getProviderId() {
        return providerId;
    }

    public void setProviderId(int providerId) {
        this.providerId = providerId;
    }
}
