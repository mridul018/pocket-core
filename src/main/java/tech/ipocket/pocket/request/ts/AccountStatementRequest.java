package tech.ipocket.pocket.request.ts;

import org.springframework.data.domain.PageRequest;
import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class AccountStatementRequest extends PageItemBaseRequestObject {
    private String mobileNumber;
    private String fromDate;
    private String toDate;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getMobileNumber() {

        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
