package tech.ipocket.pocket.request.ts.feeProfile;

public class FeeStakeHolderItem {
    private int id;
    private String stakeholderName;
    private String glCode;
    private double percentage;
    private String stakeholderPaymentMethod;

    public String getStakeholderName() {
        return stakeholderName;
    }

    public void setStakeholderName(String stakeholderName) {
        this.stakeholderName = stakeholderName;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStakeholderPaymentMethod() {
        return stakeholderPaymentMethod;
    }

    public void setStakeholderPaymentMethod(String stakeholderPaymentMethod) {
        this.stakeholderPaymentMethod = stakeholderPaymentMethod;
    }
}
