package tech.ipocket.pocket.request.ts.bank;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class UpdateBranchRequest extends BaseRequestObject {
    private String branchSwiftCode;
    private String bankCode;
    private String branchStatus;
    private int id;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if (branchSwiftCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BranchSwiftCodeRequired);
            return;
        }

        if (bankCode == null) {
            baseResponseObject.setErrorCode(PocketErrorCode.BankCodeRequired);
            return;
        }
    }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public void setBranchSwiftCode(String branchSwiftCode) {
        this.branchSwiftCode = branchSwiftCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBranchStatus() {
        return branchStatus;
    }

    public void setBranchStatus(String branchStatus) {
        this.branchStatus = branchStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
