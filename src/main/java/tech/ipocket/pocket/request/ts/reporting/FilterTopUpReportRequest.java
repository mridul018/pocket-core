package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class FilterTopUpReportRequest extends PageItemBaseRequestObject {

    private List<String> countryCodes;
    private List<String> stateList;

    private String mobileNumber;
    private String fromDate;
    private String toDate;

    List<String> operatorList;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(countryCodes==null||countryCodes.size()==0){
            baseResponseObject.setErrorCode(PocketErrorCode.CountryCodeRequired);
            return;
        }

        if(fromDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.FromDateRequired);
            return;
        }
        if(toDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ToDateRequired);
            return;
        }



    }

    public List<String> getCountryCodes() {
        return countryCodes;
    }

    public void setCountryCodes(List<String> countryCodes) {
        this.countryCodes = countryCodes;
    }

    public List<String> getStateList() {
        return stateList;
    }

    public void setStateList(List<String> stateList) {
        this.stateList = stateList;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<String> getOperatorList() {
        return operatorList;
    }

    public void setOperatorList(List<String> operatorList) {
        this.operatorList = operatorList;
    }
}
