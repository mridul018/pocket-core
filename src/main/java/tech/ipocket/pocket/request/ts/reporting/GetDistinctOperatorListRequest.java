package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.List;

public class GetDistinctOperatorListRequest extends PageItemBaseRequestObject {

    private String countryCode;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(countryCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.CountryCodeRequired);
            return;
        }

    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
