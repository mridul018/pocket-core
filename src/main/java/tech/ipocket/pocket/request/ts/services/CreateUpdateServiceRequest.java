package tech.ipocket.pocket.request.ts.services;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateUpdateServiceRequest extends BaseRequestObject {
    private int id;
    private String serviceCode;
    private String serviceName;
    private String description;
    private String status;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(serviceCode==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ServiceCodeRequired);
            return;
        }
        if(serviceCode.length()!=4){
            baseResponseObject.setErrorCode(PocketErrorCode.InvalidServiceCodeLength);
            return;
        }

        if(serviceName==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ServiceNameRequired);
            return;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
