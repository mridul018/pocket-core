package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

import java.util.ArrayList;
import java.util.List;

public class GetSummeryOfTransactionRequest extends BaseRequestObject {

    private String fromDate;
    private String toDate;

    private String transactionCategory;//IN,OUT
    private List<String> transactionTypes;
    private String mobileNumber;

    public GetSummeryOfTransactionRequest() {
        transactionTypes=new ArrayList<>();
    }

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if(mobileNumber==null){
            baseResponseObject.setErrorCode(PocketErrorCode.MobileNumberRequired);
            return;
        }

        if(fromDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.FromDateRequired);
            return;
        }

        if(toDate==null){
            baseResponseObject.setErrorCode(PocketErrorCode.ToDateRequired);
            return;
        }

        if(transactionCategory==null){
            baseResponseObject.setErrorCode(PocketErrorCode.TransactionCategoryRequired);
            return;
        }


    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTransactionCategory() {
        return transactionCategory;
    }

    public void setTransactionCategory(String transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public List<String> getTransactionTypes() {
        return transactionTypes;
    }

    public void setTransactionTypes(List<String> transactionTypes) {
        this.transactionTypes = transactionTypes;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}