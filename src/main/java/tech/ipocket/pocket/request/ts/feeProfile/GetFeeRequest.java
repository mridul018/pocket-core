package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class GetFeeRequest extends BaseRequestObject {
    private String feeCode;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }
}
