package tech.ipocket.pocket.request.ts.irregular_transaction;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class GetDisputeRequest extends BaseRequestObject {
    private String status;
    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
