package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class GetFeeProfileRequest extends BaseRequestObject {
    private String feeProfileCode;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public String getFeeProfileCode() {
        return feeProfileCode;
    }

    public void setFeeProfileCode(String feeProfileCode) {
        this.feeProfileCode = feeProfileCode;
    }
}
