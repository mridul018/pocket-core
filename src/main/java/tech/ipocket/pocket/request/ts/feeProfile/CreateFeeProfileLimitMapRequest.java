package tech.ipocket.pocket.request.ts.feeProfile;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.response.ErrorObject;
import tech.ipocket.pocket.utils.PocketErrorCode;

public class CreateFeeProfileLimitMapRequest extends BaseRequestObject {
    private String feeProfileCode;
    private String serviceCode;
    private String feeCode;


    @Override
    public void validate(BaseResponseObject baseResponseObject) {

        if (feeCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeeCodeRequired));
            return;
        }

        if (serviceCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.ServiceCodeRequired));
            return;
        }

        if (feeProfileCode == null) {
            baseResponseObject.setError(new ErrorObject(PocketErrorCode.FeePayerRequired));
            return;
        }
    }

    public String getFeeProfileCode() {
        return feeProfileCode;
    }

    public void setFeeProfileCode(String feeProfileCode) {
        this.feeProfileCode = feeProfileCode;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getFeeCode() {
        return feeCode;
    }

    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }
}
