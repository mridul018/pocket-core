package tech.ipocket.pocket.request.ts.irregular_transaction;

import tech.ipocket.pocket.request.BaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;

public class UpdateDisputeRequest extends BaseRequestObject {
    private Integer disputeId;
    private String disputeStatus;
    private String comment;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {

    }

    public Integer getDisputeId() {
        return disputeId;
    }

    public void setDisputeId(Integer disputeId) {
        this.disputeId = disputeId;
    }

    public String getDisputeStatus() {
        return disputeStatus;
    }

    public void setDisputeStatus(String disputeStatus) {
        this.disputeStatus = disputeStatus;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
