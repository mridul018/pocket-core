package tech.ipocket.pocket.request.ts.reporting;

import tech.ipocket.pocket.request.PageItemBaseRequestObject;
import tech.ipocket.pocket.response.BaseResponseObject;
import tech.ipocket.pocket.utils.PocketErrorCode;


public class GetUserListBySrWalletRequest extends PageItemBaseRequestObject {

    private String userType;
    private String srWalletId;

    @Override
    public void validate(BaseResponseObject baseResponseObject) {
        if(srWalletId==null){
            baseResponseObject.setErrorCode(PocketErrorCode.SrWalletRequired);
            return;
        }

    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSrWalletId() {
        return srWalletId;
    }

    public void setSrWalletId(String srWalletId) {
        this.srWalletId = srWalletId;
    }
}
