package tech.ipocket.pocket.request;

import tech.ipocket.pocket.response.BaseResponseObject;

public abstract class BaseRequestObject {
    private String requestId;
    private String hardwareSignature;
    private String sessionToken;
    private String deviceName;
    private String encryptedTextString;
    private String encryptedSecretKeyString;
    private String currencyCode; //BDT,AED
    private String source;  //Customer App: "CUSTOMER_APP",  Agent App: AGENT_APP

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getHardwareSignature() {
        return hardwareSignature;
    }

    public void setHardwareSignature(String hardwareSignature) {
        this.hardwareSignature = hardwareSignature;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public abstract void validate(BaseResponseObject baseResponseObject);

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getEncryptedTextString() {
        return encryptedTextString;
    }

    public void setEncryptedTextString(String encryptedTextString) {
        this.encryptedTextString = encryptedTextString;
    }

    public String getEncryptedSecretKeyString() {
        return encryptedSecretKeyString;
    }

    public void setEncryptedSecretKeyString(String encryptedSecretKeyString) {
        this.encryptedSecretKeyString = encryptedSecretKeyString;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
